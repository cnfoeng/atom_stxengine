#/ Controller version = 2.50
#/ Date = 9/20/2018 5:11 PM
#/ User remarks = 
#0
! *** ACS execution order control

AUTOEXEC:
CONID = 1;

STOPALL;
WAIT(5000);

XARRSIZE = 1000000;

INT BufferProgrmaNumber
	BufferProgrmaNumber = 0
	
! Alarm ID Define (Warning Alarm ID = (Buffer No * 10000) + Warning No, Error Alarm ID = Warning Alarm ID * -1)	
INT enum_aID_Buf0_W_SysInitProgram_Excution_Fault;
INT enum_aID_Buf0_E_SysInitProgram_Restart_Fault;
	enum_aID_Buf0_W_SysInitProgram_Excution_Fault = 1;
	enum_aID_Buf0_E_SysInitProgram_Restart_Fault = -1;
	
INT enum_aID_Buf0_W_MoCon_H0InteProgram_Excution_Fault;
INT enum_aID_Buf0_E_MoCon_H0InteProgram_Restart_Fault;
	enum_aID_Buf0_W_MoCon_H0InteProgram_Excution_Fault = 300;
	enum_aID_Buf0_E_MoCon_H0InteProgram_Restart_Fault = -300;

INT enum_aID_Buf0_W_Dyno_Engine_PID_Controller_ManagementProgram_Excution_Fault;
INT enum_aID_Buf0_E_Dyno_Engine_PID_Controller_ManagementProgram_Restart_Fault;
	enum_aID_Buf0_W_Dyno_Engine_PID_Controller_ManagementProgram_Excution_Fault = 400;
	enum_aID_Buf0_E_Dyno_Engine_PID_Controller_ManagementProgram_Restart_Fault = -400;

INT enum_aID_Buf0_W_DAQ_Program_Excution_Fault;
INT enum_aID_Buf0_E_DAQ_Program_Restart_Fault;
	enum_aID_Buf0_W_DAQ_Program_Excution_Fault = 500;
	enum_aID_Buf0_E_DAQ_Program_Restart_Fault = -500;

INT enum_aID_Buf0_W_HMIInterface_Program_Excution_Fault;
INT enum_aID_Buf0_E_HMIInterface_Program_Restart_Fault;
	enum_aID_Buf0_W_HMIInterface_Program_Excution_Fault = 600;
	enum_aID_Buf0_E_HMIInterface_Program_Restart_Fault = -600;

INT enum_aID_Buf0_W_MesurementInterface_Program_Excution_Fault;
INT enum_aID_Buf0_E_MesurementInterface_Program_Restart_Fault;
	enum_aID_Buf0_W_MesurementInterface_Program_Excution_Fault = 700;
	enum_aID_Buf0_E_MesurementInterface_Program_Restart_Fault = -700;
	
INT enum_aID_Buf0_E_SysInitProgram_IoMapping_Fault;
enum_aID_Buf0_E_SysInitProgram_IoMapping_Fault = -10001;

! *** Program Result Init
FILL(0, g_bufferProgramResult);
g_bufferProgramResultListSize = 10;

! ***MMI Show Message
DISP "[#%d buffer] program started.", BufferProgrmaNumber;

FILL(-1, g_bufferProgramResult);

CALL sr_RemoteIO_Mapping;

! *** #2 Buffer
CALL sr_Alarm_Control_Program_Excution;

! *** #1 Buffer
CALL sr_Data_Refresh_Program_Excution;

! *** #8 Buffer
CALL sr_HmiInterface_Program_Excution;
	
! *** System Initial State Check	
IF(g_bufferProgramResult(0)(1) = 100)

	! *** #3 Buffer
	CALL sr_Mode_ControlAndHost_Interface_Program_Excution;
	
	! *** #4 Buffer
	CALL sr_Dyno_Engine_PID_Controller_Management_Program_Excution;
	
	! *** #5 Buffer
	CALL sr_DAQ_Program_Excution;		
	
	! *** #9 Buffer
	!CALL sr_MesurementInterface_Program_Excution;
	
END ! 
! Progrma Complete Check
IF(g_bufferProgramResult(0)(BufferProgrmaNumber) = 700)
	! *** Program Resule Update
	g_bufferProgramResult(0)(BufferProgrmaNumber) = 99999;
END	
	
! MMI Show Message
DISP "[#%d buffer] program end.", BufferProgrmaNumber;

STOP

! *************** Subroutine  *************** !
sr_RemoteIO_Mapping:

	! *** enable autoroutines in all buffers ***
	ENABLEON;
	INT eCatNetworkSlaveNum;
	INT eCatNetworkMappingCompleteNum;
	INT eCatNodeAddressOffset;
	INT eCatSlaveIndex;
	INT index_EK1100;

	eCatSlaveIndex = 0;
	index_EK1100 = 0;
	g_eCAT_HMI_index_DI = 0;
	g_eCAT_HMI_index_DO = 0;
	g_eCAT_HMI_index_AI = 0;
	g_eCAT_HMI_index_AO = 0;
	g_eCAT_index_DI = 0;
	g_eCAT_index_DO = 0;
	g_eCAT_index_AI = 0;
	g_eCAT_index_AO = 0;
	
	! *** System Initial Program Excution ***
	FILL(0, g_eCAT_HMI_DI_Data);
	FILL(0, g_eCAT_HMI_DI_Address);
	FILL(0, g_eCAT_HMI_DI_SlaveIndex);
	FILL(0, g_eCAT_HMI_DO_Data);
	FILL(0, g_eCAT_HMI_DO_Address);
	FILL(0, g_eCAT_HMI_DO_SlaveIndex);
	FILL(0, g_eCAT_HMI_AI_Data);
	FILL(0, g_eCAT_HMI_AI_Address);
	FILL(0, g_eCAT_HMI_AI_SlaveIndex);
	FILL(0, g_eCAT_HMI_AO_Data);
	FILL(0, g_eCAT_HMI_AO_Address);
	FILL(0, g_eCAT_HMI_AO_SlaveIndex);

	FILL(0, g_eCAT_DI_Data);
	FILL(0, g_eCAT_DI_Address);
	FILL(0, g_eCAT_DI_SlaveIndex);
	FILL(0, g_eCAT_DO_Data);
	FILL(0, g_eCAT_DO_Address);
	FILL(0, g_eCAT_DO_SlaveIndex);
	FILL(0, g_eCAT_AI_Data);
	FILL(0, g_eCAT_AI_Address);
	FILL(0, g_eCAT_AI_SlaveIndex);
	FILL(0, g_eCAT_AO_Data);
	FILL(0, g_eCAT_AO_Address);
	FILL(0, g_eCAT_AO_SlaveIndex);

	CALL sr_SystemTypeDefineValiableInit;
	
	!! ***EtherCAT Mapping Data Initial
	ECUNMAP;
	WAIT 50;	
	
	! *** Get Beckhoff Slave number ***
	eCatNetworkSlaveNum = ECGETSLAVES();
	eCatNetworkMappingCompleteNum = 0;
	! MMI Show Message
	DISP "[#%d buffer] number of EtherCAT slaves = <%d>.", BufferProgrmaNumber, eCatNetworkSlaveNum;
	
	LOOP eCatNetworkSlaveNum
		
		eCatNodeAddressOffset = 0;
		
		! *** BECKHOFF Slave ***
		IF(ECGETVID(eCatSlaveIndex) = 0x00000002) 		
			! *** EK1100 EtherCAT Coupler***
			IF(ECGETPID(eCatSlaveIndex) = 0x44C2C52)	
				! *** Number of Mapping Complete node Increased ***			
				index_EK1100 = index_EK1100 + 1;
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				! *** MMI Show Message				
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);				
			
			! *** EL1008 8CH. Digital Input 24V, 3ms ***
			ELSEIF(ECGETPID(eCatSlaveIndex) = 0x03F03052)			
				eCatNodeAddressOffset = ECGETOFFSET("Input", eCatSlaveIndex)				
				
				! *** ACSPL+ Index calculation and Index Increased ***	
				IF (index_EK1100>1)
					ECIN(eCatNodeAddressOffset, g_eCAT_DI_Data(0)(g_eCAT_index_DI));	
					g_eCAT_DI_Address(0)(g_eCAT_index_DI) = eCatNodeAddressOffset;
					g_eCAT_DI_SlaveIndex(0)(g_eCAT_index_DI) = eCatSlaveIndex;
					g_eCAT_index_DI = g_eCAT_index_DI +1;
				ELSE
					ECIN(eCatNodeAddressOffset, g_eCAT_HMI_DI_Data(0)(g_eCAT_HMI_index_DI));	
					g_eCAT_HMI_DI_Address(0)(g_eCAT_HMI_index_DI) = eCatNodeAddressOffset;
					g_eCAT_HMI_DI_SlaveIndex(0)(g_eCAT_HMI_index_DI) = eCatSlaveIndex;
					g_eCAT_HMI_index_DI = g_eCAT_HMI_index_DI +1;
				END				

				! *** Number of Mapping Complete node Increased ***				
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);	
			
			! *** EL1809 16CH. Digital Input 24V, 3ms ***
			ELSEIF(ECGETPID(eCatSlaveIndex) = 0x07113052)			
				eCatNodeAddressOffset = ECGETOFFSET("Input", eCatSlaveIndex)				
				
				! *** ACSPL+ Index calculation and Index Increased ***	
				LOOP 2 ! PDO Group Num = 2 	
				
					IF (index_EK1100>1)
						ECIN(eCatNodeAddressOffset, g_eCAT_DI_Data(0)(g_eCAT_index_DI));	
						g_eCAT_DI_Address(0)(g_eCAT_index_DI) = eCatNodeAddressOffset;
						g_eCAT_DI_SlaveIndex(0)(g_eCAT_index_DI) = eCatSlaveIndex;
						g_eCAT_index_DI = g_eCAT_index_DI +1;
					ELSE
						ECIN(eCatNodeAddressOffset, g_eCAT_HMI_DI_Data(0)(g_eCAT_HMI_index_DI));	
						g_eCAT_HMI_DI_Address(0)(g_eCAT_HMI_index_DI) = eCatNodeAddressOffset;
						g_eCAT_HMI_DI_SlaveIndex(0)(g_eCAT_HMI_index_DI) = eCatSlaveIndex;
						g_eCAT_HMI_index_DI = g_eCAT_HMI_index_DI +1;
					END	! (index_EK1100>1)

					! *** Index Increased ***	
					eCatNodeAddressOffset = eCatNodeAddressOffset + 1;
				END ! LOOP 2 ! PDO Group Num = 2 
				
				! *** Number of Mapping Complete node Increased ***				
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);	
			
			! *** EL3004 4CH. Analog Input +/- 10V ***
			ELSEIF(ECGETPID(eCatSlaveIndex) = 0x0BBC3052)			
				eCatNodeAddressOffset = ECGETOFFSET("Value", eCatSlaveIndex)	
				
				! *** ACSPL+ Index calculation and Index Increased ***	
				LOOP 4 ! PDO Group Num = 2 	
				
					IF (index_EK1100>1)
						ECIN(eCatNodeAddressOffset, g_eCAT_AI_Data(0)(g_eCAT_index_AI));	
						g_eCAT_AI_Address(0)(g_eCAT_index_AI) = eCatNodeAddressOffset;
						g_eCAT_AI_SlaveIndex(0)(g_eCAT_index_AI) = eCatSlaveIndex;
						g_eCAT_index_AI = g_eCAT_index_AI +1;
					ELSE
						ECIN(eCatNodeAddressOffset, g_eCAT_HMI_AI_Data(0)(g_eCAT_HMI_index_AI));	
						g_eCAT_HMI_AI_Address(0)(g_eCAT_HMI_index_AI) = eCatNodeAddressOffset;
						g_eCAT_HMI_AI_SlaveIndex(0)(g_eCAT_HMI_index_AI) = eCatSlaveIndex;
						g_eCAT_HMI_index_AI = g_eCAT_HMI_index_AI +1;
					END	! (index_EK1100>1)

					! *** Index Increased ***	
					eCatNodeAddressOffset = eCatNodeAddressOffset + 4;
				END ! LOOP 2 ! PDO Group Num = 2 
				
				! *** Number of Mapping Complete node Increased ***				
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);	
				
			! *** EL3104 4CH. Analog Input +/- 10V Diff.***
			ELSEIF(ECGETPID(eCatSlaveIndex) = 0x0C203052)			
				eCatNodeAddressOffset = ECGETOFFSET("Value", eCatSlaveIndex)	
				
				! *** ACSPL+ Index calculation and Index Increased ***	
				LOOP 4 ! PDO Group Num = 4 	
				
					IF (index_EK1100>1)
						ECIN(eCatNodeAddressOffset, g_eCAT_AI_Data(0)(g_eCAT_index_AI));	
						g_eCAT_AI_Address(0)(g_eCAT_index_AI) = eCatNodeAddressOffset;
						g_eCAT_AI_SlaveIndex(0)(g_eCAT_index_AI) = eCatSlaveIndex;
						g_eCAT_index_AI = g_eCAT_index_AI +1;
					ELSE
						ECIN(eCatNodeAddressOffset, g_eCAT_HMI_AI_Data(0)(g_eCAT_HMI_index_AI));	
						g_eCAT_HMI_AI_Address(0)(g_eCAT_HMI_index_AI) = eCatNodeAddressOffset;
						g_eCAT_HMI_AI_SlaveIndex(0)(g_eCAT_HMI_index_AI) = eCatSlaveIndex;
						g_eCAT_HMI_index_AI = g_eCAT_HMI_index_AI +1;
					END	! (index_EK1100>1)

					! *** Index Increased ***	
					eCatNodeAddressOffset = eCatNodeAddressOffset + 4;
				END ! LOOP 4 ! PDO Group Num = 4 
				
				! *** Number of Mapping Complete node Increased ***				
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);			
			
			! *** EL3008 8CH. Analog Input +/- 10V sin.***
			ELSEIF(ECGETPID(eCatSlaveIndex) = 0x0BC03052)			
				eCatNodeAddressOffset = ECGETOFFSET("Value", eCatSlaveIndex)	
				
				LOOP 8 ! PDO Group Num = 8 				
				
					IF (index_EK1100>1)
						ECIN(eCatNodeAddressOffset, g_eCAT_AI_Data(0)(g_eCAT_index_AI));	
						g_eCAT_AI_Address(0)(g_eCAT_index_AI) = eCatNodeAddressOffset;
						g_eCAT_AI_SlaveIndex(0)(g_eCAT_index_AI) = eCatSlaveIndex;
						g_eCAT_index_AI = g_eCAT_index_AI +1;
					ELSE
						ECIN(eCatNodeAddressOffset, g_eCAT_HMI_AI_Data(0)(g_eCAT_HMI_index_AI));	
						g_eCAT_HMI_AI_Address(0)(g_eCAT_HMI_index_AI) = eCatNodeAddressOffset;
						g_eCAT_HMI_AI_SlaveIndex(0)(g_eCAT_HMI_index_AI) = eCatSlaveIndex;
						g_eCAT_HMI_index_AI = g_eCAT_HMI_index_AI +1;
					END	! (index_EK1100>1)

					! *** Index Increased ***	
					eCatNodeAddressOffset = eCatNodeAddressOffset + 4;
										
				END ! LOOP 8 ! PDO Group Num = 8 
				
				! *** Number of Mapping Complete node Increased ***				
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);
			
			! *** EL3351 1Ch. Ana. Input Resistor Bridge Terminal, Diff. 16bit***
			ELSEIF(ECGETPID(eCatSlaveIndex) = 0x0D173052)			
				eCatNodeAddressOffset = ECGETOFFSET("Value", eCatSlaveIndex)	
				
				LOOP 2 ! PDO Group Num = 2 				
				
					IF (index_EK1100>1)
						ECIN(eCatNodeAddressOffset, g_eCAT_AI_Data(0)(g_eCAT_index_AI));	
						g_eCAT_AI_Address(0)(g_eCAT_index_AI) = eCatNodeAddressOffset;
						g_eCAT_AI_SlaveIndex(0)(g_eCAT_index_AI) = eCatSlaveIndex;
						g_eCAT_index_AI = g_eCAT_index_AI +1;
					ELSE
						ECIN(eCatNodeAddressOffset, g_eCAT_HMI_AI_Data(0)(g_eCAT_HMI_index_AI));	
						g_eCAT_HMI_AI_Address(0)(g_eCAT_HMI_index_AI) = eCatNodeAddressOffset;
						g_eCAT_HMI_AI_SlaveIndex(0)(g_eCAT_HMI_index_AI) = eCatSlaveIndex;
						g_eCAT_HMI_index_AI = g_eCAT_HMI_index_AI +1;
					END	! (index_EK1100>1)
			
					! *** Index Increased					
					eCatNodeAddressOffset = eCatNodeAddressOffset + 6;
										
				END ! LOOP 2 ! PDO Group Num = 2 
				
				! *** Number of Mapping Complete node Increased ***				
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);			
			
			! *** EL3124 4Ch. Ana. Input 4-20mA Diff***
			ELSEIF(ECGETPID(eCatSlaveIndex) = 0x0C343052)			
				eCatNodeAddressOffset = ECGETOFFSET("Value", eCatSlaveIndex)	
				
				LOOP 4 ! PDO Group Num = 4 				
				
					IF (index_EK1100>1)
						ECIN(eCatNodeAddressOffset, g_eCAT_AI_Data(0)(g_eCAT_index_AI));	
						g_eCAT_AI_Address(0)(g_eCAT_index_AI) = eCatNodeAddressOffset;
						g_eCAT_AI_SlaveIndex(0)(g_eCAT_index_AI) = eCatSlaveIndex;
						g_eCAT_index_AI = g_eCAT_index_AI +1;
					ELSE
						ECIN(eCatNodeAddressOffset, g_eCAT_HMI_AI_Data(0)(g_eCAT_HMI_index_AI));	
						g_eCAT_HMI_AI_Address(0)(g_eCAT_HMI_index_AI) = eCatNodeAddressOffset;
						g_eCAT_HMI_AI_SlaveIndex(0)(g_eCAT_HMI_index_AI) = eCatSlaveIndex;
						g_eCAT_HMI_index_AI = g_eCAT_HMI_index_AI +1;
					END	! (index_EK1100>1)
			
					! *** Index Increased					
					eCatNodeAddressOffset = eCatNodeAddressOffset + 4;
										
				END ! LOOP 2 ! PDO Group Num = 2 
				
				! *** Number of Mapping Complete node Increased ***				
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);			
			
			! *** EL358 4Ch. Ana. Input 4-20mA sin***
			ELSEIF(ECGETPID(eCatSlaveIndex) = 0x0BF23052)			
				eCatNodeAddressOffset = ECGETOFFSET("Value", eCatSlaveIndex)	
				
				LOOP 8 ! PDO Group Num = 8 				
				
					IF (index_EK1100>1)
						ECIN(eCatNodeAddressOffset, g_eCAT_AI_Data(0)(g_eCAT_index_AI));	
						g_eCAT_AI_Address(0)(g_eCAT_index_AI) = eCatNodeAddressOffset;
						g_eCAT_AI_SlaveIndex(0)(g_eCAT_index_AI) = eCatSlaveIndex;
						g_eCAT_index_AI = g_eCAT_index_AI +1;
					ELSE
						ECIN(eCatNodeAddressOffset, g_eCAT_HMI_AI_Data(0)(g_eCAT_HMI_index_AI));	
						g_eCAT_HMI_AI_Address(0)(g_eCAT_HMI_index_AI) = eCatNodeAddressOffset;
						g_eCAT_HMI_AI_SlaveIndex(0)(g_eCAT_HMI_index_AI) = eCatSlaveIndex;
						g_eCAT_HMI_index_AI = g_eCAT_HMI_index_AI +1;
					END	! (index_EK1100>1)

					! *** Index Increased					
					eCatNodeAddressOffset = eCatNodeAddressOffset + 4;
										
				END ! LOOP 8 ! PDO Group Num = 8 
				
				! *** Number of Mapping Complete node Increased ***				
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);	
				
			! *** EL3208 8Ch. Ana. Input PT100(RTD) ***
			ELSEIF(ECGETPID(eCatSlaveIndex) = 0x0C883052)			
				eCatNodeAddressOffset = ECGETOFFSET("Value", eCatSlaveIndex)	
				
				LOOP 8 ! PDO Group Num = 8 				
				
					IF (index_EK1100>1)
						ECIN(eCatNodeAddressOffset, g_eCAT_AI_Data(0)(g_eCAT_index_AI));	
						g_eCAT_AI_Address(0)(g_eCAT_index_AI) = eCatNodeAddressOffset;
						g_eCAT_AI_SlaveIndex(0)(g_eCAT_index_AI) = eCatSlaveIndex;
						g_eCAT_index_AI = g_eCAT_index_AI +1;
					ELSE
						ECIN(eCatNodeAddressOffset, g_eCAT_HMI_AI_Data(0)(g_eCAT_HMI_index_AI));	
						g_eCAT_HMI_AI_Address(0)(g_eCAT_HMI_index_AI) = eCatNodeAddressOffset;
						g_eCAT_HMI_AI_SlaveIndex(0)(g_eCAT_HMI_index_AI) = eCatSlaveIndex;
						g_eCAT_HMI_index_AI = g_eCAT_HMI_index_AI +1;
					END	! (index_EK1100>1)
			
					! *** Index Increased					
					eCatNodeAddressOffset = eCatNodeAddressOffset + 4;
										
				END ! LOOP 8 ! PDO Group Num = 8 
				
				! *** Number of Mapping Complete node Increased ***				
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);			
			
			! *** EL3318 8Ch. Ana. Thermocouple(TC) ***
			ELSEIF(ECGETPID(eCatSlaveIndex) = 0x0CF63052)			
				eCatNodeAddressOffset = ECGETOFFSET("Value", eCatSlaveIndex)	
				
				LOOP 8 ! PDO Group Num = 4 				
				
					IF (index_EK1100>1)
						ECIN(eCatNodeAddressOffset, g_eCAT_AI_Data(0)(g_eCAT_index_AI));	
						g_eCAT_AI_Address(0)(g_eCAT_index_AI) = eCatNodeAddressOffset;
						g_eCAT_AI_SlaveIndex(0)(g_eCAT_index_AI) = eCatSlaveIndex;
						g_eCAT_index_AI = g_eCAT_index_AI +1;
					ELSE
						ECIN(eCatNodeAddressOffset, g_eCAT_HMI_AI_Data(0)(g_eCAT_HMI_index_AI));	
						g_eCAT_HMI_AI_Address(0)(g_eCAT_HMI_index_AI) = eCatNodeAddressOffset;
						g_eCAT_HMI_AI_SlaveIndex(0)(g_eCAT_HMI_index_AI) = eCatSlaveIndex;
						g_eCAT_HMI_index_AI = g_eCAT_HMI_index_AI +1;
					END	! (index_EK1100>1)
			
					! *** Index Increased					
					eCatNodeAddressOffset = eCatNodeAddressOffset + 4;
										
				END ! LOOP 2 ! PDO Group Num = 2 
				
				! *** Number of Mapping Complete node Increased ***				
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);			
			
			! *** EL5152 2CH. Position Measurement ***
			ELSEIF(ECGETPID(eCatSlaveIndex) = 0x14203052)			
				eCatNodeAddressOffset = ECGETOFFSET("Counter value", eCatSlaveIndex)		
				
				LOOP 2 ! PDO Group Num = 2 				
					IF (index_EK1100>1)
						ECIN(eCatNodeAddressOffset, g_eCAT_AI_Data(0)(g_eCAT_index_AI));	
						g_eCAT_AI_Address(0)(g_eCAT_index_AI) = eCatNodeAddressOffset;
						g_eCAT_AI_SlaveIndex(0)(g_eCAT_index_AI) = eCatSlaveIndex;
						g_eCAT_index_AI = g_eCAT_index_AI +1;
					ELSE
						ECIN(eCatNodeAddressOffset, g_eCAT_HMI_AI_Data(0)(g_eCAT_HMI_index_AI));	
						g_eCAT_HMI_AI_Address(0)(g_eCAT_HMI_index_AI) = eCatNodeAddressOffset;
						g_eCAT_HMI_AI_SlaveIndex(0)(g_eCAT_HMI_index_AI) = eCatSlaveIndex;
						g_eCAT_HMI_index_AI = g_eCAT_HMI_index_AI +1;
					END	! (index_EK1100>1)
				
					! *** Index Increased
					eCatNodeAddressOffset = eCatNodeAddressOffset + 10;
					
				END ! LOOP 2 ! PDO Group Num = 2 	
				
				eCatNodeAddressOffset = ECGETOFFSET("Period value", eCatSlaveIndex)		
				
				LOOP 2 ! PDO Group Num = 2 				
					IF (index_EK1100>1)
						ECIN(eCatNodeAddressOffset, g_eCAT_AI_Data(0)(g_eCAT_index_AI));	
						g_eCAT_AI_Address(0)(g_eCAT_index_AI) = eCatNodeAddressOffset;
						g_eCAT_AI_SlaveIndex(0)(g_eCAT_index_AI) = eCatSlaveIndex;
						g_eCAT_index_AI = g_eCAT_index_AI +1;
					ELSE
						ECIN(eCatNodeAddressOffset, g_eCAT_HMI_AI_Data(0)(g_eCAT_HMI_index_AI));	
						g_eCAT_HMI_AI_Address(0)(g_eCAT_HMI_index_AI) = eCatNodeAddressOffset;
						g_eCAT_HMI_AI_SlaveIndex(0)(g_eCAT_HMI_index_AI) = eCatSlaveIndex;
						g_eCAT_HMI_index_AI = g_eCAT_HMI_index_AI +1;
					END	! (index_EK1100>1)
				
					! *** Index Increased
					eCatNodeAddressOffset = eCatNodeAddressOffset + 10;
				
				END ! LOOP 2 ! PDO Group Num = 2 	
				
				! *** Number of Mapping Complete node Increased ***				
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);
					
			! *** EL5151 1CH. Position Measurement ***
			ELSEIF(ECGETPID(eCatSlaveIndex) = 0x141F3052)			
				!eCatNodeAddressOffset = ECGETOFFSET("Counter value", eCatSlaveIndex)	
				eCatNodeAddressOffset = ECGETOFFSET("Frequency value", eCatSlaveIndex)	
				
				LOOP 1 ! PDO Group Num = 1 				
					IF (index_EK1100>1)
						ECIN(eCatNodeAddressOffset, g_eCAT_AI_Data(0)(g_eCAT_index_AI));	
						g_eCAT_AI_Address(0)(g_eCAT_index_AI) = eCatNodeAddressOffset;
						g_eCAT_AI_SlaveIndex(0)(g_eCAT_index_AI) = eCatSlaveIndex;
						g_eCAT_index_AI = g_eCAT_index_AI +1;
					ELSE
						ECIN(eCatNodeAddressOffset, g_eCAT_HMI_AI_Data(0)(g_eCAT_HMI_index_AI));	
						g_eCAT_HMI_AI_Address(0)(g_eCAT_HMI_index_AI) = eCatNodeAddressOffset;
						g_eCAT_HMI_AI_SlaveIndex(0)(g_eCAT_HMI_index_AI) = eCatSlaveIndex;
						g_eCAT_HMI_index_AI = g_eCAT_HMI_index_AI +1;
					END	! (index_EK1100>1)
				
					! *** Index Increased
					eCatNodeAddressOffset = eCatNodeAddressOffset + 10;
					
				END ! LOOP 2 ! PDO Group Num = 2 	
				
				eCatNodeAddressOffset = ECGETOFFSET("Period value", eCatSlaveIndex)					
				LOOP 1 ! PDO Group Num = 2 				
					IF (index_EK1100>1)
						ECIN(eCatNodeAddressOffset, g_eCAT_AI_Data(0)(g_eCAT_index_AI));	
						g_eCAT_AI_Address(0)(g_eCAT_index_AI) = eCatNodeAddressOffset;
						g_eCAT_AI_SlaveIndex(0)(g_eCAT_index_AI) = eCatSlaveIndex;
						g_eCAT_index_AI = g_eCAT_index_AI +1;
					ELSE
						ECIN(eCatNodeAddressOffset, g_eCAT_HMI_AI_Data(0)(g_eCAT_HMI_index_AI));	
						g_eCAT_HMI_AI_Address(0)(g_eCAT_HMI_index_AI) = eCatNodeAddressOffset;
						g_eCAT_HMI_AI_SlaveIndex(0)(g_eCAT_HMI_index_AI) = eCatSlaveIndex;
						g_eCAT_HMI_index_AI = g_eCAT_HMI_index_AI +1;
					END	! (index_EK1100>1)
				
					! *** Index Increased
					eCatNodeAddressOffset = eCatNodeAddressOffset + 10;
					
				END ! LOOP 2 ! PDO Group Num = 2 	
				
				! *** Number of Mapping Complete node Increased ***				
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);
			
						
			! *** EL5101 Incremental Encoder Interface***
			ELSEIF(ECGETPID(eCatSlaveIndex) = 0x13ED3052)			
				eCatNodeAddressOffset = ECGETOFFSET("Frequency", eCatSlaveIndex)		
				!eCatNodeAddressOffset = ECGETOFFSET("Value", eCatSlaveIndex)
				
				LOOP 1 ! PDO Group Num = 1 				
					IF (index_EK1100>1)
						ECIN(eCatNodeAddressOffset, g_eCAT_AI_Data(0)(g_eCAT_index_AI));	
						g_eCAT_AI_Address(0)(g_eCAT_index_AI) = eCatNodeAddressOffset;
						g_eCAT_AI_SlaveIndex(0)(g_eCAT_index_AI) = eCatSlaveIndex;
						g_eCAT_index_AI = g_eCAT_index_AI +1;
					ELSE
						ECIN(eCatNodeAddressOffset, g_eCAT_HMI_AI_Data(0)(g_eCAT_HMI_index_AI));	
						g_eCAT_HMI_AI_Address(0)(g_eCAT_HMI_index_AI) = eCatNodeAddressOffset;
						g_eCAT_HMI_AI_SlaveIndex(0)(g_eCAT_HMI_index_AI) = eCatSlaveIndex;
						g_eCAT_HMI_index_AI = g_eCAT_HMI_index_AI +1;
					END	! (index_EK1100>1)
					
				END ! LOOP 2 ! PDO Group Num = 2 	
							
				! *** Number of Mapping Complete node Increased ***				
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);
			
			! *** EL2008 8CH. Digital Output 24V, 0.5A ***
			ELSEIF(ECGETPID(eCatSlaveIndex) = 0x07D83052)			
				eCatNodeAddressOffset = ECGETOFFSET("Output", eCatSlaveIndex)				
				
				IF (index_EK1100>1)
					ECOUT(eCatNodeAddressOffset, g_eCAT_DO_Data(0)(g_eCAT_index_DO));	
					g_eCAT_DO_Address(0)(g_eCAT_index_DO) = eCatNodeAddressOffset;
					g_eCAT_DO_SlaveIndex(0)(g_eCAT_index_DO) = eCatSlaveIndex;
					g_eCAT_index_DO = g_eCAT_index_DO +1;
				ELSE
					ECOUT(eCatNodeAddressOffset, g_eCAT_HMI_DO_Data(0)(g_eCAT_HMI_index_DO));	
					g_eCAT_HMI_DO_Address(0)(g_eCAT_HMI_index_DO) = eCatNodeAddressOffset;
					g_eCAT_HMI_DO_SlaveIndex(0)(g_eCAT_HMI_index_DO) = eCatSlaveIndex;
					g_eCAT_HMI_index_DO = g_eCAT_HMI_index_DO +1;
				END	! (index_EK1100>1)
				
				! *** Number of Mapping Complete node Increased ***				
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);
			
			! *** EL2809 16CH. Digital Output 24V, 0.5A ***
			ELSEIF(ECGETPID(eCatSlaveIndex) = 0x0AF93052)			
				eCatNodeAddressOffset = ECGETOFFSET("Output", eCatSlaveIndex)				
				
				LOOP 2 ! PDO Group Num = 2 
					IF (index_EK1100>1)
						ECOUT(eCatNodeAddressOffset, g_eCAT_DO_Data(0)(g_eCAT_index_DO));	
						g_eCAT_DO_Address(0)(g_eCAT_index_DO) = eCatNodeAddressOffset;
						g_eCAT_DO_SlaveIndex(0)(g_eCAT_index_DO) = eCatSlaveIndex;
						g_eCAT_index_DO = g_eCAT_index_DO +1;
					ELSE
						ECOUT(eCatNodeAddressOffset, g_eCAT_HMI_DO_Data(0)(g_eCAT_HMI_index_DO));	
						g_eCAT_HMI_DO_Address(0)(g_eCAT_HMI_index_DO) = eCatNodeAddressOffset;
						g_eCAT_HMI_DO_SlaveIndex(0)(g_eCAT_HMI_index_DO) = eCatSlaveIndex;
						g_eCAT_HMI_index_DO = g_eCAT_HMI_index_DO +1;
					END	! (index_EK1100>1)
									
					! *** Index Increased
					eCatNodeAddressOffset = eCatNodeAddressOffset + 1;
				END ! LOOP 2 ! PDO Group Num = 2	
				
				! *** Number of Mapping Complete node Increased ***				
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);
					
			! *** EL4034 4CH. Analog Output +/- 10V 12bit ***
			ELSEIF(ECGETPID(eCatSlaveIndex) = 0x0FC23052)			
				eCatNodeAddressOffset = ECGETOFFSET("Analog output", eCatSlaveIndex)	
				
				LOOP 4 ! PDO Group Num = 4 				
				
					IF (index_EK1100>1)
						ECOUT(eCatNodeAddressOffset, g_eCAT_AO_Data(0)(g_eCAT_index_AO));	
						g_eCAT_AO_Address(0)(g_eCAT_index_AO) = eCatNodeAddressOffset;
						g_eCAT_AO_SlaveIndex(0)(g_eCAT_index_AO) = eCatSlaveIndex;
						g_eCAT_index_AO = g_eCAT_index_AO +1;
					ELSE
						ECOUT(eCatNodeAddressOffset, g_eCAT_HMI_DO_Data(0)(g_eCAT_HMI_index_AO));	
						g_eCAT_HMI_AO_Address(0)(g_eCAT_HMI_index_AO) = eCatNodeAddressOffset;
						g_eCAT_HMI_AO_SlaveIndex(0)(g_eCAT_HMI_index_AO) = eCatSlaveIndex;
						g_eCAT_HMI_index_AO = g_eCAT_HMI_index_AO +1;
					END	! (index_EK1100>1)
				
					! *** Index Increased
					eCatNodeAddressOffset = eCatNodeAddressOffset + 2;
				END ! LOOP 4 ! PDO Group Num = 4 	
				
				! *** Number of Mapping Complete node Increased ***				
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);
			
			! *** EL4134 4CH. Analog Output +/- 10V 16bit ***
			ELSEIF(ECGETPID(eCatSlaveIndex) = 0x10263052)			
				eCatNodeAddressOffset = ECGETOFFSET("Analog output", eCatSlaveIndex)	
				
				LOOP 4 ! PDO Group Num = 4 				
				
					IF (index_EK1100>1)
						ECOUT(eCatNodeAddressOffset, g_eCAT_AO_Data(0)(g_eCAT_index_AO));	
						g_eCAT_AO_Address(0)(g_eCAT_index_AO) = eCatNodeAddressOffset;
						g_eCAT_AO_SlaveIndex(0)(g_eCAT_index_AO) = eCatSlaveIndex;
						g_eCAT_index_AO = g_eCAT_index_AO +1;
					ELSE
						ECOUT(eCatNodeAddressOffset, g_eCAT_HMI_DO_Data(0)(g_eCAT_HMI_index_AO));	
						g_eCAT_HMI_AO_Address(0)(g_eCAT_HMI_index_AO) = eCatNodeAddressOffset;
						g_eCAT_HMI_AO_SlaveIndex(0)(g_eCAT_HMI_index_AO) = eCatSlaveIndex;
						g_eCAT_HMI_index_AO = g_eCAT_HMI_index_AO +1;
					END	! (index_EK1100>1)
				
					! *** Index Increased
					eCatNodeAddressOffset = eCatNodeAddressOffset + 2;
										
				END ! LOOP 4 ! PDO Group Num = 4 	
				
				! *** Number of Mapping Complete node Increased ***				
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);
				
			! *** EL4138 8CH. Analog Output +/- 10V 12bit ***			
			ELSEIF(ECGETPID(eCatSlaveIndex) = 0x0FC63052)			
				eCatNodeAddressOffset = ECGETOFFSET("Analog output", eCatSlaveIndex)	
				
				LOOP 8 ! PDO Group Num = 4 				
				
					IF (index_EK1100>1)
						ECOUT(eCatNodeAddressOffset, g_eCAT_AO_Data(0)(g_eCAT_index_AO));	
						g_eCAT_AO_Address(0)(g_eCAT_index_AO) = eCatNodeAddressOffset;
						g_eCAT_AO_SlaveIndex(0)(g_eCAT_index_AO) = eCatSlaveIndex;
						g_eCAT_index_AO = g_eCAT_index_AO +1;
					ELSE
						ECOUT(eCatNodeAddressOffset, g_eCAT_HMI_DO_Data(0)(g_eCAT_HMI_index_AO));	
						g_eCAT_HMI_AO_Address(0)(g_eCAT_HMI_index_AO) = eCatNodeAddressOffset;
						g_eCAT_HMI_AO_SlaveIndex(0)(g_eCAT_HMI_index_AO) = eCatSlaveIndex;
						g_eCAT_HMI_index_AO = g_eCAT_HMI_index_AO +1;
					END	! (index_EK1100>1)
				
					! *** Index Increased
					eCatNodeAddressOffset = eCatNodeAddressOffset + 2;
										
				END ! LOOP 4 ! PDO Group Num = 4 	
				
				! *** Number of Mapping Complete node Increased ***				
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);	
			
			! *** EL 6002 Interface 2Ch. (RS232)***
			ELSEIF(ECGETPID(eCatSlaveIndex) = 0x17723052)	
				INT CHANNEL_OFFSET;
				CHANNEL_OFFSET = 0;
				
				LOOP 2 ! CHANNEL=2
					! *** Channel #1 Status Data Mapping
					eCatNodeAddressOffset = ECGETOFFSET("Status", eCatSlaveIndex) + CHANNEL_OFFSET
					ECIN(eCatNodeAddressOffset, g_eCAT_AI_Data(0)(g_eCAT_index_AI));	
					g_eCAT_AI_Address(0)(g_eCAT_index_AI) = eCatNodeAddressOffset;
					g_eCAT_AI_SlaveIndex(0)(g_eCAT_index_AI) = eCatSlaveIndex;
					g_eCAT_index_AI = g_eCAT_index_AI +1;
						
					! *** Channel #1 Input Data Mapping
					eCatNodeAddressOffset = ECGETOFFSET("Data In 0", eCatSlaveIndex) + CHANNEL_OFFSET
					
					LOOP 22 ! PDO Group Num = 1 				
						ECIN(eCatNodeAddressOffset, g_eCAT_AI_Data(0)(g_eCAT_index_AI));	
						g_eCAT_AI_Address(0)(g_eCAT_index_AI) = eCatNodeAddressOffset;
						g_eCAT_AI_SlaveIndex(0)(g_eCAT_index_AI) = eCatSlaveIndex;
						g_eCAT_index_AI = g_eCAT_index_AI +1;
						
						! *** Index Increased
						eCatNodeAddressOffset = eCatNodeAddressOffset + 1;
					END ! LOOP 22 ! PDO Group Num = 2 			
						
					! *** Channel #1 Control Data Mapping	
					eCatNodeAddressOffset = ECGETOFFSET("Ctrl", eCatSlaveIndex) + CHANNEL_OFFSET
					
					ECOUT(eCatNodeAddressOffset, g_eCAT_AO_Data(0)(g_eCAT_index_AO));	
					g_eCAT_AO_Address(0)(g_eCAT_index_AO) = eCatNodeAddressOffset;
					g_eCAT_AO_SlaveIndex(0)(g_eCAT_index_AO) = eCatSlaveIndex;
					g_eCAT_index_AO = g_eCAT_index_AO +1;
						
					! *** Channel #1 Output Data Mapping	
					eCatNodeAddressOffset = ECGETOFFSET("Data Out 0", eCatSlaveIndex) + CHANNEL_OFFSET			
					
					LOOP 22 ! PDO Group Num = 1 				
						
						ECOUT(eCatNodeAddressOffset, g_eCAT_AO_Data(0)(g_eCAT_index_AO));	
						g_eCAT_AO_Address(0)(g_eCAT_index_AO) = eCatNodeAddressOffset;
						g_eCAT_AO_SlaveIndex(0)(g_eCAT_index_AO) = eCatSlaveIndex;
						g_eCAT_index_AO = g_eCAT_index_AO +1;
						
						! *** Index Increased
						eCatNodeAddressOffset = eCatNodeAddressOffset + 1;
						
					END ! LOOP 22 ! PDO Group Num = 2 
					
					CHANNEL_OFFSET = 24;
				END !LOOP 2 ! CHANNEL=2
				
				! *** eEherCAT COM #1 
				COEWRITE/1 (eCatSlaveIndex, 0x8000, 0x01, 0x00)	! RTS/CTS off
				COEWRITE/1 (eCatSlaveIndex, 0x8000, 0x11, 0x06)	! 9,600 bps
				COEWRITE/1 (eCatSlaveIndex, 0x8000, 0x15, 0x03)	! 8N1
				
				! *** eEherCAT COM #2 
				COEWRITE/1 (eCatSlaveIndex, 0x8010, 0x01, 0x00)	! RTS/CTS off
				COEWRITE/1 (eCatSlaveIndex, 0x8010, 0x11, 0x06)	! 9,600 bps
				COEWRITE/1 (eCatSlaveIndex, 0x8010, 0x15, 0x03)	! 8N1

				! *** Number of Mapping Complete node Increased ***				
				eCatNetworkMappingCompleteNum = eCatNetworkMappingCompleteNum + 1;
				DISP "[#%d buffer] Node Index = <%d>, Product ID = <0x%X>.", BufferProgrmaNumber, eCatSlaveIndex, ECGETPID(eCatSlaveIndex);
					
						
			ELSE
				! *** MMI Show Message	
				DISP "[#%d buffer] Node Index = <%d>, Can not find product.", BufferProgrmaNumber, eCatSlaveIndex;
			END ! IF(ECGETPID(NodeIdx) = 0x03F03052)
			
		END ! IF(ECGETVID(NodeIdx) = 0x00000002)
		
		eCatSlaveIndex = eCatSlaveIndex + 1;
		
	END !LOOP TotalNodeCount
	
	! IO Mapping Complete Check
	IF(eCatNetworkSlaveNum <> eCatNetworkMappingCompleteNum )
		! *** Error register ***
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf0_E_SysInitProgram_IoMapping_Fault;		
		g_Alarm_Register_Request_Bit = 1;
		
		! *** Program Resule Update
		g_bufferProgramResult(0)(BufferProgrmaNumber) = -100;
		
		STOPALL;
		
	ELSE
		!	COEWRITE/1 (5, 0x8000,0x03,0);
		!CALL sr_EL6002_SerialInterface_COE_Setting;
		
		! *** MMI Show Message	
		DISP "[#%d buffer] EtherCAT Mapping Complete.", BufferProgrmaNumber, eCatSlaveIndex;
	END
	
	
RET

! *************** Subroutine  *************** !
sr_SystemTypeDefineValiableInit:
	! *** Dyno Type Init***
	g_dynoType_EC = 100;
	g_dynoType_AC = 200;
	
	g_dynoType = g_dynoType_AC;
	
RET

! ----------------------------------------------------------------------------------------------
sr_Data_Refresh_Program_Excution:

	! *** enable autoroutines in all buffers ***
	ENABLEON;
	
	! *** Data_Refresh Program Excution ***
	LOOP 10 ! *** Restart program(Buffer #1).
		! *** Data handling Program(Buffer 1) Start ***
		START 1, 1;
		
			! *** Progrma Resule Check ***
			WAIT (1000);			
			IF(PST(1).#RUN = 0)
				! MMI Show Message
				! *** Warning register ***
					
				IF (PERR(1)>0)
					DISP "[#%d buffer] HMI Variable Read - [Error] Threr is no data. PERR(1)= %d , PERL(1)= %d", BufferProgrmaNumber,PERR(1),PERL(1);	
					CALL sr_HMI_Variable_Write_toFlashMemory;
					READ gl_HmiVar_FlashRead, HmiVal_FlashWrite
				END 

				g_Alarm_Register_Request_AlarmID = enum_aID_Buf0_W_SysInitProgram_Excution_Fault;		
				g_Alarm_Register_Request_Bit = 1;
				DISP "[#%d buffer program] excution errors. Buffer Program Num = <1>", BufferProgrmaNumber;
				!STOPALL;				
				WAIT (100);
			ELSE
				GOTO lable_DataRefreshProgramEnd;
			END !IF(PST(2).#RUN = 0)
	
	END !LOOP 10 ! *** Re-initializaion program(Buffer #2) run.
	
	! *** Alarm Control Program End ***
	lable_DataRefreshProgramEnd:
	
	! *** Alarm Control Program Complete Check
	IF(PST(1).#RUN = 0)
		! *** Error register ***	
		!FILL(0, g_eCAT_Input_Data);
		!FILL(0, g_eCAT_Output_Data);
		
		! *** Program Resule Update
		g_bufferProgramResult(0)(BufferProgrmaNumber) = -100;
		
		STOPALL;	
	END !IF(g_bufferProgramResult(0)(2) <> 99999)
	
RET

! ----------------------------------------------------------------------------------------------


sr_Alarm_Control_Program_Excution:
	! *** Alarm Control Program Excution ***
	LOOP 10 ! *** Restart program(Buffer #2).
		! *** Data handling Program(Buffer 2) Start ***
		START 2, 1;
		
			! *** Progrma Resule Check ***
			WAIT (1000);			
			IF(PST(2).#RUN = 0)
				! MMI Show Message
				DISP "[#%d buffer program] excution errors. Buffer Program Num = <2>", BufferProgrmaNumber;
				STOPALL;				
				WAIT (100);
			ELSE
				GOTO lable_DataHandlingProgramEnd;
			END !IF(PST(2).#RUN = 0)
	
	END !LOOP 10 ! *** Re-initializaion program(Buffer #2) run.
	
	! *** Alarm Control Program End ***
	lable_DataHandlingProgramEnd:
	
	! *** Alarm Control Program Complete Check
	IF(PST(2).#RUN = 0)
		! *** Error register ***	
		!FILL(0, g_eCAT_Input_Data);
		!FILL(0, g_eCAT_Output_Data);
		
		! *** Program Resule Update
		g_bufferProgramResult(0)(BufferProgrmaNumber) = -200;
		
		STOPALL;	
	END !IF(g_bufferProgramResult(0)(2) <> 99999)
	
RET

! ----------------------------------------------------------------------------------------------

sr_Mode_ControlAndHost_Interface_Program_Excution:
	! *** Mode Control & Host Interface Program Excution ***
	LOOP 10 ! *** Restart program(Buffer #3).
		! *** Mode Control Program(Buffer 3) Start ***
		START 3, 1;
		
			! *** Progrma Resule Check ***
			WAIT (1000);			
			IF(PST(3).#RUN = 0)
				! MMI Show Message
				DISP "[#%d buffer program] excution errors. Buffer Program Num = <3>", BufferProgrmaNumber;
				STOPALL;				
				WAIT (100);
				
				! *** Warning register ***
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf0_W_MoCon_H0InteProgram_Excution_Fault;		
				g_Alarm_Register_Request_Bit = 1;
				
			ELSE
				GOTO lable_ModeControlProgramExcutionEnd;
			END !IF(PST(3).#RUN = 0)
	
	END !LOOP 10 ! *** Re-initializaion program(Buffer #3) run.
	
	! *** Mode Control & Host Interface Program End ***
	lable_ModeControlProgramExcutionEnd:

	! *** Mode Control & Host Interface Program Complete Check
	IF(PST(3).#RUN = 0)
		! *** Error register ***
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf0_E_MoCon_H0InteProgram_Restart_Fault;		
		g_Alarm_Register_Request_Bit = 1;
		
		! *** Program Resule Update
		g_bufferProgramResult(0)(BufferProgrmaNumber) = -300;
		
		STOPALL;	
	END ! IF(g_bufferProgramResult(0)(3) <> 99999)
	
RET

! ----------------------------------------------------------------------------------------------

sr_Dyno_Engine_PID_Controller_Management_Program_Excution:
	! *** Dynamometer & Engine PID Controller Management Program ***
	LOOP 10 ! *** Restart program(Buffer #4).
		! *** Mode Control Program(Buffer 4) Start ***
		START 4, 1;
		
			! *** Progrma Resule Check ***
			WAIT (1000);			
			IF(PST(4).#RUN = 0)
				! MMI Show Message
				DISP "[#%d buffer program] excution errors. Buffer Program Num = <4>", BufferProgrmaNumber;
				STOPALL;				
				WAIT (100);
				
				! *** Warning register ***
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf0_W_Dyno_Engine_PID_Controller_ManagementProgram_Excution_Fault;		
				g_Alarm_Register_Request_Bit = 1;
				
			ELSE
				GOTO lable_Dyno_Engine_PID_Controller_ManagementProgramExcutionEnd;
			END !IF(PST(4).#RUN = 0)
	
	END !LOOP 10 ! *** Re-initializaion program(Buffer #4) run.
	
	! *** Dynamometer & Engine PID Controller Management Program End ***
	lable_Dyno_Engine_PID_Controller_ManagementProgramExcutionEnd:

	! *** Dynamometer & Engine PID Controller Management Program Complete Check
	IF(PST(4).#RUN = 0)
		! *** Error register ***
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf0_E_Dyno_Engine_PID_Controller_ManagementProgram_Restart_Fault;		
		g_Alarm_Register_Request_Bit = 1;
		
		! *** Program Resule Update
		g_bufferProgramResult(0)(BufferProgrmaNumber) = -400;
		
		STOPALL;	
	END ! IF(g_bufferProgramResult(0)(4) <> 99999)
	
RET

! ----------------------------------------------------------------------------------------------

sr_DAQ_Program_Excution:
	! *** DAQ Program ***
	LOOP 10 ! *** Restart program(Buffer #7).
		! *** Mode Control Program(Buffer 7) Start ***
		START 7, 1;
		
			! *** Progrma Resule Check ***
			WAIT (1000);			
			IF(PST(7).#RUN = 0)
				! MMI Show Message
				DISP "[#%d buffer program] excution errors. Buffer Program Num = <7>", BufferProgrmaNumber;
				STOPALL;				
				WAIT (100);
				
				! *** Warning register ***
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf0_W_DAQ_Program_Excution_Fault;		
				g_Alarm_Register_Request_Bit = 1;
				
			ELSE
				GOTO lable_DAQ_ProgramExcutionEnd;
			END !IF(PST(7).#RUN = 0)
	
	END !LOOP 10 ! *** Re-initializaion program(Buffer #7) run.
	
	! *** DAQ Program Excution End ***
	lable_DAQ_ProgramExcutionEnd:

	! *** DAQ Program Complete Check
	IF(PST(7).#RUN = 0)
		! *** Error register ***
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf0_E_DAQ_Program_Restart_Fault;		
		g_Alarm_Register_Request_Bit = 1;
		
		! *** Program Resule Update
		g_bufferProgramResult(0)(BufferProgrmaNumber) = -500;
		
		STOPALL;	
	END ! IF(PST(7).#RUN = 0)
	
RET

! ----------------------------------------------------------------------------------------------

sr_HmiInterface_Program_Excution:
	! *** HMI Interface Program ***
	LOOP 10 ! *** Restart program(Buffer #8).
		! *** Mode Control Program(Buffer 8) Start ***
		START 8, 1;
		
			! *** Progrma Resule Check ***
			WAIT (1000);			
			IF(PST(8).#RUN = 0)
				! MMI Show Message
				DISP "[#%d buffer program] excution errors. Buffer Program Num = <8>", BufferProgrmaNumber;
				STOPALL;				
				WAIT (100);
				
				! *** Warning register ***
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf0_W_HMIInterface_Program_Excution_Fault;		
				g_Alarm_Register_Request_Bit = 1;
				
			ELSE
				GOTO lable_HmiInterface_ProgramExcutionEnd;
			END !IF(PST(8).#RUN = 0)
	
	END !LOOP 10 ! *** Re-initializaion program(Buffer #8) run.
	
	! *** DAQ Program Excution End ***
	lable_HmiInterface_ProgramExcutionEnd:

	! *** DAQ Program Complete Check
	IF(PST(8).#RUN = 0)
		! *** Error register ***
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf0_E_HMIInterface_Program_Restart_Fault;		
		g_Alarm_Register_Request_Bit = 1;
		
		! *** Program Resule Update
		g_bufferProgramResult(0)(BufferProgrmaNumber) = -600;
		
		STOPALL;	
	END ! IF(PST(8).#RUN = 0)
	
RET

! ----------------------------------------------------------------------------------------------

sr_MesurementInterface_Program_Excution:
	! *** Mesurement Interface Program ***
	LOOP 10 ! *** Restart program(Buffer #9).
		! *** Mode Control Program(Buffer 9) Start ***
		START 9, 1;
		
			! *** Progrma Resule Check ***
			WAIT (1000);			
			IF(PST(9).#RUN = 0)
				! MMI Show Message
				DISP "[#%d buffer program] excution errors. Buffer Program Num = <9>", BufferProgrmaNumber;
				STOPALL;				
				WAIT (100);
				
				! *** Warning register ***
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf0_W_MesurementInterface_Program_Excution_Fault;		
				g_Alarm_Register_Request_Bit = 1;
				
			ELSE
				GOTO lable_MesurementInterface_ProgramExcutionEnd;
			END !IF(PST(8).#RUN = 0)
	
	END !LOOP 10 ! *** Re-initializaion program(Buffer #8) run.
	
	! *** Mesurement Interface Program Excution End ***
	lable_MesurementInterface_ProgramExcutionEnd:

	! *** Mesurement Interface Program Complete Check
	IF(PST(9).#RUN = 0)
		! *** Error register ***
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf0_E_MesurementInterface_Program_Restart_Fault;		
		g_Alarm_Register_Request_Bit = 1;
		
		! *** Program Resule Update
		g_bufferProgramResult(0)(BufferProgrmaNumber) = -700;
		
		STOPALL;	
	END ! IF(PST(9).#RUN = 0)
	
RET

! ----------------------------------------------------------------------------------------------

sr_HMI_Variable_Write_toFlashMemory:
	
	! *** Variable zero clear
	int index_HmiTagVar_FlashAccess;
	FILL(0, gl_HmiVar_FlashWrite);
		
		! *** List Index & Tag Max Number Set
		index_HmiTagVar_FlashAccess = 0;	
		g_Hmi_UserDefine_TagCount = 880; ! = MAX TAG Number - 1000 
	
		LOOP (g_Hmi_UserDefine_TagCount)
			
			gl_HmiVar_FlashWrite(index_HmiTagVar_FlashAccess) = GETVAR((index_HmiTagVar_FlashAccess + 1000));			
			index_HmiTagVar_FlashAccess = index_HmiTagVar_FlashAccess + 1;
			
		END !LOOP (g_Hmi_UserDefine_TagCount)
				
			
	! *** Write Variable From Nonvolatile Memory
	WRITE gl_HmiVar_FlashWrite, HmiVal_FlashWrite
	
RET


#1
! *** System Initial Program

INT BufferProgrmaNumber
	BufferProgrmaNumber = 1

! Alarm ID Define (Warning Alarm ID = (Buffer No * 10000) + Warning No, Error Alarm ID = Warning Alarm ID * -1)	
	! *** Program Resule Update
	g_bufferProgramResult(0)(BufferProgrmaNumber) = 100;


INT enum_aID_Buf1_E_CalibrationDataInit_FbSpeedReferanceInit_Fail;
	enum_aID_Buf1_E_CalibrationDataInit_FbSpeedReferanceInit_Fail = -10101;
INT enum_aID_Buf1_E_CalibrationDataInit_FbSpeedAppliedInit_Fail;
	enum_aID_Buf1_E_CalibrationDataInit_FbSpeedAppliedInit_Fail = -10102;
INT enum_aID_Buf1_E_CalibrationDataInit_FbTorqueReferanceInit_Fail;
	enum_aID_Buf1_E_CalibrationDataInit_FbTorqueReferanceInit_Fail = -10103;
INT enum_aID_Buf1_E_CalibrationDataInit_FbTorqueAppliedInit_Fail;
	enum_aID_Buf1_E_CalibrationDataInit_FbTorqueAppliedInit_Fail = -10104;
INT enum_aID_Buf1_E_CalibrationDataInit_FbCurrentReferanceInit_Fail;
	enum_aID_Buf1_E_CalibrationDataInit_FbCurrentReferanceInit_Fail = -10105;
INT enum_aID_Buf1_E_CalibrationDataInit_FbCurrentAppliedInit_Fail;
	enum_aID_Buf1_E_CalibrationDataInit_FbCurrentAppliedInit_Fail = -10106;	
INT enum_aID_Buf1_E_CalibrationDataInit_OpCurrentReferanceInit_Fail;
	enum_aID_Buf1_E_CalibrationDataInit_OpCurrentReferanceInit_Fail = -10107;
INT enum_aID_Buf1_E_CalibrationDataInit_OpCurrentAppliedInit_Fail;
	enum_aID_Buf1_E_CalibrationDataInit_OpCurrentAppliedInit_Fail = -10108;		

! *** Feedback A/D Val Average
REAL AI_Feedback_Alpha;

REAL Hmi_bs500x_FB_SpeedADValList(1000);
REAL Hmi_bs500x_FB_TorqueADValList(1000);
REAL Hmi_bs500x_FB_CurrentADValList(1000);
REAL AI_Feedback_Alpha_List(300);
INT index_Hmi_bs500x_FB_SpeedADValMaxIndex;
INT index_Hmi_bs500x_FB_TorqueADValMaxIndex;
INT index_Hmi_bs500x_FB_CurrentADValMaxIndex;
INT index_Hmi_bs500x_FB_ALPHAADValMaxIndex;
	FILL(0, Hmi_bs500x_FB_SpeedADValList);
	FILL(0, Hmi_bs500x_FB_TorqueADValList);
	FILL(0, Hmi_bs500x_FB_CurrentADValList);
	index_Hmi_bs500x_FB_SpeedADValMaxIndex = 999;
	index_Hmi_bs500x_FB_TorqueADValMaxIndex = 999;
	index_Hmi_bs500x_FB_CurrentADValMaxIndex = 999;
	index_Hmi_bs500x_FB_ALPHAADValMaxIndex = 299;

! *** Feedback Torque Calculation variable	
REAL Calc_Torque_VoltToTorque_CalcResult;
REAL Average_TorqueList(400);
INT index_Average_TorqueListLastIndex;	
	FILL(0, Average_TorqueList);
	index_Average_TorqueListLastIndex = 399
REAL Average_FeedbackTorqueList(100);
INT index_Average_FeedbackTorqueListLastIndex;	
	FILL(0, Average_FeedbackTorqueList);
	index_Average_FeedbackTorqueListLastIndex = 99
	
! MMI Show Message
DISP "[#%d buffer] program started.", BufferProgrmaNumber;

	CALL sr_PcWrite_DataInit;	
		
	CALL sr_Calibration_DataInit;	
	
	CALL sr_unit_DynoEngineCmd_DA_ConstantRatioInit;	
	
	CALL sr_SafetyControllerReset;
	
	CALL sr_Init_GlobalVariables;
	
	CALL sr_AutomationSW_EtherCATOutput_Update_Valiable_Declaration

	
	WAIT(100)
	
	INT TimeMeasure_1;
	INT TimeMeasure_2;
	TimeMeasure_1 = TIME;
	TimeMeasure_2 = TIME;
	DISP TimeMeasure_2-TimeMeasure_1;
	int i
	i=0;

	WHILE 1		
		
		!BLOCK !sr_IO_Data_Update;
			!IF (i=10)
			!	TimeMeasure_2 = TIME;
			!	DISP TimeMeasure_2-TimeMeasure_1
			!	TimeMeasure_1 = TimeMeasure_2;
			!	i=0;
			!END
			CALL sr_IO_Data_Update;
			!CALL sr_AutomationSW_EtherCATOutput_Update
			!i = i + 1;
		!END !sr_IO_Data_Update;
		
	END !WHILE 1

! *** Program Resule Update
g_bufferProgramResult(0)(BufferProgrmaNumber) = 99999;
	
! *** MMI Terminal display
DISP "[#%d buffer] program end.", BufferProgrmaNumber;

STOP

! *************** Subroutine  *************** !
sr_HMI_Variable_Read_fromFlashMemory:
	
	INT index_HmiTagVar_FlashAccess;
	
	! *** Variable zero clear
	FILL(0, gl_HmiVar_FlashRead)

	! *** Read Variable From Nonvolatile Memory
	READ gl_HmiVar_FlashRead, HmiVal_FlashWrite
	
	! *** List Index & Tag Max Number Set
	index_HmiTagVar_FlashAccess = 70;	
	g_Hmi_UserDefine_TagCount = 880 - index_HmiTagVar_FlashAccess; ! = MAX TAG Number - 1000 
	
	! *** Check Read Variable 
	IF (MAX(gl_HmiVar_FlashRead) > 0)		
		
		LOOP (g_Hmi_UserDefine_TagCount)
			
			SETVAR(gl_HmiVar_FlashRead(index_HmiTagVar_FlashAccess), (index_HmiTagVar_FlashAccess + 1000))
			index_HmiTagVar_FlashAccess = index_HmiTagVar_FlashAccess + 1;
			
		END !LOOP (g_Hmi_UserDefine_TagCount)
		
		
	ELSE 
	
		LOOP (g_Hmi_UserDefine_TagCount)
			
			SETVAR(gl_HmiVar_FlashRead(index_HmiTagVar_FlashAccess), (index_HmiTagVar_FlashAccess + 1000))
			index_HmiTagVar_FlashAccess = index_HmiTagVar_FlashAccess + 1;
			
		END !LOOP (g_Hmi_UserDefine_TagCount)
		
		! *** MMI Show Message						
		DISP "[#%d buffer] HMI Variable Read - [Error] Threr is no data.", BufferProgrmaNumber;	
		
	END !IF (MAX(gl_HmiVar_FlashRead) > 0)
	
	IF( g_Hmi_bs2201_EditSaveTOutSt < 5000) g_Hmi_bs2201_EditSaveTOutSt = 5000; END;
	
RET

! ----------------------------------------------------------------------------------------------

sr_HMI_Variable_Write_toFlashMemory:

	! *** Variable zero clear
	FILL(0, gl_HmiVar_FlashWrite)
		
		! *** List Index & Tag Max Number Set
		index_HmiTagVar_FlashAccess = 0;	
		g_Hmi_UserDefine_TagCount = 880; ! = MAX TAG Number - 1000 
	
		LOOP (g_Hmi_UserDefine_TagCount)
			
			gl_HmiVar_FlashWrite(index_HmiTagVar_FlashAccess) = GETVAR((index_HmiTagVar_FlashAccess + 1000));			
			index_HmiTagVar_FlashAccess = index_HmiTagVar_FlashAccess + 1;
			
		END !LOOP (g_Hmi_UserDefine_TagCount)
				
			
	! *** Write Variable From Nonvolatile Memory
	WRITE gl_HmiVar_FlashWrite, HmiVal_FlashWrite
	
RET

! ----------------------------------------------------------------------------------------------

sr_Calibration_DataInit:
	
	g_Hmi_bs4001_DynoSpid_KpSt = 1.000;
	g_Hmi_bs4001_DynoSpid_KiSt = 1.000;
	g_Hmi_bs4001_DynoSpid_Kdst = 1.000;
	g_Hmi_bs5501_Dyno_CIndexSt = 1.000;
	g_Hmi_bs4003_DynoSpid_CMaxSt = 10000;
	g_Hmi_bs4003_DynoSpid_CcntMaxSt = 10000;
	g_Hmi_bs4002_DynoSpid_RefADeltaSt = 1.000;
	g_Hmi_bs4002_DynoSpid_RefDDeltaSt = 1.000;
	g_Hmi_bs5001_FB_SpeedYIndexSt = 1.000;
	g_Hmi_bs4002_DynoSpid_YMinSt = 1.000;
	g_Hmi_bs4004_DynoSpid_C_ADeltaSt = 1.000;
	g_Hmi_bs4004_DynoSpid_C_DDeltaSt = 1.000;
	g_Hmi_bs4101_DynoTpid_KpSt = 1.000;
	g_Hmi_bs4101_DynoTpid_KiSt = 1.000;
	g_Hmi_bs4101_DynoTpid_Kdst = 1.000;
	g_Hmi_bs5501_Dyno_CIndexSt = 1.000;
	g_Hmi_bs4103_DynoTpid_CMaxSt = 10000;
	g_Hmi_bs4103_DynoTpid_CcntMaxSt = 10000;
	g_Hmi_bs4102_DynoTpid_RefADeltaSt = 1.000;
	g_Hmi_bs4102_DynoTpid_RefDDeltaSt = 1.000;
	g_Hmi_bs5101_FB_TorqueYIndexSt = 1.000;
	g_Hmi_bs4102_DynoTpid_YMinSt = 1.000;
	g_Hmi_bs4104_DynoTpid_C_ADeltaSt = 1.000;
	g_Hmi_bs4104_DynoTpid_C_DDeltaSt = 1.000;
	g_Hmi_bs4001_DynoSpid_KpSt = 1.000;
	g_Hmi_bs4001_DynoSpid_KpSt = 1.000;
	g_Hmi_bs4001_DynoSpid_KpSt = 1.000;
	g_Hmi_bs4001_DynoSpid_KpSt = 1.000;
	g_Hmi_bs4001_DynoSpid_KpSt = 1.000;
	g_Hmi_bs4001_DynoSpid_KpSt = 1.000;
	g_Hmi_bs4001_DynoSpid_KpSt = 1.000;
	g_Hmi_bs4001_DynoSpid_KpSt = 1.000;
	g_Hmi_bs4001_DynoSpid_KpSt = 1.000;
	g_Hmi_bs4001_DynoSpid_KpSt = 1.000;

	INT index_LinearInterpolation_FlashReadValAccess;	
	INT FlashRead_LinearInterpolationReadCount;
	INT index_LinearInterpolation_List
	INT indexOffset_FlashReadRefeVal_AppliedVal;
		
	FILL(0, g_lInter_fbTorque_RefeVal_List);
	FILL(0, g_lInter_fbSpeed_RefeVal_List);
	FILL(0, g_lInter_fbAlpha_RefeVal_List);
	FILL(0, g_lInter_fbDynoCurrent_RefeVal_List);
	FILL(0, g_lInter_fbTorque_AppliedVal_List);
	FILL(0, g_lInter_fbSpeed_AppliedVal_List);
	FILL(0, g_lInter_fbAlpha_AppliedVal_List);
	FILL(0, g_lInter_fbDynoCurrent_AppliedVal_List);

	! *** Read Variable From Nonvolatile Memory
	CALL sr_HMI_Variable_Read_fromFlashMemory;
	IF (PERR(1)>0) !& PERL(1)=163)
		CALL sr_HMI_Variable_Write_toFlashMemory;
	END

	! *** Linear Interporation Data Init - Speed Feedback 
	index_LinearInterpolation_FlashReadValAccess = 522;	! *** Tag Address - HMI Display area(100)
	FlashRead_LinearInterpolationReadCount = 11;
	index_LinearInterpolation_List = 0;
	indexOffset_FlashReadRefeVal_AppliedVal = 24;		
	
	LOOP FlashRead_LinearInterpolationReadCount 
		g_lInter_fbSpeed_RefeVal_List(index_LinearInterpolation_List) = gl_HmiVar_FlashRead(index_LinearInterpolation_FlashReadValAccess);
		g_lInter_fbSpeed_AppliedVal_List(index_LinearInterpolation_List) = gl_HmiVar_FlashRead((index_LinearInterpolation_FlashReadValAccess + indexOffset_FlashReadRefeVal_AppliedVal));
		
		index_LinearInterpolation_FlashReadValAccess = index_LinearInterpolation_FlashReadValAccess + 2;
		index_LinearInterpolation_List = index_LinearInterpolation_List + 1;
	END

	! *** Calibration Referance Data Validation 
	IF ((MAX(g_lInter_fbSpeed_RefeVal_List) = 0) & (MIN(g_lInter_fbSpeed_RefeVal_List) = 0))	
		g_Hmi_Banner_MsgLineNum = 22; g_Hmi_Banner_Enable = 1; WAIT(g_Hmi_BannerSetAndResetInterval);g_Hmi_Banner_MsgLineNum = 0; g_Hmi_Banner_Enable = 0;
				
		! *** Error register ***
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf1_E_CalibrationDataInit_FbSpeedReferanceInit_Fail;		
		g_Alarm_Register_Request_Bit = 1;
					
		! *** MMI Show Message					
		DISP "[#%d buffer] Calibration_DataInit - [Error] Speed Referance Data is Empty.", BufferProgrmaNumber;
	END ! IF ((MAX(g_lInter_fbSpeed_RefeVal_List) = 0) & (MIN(g_lInter_fbSpeed_RefeVal_List) = 0))	
	
	! *** Calibration Applied Data Validation 
	IF ((MAX(g_lInter_fbSpeed_AppliedVal_List) = 0) & (MIN(g_lInter_fbSpeed_AppliedVal_List) = 0))	
		g_Hmi_Banner_MsgLineNum = 22; g_Hmi_Banner_Enable = 1; WAIT(g_Hmi_BannerSetAndResetInterval);g_Hmi_Banner_MsgLineNum = 0; g_Hmi_Banner_Enable = 0;	
		
		! *** Error register ***
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf1_E_CalibrationDataInit_FbSpeedAppliedInit_Fail;		
		g_Alarm_Register_Request_Bit = 1;
		
		! *** MMI Show Message					
		DISP "[#%d buffer] Calibration_DataInit - [Error] Speed Applied Data is Empty.", BufferProgrmaNumber;
	END ! IF ((MAX(g_lInter_fbSpeed_AppliedVal_List) = 0) & (MIN(g_lInter_fbSpeed_AppliedVal_List) = 0))	
	
	! ----------------------------------------------------------------------------------------------

	! *** Linear Interporation Data Init - Torque Feedback 
	index_LinearInterpolation_FlashReadValAccess = 592;	! *** Tag Address - HMI Display area(100)
	FlashRead_LinearInterpolationReadCount = 11;
	index_LinearInterpolation_List = 0;
	indexOffset_FlashReadRefeVal_AppliedVal = 24;		
	
	LOOP FlashRead_LinearInterpolationReadCount 
		g_lInter_fbTorque_RefeVal_List(index_LinearInterpolation_List) = gl_HmiVar_FlashRead(index_LinearInterpolation_FlashReadValAccess)/10;
		g_lInter_fbTorque_AppliedVal_List(index_LinearInterpolation_List) = gl_HmiVar_FlashRead((index_LinearInterpolation_FlashReadValAccess + indexOffset_FlashReadRefeVal_AppliedVal))/10;
		
		index_LinearInterpolation_FlashReadValAccess = index_LinearInterpolation_FlashReadValAccess + 2;
		index_LinearInterpolation_List = index_LinearInterpolation_List + 1;
	END

	! *** Calibration Referance Data Variable 
	IF ((MAX(g_lInter_fbTorque_RefeVal_List) = 0) & (MIN(g_lInter_fbTorque_RefeVal_List) = 0))	
		g_Hmi_Banner_MsgLineNum = 23; g_Hmi_Banner_Enable = 1; WAIT(g_Hmi_BannerSetAndResetInterval);g_Hmi_Banner_MsgLineNum = 0; g_Hmi_Banner_Enable = 0;
		
		
		! *** Error register ***
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf1_E_CalibrationDataInit_FbTorqueReferanceInit_Fail;		
		g_Alarm_Register_Request_Bit = 1;
		
		! *** MMI Show Message					
		DISP "[#%d buffer] Calibration_DataInit - [Error] Torque Referance Data Empty.", BufferProgrmaNumber;
	END

	! *** Calibration Applied Data Validation 
	IF ((MAX(g_lInter_fbTorque_AppliedVal_List) = 0) & (MIN(g_lInter_fbTorque_AppliedVal_List) = 0))	
		g_Hmi_Banner_MsgLineNum = 23; g_Hmi_Banner_Enable = 1; WAIT(g_Hmi_BannerSetAndResetInterval);g_Hmi_Banner_MsgLineNum = 0; g_Hmi_Banner_Enable = 0;	
		
		! *** Error register ***
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf1_E_CalibrationDataInit_FbTorqueAppliedInit_Fail;		
		g_Alarm_Register_Request_Bit = 1;
		
		! *** MMI Show Message					
		DISP "[#%d buffer] Calibration_DataInit - [Error] Torque Applied Data is Empty.", BufferProgrmaNumber;
	END ! IF ((MAX(g_lInter_fbSpeed_AppliedVal_List) = 0) & (MIN(g_lInter_fbSpeed_AppliedVal_List) = 0))	
	
	! ----------------------------------------------------------------------------------------------

	! *** Linear Interporation Data Init - Current Feedback 
	index_LinearInterpolation_FlashReadValAccess = 732;	! *** Tag Address - HMI Display area(100)
	FlashRead_LinearInterpolationReadCount = 11;
	index_LinearInterpolation_List = 0;
	indexOffset_FlashReadRefeVal_AppliedVal = 24;		
	
	LOOP FlashRead_LinearInterpolationReadCount 
		g_lInter_fbDynoCurrent_RefeVal_List(index_LinearInterpolation_List) = gl_HmiVar_FlashRead(index_LinearInterpolation_FlashReadValAccess);
		g_lInter_fbDynoCurrent_AppliedVal_List(index_LinearInterpolation_List) = gl_HmiVar_FlashRead((index_LinearInterpolation_FlashReadValAccess + indexOffset_FlashReadRefeVal_AppliedVal));
		
		index_LinearInterpolation_FlashReadValAccess = index_LinearInterpolation_FlashReadValAccess + 2;
		index_LinearInterpolation_List = index_LinearInterpolation_List + 1;
	END

	! *** Calibration Referance Data Variable 
	IF ((MAX(g_lInter_fbDynoCurrent_RefeVal_List) = 0) & (MIN(g_lInter_fbDynoCurrent_RefeVal_List) = 0))	
		g_Hmi_Banner_MsgLineNum = 24; g_Hmi_Banner_Enable = 1; WAIT(g_Hmi_BannerSetAndResetInterval);g_Hmi_Banner_MsgLineNum = 0; g_Hmi_Banner_Enable = 0;
		
		! *** Error register ***
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf1_E_CalibrationDataInit_FbCurrentReferanceInit_Fail;		
		g_Alarm_Register_Request_Bit = 1;
		
		! *** MMI Show Message					
		DISP "[#%d buffer] Calibration_DataInit - [Error] Current Referance Data Empty.", BufferProgrmaNumber;
	END

	! *** Calibration Applied Data Validation 
	IF ((MAX(g_lInter_fbDynoCurrent_AppliedVal_List) = 0) & (MIN(g_lInter_fbDynoCurrent_AppliedVal_List) = 0))	
		g_Hmi_Banner_MsgLineNum = 24; g_Hmi_Banner_Enable = 1; WAIT(g_Hmi_BannerSetAndResetInterval);g_Hmi_Banner_MsgLineNum = 0; g_Hmi_Banner_Enable = 0;
		
		! *** Error register ***
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf1_E_CalibrationDataInit_FbCurrentAppliedInit_Fail;		
		g_Alarm_Register_Request_Bit = 1;
		
		! *** MMI Show Message					
		DISP "[#%d buffer] Calibration_DataInit - [Error] Current Applied Data is Empty.", BufferProgrmaNumber;
	END ! IF ((MAX(g_lInter_fbSpeed_AppliedVal_List) = 0) & (MIN(g_lInter_fbSpeed_AppliedVal_List) = 0))	
	
	
	! ----------------------------------------------------------------------------------------------

	! *** Linear Interporation Data Init - Current Output 
	index_LinearInterpolation_FlashReadValAccess = 092;	! *** Tag Address - HMI Display area(100)
	FlashRead_LinearInterpolationReadCount = 11;
	index_LinearInterpolation_List = 0;
	indexOffset_FlashReadRefeVal_AppliedVal = 24;		
	
	LOOP FlashRead_LinearInterpolationReadCount 
		g_lInter_opDynoCurrent_RefeVal_List(index_LinearInterpolation_List) = gl_HmiVar_FlashRead(index_LinearInterpolation_FlashReadValAccess);
		g_lInter_opDynoCurrent_AppliedVal_List(index_LinearInterpolation_List) = gl_HmiVar_FlashRead((index_LinearInterpolation_FlashReadValAccess + indexOffset_FlashReadRefeVal_AppliedVal));
		
		index_LinearInterpolation_FlashReadValAccess = index_LinearInterpolation_FlashReadValAccess + 2;
		index_LinearInterpolation_List = index_LinearInterpolation_List + 1;
	END

	! *** Calibration Referance Data Variable 
	IF ((MAX(g_lInter_opDynoCurrent_RefeVal_List) = 0) & (MIN(g_lInter_opDynoCurrent_RefeVal_List) = 0))	
		g_Hmi_Banner_MsgLineNum = 26; g_Hmi_Banner_Enable = 1; WAIT(g_Hmi_BannerSetAndResetInterval);g_Hmi_Banner_MsgLineNum = 0; g_Hmi_Banner_Enable = 0;
		
		! *** Error register ***
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf1_E_CalibrationDataInit_OpCurrentReferanceInit_Fail;		
		g_Alarm_Register_Request_Bit = 1;
		
		! *** MMI Show Message					
		DISP "[#%d buffer] Calibration_DataInit - [Error] Current Referance Data Empty.", BufferProgrmaNumber;
	END

	! *** Calibration Applied Data Validation 
	IF ((MAX(g_lInter_opDynoCurrent_AppliedVal_List) = 0) & (MIN(g_lInter_opDynoCurrent_AppliedVal_List) = 0))	
		g_Hmi_Banner_MsgLineNum = 26; g_Hmi_Banner_Enable = 1; WAIT(g_Hmi_BannerSetAndResetInterval);g_Hmi_Banner_MsgLineNum = 0; g_Hmi_Banner_Enable = 0;
		
		! *** Error register ***
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf1_E_CalibrationDataInit_OpCurrentAppliedInit_Fail;		
		g_Alarm_Register_Request_Bit = 1;
		
		! *** MMI Show Message					
		DISP "[#%d buffer] Calibration_DataInit - [Error] Current Applied Data is Empty.", BufferProgrmaNumber;
	END ! IF ((MAX(g_lInter_fbSpeed_AppliedVal_List) = 0) & (MIN(g_lInter_fbSpeed_AppliedVal_List) = 0))	
	
	
RET

! ----------------------------------------------------------------------------------------------

sr_unit_DynoEngineCmd_DA_ConstantRatioInit:
	
	! *** Dyno. Command D/A Conversion Ration ***	
	g_unit_Cmd_100PctToDynoCurrent_DARation = 32767 / 100 / 2;
	
	! *** Engine Command D/A Conversion Ration ***		
	g_unit_Cmd_Alpha_1_100Pct_DaVal = 3276.7 * g_Hmi_bs5402_Eng_Alpha_1_MaxVoltSt;
	g_unit_Cmd_Alpha_1_0Pct_DaVal = 3276.7 * g_Hmi_bs5402_Eng_Alpha_1_MinVoltSt;
	g_unit_Cmd_Alpha_1_0Pct_ZeroOffset_DaVal = 3276.7 * g_Hmi_bs5402_Eng_Alpha_1_OffsetVoltSt;
	g_unit_Cmd_100PctToAlpha_1_DARation = (g_unit_Cmd_Alpha_1_100Pct_DaVal - g_unit_Cmd_Alpha_1_0Pct_DaVal) / 100;	
	
	g_unit_Cmd_Alpha_2_100Pct_DaVal = 3276.7 * g_Hmi_bs5403_Eng_Alpha_2_MaxVoltSt;
	g_unit_Cmd_Alpha_2_0Pct_DaVal = 3276.7 * g_Hmi_bs5403_Eng_Alpha_2_MinVoltSt;
	g_unit_Cmd_Alpha_2_0Pct_ZeroOffset_DaVal = 3276.7 * g_Hmi_bs5403_Eng_Alpha_2_OffsetVoltSt;
	g_unit_Cmd_100PctToAlpha_2_DARation = (g_unit_Cmd_Alpha_2_100Pct_DaVal - g_unit_Cmd_Alpha_2_0Pct_DaVal) / 100;	
	
	g_unit_Cmd_Alpha_DO_IdleVali_DaVal = 3276.7 * g_Hmi_bs5404_Op_Alpha_IdleValiDo_VoltSt;
	
RET

! ----------------------------------------------------------------------------------------------

sr_PcWrite_DataInit:
	
	
	! Automation PC Write List Init.
	FILL (0, g_PC_Write_Data);

	!PC Write List Index Init.
	g_index_PcWrite_PcStatus = 0x00;
	g_index_PcWrite_ControlCMD = 0x30;	
	g_index_PcWrite_RunDriveMode = 0x60;
	g_index_PcWrite_ProfileWrite = 0x90;
	g_index_PcWrite_eCatOutputData = 0x200;
	
	
	!Profile List Initialize Writing & Processing  
	FILL (0, g_PC_Write_Data_TqAlphaProfile);
	FILL (0, g_PC_Write_Data_SpAlphaProfile);
	FILL (0, g_PC_Write_Data_TqSpProfile);
	FILL (0, g_PC_Write_Data_SpTqProfile);
	FILL (0, g_PC_Process_Data_TqAlphaProfile);
	FILL (0, g_PC_Process_Data_SpAlphaProfile);
	FILL (0, g_PC_Process_Data_TqSpProfile);
	FILL (0, g_PC_Process_Data_SpTqProfile);
	
RET

! ----------------------------------------------------------------------------------------------

sr_SafetyControllerReset:

	! *** PNOZ X11P Reset On
	g_eCAT_DO_Data(0)(0x01).05 = 1;
	
	WAIT 1000;
	
	! *** PNOZ X11P Reset Off
	g_eCAT_DO_Data(0)(0x01).05 = 0;
	
RET

! ----------------------------------------------------------------------------------------------
sr_Init_GlobalVariables: 

	! *** OP LED Reset
	g_eCAT_HMI_DO_Data(0)(0x00) = 0;
	g_eCAT_HMI_DO_Data(0)(0x01) = 0;
	g_eCAT_HMI_DO_Data(0)(0x02) = 0;
	g_eCAT_HMI_DO_Data(0)(0x03) = 0;

	g_Hmi_BannerSetAndResetInterval = 1500;
	
	! Prevent Engine Reverse Operating RPM
	g_Hmi_bs3008_Eng_StopRPMSt = -100;
	g_Hmi_bs3008_PID_Change_Ramp = 10;

RET
! ----------------------------------------------------------------------------------------------

sr_AutomationSW_EtherCATOutput_Update_Valiable_Declaration:

	INT eCatPcOut_Index_PcWriteDataList;
	INT eCatPcOut_Index_PcDataToEtherCatUpdate;	
	INT eCatPcOut_EtherCatOutputAddress;
RET

! ----------------------------------------------------------------------------------------------	

sr_AutomationSW_EtherCATOutput_Update:
	BLOCK
!	eCatPcOut_Index_PcWriteDataList = 0x200;
!	eCatPcOut_Index_PcDataToEtherCatUpdate = 0;
!	eCatPcOut_EtherCatOutputAddress = 0;
	
!	LOOP g_eCAT_Output_DataNumber
		
!		eCatPcOut_EtherCatOutputAddress = g_eCAT_Output_Data_ExistIndex(0)(eCatPcOut_Index_PcDataToEtherCatUpdate);
		
		IF(eCatPcOut_EtherCatOutputAddress > -1)
		
			IF((eCatPcOut_EtherCatOutputAddress > 0xa0) & (eCatPcOut_EtherCatOutputAddress < 0x100))
			
				IF(eCatPcOut_EtherCatOutputAddress <> 0xd5)
				
					IF(eCatPcOut_EtherCatOutputAddress <> 0xd6)
					
!							g_eCAT_Output_Data(0)(eCatPcOut_EtherCatOutputAddress) = g_PC_Write_Data(eCatPcOut_Index_PcWriteDataList)
							
						END!IF(eCatPcOut_EtherCatOutputAddress <> 0xd5)
						
					END!IF(eCatPcOut_EtherCatOutputAddress <> 0xd6)
					
			END !IF((g_eCAT_Output_Address(eCatPcOut_Index_PcDataToEtherCatUpdate) > 0x100) & (g_eCAT_Output_Address(eCatPcOut_Index_PcDataToEtherCatUpdate) < 0x120)) 	
			
		END ! IF(eCatPcOut_EtherCatOutputAddress > -1)
		
!		eCatPcOut_Index_PcDataToEtherCatUpdate = eCatPcOut_Index_PcDataToEtherCatUpdate + 1;
!		eCatPcOut_Index_PcWriteDataList = eCatPcOut_Index_PcWriteDataList + 1;
		
!	END ! LOOP g_eCAT_Output_DataNumber
	END
RET

! ----------------------------------------------------------------------------------------------
sr_IO_Data_Update:
	BLOCK
				!IF (i=10)
				!	TimeMeasure_2 = TIME;
				!	DISP TimeMeasure_2-TimeMeasure_1
				!	TimeMeasure_1 = TimeMeasure_2;
				!	i=0;
				!END
				!i = i + 1;
		!g_HMI_DO_MiscOn_Lamp = g_SystemState_Misc_On;

		!sr_BS610x_HMI_MonitoringScreen_Update:
		! *** Monitoring Screen Update
		g_Hmi_bs6101_Mon_EngOilPressure = g_Hmi_bs6001_InIndex_EngOilSenMaxPressureSt * (g_eCAT_AI_Data(0)(g_Hmi_bs6001_InIndex_EngOilPressureSt) / 32767);
		g_Hmi_bs6101_Mon_EngOilTemp = (g_eCAT_AI_Data(0)(g_Hmi_bs6001_InIndex_EngOilTempSt) / 10);
		g_Hmi_bs6101_Mon_DisturDyno = (g_eCAT_AI_Data(0)(g_Hmi_bs6001_InIndex_DisturDynoSt) / 10);
		g_Hmi_bs6101_Mon_cWaterPressure = g_Hmi_bs6001_InIndex_cWaterSenMaxPressureSt * (g_eCAT_AI_Data(0)(g_Hmi_bs6001_InIndex_cWaterPressureSt) / 32767);
		g_Hmi_bs6101_Mon_cWaterTemp = (g_eCAT_AI_Data(0)(g_Hmi_bs6001_InIndex_cWaterTempSt) / 10);
		g_Hmi_bs6101_Mon_ExhaustTemp = (g_eCAT_AI_Data(0)(g_Hmi_bs6001_InIndex_ExhaustTempSt) / 10);
		!RET
		
		!sr_BS5xxx_HMI_Calibration_Update:
		! *** Feddback A/D Value Update														
		DSHIFT(Hmi_bs500x_FB_SpeedADValList, g_eCAT_AI_Data(0)(g_Hmi_bs5001_FB_SpeedYIndexSt), index_Hmi_bs500x_FB_SpeedADValMaxIndex); 	
		g_Hmi_bs500x_FB_SpeedADValMon = AVG(Hmi_bs500x_FB_SpeedADValList);
					
		!DSHIFT(Hmi_bs500x_FB_TorqueADValList, g_eCAT_AI_Data(0)(g_Hmi_bs5101_FB_TorqueYIndexSt), index_Hmi_bs500x_FB_TorqueADValMaxIndex); 	
		!g_Hmi_bs510x_FB_TorqueADValMon = AVG(Hmi_bs500x_FB_TorqueADValList);
		
		!DSHIFT(Hmi_bs500x_FB_CurrentADValList, g_eCAT_AI_Data(0)(g_Hmi_bs5301_FB_CurrentIndexSt), index_Hmi_bs500x_FB_CurrentADValMaxIndex); 	
		!g_Hmi_bs530x_FB_CurrentADValMon = AVG(Hmi_bs500x_FB_CurrentADValList);
		!RET

		! *** Input Cmd  Refresh ; HMI DI-DO for Army
		! Sector 1
		g_HMI_DI_PanelActive_Button = g_eCAT_HMI_DI_Data(0)(0x00).(0); !Panel Enable
		g_HMI_DI_Reset_Button = g_eCAT_HMI_DI_Data(0)(0x00).(1); !g_eCAT_Input_Data(0)(0x10).(5) 
		g_HMI_DI_ConfigMenu_Button = g_eCAT_HMI_DI_Data(0)(0x00).(2); !g_eCAT_Input_Data(0)(0x10).(6);
		g_HMI_DI_AlarmHistory_Button = g_eCAT_HMI_DI_Data(0)(0x00).(3); !g_eCAT_Input_Data(0)(0x10).(7);
		g_HMI_DI_DynoService_Button = g_eCAT_HMI_DI_Data(0)(0x00).(4); !g_eCAT_Input_Data(0)(0x00).0
		g_HMI_DI_HornOff_Button = g_eCAT_HMI_DI_Data(0)(0x00).(5); !g_eCAT_Input_Data(0)(0x11).1

		! Sector 2
		g_HMI_DI_Monitor_Button = g_eCAT_HMI_DI_Data(0)(0x01).(0); !g_eCAT_Input_Data(0)(0x00).1
		g_HMI_DI_EngIGOn_Button = g_eCAT_HMI_DI_Data(0)(0x01).(1); !g_eCAT_Input_Data(0)(0x01).(1)
		g_HMI_DI_EngPreheatOn_Button = g_eCAT_HMI_DI_Data(0)(0x01).(2); !g_eCAT_Input_Data(0)(0x01).(0)
		g_HMI_DI_EngStart_Button = g_eCAT_HMI_DI_Data(0)(0x01).(3); !g_eCAT_Input_Data(0)(0x01).(3)
		
		g_HMI_DI_Idle_Button = g_eCAT_HMI_DI_Data(0)(0x01).(5); !g_eCAT_Input_Data(0)(0x11).3;
		g_HMI_DI_Stop_Button = g_eCAT_HMI_DI_Data(0)(0x01).(7); !g_eCAT_Input_Data(0)(0x01).(2)
		!g_HMI_DI_IdleContrl_Button = g_eCAT_HMI_DI_Data(0)(0x03).(2); !Hydraulic Dynamo?????? ????????.;

		! Sector 3
		g_HMI_DI_Misc_Button = g_eCAT_HMI_DI_Data(0)(0x02).(0); !g_eCAT_Input_Data(0)(0x00).(2)
		g_HMI_DI_DynoOn_Button = g_eCAT_HMI_DI_Data(0)(0x02).(1); !g_eCAT_Input_Data(0)(0x10).(4)
		g_HMI_DI_Manual_Button = g_eCAT_HMI_DI_Data(0)(0x02).(3); !g_eCAT_Input_Data(0)(0x00).3
		g_HMI_DI_Remote_Button = g_eCAT_HMI_DI_Data(0)(0x02).(5); !g_eCAT_Input_Data(0)(0x00).7 
		
		! Sector 4
		g_HMI_DI_DyRPM_EngTA_Button = g_eCAT_HMI_DI_Data(0)(0x03).(0); !g_eCAT_Input_Data(0)(0x11).4;
		g_HMI_DI_DyTo_EngTA_Button = g_eCAT_HMI_DI_Data(0)(0x03).(1); !g_eCAT_Input_Data(0)(0x11).5;
		g_HMI_DI_DyRPM_EngTo_Button = g_eCAT_HMI_DI_Data(0)(0x03).(2); !g_eCAT_Input_Data(0)(0x11).6;
		g_HMI_DI_DyTo_EngRPM_Button = g_eCAT_HMI_DI_Data(0)(0x03).(3); !g_eCAT_Input_Data(0)(0x11).7;
		
		!g_HMI_DI_Automatic_Button = g_eCAT_HMI_DI_Data(0)(0x02).(6); ! g_eCAT_Input_Data(0)(0x00).5
		!g_HMI_DI_Seant_Button = g_eCAT_HMI_DI_Data(0)(0x01).(4); !g_eCAT_Input_Data(0)(0x00).7 
		!g_HMI_DI_BKReady_Button = g_eCAT_HMI_DI_Data(0)(0x02).(0); !g_eCAT_Input_Data(0)(0x10).(0)
		
		!External
		g_DI_EMSButton = g_eCAT_DI_Data(0)(0x01).(7);
		
		! *** Lamp Refresh ; HMI DI-DO for Army
		! Sector 1
		g_eCAT_HMI_DO_Data(0)(0x00).(0) = g_HMI_DO_PanelActive_Lamp; !g_eCAT_Output_Data(0)(0x20).4
		g_eCAT_HMI_DO_Data(0)(0x00).(1) = g_HMI_DO_AlarmReset_Lamp; !g_eCAT_Output_Data(0)(0x30).5  
		g_eCAT_HMI_DO_Data(0)(0x00).(2) = g_HMI_DO_ConfigMenu_Lamp; !g_eCAT_Output_Data(0)(0x30).5
		g_eCAT_HMI_DO_Data(0)(0x00).(3) = g_HMI_DO_AlarmHistory_Lamp; !g_eCAT_Output_Data(0)(0x30).5
		g_eCAT_HMI_DO_Data(0)(0x00).(4) = g_HMI_DO_DynoService_Lamp; !g_eCAT_Output_Data(0)(0x20).4
		g_eCAT_HMI_DO_Data(0)(0x00).(5) = g_HMI_DO_HornOff_Lamp; !g_eCAT_Output_Data(0)(0x31).1

		! Sector 2
		g_eCAT_HMI_DO_Data(0)(0x01).(0) = g_HMI_DO_Monitor_Lamp; !g_eCAT_Output_Data(0)(0x20).4
		g_eCAT_HMI_DO_Data(0)(0x01).(1) = g_HMI_DO_EngIGOn_Lamp; !g_eCAT_Output_Data(0)(0x21).1
		g_eCAT_HMI_DO_Data(0)(0x01).(2) = g_HMI_DO_EngPreheatOn_Lamp; !g_eCAT_Output_Data(0)(0x21).0
		g_eCAT_HMI_DO_Data(0)(0x01).(3) = g_HMI_DO_EngStarting_Lamp; !g_eCAT_Output_Data(0)(0x21).3

		g_eCAT_HMI_DO_Data(0)(0x01).(5) = g_HMI_DO_Idle_Lamp; !g_eCAT_Output_Data(0)(0x30).5
		g_eCAT_HMI_DO_Data(0)(0x01).(7) = g_HMI_DO_Stop_Lamp; !g_eCAT_Output_Data(0)(0x21).2
		!g_eCAT_HMI_DO_Data(0)(0x03).(2) = g_HMI_DO_IdleControl_Lamp; !g_eCAT_Output_Data(0)(0x30).5
		
		g_eCAT_HMI_DO_Data(0)(0x02).(0) = g_HMI_DO_MiscOn_Lamp; !g_eCAT_Output_Data(0)(0x20).4
		g_eCAT_HMI_DO_Data(0)(0x02).(1) = g_HMI_DO_DynoOn_Lamp; !g_eCAT_Output_Data(0)(0x30).4
		g_eCAT_HMI_DO_Data(0)(0x02).(3) = g_HMI_DO_Manual_Lamp; !g_eCAT_Output_Data(0)(0x20).4
		g_eCAT_HMI_DO_Data(0)(0x02).(5) = g_HMI_DO_Remote_Lamp; !g_eCAT_Output_Data(0)(0x20).4

		!g_eCAT_HMI_DO_Data(0)(0x02).(6) = g_HMI_DO_Automatic_Lamp; !g_eCAT_Output_Data(0)(0x20).4
		!g_eCAT_HMI_DO_Data(0)(0x01).(4) = g_HMI_DO_Seant_Lamp; !g_eCAT_Output_Data(0)(0x20).4

		g_eCAT_HMI_DO_Data(0)(0x03).(0) = g_HMI_DO_DyRPM_EngTA_Lamp; !g_eCAT_Output_Data(0)(0x30).5
		g_eCAT_HMI_DO_Data(0)(0x03).(1) = g_HMI_DO_DyTo_EngTA_Lamp; !g_eCAT_Output_Data(0)(0x30).5
		g_eCAT_HMI_DO_Data(0)(0x03).(2) = g_HMI_DO_DyRPM_EngTo_Lamp; !g_eCAT_Output_Data(0)(0x30).5
		g_eCAT_HMI_DO_Data(0)(0x03).(3) = g_HMI_DO_DyTo_EngRPM_Lamp; !g_eCAT_Output_Data(0)(0x30).5
		
		g_HMI_AI_Dyno_CalRefCnt = g_eCAT_HMI_AI_Data(0)(0x00); !g_eCAT_Input_Data(0)(0x40);
		g_HMI_AI_Eng_CalRefCnt = g_eCAT_HMI_AI_Data(0)(0x01); !g_eCAT_Input_Data(0)(0x41);
	END
	BLOCK

		g_AI_Target_Dyno_Remote = g_eCAT_AI_Data(0)(0x00)/32767;							
		g_AI_Target_Engine_Remote = g_eCAT_AI_Data(0)(0x01)/32767;							

		g_AI_Feedback_DynoSpeed = sign(g_eCAT_AI_Data(0)(0x00))* g_eCAT_AI_Data(0)(0x04)/100 * 60 / 1024; !g_Hmi_bs5001_FB_SpeedYIndexSt	
		
		! Prevent Reverse Operating of engine
		IF (g_AI_Feedback_DynoSpeed < g_Hmi_bs3008_Eng_StopRPMSt)
			g_SystemState_Dyno_On = 0;
			g_SystemState_Eng_IG_On = 0;
			g_DO_DynoOnSignal = 0;
			g_DO_IgnitionSignal = 0;     
		END
		IF (g_AI_Feedback_DynoSpeed > g_Hmi_bs3004_Eng_MaxSpeedSt + 50)
			g_SystemState_Dyno_On = 0;
			g_SystemState_Eng_IG_On = 0;
			g_DO_DynoOnSignal = 0;
			g_DO_IgnitionSignal = 0;     
		END
		
		!g_AI_Feedback_Vdiff = (g_eCAT_AI_Data(0)(0x05) / 2147483647) *20;
		!g_AI_Feedback_Vref = (g_eCAT_AI_Data(0)(0x06) / 2147483647) * 12;
		
		!(int)((Vdiff / Vref * 967 / 2 - 58.4) * 100);
		!g_AI_Feedback_Torque = (g_AI_Feedback_Vdiff / g_AI_Feedback_Vref * 967 / 2 - 58.4);
		g_AI_Feedback_Torque = (g_eCAT_AI_Data(0)(0x05)/100 - 60000) * 2000 / 30000 ; ! 2000N - 90000, 0N - 60000
		DSHIFT(Average_TorqueList, g_AI_Feedback_Torque, index_Average_TorqueListLastIndex);  
		DSHIFT(Average_FeedbackTorqueList, g_AI_Feedback_Torque, index_Average_FeedbackTorqueListLastIndex);  
		g_AI_Feedback_TorqueAvg = AVG(Average_FeedbackTorqueList);
		g_Monitoring_TorqueAvg = AVG(Average_TorqueList);

		AI_Feedback_Alpha = g_Hmi_Target_Engine_AlphaPosi_Range * g_eCAT_AI_Data(0)(g_Hmi_bs5201_FB_AlphaYIndexSt)/32767;		!0x02					
		DSHIFT(AI_Feedback_Alpha_List, AI_Feedback_Alpha, index_Hmi_bs500x_FB_ALPHAADValMaxIndex); 
		g_AI_Feedback_Alpha = AVG(AI_Feedback_Alpha_List);
		
		!g_AO_Feedback_Alpha = (g_AI_Feedback_Alpha/g_Hmi_Target_Engine_AlphaPosi_Range)*32767; !g_AI_Feedback_Alpha;

		g_eCAT_AO_Data(0)(g_Hmi_bs5401_Op_Alpha_1_CIndexSt) = g_AO_AlphaASignal; ! 0x92, 0x6 Electric Throttle A
		g_eCAT_AO_Data(0)(g_Hmi_bs5401_Op_Alpha_2_CIndexSt) = g_AO_AlphaBSignal; ! 0x93, 0x7 Electric Throttle B
		g_eCAT_DO_Data(0)(g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt).(g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt) = g_DO_ThrottleIVSSignal; ! ThrottleIVS g_eCAT_DO_Data(0)(0x01).(1) g_eCAT_Input_Data(0)(0x61).(1)
		g_eCAT_AO_Data(0)(g_Hmi_bs5501_Dyno_CIndexSt) = g_AO_DynoControlSignal; ! 0x82, 0x2 Dyno COntrol

		g_eCAT_AO_Data(0)(4) = g_AO_DynoSpeedActualSignal; ! 0x80, 0x0 Dyno Speed Actual
		g_eCAT_AO_Data(0)(5) = g_AO_DynoTorqueActualSignal; ! 0x81, 0x1 Dyno Torque Actual
		g_eCAT_AO_Data(0)(6) = g_AO_Feedback_Alpha; ! 0x90, 0x4 Engine Torque Actual
		g_eCAT_AO_Data(0)(1) = g_AO_ThrottleControlSignal; ! 0x81, 0x1 Dyno Torque Actual

		g_DI_DynoTroubleSignal = g_eCAT_DI_Data(0)(0x00).(1); !g_eCAT_Input_Data(0)(0x50).1
		g_DI_RemoteOnSignal = g_eCAT_DI_Data(0)(0x00).(2); !g_eCAT_Input_Data(0)(0x50).2

		g_DI_DynoSpeedModeSignal = g_eCAT_DI_Data(0)(0x00).(3); !DynoSpeedMode = g_eCAT_Input_Data(0)(0x50).3;
		g_DI_DynoTorqueModeSignal = g_eCAT_DI_Data(0)(0x00).(4); !DynoTorqueMode = g_eCAT_Input_Data(0)(0x50).4;
		g_DI_EngineAlphaModeSignal = g_eCAT_DI_Data(0)(0x00).(5); !EngineAlphaMode = g_eCAT_Input_Data(0)(0x50).5;
		g_DI_EngineTorqueModeSignal = g_eCAT_DI_Data(0)(0x00).(6); !EngineTorqueMode = g_eCAT_Input_Data(0)(0x50).6;
		g_DI_DynoOn_FeedBack = g_eCAT_DI_Data(0)(0x00).(7); !EngineTorqueMode = g_eCAT_Input_Data(0)(0x50).6;

		!g_DI_EMSButton = g_eCAT_DI_Data(0)(0x01).(3); ! Safety Controller g_eCAT_Input_Data(0)(0x51).(3)
		g_TEMP_WARNING = g_eCAT_DI_Data(0)(0x01).(0);
		g_STO_FEEDBACK = g_eCAT_DI_Data(0)(0x01).(1);
		g_DI_SafetyGuardSignal = g_eCAT_DI_Data(0)(0x01).(4); ! Safety Controller g_eCAT_Input_Data(0)(0x51).(3)
		g_DI_SafetyRelaySignal = g_eCAT_DI_Data(0)(0x01).(5); ! Safety Controller g_eCAT_Input_Data(0)(0x51).(3)

		g_eCAT_DO_Data(0)(0x00).(0) = g_DO_BKReadySignal; ! BKReady g_eCAT_Input_Data(0)(0x60).(0)
		g_eCAT_DO_Data(0)(0x00).(1) = g_DO_FANSignal; ! BKReady g_eCAT_Input_Data(0)(0x60).(0)
		g_eCAT_DO_Data(0)(0x00).(2) = g_DO_DynoOnSignal; ! DynoOn g_eCAT_Input_Data(0)(0x60).(2)
		g_eCAT_DO_Data(0)(0x00).(3) = g_DO_ThermalShockValve1; ! DynoOn g_eCAT_Input_Data(0)(0x60).(2)
		g_eCAT_DO_Data(0)(0x00).(4) = g_DO_ThermalShockValve2; ! DynoOn g_eCAT_Input_Data(0)(0x60).(2)
		g_eCAT_DO_Data(0)(0x00).(5) = g_DO_ThermalShockHeater; ! DynoOn g_eCAT_Input_Data(0)(0x60).(2)
		g_eCAT_DO_Data(0)(0x00).(6) = g_DO_DynoResetSignal; ! DynoReset g_eCAT_Input_Data(0)(0x60).(6)

		!g_eCAT_DO_Data(0)(0x01).(0) = g_TEMP_WARNING; ! DynoReset g_eCAT_Input_Data(0)(0x60).(6)
		g_eCAT_DO_Data(0)(0x01).(1) = g_DO_ThrottleIVSSignal; ! DynoReset g_eCAT_Input_Data(0)(0x60).(6)
		g_eCAT_DO_Data(0)(0x01).(2) = g_DO_PreHeatSignal; ! PreHeat g_eCAT_Input_Data(0)(0x61).(2)
		g_eCAT_DO_Data(0)(0x01).(3) = g_DO_StartSignal; ! Start g_eCAT_Input_Data(0)(0x61).(3)
		g_eCAT_DO_Data(0)(0x01).(4) = g_DO_IgnitionSignal; ! Ignition g_eCAT_Input_Data(0)(0x61).(4)
		g_eCAT_DO_Data(0)(0x01).(5) = g_DO_Seant_On; ! *** PNOZ X11P Reset On g_eCAT_Input_Data(0)(0x61).(5)
		!IF (g_AutoMd_ProfilePlay_Enable = 1 & g_ControlMode = g_CtMo_enum_Automatic)
		!	g_eCAT_DO_Data(0)(0x01).(5) = 1; ! FAKE SIGNA: FOR BK DAS
		!ELSE
		!	g_eCAT_DO_Data(0)(0x01).(5) = g_SystemState_Eng_IG_On; ! *** PNOZ X11P Reset On g_eCAT_Input_Data(0)(0x61).(5)
		!END
		
		g_eCAT_DO_Data(0)(0x01).(6) = g_DO_HornSignal; ! Horn g_eCAT_Input_Data(0)(0x61).(6)
		g_eCAT_DO_Data(0)(0x01).(7) = g_DO_Alarm_Lamp; ! Horn g_eCAT_Input_Data(0)(0x61).(6)
	END
RET
!----------------------------------------------

#2
! *** Alarm Control Program
	
INT BufferProgrmaNumber
	BufferProgrmaNumber = 2
		
! MMI Show Message
DISP "[#%d buffer] program started.", BufferProgrmaNumber;
		
	!GLOBAL INT g_Alarm_Exists_Bit -> D-Buffer
	!GLOBAL INT g_Error_Exists_Bit -> D-Buffer
	!GLOBAL INT g_Warning_Exists_Bit -> D-Buffer
	g_Alarm_Exists_Bit = 0;
	g_Error_Exists_Bit = 0;
	g_Warning_Exists_Bit = 0;
	
	INT list_Alarm_History(1000);
	INT alarm_History_ListSize;
		alarm_History_ListSize = 999;
	!GLOBAL INT alram_Register_Request_AlarmID;-> D-Buffer
	!GLOBAL INT ararm_Register_Request_Bit; ->D-Buffer
	INT alarm_RegisterCmp_Index;
	INT alarm_RegisterRequest_Index;
	
	! *** Dynamometer Equipment Control variable
	!GLOBAL INT g_dynoCtrl_OilPumpOnRPM;
	g_dynoCtrl_OilPumpOnRPM = 100;
	
	! Alarm ID Define (Warning Alarm ID = (Buffer No * 10000) + Warning No, Error Alarm ID = Warning Alarm ID * -1)	
	INT enum_aID_Buf2_E_Dyno_AMP_Fault;	
		enum_aID_Buf2_E_Dyno_AMP_Fault = -20001;
	INT	alarm_Filter_E_Dyno_AMP_Fault;
		alarm_Filter_E_Dyno_AMP_Fault = 0;
		
	INT enum_aID_Buf2_E_Dyno_OilPump_Level_Fault;	
		enum_aID_Buf2_E_Dyno_OilPump_Level_Fault = -20002;
	INT	alarm_Filter_E_Dyno_OilPump_Level_Fault;
		alarm_Filter_E_Dyno_OilPump_Level_Fault = 0;	
		
	INT enum_aID_Buf2_E_Dyno_Water_Temp_Fault;	
		enum_aID_Buf2_E_Dyno_Water_Temp_Fault = -20003;
	INT	alarm_Filter_E_Dyno_Water_Temp_Fault;
		alarm_Filter_E_Dyno_Water_Temp_Fault = 0;
		
	INT enum_aID_Buf2_E_Dyno_CasingRing_Fault;	
		enum_aID_Buf2_E_Dyno_CasingRing_Fault = -20004;
	INT	alarm_Filter_E_Dyno_CasingRing_Fault;
		alarm_Filter_E_Dyno_CasingRing_Fault = 0;		
	
	
	CALL sr_AlarmServer_initialization;
		
	! *** Program Resule Update
	g_bufferProgramResult(0)(BufferProgrmaNumber) = 100;
	
	WAIT(100)
	
		WHILE 1		
			
			!CALL sr_Alarm_Check;
			
			!CALL sr_AcsController_StateReport;
			
			!CALL sr_DynoOilPump_Process;
			
			WAIT(1);
			
		END !WHILE 1

! *** Program Resule Update
g_bufferProgramResult(0)(BufferProgrmaNumber) = 99999;
	
! *** MMI Terminal display
DISP "[#%d buffer] program end.", BufferProgrmaNumber;

STOP

! ----------------------------------------------------------------------------------------------

! *** ACS Error On Post processing
ON g_Error_Exists_Bit	
	! *** HORN On
	g_DO_Alarm_Lamp = 1;
	if (g_HMI_DO_HornOff_Lamp = 0)
		! *** ALARM LAMP On
		g_DO_HornSignal = 1;
	end
RET

! *** Error Off Post processing
ON ^g_Error_Exists_Bit	
	IF(g_PC_Write_Data(0x10) = 0)
		! *** HORN Off
		g_DO_HornSignal = 0;
		! *** ALARM LAMP Off
		g_DO_Alarm_Lamp = 0;
	END ! IF(g_PC_Write_Data(0x10) = 0)
RET

! ----------------------------------------------------------------------------------------------

! *** ATOM Error On Post processing
ON g_PC_Write_Data(0x10)	
	! *** HORN On
	g_DO_HornSignal = 1;
	! *** ALARM LAMP On
	g_DO_Alarm_Lamp = 1;
	
RET

! *** ATOM Error Off Post processing
ON ^g_PC_Write_Data(0x10)	
	IF (g_Error_Exists_Bit = 0)
		! *** HORN Off
		g_DO_HornSignal = 0;
		! *** ALARM LAMP Off
		g_DO_Alarm_Lamp = 0;
	END ! IF (g_Error_Exists_Bit = 0)
RET

! ----------------------------------------------------------------------------------------------

! *** Alarm Register Autoroutine
ON g_Alarm_Register_Request_Bit

	CALL sr_AlarmServer_AlarmRegister;
	g_Alarm_Register_Request_Bit = 0;
	
RET

! ----------------------------------------------------------------------------------------------

ON ^PST(2).#RUN
	
	IF(^PST(0).#RUN)
		
		STOPALL;
		
		!FILL(0, g_eCAT_Output_Data);
		
		DISP "[#%d buffer] Alarm Server Stop! All Program Stop Autoroutine.", BufferProgrmaNumber
	END
RET

ON g_SystemState_Reset
	
	CALL sr_AlarmServer_initialization;
	
	DISP "[#%d buffer] Autoroutine; Clear Alarm History.", BufferProgrmaNumber
RET
! *************** Subroutine  *************** !

sr_AlarmServer_initialization:
	
	! *** Variable initialization	 
	g_Alarm_Exists_Bit = 0;
	g_Error_Exists_Bit = 0;
	g_Warning_Exists_Bit = 0;
	
	FILL(0, list_Alarm_History);
	g_Alarm_Register_Request_AlarmID = 0;
	g_Alarm_Register_Request_Bit = 0;
	alarm_RegisterCmp_Index = 0;	
	
RET

! ----------------------------------------------------------------------------------------------

sr_AcsController_StateReport:
		
	! *** Control fault >> Operaion PLC/Main PLC
	IF(g_Error_Exists_Bit = 0)
		!g_eCAT_Output_Data(0)(0x30).0 = 1;
		!g_eCAT_Output_Data(0)(0x30).1 = 0;
	ELSE
		!g_eCAT_Output_Data(0)(0x30).0 = 0;
		!g_eCAT_Output_Data(0)(0x30).1 = 1;
	END
RET

! ----------------------------------------------------------------------------------------------

sr_DynoOilPump_Process:
! *** Dynamometer Oil Pump On / Off Control
	IF(g_Feedback_Speed > g_dynoCtrl_OilPumpOnRPM)
		! *** Oil Pump On CMD >> Power unit LSE 513
		!g_eCAT_Output_Data(0)(0x20).0 = 1;
	ELSE
		! *** Oil Pump On CMD >> Power unit LSE 513
		!g_eCAT_Output_Data(0)(0x20).0 = 0;
	END
	
RET

! ----------------------------------------------------------------------------------------------

sr_AlarmServer_AlarmRegister:
g_Hmi_bs700x_Alarm_ID = g_Alarm_Register_Request_AlarmID;
			
	! *** Same Alarm Check
		IF((list_Alarm_History(alarm_RegisterCmp_Index) <> g_Alarm_Register_Request_AlarmID) & (g_Alarm_Register_Request_AlarmID <> 0))				
			
			list_Alarm_History(alarm_RegisterRequest_Index) = g_Alarm_Register_Request_AlarmID;
			
			g_Hmi_bs700x_Alarm_ID = g_Alarm_Register_Request_AlarmID;
			
			alarm_RegisterCmp_Index = alarm_RegisterRequest_Index
				! *** Alarm Type Check
				IF(list_Alarm_History(alarm_RegisterCmp_Index) > 0)
					g_Warning_Exists_Bit = 1;
					g_Alarm_Exists_Bit = 1;
				ELSE
					g_Error_Exists_Bit = 1;
					g_Alarm_Exists_Bit = 1;
				END
				
			! *** Alarm List Index Check
			IF(alarm_RegisterCmp_Index < alarm_History_ListSize)
				alarm_RegisterRequest_Index = alarm_RegisterRequest_Index + 1;
			ELSE
				alarm_RegisterRequest_Index = 0
			END ! IF(Alarm_index_Latest_aHistory >= Alarm_aHistory_ListSize)
			
			! *** MMI Terminal display		
			DISP "[#%d buffer] Alarm Register! AID = <%d>", BufferProgrmaNumber, list_Alarm_History(alarm_RegisterCmp_Index);
			
		END ! IF(Alarm_list_Alarm_History(Alarm_index_Latest_aHistory) <> Alarm_enum_aID_Dyno_AMP_Fault)	
		
RET


! ----------------------------------------------------------------------------------------------

sr_Alarm_Check:
	
	! System Initial Program Complete Check
	IF(g_bufferProgramResult(0)(1) = 99999)
			
		IF(g_SystemState_Dyno_On = 1)
						
			!CALL sr_Dyno_CasingRing_Fault;
			!CALL sr_Dyno_Water_TempFlow_Fault;			
			!CALL sr_Dyno_AMP_Fault;		
		END
		
		IF(g_Feedback_Speed > g_dynoCtrl_OilPumpOnRPM)
			
			!CALL sr_Dyno_OilPump_Level_Fault;
		END
		
		
	!	CALL sr_Alarm_Host_HMI_PLC_StateCheck;
		
	END ! IF(g_bufferProgramResult(0)(1) = 9999)
		
RET




#3
! *** Mode Control & Host Interface Program
	
INT BufferProgrmaNumber
	BufferProgrmaNumber = 3

GLOBAL INT g_Buffer3_displayCmdLevel;
	g_Buffer3_displayCmdLevel = 0;
		
! MMI Show Message
IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] program started.", BufferProgrmaNumber; END;

! Alarm ID Define (Error Alarm ID = (Buffer No * 10000) + Error No, Error Alarm ID = Error Alarm ID * -1)
! Alarm ID Define (Warning Alarm ID = (Buffer No * 10000) + Warning No, Error Alarm ID = Warning Alarm ID)	
INT enum_aID_Buf3_W_SysCMD_Reset_ReqOn;
	enum_aID_Buf3_W_SysCMD_Reset_ReqOn = 30001;
INT enum_aID_Buf3_W_SysCMD_Reset_ReqOff;
	enum_aID_Buf3_W_SysCMD_Reset_ReqOff = 30002;	

INT enum_aID_Buf3_E_SysCMD_Dyno_On_Fail;
	enum_aID_Buf3_E_SysCMD_Dyno_On_Fail = -30001;
INT enum_aID_Buf3_E_SysCMD_Dyno_Off_Fail;
	enum_aID_Buf3_E_SysCMD_Dyno_Off_Fail = -30002;

INT enum_aID_Buf3_W_SysCMD_EMS_ReqOn;
	enum_aID_Buf3_W_SysCMD_EMS_ReqOn = -30003;
INT enum_aID_Buf3_W_SysCMD_EMS_ReqOff;
	enum_aID_Buf3_W_SysCMD_EMS_ReqOff = 30003;
	
INT enum_aID_Buf3_W_SysCMD_DynaFualt_ReqOn;
	enum_aID_Buf3_W_SysCMD_DynaFualt_ReqOn = 30004;
INT enum_aID_Buf3_E_SysCMD_IG_On_Fail;
	enum_aID_Buf3_E_SysCMD_IG_On_Fail = -30011;
INT enum_aID_Buf3_E_SysCMD_IG_Off_Fail;
	enum_aID_Buf3_E_SysCMD_IG_Off_Fail = -30012;
	
INT enum_aID_Buf3_E_SysCMD_Misc_On_Fail;
	enum_aID_Buf3_E_SysCMD_Misc_On_Fail = -30015;
INT enum_aID_Buf3_E_SysCMD_Misc_Off_Fail;
	enum_aID_Buf3_E_SysCMD_Misc_Off_Fail = -30016;

INT enum_aID_Buf3_E_SysCMD_Engine_PreheatOn_Fail;
	enum_aID_Buf3_E_SysCMD_Engine_PreheatOn_Fail = -30021;
INT enum_aID_Buf3_E_SysCMD_Engine_PreheatOff_Fail;
	enum_aID_Buf3_E_SysCMD_Engine_PreheatOff_Fail = -30022;
INT enum_aID_Buf3_E_SysCMD_Engine_PreheatTOutOff_Fail;
	enum_aID_Buf3_E_SysCMD_Engine_PreheatTOutOff_Fail = -30023;

INT enum_aID_Buf3_E_SysCMD_Engine_StartCmdOn_Fail;
	enum_aID_Buf3_E_SysCMD_Engine_StartCmdOn_Fail = -30031;
INT enum_aID_Buf3_W_SysCMD_Engine_StartStopSameTimeSwOn
	enum_aID_Buf3_W_SysCMD_Engine_StartStopSameTimeSwOn = 30032;
INT enum_aID_Buf3_E_SysCMD_Engine_StartCmdTOutOff_Fail;
	enum_aID_Buf3_E_SysCMD_Engine_StartCmdTOutOff_Fail = -30033;
INT enum_aID_Buf3_E_SysCMD_Engine_StartCmdTSReachedOff_Fail;
	enum_aID_Buf3_E_SysCMD_Engine_StartCmdTSReachedOff_Fail = -30034;

INT enum_aID_Buf3_E_SysCMD_Engine_StopCmdOff_Fail;
	enum_aID_Buf3_E_SysCMD_Engine_StopCmdOff_Fail = -30041;


INT enum_aID_Buf3_W_DrMn_ChgReq_ManualModeCheck_Fail;
	enum_aID_Buf3_W_DrMn_ChgReq_ManualModeCheck_Fail = 32001;
INT enum_aID_Buf3_W_DrMn_IdleChgReq_EngStartCheck_Fail;
	enum_aID_Buf3_W_DrMn_IdleChgReq_EngStartCheck_Fail = 32002;
INT enum_aID_Buf3_W_DrMn_IdleExceptChgReq_EngStartCheck_Fail;
	enum_aID_Buf3_W_DrMn_IdleExceptChgReq_EngStartCheck_Fail = 32003;
INT enum_aID_Buf3_W_DrMn_IdleExceptChgReq_DynoOnCheck_Fail;
	enum_aID_Buf3_W_DrMn_IdleExceptChgReq_DynoOnCheck_Fail = 32004;
INT enum_aID_Buf3_W_DrMn_IdleExceptChgReq_IdleMdCheck_Fail;
	enum_aID_Buf3_W_DrMn_IdleExceptChgReq_IdleMdCheck_Fail = 32005;
INT enum_aID_Buf3_W_DrMn_ChgReq_AutomaticModeCheck_Fail;
	enum_aID_Buf3_W_DrMn_ChgReq_AutomaticModeCheck_Fail = 32006;
	
INT enum_aID_Buf3_W_DrMn_ChgReq_InterlockCheck_Fail;
	enum_aID_Buf3_W_DrMn_ChgReq_InterlockCheck_Fail = 32011;

INT enum_aID_Buf3_W_AuSoftW_ChgReq_SoftWare_Fault;
	enum_aID_Buf3_W_AuSoftW_ChgReq_SoftWare_Fault = 33001;
INT enum_aID_Buf3_W_AuSoftW_ChgReq_SoftWare_NotIdle;
	enum_aID_Buf3_W_AuSoftW_ChgReq_SoftWare_NotIdle = 33002;	
INT enum_aID_Buf3_W_AuSoftW_ChgReq_SoftWare_NotOn;
	enum_aID_Buf3_W_AuSoftW_ChgReq_SoftWare_NotOn = 33003;		
	
INT enum_aID_Buf3_W_AuSoftW_TqApUsingProfile_WriteCompleteCheckFail;
	enum_aID_Buf3_W_AuSoftW_TqApUsingProfile_WriteCompleteCheckFail = 33004;
INT enum_aID_Buf3_W_AuSoftW_SpApUsingProfile_WriteCompleteCheckFail;
	enum_aID_Buf3_W_AuSoftW_SpApUsingProfile_WriteCompleteCheckFail = 33005;	
INT enum_aID_Buf3_W_AuSoftW_TqSpUsingProfile_WriteCompleteCheckFail;
	enum_aID_Buf3_W_AuSoftW_TqSpUsingProfile_WriteCompleteCheckFail = 33006;	
INT enum_aID_Buf3_W_AuSoftW_SpTqUsingProfile_WriteCompleteCheckFail;
	enum_aID_Buf3_W_AuSoftW_SpTqUsingProfile_WriteCompleteCheckFail = 33007;		
INT enum_aID_Buf3_W_AuSoftW_UsingProfile_Fail_AutoMd_ProfilePlay_On;
	enum_aID_Buf3_W_AuSoftW_UsingProfile_Fail_AutoMd_ProfilePlay_On = 33008;		
	
INT enum_aID_Buf3_W_AuSoftW_TqApProfile_StepCountCheckFail;
	enum_aID_Buf3_W_AuSoftW_TqApProfile_StepCountCheckFail = 33009;
INT enum_aID_Buf3_W_AuSoftW_SpApProfile_StepCountCheckFail;
	enum_aID_Buf3_W_AuSoftW_SpApProfile_StepCountCheckFail = 33010;	
INT enum_aID_Buf3_W_AuSoftW_TqSpProfile_StepCountCheckFail;
	enum_aID_Buf3_W_AuSoftW_TqSpProfile_StepCountCheckFail = 33011;	
INT enum_aID_Buf3_W_AuSoftW_SpTqProfile_StepCountCheckFail;
	enum_aID_Buf3_W_AuSoftW_SpTqProfile_StepCountCheckFail = 33012;		

INT enum_aID_Buf3_W_Automatic_ControlOnOffBit_Fail;
	enum_aID_Buf3_W_Automatic_ControlOnOffBit_Fail = 33013;
INT enum_aID_Buf3_W_Automatic_CtrlOn_AutomaticModeCheckFail;
	enum_aID_Buf3_W_Automatic_CtrlOn_AutomaticModeCheckFail = 33014;	


INT enum_aID_Buf3_W_DrMn_IdleMdCheck_SpeedCheck_Fail;
	enum_aID_Buf3_W_DrMn_IdleMdCheck_SpeedCheck_Fail = 33015;
INT enum_aID_Buf3_W_DrMn_IdleMdCheck_TorqueCheck_Fail;
	enum_aID_Buf3_W_DrMn_IdleMdCheck_TorqueCheck_Fail = 33016;
INT enum_aID_Buf3_W_DrMn_IdleMdCheck_AlphaCheck_Fail;
	enum_aID_Buf3_W_DrMn_IdleMdCheck_AlphaCheck_Fail = 33017;

INT enum_aID_Buf3_W_DynoService_IdleAndDefaultMdCheck_Fail;
	enum_aID_Buf3_W_DynoService_IdleAndDefaultMdCheck_Fail = 33018;
	
INT enum_aID_Buf3_W_MeasurEqp_bbm_ResetFail_OtherRequestOn;
	enum_aID_Buf3_W_MeasurEqp_bbm_ResetFail_OtherRequestOn = 34001;
INT enum_aID_Buf3_W_MeasurEqp_bbm_MeasurStartFail_OtherRequestOn;
	enum_aID_Buf3_W_MeasurEqp_bbm_MeasurStartFail_OtherRequestOn = 34002;
INT enum_aID_Buf3_W_MeasurEqp_bbm_MeasurStopFail_OtherRequestOn;
	enum_aID_Buf3_W_MeasurEqp_bbm_MeasurStopFail_OtherRequestOn = 34003;
	
INT enum_aID_Buf3_W_MeasurEqp_smkm_ResetFail_OtherRequestOn;
	enum_aID_Buf3_W_MeasurEqp_smkm_ResetFail_OtherRequestOn = 34011;
INT enum_aID_Buf3_W_MeasurEqp_smkm_MeasurStartFail_OtherRequestOn;
	enum_aID_Buf3_W_MeasurEqp_smkm_MeasurStartFail_OtherRequestOn = 34012;
INT enum_aID_Buf3_W_MeasurEqp_smkm_MeasurStopFail_OtherRequestOn;
	enum_aID_Buf3_W_MeasurEqp_smkm_MeasurStopFail_OtherRequestOn = 34013;	

INT enum_aID_Buf3_W_MeasurEqp_FuelMeter_ResetFail_OtherRequestOn;
	enum_aID_Buf3_W_MeasurEqp_FuelMeter_ResetFail_OtherRequestOn = 34021;
INT enum_aID_Buf3_W_MeasurEqp_FuelMeter_MeasurStartFail_OtherRequestOn;
	enum_aID_Buf3_W_MeasurEqp_FuelMeter_MeasurStartFail_OtherRequestOn = 34022;
INT enum_aID_Buf3_W_MeasurEqp_FuelMeter_MeasurStopFail_OtherRequestOn;
	enum_aID_Buf3_W_MeasurEqp_FuelMeter_MeasurStopFail_OtherRequestOn = 34023;		
	
! *** HMI Variable	
INT index_HmiTagVar_FlashAccess;	
INT index_Hmi_EdtValToStoreValSave;
INT Hmi_EdtValToStoreValSaveCount;

! *** HMI Save Time-Out Time Measure	
	g_Hmi_bs2201_EditSaveTOutSt = 10000;
REAL Hmi_SaveTimeOut_Measure_1;
REAL Hmi_SaveTimeOut_Measure_2;
REAL Hmi_SaveTimeOut_Difference;
	
	Hmi_SaveTimeOut_Measure_1 = 0;
	Hmi_SaveTimeOut_Measure_2 = 0;
	Hmi_SaveTimeOut_Difference = 0;
! *** HMI Banner display enable time	
!GLOBAL INT g_Hmi_BannerSetAndResetInterval
	g_Hmi_BannerSetAndResetInterval = 1500;
! *** O/P Switch or HMI Screen change control	
INT OpBox_HmiScreenChgCheck_PreviousControlMode
	OpBox_HmiScreenChgCheck_PreviousControlMode = g_CtMo_enum_Default;
INT OpBox_HmiScreenChgCheck_PreviousDriveMode
	OpBox_HmiScreenChgCheck_PreviousDriveMode = g_DrMn_enum_Default;
INT OpBox_EmoSwitch_PreviousStatus;
	OpBox_EmoSwitch_PreviousStatus = 1;
	
INT OpBox_SafetyGuardSwitch_PreviousStatus;
	OpBox_SafetyGuardSwitch_PreviousStatus = 1;
	
INT DAT_DYNO_FAULT_ON;
	DAT_DYNO_FAULT_ON = 0;
INT DAT_HORN_SILENT_ON;
	DAT_HORN_SILENT_ON = 0;
int DI_Reset_Button;
	DI_Reset_Button = 0;

INT DrMn_Current_ChangeReq_Mode;
INT DrMn_Previous_ChangeReq_Mode;
INT Dyno_Off_Cmd_Processing_Complete	

	CALL sr_SystemCommand_Valiable_Declaration;
	CALL sr_SystemCommand_Valiable_Init;	
	! *** Program Resule Update
	g_bufferProgramResult(0)(BufferProgrmaNumber) = 100;
	
	CALL sr_ControlMode_Valiable_Declaration;
	CALL sr_ControlMode_Valiable_Init;	
	! *** Program Resule Update
	g_bufferProgramResult(0)(BufferProgrmaNumber) = 200;
	
	CALL sr_DriveManual_Valiable_Declaration
	CALL sr_DriveManual_Valiable_Init;	
	! *** Program Resule Update
	g_bufferProgramResult(0)(BufferProgrmaNumber) = 300;	


	! *** Program Resule Update
	g_bufferProgramResult(0)(BufferProgrmaNumber) = 400;
	
	CALL sr_AutomationSWCommand_Valiable_Declaration;
	CALL sr_AutomationSWCommand_Valiable_Init;
	
	! *** Program Resule Update
	g_bufferProgramResult(0)(BufferProgrmaNumber) = 500;
	
		WHILE (1)			

			CALL sr_SystemCommand_Process;			
			
			CALL sr_ControlMode_ChangeRequest_Process;
			
			CALL sr_DriveManual_ChangeRequest_Process;				
						
			
			CALL sr_AutomationSWCommand_Profile_WriteProcess;
			
			CALL sr_AutomationSWCommand_CtrlModeChgCommand_Process;
			
			CALL sr_AutomationSWCommand_MeasurEqpControl_Process;
			
			CALL sr_AutomationSWCommand_StopCommand_Process;
			
			WAIT(1);
			
		END !WHILE 1
			
! *** Program Resule Update
g_bufferProgramResult(0)(BufferProgrmaNumber) = 99999;

! *** MMI Terminal display
DISP "[#%d buffer] program end.", BufferProgrmaNumber;

STOP

! *************** Autoroutine  *************** !

! ***  Mode Control (#3) Program Stop processing
ON ^PST(3).#RUN	
	
	CALL sr_SystemCommand_Valiable_Init;	
	
	IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] Mode Control Program Stop! All Program Stop Autoroutine.", BufferProgrmaNumber END;
	
RET

! *************** Autoroutine  *************** !
! *** Horn Off
ON g_HMI_DI_HornOff_Button
	
	IF(DAT_HORN_SILENT_ON <> 1)	
		! *** MMI Terminal display		
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Autoroutine; DAS Horn Silent ON!", BufferProgrmaNumber; END;
		
		! *** Banner message display.(EMO Switch On) 								
		!g_Hmi_Banner_MsgLineNum = 0; g_Bf8_AutoR_Banner_Display = 0;
		
		WAIT(100);
		
		! *** O/P EMO Previous Status Update		
		DAT_HORN_SILENT_ON = 1;
		
		g_HMI_DO_HornOff_Lamp = 1;
		g_DO_HornSignal = 0;
		!g_DO_Alarm_Lamp = 0;
		
		! *** MMI Terminal display		
	ELSE
! *** MMI Terminal display		
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Autoroutine; DAS Horn Silent OFF!", BufferProgrmaNumber; END;
		
		! *** Banner message display.(EMO Switch On) 								
		!g_Hmi_Banner_MsgLineNum = 0; g_Bf8_AutoR_Banner_Display = 0;
		
		WAIT(100);
		
		! *** O/P EMO Previous Status Update		
		DAT_HORN_SILENT_ON = 0;
		
		g_HMI_DO_HornOff_Lamp = 0;
		g_DO_HornSignal = 0;
		! *** MMI Terminal display		
	END ! IF(OpBox_EmoSwitch_PreviousStatus = 0)	
RET


! *** STO_FEEDBACK Fail
ON ^g_STO_FEEDBACK
	
	IF(DAT_DYNO_FAULT_ON <> 1)	
		! *** MMI Terminal display		
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Autoroutine; DAS Dyno Fault ON!", BufferProgrmaNumber; END;
		
		! *** Banner message display.(EMO Switch On) 								
		!g_Hmi_Banner_MsgLineNum = 0; g_Bf8_AutoR_Banner_Display = 0;
		
		WAIT(100);
		
		! *** O/P EMO Previous Status Update		
		DAT_DYNO_FAULT_ON = 1;
		
		
		! *** Alarm Register
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_SysCMD_DynaFualt_ReqOn;		
		g_Alarm_Register_Request_Bit = 1;
							
		! *** MMI Terminal display		
	END ! IF(OpBox_EmoSwitch_PreviousStatus = 0)	
RET
! *** O/P EMS On or Safety Relay Fail
ON g_STO_FEEDBACK
	
	IF(DAT_DYNO_FAULT_ON = 1)	
		! *** MMI Terminal display		
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Autoroutine; DAS Dyno Fault OFF!", BufferProgrmaNumber; END;
		
		! *** Banner message display.(EMO Switch On) 								
		!g_Hmi_Banner_MsgLineNum = 0; g_Bf8_AutoR_Banner_Display = 0;
		
		WAIT(100);
		
		! *** O/P EMO Previous Status Update		
		DAT_DYNO_FAULT_ON = 0;
		! *** MMI Terminal display		
	END ! IF(OpBox_EmoSwitch_PreviousStatus = 0)	
RET


! *** O/P EMS On or Safety Relay Fail
ON ^g_TEMP_WARNING
	
	IF(DAT_DYNO_FAULT_ON <> 1)	
		! *** MMI Terminal display		
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Autoroutine; DAS Dyno Fault ON!", BufferProgrmaNumber; END;
		
		! *** Banner message display.(EMO Switch On) 								
		!g_Hmi_Banner_MsgLineNum = 0; g_Bf8_AutoR_Banner_Display = 0;
		
		WAIT(100);
		
		! *** O/P EMO Previous Status Update		
		DAT_DYNO_FAULT_ON = 1;
		
		
		! *** Alarm Register
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_SysCMD_DynaFualt_ReqOn;		
		g_Alarm_Register_Request_Bit = 1;
							
		! *** MMI Terminal display		
	END ! IF(OpBox_EmoSwitch_PreviousStatus = 0)	
RET
! *** O/P EMS On or Safety Relay Fail
ON g_TEMP_WARNING
	
	IF(DAT_DYNO_FAULT_ON = 1)	
		! *** MMI Terminal display		
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Autoroutine; DAS Dyno Fault OFF!", BufferProgrmaNumber; END;
		
		! *** Banner message display.(EMO Switch On) 								
		!g_Hmi_Banner_MsgLineNum = 0; g_Bf8_AutoR_Banner_Display = 0;
		
		WAIT(100);
		
		! *** O/P EMO Previous Status Update		
		DAT_DYNO_FAULT_ON = 0;
		! *** MMI Terminal display		
	END ! IF(OpBox_EmoSwitch_PreviousStatus = 0)	
RET


! *** O/P EMS On or Safety Relay Fail
ON g_DI_DynoTroubleSignal
	
	IF(DAT_DYNO_FAULT_ON <> 1)	
		! *** MMI Terminal display		
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Autoroutine; DAS Dyno Fault ON!", BufferProgrmaNumber; END;
		
		! *** Banner message display.(EMO Switch On) 								
		!g_Hmi_Banner_MsgLineNum = 0; g_Bf8_AutoR_Banner_Display = 0;
		
		WAIT(100);
		
		! *** O/P EMO Previous Status Update		
		DAT_DYNO_FAULT_ON = 1;
		
		
		! *** Alarm Register
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_SysCMD_DynaFualt_ReqOn;		
		g_Alarm_Register_Request_Bit = 1;
							
		! *** MMI Terminal display		
	END ! IF(OpBox_EmoSwitch_PreviousStatus = 0)	
RET
! *** O/P EMS On or Safety Relay Fail
ON ^g_DI_DynoTroubleSignal
	
	IF(DAT_DYNO_FAULT_ON = 1)	
		! *** MMI Terminal display		
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Autoroutine; DAS Dyno Fault OFF!", BufferProgrmaNumber; END;
		
		! *** Banner message display.(EMO Switch On) 								
		!g_Hmi_Banner_MsgLineNum = 0; g_Bf8_AutoR_Banner_Display = 0;
		
		WAIT(100);
		
		! *** O/P EMO Previous Status Update		
		DAT_DYNO_FAULT_ON = 0;
		! *** MMI Terminal display		
	END ! IF(OpBox_EmoSwitch_PreviousStatus = 0)	
RET

! *** O/P EMS On or Safety Relay Fail
ON g_DI_SafetyGuardSignal
	
	IF(OpBox_SafetyGuardSwitch_PreviousStatus <> 1)	
		! *** MMI Terminal display		
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Autoroutine; O/P EMS On or Safety Relay Fail!", BufferProgrmaNumber; END;
		
		! *** Banner message display.(EMO Switch On) 								
		g_Hmi_Banner_MsgLineNum = 0; g_Bf8_AutoR_Banner_Display = 0;
		
		WAIT(100);
		
		!IF(PST(0).#RUN <> 1)
		!	START 0,1;
		!END
		
		! *** O/P EMO Previous Status Update		
		OpBox_SafetyGuardSwitch_PreviousStatus = 1;
		
		
		! *** Alarm Register
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_SysCMD_Reset_ReqOn;		
		g_Alarm_Register_Request_Bit = 1;
							
		! *** MMI Terminal display		
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp]Autoroutine; O/P EMS On or Safety Relay Fail!", BufferProgrmaNumber;	END;
	END ! IF(OpBox_EmoSwitch_PreviousStatus = 0)	
RET

! ----------------------------------------------------------------------------------------------

ON ^g_DI_SafetyGuardSignal
	BLOCK
	IF(OpBox_SafetyGuardSwitch_PreviousStatus <> 2)
	
		! *** MMI Terminal display		
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Autoroutine; O/P EMS On or Safety Relay Normal!", BufferProgrmaNumber; END;
		
		! *** O/P EMO Previous Status Update		
		OpBox_SafetyGuardSwitch_PreviousStatus = 2;
		
		g_SystemState_EMS = 1;
		
		g_stop_StopMode = g_stop_enum_HardStopping;
		! *** Dyno Current Command 0 Set
		g_AO_DynoControlSignal = 0;!16383;
		! *** Engine Start Off CMD Excute
		g_DO_StartSignal = 0;
		! *** Engine Ignition Off CMD Excute 
		!if (g_SystemState_Eng_IG_On = 1)  ! Toggle Signal Processing
		!	g_DO_IgnitionSignal = 1;
		!END
		!if (g_SystemState_Dyno_On = 1)
		!	g_DO_DynoOnSignal = 1;
		!END
		!if (g_SystemState_Misc_On=1)
		!	g_DO_BKReadySignal = 1;
		!END
			
		! *** Check Delay Time for Dyno On State
		!WAIT(100);
	
		! *** Engine Ignition Off CMD Check & Result Update
		g_DO_DynoOnSignal = 0;
		g_DO_IgnitionSignal = 0;
		g_DO_BKReadySignal = 0;
		g_DO_FANSignal = 0;
		g_SystemState_Eng_Starting = 0;	
		g_SystemState_Eng_Started = 0;	
			
		! *** Engine Ignition Reset.
		g_SystemState_Eng_IG_On = 0;
		g_SystemState_Misc_On = 0;
		g_SystemState_Dyno_On = 0;
		
		! *** Dyno Off. Control Mode Monitor.
		g_ControlMode = g_CtMo_enum_Monitor;
		! *** Engine Stop. Drive Mode Change to default.				
		DrMn_Current_ChangeReq_Mode  = g_DrMn_enum_Default;
		DrMn_Previous_ChangeReq_Mode = 0;	

		! *** Alarm Register
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_SysCMD_EMS_ReqOn;		
		g_Alarm_Register_Request_Bit = 1;
		
		! *** Stop Dyno./Engine PID Program
		IF(PST(5).#RUN = 1)
			STOP 5;
		END
		
		IF(PST(6).#RUN = 1)
			STOP 6;
		END			
		
		g_Hmi_Banner_MsgLineNum = 20; g_Bf8_AutoR_Banner_Display = 1;
		! *** MMI Terminal display		
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp]Autoroutine; O/P EMS On or Safety Relay Normal!", BufferProgrmaNumber; END;
		
	END ! IF(OpBox_EmoSwitch_PreviousStatus <> 0)
	END
RET
! *** O/P EMS Off or Safety Relay Normal
! *** O/P EMS On or Safety Relay Fail
ON ^g_DI_EMSButton
	
	IF(OpBox_EmoSwitch_PreviousStatus <> 1)	
		! *** MMI Terminal display		
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Autoroutine; O/P EMS On or Safety Relay Fail!", BufferProgrmaNumber; END;
		
		! *** Banner message display.(EMO Switch On) 								
		g_Hmi_Banner_MsgLineNum = 0; g_Bf8_AutoR_Banner_Display = 0;
		
		WAIT(100);
		
		!IF(PST(0).#RUN <> 1)
		!	START 0,1;
		!END
		
		! *** O/P EMO Previous Status Update		
		OpBox_EmoSwitch_PreviousStatus = 1;
		
		
		! *** Alarm Register
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_SysCMD_Reset_ReqOn;		
		g_Alarm_Register_Request_Bit = 1;
							
		! *** MMI Terminal display		
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp]Autoroutine; O/P EMS On or Safety Relay Fail!", BufferProgrmaNumber;	END;
	END ! IF(OpBox_EmoSwitch_PreviousStatus = 0)	
RET

ON g_DI_EMSButton
	BLOCK
	IF(OpBox_EmoSwitch_PreviousStatus <> 2)
	
		! *** MMI Terminal display		
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Autoroutine; O/P EMS On or Safety Relay Normal!", BufferProgrmaNumber; END;
		
		! *** O/P EMO Previous Status Update		
		OpBox_EmoSwitch_PreviousStatus = 2;
		
		g_SystemState_EMS = 1;
		
		g_stop_StopMode = g_stop_enum_HardStopping;
		! *** Dyno Current Command 0 Set
		g_AO_DynoControlSignal = 0;!16383;
		! *** Engine Start Off CMD Excute
		g_DO_StartSignal = 0;
		! *** Engine Ignition Off CMD Excute 
		!if (g_SystemState_Eng_IG_On = 1)  ! Toggle Signal Processing
		!	g_DO_IgnitionSignal = 1;
		!END
		!if (g_SystemState_Dyno_On = 1)
		!	g_DO_DynoOnSignal = 1;
		!END
		!if (g_SystemState_Misc_On=1)
		!	g_DO_BKReadySignal = 1;
		!END
			
		! *** Check Delay Time for Dyno On State
		!WAIT(100);
	
		! *** Engine Ignition Off CMD Check & Result Update
		g_DO_DynoOnSignal = 0;
		g_DO_FANSignal = 0;
		g_DO_IgnitionSignal = 0;
		g_DO_BKReadySignal = 0;
		g_SystemState_Eng_Starting = 0;	
		g_SystemState_Eng_Started = 0;	
			
		! *** Engine Ignition Reset.
		g_SystemState_Eng_IG_On = 0;
		g_SystemState_Misc_On = 0;
		g_SystemState_Dyno_On = 0;
		
		! *** Dyno Off. Control Mode Monitor.
		g_ControlMode = g_CtMo_enum_Monitor;
		! *** Engine Stop. Drive Mode Change to default.				
		DrMn_Current_ChangeReq_Mode  = g_DrMn_enum_Default;
		DrMn_Previous_ChangeReq_Mode = 0;	

		! *** Alarm Register
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_SysCMD_EMS_ReqOn;		
		g_Alarm_Register_Request_Bit = 1;
		
		! *** Stop Dyno./Engine PID Program
		IF(PST(5).#RUN = 1)
			STOP 5;
		END
		
		IF(PST(6).#RUN = 1)
			STOP 6;
		END			
		
		g_Hmi_Banner_MsgLineNum = 20; g_Bf8_AutoR_Banner_Display = 1;
		! *** MMI Terminal display		
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp]Autoroutine; O/P EMS On or Safety Relay Normal!", BufferProgrmaNumber; END;
		
	END ! IF(OpBox_EmoSwitch_PreviousStatus <> 0)
	END
RET

! *************** Subroutine  *************** !

sr_SystemCommand_Valiable_Declaration:
	! *** System command managing variable declaration
	INT Sys_Current_Command_Input;
	INT Sys_Previous_Command_Input;
		
	INT Sys_refreshBitIndex_Dyno_On;	
	INT Sys_refreshBitIndex_Eng_IG_On;	
	INT Sys_refreshBitIndex_Eng_Preheat_On;	
	INT Sys_refreshBitIndex_Eng_Start;	
	INT Sys_refreshBitIndex_Stop;	
	INT Sys_refreshBitIndex_Reset;	
	INT Sys_refreshBitIndex_EMS;	
	INT Sys_refreshBitIndex_Misc;
	INT Sys_refreshBitIndex_Seant;
	!GLOBAL INT g_SystemState_Dyno_On; -> D-Buffer
	!GLOBAL INT g_SystemState_Reset; -> D-Buffer
	
	! *** Dyno command managing variable declaration
	INT Dyno_OnOff_Cmd_Processing_Result
	INT Dyno_On_Cmd_Processing_Complete
	! *** Engine command managing variable declaration
	REAL Engine_Preheat_SwitchOnTimeMeasure_1;
	REAL Engine_Preheat_SwitchOnTimeMeasure_2;	
	REAL Engine_Start_SwitchOnTimeMeasure_1;
	REAL Engine_Start_SwitchOnTimeMeasure_2;	
	
RET

! ----------------------------------------------------------------------------------------------

sr_SystemCommand_Valiable_Init:
	
	! *** System command managing variable initialization

	Sys_Current_Command_Input = 0;
	Sys_Previous_Command_Input = 0;				
	
	Sys_refreshBitIndex_Dyno_On  = 0;
	Sys_refreshBitIndex_Eng_IG_On = 1;
	Sys_refreshBitIndex_Eng_Preheat_On = 2;
	Sys_refreshBitIndex_Eng_Start = 3;
	Sys_refreshBitIndex_Stop = 4;
	Sys_refreshBitIndex_EMS = 5;
	Sys_refreshBitIndex_Reset  = 6;		
	Sys_refreshBitIndex_Misc = 7;
	Sys_refreshBitIndex_Seant = 8;
	
	g_SystemState_Dyno_On = 0;	
	g_SystemState_Eng_IG_On = 0;	
	g_SystemState_Eng_Preheat_On = 0;	 
	g_SystemState_Eng_Starting = 0;	
	g_SystemState_Eng_Started = 0;	
	g_SystemState_Misc_On = 0;	

	g_DO_Seant_On = 0;	

	
	g_stop_enum_Default = -1;
	g_stop_enum_Stopping = 10;
	g_stop_enum_IdleStopping = 20;
	g_stop_enum_SoftStopping = 30;
	g_stop_enum_HardStopping = 40;
	g_stop_enum_Stopped = 100;	
	g_stop_StopMode = g_stop_enum_Default;
	
	g_SystemState_Reset = 0;	
	g_SystemState_EMS = 0;
	! *** Dyno command managing variable initialization
	Dyno_OnOff_Cmd_Processing_Result = -1;
	Dyno_On_Cmd_Processing_Complete = 100;
	Dyno_Off_Cmd_Processing_Complete = 200;

	! *** Engine command managing variable initialization
	Engine_Preheat_SwitchOnTimeMeasure_1 = 0;
	Engine_Preheat_SwitchOnTimeMeasure_2 = 0;

	Engine_Start_SwitchOnTimeMeasure_1 = 0;
	Engine_Start_SwitchOnTimeMeasure_2 = 0;	
	
RET

! ----------------------------------------------------------------------------------------------

sr_ControlMode_Valiable_Declaration:
	! *** Control mode managing variable declaration
	INT CtMo_Current_ChangeReq_Input;
	INT CtMo_Previous_ChangeReq_Input;
	
	INT CtMo_inputBitIndex_Monitor;
	INT CtMo_inputBitIndex_DynoService;
	INT CtMo_inputBitIndex_Manual;
	INT CtMo_inputBitIndex_Automatic;
	INT CtMo_inputBitIndex_Remote;
	!GLOBAL INT g_ControlMode; -> D-Buffer
	!GLOBAL INT g_ControlPreviousMode
	!GLOBAL INT CtMo_enum_Default; -> D-Buffer
	!GLOBAL INT CtMo_enum_Monitor; -> D-Buffer
	!GLOBAL INT CtMo_enum_DynoService; -> D-Buffer
	!GLOBAL INT CtMo_enum_Manual; -> D-Buffer
	!GLOBAL INT CtMo_enum_Automatic; -> D-Buffer
	
	! *** Control Mode Interlock variable define
	INT CtMo_Function_Enable;
	
RET

! ----------------------------------------------------------------------------------------------

sr_ControlMode_Valiable_Init:
	
	! *** Control mode managing variable initialization
	
	CtMo_Current_ChangeReq_Input = 0;
	CtMo_Previous_ChangeReq_Input = 0;
			
	CtMo_inputBitIndex_Monitor = 0;
	CtMo_inputBitIndex_DynoService = 1;
	CtMo_inputBitIndex_Manual = 2;
	CtMo_inputBitIndex_Automatic  = 3;
	CtMo_inputBitIndex_Remote  = 4;
	
	g_CtMo_enum_Default = 10;
	g_CtMo_enum_Monitor = 100;
	g_CtMo_enum_OpenLoopTest = 200;
	g_CtMo_enum_Manual = 300;
	g_CtMo_enum_Automatic = 400;
	g_CtMo_enum_Remote = 500;
	
	g_ControlMode = g_CtMo_enum_Monitor;	
	
	! *** Control Mode Interlock variable initialization
	CtMo_Function_Enable = 0;		
	
	CtMo_Function_Enable.(0) = 1; ! Monitor disable
	CtMo_Function_Enable.(1) = 1; ! DynoService disable
	CtMo_Function_Enable.(2) = 1; ! Manual enable
	CtMo_Function_Enable.(3) = 1; ! Automatic enable
	CtMo_Function_Enable.(4) = 1; ! Remote enable
	
RET

! ----------------------------------------------------------------------------------------------

sr_DriveManual_Valiable_Declaration:
	! *** Drive manual managing variable declaration
	
	INT DrMn_ManualMode_Current_ChangeReq_Input;
	INT DrMn_ManualMode_Previous_ChangeReq_Input;
	INT DrMn_AutomaticMode_Current_ChangeReq_Input;
	INT DrMn_AutomaticMode_Previous_ChangeReq_Input;
	INT DrMn_RemoteMode_Current_ChangeReq_Input;
	INT DrMn_RemoteMode_Previous_ChangeReq_Input;

	INT DrMn_Current_ChangeReq_Process_Input;
	INT DrMn_Previous_ChangeReq_Process_Input;
	
	INT DrMn_ChangeReq_Process_Execute;
	
	INT DrMn_bitIndex_Idle;
	INT DrMn_bitIndex_IdleContrl;
	INT DrMn_bitIndex_DyTo_EngTA;
	INT DrMn_bitIndex_DyRPM_EngTA;
	INT DrMn_bitIndex_DyTo_EngRPM;
	INT DrMn_bitIndex_DyRPM_EngTo;	
	
	REAL DrMn_Before_IdleContrl_OnTime;
	REAL DrMn_Before_DyTo_EngTA_OnTime;
	REAL DrMn_Before_DyRPM_EngTA_OnTime;
	REAL DrMn_Before_DyTo_EngRPM_OnTime;
	REAL DrMn_Before_DyRPM_EngTo_OnTime;
	
	INT DrMn_IdleContrl_ModeChangeReqConut;
	INT DrMn_DyTo_EngTA_ModeChangeReqConut;
	INT DrMn_DyRPM_EngTA_ModeChangeReqConut;
	INT DrMn_DyTo_EngRPM_ModeChangeReqConut;
	INT DrMn_DyRPM_EngTo_ModeChangeReqConut;

	! *** Drive manual Interlock variable declaration
	INT DrMn_Function_Enable;
	
	INT DrMn_MoChg_Interlock(5)(6);
	INT DrMn_MoChg_Interlock_MaxRowIndex
	INT DrMn_MoChg_Interlock_MaxColumnIndex
		DrMn_MoChg_Interlock_MaxRowIndex = 4
		DrMn_MoChg_Interlock_MaxColumnIndex = 5
	INT DrMn_MoChg_Interlock_ControlMode_SelectIndex;		
	INT DrMn_MoChg_Interlock_DriveMode_SelectIndex;
	
	INT DriveModeChangeCondition_IdleExcept;
	
	REAL DriveMode_MoChgCompleteTime;
RET

! ----------------------------------------------------------------------------------------------

sr_DriveManual_Valiable_Init:
	
	! *** Drive manual managing variable initialization		
	DrMn_ManualMode_Current_ChangeReq_Input = 0;
	DrMn_ManualMode_Previous_ChangeReq_Input = 0;
	DrMn_AutomaticMode_Current_ChangeReq_Input = 0;
	DrMn_AutomaticMode_Previous_ChangeReq_Input = 0;
	DrMn_RemoteMode_Current_ChangeReq_Input = 0;
	DrMn_RemoteMode_Previous_ChangeReq_Input = 0;

	DrMn_Current_ChangeReq_Process_Input = 0;
	DrMn_Previous_ChangeReq_Process_Input = 0;
	
	DrMn_ChangeReq_Process_Execute = 0;
	
	DrMn_bitIndex_Idle			= 1;
	DrMn_bitIndex_IdleContrl	= 0;
	DrMn_bitIndex_DyTo_EngTA	= 3;
	DrMn_bitIndex_DyRPM_EngTA	= 2;
	DrMn_bitIndex_DyTo_EngRPM 	= 5;
	DrMn_bitIndex_DyRPM_EngTo	= 4;	
	
	DrMn_Before_IdleContrl_OnTime = 0;
	DrMn_Before_DyTo_EngTA_OnTime = 0;
	DrMn_Before_DyRPM_EngTA_OnTime = 0;
	DrMn_Before_DyTo_EngRPM_OnTime = 0;
	DrMn_Before_DyRPM_EngTo_OnTime = 0;
	
	DrMn_IdleContrl_ModeChangeReqConut = 0;
	DrMn_DyTo_EngTA_ModeChangeReqConut = 0;
	DrMn_DyRPM_EngTA_ModeChangeReqConut = 0;
	DrMn_DyTo_EngRPM_ModeChangeReqConut = 0;
	DrMn_DyRPM_EngTo_ModeChangeReqConut = 0;
		
	g_DrMn_enum_Default = -1;
	g_DrMn_enum_Idle = 100;
	g_DrMn_enum_IdleContrl = 101;
	g_DrMn_enum_DyTo_EngTA = 102;
	g_DrMn_enum_DyRPM_EngTA = 103;
	g_DrMn_enum_DyTo_EngRPM = 104;
	g_DrMn_enum_DyRPM_EngTo = 105;
	
	g_DriveManual_Mode = g_DrMn_enum_Default;
	DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Default;
	DrMn_Previous_ChangeReq_Mode = 0;	

	! *** Drive manual Interlock variable initialization
	DrMn_Function_Enable = 0;
	DrMn_Function_Enable.(0) = 1; ! Idle enable
	DrMn_Function_Enable.(1) = 1; ! Idle Contrl disable
	DrMn_Function_Enable.(2) = 1; ! DyTo_EngTA enable
	DrMn_Function_Enable.(3) = 1; ! DyRPM_EngTA enable
	DrMn_Function_Enable.(4) = 1; ! DyTo_EngRPM enable
	DrMn_Function_Enable.(5) = 1; ! DyRPM_EngTo enable
	
	FILL(0, DrMn_MoChg_Interlock);
	!			Ible	Idle Control	Dyno To/Eng TA		Dyno rpm/Eng TA		Dyno To/Eng Rpm		Dyno rpm/Eng To
	!Monitor	0		0				0					0					0					0
	!Service	1		1				1					1					1					1
	!Manual		1		1				1					1					1					1
	!Auto		1		0				1					1					1					1
	
	
	!FILL(1, DrMn_MoChg_Interlock, 0, 3, 0, 0); ! All 0
	!FILL(1, DrMn_MoChg_Interlock, 1, 1, 0, 0); ! Service
	FILL(1, DrMn_MoChg_Interlock, 2, 2, 0, 5); ! Manual		
	FILL(1, DrMn_MoChg_Interlock, 3, 3, 0, 5); ! Auto		
	FILL(1, DrMn_MoChg_Interlock, 4, 4, 0, 5); ! Remote		
	
	DriveModeChangeCondition_IdleExcept = 0;
	DriveMode_MoChgCompleteTime = 0;
RET

! ----------------------------------------------------------------------------------------------

sr_InputRepresh_SystemCommand:
	BLOCK
		Sys_Current_Command_Input = 0;	
		
		Sys_Current_Command_Input.(Sys_refreshBitIndex_Dyno_On) = g_HMI_DI_DynoOn_Button; !g_eCAT_Input_Data(0)(0x10).(4)
		IF (g_ControlMode <> g_CtMo_enum_Automatic)
			Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_IG_On) = g_HMI_DI_EngIGOn_Button;
		ELSE
			Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_IG_On) = g_Automatic_Eng_IG_On;
		END
		Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_Preheat_On) = g_HMI_DI_EngPreheatOn_Button;
		Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_Start) = g_HMI_DI_EngStart_Button;
		Sys_Current_Command_Input.(Sys_refreshBitIndex_Stop) = g_HMI_DI_Stop_Button;
		Sys_Current_Command_Input.(Sys_refreshBitIndex_EMS) = g_DI_EMSButton ! Safety Controller
		if (DI_Reset_Button=1)
			Sys_Current_Command_Input.(Sys_refreshBitIndex_Reset) = DI_Reset_Button;
			DI_Reset_Button = 0;
		else
			Sys_Current_Command_Input.(Sys_refreshBitIndex_Reset) = g_HMI_DI_Reset_Button;
		end
		
		! emoSWITCH Pressed, Reset Unable
		if (OpBox_EmoSwitch_PreviousStatus<>1)
			Sys_Current_Command_Input.(Sys_refreshBitIndex_Reset) = 0;
			g_Hmi_Banner_MsgLineNum = 20; g_Bf8_AutoR_Banner_Display = 1;
		END
		Sys_Current_Command_Input.(Sys_refreshBitIndex_Misc) = g_HMI_DI_Misc_Button;
		Sys_Current_Command_Input.(Sys_refreshBitIndex_Seant) = g_HMI_DI_Seant_Button ! Safety Controller
	END
RET

! ----------------------------------------------------------------------------------------------

sr_SystemCommand_Process:
	
	CALL sr_InputRepresh_SystemCommand;
	BLOCK
	IF(Sys_Current_Command_Input <> Sys_Previous_Command_Input)			
		
		! ----------------------------------------------------------------------------------------------
		! *** Engine Ignition On/Off Command 
		IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Seant) <> Sys_Previous_Command_Input.(Sys_refreshBitIndex_Seant))		
			! *** Input Check (Ignition On or Off) 		
			IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Seant) = 1)
				! *** Error Check.  
				IF(g_Error_Exists_Bit <> 0)	| (g_ControlMode <> g_CtMo_enum_Manual & g_ControlMode <> g_CtMo_enum_Automatic & g_ControlMode <> g_CtMo_enum_Remote)	| (g_SystemState_Misc_On = 0)
					IF(g_Error_Exists_Bit <> 0)
						g_Hmi_Banner_MsgLineNum = 25; g_Bf8_AutoR_Banner_Display = 1;
					ELSE
						IF(g_Error_Exists_Bit <> 0)
							g_Hmi_Banner_MsgLineNum = 6; g_Bf8_AutoR_Banner_Display = 1;
						END
					END
				ELSE						
					! *** Engine Ignition On/Off State Cheange Off >> On
					IF(g_DO_Seant_On = 0)
					
						! *** MMI Message Send
						IF (g_Buffer3_displayCmdLevel <> 0); 
							DISP "[#%d buffer] [cmp]Autoroutine; Seant On!", BufferProgrmaNumber; 
						END;
						
						! *** Dyno Seant On
						g_DO_Seant_On = 1;
						g_HMI_DO_Seant_Lamp = 1;
						
					! *** Engine Ignition On/Off State Cheange On >> Off				
					ELSEIF(g_DO_Seant_On = 1)
						
						! *** MMI Terminal display		
						IF (g_Buffer3_displayCmdLevel <> 0); 
							DISP "[#%d buffer] [req] Autoroutine; Seant Off!", BufferProgrmaNumber; 
						END;
						
						! *** Dyno Reset Off
						g_DO_Seant_On = 0;
						g_HMI_DO_Seant_Lamp = 0;
						
					END !IF(g_SystemState_Eng_IG_On = 0)
				END !IF(g_Error_Exists_Bit <> 0)		
			END ! IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_IG_On) = 1)					
					
		END ! IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_IG_On) <> Sys_Previous_Command_Input.(Sys_refreshBitIndex_Eng_IG_On))		
			
		! ----------------------------------------------------------------------------------------------
		! *** Reset On/Off Command 
		IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Reset) <> Sys_Previous_Command_Input.(Sys_refreshBitIndex_Reset))		
			
			! *** Input Check (Dynamometer On or Off) 		
			IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Reset) = 1)
				
				! *** MMI Terminal display		
				IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Autoroutine; O/P Reset On!", BufferProgrmaNumber; END;
				
							
				g_SystemState_Reset = 1;
				
				! *** Safety Relay Reset On 	
				g_DO_X11PResetOnSignal = 1;
				
				! *** Dyno Reset On
				g_DO_DynoResetSignal = 1;
				g_HMI_DO_HornOff_Lamp = 0;
				g_DO_HornSignal = 0;
				DAT_HORN_SILENT_ON = 0;

				! *** Banner message display.(EMO Switch On) 								
				g_Hmi_Banner_MsgLineNum = 21; g_Bf8_AutoR_Banner_Display = 1;			
				
				! *** Alarm Register
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_SysCMD_Reset_ReqOn;		
				g_Alarm_Register_Request_Bit = 1;
				
				! *** MMI Terminal display		
				!IF (g_Buffer3_displayCmdLevel <> 0);  
					DISP "[#%d buffer] [cmp]Autoroutine; O/P Reset On!", BufferProgrmaNumber; 
				!END;
			ELSE
				! *** MMI Terminal display		
				!IF (g_Buffer3_displayCmdLevel <> 0); 
					DISP "[#%d buffer] [req] Autoroutine; O/P Reset Off!", BufferProgrmaNumber; 
				!END;
	
				g_SystemState_Reset = 0;
							
				! *** Safety Relay Reset Off 	
				g_DO_X11PResetOnSignal = 0;
				
				! *** Dyno Reset Off
				g_DO_DynoResetSignal = 0;
				
				! *** Alarm Register
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_SysCMD_Reset_ReqOff;		
				g_Alarm_Register_Request_Bit = 1;
				
				! *** MMI Terminal display		
				IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp]Autoroutine; O/P Reset Off!", BufferProgrmaNumber; END;
				
			END ! IF(Sys_Current_Command_Input.(g_SystemState_Reset) = 1)
		END ! IF(Sys_Current_Command_Input.(g_SystemState_Reset) <> Sys_Previous_Command_Input.(g_SystemState_Reset))		
		
		! ----------------------------------------------------------------------------------------------
		
		! *** Dynamometer On/Off Command 
		IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Dyno_On ) <> Sys_Previous_Command_Input.(Sys_refreshBitIndex_Dyno_On))		
			! *** Input Check (Dynamometer On or Off) 		
			IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Dyno_On) = 1)
				
				! *** Error Check.
				IF(g_Error_Exists_Bit <> 0)|(g_ControlMode <> g_CtMo_enum_Manual)|(g_SystemState_Misc_On = 0)
					IF(g_Error_Exists_Bit <> 0)
						g_Hmi_Banner_MsgLineNum = 25; g_Bf8_AutoR_Banner_Display = 1;
					ELSE 
						IF (g_ControlMode <> g_CtMo_enum_Manual)
							g_Hmi_Banner_MsgLineNum = 6; g_Bf8_AutoR_Banner_Display = 1;
						END
					END
				ELSE
					! *** Dynamometer On/Off State Cheange Off >> On
					IF(g_SystemState_Dyno_On = 0)
			
						! *** MMI Message Send						
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Subroutine; Dyno. On", BufferProgrmaNumber; END;
											
						CALL sr_Dyno_Control_On_Command_Processing;
						
						! *** Command Result Check
						IF(Dyno_On_Cmd_Processing_Complete = 1)		
							g_SystemState_Dyno_On = 1;	
							
							! *** Engine Stated Check & Drive Mode Change
							IF(g_SystemState_Eng_Started = 1)	
								DrMn_Current_ChangeReq_Mode = g_DrMn_enum_IdleContrl;	
								DrMn_Previous_ChangeReq_Mode = 0;									
							END		
						ELSE 
							! *** Alarm Register
							g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_E_SysCMD_Dyno_On_Fail ;		
							g_Alarm_Register_Request_Bit = 1;
							! *** HMI Banner
							g_Hmi_Banner_MsgLineNum = 1; g_Bf8_AutoR_Banner_Display = 1;
						END
						
						! *** MMI Message Send									
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Dyno. Control On CMD = <%d>", BufferProgrmaNumber, g_SystemState_Dyno_On; END;
						
					! *** Dynamometer On/Off State Cheange On >> Off				
					ELSEIF(g_SystemState_Dyno_On = 1)
						
						! *** MMI Message Send						
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Subroutine; Dyno. Off", BufferProgrmaNumber; END;
											
						CALL sr_Dyno_Control_Off_Command_Processing;
						
						! *** Command Result Check
						IF(Dyno_Off_Cmd_Processing_Complete = 1)		
							g_SystemState_Dyno_On = 0;
							
							! *** Engine Stated Check & Drive Mode Change
							IF(g_SystemState_Eng_Started = 1)								
								DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Idle;
								DrMn_Previous_ChangeReq_Mode = 0;	
							ELSE
								! *** Dyno Off. Control Mode Monitor.
								!g_ControlMode = g_CtMo_enum_Monitor;
								
								! *** Dyno Off. Drive Mode Reset.
								DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Default;
								DrMn_Previous_ChangeReq_Mode = 0;	
							END	
							
						ELSE 
							! *** Alarm Register
							g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_E_SysCMD_Dyno_Off_Fail;		
							g_Alarm_Register_Request_Bit = 1;
						END
						
						! *** MMI Message Send						
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Dyno. Control Off CMD = <%d>", BufferProgrmaNumber, g_SystemState_Dyno_On; END;
						
					END !IF(g_SystemState_Dyno_On = 0)					
				END !IF(g_Error_Exists_Bit <> 0)	 	
			END ! IF(Sys_Current_Command_Input.(Sys_inputBitIndex_Dyno_On) = 1)
		END ! IF(Sys_Current_Command_Input.(Sys_inputBitIndex_Dyno_Ctrl_On) <> Sys_Previous_Command_Input.(Sys_inputBitIndex_Dyno_Ctrl_On))		
		
		! ----------------------------------------------------------------------------------------------
		
		! *** Engine Ignition On/Off Command 
		IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_IG_On) <> Sys_Previous_Command_Input.(Sys_refreshBitIndex_Eng_IG_On))		
			! *** Input Check (Ignition On or Off) 		
			IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_IG_On) = 1)
				! *** Error Check.  
				IF(g_Error_Exists_Bit <> 0)	| (g_ControlMode <> g_CtMo_enum_Manual & g_ControlMode <> g_CtMo_enum_Automatic & g_ControlMode <> g_CtMo_enum_Remote)	| (g_SystemState_Misc_On = 0)
					IF(g_Error_Exists_Bit <> 0)
						g_Hmi_Banner_MsgLineNum = 25; g_Bf8_AutoR_Banner_Display = 1;
					ELSE
						IF(g_Error_Exists_Bit <> 0)
							g_Hmi_Banner_MsgLineNum = 6; g_Bf8_AutoR_Banner_Display = 1;
						END
					END
				ELSE						
					! *** Engine Ignition On/Off State Cheange Off >> On
					IF(g_SystemState_Eng_IG_On = 0)
					
						! *** MMI Message Send
						IF (g_Buffer3_displayCmdLevel <> 0); 
							DISP "[#%d buffer] [req] Subroutine; Engine Ignition On 2", BufferProgrmaNumber; 
						END;
						
						! *** Engine Alpha Zero position set
						g_AO_AlphaASignal = g_unit_Cmd_Alpha_1_0Pct_ZeroOffset_DaVal + g_unit_Cmd_Alpha_1_0Pct_DaVal;
						g_AO_AlphaBSignal = g_unit_Cmd_Alpha_2_0Pct_ZeroOffset_DaVal + g_unit_Cmd_Alpha_2_0Pct_DaVal;
						
						
						IF((g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt >= 0) & (g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt >= 0))
							g_DO_ThrottleIVSSignal = 0;
						END !IF((g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt >= 0) & (g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt >= 0))							
						
						! *** Ignition On CMD Excute
						g_DO_IgnitionSignal = 1;
						
						! *** Check Delay Time for Dyno On State
						WAIT(10);
						
						! *** Ignition On CMD Check & Result Update
						IF(g_DO_IgnitionSignal = 1)					
							g_SystemState_Eng_IG_On = 1;
							g_PC_Write_Data(0x120) = g_SystemState_Eng_IG_On
							!g_DO_IgnitionSignal = 0
						ELSE						
							! *** Alarm Register
							g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_E_SysCMD_IG_Off_Fail;		
							g_Alarm_Register_Request_Bit = 1;
						END	
						
						! *** MMI Message Send			
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Engine Ignition On CMD = <%d>", BufferProgrmaNumber, g_DO_IgnitionSignal; END;
						
					! *** Engine Ignition On/Off State Cheange On >> Off				
					ELSEIF(g_SystemState_Eng_IG_On = 1)
						
						! *** MMI Message Send
						!IF (g_Buffer3_displayCmdLevel <> 0); 
						DISP "[#%d buffer] [req] Subroutine; Engine Ignition Off", BufferProgrmaNumber; 
						!END;
						
						! *** Engine Alpha Zero position set
						g_AO_AlphaASignal = g_unit_Cmd_Alpha_1_0Pct_ZeroOffset_DaVal + g_unit_Cmd_Alpha_1_0Pct_DaVal;
						g_AO_AlphaBSignal = g_unit_Cmd_Alpha_2_0Pct_ZeroOffset_DaVal + g_unit_Cmd_Alpha_2_0Pct_DaVal;
						
						IF((g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt >= 0) & (g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt >= 0))
							g_DO_ThrottleIVSSignal = 0;
						END !IF((g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt >= 0) & (g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt >= 0))							
						
						
						! *** Ignition Off CMD Excute
						!g_DO_IgnitionSignal = 0;
						
						! *** Check Delay Time for Dyno On State
						!WAIT(10);
										
						! *** Ignition Off CMD Check & Result Update
						g_SystemState_Eng_IG_On = 0;
						g_SystemState_Eng_Started = 0;
						g_PC_Write_Data(0x120) = g_SystemState_Eng_IG_On
						!g_DriveManual_Mode = g_DrMn_enum_Default;
						!DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Default;
						!DrMn_Previous_ChangeReq_Mode = 0;
						g_DO_IgnitionSignal = 0;
						
						IF(g_SystemState_Dyno_On = 0 & (g_DriveManual_Mode = g_DrMn_enum_Idle | g_DriveManual_Mode = g_DrMn_enum_IdleContrl))								
							DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Default;
							DrMn_Previous_ChangeReq_Mode = 0;	
						END	

						! *** MMI Message Send		
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Engine Ignition Off CMD = <%d>", BufferProgrmaNumber, g_DO_IgnitionSignal; END;
						
					END !IF(g_SystemState_Eng_IG_On = 0)
				END !IF(g_Error_Exists_Bit <> 0)		
				g_Automatic_Eng_IG_On = 0;
			END ! IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_IG_On) = 1)					
					
		END ! IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_IG_On) <> Sys_Previous_Command_Input.(Sys_refreshBitIndex_Eng_IG_On))		
			
		! ----------------------------------------------------------------------------------------------
		
		! *** Engine Preheat On/Off Command 
		IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_Preheat_On) <> Sys_Previous_Command_Input.(Sys_refreshBitIndex_Eng_Preheat_On))		
			! *** Input Check (Engine Preheat On or Off) 		
			IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_Preheat_On) = 1)
				! *** Error Check.
				IF(g_Error_Exists_Bit <> 0)					
					g_Hmi_Banner_MsgLineNum = 25; g_Bf8_AutoR_Banner_Display = 1;
				ELSE
					! *** Control Mode check to be manual. if control mode is not manual, error banner message display.
					IF(g_ControlMode <> g_CtMo_enum_Manual)					
						g_Hmi_Banner_MsgLineNum = 6; g_Bf8_AutoR_Banner_Display = 1;
					ELSE
						! *** Engine Ignition On Check
						IF(g_SystemState_Eng_IG_On = 0)
							! *** HMI Banner
							g_Hmi_Banner_MsgLineNum = 3; g_Bf8_AutoR_Banner_Display = 1;
						ELSE	
							! *** Engine Started Check
							IF(g_SystemState_Eng_Started = 1)
								! *** engine is already stated! Banner display.
								g_Hmi_Banner_MsgLineNum = 5; g_Bf8_AutoR_Banner_Display = 1;
							ELSE
								! *** Engine Preheat On/Off State Cheange Off >> On
								IF(g_SystemState_Eng_Preheat_On = 0)
								
									! *** MMI Message Send
									IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Subroutine; Engine Preheat On", BufferProgrmaNumber; END;
															
									! *** Engine Preheat On CMD Excute
									g_DO_PreHeatSignal = 1;
									
									! *** Check Delay Time for Dyno On State
									WAIT(10);
									
									! *** Engine Preheat On CMD Check & Result Update
									IF(g_DO_PreHeatSignal = 1)					
										g_SystemState_Eng_Preheat_On = 1;		
										
										Engine_Preheat_SwitchOnTimeMeasure_1 = TIME;
										Engine_Preheat_SwitchOnTimeMeasure_2 = 0;
									ELSE
										! *** Alarm Register
										g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_E_SysCMD_Engine_PreheatOn_Fail;		
										g_Alarm_Register_Request_Bit = 1;
									END	
									! *** MMI Message Send		
									IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Engine Preheat On CMD = <%d>", BufferProgrmaNumber, g_SystemState_Eng_Preheat_On; END;
									
								! *** Engine Preheat On/Off State Cheange On >> Off				
								ELSEIF(g_SystemState_Eng_Preheat_On = 1)
									
									! *** MMI Message Send
									IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Subroutine; Engine Preheat Off", BufferProgrmaNumber; END;
														
									! *** Engine Preheat Off CMD Excute
									g_DO_PreHeatSignal = 0;
									
									! *** Check Delay Time for Dyno On State
									WAIT(10);
									
									! *** Engine Preheat Off CMD Check & Result Update
									IF(g_DO_PreHeatSignal = 0)					
										g_SystemState_Eng_Preheat_On = 0;						
									ELSE
										! *** Alarm Register
										g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_E_SysCMD_Engine_PreheatOff_Fail;		
										g_Alarm_Register_Request_Bit = 1;
									END	
									
									! *** MMI Message Send		
									IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Engine Preheat Off CMD = <%d>", BufferProgrmaNumber, g_DO_PreHeatSignal; END;
									
								END !IF(g_SystemState_Eng_IG_On = 0)
							END !IF(g_SystemState_Eng_Start = 1)
						END !IF(g_SystemState_Eng_IG_On = 0)						
					END !IF(g_ControlMode <> g_CtMo_enum_Manual)
				END !IF(g_Error_Exists_Bit <> 0)		
			END ! IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_IG_On) = 1)
		END ! IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_IG_On) <> Sys_Previous_Command_Input.(Sys_refreshBitIndex_Eng_IG_On))		
		
		! ----------------------------------------------------------------------------------------------
		
				! *** Engine Preheat On/Off Command 
		IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Misc) <> Sys_Previous_Command_Input.(Sys_refreshBitIndex_Misc))		
			! *** Input Check (Engine Preheat On or Off) 		
			IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Misc) = 1)
				! *** Error Check.
				IF(g_Error_Exists_Bit <> 0)					
					g_Hmi_Banner_MsgLineNum = 25; g_Bf8_AutoR_Banner_Display = 1;
				ELSE
					! *** Control Mode check to be manual. if control mode is not manual, error banner message display.
					IF(g_ControlMode <> g_CtMo_enum_Manual)					
						g_Hmi_Banner_MsgLineNum = 6; g_Bf8_AutoR_Banner_Display = 1;
					ELSE
						! *** Engine Ignition On Check 20170414
						IF(g_SystemState_Eng_IG_On = 1)
							! *** HMI Banner
							g_Hmi_Banner_MsgLineNum = 3; g_Bf8_AutoR_Banner_Display = 1;
						ELSE	
							! *** Engine Started Check 20170414
							IF(g_SystemState_Dyno_On = 1)
								! *** engine is already stated! Banner display.
								g_Hmi_Banner_MsgLineNum = 5; g_Bf8_AutoR_Banner_Display = 1;
							ELSE
								! *** Engine Preheat On/Off State Cheange Off >> On
								IF(g_SystemState_Misc_On = 0)
								
									! *** MMI Message Send
									IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Subroutine; Misc On", BufferProgrmaNumber; END;
									g_DO_FANSignal = 1;

									! *** Engine Preheat On CMD Excute
									g_DO_BKReadySignal = 1;
									
									! *** Check Delay Time for Dyno On State
									g_Flashing_Misc_On = 1;
									LOOP 10
										WAIT(500);
										IF g_DI_DynoOn_FeedBack
											goto SUCCESS;
										END
									END
									
									SUCCESS:
									g_Flashing_Misc_On = 0;
									
									! *** Engine Preheat On CMD Check & Result Update
									IF(g_DI_DynoOn_FeedBack = 1)					
										g_SystemState_Misc_On = 1;
										!g_DO_BKReadySignal = 0;
										!WAIT(100);
										!DI_Reset_Button = 1;
									ELSE
										g_DO_FANSignal = 0;
										g_DO_BKReadySignal = 0;
										! *** Alarm Register
										g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_E_SysCMD_Engine_PreheatOn_Fail;		
										g_Alarm_Register_Request_Bit = 1;
									END	
									! *** MMI Message Send		
									IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Misc On CMD = <%d>", BufferProgrmaNumber, g_SystemState_Eng_Preheat_On; END;
									
								! *** Engine Preheat On/Off State Cheange On >> Off				
								ELSEIF(g_SystemState_Misc_On = 1)
									
									! *** MMI Message Send
									IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Subroutine; Misc Off", BufferProgrmaNumber; END;
														
									! *** Engine Preheat Off CMD Excute
									g_DO_BKReadySignal = 0;
									
									! *** Check Delay Time for Dyno On State
									g_Flashing_Misc_On = 1;
									LOOP 10
										WAIT(500);
										IF ^g_DI_DynoOn_FeedBack
											goto SUCCESS1;
										END
									END
									SUCCESS1:
									g_Flashing_Misc_On = 0;
									
									! *** Engine Preheat Off CMD Check & Result Update
									IF(g_DI_DynoOn_FeedBack = 0)					
										!g_DO_BKReadySignal = 0;
										g_SystemState_Misc_On = 0;
										g_DO_FANSignal = 0;
									ELSE
										! *** Alarm Register
										g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_E_SysCMD_Engine_PreheatOff_Fail;		
										g_Alarm_Register_Request_Bit = 1;
									END	
									
									! *** MMI Message Send		
									IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Misc Off CMD = <%d>", BufferProgrmaNumber, g_DO_BKReadySignal; END;
									
								END !IF(g_SystemState_Eng_IG_On = 0)
							END !IF(g_SystemState_Eng_Start = 1)
						END !IF(g_SystemState_Eng_IG_On = 0)						
					END !IF(g_ControlMode <> g_CtMo_enum_Manual)
				END !IF(g_Error_Exists_Bit <> 0)		
			END ! IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_IG_On) = 1)

		END ! IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_IG_On) <> Sys_Previous_Command_Input.(Sys_refreshBitIndex_Eng_IG_On))		
		
		! *** Engine Start Command Change
		IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_Start) <> Sys_Previous_Command_Input.(Sys_refreshBitIndex_Eng_Start))		
			! *** Input Check (Engine Start On, Stop Command = 0ff) 		
			IF((Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_Start) = 1) & (Sys_Current_Command_Input.(Sys_refreshBitIndex_Stop) = 0))
				
				! *** Error Check.
				IF(g_Error_Exists_Bit <> 0)					
					g_Hmi_Banner_MsgLineNum = 25; g_Bf8_AutoR_Banner_Display = 1;
				ELSE
					! *** Control Mode check to be manual. if control mode is not manual, error banner message display.
					IF(g_ControlMode <> g_CtMo_enum_Manual)					
						g_Hmi_Banner_MsgLineNum = 6; g_Bf8_AutoR_Banner_Display = 1;
					ELSE	
						! *** Engine Ignition On Check
						IF(g_SystemState_Eng_IG_On = 0)
							! *** HMI Banner
							g_Hmi_Banner_MsgLineNum = 3; g_Bf8_AutoR_Banner_Display = 1;
						ELSE
							
							! *** Engine Start State Cheange Off >> On
							IF(g_SystemState_Eng_Started = 0)
							
								! *** MMI Message Send
								IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Subroutine; Engine Start On", BufferProgrmaNumber; END;
								
								! *** Engine Alpha Zero position set
								g_AO_AlphaASignal = g_unit_Cmd_Alpha_1_0Pct_DaVal;
								g_AO_AlphaBSignal = g_unit_Cmd_Alpha_2_0Pct_DaVal;
								
								
								IF((g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt >= 0) & (g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt >= 0))
									g_DO_ThrottleIVSSignal = 0;
								END !IF((g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt >= 0) & (g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt >= 0))							


								! *** Engine Start On CMD Excute
								g_DO_StartSignal = 1;
								
								! *** Check Delay Time for Dyno On State
								WAIT(100);
								
								! *** Engine Start On CMD Check & Result Update
								IF(g_DO_StartSignal = 1)					
									g_SystemState_Eng_Starting = 1;	
									g_SystemState_Eng_Started = 0;								
									g_stop_StopMode = g_stop_enum_Default;
				
									Engine_Start_SwitchOnTimeMeasure_1 = TIME;
									Engine_Start_SwitchOnTimeMeasure_2 = 0;
								ELSE
									! *** Alarm Register
									g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_E_SysCMD_Engine_StartCmdOn_Fail;;		
									g_Alarm_Register_Request_Bit = 1;
								END					
								
								! *** MMI Message Send		
								IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Engine Start On CMD = <%d>", BufferProgrmaNumber, g_DO_StartSignal; END;
							ELSE 
								! *** engine is already stated! Banner display.
								g_Hmi_Banner_MsgLineNum = 5; g_Bf8_AutoR_Banner_Display = 1;
							END !IF(g_SystemState_Eng_Start = 0)
						END ! IF(g_SystemState_Eng_IG_On = 0)	
					END !IF(g_ControlMode <> g_CtMo_enum_Manual)		
				END !IF(g_Error_Exists_Bit <> 0)	
			ELSEIF((Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_Start) = 1) & (Sys_Current_Command_Input.(Sys_refreshBitIndex_Stop) = 1))
				
				! *** MMI Message Send
				IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Engine Start And Stop On Error", BufferProgrmaNumber; END;
				
				! *** Alarm Register
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_SysCMD_Engine_StartStopSameTimeSwOn;		
				g_Alarm_Register_Request_Bit = 1;				
				
				! *** MMI Message Send		
				IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Engine / Start Switch = <%d>, Stop Switch = <%d>", BufferProgrmaNumber, Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_Start), Sys_Current_Command_Input.(Sys_refreshBitIndex_Stop); END;
					
			END ! IF((Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_Start) = 1) & (Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_Stop) = 0))
			
		END ! IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_Start) <> Sys_Previous_Command_Input.(Sys_refreshBitIndex_Eng_Start))			
				
	END ! IF(Sys_Current_Command_Input <> Sys_Previous_Command_Input)
		
	! ----------------------------------------------------------------------------------------------
	
	! *** Time-out Check 		
	
	! *** Engine Preheat On Time-out Check 		
	IF(((Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_Preheat_On) = 1) & (g_SystemState_Eng_Preheat_On = 1)) | (g_DO_PreHeatSignal = 1))
	
		Engine_Preheat_SwitchOnTimeMeasure_2 = TIME;
		IF((Engine_Preheat_SwitchOnTimeMeasure_2 - Engine_Preheat_SwitchOnTimeMeasure_1)/1000 > g_Hmi_bs3003_Eng_PreheatTimeSt)
			! *** MMI Message Send
			IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Subroutine; Engine Preheat Time-Out Off", BufferProgrmaNumber; END;
								
			! *** Engine Preheat Off CMD Excute
			g_DO_PreHeatSignal = 0;
			
			! *** Check Delay Time for Dyno On State
			WAIT(100);
			
			! *** Engine Preheat Off CMD Check & Result Update
			IF(g_DO_PreHeatSignal = 0)					
				g_SystemState_Eng_Preheat_On = 0;						
			ELSE
				! *** Alarm Register
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_E_SysCMD_Engine_PreheatTOutOff_Fail;;		
				g_Alarm_Register_Request_Bit = 1;					
			END	
			
			! *** MMI Message Send		
			IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Engine Preheat Preheat Time-Out Off CMD = <%d>", BufferProgrmaNumber, g_DO_PreHeatSignal; END;
		END !IF((Engine_Preheat_SwitchOnTimeMeasure_2 - Engine_Preheat_SwitchOnTimeMeasure_1)/1000 > g_Hmi_bs3003_Eng_PreheatTimeSt)
	END !IF((Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_Preheat_On) = 1) & (g_SystemState_Eng_Preheat_On = 1))
	
	! ----------------------------------------------------------------------------------------------

	! *** Engine Start On Time-out Check 	
	IF(((Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_Start) = 1) & (g_SystemState_Eng_Starting = 1)) | (g_DO_StartSignal = 1))
	
		Engine_Start_SwitchOnTimeMeasure_2  = TIME;
		IF(((Engine_Start_SwitchOnTimeMeasure_2 - Engine_Start_SwitchOnTimeMeasure_1)/1000 > g_Hmi_bs3002_Eng_StartTimeOutSt) | (Sys_Current_Command_Input.(Sys_refreshBitIndex_Stop) = 1))
			! *** MMI Message Send
			!IF (g_Buffer3_displayCmdLevel <> 0); 
			DISP "[#%d buffer] [req] Subroutine; Engine Start Time-Out Off", BufferProgrmaNumber; 
			!END;
								
			! *** Engine Start Off CMD Excute
			g_DO_StartSignal = 0;
			
			! *** Check Delay Time for Dyno On State
			WAIT(10);
			DISP "[#%d buffer] [req] %d ; %d", BufferProgrmaNumber,g_Feedback_Speed,g_Hmi_bs3002_Eng_StartCompSpeedSt; 

			! *** Engine Start Off CMD Check & Result Update g_Feedback_Speed
			IF(( g_AI_Feedback_DynoSpeed>= g_Hmi_bs3002_Eng_StartCompSpeedSt) & (g_SystemState_Eng_Starting = 1) & (g_DO_StartSignal = 0))					
				g_stop_StopMode = g_stop_enum_Default;	
				
				! *** Dyno On State Check & Drive Mode Change
				IF(g_SystemState_Dyno_On = 1)	
					g_DriveManual_Mode = g_DrMn_enum_IdleContrl;
					DrMn_Current_ChangeReq_Mode = g_DrMn_enum_IdleContrl;
					DrMn_Previous_ChangeReq_Mode = 0;	
				ELSE					
					DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Idle;
					DrMn_Previous_ChangeReq_Mode = 0;	
				END	
				WAIT(100);
				DISP "[#%d buffer] [req] Subroutine; Engine Started", BufferProgrmaNumber; 

				g_SystemState_Eng_Starting = 0;
				g_SystemState_Eng_Started = 1;
			ELSE
				! *** Engine Ignition Off
				g_DO_IgnitionSignal = 0;
				g_SystemState_Eng_IG_On = 0;
			
				g_SystemState_Eng_Starting = 0;		
				g_SystemState_Eng_Started = 0;	
				g_stop_StopMode = g_stop_enum_Stopped;
				
				! *** Engine Start fail. Drive Mode Change to default.
				g_DriveManual_Mode = g_DrMn_enum_Default;
				DrMn_Current_ChangeReq_Mode  = g_DrMn_enum_Default;
				DrMn_Previous_ChangeReq_Mode = 0;	
				
				! *** Alarm Register
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_E_SysCMD_Engine_StartCmdTOutOff_Fail;		
				g_Alarm_Register_Request_Bit = 1;
			END					
			
			! *** MMI Message Send			
			IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Engine Start Time-Out Off CMD = <%d>", BufferProgrmaNumber, g_DO_StartSignal; END;
		END !IF((Engine_Preheat_SwitchOnTimeMeasure_2 - Engine_Preheat_SwitchOnTimeMeasure_1)/1000 > g_Hmi_bs3003_Eng_PreheatTimeSt)
	END !IF((Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_Preheat_On) = 1) & (g_SystemState_Eng_Preheat_On = 1))
	
	! ----------------------------------------------------------------------------------------------

	! *** Engine Speed is Engine Start Speed. Stop Engien Start.		
	IF((g_Feedback_Speed >= g_Hmi_bs3002_Eng_StartCompSpeedSt) & (g_SystemState_Eng_Starting = 1)) ! & (g_DO_StartSignal = 1))
	
		! *** MMI Message Send
		!IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Subroutine; F/B Engine Speed > Start Speed", BufferProgrmaNumber; END;
		!IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] F/B Speed=<%d>, Start Speed=<%d>", BufferProgrmaNumber, g_Feedback_Speed, g_Hmi_bs3004_Eng_IdleMinSpeedSt;END;
		DISP "[#%d buffer] [req] Subroutine; F/B Engine Speed > Start Speed", BufferProgrmaNumber;					
		! *** Engine Start Off CMD Excute
		g_DO_StartSignal = 0;
		
		! *** Check Delay Time for Dyno On State
		WAIT(10);
		
		! *** Engine Start CMD Off & Engine start condition check 
		IF(g_DO_StartSignal = 0)					
			g_stop_StopMode = g_stop_enum_Default;
			
			! *** Dyno On State Check & Drive Mode Change
			IF(g_SystemState_Dyno_On = 1)				
				DrMn_Current_ChangeReq_Mode  = g_DrMn_enum_IdleContrl;
				DrMn_Previous_ChangeReq_Mode = 0;	
				g_DriveManual_Mode = g_DrMn_enum_IdleContrl;
				CALL sr_PARAMETER_INIT;
			ELSE				
				DrMn_Current_ChangeReq_Mode  = g_DrMn_enum_Idle;
				DrMn_Previous_ChangeReq_Mode = 0;	
			END	
			
			WAIT(100);
			g_SystemState_Eng_Starting = 0;		
			g_SystemState_Eng_Started = 1;		

		ELSE
			! *** Engine Ignition Off
			g_DO_IgnitionSignal = 0;
			g_SystemState_Eng_IG_On = 0;
			
			g_SystemState_Eng_Starting = 0;		
			g_SystemState_Eng_Started = 0;		
			g_stop_StopMode = g_stop_enum_Stopped;
			
			
			! *** Engine Start fail. Drive Mode Change to default.
			g_DriveManual_Mode = g_DrMn_enum_Default;
			DrMn_Current_ChangeReq_Mode	 = g_DrMn_enum_Default;
			DrMn_Previous_ChangeReq_Mode = 0;	
			
			! *** Alarm Register
			g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_E_SysCMD_Engine_StartCmdTSReachedOff_Fail;		
			g_Alarm_Register_Request_Bit = 1;
		END		
		
		! *** MMI Message Send			
		!IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Engine Stop On CMD = <%d>", BufferProgrmaNumber, g_eCAT_Output_Data(0)(0x61).3; END;
		
	END !IF((Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_Preheat_On) = 1) & (g_SystemState_Eng_Preheat_On = 1))
	
	! ----------------------------------------------------------------------------------------------

	! *** Engine Speed is Engine Start Speed. Stop Engien Start.		
	IF((g_Feedback_Speed >= g_Hmi_bs3002_Eng_StartCompSpeedSt) & (g_SystemState_Eng_IG_On = 1))
	
		! *** MMI Message Send
		IF (g_Buffer3_displayCmdLevel <> 0); 
			DISP "[#%d buffer] [req] Subroutine; Ignition On; F/B Engine Speed > Start Speed", BufferProgrmaNumber; 
		END;
		!IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] F/B Speed=<%d>, Start Speed=<%d>", BufferProgrmaNumber, g_Feedback_Speed, g_Hmi_bs3004_Eng_IdleMinSpeedSt;END;
		
		! *** Engine start condition check 
						
		!WAIT(100);

		g_SystemState_Eng_Starting = 0;		
		g_SystemState_Eng_Started = 1;		
		g_stop_StopMode = g_stop_enum_Default;	
		! *** MMI Message Send			
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Engine Stop On CMD = <%d>", BufferProgrmaNumber, g_DO_StartSignal; END;
		
	END !IF((Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_Preheat_On) = 1) & (g_SystemState_Eng_Preheat_On = 1))
	
	! ----------------------------------------------------------------------------------------------
			
	! *** EMS On/Off and Hard Stop Command 
	IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_EMS) <> Sys_Previous_Command_Input.(Sys_refreshBitIndex_EMS))&(g_SystemState_Misc_On=1)		
		DISP Sys_Current_Command_Input.(Sys_refreshBitIndex_EMS)
		! *** Input Check (EMS On) 		
		IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_EMS) = 1)
			
			! *** MMI Terminal display		
			IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] EMS Switch On or Safety Relay Fail!", BufferProgrmaNumber; END;
									
			g_SystemState_EMS = 1;
			
			! *** Engine Ignition Off CMD Excute
			!g_DO_IgnitionSignal = 1;
			!g_DO_BKReadySignal = 1;
			
			! *** Engine Start Off CMD Excute
			g_DO_StartSignal = 0;
			
			g_stop_StopMode = g_stop_enum_HardStopping;
				
			! *** Check Delay Time for Dyno On State
			WAIT(100);
		
			! *** Engine Ignition Off CMD Check & Result Update
			!IF(g_DO_IgnitionSignal = 1)					
				g_DO_IgnitionSignal = 0;
				g_DO_BKReadySignal = 0;
				g_DO_FANSignal = 0;
				g_SystemState_Eng_Starting = 0;	
				g_SystemState_Eng_Started = 0;	
				
				! *** Engine Stop. Control Mode Change to default.
				g_ControlMode = g_CtMo_enum_Manual;
				
				! *** Engine Stop. Drive Mode Change to default.				
				DrMn_Current_ChangeReq_Mode  = g_DrMn_enum_Default;
				DrMn_Previous_ChangeReq_Mode = 0;	
				! *** Engine Ignition Reset.
				g_SystemState_Eng_IG_On = 0;
				g_SystemState_Misc_On = 0;
			!ELSE	
				! *** Alarm Register
			!	g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_E_SysCMD_Engine_StopCmdOff_Fail;		
			!	g_Alarm_Register_Request_Bit = 1;			
			!END					
			
			! *** Banner message display.(EMO Switch On) 								
			g_Hmi_Banner_MsgLineNum = 20; g_Bf8_AutoR_Banner_Display = 1;
			
			! *** Alarm Register
			g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_SysCMD_EMS_ReqOn;		
			g_Alarm_Register_Request_Bit = 1;
			
			! *** MMI Terminal display		
			IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] EMS Switch On or Safety Relay Fail!", BufferProgrmaNumber; END;
		ELSE
			! *** MMI Terminal display		
			IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] EMS Switch On or Safety Relay Normal!", BufferProgrmaNumber; END;

			g_SystemState_EMS = 0;
				
			! *** Banner message display.(EMO Switch On) 								
			g_Hmi_Banner_MsgLineNum = 0; g_Bf8_AutoR_Banner_Display = 0;
			
			! *** Alarm Register
			g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_SysCMD_EMS_ReqOff;		
			g_Alarm_Register_Request_Bit = 1;
			
			! *** MMI Terminal display		
			IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp]Autoroutine; O/P Reset Normal!", BufferProgrmaNumber; END;
			
		END ! IF(Sys_Current_Command_Input.(g_SystemState_Reset) = 1)
	END ! IF(Sys_Current_Command_Input.(g_SystemState_Reset) <> Sys_Previous_Command_Input.(g_SystemState_Reset))

	! ----------------------------------------------------------------------------------------------
		
	! *** Engine Off(Soft Stop CMD)		
	!IF((Sys_Current_Command_Input.(Sys_refreshBitIndex_Stop) = 1) & (g_stop_StopMode <> g_stop_enum_Stopped))
	IF(Sys_Current_Command_Input.(Sys_refreshBitIndex_Stop) = 1)&(g_SystemState_Misc_On=1)
							
		! *** MMI Message Send
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Subroutine; Enging Off;Soft Stopping On", BufferProgrmaNumber; END;
							
		! *** Engine Ignition Off CMD Excute
		!g_DO_IgnitionSignal = 1;
		!g_DO_BKReadySignal = 1;
		
		! *** Engine Start Off CMD Excute
		g_DO_StartSignal = 0;
		
		g_stop_StopMode = g_stop_enum_SoftStopping;
			
		! *** Check Delay Time for Dyno On State
		!WAIT(100);
		
		! *** Engine Ignition Off CMD Check & Result Update

			g_DO_IgnitionSignal = 0;
			g_DO_BKReadySignal = 0;
			g_DO_FANSignal = 0;
			g_SystemState_Eng_Starting = 0;	
			g_SystemState_Eng_Started = 0;	
			
			! *** Engine Stop. Control Mode Change to default.
			g_ControlMode = g_CtMo_enum_Manual;
			
			! *** Engine Stop. Drive Mode Change to default.
			DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Default;
			DrMn_Previous_ChangeReq_Mode = 0;	
			
			! *** Engine Ignition Reset.
			g_SystemState_Eng_IG_On = 0;
			g_SystemState_Misc_On = 0;

		
		! *** MMI Message Send			
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Soft Stopping On CMD = <%d>", BufferProgrmaNumber, g_stop_StopMode; END;
		
		! ----------------------------------------------------------------------------------------------

		! *** MMI Message Send						
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Subroutine; Enging Off; Dyno. Off", BufferProgrmaNumber; END;
							
		CALL sr_Dyno_Control_Off_Command_Processing;
		
		! *** Command Result Check
		IF(Dyno_Off_Cmd_Processing_Complete = 1)		
			g_SystemState_Dyno_On = 0;
			
			! *** Engine Stated Check & Drive Mode Change
			IF(g_SystemState_Eng_Started = 1)								
				DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Idle;
				DrMn_Previous_ChangeReq_Mode = 0;	
			ELSE
				! *** Dyno Off. Control Mode Monitor.
				g_ControlMode = g_CtMo_enum_Monitor;
				
				! *** Dyno Off. Drive Mode Reset.
				DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Default;
				DrMn_Previous_ChangeReq_Mode = 0;	
			END	
			
		ELSE 
			! *** Alarm Register
			g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_E_SysCMD_Dyno_Off_Fail;		
			g_Alarm_Register_Request_Bit = 1;
		END
		
		! *** MMI Message Send						
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Subroutine; Enging Off; Dyno. Control Off CMD = <%d>", BufferProgrmaNumber, g_SystemState_Dyno_On; END;
		
		
	END !IF((Sys_Current_Command_Input.(Sys_refreshBitIndex_Eng_Preheat_On) = 1) & (g_SystemState_Eng_Preheat_On = 1))
	
	! ----------------------------------------------------------------------------------------------

	! *** Stopped Check		
	IF((g_Feedback_Speed < g_Hmi_bs3004_Eng_IdleMinSpeedSt) & ((g_stop_StopMode = g_stop_enum_HardStopping) | (g_stop_StopMode = g_stop_enum_SoftStopping) | (g_stop_StopMode = g_stop_enum_Stopping)))
		! *** MMI Message Send
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Stopped Check From = <%d>", BufferProgrmaNumber, g_stop_StopMode; END;
		
			! *** Engine Stop. Control Mode Change to default.
			IF(g_SystemState_Dyno_On <> 1)			
				g_ControlMode = g_CtMo_enum_Monitor;
			END
			! *** Engine Stop. Drive Mode Change to default.
			g_DriveManual_Mode = g_DrMn_enum_Default;	
			DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Default;
			DrMn_Previous_ChangeReq_Mode = 0;	
			
			g_stop_StopMode = g_stop_enum_Stopped;
			
		! *** MMI Message Send			
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Stop Status = <%d>", BufferProgrmaNumber, g_stop_StopMode; END;
		
	END

	Sys_Previous_Command_Input = Sys_Current_Command_Input;
	END
RET

! ----------------------------------------------------------------------------------------------

sr_InputRepresh_ControlMode_ChangeRequest:
		 
	CtMo_Current_ChangeReq_Input = 0;
	
	IF(CtMo_Function_Enable.(0) = 1) !>>> Monitor
		CtMo_Current_ChangeReq_Input.CtMo_inputBitIndex_Monitor = g_HMI_DI_Monitor_Button;
		
		! *** MMI Show Message							
		!DISP "[#%d buffer] ControlMode ChangeRequest - [req] Monitor Mode Change", BufferProgrmaNumber;	
	END
	IF(CtMo_Function_Enable.(1) = 1) !>>> DynoService 
		CtMo_Current_ChangeReq_Input.CtMo_inputBitIndex_DynoService = g_HMI_DI_DynoService_Button;
		
		! *** MMI Show Message							
		!DISP "[#%d buffer] ControlMode ChangeRequest - [req] Dyno. Service Mode Change", BufferProgrmaNumber;	
	END
	IF(CtMo_Function_Enable.(2) = 1) !>>> Manual 
		CtMo_Current_ChangeReq_Input.CtMo_inputBitIndex_Manual = g_HMI_DI_Manual_Button; 	
		
		! *** MMI Show Message							
		!DISP "[#%d buffer] ControlMode ChangeRequest - [req] Manual Mode Change", BufferProgrmaNumber;	
	END	
	IF(CtMo_Function_Enable.(3) = 1) !>>> Automatic  
		CtMo_Current_ChangeReq_Input.CtMo_inputBitIndex_Automatic  =g_HMI_DI_Automatic_Button ;
		
		! *** MMI Show Message							
		!DISP "[#%d buffer] ControlMode ChangeRequest - [req] Automatic Mode Change", BufferProgrmaNumber;	
	END
	IF(CtMo_Function_Enable.(4) = 1) !>>> Remote  
		CtMo_Current_ChangeReq_Input.CtMo_inputBitIndex_Remote  = g_HMI_DI_Remote_Button ;
		!CtMo_Current_ChangeReq_Input.CtMo_inputBitIndex_Remote  = g_eCAT_Input_Data(0)(0x50).2 ;
		
		! *** Remote Mode Interlock 
		!IF ((g_ControlMode=g_CtMo_enum_Remote) & (g_eCAT_Input_Data(0)(0x50).2=0))
		!	CtMo_Current_ChangeReq_Input.CtMo_inputBitIndex_Manual = 1;
		!END
		
		! *** MMI Show Message							
		!DISP "[#%d buffer] ControlMode ChangeRequest - [req] Automatic Mode Change", BufferProgrmaNumber;	
	END

		! *** Automatic Mode Interlock 
		IF((g_ControlMode  = g_CtMo_enum_Automatic) & ((g_PC_Write_Data(g_index_PcWrite_PcStatus + 6) <> 1) | (g_PC_Write_Data(g_index_PcWrite_PcStatus + 0x20) <> 1))) !>>> Automation Software Off in Aumatic Mode 
			CtMo_Current_ChangeReq_Input = 0;
			CtMo_Current_ChangeReq_Input.CtMo_inputBitIndex_Manual = 1; 	
			
			! *** MMI Show Message							
			IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] ControlMode ChangeRequest - [req]Automatic >>> Manual; Autoamtic Interlock Fail!", BufferProgrmaNumber; END;
		END
	
RET

! ----------------------------------------------------------------------------------------------

sr_ControlMode_ChangeRequest_Process:
BLOCK
	CALL sr_InputRepresh_ControlMode_ChangeRequest;
			
	! *** Processing control mode change request
	IF(CtMo_Current_ChangeReq_Input <> CtMo_Previous_ChangeReq_Input)		
		
		CtMo_Previous_ChangeReq_Input = CtMo_Current_ChangeReq_Input;
		
		! *** Control Mode Change Request MMI Show Message			
		IF(CtMo_Current_ChangeReq_Input.CtMo_inputBitIndex_Monitor = 1) !>>> Monitor		
			! *** MMI Show Message							
			IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] ControlMode ChangeRequest - [req] Monitor Mode Change", BufferProgrmaNumber; END;
		END
		IF(CtMo_Current_ChangeReq_Input.CtMo_inputBitIndex_DynoService = 1) !>>> DynoService 
			! *** MMI Show Message							
			IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] ControlMode ChangeRequest - [req] Dyno. Service Mode Change", BufferProgrmaNumber; END;	
		END
		IF(CtMo_Current_ChangeReq_Input.CtMo_inputBitIndex_Manual = 1) !>>> Manual 
			! *** MMI Show Message							
			IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] ControlMode ChangeRequest - [req] Manual Mode Change", BufferProgrmaNumber; END;
		END	
		IF(CtMo_Current_ChangeReq_Input.CtMo_inputBitIndex_Automatic = 1) !>>> Automatic  
			! *** MMI Show Message							
			IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] ControlMode ChangeRequest - [req] Automatic Mode Change", BufferProgrmaNumber; END;
		END
		IF(CtMo_Current_ChangeReq_Input.CtMo_inputBitIndex_Remote = 1) !>>> Remote  
			! *** MMI Show Message							
			IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] ControlMode ChangeRequest - [req] Remote Mode Change", BufferProgrmaNumber; END;
		END
	
		! *** Control mode & Drive Manual Change Interlock
		! *** Monitor/Automatic Mode check
			
			! *** Monitor Mode 		
			IF ((CtMo_Current_ChangeReq_Input.(CtMo_inputBitIndex_Monitor) = 1) &(CtMo_Function_Enable.(0) = 1))		
			
				IF((g_ControlMode <> g_CtMo_enum_Monitor) & ((CtMo_Current_ChangeReq_Input - POW(2,CtMo_inputBitIndex_Monitor)) = 0))
					IF(g_ControlMode <> g_CtMo_enum_Default)
					
						g_ControlMode = g_CtMo_enum_Monitor;				
						
						g_DriveManual_Mode = g_DrMn_enum_Default;
						DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Default;
						DrMn_Previous_ChangeReq_Mode = 0;	
						
						g_SystemState_Eng_Starting = 0;	
						g_SystemState_Eng_Started = 0;	
						
						IF (g_SystemState_Misc_On = 1 | g_SystemState_Dyno_On = 1 | g_SystemState_Eng_IG_On = 1)
							IF (g_SystemState_Dyno_On = 1)
								!g_DO_DynoOnSignal = 1;
								!wait(200);
								g_DO_DynoOnSignal = 0;
							END
							IF (g_SystemState_Eng_IG_On = 1)
								!g_DO_IgnitionSignal = 1
								!wait(200);
								g_DO_IgnitionSignal = 0;
							END
							IF (g_SystemState_Misc_On = 1)
								!g_DO_BKReadySignal = 1;
								!wait(500);
								g_DO_FANSignal = 0;
								g_DO_BKReadySignal = 0;
							END
						END
						g_SystemState_Misc_On = 0;
						g_SystemState_Eng_IG_On = 0;
						g_SystemState_Dyno_On = 0;
						g_stop_StopMode = -1;
						! *** MMI Show Message							
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] ControlMode - [cmp]Change to control mode monitor.", BufferProgrmaNumber;	END;
					ELSE
						g_Hmi_Banner_MsgLineNum = 6; g_Bf8_AutoR_Banner_Display = 1;
					END
				END ! IF(g_ControlMode <> enum_ControlMode_Monitor)			
			ELSEIF ((CtMo_Current_ChangeReq_Input.(CtMo_inputBitIndex_Automatic ) = 1) & (CtMo_Function_Enable.(3) = 1))  
			
				IF((g_ControlMode <> g_CtMo_enum_Automatic) & ((CtMo_Current_ChangeReq_Input - POW(2,CtMo_inputBitIndex_Automatic)) = 0))							
					IF(g_ControlMode <> g_CtMo_enum_Default)						
						IF(g_SystemState_Eng_Started <> 1)
							g_Hmi_Banner_MsgLineNum = 4; g_Bf8_AutoR_Banner_Display = 1;
							
							! *** MMI Show Message
							IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] Automation S/W On. Engine On Check Fail!"	END;								
						ELSEIF(g_SystemState_Dyno_On <> 1)
							g_Hmi_Banner_MsgLineNum = 2; g_Bf8_AutoR_Banner_Display = 1;
							
							! *** MMI Show Message
							IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] Automation S/W On. Dyno. On Check Fail!" END;							
						ELSEIF((g_PC_Write_Data(g_index_PcWrite_PcStatus + 6) <> 1) | (g_PC_Write_Data(g_index_PcWrite_PcStatus + 7) <> 0))!Automation S/W Normal/Fault Check.
							g_Hmi_Banner_MsgLineNum = 13; g_Bf8_AutoR_Banner_Display = 1;
							
							! *** MMI Show Message
							IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] Automation S/W Normal/Fault Check Fail!" END;
							
							! *** Error register ***
							g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_AuSoftW_ChgReq_SoftWare_Fault;		
							g_Alarm_Register_Request_Bit = 1;
						ELSEIF(g_PC_Write_Data(g_index_PcWrite_PcStatus + 0xA) <> 1)!Automation S/W Idle Mode Check.
							g_Hmi_Banner_MsgLineNum = 14; g_Bf8_AutoR_Banner_Display = 1;
							
							! *** MMI Show Message
							IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] Automation S/W is not IDLE!" END;
						
							! *** Error register ***
							g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_AuSoftW_ChgReq_SoftWare_NotIdle ;		
							g_Alarm_Register_Request_Bit = 1;
						ELSEIF(g_PC_Write_Data(g_index_PcWrite_PcStatus + 0x20) <> 1)!Automation S/W On Check.
							g_Hmi_Banner_MsgLineNum = 15; g_Bf8_AutoR_Banner_Display = 1;
							
							! *** MMI Show Message
							IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] Automation S/W is not On!" END;
						
							! *** Error register ***
							g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_AuSoftW_ChgReq_SoftWare_NotOn ;		
							g_Alarm_Register_Request_Bit = 1;
						ELSE
									
							g_ControlMode = g_CtMo_enum_Automatic;	
							!Initialize Automatic Eng_IG_On SW
							!g_Automatic_Eng_IG_On = g_SystemState_Eng_IG_On;
						
							! *** MMI Show Message
							IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] ControlMode - [cmp]Change to control mode Automatic.", BufferProgrmaNumber; END;
						END ! IF(g_SystemState_Eng_Started <> 1)		
					ELSE
						g_Hmi_Banner_MsgLineNum = 6; g_Bf8_AutoR_Banner_Display = 1;
					END	! IF(g_ControlMode <> g_CtMo_enum_Default)
				END ! IF(g_ControlMode <> enum_ControlMode_Automatic)		
				
			END ! IF (g_SystemState_Dyno_Ctrl_On = 0)		
		
		
		! *** Manual/Dyno Service Control Mode Change
		IF ((CtMo_Current_ChangeReq_Input.(CtMo_inputBitIndex_DynoService) = 1) & (CtMo_Function_Enable.(1) = 1))
			
			IF((g_ControlMode <> g_CtMo_enum_OpenLoopTest) & ((CtMo_Current_ChangeReq_Input - POW(2,CtMo_inputBitIndex_DynoService)) = 0))						
				IF(g_ControlMode <> g_CtMo_enum_Default)	
				
					IF(g_SystemState_Eng_Started <> 1)
						g_Hmi_Banner_MsgLineNum = 4; g_Bf8_AutoR_Banner_Display = 1;
						
						! *** MMI Show Message
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] Open Loop Test S/W On. Engine On Check Fail!"	END;							
					ELSE
						IF(g_SystemState_Dyno_On <> 1)
							g_Hmi_Banner_MsgLineNum = 2; g_Bf8_AutoR_Banner_Display = 1;
							
							! *** MMI Show Message
							IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] Open Loop Test S/W On. Dyno. On Check Fail!"	END;								
						ELSE
							IF(g_DriveManual_Mode <> g_DrMn_enum_Idle)									
								! *** previous drive mode is not manual! Banner display.
								g_Hmi_Banner_MsgLineNum = 8; g_Bf8_AutoR_Banner_Display = 1;
								
								! *** MMI Show Message
								IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] Open Loop Test S/W On. Idle Mode Check Fail!"	END;	
							ELSE
							
								g_ControlMode = g_CtMo_enum_OpenLoopTest;
									
									g_DriveManual_Mode = g_DrMn_enum_Default;
									DrMn_Current_ChangeReq_Mode  = g_DrMn_enum_Default;
									DrMn_Previous_ChangeReq_Mode = 0;	
									! *** Operator panel demand CMD reference value update
									g_Op_Demand_Initial_Current = 0;

									g_Op_Demand_Initial_Speed = 0;
									g_Op_Demand_Initial_Torque = 0;
									g_Op_Demand_Initial_AlphaPosi = g_Feedback_AlphaPosi;
									
									! *** Update Operator panel knob count 
									
									g_Op_Demand_DynoCurrent_CalRefCnt = g_HMI_AI_Dyno_CalRefCnt; ! operator pannel knob #1
									g_Op_Demand_EngAlphaPosi_CalRefCnt = g_HMI_AI_Eng_CalRefCnt; ! operator pannel knob #2
															
								! *** MMI Show Message						
								IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] ControlMode - [cmp]Change to control mode dyno service.", BufferProgrmaNumber; END;
							END ! IF(g_DriveManual_Mode <> g_DrMn_enum_Idle)	
						END !IF(g_SystemState_Dyno_On <> 1)	
					END !IF(g_SystemState_Eng_Started <> 1)			
				ELSE
					g_Hmi_Banner_MsgLineNum = 6; g_Bf8_AutoR_Banner_Display = 1;
				END	! IF(g_ControlMode <> g_CtMo_enum_Default)
			END ! IF(g_ControlMode <> enum_ControlMode_DynoService)	
				
		ELSEIF ((CtMo_Current_ChangeReq_Input.(CtMo_inputBitIndex_Manual) = 1))! & (CtMo_Function_Enable.(2) = 1))
		
			IF((g_ControlMode <> g_CtMo_enum_Manual) & ((CtMo_Current_ChangeReq_Input - POW(2,CtMo_inputBitIndex_Manual)) = 0))								
				g_ControlMode = g_CtMo_enum_Manual;
				g_stop_StopMode = -1;

				! *** Operator panel demand CMD reference value update
				g_Dyno_PidControl_StateMachine = 500;
				g_Eng_PidControl_StateMachine = 500;
				DrMn_Current_ChangeReq_Mode = g_DriveManual_Mode;
				CALL sr_PARAMETER_INIT

				! *** Drive mode value update
!				IF((g_SystemState_Eng_Started = 1) & (g_SystemState_Dyno_On = 1))				
!					g_DriveManual_Mode = g_DrMn_enum_IdleContrl;	
!					DrMn_Current_ChangeReq_Mode = g_DrMn_enum_IdleContrl;	
!				ELSEIF((g_SystemState_Eng_Started = 1) | (g_SystemState_Dyno_On = 1))	
!					g_DriveManual_Mode = g_DrMn_enum_Idle;
!					DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Idle;
!				END ! IF((g_SystemState_Eng_Started = 1) & (g_SystemState_Dyno_On = 1))
				
				! *** MMI Show Message
				IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] ControlMode - [cmp]Change to control mode manual.", BufferProgrmaNumber; END;
					
			END ! IF(g_ControlMode <> enum_ControlMode_Manual)
		ELSEIF ((CtMo_Current_ChangeReq_Input.(CtMo_inputBitIndex_Remote) = 1) & (CtMo_Function_Enable.(4) = 1))
		
			IF((g_ControlMode <> g_CtMo_enum_Remote) & ((CtMo_Current_ChangeReq_Input - POW(2,CtMo_inputBitIndex_Remote)) = 0))								
				g_ControlMode = g_CtMo_enum_Remote;
				
				! *** Operator panel demand CMD reference value update
				g_Dyno_PidControl_StateMachine = 500;
				g_Eng_PidControl_StateMachine = 500;
				DrMn_Current_ChangeReq_Mode = g_DriveManual_Mode;
				CALL sr_PARAMETER_INIT

				! *** Drive mode value update
!				IF((g_SystemState_Eng_Started = 1) & (g_SystemState_Dyno_On = 1))				
!					g_DriveManual_Mode = g_DrMn_enum_IdleContrl;	
!					DrMn_Current_ChangeReq_Mode = g_DrMn_enum_IdleContrl;	
!				ELSEIF((g_SystemState_Eng_Started = 1) | (g_SystemState_Dyno_On = 1))	
!					g_DriveManual_Mode = g_DrMn_enum_Idle;
!					DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Idle;
!				END ! IF((g_SystemState_Eng_Started = 1) & (g_SystemState_Dyno_On = 1))
				
				! *** MMI Show Message
				IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] ControlMode - [cmp]Change to control mode Remote.", BufferProgrmaNumber; END;
					
			END ! IF(g_ControlMode <> enum_ControlMode_Manual)
		END !  IF ((CtMo_Current_ChangeReq_Input.(CtMo_inputBitIndex_DynoService) = 1) & (CtMo_Function_Enable.(1) = 1))
	
		
	END ! IF(CtMo_Current_ChangeReq_Value <> CtMo_Previous_ChangeReq_Value)
END
RET

! ----------------------------------------------------------------------------------------------

sr_DriveManual_ChangeRequest_Process:
	
BLOCK
	IF(g_ControlMode = g_CtMo_enum_Automatic)
		CALL sr_InputRepresh_AutomaticMode_DriveManual_ChangeRequest;		
	ELSEIF(g_ControlMode = g_CtMo_enum_Remote)
		CALL sr_InputRepresh_RemoteMode_DriveManual_ChangeRequest;		
	ELSE
		CALL sr_InputRepresh_ManualMode_DriveManual_ChangeRequest;	
	END
			
	CALL sr_DrMn_ManualMode_ChangeReq_Process_InputValidation
	
	CALL sr_DrMn_ChangeReq_Process_ModeChange
END
RET

! ----------------------------------------------------------------------------------------------

sr_InputRepresh_RemoteMode_DriveManual_ChangeRequest:
BLOCK
	DrMn_ManualMode_Current_ChangeReq_Input = 0;
	
	IF((DrMn_Function_Enable.(2) = 1)&(g_DI_DynoTorqueModeSignal = 1)&(g_DI_EngineAlphaModeSignal=1)) !>>> DyTo_EngTA 
		DrMn_ManualMode_Current_ChangeReq_Input.DrMn_bitIndex_DyTo_EngTA = 1;
	ELSE
		DrMn_ManualMode_Current_ChangeReq_Input.DrMn_bitIndex_DyTo_EngTA = 0;
	END
	IF((DrMn_Function_Enable.(3) = 1)&(g_DI_DynoSpeedModeSignal = 1)&(g_DI_EngineAlphaModeSignal=1)) !>>> DyRPM_EngTA 
		DrMn_ManualMode_Current_ChangeReq_Input.DrMn_bitIndex_DyRPM_EngTA = 1;
	ELSE
		DrMn_ManualMode_Current_ChangeReq_Input.DrMn_bitIndex_DyRPM_EngTA = 0;
	END
	IF((DrMn_Function_Enable.(5) = 1)&(g_DI_DynoSpeedModeSignal = 1)&(g_DI_EngineTorqueModeSignal=1)) !>>> DyRPM_EngTo 
		DrMn_ManualMode_Current_ChangeReq_Input.DrMn_bitIndex_DyRPM_EngTo = 1;
	ELSE
		DrMn_ManualMode_Current_ChangeReq_Input.DrMn_bitIndex_DyRPM_EngTo = 0;
	END
		!DISP "[#%d buffer] DrMn_ManualMode_Current_ChangeReq_Input [#%d].", BufferProgrmaNumber,DrMn_ManualMode_Current_ChangeReq_Input
END
RET

! ----------------------------------------------------------------------------------------------
sr_InputRepresh_ManualMode_DriveManual_ChangeRequest:
BLOCK
	DrMn_ManualMode_Current_ChangeReq_Input = 0;	
	
	IF(DrMn_Function_Enable.(0) = 1) !>>> Idle 
		DrMn_ManualMode_Current_ChangeReq_Input.DrMn_bitIndex_Idle = g_HMI_DI_Idle_Button;
	END
	IF(DrMn_Function_Enable.(1) = 1) !>>> IdleContrl 
		DrMn_ManualMode_Current_ChangeReq_Input.DrMn_bitIndex_IdleContrl = g_HMI_DI_IdleContrl_Button;
	END
	IF(DrMn_Function_Enable.(2) = 1) !>>> DyTo_EngTA 
		DrMn_ManualMode_Current_ChangeReq_Input.DrMn_bitIndex_DyTo_EngTA = g_HMI_DI_DyTo_EngTA_Button;
	END
	IF(DrMn_Function_Enable.(3) = 1) !>>> DyRPM_EngTA 
		DrMn_ManualMode_Current_ChangeReq_Input.DrMn_bitIndex_DyRPM_EngTA = g_HMI_DI_DyRPM_EngTA_Button;
	END
	IF(DrMn_Function_Enable.(4) = 1) !>>> DyTo_EngRPM 
		DrMn_ManualMode_Current_ChangeReq_Input.DrMn_bitIndex_DyTo_EngRPM = g_HMI_DI_DyTo_EngRPM_Button;
	END
	IF(DrMn_Function_Enable.(5) = 1) !>>> DyRPM_EngTo 
		DrMn_ManualMode_Current_ChangeReq_Input.DrMn_bitIndex_DyRPM_EngTo = g_HMI_DI_DyRPM_EngTo_Button;
	END
END
RET

! ----------------------------------------------------------------------------------------------

sr_InputRepresh_AutomaticMode_DriveManual_ChangeRequest:
BLOCK
	DrMn_AutomaticMode_Current_ChangeReq_Input = 0;	
	
	IF(DrMn_Function_Enable.(0) = 1) !>>> Idle 
		IF (g_PC_Write_Data(g_index_PcWrite_RunDriveMode + 0) = 1)DrMn_AutomaticMode_Current_ChangeReq_Input.DrMn_bitIndex_Idle = 1; END;
	END
	IF(DrMn_Function_Enable.(1) = 1) !>>> IdleContrl 
		IF (g_PC_Write_Data(g_index_PcWrite_RunDriveMode + 1) = 1)DrMn_AutomaticMode_Current_ChangeReq_Input.DrMn_bitIndex_IdleContrl = 1; END;
	END
	IF(DrMn_Function_Enable.(2) = 1) !>>> DyTo_EngTA 
		IF (g_PC_Write_Data(g_index_PcWrite_RunDriveMode + 2) = 1)DrMn_AutomaticMode_Current_ChangeReq_Input.DrMn_bitIndex_DyTo_EngTA = 1; END;
	END
	IF(DrMn_Function_Enable.(3) = 1) !>>> DyRPM_EngTA 
		IF (g_PC_Write_Data(g_index_PcWrite_RunDriveMode + 3) = 1)DrMn_AutomaticMode_Current_ChangeReq_Input.DrMn_bitIndex_DyRPM_EngTA = 1; END;
	END
	IF(DrMn_Function_Enable.(4) = 1) !>>> DyTo_EngRPM 
		IF (g_PC_Write_Data(g_index_PcWrite_RunDriveMode + 4) = 1)DrMn_AutomaticMode_Current_ChangeReq_Input.DrMn_bitIndex_DyTo_EngRPM = 1; END;
	END
	IF(DrMn_Function_Enable.(5) = 1) !>>> DyRPM_EngTo 
		IF (g_PC_Write_Data(g_index_PcWrite_RunDriveMode + 5) = 1)DrMn_AutomaticMode_Current_ChangeReq_Input.DrMn_bitIndex_DyRPM_EngTo = 1; END;
	END
END	
RET

! ----------------------------------------------------------------------------------------------

sr_DrMn_ManualMode_ChangeReq_Process_InputValidation:
BLOCK	
	DrMn_ChangeReq_Process_Execute = 0;
	
	! *** Check Drive Mode Chenge Request - Manual Mode
	IF(DrMn_ManualMode_Current_ChangeReq_Input <> DrMn_ManualMode_Previous_ChangeReq_Input)
	
		DrMn_ManualMode_Previous_ChangeReq_Input = DrMn_ManualMode_Current_ChangeReq_Input;

		IF((g_ControlMode <> g_CtMo_enum_Manual) & (g_ControlMode <> g_CtMo_enum_Remote))
			! *** Alarm Register
			g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_DrMn_ChgReq_ManualModeCheck_Fail;		
			g_Alarm_Register_Request_Bit = 1;
			
			! *** the control mode is not manual! Banner display.
			g_Hmi_Banner_MsgLineNum = 6; g_Bf8_AutoR_Banner_Display = 1;
		ELSE 
			DrMn_ChangeReq_Process_Execute = 1;				
			DrMn_Current_ChangeReq_Process_Input = DrMn_ManualMode_Current_ChangeReq_Input;
			
		END ! IF(g_ControlMode <> g_CtMo_enum_Manual)
	END	! IF(DrMn_ManualMode_Current_ChangeReq_Input <> DrMn_ManualMode_Previous_ChangeReq_Input)

	! *** Check Drive Mode Chenge Request - Automatic Mode
	IF(DrMn_AutomaticMode_Current_ChangeReq_Input <> DrMn_AutomaticMode_Previous_ChangeReq_Input)
	
		DrMn_AutomaticMode_Previous_ChangeReq_Input = DrMn_AutomaticMode_Current_ChangeReq_Input;

		IF(g_ControlMode <> g_CtMo_enum_Automatic)
			! *** Alarm Register
			g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_DrMn_ChgReq_AutomaticModeCheck_Fail;		
			g_Alarm_Register_Request_Bit = 1;
			
			! *** the control mode is not manual! Banner display.
			g_Hmi_Banner_MsgLineNum = 7; g_Bf8_AutoR_Banner_Display = 1;
		ELSE
			DrMn_ChangeReq_Process_Execute = 1;	
			DrMn_Current_ChangeReq_Process_Input = DrMn_AutomaticMode_Current_ChangeReq_Input;
		END ! IF(g_ControlMode <> g_CtMo_enum_Manual)
	END	! IF(DrMn_ManualMode_Current_ChangeReq_Input <> DrMn_ManualMode_Previous_ChangeReq_Input)
END	
	
		IF(DrMn_ChangeReq_Process_Execute = 1)
			DriveModeChangeCondition_IdleExcept = 0;		
			! *** Control mode & Drive Manual Change Interlock		
			! *** Idle/Idle Control Drive Manual mode change request
			
			IF((DrMn_Current_ChangeReq_Process_Input.(DrMn_bitIndex_Idle) = 1) & ((DrMn_Current_ChangeReq_Process_Input - POW(2,DrMn_bitIndex_Idle)) = 0) & (DrMn_Function_Enable.(0) = 1))
				
				IF((g_DriveManual_Mode = g_DrMn_enum_IdleContrl) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngRPM) | (g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTo))
					
					call sr_Dyno_Control_Off_Command_Processing;
					IF(Dyno_Off_Cmd_Processing_Complete = 1)		
						g_SystemState_Dyno_On = 0;
						
						! *** Engine Stated Check & Drive Mode Change
						IF(g_SystemState_Eng_Started = 1)								
							DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Idle;
							DrMn_Previous_ChangeReq_Mode = 0;	
						ELSE
							! *** Dyno Off. Control Mode Monitor.
							!g_ControlMode = g_CtMo_enum_Monitor;
							
							! *** Dyno Off. Drive Mode Reset.
							DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Default;
							DrMn_Previous_ChangeReq_Mode = 0;	
						END	
						
					ELSE 
						! *** Alarm Register
						g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_E_SysCMD_Dyno_Off_Fail;		
						g_Alarm_Register_Request_Bit = 1;
					END
					!DrMn_MoChg_Interlock_DriveMode_SelectIndex = DrMn_bitIndex_Idle;
					!DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Idle;
					!DrMn_Previous_ChangeReq_Mode = 0;
					
					! *** MMI Show Message					
					DISP "[#%d buffer]DriverManual - [req] Change to Idle from Other mode.", BufferProgrmaNumber; 

				ELSE
					IF(g_SystemState_Eng_Started = 0)
						! *** Alarm Register
						g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_DrMn_IdleChgReq_EngStartCheck_Fail;		
						g_Alarm_Register_Request_Bit = 1;
				
						! *** engine is already stated! Banner display.
						g_Hmi_Banner_MsgLineNum = 4; g_Bf8_AutoR_Banner_Display = 1;
					ELSE						
						DrMn_MoChg_Interlock_DriveMode_SelectIndex = DrMn_bitIndex_Idle;
						DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Idle;
						DrMn_Previous_ChangeReq_Mode = 0;
						
						! *** MMI Show Message					
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer]DriverManual - [req] Change to Idle.", BufferProgrmaNumber; END;					
					END !IF(g_SystemState_Eng_Start = 0)
				END ! 	
			ELSEIF((DrMn_Current_ChangeReq_Process_Input.(DrMn_bitIndex_IdleContrl) = 1) & ((DrMn_Current_ChangeReq_Process_Input - POW(2,DrMn_bitIndex_IdleContrl)) = 0) & (DrMn_Function_Enable.(1) = 1))				
				
				! *** Drive Mode change condition check
				!CALL sr_DriveModeChangeConditionCheck_IdleExcept;
				IF(((DrMn_Before_IdleContrl_OnTime - TIME) < 15000) & (DrMn_IdleContrl_ModeChangeReqConut = 1))
					DrMn_IdleContrl_ModeChangeReqConut = 0;
					
					! *** dyno on state check
					IF(g_SystemState_Dyno_On = 0)
						
						! *** Error register ***
						g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_DrMn_IdleExceptChgReq_DynoOnCheck_Fail;		
						g_Alarm_Register_Request_Bit = 1;
					
						! *** Dynamometer On Error! Banner display.
						g_Hmi_Banner_MsgLineNum = 2; g_Bf8_AutoR_Banner_Display = 1;
					ELSE	
		
						DriveModeChangeCondition_IdleExcept = 1;
						! *** MMI Show Message						
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] DriveManual - [req] Double Click; Change to IdleControl.", BufferProgrmaNumber;	END;
					END ! IF(g_SystemState_Dyno_On = 0)
				ELSE
					! *** Drive Mode change condition check
					CALL sr_DriveModeChangeConditionCheck_IdleExcept;
					
					DrMn_IdleContrl_ModeChangeReqConut = 1;
					
				END ! IF((DrMn_Before_IdleContrl_OnTime - TIME) < 5000)
				
				IF(DrMn_IdleContrl_ModeChangeReqConut = 1)
					DrMn_Before_IdleContrl_OnTime = TIME;
				END;					
					
				IF(DriveModeChangeCondition_IdleExcept = 1)
					DrMn_MoChg_Interlock_DriveMode_SelectIndex = DrMn_bitIndex_IdleContrl;
					DrMn_Current_ChangeReq_Mode = g_DrMn_enum_IdleContrl;
					DrMn_Previous_ChangeReq_Mode = 0;
					
					! *** MMI Show Message					
					IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] DriveManual - [req] Change to IdleControl.", BufferProgrmaNumber;	END;
				END !IF((DrMn_Current_ChangeReq_Input - POW(2,DrMn_bitIndex_IdleContrl)) = 0)							
						
			! *** DyTo_EngTA/DyRPM_EngTA/DyTo_EngRPM/DyRPM_EngTo Drive Manual mode change request
			ELSEIF((DrMn_Current_ChangeReq_Process_Input.(DrMn_bitIndex_DyTo_EngTA) = 1) & ((DrMn_Current_ChangeReq_Process_Input - POW(2,DrMn_bitIndex_DyTo_EngTA)) = 0) & (DrMn_Function_Enable.(2) = 1))
				! *** Drive Mode change condition check
				!CALL sr_DriveModeChangeConditionCheck_IdleExcept;
				
				IF((((DrMn_Before_DyTo_EngTA_OnTime - TIME) < 5000) & (DrMn_DyTo_EngTA_ModeChangeReqConut = 1))|(g_ControlMode = g_CtMo_enum_Remote)|(g_ControlMode = g_CtMo_enum_Automatic))
					DrMn_DyTo_EngTA_ModeChangeReqConut = 0;
					
					! *** dyno on state check
					IF(g_SystemState_Dyno_On = 0)
						
						! *** Error register ***
						g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_DrMn_IdleExceptChgReq_DynoOnCheck_Fail;		
						g_Alarm_Register_Request_Bit = 1;
					
						! *** Dynamometer On Error! Banner display.
						g_Hmi_Banner_MsgLineNum = 2; g_Bf8_AutoR_Banner_Display = 1;
					ELSE	
		
						DriveModeChangeCondition_IdleExcept = 1;
						! *** MMI Show Message						
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] DriveManual - [req] Double Click; Change to DyTo_EngTA.", BufferProgrmaNumber;	END;
					END ! IF(g_SystemState_Dyno_On = 0)
				ELSE
					! *** Drive Mode change condition check
					CALL sr_DriveModeChangeConditionCheck_IdleExcept;
					
					DrMn_DyTo_EngTA_ModeChangeReqConut = 1;
					
				END ! IF((DrMn_Before_DyRPM_EngTA_OnTime - TIME) < 5000)
				
				IF(DrMn_DyTo_EngTA_ModeChangeReqConut = 1)
					DrMn_Before_DyTo_EngTA_OnTime = TIME;
				END; ! IF(DrMn_DyTo_EngTA_ModeChangeReqConut = 1)
				
				IF(DriveModeChangeCondition_IdleExcept = 1)
					DrMn_MoChg_Interlock_DriveMode_SelectIndex = DrMn_bitIndex_DyTo_EngTA;
					DrMn_Current_ChangeReq_Mode = g_DrMn_enum_DyTo_EngTA;
					DrMn_Previous_ChangeReq_Mode = 0;
					
					! *** MMI Show Message						
					IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] DriveManual - [req] Change to DyTo_EngTA.", BufferProgrmaNumber;	END;
				END !IF((DrMn_Current_ChangeReq_Input - POW(2,DrMn_bitIndex_DyTo_EngTA)) = 0)						
											
			ELSEIF((DrMn_Current_ChangeReq_Process_Input.(DrMn_bitIndex_DyRPM_EngTA) = 1) & ((DrMn_Current_ChangeReq_Process_Input - POW(2,DrMn_bitIndex_DyRPM_EngTA)) = 0) & (DrMn_Function_Enable.(3) = 1))
				! *** Drive Mode change condition check
				!CALL sr_DriveModeChangeConditionCheck_IdleExcept;
				
				IF((((DrMn_Before_DyRPM_EngTA_OnTime - TIME) < 5000) & (DrMn_DyRPM_EngTA_ModeChangeReqConut = 1))|(g_ControlMode = g_CtMo_enum_Remote)|(g_ControlMode = g_CtMo_enum_Automatic))
					DrMn_DyRPM_EngTA_ModeChangeReqConut = 0;
					
					! *** dyno on state check
					IF(g_SystemState_Dyno_On = 0)
						
						! *** Error register ***
						g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_DrMn_IdleExceptChgReq_DynoOnCheck_Fail;		
						g_Alarm_Register_Request_Bit = 1;
					
						! *** Dynamometer On Error! Banner display.
						g_Hmi_Banner_MsgLineNum = 2; g_Bf8_AutoR_Banner_Display = 1;
					ELSE	
		
						DriveModeChangeCondition_IdleExcept = 1;
						! *** MMI Show Message						
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] DriveManual - [req] Double Click; Change to DyRPM_EngTA.", BufferProgrmaNumber;	END;
					END ! IF(g_SystemState_Dyno_On = 0)
				ELSE
					! *** Drive Mode change condition check
					CALL sr_DriveModeChangeConditionCheck_IdleExcept;
					
					DrMn_DyRPM_EngTA_ModeChangeReqConut = 1;
					
				END ! IF((DrMn_Before_DyRPM_EngTA_OnTime - TIME) < 5000)
				
				IF(DrMn_DyRPM_EngTA_ModeChangeReqConut = 1)
					DrMn_Before_DyRPM_EngTA_OnTime = TIME;
				END; ! IF(DrMn_DyRPM_EngTA_ModeChangeReqConut = 1)
				
				IF(DriveModeChangeCondition_IdleExcept = 1)
					DrMn_MoChg_Interlock_DriveMode_SelectIndex = DrMn_bitIndex_DyRPM_EngTA;
					DrMn_Current_ChangeReq_Mode = g_DrMn_enum_DyRPM_EngTA;
					DrMn_Previous_ChangeReq_Mode = 0;
					
					! *** MMI Show Message						
					IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] DriveManual - [req] Change to DyRPM_EngTA.", BufferProgrmaNumber;	END;
				END !IF((DrMn_Current_ChangeReq_Input - POW(2,DrMn_bitIndex_DyRPM_EngTA)) = 0)						
				
			ELSEIF((DrMn_Current_ChangeReq_Process_Input.(DrMn_bitIndex_DyTo_EngRPM) = 1) & ((DrMn_Current_ChangeReq_Process_Input - POW(2,DrMn_bitIndex_DyTo_EngRPM)) = 0) & (DrMn_Function_Enable.(4) = 1))
				! *** Drive Mode change condition check
				!CALL sr_DriveModeChangeConditionCheck_IdleExcept;
				
				IF((((DrMn_Before_DyTo_EngRPM_OnTime - TIME) < 5000) & (DrMn_DyTo_EngRPM_ModeChangeReqConut = 1))|(g_ControlMode = g_CtMo_enum_Remote)|(g_ControlMode = g_CtMo_enum_Automatic))
					DrMn_DyTo_EngRPM_ModeChangeReqConut = 0;
					
					! *** dyno on state check
					IF(g_SystemState_Dyno_On = 0)
						
						! *** Error register ***
						g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_DrMn_IdleExceptChgReq_DynoOnCheck_Fail;		
						g_Alarm_Register_Request_Bit = 1;
					
						! *** Dynamometer On Error! Banner display.
						g_Hmi_Banner_MsgLineNum = 2; g_Bf8_AutoR_Banner_Display = 1;
					ELSE	
		
						DriveModeChangeCondition_IdleExcept = 1;
						! *** MMI Show Message						
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] DriveManual - [req] Double Click; Change to DyTo_EngRPM.", BufferProgrmaNumber;	END;
					END ! IF(g_SystemState_Dyno_On = 0)
				ELSE
					! *** Drive Mode change condition check
					CALL sr_DriveModeChangeConditionCheck_IdleExcept;
					
					DrMn_DyTo_EngRPM_ModeChangeReqConut = 1;
					
				END ! IF((DrMn_Before_DyRPM_EngTA_OnTime - TIME) < 5000)
				
				IF(DrMn_DyTo_EngRPM_ModeChangeReqConut = 1)
					DrMn_Before_DyTo_EngRPM_OnTime = TIME;
				END; ! IF(DrMn_DyTo_EngRPM_ModeChangeReqConut = 1)
				
				IF(DriveModeChangeCondition_IdleExcept = 1)
					DrMn_MoChg_Interlock_DriveMode_SelectIndex = DrMn_bitIndex_DyTo_EngRPM;
					DrMn_Current_ChangeReq_Mode = g_DrMn_enum_DyTo_EngRPM;
					DrMn_Previous_ChangeReq_Mode = 0;
					
					! *** MMI Show Message						
					IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] DriveManual - [req] Change to DyTo_EngRPM.", BufferProgrmaNumber; END;	
				END !IF((DrMn_Current_ChangeReq_Input - POW(2,DrMn_bitIndex_DyTo_EngRPM)) = 0)						
				
			ELSEIF((DrMn_Current_ChangeReq_Process_Input.(DrMn_bitIndex_DyRPM_EngTo) = 1) & ((DrMn_Current_ChangeReq_Process_Input - POW(2,DrMn_bitIndex_DyRPM_EngTo)) = 0) & (DrMn_Function_Enable.(5) = 1))
				
				! *** Drive Mode change condition check
				!CALL sr_DriveModeChangeConditionCheck_IdleExcept;
				
				IF((((DrMn_Before_DyRPM_EngTo_OnTime - TIME) < 5000) & (DrMn_DyRPM_EngTo_ModeChangeReqConut = 1))|(g_ControlMode = g_CtMo_enum_Remote)|(g_ControlMode = g_CtMo_enum_Automatic))
					DrMn_DyRPM_EngTo_ModeChangeReqConut = 0;
					
					! *** dyno on state check
					IF(g_SystemState_Dyno_On = 0)
						
						! *** Error register ***
						g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_DrMn_IdleExceptChgReq_DynoOnCheck_Fail;		
						g_Alarm_Register_Request_Bit = 1;
					
						! *** Dynamometer On Error! Banner display.
						g_Hmi_Banner_MsgLineNum = 2; g_Bf8_AutoR_Banner_Display = 1;
					ELSE	
		
						DriveModeChangeCondition_IdleExcept = 1;
						! *** MMI Show Message						
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] DriveManual - [req] Double Click; Change to DyRPM_EngTo.", BufferProgrmaNumber;	END;
					END ! IF(g_SystemState_Dyno_On = 0)
				ELSE
					! *** Drive Mode change condition check
					CALL sr_DriveModeChangeConditionCheck_IdleExcept;
					
					DrMn_DyRPM_EngTo_ModeChangeReqConut = 1;
					
				END ! IF((DrMn_Before_DyRPM_EngTA_OnTime - TIME) < 5000)
				
				IF(DrMn_DyRPM_EngTo_ModeChangeReqConut = 1)
					DrMn_Before_DyRPM_EngTo_OnTime = TIME;
				END;
				
				IF(DriveModeChangeCondition_IdleExcept = 1)
					DrMn_MoChg_Interlock_DriveMode_SelectIndex = DrMn_bitIndex_DyRPM_EngTo;
					DrMn_Current_ChangeReq_Mode = g_DrMn_enum_DyRPM_EngTo;
					DrMn_Previous_ChangeReq_Mode = 0;
					
					! *** MMI Show Message						
					IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] DriveManual - [req] Change to DyRPM_EngTo.", BufferProgrmaNumber; END;	
				END !IF((DrMn_Current_ChangeReq_Input - POW(2,DrMn_bitIndex_DyRPM_EngTo)) = 0)							
		
			END !IF((DrMn_Current_ChangeReq_Input.(DrMn_bitIndex_Idle) = 1) & (DrMn_Function_Enable.(0) = 1))
			
	END !IF(DrMn_ChangeReq_Process_Execute = 1)	
RET

! ----------------------------------------------------------------------------------------------

sr_DrMn_ChangeReq_Process_ModeChange:
BLOCK
	IF((g_DriveManual_Mode <> DrMn_Current_ChangeReq_Mode) & (DrMn_Previous_ChangeReq_Mode <> DrMn_Current_ChangeReq_Mode))
		
		IF(g_ControlMode <> g_CtMo_enum_Default)
		
			DrMn_MoChg_Interlock_ControlMode_SelectIndex = 0;
			
			LOOP (DrMn_MoChg_Interlock_MaxRowIndex + 1)
				IF(g_ControlMode = ((DrMn_MoChg_Interlock_ControlMode_SelectIndex + 1) * 100))
					IF(DrMn_MoChg_Interlock(DrMn_MoChg_Interlock_ControlMode_SelectIndex)(DrMn_MoChg_Interlock_DriveMode_SelectIndex) = 1)
						
						CALL sr_PARAMETER_INIT
						
						g_DriveManual_Mode = DrMn_Current_ChangeReq_Mode;	
						
						DriveMode_MoChgCompleteTime = TIME;
						
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#3 buffer] DriveManual - [cmp]Change to control mode. Ch Mode = <%d>", g_DriveManual_Mode; END;
						
					END !IF(DrMn_MoChg_Interlock(DrMn_MoChg_Interlock_ControlMode_SelectIndex)(DrMn_MoChg_Interlock_DriveMode_SelectIndex) = 1)
				END !IF(g_ControlMode = ((DrMn_MoChg_Interlock_CtMo_ColumnSelectIndex + 1) * 100))
				
				DrMn_MoChg_Interlock_ControlMode_SelectIndex = DrMn_MoChg_Interlock_ControlMode_SelectIndex + 1
			END ! LOOP (DrMn_MoChg_Interlock_MaxRowIndex + 1)
			
			IF(g_DriveManual_Mode <> DrMn_Current_ChangeReq_Mode)
					
				! *** Error register ***
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_DrMn_ChgReq_InterlockCheck_Fail;		
				g_Alarm_Register_Request_Bit = 1;
				
				! *** MMI Show Message		
				IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#3 buffer] DriveManual - [fail]Change to control mode. Current Mode = <%d>, Ch Req Mode = <%d>", g_DriveManual_Mode, DrMn_Current_ChangeReq_Mode; END;
					
			END ! IF(g_DriveManual_Mode <> DrMn_Current_ChangeReq_Mode)
			
			DrMn_Previous_ChangeReq_Mode = DrMn_Current_ChangeReq_Mode;			
					
		END ! IF(g_ControlMode <> CtMo_enum_Default)	
		
	END ! IF(g_DriveManual_Mode <> DrMn_Current_ChangeReq_Mode)
	
	! *** Dyno & Engine PID State Check
	IF((g_DriveManual_Mode = g_DrMn_enum_Idle) & ((TIME - DriveMode_MoChgCompleteTime)>1000))
		IF(PST(6).#RUN = 0)
			DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Default;
			DrMn_Previous_ChangeReq_Mode = g_DrMn_enum_Default;
			
			g_DriveManual_Mode = g_DrMn_enum_Default;
		END ! IF(PST(6).#RUN = 0)
	END ! IF((g_DriveManual_Mode = g_DrMn_enum_Idle) & ((DriveMode_MoChgCompleteTime - TIME)>100))
	
	IF(((g_DriveManual_Mode = g_DrMn_enum_IdleContrl) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngRPM) | (g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTo)) & ((TIME - DriveMode_MoChgCompleteTime)>1000))
		IF((PST(5).#RUN = 0) | (PST(6).#RUN = 0))
			DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Default;
			DrMn_Previous_ChangeReq_Mode = g_DrMn_enum_Default;
			CALL sr_Engine_Stop_Process;
			g_DriveManual_Mode = g_DrMn_enum_Default;
		END ! IF((PST(0).#RUN = 0) | (PST(6).#RUN = 0))
	END !IF(((g_DriveManual_Mode = g_DrMn_enum_IdleContrl) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngRPM) | (g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTo)) & ((DriveMode_MoChgCompleteTime - TIME)>100))
END	
RET
!--------------------------------------------------------------------

sr_PARAMETER_INIT:
	BLOCK
		g_PID_Change_Ramp = g_Hmi_bs3008_PID_Change_Ramp;
		DISP "[#%d buffer] [%d] Change Mode", BufferProgrmaNumber, g_DriveManual_Mode; 
		g_Op_Demand_Initial_Current = 0;

		! *** Operator panel demand CMD reference value update
		g_Op_Demand_Initial_Speed = 0;
		g_Op_Demand_Initial_Torque = 0;
		g_Op_Demand_Initial_AlphaPosi = 0;
		
		g_Op_Demand_EngSpeed_CalRefCnt = 0;
		g_Op_Demand_EngTorque_CalRefCnt = 0;
		g_Op_Demand_EngAlphaPosi_CalRefCnt = 0;
		g_Op_Demand_DynoSpeed_CalRefCnt = 0;
		g_Op_Demand_DynoTorque_CalRefCnt = 0;

		!g_Target_Dyno_Speed = 0;	
		!g_Target_Dyno_Torque = 0;					
		!g_Target_Engine_Speed = 0; 
		!g_Target_Engine_Torque = 0; 												
		!g_Target_Engine_AlphaPosi = 0;	
		!DISP g_HMI_AI_Dyno_CalRefCnt, g_HMI_AI_Eng_CalRefCnt
		g_DriveManual_Mode = DrMn_Current_ChangeReq_Mode;
		! *** Update Operator panel knob count 
		IF(DrMn_Current_ChangeReq_Mode = g_DrMn_enum_DyTo_EngTA)
			g_Op_Demand_Initial_Torque = FLOOR(g_Feedback_Torque/g_Hmi_bs2202_TorqueCngPerKnobPulseSt)*g_Hmi_bs2202_TorqueCngPerKnobPulseSt;
			g_Op_Demand_Initial_AlphaPosi = FLOOR(g_Feedback_AlphaPosi/g_Hmi_bs2202_AlphaCngPerKnobPulseSt*10)*g_Hmi_bs2202_AlphaCngPerKnobPulseSt/10;
			
			g_Target_Dyno_Torque = g_Op_Demand_Initial_Torque;
			g_Target_Engine_AlphaPosi = g_Op_Demand_Initial_AlphaPosi;
			g_Op_Demand_DynoTorque_CalRefCnt = g_HMI_AI_Dyno_CalRefCnt; ! operator pannel knob #1
			g_Op_Demand_EngAlphaPosi_CalRefCnt = g_HMI_AI_Eng_CalRefCnt; ! operator pannel knob #2
		ELSEIF(DrMn_Current_ChangeReq_Mode = g_DrMn_enum_DyRPM_EngTA)
			g_Op_Demand_Initial_Speed = FLOOR(g_Feedback_Speed/g_Hmi_bs2202_SpeedCngPerKnobPulseSt)*g_Hmi_bs2202_SpeedCngPerKnobPulseSt;
			g_Op_Demand_Initial_AlphaPosi = FLOOR(g_Feedback_AlphaPosi/g_Hmi_bs2202_AlphaCngPerKnobPulseSt*10)*g_Hmi_bs2202_AlphaCngPerKnobPulseSt/10;

			g_Target_Dyno_Speed = g_Op_Demand_Initial_Speed;
			g_Target_Engine_AlphaPosi = g_Op_Demand_Initial_AlphaPosi;
			g_Op_Demand_DynoSpeed_CalRefCnt = g_HMI_AI_Dyno_CalRefCnt; ! operator pannel knob #1
			g_Op_Demand_EngAlphaPosi_CalRefCnt = g_HMI_AI_Eng_CalRefCnt; ! operator pannel knob #2
		ELSEIF(DrMn_Current_ChangeReq_Mode = g_DrMn_enum_DyTo_EngRPM)
			g_Op_Demand_Initial_Speed = FLOOR(g_Feedback_Speed/g_Hmi_bs2202_SpeedCngPerKnobPulseSt)*g_Hmi_bs2202_SpeedCngPerKnobPulseSt;
			g_Op_Demand_Initial_Torque = FLOOR(g_Feedback_Torque/g_Hmi_bs2202_TorqueCngPerKnobPulseSt)*g_Hmi_bs2202_TorqueCngPerKnobPulseSt;

			g_Target_Dyno_Torque = g_Op_Demand_Initial_Torque;
			g_Target_Engine_Speed = g_Op_Demand_Initial_Speed;
			g_Op_Demand_DynoTorque_CalRefCnt = g_HMI_AI_Dyno_CalRefCnt; ! operator pannel knob #1
			g_Op_Demand_EngSpeed_CalRefCnt = g_HMI_AI_Eng_CalRefCnt; ! operator pannel knob #2
		ELSEIF(DrMn_Current_ChangeReq_Mode = g_DrMn_enum_DyRPM_EngTo)
			g_Op_Demand_Initial_Speed = FLOOR(g_Feedback_Speed/g_Hmi_bs2202_SpeedCngPerKnobPulseSt)*g_Hmi_bs2202_SpeedCngPerKnobPulseSt;
			g_Op_Demand_Initial_Torque = FLOOR(g_Feedback_Torque/g_Hmi_bs2202_TorqueCngPerKnobPulseSt)*g_Hmi_bs2202_TorqueCngPerKnobPulseSt;

			g_Target_Dyno_Speed = g_Feedback_Speed;
			g_Target_Engine_Torque = g_Feedback_Torque;
			g_Op_Demand_DynoSpeed_CalRefCnt = g_HMI_AI_Dyno_CalRefCnt; ! operator pannel knob #1
			g_Op_Demand_EngTorque_CalRefCnt = g_HMI_AI_Eng_CalRefCnt; ! operator pannel knob #2	
		ELSE
			! 5/20 Imsi

			g_Target_Engine_AlphaPosi = g_Hmi_bs3003_Eng_StopAlphaSt;	

		END
		!DISP "Dyno_Speed : %d ,Dyno_Torque : %d ,Engine_Speed : %d , Engine_Torque : %d, Engine_AlphaPosi : %d ", g_Target_Dyno_Speed, g_Target_Dyno_Torque, g_Target_Engine_Speed, g_Target_Engine_Torque, g_Target_Engine_AlphaPosi;

	END
RET
!--------------------------------------------------------------------
sr_Engine_Stop_Process:
				
		! *** MMI Message Send
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Subroutine; Enging Off;Soft Stopping On", BufferProgrmaNumber; END;
							
		! *** Engine Ignition Off CMD Excute
			!g_DO_BKReadySignal = 1;
			!g_DO_IgnitionSignal = 1;
		
		! *** Engine Start Off CMD Excute
		g_DO_StartSignal = 0;
		
		g_stop_StopMode = g_stop_enum_SoftStopping;
			
		! *** Check Delay Time for Dyno On State
		WAIT(100);
		
		! *** Engine Ignition Off CMD Check & Result Update
		!IF(g_DO_IgnitionSignal = 1)					
			g_DO_BKReadySignal = 0;
			g_DO_IgnitionSignal = 0;
			g_SystemState_Eng_Starting = 0;	
			g_SystemState_Eng_Started = 0;	
			
			! *** Engine Stop. Control Mode Change to default.
			g_ControlMode = g_CtMo_enum_Manual;
			
			! *** Engine Stop. Drive Mode Change to default.
			DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Default;
			DrMn_Previous_ChangeReq_Mode = 0;	
			
			! *** Engine Ignition Reset.
			g_SystemState_Eng_IG_On = 0;
			g_SystemState_Misc_On = 0;
		!ELSE	
			! *** Alarm Register
		!	g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_E_SysCMD_Engine_StopCmdOff_Fail;		
		!	g_Alarm_Register_Request_Bit = 1;			
		!END					
		
		! *** MMI Message Send			
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Soft Stopping On CMD = <%d>", BufferProgrmaNumber, g_stop_StopMode; END;
		
		! ----------------------------------------------------------------------------------------------

		! *** MMI Message Send						
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Subroutine; Enging Off; Dyno. Off", BufferProgrmaNumber; END;
							
		CALL sr_Dyno_Control_Off_Command_Processing;
		
		! *** Command Result Check
		IF(Dyno_Off_Cmd_Processing_Complete = 1)		
			g_SystemState_Dyno_On = 0;
			
			! *** Engine Stated Check & Drive Mode Change
			IF(g_SystemState_Eng_Started = 1)								
				DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Idle;
				DrMn_Previous_ChangeReq_Mode = 0;	
			ELSE
				! *** Dyno Off. Control Mode Monitor.
				g_ControlMode = g_CtMo_enum_Monitor;
				
				! *** Dyno Off. Drive Mode Reset.
				DrMn_Current_ChangeReq_Mode = g_DrMn_enum_Default;
				DrMn_Previous_ChangeReq_Mode = 0;	
			END	
			
		ELSE 
			! *** Alarm Register
			g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_E_SysCMD_Dyno_Off_Fail;		
			g_Alarm_Register_Request_Bit = 1;
		END
		
		! *** MMI Message Send						
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Subroutine; Enging Off; Dyno. Control Off CMD = <%d>", BufferProgrmaNumber, g_SystemState_Dyno_On; END;
		
RET
! ----------------------------------------------------------------------------------------------

sr_DriveModeChangeConditionCheck_IdleExcept:

	DriveModeChangeCondition_IdleExcept = 0;	

	! *** engine start state check
	IF(g_SystemState_Eng_Started = 0)
		! *** Error register ***
		g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_DrMn_IdleExceptChgReq_EngStartCheck_Fail;		
		g_Alarm_Register_Request_Bit = 1;
				
		! *** engine is already stated! Banner display.
		!g_Hmi_Banner_MsgLineNum = 4; g_Bf8_AutoR_Banner_Display = 1;
		! *** engine is already stated! Banner display.
		g_Hmi_Banner_MsgLineNum = 32; g_Bf8_AutoR_Banner_Display = 1;
	
	ELSE
		! *** dyno on state check
		IF(g_SystemState_Dyno_On = 0)
			
			! *** Error register ***
			g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_DrMn_IdleExceptChgReq_DynoOnCheck_Fail;		
			g_Alarm_Register_Request_Bit = 1;
		
			! *** engine is already stated! Banner display.
			!g_Hmi_Banner_MsgLineNum = 2; g_Bf8_AutoR_Banner_Display = 1;
			! *** engine is already stated! Banner display.
			g_Hmi_Banner_MsgLineNum = 32; g_Bf8_AutoR_Banner_Display = 1;
			
		ELSE
			! *** Derive mode check
			IF(g_DriveManual_Mode <> g_DrMn_enum_Idle)
				! *** Error register ***
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_DrMn_IdleExceptChgReq_IdleMdCheck_Fail;		
				g_Alarm_Register_Request_Bit = 1;
			
				! *** previous drive mode is not manual! Banner display.
				!g_Hmi_Banner_MsgLineNum = 9; g_Bf8_AutoR_Banner_Display = 1;
				! *** engine is already stated! Banner display.
				g_Hmi_Banner_MsgLineNum = 32; g_Bf8_AutoR_Banner_Display = 1;
			
			ELSE				
				! *** System Idle State Check - Speed
				
				!IF(g_Feedback_Speed > g_Hmi_bs3001_Eng_IdleMinSpeedSt)
					! *** Error register ***
				!	g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_DrMn_IdleMdCheck_SpeedCheck_Fail;		
				!	g_Alarm_Register_Request_Bit = 1;
				
					! *** previous drive mode is not manual! Banner display.
				!	g_Hmi_Banner_MsgLineNum = 10; g_Bf8_AutoR_Banner_Display = 1;
				!ELSE
					! *** System Idle State Check - Torque
					
				!	IF(g_Feedback_Torque > g_Hmi_bs3003_Eng_StopTorqueSt)
						! *** Error register ***
				!		g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_DrMn_IdleMdCheck_TorqueCheck_Fail;		
				!		g_Alarm_Register_Request_Bit = 1;
					
						! *** previous drive mode is not manual! Banner display.
				!		g_Hmi_Banner_MsgLineNum = 11; g_Bf8_AutoR_Banner_Display = 1;
				!	ELSE
						! *** System Idle State Check - Alpha
						IF(g_Feedback_AlphaPosi > (g_Hmi_bs3003_Eng_StopAlphaSt + 1))
							! *** Error register ***
							g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_DrMn_IdleMdCheck_AlphaCheck_Fail;		
							g_Alarm_Register_Request_Bit = 1;
						
							! *** previous drive mode is not manual! Banner display.
							g_Hmi_Banner_MsgLineNum = 12; g_Bf8_AutoR_Banner_Display = 1;
						ELSE	
						
							DriveModeChangeCondition_IdleExcept = 1;
							
						END !IF(g_Feedback_Torque > g_Hmi_bs3002_Eng_StopTorqueSt) 
					!END ! IF(g_Feedback_Speed > g_Hmi_bs3001_Eng_IdleMinSpeedSt)
				!END ! IF(g_Feedback_Speed > g_Hmi_bs3001_Eng_IdleMinSpeedSt)
			END ! IF(g_DriveManual_Mode <> g_DrMn_enum_Idle)
		END	! IF(g_SystemState_Dyno_On = 0)
	END	! IF(g_SystemState_Eng_Start = 0)
	
RET
! ----------------------------------------------------------------------------------------------

sr_Dyno_Control_On_Command_Processing:
	
	! *** Result Variable Init	
	Dyno_OnOff_Cmd_Processing_Result = 0;
	
	
	! *** Dyno Current Command 0 Set
	g_AO_DynoControlSignal = 0;
	g_DO_FANSignal = 1;

	! *** Dyno On CMD Excute
	!g_eCAT_Output_Data(0)(0x60).6 = 1;
	
	! *** Check Delay Time for Dyno On State
	!WAIT(1000);
	
	! *** Dyno On CMD Excute
	!g_eCAT_Output_Data(0)(0x60).6 = 0;
	
	WAIT(10);
	
	! *** Dyno On CMD Excute
	g_DO_DynoOnSignal = 1;
	
	! *** Check Delay Time for Dyno On State
	!WAIT(1000);
	!g_DO_DynoOnSignal = 0;
	
	! *** Dyno Off CMD Check <Dyno TROUBLE>
	!IF(g_eCAT_Input_Data(0)(0x50).1 = 0)				
	!	Dyno_OnOff_Cmd_Processing_Result = 1;
	!ELSE
		! *** MMI Show Message		
	!	IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#3 buffer] Dyno On CMD - [fail] Dyno Trouble Bit = <%d>", g_DriveManual_Mode, g_eCAT_Input_Data(0)(0x50).1; END;
				
	!END
	Dyno_OnOff_Cmd_Processing_Result = 1;
	! *** Processing result update
	Dyno_On_Cmd_Processing_Complete = Dyno_OnOff_Cmd_Processing_Result;
RET

! ----------------------------------------------------------------------------------------------

sr_Dyno_Control_Off_Command_Processing:
	
	! *** Result Variable Init	
	Dyno_OnOff_Cmd_Processing_Result = 0;
	
	! *** Dyno Current Command 0 Set
	g_AO_DynoControlSignal = 0;!16383;
	g_DO_FANSignal = 0;
	
	WAIT(10);
		
	! *** Dyno Off CMD Excute
	!g_DO_DynoOnSignal = 1;
	
	! *** Check Delay Time for Dyno On State
	!WAIT(1000);
	
	g_DO_DynoOnSignal = 0;

	! *** Dyno Off CMD Check <Dyno TROUBLE>
	!IF(g_eCAT_Input_Data(0)(0x50).1 = 0)				
		Dyno_OnOff_Cmd_Processing_Result = 1;	
		
		! *** MMI Show Message		
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#3 buffer] Dyno Off CMD - [fail] Dyno Trouble Bit = <%d>", g_DriveManual_Mode, g_DI_DynoTroubleSignal; END;
			
	!ELSE
		
	!END
	Dyno_OnOff_Cmd_Processing_Result = 1;	
	! *** Processing result update
	Dyno_Off_Cmd_Processing_Complete = Dyno_OnOff_Cmd_Processing_Result;
RET

! ----------------------------------------------------------------------------------------------

sr_AutomationSWCommand_Valiable_Declaration:
	! *** Prifile Write Valiable Declaration
	INT profile_Current_UsingCMD;
	INT profile_Prevous_UsingCMD;
	
	! *** Prifile Data List Info
	!GLOBAL INT g_pf_IndexStart_Infomation
	!GLOBAL INT g_pf_IndexSize_Infomation
	!GLOBAL INT g_pf_IndexStart_DynoProfile	
	!GLOBAL INT g_pf_IndexStart_EngineProfile
	!GLOBAL INT g_pf_IndexSize_DynoEngProfile
	!GLOBAL INT g_pf_ProfileListSize
	
	! *** Automatic Mode Control Command
	INT AutoMd_Current_Control_CMD;
	INT AutoMd_Previous_Control_CMD;
	!GLOBAL INT g_AutoMd_ProfilePlay_Enable;	
		
	! *** Automatic Mode Control Command Index 
	INT index_AutoMd_CtrlCmd_Init;
	INT index_AutoMd_CtrlCmd_Idle;
	INT index_AutoMd_CtrlCmd_Run;
	INT index_AutoMd_CtrlCmd_Stop;
	INT index_AutoMd_CtrlCmd_Pause;
	INT index_AutoMd_CtrlCmd_Resume;
	
	! *** Measurement Eqp Control Command
	!GLOBAL INT g_index_PcWrite_MeasureEqpCMD;
	INT MesureEqp_BlowByMt_Current_Control_CMD;
	INT MesureEqp_BlowByMt_Previous_Control_CMD;
	INT MesureEqp_SmokeMt_Current_Control_CMD;
	INT MesureEqp_SmokeMt_Previous_Control_CMD;
	INT MesureEqp_FuelMt_Current_Control_CMD;
	INT MesureEqp_FuelMt_Previous_Control_CMD;
	
	! *** Measurement Eqp Control Command Index
	INT index_MesureEqp_CtrlCmd_Reset;
	INT index_MesureEqp_CtrlCmd_MeasureStart;
	INT index_MesureEqp_CtrlCmd_MeasureStop;	
	
	! *** Measurement Eqp Controller Waiting to reset 
	REAL MesureEqp_BlowBy_ResetStartTime;
	REAL MesureEqp_BlowBy_ResetProcessTime;
	REAL MesureEqp_SmokeMeter_ResetStartTime;
	REAL MesureEqp_SmokeMeter_ResetProcessTime;
	REAL MesureEqp_FuelMeter_ResetStartTime;
	REAL MesureEqp_FuelMeter_ResetProcessTime;	
	
	! *** ATOM Software Stop Command
	INT AtomSwStop_Current_Control_CMD;
	INT AtomSwStop_Previous_Control_CMD;
	
	! *** ATOM Software Stop Command Index 
	INT index_AtomSwStop_CtrlCmd_Idle;
	INT index_AtomSwStop_CtrlCmd_Soft;
	INT index_AtomSwStop_CtrlCmd_Hard;
RET

! ----------------------------------------------------------------------------------------------

sr_AutomationSWCommand_Valiable_Init:
	! *** Prifile Write Valiable Initialization	
	profile_Current_UsingCMD = 0;
	profile_Prevous_UsingCMD = 0;
	
	! *** Prifile Data List Initialization
	g_pf_IndexStart_Infomation = 0;
	g_pf_IndexSize_Infomation = 99;
	g_pf_IndexStart_DynoProfile = 100;	
	g_pf_IndexStart_EngineProfile = 2600;
	g_pf_IndexSize_DynoEngProfile = 2499;
	g_pf_ProfileListSize = (5100 - 1);	
	
	! *** Automatic Mode Control Command Initialization	
	AutoMd_Current_Control_CMD = 0;
	AutoMd_Previous_Control_CMD = -1;
	g_AutoMd_ProfilePlay_Enable = 0;
		
	! *** Automatic Mode Control Command Index Initialization	
	index_AutoMd_CtrlCmd_Init = 0;
	index_AutoMd_CtrlCmd_Idle = 1;
	index_AutoMd_CtrlCmd_Run = 2;
	index_AutoMd_CtrlCmd_Stop = 3;
	index_AutoMd_CtrlCmd_Pause = 4;
	index_AutoMd_CtrlCmd_Resume = 5;
	
	! *** Measurement Eqp Control Command Initialization
	g_index_PcWrite_MeasureEqpCMD = 0xc0;
	MesureEqp_BlowByMt_Current_Control_CMD = 0;
	MesureEqp_BlowByMt_Previous_Control_CMD = 0;
	MesureEqp_SmokeMt_Current_Control_CMD = 0;
	MesureEqp_SmokeMt_Previous_Control_CMD = 0;
	MesureEqp_FuelMt_Current_Control_CMD = 0;
	MesureEqp_FuelMt_Previous_Control_CMD = 0;
	
	! *** Measurement Eqp Control Command Index Initialization	
	index_MesureEqp_CtrlCmd_Reset = 0;
	index_MesureEqp_CtrlCmd_MeasureStart = 1;
	index_MesureEqp_CtrlCmd_MeasureStop = 2;		
	
	! *** Measurement Eqp Controller Waiting to reset Initialization
	MesureEqp_BlowBy_ResetStartTime = 0;
	MesureEqp_BlowBy_ResetProcessTime = 0;
	MesureEqp_SmokeMeter_ResetStartTime = 0;
	MesureEqp_SmokeMeter_ResetProcessTime = 0;
	MesureEqp_FuelMeter_ResetStartTime = 0;
	MesureEqp_FuelMeter_ResetProcessTime = 0;
	
	! *** ATOM Software Stop Command Initialization
	AtomSwStop_Current_Control_CMD = 0;
	AtomSwStop_Previous_Control_CMD = 0;
	
	! *** ATOM Software Stop Command Index Initialization
	index_AtomSwStop_CtrlCmd_Idle = 0;
	index_AtomSwStop_CtrlCmd_Soft = 1;
	index_AtomSwStop_CtrlCmd_Hard = 2;
RET

! ----------------------------------------------------------------------------------------------

sr_AutomationSWCommand_Profile_WriteProcess:	
	
	! *** Profile Using Command Update 
	profile_Current_UsingCMD = 0;	
	IF(g_PC_Write_Data(g_index_PcWrite_ProfileWrite + 2) = 1 ) profile_Current_UsingCMD.0 = 1; END; ! T/Alpha Profile Data Using		
	IF(g_PC_Write_Data(g_index_PcWrite_ProfileWrite + 6) = 1 ) profile_Current_UsingCMD.1 = 1; END; ! N/Alpha Profile Data Using	
	IF(g_PC_Write_Data(g_index_PcWrite_ProfileWrite + 0xA) = 1 ) profile_Current_UsingCMD.2 = 1; END; ! T/N Profile Data Using
	IF(g_PC_Write_Data(g_index_PcWrite_ProfileWrite + 0xE) = 1 ) profile_Current_UsingCMD.3 = 1; END; ! N/T Profile Data Using
	
	! ----------------------------------------------------------------------------------------------

	! *** Check Profile Using Command Changing
	IF(profile_Current_UsingCMD <> profile_Prevous_UsingCMD)
		! *** MMI Show Message					
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] UsingProfile - [req] Profile Using. Using CMD = <%d>", BufferProgrmaNumber, profile_Current_UsingCMD;	END;	
		DISP "[#%d buffer] UsingProfile - [req] Profile Using. Using CMD = <%d>", BufferProgrmaNumber, profile_Current_UsingCMD;
		
		profile_Prevous_UsingCMD = profile_Current_UsingCMD;
		
		IF(g_AutoMd_ProfilePlay_Enable = 1)
					
			! *** the control mode is not manual! Banner display.
			g_Hmi_Banner_MsgLineNum = 19; g_Bf8_AutoR_Banner_Display = 1;
			
			! *** Error register ***
			g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_AuSoftW_UsingProfile_Fail_AutoMd_ProfilePlay_On ;		
			g_Alarm_Register_Request_Bit = 1;				
				
			! *** MMI Show Message							
			IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] UsingProfile - [Error] Profile Using Fail; Profile Play = <%d>", BufferProgrmaNumber, g_AutoMd_ProfilePlay_Enable; END;
			DISP "[#%d buffer] UsingProfile - [Error] Profile Using Fail; Profile Play = <%d>", BufferProgrmaNumber, g_AutoMd_ProfilePlay_Enable;
		ELSE
		
			! T/Alpha Profile Data CopyUsing CMD
			IF(profile_Current_UsingCMD.0 = 1 )
				IF(g_PC_Write_Data(g_index_PcWrite_ProfileWrite + 1) <> 1 )
					g_Hmi_Banner_MsgLineNum = 18; g_Bf8_AutoR_Banner_Display = 1;
					
					! *** Error register ***
					g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_AuSoftW_TqApUsingProfile_WriteCompleteCheckFail ;		
					g_Alarm_Register_Request_Bit = 1;
					
					! *** MMI Show Message					
					IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] UsingProfile - [Alarm] Profile Write Complete Bit Off", BufferProgrmaNumber; END;
					DISP "[#%d buffer] UsingProfile - [Alarm] Profile Write Complete Bit Off", BufferProgrmaNumber;
				ELSE
					
					IF((MAX(g_PC_Write_Data_TqAlphaProfile) = 0) & (MIN(g_PC_Write_Data_TqAlphaProfile) = 0))
						g_Hmi_Banner_MsgLineNum = 18; g_Bf8_AutoR_Banner_Display = 1;
						
						! *** Error register ***
						g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_AuSoftW_TqApProfile_StepCountCheckFail ;		
						g_Alarm_Register_Request_Bit = 1;
						
						! *** MMI Show Message					
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] UsingProfile - [Alarm] T/Alpha Profile is Empty.", BufferProgrmaNumber;END;
						DISP "[#%d buffer] UsingProfile - [Alarm] T/Alpha Profile is Empty.", BufferProgrmaNumber;
					ELSE
						! T/Alpha Profile Data Copy
						COPY(g_PC_Write_Data_TqAlphaProfile, g_PC_Process_Data_TqAlphaProfile);
						
						! *** MMI Show Message					
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] UsingProfile - [cmp] T/Alpha Profile Data Using.", BufferProgrmaNumber; END;	
						DISP "[#%d buffer] UsingProfile - [cmp] T/Alpha Profile Data Using.", BufferProgrmaNumber;
					END ! IF((MAX(g_PC_Write_Data_TqAlphaProfile) = 0) & (MIN(g_PC_Write_Data_TqAlphaProfile) = 0))
				END ! IF(g_PC_Write_Data(g_index_PcWrite_ProfileWrite + 1) <> 1 )
			END ! IF(g_PC_Write_Data(g_index_PcWrite_ProfileWrite + 2) = 1 )
			
			! ----------------------------------------------------------------------------------------------
			
			! N/Alpha Profile Data CopyUsing CMD
			IF(profile_Current_UsingCMD.1 = 1 )
				IF(g_PC_Write_Data(g_index_PcWrite_ProfileWrite + 5) <> 1 )
					g_Hmi_Banner_MsgLineNum = 18; g_Bf8_AutoR_Banner_Display = 1;
					
					! *** Error register ***
					g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_AuSoftW_SpApUsingProfile_WriteCompleteCheckFail;		
					g_Alarm_Register_Request_Bit = 1;
					
					! *** MMI Show Message					
					IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] UsingProfile - [Alarm] Profile Write Complete Bit Off", BufferProgrmaNumber;	END;
					 DISP "[#%d buffer] UsingProfile - [Alarm] Profile Write Complete Bit Off", BufferProgrmaNumber;
				ELSE
					
					IF((MAX(g_PC_Write_Data_SpAlphaProfile) = 0) & (MIN(g_PC_Write_Data_SpAlphaProfile) = 0))
						g_Hmi_Banner_MsgLineNum = 18; g_Bf8_AutoR_Banner_Display = 1;
						
						! *** Error register ***
						g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_AuSoftW_SpApProfile_StepCountCheckFail ;		
						g_Alarm_Register_Request_Bit = 1;
						
						! *** MMI Show Message					
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] UsingProfile - [Alarm] N/Alpha Profile is Empty.", BufferProgrmaNumber; END;
						DISP "[#%d buffer] UsingProfile - [Alarm] N/Alpha Profile is Empty.", BufferProgrmaNumber;
					ELSE				
						! N/Alpha Profile Data Copy
						COPY(g_PC_Write_Data_SpAlphaProfile, g_PC_Process_Data_SpAlphaProfile);
						
						! *** MMI Show Message					
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] UsingProfile - [cmp] N/Alpha Profile Data Using.", BufferProgrmaNumber; END;
						DISP "[#%d buffer] UsingProfile - [cmp] N/Alpha Profile Data Using.", BufferProgrmaNumber;
					END ! IF((MAX(g_PC_Write_Data_SpAlphaProfile) = 0) & (MIN(g_PC_Write_Data_SpAlphaProfile) = 0))
				END ! IF(g_PC_Write_Data(g_index_PcWrite_ProfileWrite + 5) <> 1 )
			END ! IF(g_PC_Write_Data(g_index_PcWrite_ProfileWrite + 6) = 1 )
			
			! ----------------------------------------------------------------------------------------------
			
			! T/N Profile Data CopyUsing CMD
			IF(profile_Current_UsingCMD.2 = 1 )
				IF(g_PC_Write_Data(g_index_PcWrite_ProfileWrite + 9) <> 1 )
					g_Hmi_Banner_MsgLineNum = 18; g_Bf8_AutoR_Banner_Display = 1;
					
					! *** Error register ***
					g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_AuSoftW_SpTqUsingProfile_WriteCompleteCheckFail;		
					g_Alarm_Register_Request_Bit = 1;
					
					! *** MMI Show Message					
					IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] UsingProfile - [Alarm] Profile Write Complete Bit Off", BufferProgrmaNumber;	END;
					 DISP "[#%d buffer] UsingProfile - [Alarm] Profile Write Complete Bit Off", BufferProgrmaNumber;
				ELSE
					
					IF((MAX(g_PC_Write_Data_TqSpProfile) = 0) & (MIN(g_PC_Write_Data_TqSpProfile) = 0))
						g_Hmi_Banner_MsgLineNum = 18; g_Bf8_AutoR_Banner_Display = 1;
						
						! *** Error register ***
						g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_AuSoftW_TqSpProfile_StepCountCheckFail;		
						g_Alarm_Register_Request_Bit = 1;
						
						! *** MMI Show Message					
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] UsingProfile - [Alarm] T/N Profile is Empty.", BufferProgrmaNumber;	END;
						 DISP "[#%d buffer] UsingProfile - [Alarm] T/N Profile is Empty.", BufferProgrmaNumber;
					ELSE		
						! T/N Profile Data Copy
						COPY(g_PC_Write_Data_TqSpProfile, g_PC_Process_Data_TqSpProfile);
						
						! *** MMI Show Message					
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] UsingProfile - [cmp] T/N Profile Data Using.", BufferProgrmaNumber;	END;
						DISP "[#%d buffer] UsingProfile - [cmp] T/N Profile Data Using.", BufferProgrmaNumber;
					END ! IF((MAX(g_PC_Write_Data_TqSpProfile) = 0) & (MIN(g_PC_Write_Data_TqSpProfile) = 0))
				END ! IF(g_PC_Write_Data(g_index_PcWrite_ProfileWrite + 9) <> 1 )
			END ! IF(g_PC_Write_Data(g_index_PcWrite_ProfileWrite + 0xA) = 1 )
			
			! ----------------------------------------------------------------------------------------------
			
			! N/T Profile Data CopyUsing CMD
			IF(profile_Current_UsingCMD.3 = 1 )
				IF(g_PC_Write_Data(g_index_PcWrite_ProfileWrite + 0xD) <> 1 )
					g_Hmi_Banner_MsgLineNum = 18; g_Bf8_AutoR_Banner_Display = 1;
					
					! *** Error register ***
					g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_AuSoftW_SpTqUsingProfile_WriteCompleteCheckFail;		
					g_Alarm_Register_Request_Bit = 1;
					
					! *** MMI Show Message					
					IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer]UsingProfile - [Alarm] Profile Write Complete Bit Off", BufferProgrmaNumber;END;
					DISP "[#%d buffer]UsingProfile - [Alarm] Profile Write Complete Bit Off", BufferProgrmaNumber;
				ELSE
					
					IF((MAX(g_PC_Write_Data_SpTqProfile) = 0) & (MIN(g_PC_Write_Data_SpTqProfile) = 0))
						g_Hmi_Banner_MsgLineNum = 18; g_Bf8_AutoR_Banner_Display = 1;
						
						! *** Error register ***
						g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_AuSoftW_SpTqProfile_StepCountCheckFail ;		
						g_Alarm_Register_Request_Bit = 1;
						
						! *** MMI Show Message					
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer]UsingProfile - [Alarm] N/T Profile is Empty.", BufferProgrmaNumber;END;
						DISP "[#%d buffer]UsingProfile - [Alarm] N/T Profile is Empty.", BufferProgrmaNumber;
					ELSE		
						! N/T Profile Data Copy
						COPY(g_PC_Write_Data_SpTqProfile, g_PC_Process_Data_SpTqProfile);
						
						! *** MMI Show Message					
						IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer]UsingProfile - [cmp] N/T Profile Data Using.", BufferProgrmaNumber;END;
						DISP "[#%d buffer]UsingProfile - [cmp] N/T Profile Data Using.", BufferProgrmaNumber;
					END ! IF(g_PC_Write_Data_SpTqProfile(0) < 0)
				END ! IF(g_PC_Write_Data(g_index_PcWrite_ProfileWrite + 0xD) <> 1 )
			END ! IF(g_PC_Write_Data(g_index_PcWrite_ProfileWrite + 0xE) = 1 )
		
		END ! IF(g_AutoMd_ProfilePlay_Enable = 1)
		
		! *** MMI Show Message					
		IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] UsingProfile Subroutine End. ", BufferProgrmaNumber;	END;
		
	END	!IF(profile_Current_UsingCMD <> profile_Prevous_UsingCMD)
RET	

! ----------------------------------------------------------------------------------------------

sr_AutomationSWCommand_CtrlModeChgCommand_Process:	
	
	! *** Automatic Control Command Update
	AutoMd_Current_Control_CMD = 0;
	IF(g_PC_Write_Data(g_index_PcWrite_ControlCMD + 0x0) = 1)AutoMd_Current_Control_CMD.index_AutoMd_CtrlCmd_Init = 1; END; ! Control INIT >>> 1
	IF(g_PC_Write_Data(g_index_PcWrite_ControlCMD + 0x1) = 1)AutoMd_Current_Control_CMD.index_AutoMd_CtrlCmd_Idle = 1; END; ! Control IDLE >>> 2
	IF(g_PC_Write_Data(g_index_PcWrite_ControlCMD + 0x2) = 1)AutoMd_Current_Control_CMD.index_AutoMd_CtrlCmd_Run = 1; END; ! Control Run >>> 4
	IF(g_PC_Write_Data(g_index_PcWrite_ControlCMD + 0x3) = 1)AutoMd_Current_Control_CMD.index_AutoMd_CtrlCmd_Stop = 1; END; ! Control Stop >>> 8
	IF(g_PC_Write_Data(g_index_PcWrite_ControlCMD + 0x4) = 1)AutoMd_Current_Control_CMD.index_AutoMd_CtrlCmd_Pause = 1; END; ! Control Pause >>> 16
	IF(g_PC_Write_Data(g_index_PcWrite_ControlCMD + 0x5) = 1)AutoMd_Current_Control_CMD.index_AutoMd_CtrlCmd_Resume = 1; END; ! Control Resume
	
	! ----------------------------------------------------------------------------------------------

	! *** Check Command Changing 
	IF(AutoMd_Current_Control_CMD <> AutoMd_Previous_Control_CMD)
		
		AutoMd_Previous_Control_CMD = AutoMd_Current_Control_CMD;
		
		! *** PC Software Control Run
		IF(AutoMd_Current_Control_CMD.index_AutoMd_CtrlCmd_Run = 1)
			! *** Control Mode Check
			IF(g_ControlMode <> g_CtMo_enum_Automatic)
				! *** the control mode is not manual! Banner display.
				g_Hmi_Banner_MsgLineNum = 17; g_Bf8_AutoR_Banner_Display = 1;
				
				! *** MMI Message							
				IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] Automatic Control On Fail - Not Automatic Mode.", BufferProgrmaNumber; END;		
					
				! *** Error register ***
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_Automatic_CtrlOn_AutomaticModeCheckFail;		
				g_Alarm_Register_Request_Bit = 1;
			ELSE	
				! *** Bit Check (Control Run)
				IF(AutoMd_Current_Control_CMD - POW(2,index_AutoMd_CtrlCmd_Run) = 0)
				
					g_AutoMd_ProfilePlay_Enable = 1;
					g_Dyno_PidControl_StateMachine = 500;
					g_Eng_PidControl_StateMachine = 500;
					! *** MMI Show Message							
					IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] Automatic Command - [cmp]Control On.", BufferProgrmaNumber; END;
					
				ELSE
					g_AutoMd_ProfilePlay_Enable = 0;
					
					! *** the control mode is not manual! Banner display.
					g_Hmi_Banner_MsgLineNum = 16; g_Bf8_AutoR_Banner_Display = 1;
					
					! *** MMI Message							
					IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] Status of Automatic Control On/Off Bit is Fail.", BufferProgrmaNumber; END;		
					
					! *** Error register ***
					g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_Automatic_ControlOnOffBit_Fail;		
					g_Alarm_Register_Request_Bit = 1;
				END ! IF((g_PC_Write_Data(g_index_PcWrite_ControlCMD + 0x2) = 1) | (g_PC_Write_Data(g_index_PcWrite_ControlCMD + 0x3) = 0))
			END ! IF(g_ControlMode <> g_CtMo_enum_Automatic)
		END ! IF(AutoMd_Current_Control_CMD.index_AutoMd_CtrlCmd_Run = 1)
		
		
		! *** PC Software Control STOP
		IF(AutoMd_Current_Control_CMD.index_AutoMd_CtrlCmd_Stop = 1)
			! *** Control Mode Check
			IF(g_ControlMode <> g_CtMo_enum_Automatic)
				! *** the control mode is not manual! Banner display.
				g_Hmi_Banner_MsgLineNum = 17; g_Bf8_AutoR_Banner_Display = 1;
				
				! *** MMI Message							
				IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] Automatic Control On Fail - Not Automatic Mode.", BufferProgrmaNumber; END;		
					
				! *** Error register ***
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_Automatic_CtrlOn_AutomaticModeCheckFail;		
				g_Alarm_Register_Request_Bit = 1;
			ELSE	
			
				! *** Control Stop On
				IF(AutoMd_Current_Control_CMD - POW(2,index_AutoMd_CtrlCmd_Stop) = 0)
				
					g_AutoMd_ProfilePlay_Enable = 0;
					
					! *** MMI Show Message							
					IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] Automatic Command - [cmp]Control Off.", BufferProgrmaNumber; END;
					
!				ELSE
!					g_AutoMd_ProfilePlay_Enable = 0;
					
					! *** the control mode is not manual! Banner display.
!					g_Hmi_Banner_MsgLineNum = 16; g_Bf8_AutoR_Banner_Display = 1;
					
					! *** MMI Message							
!					IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] Status of Automatic Control On/Off Bit is Fail.", BufferProgrmaNumber; END;		
					
					! *** Error register ***
!					g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_Automatic_ControlOnOffBit_Fail;		
!					g_Alarm_Register_Request_Bit = 1;
				END ! IF((g_PC_Write_Data(g_index_PcWrite_ControlCMD + 0x2) = 1) | (g_PC_Write_Data(g_index_PcWrite_ControlCMD + 0x3) = 0))
			END ! IF(g_ControlMode <> g_CtMo_enum_Automatic)			
			
		END ! IF(AutoMd_Current_Control_CMD.index_AutoMd_CtrlCmd_Run = 1)	
		
	END ! IF(AutoMd_Previous_CtrlOn_CMD <> g_PC_Write_Data(g_index_PcWrite_ControlCMD + 0x2))	

RET	

! ----------------------------------------------------------------------------------------------

sr_AutomationSWCommand_MeasurEqpControl_Process:	
	
	! *** Automatic Control Command Update		
	MesureEqp_BlowByMt_Current_Control_CMD = 0;	
	MesureEqp_SmokeMt_Current_Control_CMD = 0;	
	MesureEqp_FuelMt_Current_Control_CMD = 0;
	
	IF(g_PC_Write_Data(g_index_PcWrite_MeasureEqpCMD + 0x0) = 1)MesureEqp_BlowByMt_Current_Control_CMD.index_MesureEqp_CtrlCmd_Reset = 1; END; 
	IF(g_PC_Write_Data(g_index_PcWrite_MeasureEqpCMD + 0x1) = 1)MesureEqp_BlowByMt_Current_Control_CMD.index_MesureEqp_CtrlCmd_MeasureStart = 1; END; 
	IF(g_PC_Write_Data(g_index_PcWrite_MeasureEqpCMD + 0x2) = 1)MesureEqp_BlowByMt_Current_Control_CMD.index_MesureEqp_CtrlCmd_MeasureStop = 1; END;

	IF(g_PC_Write_Data(g_index_PcWrite_MeasureEqpCMD + 0x4) = 1)MesureEqp_SmokeMt_Current_Control_CMD.index_MesureEqp_CtrlCmd_Reset = 1; END; 
	IF(g_PC_Write_Data(g_index_PcWrite_MeasureEqpCMD + 0x5) = 1)MesureEqp_SmokeMt_Current_Control_CMD.index_MesureEqp_CtrlCmd_MeasureStart = 1; END; 
	IF(g_PC_Write_Data(g_index_PcWrite_MeasureEqpCMD + 0x6) = 1)MesureEqp_SmokeMt_Current_Control_CMD.index_MesureEqp_CtrlCmd_MeasureStop = 1; END;

	IF(g_PC_Write_Data(g_index_PcWrite_MeasureEqpCMD + 0x8) = 1)MesureEqp_FuelMt_Current_Control_CMD.index_MesureEqp_CtrlCmd_Reset = 1; END; 
	IF(g_PC_Write_Data(g_index_PcWrite_MeasureEqpCMD + 0x9) = 1)MesureEqp_FuelMt_Current_Control_CMD.index_MesureEqp_CtrlCmd_MeasureStart = 1; END; 
	IF(g_PC_Write_Data(g_index_PcWrite_MeasureEqpCMD + 0xA) = 1)MesureEqp_FuelMt_Current_Control_CMD.index_MesureEqp_CtrlCmd_MeasureStop = 1; END;

	! ----------------------------------------------------------------------------------------------

	! *** Check Command Changing - Blow By Meter
	IF(MesureEqp_BlowByMt_Current_Control_CMD <> MesureEqp_BlowByMt_Previous_Control_CMD)
		
		MesureEqp_BlowByMt_Previous_Control_CMD = MesureEqp_BlowByMt_Current_Control_CMD;
		
		! *** Measure Equipmnt Reset
		IF(MesureEqp_BlowByMt_Current_Control_CMD.index_MesureEqp_CtrlCmd_Reset = 1)
			! *** Measure CMD Check
			IF((MesureEqp_BlowByMt_Current_Control_CMD - POW(2, index_MesureEqp_CtrlCmd_Reset)) <> 0)			
				! *** the control mode is not manual! Banner display.
				g_Hmi_Banner_MsgLineNum = 28; g_Bf8_AutoR_Banner_Display = 1;
				
				! *** MMI Message							
				DISP "[#%d buffer] Blow By Meter Reset Fail! - Other Request On", BufferProgrmaNumber;		
					
				! *** Error register ***
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_MeasurEqp_bbm_ResetFail_OtherRequestOn;		
				g_Alarm_Register_Request_Bit = 1;
				
				! *** Update Error Code (to read Automation SW)
				g_BlowByMeter_442_ErrorCode = enum_aID_Buf3_W_MeasurEqp_bbm_ResetFail_OtherRequestOn;
				! *** Update Measurement Equipment Status (to read Automation SW)
				g_BlowByMeter_442_Status = g_enum_MesureEqpStatus_Error;
				
			ELSE	
				! *** Blow By Meter Reset Request
				g_BlowByMeter_442_CtrlCMD =	g_enum_MesureEqpCmd_InitRequest
				
				! *** MMI Message	
				DISP "[#%d buffer] The Automation SW request Blow By Meter to reset!", BufferProgrmaNumber;		
				
			END ! IF(MesureEqp_Current_Control_CMD.index_MesureEqp_CtrlCmd_BlowBy_Measure = 1)
		END ! IF(MesureEqp_Current_Control_CMD.index_MesureEqp_CtrlCmd_BlowBy_Reset = 1)	
	
		! ----------------------------------------------------------------------------------------------

		! *** Measure Equipmnt Measurement Start
		IF(MesureEqp_BlowByMt_Current_Control_CMD.index_MesureEqp_CtrlCmd_MeasureStart = 1)
			! *** Measure CMD Check
			IF((MesureEqp_BlowByMt_Current_Control_CMD - POW(2, index_MesureEqp_CtrlCmd_MeasureStart)) <> 0)			
				! *** the control mode is not manual! Banner display.
				g_Hmi_Banner_MsgLineNum = 30; g_Bf8_AutoR_Banner_Display = 1;
				
				! *** MMI Message							
				DISP "[#%d buffer] Blow By Meter Measurement Start Fail! - Other Request On", BufferProgrmaNumber;		
					
				! *** Error register ***
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_MeasurEqp_bbm_MeasurStartFail_OtherRequestOn;		
				g_Alarm_Register_Request_Bit = 1;
				
				! *** Update Error Code (to read Automation SW)
				g_BlowByMeter_442_ErrorCode = enum_aID_Buf3_W_MeasurEqp_bbm_MeasurStartFail_OtherRequestOn;
				! *** Update Measurement Equipment Status (to read Automation SW)
				g_BlowByMeter_442_Status = g_enum_MesureEqpStatus_Error;
				
			ELSE	
				! *** Blow By Meter Reset Request
				g_BlowByMeter_442_CtrlCMD =	g_enum_MesureEqpCmd_MeasurementRequest
				
				! *** MMI Message							
				DISP "[#%d buffer] The Automation SW request Blow By Meter to start measurement!", BufferProgrmaNumber;		
				
			END ! IF(MesureEqp_Current_Control_CMD.index_MesureEqp_CtrlCmd_BlowBy_Measure = 1)
		END ! IF(MesureEqp_Current_Control_CMD.index_MesureEqp_CtrlCmd_BlowBy_Reset = 1)	
	
		! ----------------------------------------------------------------------------------------------
	
		! *** Measure Equipmnt Measurement Stop
		IF(MesureEqp_BlowByMt_Current_Control_CMD.index_MesureEqp_CtrlCmd_MeasureStop = 1)
			! *** Measure CMD Check
			IF((MesureEqp_BlowByMt_Current_Control_CMD - POW(2, index_MesureEqp_CtrlCmd_MeasureStop)) <> 0)			
				! *** the control mode is not manual! Banner display.
				g_Hmi_Banner_MsgLineNum = 31; g_Bf8_AutoR_Banner_Display = 1;
				
				! *** MMI Message							
				DISP "[#%d buffer] Blow By Meter Measurement Stop Fail! - Other Request On", BufferProgrmaNumber;		
					
				! *** Error register ***
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_MeasurEqp_bbm_MeasurStopFail_OtherRequestOn;		
				g_Alarm_Register_Request_Bit = 1;
				
				! *** Update Error Code (to read Automation SW)
				g_BlowByMeter_442_ErrorCode = enum_aID_Buf3_W_MeasurEqp_bbm_MeasurStopFail_OtherRequestOn;
				! *** Update Measurement Equipment Status (to read Automation SW)
				g_BlowByMeter_442_Status = g_enum_MesureEqpStatus_Error;
				
			ELSE	
				! *** Blow By Meter Reset Request
				g_BlowByMeter_442_CtrlCMD =	g_enum_MesureEqpCmd_MeasurStopRequest;
				
				! *** MMI Message							
				DISP "[#%d buffer] The Automation SW request Blow By Meter to stop measurement!", BufferProgrmaNumber;		
				
			END ! IF(MesureEqp_Current_Control_CMD.index_MesureEqp_CtrlCmd_BlowBy_Measure = 1)
		END ! IF(MesureEqp_Current_Control_CMD.index_MesureEqp_CtrlCmd_BlowBy_Reset = 1)	
	
		! ----------------------------------------------------------------------------------------------
	
	END ! IF(MesureEqp_Current_Control_CMD <> MesureEqp_Previous_Control_CMD)
	
	
	! ----------------------------------------------------------------------------------------------

	! *** Check Command Changing - Smoke Meter
	IF(MesureEqp_SmokeMt_Current_Control_CMD <> MesureEqp_SmokeMt_Previous_Control_CMD)
		
		MesureEqp_SmokeMt_Previous_Control_CMD = MesureEqp_SmokeMt_Current_Control_CMD;
		
		! *** Measure Equipmnt Reset
		IF(MesureEqp_SmokeMt_Current_Control_CMD.index_MesureEqp_CtrlCmd_Reset = 1)
			! *** Measure CMD Check
			IF((MesureEqp_SmokeMt_Current_Control_CMD - POW(2, index_MesureEqp_CtrlCmd_Reset)) <> 0)			
				! *** the control mode is not manual! Banner display.
				g_Hmi_Banner_MsgLineNum = 33; g_Bf8_AutoR_Banner_Display = 1;
				
				! *** MMI Message							
				DISP "[#%d buffer]Smoke Meter Reset Fail! - Other Request On", BufferProgrmaNumber;		
					
				! *** Error register ***
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_MeasurEqp_smkm_ResetFail_OtherRequestOn ;		
				g_Alarm_Register_Request_Bit = 1;
				
				! *** Update Error Code (to read Automation SW)
				g_SmokeMeter_415_ErrorCode = enum_aID_Buf3_W_MeasurEqp_smkm_ResetFail_OtherRequestOn;
				! *** Update Measurement Equipment Status (to read Automation SW)
				g_SmokeMeter_415_Status = g_enum_MesureEqpStatus_Error;
				
			ELSE	
				! *** Smoke Meter Reset Request
				g_SmokeMeter_415_CtrlCMD =	g_enum_MesureEqpCmd_InitRequest
				
				! *** MMI Message	
				DISP "[#%d buffer] The Automation SW request Smoke Meter to reset!", BufferProgrmaNumber;		
				
			END ! IF(MesureEqp_Current_Control_CMD.index_MesureEqp_CtrlCmd_BlowBy_Measure = 1)
		END ! IF(MesureEqp_Current_Control_CMD.index_MesureEqp_CtrlCmd_BlowBy_Reset = 1)	
	
		! ----------------------------------------------------------------------------------------------

		! *** Measure Equipmnt Measurement Start
		IF(MesureEqp_SmokeMt_Current_Control_CMD.index_MesureEqp_CtrlCmd_MeasureStart = 1)
			! *** Measure CMD Check
			IF((MesureEqp_SmokeMt_Current_Control_CMD - POW(2, index_MesureEqp_CtrlCmd_MeasureStart)) <> 0)			
				! *** the control mode is not manual! Banner display.
				g_Hmi_Banner_MsgLineNum = 35; g_Bf8_AutoR_Banner_Display = 1;
				
				! *** MMI Message							
				DISP "[#%d buffer] Smoke Meter Measurement Start Fail! - Other Request On", BufferProgrmaNumber;		
					
				! *** Error register ***
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_MeasurEqp_smkm_MeasurStartFail_OtherRequestOn;		
				g_Alarm_Register_Request_Bit = 1;
				
				! *** Update Error Code (to read Automation SW)
				g_SmokeMeter_415_ErrorCode = enum_aID_Buf3_W_MeasurEqp_smkm_MeasurStartFail_OtherRequestOn;
				! *** Update Measurement Equipment Status (to read Automation SW)
				g_SmokeMeter_415_Status = g_enum_MesureEqpStatus_Error;
				
			ELSE	
				! *** Blow By Meter Reset Request
				g_SmokeMeter_415_CtrlCMD =	g_enum_MesureEqpCmd_MeasurementRequest
				
				! *** MMI Message							
				DISP "[#%d buffer] The Automation SW request Smoke Meter to start measurement!", BufferProgrmaNumber;		
				
			END ! IF(MesureEqp_Current_Control_CMD.index_MesureEqp_CtrlCmd_BlowBy_Measure = 1)
		END ! IF(MesureEqp_Current_Control_CMD.index_MesureEqp_CtrlCmd_BlowBy_Reset = 1)	
	
		! ----------------------------------------------------------------------------------------------
	
		! *** Measure Equipmnt Measurement Stop
		IF(MesureEqp_SmokeMt_Current_Control_CMD.index_MesureEqp_CtrlCmd_MeasureStop = 1)
			! *** Measure CMD Check
			IF((MesureEqp_SmokeMt_Current_Control_CMD - POW(2, index_MesureEqp_CtrlCmd_MeasureStop)) <> 0)			
				! *** the control mode is not manual! Banner display.
				g_Hmi_Banner_MsgLineNum = 36; g_Bf8_AutoR_Banner_Display = 1;
				
				! *** MMI Message							
				DISP "[#%d buffer] Smoke Meter Measurement Stop Fail! - Other Request On", BufferProgrmaNumber;		
					
				! *** Error register ***
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_MeasurEqp_smkm_MeasurStopFail_OtherRequestOn;		
				g_Alarm_Register_Request_Bit = 1;
				
				! *** Update Error Code (to read Automation SW)
				g_SmokeMeter_415_ErrorCode = enum_aID_Buf3_W_MeasurEqp_smkm_MeasurStopFail_OtherRequestOn;
				! *** Update Measurement Equipment Status (to read Automation SW)
				g_SmokeMeter_415_Status = g_enum_MesureEqpStatus_Error;
				
			ELSE	
				! *** Blow By Meter Reset Request
				g_SmokeMeter_415_CtrlCMD =	g_enum_MesureEqpCmd_MeasurStopRequest;
				
				! *** MMI Message							
				DISP "[#%d buffer] The Automation SW request Smoke Meter to stop measurement!", BufferProgrmaNumber;		
				
			END ! IF(MesureEqp_Current_Control_CMD.index_MesureEqp_CtrlCmd_BlowBy_Measure = 1)
		END ! IF(MesureEqp_Current_Control_CMD.index_MesureEqp_CtrlCmd_BlowBy_Reset = 1)		
	END ! IF(MesureEqp_Current_Control_CMD <> MesureEqp_Previous_Control_CMD)
	
	! ----------------------------------------------------------------------------------------------

	! *** Check Command Changing - Fuel Meter
	IF(MesureEqp_FuelMt_Current_Control_CMD <> MesureEqp_FuelMt_Previous_Control_CMD)
		
		MesureEqp_FuelMt_Previous_Control_CMD = MesureEqp_FuelMt_Current_Control_CMD;
		
		! *** Measure Equipmnt Reset
		IF(MesureEqp_FuelMt_Current_Control_CMD.index_MesureEqp_CtrlCmd_Reset = 1)
			! *** Measure CMD Check
			IF((MesureEqp_FuelMt_Current_Control_CMD - POW(2, index_MesureEqp_CtrlCmd_Reset)) <> 0)			
				! *** the control mode is not manual! Banner display.
				g_Hmi_Banner_MsgLineNum = 37; g_Bf8_AutoR_Banner_Display = 1;
				
				! *** MMI Message							
				DISP "[#%d buffer]Fuel Meter Reset Fail! - Other Request On", BufferProgrmaNumber;		
					
				! *** Error register ***
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_MeasurEqp_FuelMeter_ResetFail_OtherRequestOn;		
				g_Alarm_Register_Request_Bit = 1;
				
				! *** Update Error Code (to read Automation SW)
				g_FuelMeter_733_ErrorCode = enum_aID_Buf3_W_MeasurEqp_FuelMeter_ResetFail_OtherRequestOn;
				! *** Update Measurement Equipment Status (to read Automation SW)
				g_FuelMeter_733_Status = g_enum_MesureEqpStatus_Error;
				
			ELSE	
				! *** Fuel Meter Reset Request
				g_FuelMeter_733_CtrlCMD =	g_enum_MesureEqpCmd_InitRequest
				
				! *** MMI Message	
				DISP "[#%d buffer] The Automation SW request Fuel Meter to reset!", BufferProgrmaNumber;		
				
			END ! IF(MesureEqp_Current_Control_CMD.index_MesureEqp_CtrlCmd_BlowBy_Measure = 1)
		END ! IF(MesureEqp_Current_Control_CMD.index_MesureEqp_CtrlCmd_BlowBy_Reset = 1)	
	
		! ----------------------------------------------------------------------------------------------

		! *** Measure Equipmnt Measurement Start
		IF(MesureEqp_FuelMt_Current_Control_CMD.index_MesureEqp_CtrlCmd_MeasureStart = 1)
			! *** Measure CMD Check
			IF((MesureEqp_FuelMt_Current_Control_CMD - POW(2, index_MesureEqp_CtrlCmd_MeasureStart)) <> 0)			
				! *** the control mode is not manual! Banner display.
				g_Hmi_Banner_MsgLineNum = 39; g_Bf8_AutoR_Banner_Display = 1;
				
				! *** MMI Message							
				DISP "[#%d buffer] Fuel Meter Measurement Start Fail! - Other Request On", BufferProgrmaNumber;		
					
				! *** Error register ***
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_MeasurEqp_FuelMeter_MeasurStartFail_OtherRequestOn;		
				g_Alarm_Register_Request_Bit = 1;
				
				! *** Update Error Code (to read Automation SW)
				g_FuelMeter_733_ErrorCode = enum_aID_Buf3_W_MeasurEqp_FuelMeter_MeasurStartFail_OtherRequestOn;
				! *** Update Measurement Equipment Status (to read Automation SW)
				g_FuelMeter_733_Status = g_enum_MesureEqpStatus_Error;
				
			ELSE	
				! *** Fuel Meter Reset Request
				g_FuelMeter_733_CtrlCMD =	g_enum_MesureEqpCmd_MeasurementRequest
				
				! *** MMI Message							
				DISP "[#%d buffer] The Automation SW request Fuel Meter to start measurement!", BufferProgrmaNumber;		
				
			END ! IF(MesureEqp_Current_Control_CMD.index_MesureEqp_CtrlCmd_BlowBy_Measure = 1)
		END ! IF(MesureEqp_Current_Control_CMD.index_MesureEqp_CtrlCmd_BlowBy_Reset = 1)	
	
		! ----------------------------------------------------------------------------------------------
	
		! *** Measure Equipmnt Measurement Stop
		IF(MesureEqp_FuelMt_Current_Control_CMD.index_MesureEqp_CtrlCmd_MeasureStop = 1)
			! *** Measure CMD Check
			IF((MesureEqp_FuelMt_Current_Control_CMD - POW(2, index_MesureEqp_CtrlCmd_MeasureStop)) <> 0)			
				! *** the control mode is not manual! Banner display.
				g_Hmi_Banner_MsgLineNum = 40; g_Bf8_AutoR_Banner_Display = 1;
				
				! *** MMI Message							
				DISP "[#%d buffer] Fuel Meter Measurement Stop Fail! - Other Request On", BufferProgrmaNumber;		
					
				! *** Error register ***
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf3_W_MeasurEqp_FuelMeter_MeasurStopFail_OtherRequestOn;		
				g_Alarm_Register_Request_Bit = 1;
				
				! *** Update Error Code (to read Automation SW)
				g_FuelMeter_733_ErrorCode = enum_aID_Buf3_W_MeasurEqp_FuelMeter_MeasurStopFail_OtherRequestOn;
				! *** Update Measurement Equipment Status (to read Automation SW)
				g_FuelMeter_733_Status = g_enum_MesureEqpStatus_Error;
				
			ELSE	
				! *** Blow By Meter Reset Request
				g_FuelMeter_733_CtrlCMD =	g_enum_MesureEqpCmd_MeasurStopRequest;
				
				! *** MMI Message							
				DISP "[#%d buffer] The Automation SW request Fuel Meter to stop measurement!", BufferProgrmaNumber;		
				
			END ! IF(MesureEqp_Current_Control_CMD.index_MesureEqp_CtrlCmd_BlowBy_Measure = 1)
		END ! IF(MesureEqp_Current_Control_CMD.index_MesureEqp_CtrlCmd_BlowBy_Reset = 1)		
	END ! IF(MesureEqp_Current_Control_CMD <> MesureEqp_Previous_Control_CMD)
	
	
RET	


! ----------------------------------------------------------------------------------------------

sr_AutomationSWCommand_StopCommand_Process:	
		
	! *** ATOM Software Stop Command Update
	AtomSwStop_Current_Control_CMD = 0;
	IF(g_PC_Write_Data(g_index_PcWrite_ControlCMD + 0xD) = 1)AtomSwStop_Current_Control_CMD.index_AtomSwStop_CtrlCmd_Idle = 1; END; ! Idle >>> 1
	IF(g_PC_Write_Data(g_index_PcWrite_ControlCMD + 0xE) = 1)AtomSwStop_Current_Control_CMD.index_AtomSwStop_CtrlCmd_Soft = 1; END; ! Soft >>> 2
	IF(g_PC_Write_Data(g_index_PcWrite_ControlCMD + 0xF) = 1)AtomSwStop_Current_Control_CMD.index_AtomSwStop_CtrlCmd_Hard = 1; END; ! Hard >>> 4
	
	! ----------------------------------------------------------------------------------------------

	! *** Check Command Changing 
	IF(AtomSwStop_Current_Control_CMD <> AtomSwStop_Previous_Control_CMD)
		
		AtomSwStop_Previous_Control_CMD = AtomSwStop_Current_Control_CMD;
		
		! *** PC Software Idle Stop
		IF(AtomSwStop_Current_Control_CMD.index_AtomSwStop_CtrlCmd_Idle = 1)
						
			g_stop_StopMode = g_stop_enum_IdleStopping;
			
			DrMn_ManualMode_Previous_ChangeReq_Input = g_DrMn_enum_Default; 
			DrMn_ManualMode_Current_ChangeReq_Input = g_DrMn_enum_Default;
			g_DriveManual_Mode = g_DrMn_enum_Default;

			! *** MMI Show Message							
			IF (g_Buffer3_displayCmdLevel <> 0); DISP "[#%d buffer] Automatic Command - [cmp]Idle Stop.", BufferProgrmaNumber; END;
		
		END ! IF(AtomSwStop_Current_Control_CMD.index_AtomSwStop_CtrlCmd_Idle = 1)
		
	END ! IF(AtomStop_Current_Control_CMD <> AtomStop_Previous_Control_CMD)

RET	

! ----------------------------------------------------------------------------------------------

#4
! *** Dynamometer & Engine PID Controller Management Program

INT BufferProgrmaNumber
	BufferProgrmaNumber = 4
		
REAL SPEEDUPDATEDELTA;
REAL LASTSPEEDUPDATETIME;
	LASTSPEEDUPDATETIME = TIME;

! *** MMI Show Message
DISP "[#%d buffer] program started.", BufferProgrmaNumber;

GLOBAL INT g_Buffer4_displayCmdLevel;
		g_Buffer4_displayCmdLevel = 0;
		
! Alarm ID Define (Warning Alarm ID = (Buffer No * 10000) + Warning No, Error Alarm ID = Warning Alarm ID * -1)	


INT enum_aID_Buf4_E_IdleMode_EngDynoPID_NotCompiles;
	enum_aID_Buf4_E_IdleMode_EngDynoPID_NotCompiles = -40001;
INT enum_aID_Buf4_E_IdleMode_DynoPIDStopCMD_Fail;
	enum_aID_Buf4_E_IdleMode_DynoPIDStopCMD_Fail = -40002;
INT enum_aID_Buf4_E_IdleMode_EnginePIDStartCMD_Fail;
	enum_aID_Buf4_E_IdleMode_EnginePIDStartCMD_Fail = -40003;

INT enum_aID_Buf4_E_ExceptIdleMode_EngDynoPID_NotCompiles;
	enum_aID_Buf4_E_ExceptIdleMode_EngDynoPID_NotCompiles = -40011;
INT enum_aID_Buf4_E_ExceptIdleMode_DynoPIDStartCMD_Fail;
	enum_aID_Buf4_E_ExceptIdleMode_DynoPIDStartCMD_Fail = -40012;
INT enum_aID_Buf4_E_ExceptIdleMode_EnginePIDStartCMD_Fail;
	enum_aID_Buf4_E_ExceptIdleMode_EnginePIDStartCMD_Fail = -40013;

	
INT enum_aID_Buf4_E_OpStopCmd_EngPID_StopFail;
	enum_aID_Buf4_E_OpStopCmd_EngPID_StopFail = -40021;
INT enum_aID_Buf4_E_OpStopCmd_DynoPID_StopFail;
	enum_aID_Buf4_E_OpStopCmd_DynoPID_StopFail = -40022;

INT enum_aID_Buf4_E_EngineStart_DynoPIDStartCMD_Fail;
	enum_aID_Buf4_E_EngineStart_DynoPIDStartCMD_Fail = -40031;
INT enum_aID_Buf4_E_EngineStart_DynoPID_NotCompiles;
	enum_aID_Buf4_E_EngineStart_DynoPID_NotCompiles = -40032;

INT RPM_ERROR_Count;
RPM_ERROR_Count = 1;
! *** Fixed-time Measure
	REAL PeriodicControlTime_Measure_1;
	REAL PeriodicControlTime_Measure_2;
	REAL PeriodicControlTime_Difference;
	REAL PeriodicControlTime_ProgExcution_Interval;
	
		PeriodicControlTime_Measure_1 = 0;
		PeriodicControlTime_Measure_2 = 0;
		PeriodicControlTime_Difference = 0;
		PeriodicControlTime_ProgExcution_Interval = 2; ! [msec]
! +++ Set Value Average

	int index_Average_TargetLastIndex;
	index_Average_TargetLastIndex = 149;
	REAL Target_Dyno_Torque;
	REAL Target_Dyno_Speed;
	REAL Target_Engine_AlphaPosi;
	REAL Target_Engine_Speed;
	REAL Target_Engine_Torque;
	REAL Average_Target_Dyno_TorqueList(150);
	REAL Average_Target_Dyno_SpeedList(150);
	REAL Average_Target_Eng_AlphaPosiList(150);
	REAL Average_Target_Engine_SpeedList(150);
	REAL Average_Target_Engine_TorqueList(150);
	FILL(0, Average_Target_Dyno_TorqueList);
	FILL(0, Average_Target_Dyno_SpeedList);
	FILL(0, Average_Target_Eng_AlphaPosiList);
	FILL(0, Average_Target_Engine_SpeedList);
	FILL(0, Average_Target_Engine_TorqueList);

! *** Feedback Speed Calculation variable	
	REAL Calc_Rpm_Now_Frequency;
	REAL Calc_Rpm_FrequencyToRpm_CalcResult;
	REAL Calc_Rpm_VoltToRpm_CalcResult;
	REAL Average_SpeedList(50);	
	REAL Average_OutputSpeedList(150);	
	INT index_Average_SpeedListLastIndex;
	INT index_Average_OutputSpeedListLastIndex;
		FILL(0, Average_SpeedList);
		FILL(0, Average_OutputSpeedList);
		index_Average_SpeedListLastIndex = 49;
		index_Average_OutputSpeedListLastIndex = 149;
		
! *** Feedback Throttle Actuator	Position Calculation variable	
	REAL Calc_Throttle_VoltToPosition_CalcResult;	
! *** Power Calculation variable	
	REAL Calc_Power_Kw_CalcResult;	
! *** Feedbck Current Calculation variable	
	REAL Calc_Current_A_CalcResult;	
! *** Dyno. Engine PID Program Control
	INT DrMnCtrl_PreviousDriveMode;
		DrMnCtrl_PreviousDriveMode = g_DrMn_enum_Default;
		
	INT SysCmdCtrl_PreviousStopCmd;
		SysCmdCtrl_PreviousStopCmd = -1;
	
! *** PID Program Target Calculation variable	
	REAL pid_Target_Calc_Speed;
	REAL pid_Target_Calc_Torque;	
	REAL pid_Target_Calc_AlphaPosi;	
		pid_Target_Calc_Speed = 0;
		pid_Target_Calc_Torque = 0;
		pid_Target_Calc_AlphaPosi = 0;	
	REAL pid_AlphaCngPerKnobPulse_IntToReal;
		pid_AlphaCngPerKnobPulse_IntToReal = 0;
	REAL pid_CurrentCngPerKnobPulse_IntToReal;
		pid_CurrentCngPerKnobPulse_IntToReal = 0;	
		
! *** Service Control Mode Program Target Calculation variable	
	REAL ServiceMd_Target_Calc_Current;	
	REAL ServiceMd_Target_Calc_CurrentLimitChk;	
	REAL ServiceMd_Target_Calc_AlphaPosi;
	REAL ServiceMd_Target_Calc_Alpha_1_DaVal;
	REAL ServiceMd_Target_Calc_Alpha_2_DaVal;
		ServiceMd_Target_Calc_Current = 0;
		ServiceMd_Target_Calc_CurrentLimitChk = 0;
		ServiceMd_Target_Calc_AlphaPosi = 0;
		ServiceMd_Target_Calc_Alpha_1_DaVal = 0;
		ServiceMd_Target_Calc_Alpha_2_DaVal = 0;

! *** Automatic Control Mode Process		
	! *** Profile Index
	INT currentIndex_AutoMd_DynoProfile;
	INT currentIndex_AutoMd_EngProfile;
	INT nextIndex_AutoMd_DynoProfile;
	INT nextIndex_AutoMd_EngProfile;
		currentIndex_AutoMd_DynoProfile = 0;
		currentIndex_AutoMd_EngProfile = 0;
		nextIndex_AutoMd_DynoProfile = 0;
		nextIndex_AutoMd_EngProfile = 0;
	
	! *** Profile Linear interpolation	
	REAL time_AutoProfilePlay_CurrentTime;
	REAL time_AutoProfilePlay_PreviousTime;
	REAL time_AutoProfileTotal_PlayTime_RealType;
	REAL time_AutoProfileTotal_PlayTime_RoundOff;
	INT time_AutoProfileTotal_PlayTime_IntType;
	REAL AutoMd_Dyno_ProfileTarget;
	REAL AutoMd_Engine_ProfileTarget;
	REAL AutoMd_InterPolation_Gain;
	REAL target_CurrentIndex_Dyno;
	REAL target_NextIndex_Dyno;
	REAL target_CurrentIndex_Engine;
	REAL target_NextIndex_Engine;
	
		AutoMd_InterPolation_Gain = 0;
		time_AutoProfilePlay_CurrentTime = 0;
		time_AutoProfilePlay_PreviousTime = 0;
		time_AutoProfileTotal_PlayTime_RealType = 0;
		time_AutoProfileTotal_PlayTime_RoundOff = 0;
		time_AutoProfileTotal_PlayTime_IntType = 0;
		AutoMd_Dyno_ProfileTarget = 0;
		AutoMd_Engine_ProfileTarget = 0;
		target_CurrentIndex_Dyno = 0;
		target_NextIndex_Dyno = 0;
		target_CurrentIndex_Engine = 0;
		target_NextIndex_Engine = 0;
		
	! *** Type of Test Cycle	
	INT AutoMd_TestCycle;
		AutoMd_TestCycle = 0; ! 0 = Stationary; 1 = Transient Cycle
	REAL AutoMd_StationaryTargetTimeMeasure;
		AutoMd_StationaryTargetTimeMeasure = 0;
		
! *** Program Resule Update
g_bufferProgramResult(0)(BufferProgrmaNumber) = 99999;

WHILE 1

	BLOCK 
	
	PeriodicControlTime_Measure_1 = TIME
				
		IF((PeriodicControlTime_Measure_1 - PeriodicControlTime_Measure_2) >= PeriodicControlTime_ProgExcution_Interval)					
	
			! *** measure the pid execution time.
			PeriodicControlTime_Difference = PeriodicControlTime_Measure_1 - PeriodicControlTime_Measure_2
			PeriodicControlTime_Measure_2 = PeriodicControlTime_Measure_1	
			!DISP PeriodicControlTime_Difference
			! *** HMI Demand Graph Min Variable Zero Set.
			g_Hmi_bs10x_Demand_GraphZeroVal = 0;
			! ----------------------------------------------------------------------------------------------
			! ***
			! *** Dynamometer & Engine Reference Calculation & Update to Global variable			
			! ***	
			! ----------------------------------------------------------------------------------------------
			! ***			
			! *** Stop Mode Reference Update
			! ***
			! *** Stop Mode
			IF((g_stop_StopMode = g_stop_enum_Stopping)| (g_stop_StopMode = g_stop_enum_SoftStopping) | (g_stop_StopMode = g_stop_enum_HardStopping)) 			
				! *** ref
				g_Target_Dyno_Speed = 0;	
				g_Target_Dyno_Torque = 0;					
				g_Target_Engine_Speed = 0; 
				g_Target_Engine_Torque = 0; 												
				g_Target_Engine_AlphaPosi = 0;	
			ELSEIF((g_DriveManual_Mode = g_DrMn_enum_Idle) | (g_DriveManual_Mode = g_DrMn_enum_IdleContrl)) 			
				! *** ref
				g_Target_Dyno_Speed = 0;	
				g_Target_Dyno_Torque = 0;					
				g_Target_Engine_Speed = 0; 
				g_Target_Engine_Torque = 0; 												
				g_Target_Engine_AlphaPosi = g_Hmi_bs3003_Eng_StopAlphaSt;	
				
			ELSEIF((g_SystemState_Eng_Starting = 1) & (g_SystemState_Eng_Started = 0)) 			
				! *** ref
				g_Target_Dyno_Speed = 0;	
				g_Target_Dyno_Torque = g_Hmi_bs3001_Eng_DynoStartTorqueSt;					
				g_Target_Engine_Speed = 0; 
				g_Target_Engine_Torque = 0; 												
				g_Target_Engine_AlphaPosi = g_Hmi_bs3003_Eng_StopAlphaSt;	

			ELSEIF(g_ControlMode = g_CtMo_enum_Remote)
				IF(g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA) 
					! *** ref
					g_Target_Dyno_Speed = 0; ! *** Other mode Target Zero Setting
					g_Target_Engine_Speed = 0; ! *** Other mode Target Zero Setting
					g_Target_Engine_Torque = 0; ! *** Other mode Target Zero Setting

					! *** Demand Caculation from DAS
					IF (g_DI_RemoteOnSignal=1)
						Target_Dyno_Torque = g_AI_Target_Dyno_Remote*g_Hmi_Target_Dyno_Torque_Range;	!g_Hmi_Target_Dyno_Torque_Range=400						
						DSHIFT(Average_Target_Dyno_TorqueList, Target_Dyno_Torque, index_Average_TargetLastIndex);  
						g_Target_Dyno_Torque = AVG(Average_Target_Dyno_TorqueList);

						Target_Engine_AlphaPosi = g_AI_Target_Engine_Remote*g_Hmi_Target_Engine_AlphaPosi_Range;	!Target_Engine_AlphaPosi_Range=100						
						DSHIFT(Average_Target_Eng_AlphaPosiList, Target_Engine_AlphaPosi, index_Average_TargetLastIndex);  
						g_Target_Engine_AlphaPosi = AVG(Average_Target_Eng_AlphaPosiList);

						!DISP "[#%d buffer] g_Target_Dyno_Torque [#%d].", BufferProgrmaNumber,g_Target_Dyno_Torque
						!DISP "[#%d buffer] g_Target_Engine_AlphaPosi [#%d].", BufferProgrmaNumber,g_Target_Engine_AlphaPosi

						! *** HMI Demand Update
						g_Hmi_bs10x_Demand_Torque = g_Target_Dyno_Torque;
						g_Hmi_bs10x_Demand_AlphaPosi = g_Target_Engine_AlphaPosi;
					END

				ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTA)
					! *** ref
					g_Target_Dyno_Torque = 0; ! *** Other mode Target Zero Setting
					g_Target_Engine_Speed = 0; ! *** Other mode Target Zero Setting
					g_Target_Engine_Torque = 0; ! *** Other mode Target Zero Setting
								
					! *** Demand Caculation from DAS 										
					IF (g_DI_RemoteOnSignal=1)
						Target_Dyno_Speed = g_AI_Target_Dyno_Remote*g_Hmi_Target_Dyno_Speed_Range; !g_Hmi_Target_Dyno_Speed_Range=8000;	
						DSHIFT(Average_Target_Dyno_SpeedList, Target_Dyno_Speed, index_Average_TargetLastIndex);  
						g_Target_Dyno_Speed = AVG(Average_Target_Dyno_SpeedList);
						
						Target_Engine_AlphaPosi = g_AI_Target_Engine_Remote*g_Hmi_Target_Engine_AlphaPosi_Range;	!Target_Engine_AlphaPosi_Range=100
						DSHIFT(Average_Target_Eng_AlphaPosiList, Target_Engine_AlphaPosi, index_Average_TargetLastIndex);  
						g_Target_Engine_AlphaPosi = AVG(Average_Target_Eng_AlphaPosiList);

						! *** HMI Demand Update
						g_Hmi_bs10x_Demand_Speed = g_Target_Dyno_Speed;
						g_Hmi_bs10x_Demand_AlphaPosi = g_Target_Engine_AlphaPosi;
					END					
				ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyTo_EngRPM)						
					! *** Demand Caculation from DAS 										
					g_Target_Dyno_Speed = 0; ! *** Other mode Target Zero Setting
					g_Target_Engine_Torque = 0; ! *** Other mode Target Zero Setting
					g_Target_Engine_AlphaPosi = 0; ! *** Other mode Target Zero Setting

					IF (g_DI_RemoteOnSignal=1)
						Target_Dyno_Torque = g_AI_Target_Dyno_Remote*g_Hmi_Target_Dyno_Torque_Range;	!g_Hmi_Target_Dyno_Torque_Range=400	
						DSHIFT(Average_Target_Dyno_TorqueList, Target_Dyno_Torque, index_Average_TargetLastIndex);  
						g_Target_Dyno_Torque = AVG(Average_Target_Dyno_TorqueList);

						Target_Engine_Speed = g_AI_Target_Engine_Remote*g_Hmi_Target_Engine_Speed_Range; !g_Hmi_Target_Engine_Speed_Range=8000;							
						DSHIFT(Average_Target_Engine_SpeedList, Target_Engine_Speed, index_Average_TargetLastIndex);  
						g_Target_Engine_Speed = AVG(Average_Target_Engine_SpeedList);

						! *** HMI Demand Update
						g_Hmi_bs10x_Demand_Torque = g_Target_Dyno_Torque;
						g_Hmi_bs10x_Demand_Speed = g_Target_Engine_Speed;					
					END
				ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTo)						
					g_Target_Dyno_Torque = 0; ! *** Other mode Target Zero Setting
					g_Target_Engine_Speed = 0; ! *** Other mode Target Zero Setting
					g_Target_Engine_AlphaPosi = 0; ! *** Other mode Target Zero Setting

					! *** Demand Caculation from DAS 										
					IF (g_DI_RemoteOnSignal=1)
						Target_Dyno_Speed = g_AI_Target_Dyno_Remote*g_Hmi_Target_Dyno_Speed_Range; !g_Hmi_Target_Dyno_Speed_Range=8000;							
						DSHIFT(Average_Target_Dyno_SpeedList, Target_Dyno_Speed, index_Average_TargetLastIndex);  
						g_Target_Dyno_Speed = AVG(Average_Target_Dyno_SpeedList);
						
						Target_Engine_Torque = g_AI_Target_Engine_Remote*g_Hmi_Target_Engine_Torque_Range; !g_Hmi_Target_Engine_Torque_Range=4000;							
						DSHIFT(Average_Target_Engine_TorqueList, Target_Engine_Torque, index_Average_TargetLastIndex);  
						g_Target_Engine_Torque = AVG(Average_Target_Engine_TorqueList);
						
						! *** HMI Demand Update
						g_Hmi_bs10x_Demand_Speed = g_Target_Dyno_Speed;
						g_Hmi_bs10x_Demand_Torque = g_Target_Engine_Torque;
					END
				ELSE					
					g_Target_Dyno_Speed = 0;
					g_Target_Dyno_Torque = 0;
					g_Target_Engine_Speed = 0;
					g_Target_Engine_Torque = 0;
					g_Target_Engine_AlphaPosi = 0;	
					
				END ! IF(g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA) 
			ELSEIF(g_ControlMode = g_CtMo_enum_Manual)
				IF(g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA) 
					! *** ref
					!g_Target_Dyno_Speed = 0; ! *** Other mode Target Zero Setting
					!g_Target_Engine_Speed = 0; ! *** Other mode Target Zero Setting
					!g_Target_Engine_Torque = 0; ! *** Other mode Target Zero Setting
												
						! *** Demand CaculationKnob 										
						pid_Target_Calc_Torque = g_Op_Demand_Initial_Torque + (g_HMI_AI_Dyno_CalRefCnt - g_Op_Demand_DynoTorque_CalRefCnt) * g_Hmi_bs2202_TorqueCngPerKnobPulseSt;
						IF(pid_Target_Calc_Torque < g_Hmi_bs3005_Eng_MinTorqueSt) 
							pid_Target_Calc_Torque = g_Hmi_bs3005_Eng_MinTorqueSt;							
							g_Op_Demand_DynoTorque_CalRefCnt = g_HMI_AI_Dyno_CalRefCnt-((g_Hmi_bs3005_Eng_MinTorqueSt - g_Op_Demand_Initial_Torque) / g_Hmi_bs2202_TorqueCngPerKnobPulseSt);	
						ELSEIF(pid_Target_Calc_Torque > g_Hmi_bs3005_Eng_MaxTorqueSt) 
							pid_Target_Calc_Torque = g_Hmi_bs3005_Eng_MaxTorqueSt; 							
							g_Op_Demand_DynoTorque_CalRefCnt = g_HMI_AI_Dyno_CalRefCnt-((g_Hmi_bs3005_Eng_MaxTorqueSt - g_Op_Demand_Initial_Torque) / g_Hmi_bs2202_TorqueCngPerKnobPulseSt);	
						END	!IF(pid_Target_Calc_Torque < 0) 							
				    g_Target_Dyno_Torque = pid_Target_Calc_Torque;
					
						! *** Demand CaculationKnob	
						pid_AlphaCngPerKnobPulse_IntToReal = g_Hmi_bs2202_AlphaCngPerKnobPulseSt / 10;
						pid_Target_Calc_AlphaPosi = g_Op_Demand_Initial_AlphaPosi + (g_HMI_AI_Eng_CalRefCnt - g_Op_Demand_EngAlphaPosi_CalRefCnt) * (pid_AlphaCngPerKnobPulse_IntToReal);
						IF(pid_Target_Calc_AlphaPosi < 0) 
							pid_Target_Calc_AlphaPosi = 0; 
							g_Op_Demand_EngAlphaPosi_CalRefCnt = g_HMI_AI_Eng_CalRefCnt-((-g_Op_Demand_Initial_AlphaPosi)/(pid_AlphaCngPerKnobPulse_IntToReal));							
						ELSEIF(pid_Target_Calc_AlphaPosi > 100) 
							pid_Target_Calc_AlphaPosi = 100; 							
							g_Op_Demand_EngAlphaPosi_CalRefCnt = g_HMI_AI_Eng_CalRefCnt-((100 - g_Op_Demand_Initial_AlphaPosi)/(pid_AlphaCngPerKnobPulse_IntToReal));	
						END	!IF(pid_Target_Calc_AlphaPosi < 0) 							
					g_Target_Engine_AlphaPosi = pid_Target_Calc_AlphaPosi;
					
					
					! *** HMI Demand Update
					g_Hmi_bs10x_Demand_Torque = g_Target_Dyno_Torque;
					g_Hmi_bs10x_Demand_AlphaPosi = g_Target_Engine_AlphaPosi;
					
				ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTA)
					! *** ref
					!g_Target_Dyno_Torque = 0; ! *** Other mode Target Zero Setting
					!g_Target_Engine_Speed = 0; ! *** Other mode Target Zero Setting
					!g_Target_Engine_Torque = 0; ! *** Other mode Target Zero Setting
								
						! *** Demand CaculationKnob 										
						pid_Target_Calc_Speed = g_Op_Demand_Initial_Speed + (g_HMI_AI_Dyno_CalRefCnt - g_Op_Demand_DynoSpeed_CalRefCnt) * g_Hmi_bs2202_SpeedCngPerKnobPulseSt;
						IF(pid_Target_Calc_Speed  < 0) 						
							g_Op_Demand_DynoSpeed_CalRefCnt = g_HMI_AI_Dyno_CalRefCnt-(( - g_Op_Demand_Initial_Speed - pid_Target_Calc_Speed) / g_Hmi_bs2202_SpeedCngPerKnobPulseSt);	
							pid_Target_Calc_Speed  = 0; 
							g_Op_Demand_Initial_Speed = 0;
						ELSEIF(pid_Target_Calc_Speed  > g_Hmi_bs3004_Eng_MaxSpeedSt) 
							pid_Target_Calc_Speed = g_Hmi_bs3004_Eng_MaxSpeedSt; 							
							g_Op_Demand_DynoSpeed_CalRefCnt = g_HMI_AI_Dyno_CalRefCnt-((g_Hmi_bs3004_Eng_MaxSpeedSt - g_Op_Demand_Initial_Speed) / g_Hmi_bs2202_SpeedCngPerKnobPulseSt);	
						END	!IF(pid_Target_Calc_Speed < 0) 				
					  g_Target_Dyno_Speed = pid_Target_Calc_Speed;
					
						! *** Demand CaculationKnob	
						pid_AlphaCngPerKnobPulse_IntToReal = g_Hmi_bs2202_AlphaCngPerKnobPulseSt / 10;
						pid_Target_Calc_AlphaPosi = g_Op_Demand_Initial_AlphaPosi + (g_HMI_AI_Eng_CalRefCnt - g_Op_Demand_EngAlphaPosi_CalRefCnt) * pid_AlphaCngPerKnobPulse_IntToReal;
						IF(pid_Target_Calc_AlphaPosi < 0) 
							pid_Target_Calc_AlphaPosi = 0; 
							g_Op_Demand_EngAlphaPosi_CalRefCnt = g_HMI_AI_Eng_CalRefCnt-((-g_Op_Demand_Initial_AlphaPosi)/pid_AlphaCngPerKnobPulse_IntToReal);							
						ELSEIF(pid_Target_Calc_AlphaPosi > 100) 
							pid_Target_Calc_AlphaPosi = 100; 							
							g_Op_Demand_EngAlphaPosi_CalRefCnt = g_HMI_AI_Eng_CalRefCnt-((100 - g_Op_Demand_Initial_AlphaPosi)/pid_AlphaCngPerKnobPulse_IntToReal);	
						END	!IF(pid_Target_Calc_AlphaPosi < 0) 							
					g_Target_Engine_AlphaPosi = pid_Target_Calc_AlphaPosi;
					
					
					! *** HMI Demand Update
					g_Hmi_bs10x_Demand_Speed = g_Target_Dyno_Speed;
					g_Hmi_bs10x_Demand_AlphaPosi = g_Target_Engine_AlphaPosi;
										
				ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyTo_EngRPM)						
					! *** ref
					!g_Target_Dyno_Speed = 0; ! *** Other mode Target Zero Setting
					!g_Target_Engine_Torque = 0; ! *** Other mode Target Zero Setting
					!g_Target_Engine_AlphaPosi = 0; ! *** Other mode Target Zero Setting
					
						! *** Demand CaculationKnob 										
						pid_Target_Calc_Torque = g_Op_Demand_Initial_Torque + (g_HMI_AI_Dyno_CalRefCnt - g_Op_Demand_DynoTorque_CalRefCnt) * g_Hmi_bs2202_TorqueCngPerKnobPulseSt;
						IF(pid_Target_Calc_Torque < g_Hmi_bs3005_Eng_MinTorqueSt) 
							pid_Target_Calc_Torque = g_Hmi_bs3005_Eng_MinTorqueSt;							
							g_Op_Demand_DynoTorque_CalRefCnt = g_HMI_AI_Dyno_CalRefCnt-((g_Hmi_bs3005_Eng_MinTorqueSt - g_Op_Demand_Initial_Torque) / g_Hmi_bs2202_TorqueCngPerKnobPulseSt);	
						ELSEIF(pid_Target_Calc_Torque > g_Hmi_bs3005_Eng_MaxTorqueSt) 
							pid_Target_Calc_Torque = g_Hmi_bs3005_Eng_MaxTorqueSt; 							
							g_Op_Demand_DynoTorque_CalRefCnt = g_HMI_AI_Dyno_CalRefCnt-((g_Hmi_bs3005_Eng_MaxTorqueSt - g_Op_Demand_Initial_Torque) / g_Hmi_bs2202_TorqueCngPerKnobPulseSt);	
						END	!IF(pid_Target_Calc_Torque < 0) 							
					g_Target_Dyno_Torque = pid_Target_Calc_Torque;
					
					
						! *** Demand CaculationKnob 										
						pid_Target_Calc_Speed = g_Op_Demand_Initial_Speed + (g_HMI_AI_Eng_CalRefCnt - g_Op_Demand_EngSpeed_CalRefCnt) * g_Hmi_bs2202_SpeedCngPerKnobPulseSt;
						IF(pid_Target_Calc_Speed  < 0) 						
							g_Op_Demand_DynoSpeed_CalRefCnt = g_HMI_AI_Eng_CalRefCnt-(( - g_Op_Demand_Initial_Speed - pid_Target_Calc_Speed) / g_Hmi_bs2202_SpeedCngPerKnobPulseSt);	
							pid_Target_Calc_Speed  = 0; 							
						ELSEIF(pid_Target_Calc_Speed  > g_Hmi_bs3004_Eng_MaxSpeedSt) 
							pid_Target_Calc_Speed = g_Hmi_bs3004_Eng_MaxSpeedSt; 							
							g_Op_Demand_DynoSpeed_CalRefCnt = g_HMI_AI_Eng_CalRefCnt-((g_Hmi_bs3004_Eng_MaxSpeedSt - g_Op_Demand_Initial_Speed) / g_Hmi_bs2202_SpeedCngPerKnobPulseSt);	
						END	!IF(pid_Target_Calc_Speed < 0) 	
					g_Target_Engine_Speed = pid_Target_Calc_Speed;	
					
					
							
					! *** HMI Demand Update
					g_Hmi_bs10x_Demand_Torque = g_Target_Dyno_Torque;
					g_Hmi_bs10x_Demand_Speed = g_Target_Engine_Speed;					
					
				ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTo)						
					! *** ref
					!g_Target_Dyno_Torque = 0; ! *** Other mode Target Zero Setting
					!g_Target_Engine_Speed = 0; ! *** Other mode Target Zero Setting
					!g_Target_Engine_AlphaPosi = 0; ! *** Other mode Target Zero Setting		

						! *** Demand CaculationKnob 										
						pid_Target_Calc_Speed = g_Op_Demand_Initial_Speed + (g_HMI_AI_Dyno_CalRefCnt - g_Op_Demand_DynoSpeed_CalRefCnt) * g_Hmi_bs2202_SpeedCngPerKnobPulseSt;
						IF(pid_Target_Calc_Speed  < 0) 							 						
							g_Op_Demand_DynoSpeed_CalRefCnt =g_HMI_AI_Dyno_CalRefCnt-(( - g_Op_Demand_Initial_Speed - pid_Target_Calc_Speed) / g_Hmi_bs2202_SpeedCngPerKnobPulseSt);	
							pid_Target_Calc_Speed  = 0; 		
						ELSEIF(pid_Target_Calc_Speed  > g_Hmi_bs3004_Eng_MaxSpeedSt) 
							pid_Target_Calc_Speed = g_Hmi_bs3004_Eng_MaxSpeedSt; 							
							g_Op_Demand_DynoSpeed_CalRefCnt = g_HMI_AI_Dyno_CalRefCnt-((g_Hmi_bs3004_Eng_MaxSpeedSt - g_Op_Demand_Initial_Speed) / g_Hmi_bs2202_SpeedCngPerKnobPulseSt);	
						END	!IF(pid_Target_Calc_Speed < 0) 						
					g_Target_Dyno_Speed = pid_Target_Calc_Speed;
					
					
						! *** Demand CaculationKnob 										
						pid_Target_Calc_Torque = g_Op_Demand_Initial_Torque + (g_HMI_AI_Eng_CalRefCnt - g_Op_Demand_EngTorque_CalRefCnt) * g_Hmi_bs2202_TorqueCngPerKnobPulseSt;
						IF(pid_Target_Calc_Torque < g_Hmi_bs3005_Eng_MinTorqueSt) 
							pid_Target_Calc_Torque = g_Hmi_bs3005_Eng_MinTorqueSt;							
							g_Op_Demand_DynoTorque_CalRefCnt = g_HMI_AI_Eng_CalRefCnt-((g_Hmi_bs3005_Eng_MinTorqueSt - g_Op_Demand_Initial_Torque) / g_Hmi_bs2202_TorqueCngPerKnobPulseSt);	
						ELSEIF(pid_Target_Calc_Torque > g_Hmi_bs3005_Eng_MaxTorqueSt) 
							pid_Target_Calc_Torque = g_Hmi_bs3005_Eng_MaxTorqueSt; 							
							g_Op_Demand_DynoTorque_CalRefCnt = g_HMI_AI_Eng_CalRefCnt-((g_Hmi_bs3005_Eng_MaxTorqueSt - g_Op_Demand_Initial_Torque) / g_Hmi_bs2202_TorqueCngPerKnobPulseSt);	
						END	!IF(pid_Target_Calc_Torque < 0) 							
					g_Target_Engine_Torque = pid_Target_Calc_Torque;		
					
					
					! *** HMI Demand Update
					g_Hmi_bs10x_Demand_Speed = g_Target_Dyno_Speed;
					g_Hmi_bs10x_Demand_Torque = g_Target_Engine_Torque;
					!DISP "4 : Dyno_Speed : %d ,Dyno_Torque : %d ,Engine_Speed : %d , Engine_Torque : %d, Engine_AlphaPosi : %d ", g_Target_Dyno_Speed, g_Target_Dyno_Torque, g_Target_Engine_Speed, g_Target_Engine_Torque, g_Target_Engine_AlphaPosi;
					
				ELSE					
					g_Target_Dyno_Speed = 0;
					g_Target_Dyno_Torque = 0;
					g_Target_Engine_Speed = 0;
					g_Target_Engine_Torque = 0;
					g_Target_Engine_AlphaPosi = 0;	
					
				END ! IF(g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA) 
				!DISP "4 : Dyno_Speed : %d ,Dyno_Torque : %d ,Engine_Speed : %d , Engine_Torque : %d, Engine_AlphaPosi : %d ", g_Target_Dyno_Speed, g_Target_Dyno_Torque, g_Target_Engine_Speed, g_Target_Engine_Torque, g_Target_Engine_AlphaPosi;
			ELSEIF(g_ControlMode = g_CtMo_enum_OpenLoopTest)
			
				! *** Demand CaculationKnob	
					pid_CurrentCngPerKnobPulse_IntToReal = g_Hmi_bs2202_CurrentCngPerKnobPulseSt / 10;
					ServiceMd_Target_Calc_Current = g_Op_Demand_Initial_Current + (g_HMI_AI_Dyno_CalRefCnt - g_Op_Demand_DynoCurrent_CalRefCnt) * pid_CurrentCngPerKnobPulse_IntToReal;
					IF(ServiceMd_Target_Calc_Current  < -100) 
						ServiceMd_Target_Calc_Current  = -100; 
						g_Op_Demand_DynoCurrent_CalRefCnt = g_HMI_AI_Dyno_CalRefCnt-((-100 -g_Op_Demand_Initial_Current)/pid_CurrentCngPerKnobPulse_IntToReal);							
					ELSEIF(ServiceMd_Target_Calc_Current  > 100) 
						ServiceMd_Target_Calc_Current  = 100; 							
						g_Op_Demand_DynoCurrent_CalRefCnt = g_HMI_AI_Dyno_CalRefCnt-((100 - g_Op_Demand_Initial_Current)/pid_CurrentCngPerKnobPulse_IntToReal);	
					END	!IF(pid_Target_Calc_AlphaPosi < 0)
					
				! *** Taget Update ***		
				g_Target_Dyno_Current = ServiceMd_Target_Calc_Current;
				g_Hmi_bs110_Demand_DynoCurrent = ServiceMd_Target_Calc_Current;
								
				ServiceMd_Target_Calc_CurrentLimitChk = 16383 + (g_Target_Dyno_Current * g_unit_Cmd_100PctToDynoCurrent_DARation);
				
				IF(ServiceMd_Target_Calc_CurrentLimitChk > 32767) ServiceMd_Target_Calc_CurrentLimitChk = 32767; END
				IF(ServiceMd_Target_Calc_CurrentLimitChk < 0) ServiceMd_Target_Calc_CurrentLimitChk = 0; END
				g_AO_DynoControlSignal = ServiceMd_Target_Calc_CurrentLimitChk;
				
				
					! *** Demand CaculationKnob		
					pid_AlphaCngPerKnobPulse_IntToReal = g_Hmi_bs2202_AlphaCngPerKnobPulseSt / 10;
					ServiceMd_Target_Calc_AlphaPosi = g_Op_Demand_Initial_AlphaPosi + (g_HMI_AI_Eng_CalRefCnt - g_Op_Demand_EngAlphaPosi_CalRefCnt) * pid_AlphaCngPerKnobPulse_IntToReal;
					IF(ServiceMd_Target_Calc_AlphaPosi < 0) 
						ServiceMd_Target_Calc_AlphaPosi = 0; 
						g_Op_Demand_EngAlphaPosi_CalRefCnt = g_HMI_AI_Eng_CalRefCnt-((-g_Op_Demand_Initial_AlphaPosi)/pid_AlphaCngPerKnobPulse_IntToReal);							
					ELSEIF(ServiceMd_Target_Calc_AlphaPosi > 100) 
						ServiceMd_Target_Calc_AlphaPosi = 100; 							
						g_Op_Demand_EngAlphaPosi_CalRefCnt = g_HMI_AI_Eng_CalRefCnt-((100 - g_Op_Demand_Initial_AlphaPosi)/pid_AlphaCngPerKnobPulse_IntToReal);	
					END	!IF(pid_Target_Calc_AlphaPosi < 0) 		
				
				! *** Taget Position Update ***		
				g_Target_Engine_AlphaPosi = ServiceMd_Target_Calc_AlphaPosi;
				g_Hmi_bs10x_Demand_AlphaPosi = ServiceMd_Target_Calc_AlphaPosi;
								
				! *** Engine Command D/A Conversion Ration ***			
				ServiceMd_Target_Calc_Alpha_1_DaVal = g_unit_Cmd_Alpha_1_0Pct_ZeroOffset_DaVal + g_unit_Cmd_Alpha_1_0Pct_DaVal + (ServiceMd_Target_Calc_AlphaPosi * g_unit_Cmd_100PctToAlpha_1_DARation);
				ServiceMd_Target_Calc_Alpha_2_DaVal = g_unit_Cmd_Alpha_2_0Pct_ZeroOffset_DaVal + g_unit_Cmd_Alpha_2_0Pct_DaVal + (ServiceMd_Target_Calc_AlphaPosi * g_unit_Cmd_100PctToAlpha_2_DARation);
				
				IF(g_unit_Cmd_Alpha_1_0Pct_DaVal < g_unit_Cmd_Alpha_1_100Pct_DaVal)
					IF(ServiceMd_Target_Calc_Alpha_1_DaVal < g_unit_Cmd_Alpha_1_0Pct_DaVal); ServiceMd_Target_Calc_Alpha_1_DaVal = g_unit_Cmd_Alpha_1_0Pct_DaVal; END
					IF(ServiceMd_Target_Calc_Alpha_1_DaVal > g_unit_Cmd_Alpha_1_100Pct_DaVal); ServiceMd_Target_Calc_Alpha_1_DaVal = g_unit_Cmd_Alpha_1_100Pct_DaVal; END
				ELSE
					IF(ServiceMd_Target_Calc_Alpha_1_DaVal > g_unit_Cmd_Alpha_1_0Pct_DaVal); ServiceMd_Target_Calc_Alpha_1_DaVal = g_unit_Cmd_Alpha_1_0Pct_DaVal; END
					IF(ServiceMd_Target_Calc_Alpha_1_DaVal < g_unit_Cmd_Alpha_1_100Pct_DaVal); ServiceMd_Target_Calc_Alpha_1_DaVal = g_unit_Cmd_Alpha_1_100Pct_DaVal; END
				END !IF(g_unit_Cmd_Alpha_1_0Pct_DaVal < g_unit_Cmd_Alpha_1_100Pct_DaVal)
				
				IF(g_unit_Cmd_Alpha_2_0Pct_DaVal < g_unit_Cmd_Alpha_2_100Pct_DaVal)
					IF(ServiceMd_Target_Calc_Alpha_2_DaVal < g_unit_Cmd_Alpha_2_0Pct_DaVal); ServiceMd_Target_Calc_Alpha_2_DaVal = g_unit_Cmd_Alpha_2_0Pct_DaVal; END
					IF(ServiceMd_Target_Calc_Alpha_2_DaVal > g_unit_Cmd_Alpha_2_100Pct_DaVal); ServiceMd_Target_Calc_Alpha_2_DaVal = g_unit_Cmd_Alpha_2_100Pct_DaVal; END
				ELSE
					IF(ServiceMd_Target_Calc_Alpha_2_DaVal > g_unit_Cmd_Alpha_2_0Pct_DaVal); ServiceMd_Target_Calc_Alpha_2_DaVal = g_unit_Cmd_Alpha_2_0Pct_DaVal; END
					IF(ServiceMd_Target_Calc_Alpha_2_DaVal < g_unit_Cmd_Alpha_2_100Pct_DaVal); ServiceMd_Target_Calc_Alpha_2_DaVal = g_unit_Cmd_Alpha_2_100Pct_DaVal; END
				END !IF(g_unit_Cmd_Alpha_2_0Pct_DaVal < g_unit_Cmd_Alpha_2_100Pct_DaVal)
				
				! *** c Output				
				g_AO_AlphaASignal = ServiceMd_Target_Calc_Alpha_1_DaVal;	
				g_AO_AlphaBSignal = ServiceMd_Target_Calc_Alpha_2_DaVal;

				IF((g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt >= 0) & (g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt >= 0))
					IF(ServiceMd_Target_Calc_Alpha_1_DaVal > g_unit_Cmd_Alpha_DO_IdleVali_DaVal)				
						g_DO_ThrottleIVSSignal = 1;
					ELSE
						g_DO_ThrottleIVSSignal = 0;
					END ! IF(ServiceMd_Target_Calc_Alpha_1_DaVal > g_unit_Cmd_Alpha_DO_IdleVali_DaVal)	
				END !IF((g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt >= 0) & (g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt >= 0))							

				
						
			ELSEIF(g_ControlMode = g_CtMo_enum_Automatic)	
				
				IF(g_AutoMd_ProfilePlay_Enable = 1)
				
					IF(g_ControlMode <> g_CtMo_enum_Automatic)
						! *** the control mode is not manual! Banner display.
						g_Hmi_Banner_MsgLineNum = 17; g_Hmi_Banner_Enable = 1; WAIT(g_Hmi_BannerSetAndResetInterval);g_Hmi_Banner_MsgLineNum = 0; g_Hmi_Banner_Enable = 0;
						
						! *** MMI Show Message							
						DISP "[#%d buffer] Automatic Profile Play - Not Automatic Mode.", BufferProgrmaNumber;
						
						g_AutoMd_ProfilePlay_Enable = 0;
						
					ELSE
						
						IF(AutoMd_TestCycle = 1) ! Transient Cycle
							! *** Read time
							time_AutoProfilePlay_CurrentTime = TIME;
							
							! *** Profile Index Calcuration
							time_AutoProfileTotal_PlayTime_RealType =  time_AutoProfileTotal_PlayTime_RealType + (time_AutoProfilePlay_CurrentTime - time_AutoProfilePlay_PreviousTime)/1000;						
							time_AutoProfileTotal_PlayTime_IntType = time_AutoProfileTotal_PlayTime_RealType;
							! *** Profile Index Round Off Process
							time_AutoProfileTotal_PlayTime_RoundOff = time_AutoProfileTotal_PlayTime_IntType - time_AutoProfileTotal_PlayTime_RealType;						
							IF((time_AutoProfileTotal_PlayTime_RoundOff <= 0.5) & (time_AutoProfileTotal_PlayTime_RoundOff > 0))
								time_AutoProfileTotal_PlayTime_IntType = time_AutoProfileTotal_PlayTime_IntType -1;
							END ! IF((time_AutoProfileTotal_PlayTime_RoundOff <= 0.5) & (time_AutoProfileTotal_PlayTime_RoundOff > 0))
							! *** Index Variable Set at First Running 
							IF((time_AutoProfilePlay_PreviousTime = 0) | (time_AutoProfileTotal_PlayTime_IntType < 0)) 
								time_AutoProfileTotal_PlayTime_RealType = 0;
								time_AutoProfileTotal_PlayTime_IntType = 0; 
							END ! IF((time_AutoProfilePlay_PreviousTime = 0) | (time_AutoProfileTotal_PlayTime_IntType < 0)) 
							
							! *** Previous Time Update 					
							time_AutoProfilePlay_PreviousTime = time_AutoProfilePlay_CurrentTime;
						
							! *** Linear interpolation Gain Calculation 			
							AutoMd_InterPolation_Gain = time_AutoProfileTotal_PlayTime_RealType - time_AutoProfileTotal_PlayTime_IntType;
							IF(AutoMd_InterPolation_Gain < 0.001)AutoMd_InterPolation_Gain = 0;END
							
							! *** Profile Index Calculation 	
							currentIndex_AutoMd_DynoProfile = g_pf_IndexStart_DynoProfile + time_AutoProfileTotal_PlayTime_IntType;
							currentIndex_AutoMd_EngProfile = g_pf_IndexStart_EngineProfile + time_AutoProfileTotal_PlayTime_IntType;
							nextIndex_AutoMd_DynoProfile = currentIndex_AutoMd_DynoProfile  + 1;
							nextIndex_AutoMd_EngProfile = currentIndex_AutoMd_EngProfile  + 1;
							
							IF(g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA)							
								
								! *** Index/Profile Count Check	
								IF(time_AutoProfileTotal_PlayTime_IntType >= g_PC_Process_Data_TqAlphaProfile(g_pf_IndexStart_Infomation + 0))
									
									g_AutoMd_ProfilePlay_Enable = 0;
									
									! *** MMI Show Message							
									DISP "[#%d buffer] Automatic Profile Play Stop. (DyTo_EngTA).", BufferProgrmaNumber;
								ELSE
									
									! *** Get Current Target
									target_CurrentIndex_Dyno = g_PC_Process_Data_TqAlphaProfile(currentIndex_AutoMd_DynoProfile );
									target_CurrentIndex_Engine = g_PC_Process_Data_TqAlphaProfile(currentIndex_AutoMd_EngProfile);
									
									! *** Get Next Target
									IF(nextIndex_AutoMd_DynoProfile >= g_pf_IndexSize_DynoEngProfile)
										target_NextIndex_Dyno = 0;
										target_NextIndex_Engine = 0;
										
										g_AutoMd_ProfilePlay_Enable = 0;
										
										! *** MMI Show Message							
										DISP "[#%d buffer] Automafgfgfglay Stop. (DyTo_EngTA).", BufferProgrmaNumber;
									ELSE
										target_NextIndex_Dyno = g_PC_Process_Data_TqAlphaProfile(nextIndex_AutoMd_DynoProfile);
										target_NextIndex_Engine = g_PC_Process_Data_TqAlphaProfile(nextIndex_AutoMd_EngProfile);
									END ! IF(time_AutoProfileTotal_PlayTime_IntType >= g_pf_IndexSize_DynoEngProfile)
									
									! *** Linear interpolation Calculation (Dyno / Engine)	
									AutoMd_Dyno_ProfileTarget = target_CurrentIndex_Dyno + ((target_NextIndex_Dyno - target_CurrentIndex_Dyno) * AutoMd_InterPolation_Gain);
									AutoMd_Engine_ProfileTarget = target_CurrentIndex_Engine + ((target_NextIndex_Engine - target_CurrentIndex_Engine) * AutoMd_InterPolation_Gain);
									
									! *** Target Limit Check (Dyno / Engine)
									IF(AutoMd_Dyno_ProfileTarget > g_Hmi_bs3005_Eng_MaxTorqueSt) AutoMd_Dyno_ProfileTarget = g_Hmi_bs3005_Eng_MaxTorqueSt; END
									IF(AutoMd_Dyno_ProfileTarget < g_Hmi_bs3005_Eng_MinTorqueSt) AutoMd_Dyno_ProfileTarget = g_Hmi_bs3005_Eng_MinTorqueSt; END
									IF(AutoMd_Engine_ProfileTarget > 100) AutoMd_Dyno_ProfileTarget = 100; END
									IF(AutoMd_Engine_ProfileTarget < 0) AutoMd_Engine_ProfileTarget = 0; END
									
									! *** Target Opdate (Dyno / Engine)	
									!g_Target_Dyno_Speed = 0;
									g_Target_Dyno_Torque = AutoMd_Dyno_ProfileTarget;
									!g_Target_Dyno_Current = 0;
									!g_Target_Engine_Speed = 0;
									!g_Target_Engine_Torque = 0;
									g_Target_Engine_AlphaPosi = AutoMd_Engine_ProfileTarget;
								
									! *** MMI Show Message
									IF (g_Buffer4_displayCmdLevel <> 0); DISP time_AutoProfileTotal_PlayTime_RealType, AutoMd_Dyno_ProfileTarget, g_Target_Engine_AlphaPosi; END						
									
								END !IF(g_PC_Write_Data_TqAlphaProfile(index_AutoMd_ProfileOffset_Info + 0) >= time_AutoProfileTotal_PlayTime_IntType)						
							
							ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTA)
								! *** Index/Profile Count Check	
								IF(time_AutoProfileTotal_PlayTime_IntType >= g_PC_Process_Data_SpAlphaProfile(g_pf_IndexStart_Infomation + 0))
									
									g_AutoMd_ProfilePlay_Enable = 0;
									
									!g_Target_Dyno_Speed = 0;
									!g_Target_Dyno_Torque = 0;
									!g_Target_Dyno_Current = 0;
									!g_Target_Engine_Speed = 0;
									!g_Target_Engine_Torque = 0;
									!g_Target_Engine_AlphaPosi = 0;
									
									! *** MMI Show Message							
									DISP "[#%d buffer] Automatic Profile Play Stop. (DySp_EngAlpha).", BufferProgrmaNumber;
								ELSE
									
									! *** Get Current Target
									target_CurrentIndex_Dyno = g_PC_Process_Data_SpAlphaProfile(currentIndex_AutoMd_DynoProfile );
									target_CurrentIndex_Engine = g_PC_Process_Data_SpAlphaProfile(currentIndex_AutoMd_EngProfile);
									
									! *** Get Next Target
									IF(nextIndex_AutoMd_DynoProfile >= g_pf_IndexSize_DynoEngProfile)
										target_NextIndex_Dyno = 0;
										target_NextIndex_Engine = 0;
										
										g_AutoMd_ProfilePlay_Enable = 0;
									ELSE
										target_NextIndex_Dyno = g_PC_Process_Data_SpAlphaProfile(nextIndex_AutoMd_DynoProfile);
										target_NextIndex_Engine = g_PC_Process_Data_SpAlphaProfile(nextIndex_AutoMd_EngProfile);
									END ! IF(time_AutoProfileTotal_PlayTime_IntType >= g_pf_IndexSize_DynoEngProfile)
									
									! *** Linear interpolation Calculation (Dyno / Engine)	
									AutoMd_Dyno_ProfileTarget = target_CurrentIndex_Dyno + ((target_NextIndex_Dyno - target_CurrentIndex_Dyno) * AutoMd_InterPolation_Gain);
									AutoMd_Engine_ProfileTarget = target_CurrentIndex_Engine + ((target_NextIndex_Engine - target_CurrentIndex_Engine) * AutoMd_InterPolation_Gain);
									
									! *** Target Limit Check (Dyno / Engine)
									IF(AutoMd_Dyno_ProfileTarget > g_Hmi_bs3004_Eng_MaxSpeedSt) AutoMd_Dyno_ProfileTarget = g_Hmi_bs3004_Eng_MaxSpeedSt; END
									IF(AutoMd_Dyno_ProfileTarget < g_Hmi_bs3004_Eng_IdleMinSpeedSt) AutoMd_Dyno_ProfileTarget = g_Hmi_bs3004_Eng_IdleMinSpeedSt; END
									IF(AutoMd_Engine_ProfileTarget > 100) AutoMd_Dyno_ProfileTarget = 100; END
									IF(AutoMd_Engine_ProfileTarget < 0) AutoMd_Engine_ProfileTarget = 0; END
									
									! *** Target Opdate (Dyno / Engine)	
									g_Target_Dyno_Speed = AutoMd_Dyno_ProfileTarget;
									!g_Target_Dyno_Torque = 0;
									!g_Target_Dyno_Current = 0;
									!g_Target_Engine_Speed = 0;
									!g_Target_Engine_Torque = 0;
									g_Target_Engine_AlphaPosi = AutoMd_Engine_ProfileTarget;
								END	! IF(time_AutoProfileTotal_PlayTime_IntType >= g_PC_Process_Data_SpAlphaProfile(g_pf_IndexStart_Infomation + 0))
								
								! *** MMI Show Message
								!IF (g_Buffer4_displayCmdLevel <> 0); DISP time_AutoProfileTotal_PlayTime_RealType, AutoMd_Dyno_ProfileTarget, AutoMd_Engine_ProfileTarget; END						
							
							ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyTo_EngRPM)
								! *** Index/Profile Count Check	
								IF(time_AutoProfileTotal_PlayTime_IntType >= g_PC_Process_Data_TqSpProfile(g_pf_IndexStart_Infomation + 0))
									
									g_AutoMd_ProfilePlay_Enable = 0;
									
									!g_Target_Dyno_Speed = 0;
									!g_Target_Dyno_Torque = 0;
									!g_Target_Dyno_Current = 0;
									!g_Target_Engine_Speed = 0;
									!g_Target_Engine_Torque = 0;
									!g_Target_Engine_AlphaPosi = 0;
									
									! *** MMI Show Message							
									DISP "[#%d buffer] Automatic Profile Play Stop. (DyTq_EngRpm).", BufferProgrmaNumber;
								ELSE
									
									! *** Get Current Target
									target_CurrentIndex_Dyno = g_PC_Process_Data_TqSpProfile(currentIndex_AutoMd_DynoProfile );
									target_CurrentIndex_Engine = g_PC_Process_Data_TqSpProfile(currentIndex_AutoMd_EngProfile);
									
									! *** Get Next Target
									IF(nextIndex_AutoMd_DynoProfile >= g_pf_IndexSize_DynoEngProfile)
										target_NextIndex_Dyno = 0;
										target_NextIndex_Engine = 0;
										
										g_AutoMd_ProfilePlay_Enable = 0;
									ELSE
										target_NextIndex_Dyno = g_PC_Process_Data_TqSpProfile(nextIndex_AutoMd_DynoProfile);
										target_NextIndex_Engine = g_PC_Process_Data_TqSpProfile(nextIndex_AutoMd_EngProfile);
									END ! IF(time_AutoProfileTotal_PlayTime_IntType >= g_pf_IndexSize_DynoEngProfile)
									
									! *** Linear interpolation Calculation (Dyno / Engine)	
									AutoMd_Dyno_ProfileTarget = target_CurrentIndex_Dyno + ((target_NextIndex_Dyno - target_CurrentIndex_Dyno) * AutoMd_InterPolation_Gain);
									AutoMd_Engine_ProfileTarget = target_CurrentIndex_Engine + ((target_NextIndex_Engine - target_CurrentIndex_Engine) * AutoMd_InterPolation_Gain);
									
									! *** Target Limit Check (Dyno / Engine)
									IF(AutoMd_Dyno_ProfileTarget > g_Hmi_bs3005_Eng_MaxTorqueSt) AutoMd_Dyno_ProfileTarget = g_Hmi_bs3005_Eng_MaxTorqueSt; END
									IF(AutoMd_Dyno_ProfileTarget < g_Hmi_bs3005_Eng_MinTorqueSt) AutoMd_Dyno_ProfileTarget = g_Hmi_bs3005_Eng_MinTorqueSt; END
									IF(AutoMd_Engine_ProfileTarget > g_Hmi_bs3004_Eng_MaxSpeedSt) AutoMd_Engine_ProfileTarget = g_Hmi_bs3004_Eng_MaxSpeedSt; END
									IF(AutoMd_Engine_ProfileTarget < g_Hmi_bs3004_Eng_IdleMinSpeedSt) AutoMd_Engine_ProfileTarget = g_Hmi_bs3004_Eng_IdleMinSpeedSt; END
									
									! *** Target Opdate (Dyno / Engine)	
									!g_Target_Dyno_Speed = 0;
									g_Target_Dyno_Torque = AutoMd_Dyno_ProfileTarget;
									!g_Target_Dyno_Current = 0;
									g_Target_Engine_Speed = AutoMd_Engine_ProfileTarget;
									!g_Target_Engine_Torque = 0;
									!g_Target_Engine_AlphaPosi = 0;								
										
								END !IF(g_PC_Write_Data_TqAlphaProfile(index_AutoMd_ProfileOffset_Info + 0) >= time_AutoProfileTotal_PlayTime_IntType)						
								
								! *** MMI Show Message
								IF (g_Buffer4_displayCmdLevel <> 0); DISP time_AutoProfileTotal_PlayTime_RealType, AutoMd_Dyno_ProfileTarget, AutoMd_Engine_ProfileTarget; END						
									
							ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTo)
								! *** Index/Profile Count Check	
								IF(time_AutoProfileTotal_PlayTime_IntType >= g_PC_Process_Data_SpTqProfile(g_pf_IndexStart_Infomation + 0))
									
									g_AutoMd_ProfilePlay_Enable = 0;
									
									!g_Target_Dyno_Speed = 0;
									!g_Target_Dyno_Torque = 0;
									!g_Target_Dyno_Current = 0;
									!g_Target_Engine_Speed = 0;
									!g_Target_Engine_Torque = 0;
									!g_Target_Engine_AlphaPosi = 0;
									
									! *** MMI Show Message							
									DISP "[#%d buffer] Automatic Profile Play Stop. (DySp_EngTq).", BufferProgrmaNumber;
								ELSE
									
									! *** Get Current Target
									target_CurrentIndex_Dyno = g_PC_Process_Data_SpTqProfile(currentIndex_AutoMd_DynoProfile );
									target_CurrentIndex_Engine = g_PC_Process_Data_SpTqProfile(currentIndex_AutoMd_EngProfile);
									
									! *** Get Next Target
									IF(nextIndex_AutoMd_DynoProfile >= g_pf_IndexSize_DynoEngProfile)
										target_NextIndex_Dyno = 0;
										target_NextIndex_Engine = 0;
										
										g_AutoMd_ProfilePlay_Enable = 0;
									ELSE
										target_NextIndex_Dyno = g_PC_Process_Data_SpTqProfile(nextIndex_AutoMd_DynoProfile);
										target_NextIndex_Engine = g_PC_Process_Data_SpTqProfile(nextIndex_AutoMd_EngProfile);
									END ! IF(time_AutoProfileTotal_PlayTime_IntType >= g_pf_IndexSize_DynoEngProfile)
									
									! *** Linear interpolation Calculation (Dyno / Engine)	
									AutoMd_Dyno_ProfileTarget = target_CurrentIndex_Dyno + ((target_NextIndex_Dyno - target_CurrentIndex_Dyno) * AutoMd_InterPolation_Gain);
									AutoMd_Engine_ProfileTarget = target_CurrentIndex_Engine + ((target_NextIndex_Engine - target_CurrentIndex_Engine) * AutoMd_InterPolation_Gain);
									
									! *** Target Limit Check (Dyno / Engine)								
									IF(AutoMd_Dyno_ProfileTarget > g_Hmi_bs3004_Eng_MaxSpeedSt) AutoMd_Dyno_ProfileTarget = g_Hmi_bs3004_Eng_MaxSpeedSt; END
									IF(AutoMd_Dyno_ProfileTarget < g_Hmi_bs3004_Eng_IdleMinSpeedSt) AutoMd_Dyno_ProfileTarget = g_Hmi_bs3004_Eng_IdleMinSpeedSt; END								
									IF(AutoMd_Engine_ProfileTarget > g_Hmi_bs3005_Eng_MaxTorqueSt) AutoMd_Engine_ProfileTarget = g_Hmi_bs3005_Eng_MaxTorqueSt; END
									IF(AutoMd_Engine_ProfileTarget < g_Hmi_bs3005_Eng_MinTorqueSt) AutoMd_Engine_ProfileTarget = g_Hmi_bs3005_Eng_MinTorqueSt; END
									
									! *** Target Opdate (Dyno / Engine)	
									g_Target_Dyno_Speed = AutoMd_Dyno_ProfileTarget;
									!g_Target_Dyno_Torque = 0;
									!g_Target_Dyno_Current = 0;
									!g_Target_Engine_Speed = 0;
									g_Target_Engine_Torque = AutoMd_Engine_ProfileTarget;
									!g_Target_Engine_AlphaPosi = 0;								
										
								END !IF(g_PC_Write_Data_TqAlphaProfile(index_AutoMd_ProfileOffset_Info + 0) >= time_AutoProfileTotal_PlayTime_IntType)						
								
								! *** MMI Show Message
								IF (g_Buffer4_displayCmdLevel <> 0); DISP time_AutoProfileTotal_PlayTime_RealType, AutoMd_Dyno_ProfileTarget, AutoMd_Engine_ProfileTarget; END						
									
							END ! IF(g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA)
						ELSEIF(AutoMd_TestCycle = 0)
							!IF ((TIME - AutoMd_StationaryTargetTimeMeasure) - 1000)
							
								IF(g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA)	
									
									! *** Get Current Target	
									AutoMd_Dyno_ProfileTarget = g_PC_Write_Data(0x100);
									AutoMd_Engine_ProfileTarget = g_PC_Write_Data(0x112);
									
									! *** Target Limit Check (Dyno / Engine)	
									IF(AutoMd_Dyno_ProfileTarget > g_Hmi_bs3005_Eng_MaxTorqueSt) AutoMd_Dyno_ProfileTarget = g_Hmi_bs3005_Eng_MaxTorqueSt; END
									IF(AutoMd_Dyno_ProfileTarget < g_Hmi_bs3005_Eng_MinTorqueSt) AutoMd_Dyno_ProfileTarget = g_Hmi_bs3005_Eng_MinTorqueSt; END
									IF(AutoMd_Engine_ProfileTarget > 100) AutoMd_Dyno_ProfileTarget = 100; END
									IF(AutoMd_Engine_ProfileTarget < 0) AutoMd_Dyno_ProfileTarget = 0; END									
									
									! *** Target Opdate (Dyno / Engine)	
									!g_Target_Dyno_Speed = 0;
									g_Target_Dyno_Torque = AutoMd_Dyno_ProfileTarget;
									!g_Target_Dyno_Current = 0;
									!g_Target_Engine_Speed = 0;
									!g_Target_Engine_Torque = 0;
									g_Target_Engine_AlphaPosi = AutoMd_Engine_ProfileTarget;
								
									! *** MMI Show Message
									IF (g_Buffer4_displayCmdLevel <> 0); DISP time_AutoProfileTotal_PlayTime_RealType, AutoMd_Dyno_ProfileTarget, g_Target_Engine_AlphaPosi; END						
																	
								ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTA)
									
									! *** Get Current Target	
									AutoMd_Dyno_ProfileTarget = g_PC_Write_Data(0x101);
									AutoMd_Engine_ProfileTarget = g_PC_Write_Data(0x112);
									
									! *** Target Limit Check (Dyno / Engine)
									IF(AutoMd_Dyno_ProfileTarget > g_Hmi_bs3004_Eng_MaxSpeedSt) AutoMd_Dyno_ProfileTarget = g_Hmi_bs3004_Eng_MaxSpeedSt; END
									IF(AutoMd_Dyno_ProfileTarget < g_Hmi_bs3004_Eng_IdleMinSpeedSt) AutoMd_Dyno_ProfileTarget = g_Hmi_bs3004_Eng_IdleMinSpeedSt; END
									IF(AutoMd_Engine_ProfileTarget > 100) AutoMd_Dyno_ProfileTarget = 100; END
									IF(AutoMd_Engine_ProfileTarget < 0) AutoMd_Dyno_ProfileTarget = 0; END
									
									! *** Target Opdate (Dyno / Engine)	
									g_Target_Dyno_Speed = AutoMd_Dyno_ProfileTarget;
									!g_Target_Dyno_Torque = 0;
									!g_Target_Dyno_Current = 0;
									!g_Target_Engine_Speed = 0;
									!g_Target_Engine_Torque = 0;
									g_Target_Engine_AlphaPosi = AutoMd_Engine_ProfileTarget;
									
									! *** MMI Show Message
									IF (g_Buffer4_displayCmdLevel <> 0); DISP time_AutoProfileTotal_PlayTime_RealType, AutoMd_Dyno_ProfileTarget, AutoMd_Engine_ProfileTarget; END						
								
								ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyTo_EngRPM)
									
									! *** Get Current Target	
									AutoMd_Dyno_ProfileTarget = g_PC_Write_Data(0x100);
									AutoMd_Engine_ProfileTarget = g_PC_Write_Data(0x111);
									
									! *** Target Limit Check (Dyno / Engine)	
									IF(AutoMd_Dyno_ProfileTarget > g_Hmi_bs3005_Eng_MaxTorqueSt) AutoMd_Dyno_ProfileTarget = g_Hmi_bs3005_Eng_MaxTorqueSt; END
									IF(AutoMd_Dyno_ProfileTarget < g_Hmi_bs3005_Eng_MinTorqueSt) AutoMd_Dyno_ProfileTarget = g_Hmi_bs3005_Eng_MinTorqueSt; END
									IF(AutoMd_Engine_ProfileTarget > g_Hmi_bs3004_Eng_MaxSpeedSt) AutoMd_Engine_ProfileTarget = g_Hmi_bs3004_Eng_MaxSpeedSt; END
									IF(AutoMd_Engine_ProfileTarget < g_Hmi_bs3004_Eng_IdleMinSpeedSt) AutoMd_Engine_ProfileTarget = g_Hmi_bs3004_Eng_IdleMinSpeedSt; END
																		
									! *** Target Opdate (Dyno / Engine)	
									!g_Target_Dyno_Speed = 0;
									g_Target_Dyno_Torque = AutoMd_Dyno_ProfileTarget;
									!g_Target_Dyno_Current = 0;
									g_Target_Engine_Speed = AutoMd_Engine_ProfileTarget;
									!g_Target_Engine_Torque = 0;
									!g_Target_Engine_AlphaPosi = 0;	
									
									! *** MMI Show Message
									IF (g_Buffer4_displayCmdLevel <> 0); DISP time_AutoProfileTotal_PlayTime_RealType, AutoMd_Dyno_ProfileTarget, AutoMd_Engine_ProfileTarget; END						
										
								ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTo)
									
									! *** Get Current Target	
									AutoMd_Dyno_ProfileTarget = g_PC_Write_Data(0x101);
									AutoMd_Engine_ProfileTarget = g_PC_Write_Data(0x110);
									
									! *** Target Limit Check (Dyno / Engine)								
									IF(AutoMd_Dyno_ProfileTarget > g_Hmi_bs3004_Eng_MaxSpeedSt) AutoMd_Dyno_ProfileTarget = g_Hmi_bs3004_Eng_MaxSpeedSt; END
									IF(AutoMd_Dyno_ProfileTarget < g_Hmi_bs3004_Eng_IdleMinSpeedSt) AutoMd_Dyno_ProfileTarget = g_Hmi_bs3004_Eng_IdleMinSpeedSt; END								
									IF(AutoMd_Engine_ProfileTarget > g_Hmi_bs3005_Eng_MaxTorqueSt) AutoMd_Engine_ProfileTarget = g_Hmi_bs3005_Eng_MaxTorqueSt; END
									IF(AutoMd_Engine_ProfileTarget < g_Hmi_bs3005_Eng_MinTorqueSt) AutoMd_Engine_ProfileTarget = g_Hmi_bs3005_Eng_MinTorqueSt; END
									
									! *** Target Opdate (Dyno / Engine)	
									g_Target_Dyno_Speed = AutoMd_Dyno_ProfileTarget;
									!g_Target_Dyno_Torque = 0;
									!g_Target_Dyno_Current = 0;
									!g_Target_Engine_Speed = 0;
									g_Target_Engine_Torque = AutoMd_Engine_ProfileTarget;
									!g_Target_Engine_AlphaPosi = 0;								
									
									! *** MMI Show Message
									IF (g_Buffer4_displayCmdLevel <> 0); DISP time_AutoProfileTotal_PlayTime_RealType, AutoMd_Dyno_ProfileTarget, AutoMd_Engine_ProfileTarget; END						
								END ! IF(g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA)	
								
								if ((g_PC_Write_Data(0x120) <> g_SystemState_Eng_IG_On) & (g_Automatic_Eng_IG_On<>1))
									g_Automatic_Eng_IG_On = 1; ! See Buffer3 663Lines
									DISP "g_Automatic_Eng_IG_On";
								end
								g_DO_ThermalShockValve1 = g_PC_Write_Data(0x121);
								g_DO_ThermalShockValve2 = g_PC_Write_Data(0x121);
								g_DO_ThermalShockHeater = g_PC_Write_Data(0x122);
								
							!END ! IF ((TIME - AutoMd_StationaryTargetTimeMeasure)) - 1000)									
						END ! IF(AutoMd_TestCycle = 1) 	
					END ! IF(g_ControlMode <> g_CtMo_enum_Automatic)	
				ELSE
					time_AutoProfilePlay_CurrentTime = 0;
					time_AutoProfileTotal_PlayTime_RealType = 0;
					time_AutoProfileTotal_PlayTime_IntType = 0;
					time_AutoProfileTotal_PlayTime_RoundOff = 0;
					time_AutoProfilePlay_PreviousTime = 0;
					AutoMd_InterPolation_Gain = 0;
					currentIndex_AutoMd_DynoProfile = 0;
					currentIndex_AutoMd_EngProfile = 0;
					nextIndex_AutoMd_DynoProfile = 0;
					nextIndex_AutoMd_EngProfile = 0;
					target_CurrentIndex_Dyno = 0;
					target_CurrentIndex_Engine = 0;
					
					! *** Target Opdate (Dyno / Engine)	
					!g_Target_Dyno_Speed = 0;
					!g_Target_Dyno_Torque = 0;
					!g_Target_Dyno_Current = 0;
					!g_Target_Engine_Speed = 0;
					!g_Target_Engine_Torque = 0;
					!g_Target_Engine_AlphaPosi = 0;
					
				END ! IF(AutoMd_ProfilePlay_Enable = 1)
			
			END ! IF(g_ControlMode = g_CtMo_enum_Manual)
			
			! ----------------------------------------------------------------------------------------------
			! ***			
			! *** Feedback Voltage Calculation & Update to Global variable
			! ***				
				!Calc_Rpm_VoltToRpm_CalcResult = MAPN(g_eCAT_Input_Data(0)(g_Hmi_bs5001_FB_SpeedYIndexSt), g_lInter_fbSpeed_RefeVal_List, g_lInter_fbSpeed_AppliedVal_List);
				
				!DSHIFT(Average_SpeedList, Calc_Rpm_VoltToRpm_CalcResult, index_Average_SpeedListLastIndex);  
				
				!Calc_Rpm_VoltToRpm_CalcResult = AVG(Average_SpeedList);
				! *** Feedback RPM Update to Global variable				
				!g_Feedback_Speed = Calc_Rpm_VoltToRpm_CalcResult;
			
				! *** Feedback RPM Update to HMI		
				!g_Hmi_bsHeader1_Fb_Speed = Calc_Rpm_VoltToRpm_CalcResult;
			! ----------------------------------------------------------------------------------------------
			! ***			
			! *** Feedback Frequency Calculation & Update to Global variable
			! ***	
				!Calc_Rpm_Now_Frequency = g_AI_Feedback_DynoSpeed;
				!Encode Error signal Remove. Remove ubnormal low rpm signal
				IF (((Calc_Rpm_FrequencyToRpm_CalcResult - g_AI_Feedback_DynoSpeed)/Calc_Rpm_FrequencyToRpm_CalcResult<0.8) | Calc_Rpm_FrequencyToRpm_CalcResult<300 | g_stop_StopMode <> g_stop_enum_Default | g_SystemState_Misc_On=0 | RPM_ERROR_Count > 100)
					Calc_Rpm_FrequencyToRpm_CalcResult = g_AI_Feedback_DynoSpeed;
					RPM_ERROR_Count = 0;
				ELSE
					RPM_ERROR_Count = RPM_ERROR_Count + 1;
				END
				! g_eCAT_Input_Data(0)(0x90)
				!Calc_Rpm_VoltToRpm_CalcResult = (g_eCAT_Input_Data(0)(g_Hmi_bs5001_FB_SpeedYIndexSt)) / 60
			! *** Feedback RPM Update to Global variable	
			    SPEEDUPDATEDELTA = TIME-LASTSPEEDUPDATETIME;
				LASTSPEEDUPDATETIME = TIME;
				DSHIFT(Average_SpeedList, Calc_Rpm_FrequencyToRpm_CalcResult, index_Average_SpeedListLastIndex);  
				DSHIFT(Average_OutputSpeedList, Calc_Rpm_FrequencyToRpm_CalcResult, index_Average_OutputSpeedListLastIndex);  

				g_Feedback_Speed = AVG(Average_SpeedList);
				
				! DYNO SPEED ACTUAL_OUTPUT
				g_AO_DynoSpeedActualSignal = AVG(Average_OutputSpeedList) / g_Hmi_bs5001_FB_SpeedMaxSpeedSt * 32767;

				!g_Feedback_Speed = Calc_Rpm_FrequencyToRpm_CalcResult;
		
			! *** Feedback RPM Update to HMI		
				g_Hmi_bsHeader1_Fb_Speed = AVG(Average_OutputSpeedList);
				!g_Hmi_bsHeader1_Fb_Speed = AVG(Average_SpeedList);
							
			! ----------------------------------------------------------------------------------------------
			
			! *** Feedback Torque Calculation & Update to Global variable
			
				! *** Voltage >> Torque Calculation
				!Calc_Torque_VoltToTorque_CalcResult = ((g_eCAT_Input_Data(0)(0x71) + 2332)/559 * 100);				
				!Calc_Torque_VoltToTorque_CalcResult = (g_Hmi_bs5102_FB_TorqueMaxTorqueSt * (g_eCAT_Input_Data(0)(g_Hmi_bs5101_FB_TorqueYIndexSt)) / 32767) - g_Hmi_bs5102_FB_TorqueZeroOffsetST;
				!Calc_Torque_VoltToTorque_CalcResult = MAPN(g_eCAT_Input_Data(0)(g_Hmi_bs5101_FB_TorqueYIndexSt), g_lInter_fbTorque_RefeVal_List, g_lInter_fbTorque_AppliedVal_List);
				g_Hmi_bs510x_FB_TorqueADValMon 	= g_Monitoring_TorqueAvg * 10;
				
				real CalibratedTorque;
				CalibratedTorque = MAPN(g_AI_Feedback_TorqueAvg, g_lInter_fbTorque_RefeVal_List, g_lInter_fbTorque_AppliedVal_List);
				real CalibratedMonitoringTorque;
				CalibratedMonitoringTorque = MAPN(g_Monitoring_TorqueAvg, g_lInter_fbTorque_RefeVal_List, g_lInter_fbTorque_AppliedVal_List);

				! TORQUE ACTUAL_OUTPUT
				!g_AO_DynoTorqueActualSignal = CalibratedMonitoringTorque / g_Hmi_bs5101_FB_TorqueMaxTorqueSt /10 * 32767*9.818;
				g_AO_DynoTorqueActualSignal = CalibratedMonitoringTorque / g_Hmi_bs5101_FB_TorqueMaxTorqueSt * 32767;
			! *** Feedback Torque Update to Global variable
			g_Feedback_Torque = CalibratedTorque;
			
			! *** Feedback Torque Update to HMI			
			g_Hmi_bsHeader1_Fb_Torque = CalibratedMonitoringTorque;
			
			
			
			!g_Hmi_bsHeader1_Fb_Torque = g_Feedback_Torque!Calc_Torque_VoltToTorque_CalcResult;			
			! ----------------------------------------------------------------------------------------------
		
			! *** Throttle Actuator Position Calculation & Update to Global variable
			IF(g_Hmi_bs5201_FB_AlphaSourceSt = 1)
				! *** Voltage >> Throttle Actuator Position Calculation
				Calc_Throttle_VoltToPosition_CalcResult = g_AI_Feedback_Alpha - (g_Hmi_bs5201_FB_AlphaZeroOffsetSd / 10);
				
				! *** Feedback Throttle Actuator Position Update to Global variable
				g_Feedback_AlphaPosi = Calc_Throttle_VoltToPosition_CalcResult;
			ELSE
				! *** Feedback Throttle Actuator Position Update to Global variable
				g_Feedback_AlphaPosi = g_Target_Engine_AlphaPosi;
			END; !IF(g_Hmi_bs5201_FB_AlphaSourceSt = 1)
			
			! *** Feedback Throttle Actuator Position Update to HMI
			g_Hmi_bsHeader1_Fb_AlphaPosi = g_Feedback_AlphaPosi;			
			! ----------------------------------------------------------------------------------------------			
			
			! *** Power Calculation & Update to Global variable		
			! *** Power[w] = Torque[N'M] * Angular velocity[rad/sec]( (RPM/60) * 2Pi = (1/60) * 2Pi = 0.10471 ) 
			!Calc_Power_Kw_CalcResult = g_Feedback_Torque * (g_Feedback_Speed * 0.10471) / 1000;
			!  *** Power[w] = Speed[RPM] * Torque[N'M] /9549.2966				
			Calc_Power_Kw_CalcResult = g_Feedback_Speed * g_Feedback_Torque / 9549.2966
			
			! *** Power Update to HMI	
			g_Hmi_bsHeader1_Fb_Power = Calc_Power_Kw_CalcResult;
			
		
			! *** Feedback Current Calculation & Update to Global variable
			
				! *** Voltage >> Torque Calculation
				!Calc_Current_A_CalcResult = MAPN(g_eCAT_Input_Data(0)(g_Hmi_bs5301_FB_CurrentIndexSt), g_lInter_fbDynoCurrent_RefeVal_List, g_lInter_fbDynoCurrent_AppliedVal_List);
						
			! *** Feedback Torque Update to HMI			
			!g_Hmi_bsHeader2_Fb_DynoCurrent = Calc_Current_A_CalcResult;
			
			! ----------------------------------------------------------------------------------------------
			
			! ***
			! *** Dyno/Engine PID Program Control
			! ***g_DriveManual_PreviousMode 
			! *** 
			! *** Operator pannel Stop Command 			
			! *** 
			
			IF(((g_stop_StopMode = g_stop_enum_SoftStopping) | (g_stop_StopMode = g_stop_enum_HardStopping)) & (SysCmdCtrl_PreviousStopCmd <> g_stop_StopMode))					
			
				SysCmdCtrl_PreviousStopCmd = g_stop_StopMode;
				
				IF(g_Feedback_Speed >= g_Hmi_bs3004_Eng_IdleMinSpeedSt)
					IF(PST(5).#RUN <> 1)
						
						START 5, 1;
						
						! *** MMI Message							
						DISP "[#%d buffer] [req] O/P Stop CMD Dynamometer PID Progrma Start.", BufferProgrmaNumber;	
					END ! IF(PST(5).#RUN = 1)
				ELSE
					IF(PST(5).#RUN = 1)
						
						STOP 5;
						
						! *** MMI Message							
						DISP "[#%d buffer] [req] O/P Stop CMD Dynamometer PID Progrma Stop.", BufferProgrmaNumber;						
						
					END ! IF(PST(5).#RUN = 1)
				END	!IF((g_Feedback_Speed > g_Hmi_bs3003_Eng_StartSpeedSt)
			
				IF(PST(6).#RUN = 1)
					
					STOP 6;
					
					! *** MMI Message							
					DISP "[#%d buffer] [req] O/P Stop CMD Engine PID Progrma Stop.", BufferProgrmaNumber;						
						
				END ! IF(PST(6).#RUN = 1)
									
				! *** Engine PID Program Running and Dynamometer PID Stop Check.
				WAIT(500);				
				
				IF(PST(6).#RUN = 0)
					! *** MMI Message							
					DISP "[#%d buffer] O/P Stop CMD Engine PID Progrma Stop", BufferProgrmaNumber;
				ELSE
					! *** MMI Message							
					DISP "[#%d buffer] O/P Stop CMD Engine PID Progrma Run.", BufferProgrmaNumber;
					
					! *** Error register ***
					g_Alarm_Register_Request_AlarmID = enum_aID_Buf4_E_OpStopCmd_EngPID_StopFail;		
					g_Alarm_Register_Request_Bit = 1;
				END ! IF(PST(6).#RUN 0 1)						
			ELSEIF((g_ControlMode = g_CtMo_enum_Manual) & (g_SystemState_Eng_Starting = 1) & (g_SystemState_Eng_Started = 0))				
				IF(g_dynoType = g_dynoType_AC)					
					IF(g_SystemState_Dyno_On = 1)					
						IF(PST(5).#COMPILED = 1)&(PST(6).#COMPILED = 1)
							
							IF(PST(5).#RUN <> 1|PST(6).#RUN <> 1)
								IF(PST(5).#RUN <> 1)
								
									START 5,1;
								END ! IF(PST(5).#RUN <> 1)
								IF(PST(6).#RUN <> 1)
								
									START 6,1;
								END ! IF(PST(5).#RUN <> 1)
								WAIT(500);
							END		
							! *** Engine PID Program Running and Dynamometer PID Running  State Check.
							
							IF(PST(5).#RUN = 1 & PST(6).#RUN = 1)
								! *** MMI Message							
								!DISP "[#%d buffer] Manual; Engine Start; Dynamometer PID Program Start Complete.", BufferProgrmaNumber;						
							ELSE
								! *** MMI Message							
								!DISP "[#%d buffer] Manual; Engine Start; Dynamometer PID Program Start Fail.", BufferProgrmaNumber;		
								
								! *** Error register ***
								g_Alarm_Register_Request_AlarmID = enum_aID_Buf4_E_EngineStart_DynoPIDStartCMD_Fail;		
								g_Alarm_Register_Request_Bit = 1;
							END ! IF(PST(5).#RUN = 1)
														
						ELSE
							
							! *** Error register ***
							g_Alarm_Register_Request_AlarmID = enum_aID_Buf4_E_EngineStart_DynoPID_NotCompiles ;		
							g_Alarm_Register_Request_Bit = 1;

							! *** Program Resule Update
							g_bufferProgramResult(0)(BufferProgrmaNumber) = enum_aID_Buf4_E_EngineStart_DynoPID_NotCompiles ;
							DISP "[#%d buffer] Manual; Engine Start; Dynamometer PID Progrma not compiled. Program start fail.", BufferProgrmaNumber;
						END ! IF(PST(4).#COMPILED = 1)
					END ! IF(g_SystemState_Dyno_On = 1)
					
				END ! IF(g_dynoType = g_dynoType_AC)
			ELSEIF(((g_ControlMode = g_CtMo_enum_Manual) | (g_ControlMode = g_CtMo_enum_Automatic) | (g_ControlMode = g_CtMo_enum_Remote)) & (DrMnCtrl_PreviousDriveMode <> g_DriveManual_Mode))
			
				DrMnCtrl_PreviousDriveMode = g_DriveManual_Mode;
				
				IF((g_DriveManual_Mode = g_DrMn_enum_Idle) & (g_SystemState_Eng_Started = 1))					
					
					IF((PST(5).#COMPILED = 1) & (PST(6).#COMPILED = 1))
						
						IF(PST(5).#RUN = 1)
							
							STOP 5;
							
						END
						IF(PST(6).#RUN <> 1)	
						
							Start 6,1;					
							
						END
											
						! *** Engine PID Program Running and Dynamometer PID Stop  State Check.
						WAIT(500);
						
						IF(PST(5).#RUN = 0)
							! *** MMI Message							
							DISP "[#%d buffer] Manual Idle Dynamometer PID Progrma Stop Complete.", BufferProgrmaNumber;						
						ELSE
							! *** MMI Message							
							DISP "[#%d buffer] Manual Idle Dynamometer PID Progrma Stop Fail.", BufferProgrmaNumber;		
							
							! *** Error register ***
							g_Alarm_Register_Request_AlarmID = enum_aID_Buf4_E_IdleMode_DynoPIDStopCMD_Fail;		
							g_Alarm_Register_Request_Bit = 1;
						END !IF(PST(5).#RUN = 0)
						 
						
						IF(PST(6).#RUN = 1)
							! *** MMI Message							
							DISP "[#%d buffer] Manual Idle Engine PID Progrma Start Complete.", BufferProgrmaNumber;
						ELSE
							! *** MMI Message							
							DISP "[#%d buffer] Manual Idle Engine PID Progrma Start Fail.", BufferProgrmaNumber;
							
							! *** Error register ***
							g_Alarm_Register_Request_AlarmID = enum_aID_Buf4_E_IdleMode_EnginePIDStartCMD_Fail;		
							g_Alarm_Register_Request_Bit = 1;
						END ! IF(PST(6).#RUN = 1)
						
						
					ELSE
						
						! *** Error register ***
						g_Alarm_Register_Request_AlarmID = enum_aID_Buf4_E_IdleMode_EngDynoPID_NotCompiles;		
						g_Alarm_Register_Request_Bit = 1;

						! *** Program Resule Update
						g_bufferProgramResult(0)(BufferProgrmaNumber) = enum_aID_Buf4_E_IdleMode_EngDynoPID_NotCompiles;
						DISP "[#%d buffer] IDLE Engine / Dynamometer PID Progrma not compiled. Program start fail.", BufferProgrmaNumber;
					END ! IF((PST(4).#COMPILED = 1) & (PST(5).#COMPILED = 1))
						
				ELSEIF((g_ControlMode = g_CtMo_enum_Automatic) & ((g_DriveManual_Mode = g_DrMn_enum_IdleContrl) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngRPM) | (g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTo)))
					
					IF((g_SystemState_Eng_Started = 1) & (g_SystemState_Dyno_On = 1))
						IF((PST(5).#COMPILED = 1) & (PST(6).#COMPILED = 1))
							
							IF(PST(5).#RUN <> 1)
							
								START 5,1;
							END
							IF(PST(6).#RUN <> 1)
								
								START 6,1;
							END
												
							! *** Engine PID Program Running and Dynamometer PID Running  State Check.
							WAIT(500);
							
							IF(PST(5).#RUN = 1)
								! *** MMI Message							
								DISP "[#%d buffer] Automatic Except Idle Dynamometer PID Progrma Start Complete.", BufferProgrmaNumber;						
							ELSE
								! *** MMI Message							
								DISP "[#%d buffer] Automatic Except Idle Dynamometer PID Progrma Start Fail.", BufferProgrmaNumber;		
								
								! *** Error register ***
								g_Alarm_Register_Request_AlarmID = enum_aID_Buf4_E_ExceptIdleMode_DynoPIDStartCMD_Fail;		
								g_Alarm_Register_Request_Bit = 1;
							END ! IF(PST(5).#RUN = 1)
							
							
							IF(PST(6).#RUN = 1)
								! *** MMI Message							
								DISP "[#%d buffer] Automatic Except Idle Engine PID Progrma Start Complete.", BufferProgrmaNumber;
							ELSE
								! *** MMI Message							
								DISP "[#%d buffer] Automatic Except Idle Engine PID Progrma Start Fail.", BufferProgrmaNumber;
								
								! *** Error register ***
								g_Alarm_Register_Request_AlarmID = enum_aID_Buf4_E_ExceptIdleMode_EnginePIDStartCMD_Fail;		
								g_Alarm_Register_Request_Bit = 1;
							END !IF(PST(6).#RUN = 1)
							
							
						ELSE
							
							! *** Error register ***
							g_Alarm_Register_Request_AlarmID = enum_aID_Buf4_E_ExceptIdleMode_EngDynoPID_NotCompiles ;		
							g_Alarm_Register_Request_Bit = 1;

							! *** Program Resule Update
							g_bufferProgramResult(0)(BufferProgrmaNumber) = enum_aID_Buf4_E_ExceptIdleMode_EngDynoPID_NotCompiles ;
							DISP "[#%d buffer] Except IDLE Engine / Dynamometer PID Progrma not compiled. Program start fail.", BufferProgrmaNumber;
						END ! IF((PST(4).#COMPILED = 1) & (PST(5).#COMPILED = 1))
					END ! IF((g_SystemState_Eng_Started = 1) & (g_SystemState_Dyno_On = 1))
					
				ELSEIF(((g_ControlMode = g_CtMo_enum_Manual) | (g_ControlMode = g_CtMo_enum_Remote) ) & ((g_DriveManual_Mode = g_DrMn_enum_IdleContrl) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngRPM) | (g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTo)))
									
					IF((PST(5).#COMPILED = 1) & (PST(6).#COMPILED = 1))
						
						IF(PST(5).#RUN <> 1)
							DISP "[#%d buffer] Manual Except Idle Dynamometer PID Progrma Start.", BufferProgrmaNumber;
							START 5,1;
						END
						IF(PST(6).#RUN <> 1)
							DISP "[#%d buffer] Manual Except Idle Engine PID Progrma Start.", BufferProgrmaNumber;
							
							START 6,1;
						END
											
						! *** Engine PID Program Running and Dynamometer PID Running  State Check.
						WAIT(500);
						
						IF(PST(5).#RUN = 1)
							! *** MMI Message							
							DISP "[#%d buffer] Manual Except Idle Dynamometer PID Progrma Start Complete.", BufferProgrmaNumber;						
						ELSE
							! *** MMI Message							
							DISP "[#%d buffer] Manual Except Idle Dynamometer PID Progrma Start Fail.", BufferProgrmaNumber;		
							
							! *** Error register ***
							g_Alarm_Register_Request_AlarmID = enum_aID_Buf4_E_ExceptIdleMode_DynoPIDStartCMD_Fail;		
							g_Alarm_Register_Request_Bit = 1;
						END ! IF(PST(5).#RUN = 1)
						
						
						IF(PST(6).#RUN = 1)
							! *** MMI Message							
							DISP "[#%d buffer] Manual Except Idle Engine PID Progrma Start Complete.", BufferProgrmaNumber;
						ELSE
							! *** MMI Message							
							DISP "[#%d buffer] Manual Except Idle Engine PID Progrma Start Fail.", BufferProgrmaNumber;
							
							! *** Error register ***
							g_Alarm_Register_Request_AlarmID = enum_aID_Buf4_E_ExceptIdleMode_EnginePIDStartCMD_Fail;		
							g_Alarm_Register_Request_Bit = 1;
						END !IF(PST(6).#RUN = 1)

					ELSE
						
						! *** Error register ***
						g_Alarm_Register_Request_AlarmID = enum_aID_Buf4_E_ExceptIdleMode_EngDynoPID_NotCompiles ;		
						g_Alarm_Register_Request_Bit = 1;

						! *** Program Resule Update
						g_bufferProgramResult(0)(BufferProgrmaNumber) = enum_aID_Buf4_E_ExceptIdleMode_EngDynoPID_NotCompiles ;
						DISP "[#%d buffer] Except IDLE Engine / Dynamometer PID Progrma not compiled. Program start fail.", BufferProgrmaNumber;
					END ! IF((PST(4).#COMPILED = 1) & (PST(5).#COMPILED = 1))
						
					
				END ! IF(g_DriveManual_Mode = g_DrMn_enum_Idle) 
			END ! IF(g_ControlMode = 	g_CtMo_enum_Manual)	
			
		END ! IF((Pid_Time_Measure_1 - Pid_Time_Measure_2) >= Dyno_PID_Excution_Interval)

	END ! BLOCK
END

! *** Program Resule Update
g_bufferProgramResult(0)(BufferProgrmaNumber) = -100;

! *** MMI Terminal display
DISP "[#%d buffer] program end.", BufferProgrmaNumber;

STOP

! *************** Autoroutine  *************** !
! ***  Mode Control & Host Interface Program Run.
ON PST(3).#RUN	
	IF (^PST(3).#RUN)	
		START 4,1;
	END
	DISP "[#%d buffer] Buffer #5 Run -> Buffer #4 strat in autoroutine", BufferProgrmaNumber
	
RET

! ----------------------------------------------------------------------------------------------

#5
! *** Dynamometer Control Program

INT BufferProgrmaNumber
	BufferProgrmaNumber = 5		
! *** MMI Show Message
DISP "[#%d buffer] program started.", BufferProgrmaNumber;

! Alarm ID Define (Warning Alarm ID = (Buffer No * 10000) + Warning No, Error Alarm ID = Warning Alarm ID * -1)	
INT enum_aID_Buf5_W_PIDOperable_Fault;
	enum_aID_Buf5_W_PIDOperable_Fault = -50001;
INT enum_aID_Buf5_W_PID_cLimit_Fault;
	enum_aID_Buf5_W_PID_cLimit_Fault = -51001;
INT enum_aID_Buf5_W_PID_yLimit_Fault;
	enum_aID_Buf5_W_PID_yLimit_Fault = -51002;

	XARRSIZE = 1000000;
	g_PID_MaxRatio = 10;

	real G1;
	real G2;
	
	GLOBAL INT g_Buffer5_displayCmdLevel;
		g_Buffer5_displayCmdLevel = 0;
	
	! *** Dynamometer Control Variable
	!GLOBAL INT g_Dyno_Control_Operable;
		g_Dyno_Control_Operable = 0;
	
	!GLOBAL INT g_Dyno_ControlMode;	
	REAL Dyno_PID_Excution_Interval;
		Dyno_PID_Excution_Interval = 2;
	
	INT Dyno_DrvMd_Previous_Mode;
		Dyno_DrvMd_Previous_Mode = g_DrMn_enum_Default;
	
		
	INT Dyno_enum_ControlMode_Default;
	INT Dyno_enum_ControlMode_Idle;
	INT Dyno_enum_ControlMode_Speed;	
	INT Dyno_enum_ControlMode_Torque;	
		
		Dyno_enum_ControlMode_Default = 10;
		Dyno_enum_ControlMode_Idle = 100;
		Dyno_enum_ControlMode_Speed = 200;
		Dyno_enum_ControlMode_Torque = 300;
	
	int Dyno_Prev_ControlMode;

		g_Dyno_ControlMode = Dyno_enum_ControlMode_Default;
		Dyno_Prev_ControlMode = Dyno_enum_ControlMode_Default;
	! *** PID Control State Machine Variable
	!GLOBAL INT g_Dyno_PidControl_StateMachine;	
	INT Dyno_enum_PidStateMachine_Default;
	INT Dyno_enum_PidStateMachine_Error;
	INT Dyno_enum_PidStateMachine_Idle;
	INT Dyno_enum_PidStateMachine_Init;
	INT Dyno_enum_PidStateMachine_Steady;
	INT Dyno_enum_PidStateMachine_Change;
	INT Dyno_enum_PidStateMachine_Stop;
	INT Dyno_enum_PidStateMachine_ControlModeChange;
	
		Dyno_enum_PidStateMachine_Default = 10;
		Dyno_enum_PidStateMachine_Error = 100;
		Dyno_enum_PidStateMachine_Idle = 200;
		Dyno_enum_PidStateMachine_Init = 300
		Dyno_enum_PidStateMachine_Steady = 400;
		Dyno_enum_PidStateMachine_Change = 500;
		Dyno_enum_PidStateMachine_Stop = 600;
		Dyno_enum_PidStateMachine_ControlModeChange = 700;
		
	! *** Compansation Variable 
	REAL Torque_CompanRefeVar;
	
	! *** PID Controler Variable 	
	INT Dyno_Pid_Control_Operable;
	INT Dyno_Pid_Control_CErrorCnt;
	INT Dyno_Pid_Control_YErrorCnt;
	INT Dyno_T_Pid_Control_TdCnt;
	REAL Dyno_Pid_Control_YPrevValue;	
	REAL Dyno_S_PID_ValList(1)(29);
	REAL Dyno_T_PID_ValList(1)(29);
	!GLOBAL REAL g_Dyno_PID_Process_ValList(1)(29);	
		
	! *** PID Controler Variable List Index
	INT index_PidList_Kp;
	INT index_PidList_Ki;
	INT index_PidList_Kd;
	INT index_PidList_CIndex;
	INT index_PidList_C;
	INT index_PidList_Cmax;
	INT index_PidList_CNTmax;
	INT index_PidList_RefADelta;
	INT index_PidList_RefDDelta;
	INT index_PidList_YIndex;
	INT index_PidList_Y;
	INT index_PidList_Ymin;
	INT index_PidList_Ymax;	
	INT index_PidList_YCNTmax;
	INT index_PidList_TargetIndex;	
	INT index_PidList_Target;	
	INT index_PidList_Ref;
	INT index_PidList_P_Error;
	INT index_PidList_P_PrevError;
	INT index_PidList_I_Error;
	INT index_PidList_D_Error;
	INT index_PidList_P_Portion;
	INT index_PidList_I_Portion;
	INT index_PidList_D_Portion;
	INT index_PidList_PidIntervals;
	INT index_PidList_CADelta;
	INT index_PidList_CDDelta;
	INT index_PidList_C_Ref;
	INT index_PidList_C_PrevOutput;
	
		CALL sr_PidControler_VariableList_Initial;
	
	! *** PID Data Logger Variable
	!GLOBAL REAL g_Dyno_PID_DataLogger_Matrix(10000)(29)	
	INT Pid_DataLogger_Matrix_MaxRowIndex
	INT Pid_DataLogger_Matrix_MaxColumnIndex
	INT Pid_DataLogger_Matrix_RowCopy_Index	
		
		FILL(0, g_Dyno_PID_DataLogger_Matrix)
		Pid_DataLogger_Matrix_MaxRowIndex = 10000 - 1
		Pid_DataLogger_Matrix_MaxColumnIndex = 29 - 1
		Pid_DataLogger_Matrix_RowCopy_Index = 0;		
	
	! *** PID Time Measure
	REAL Pid_Time_Measure_1;
	REAL Pid_Time_Measure_2;
	REAL Pid_Time_Difference;
		
		Pid_Time_Measure_1 = 0;
		Pid_Time_Measure_2 = 0;
		Pid_Time_Difference = 0;	
		
	CALL sr_PidControler_ControlModeSelect_PidParaAppend;	
	CALL sr_PidControler_ExecutionContition_Check;
	
	! ***
	! *** PID Controller 
	! ***		
	g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Change;

	WHILE ((g_Dyno_Control_Operable = 1) & (PST(3).#RUN = 1)& (PST(4).#RUN = 1))
	
	
		BLOCK !Dynamometer PID		
			
			! PID Control excution condition check
			g_Dyno_Control_Operable = 0;
			
			IF ((g_stop_StopMode = g_stop_enum_SoftStopping) | (g_stop_StopMode = g_stop_enum_HardStopping))
				IF(g_SystemState_Dyno_On = 1)
							
					g_Dyno_Control_Operable = 1;
					
					! *** MMI Show Message
					IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Check PID condition On.;Stop mode; Stop Mode= <%d>.", BufferProgrmaNumber, g_stop_StopMode; END
					
				END ! IF(g_SystemState_Dyno_On = 1)	
			ELSEIF((g_SystemState_Eng_Starting = 1) & (g_SystemState_Eng_Started = 0))
				IF(g_SystemState_Eng_IG_On = 1)
							
					g_Dyno_Control_Operable = 1;
					
					! *** MMI Show Message
					IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Check PID condition On.;Engien Start.", BufferProgrmaNumber; END
					
				END ! IF(g_SystemState_Dyno_On = 1)	
			ELSE
			
				IF(g_Error_Exists_Bit = 0)
					!IF(g_SystemState_Dyno_On = 1)
						IF((g_ControlMode <> g_CtMo_enum_Default) & (g_DriveManual_Mode <> g_DrMn_enum_Default)|g_PID_Change_Ramp>0)		
							IF((g_Dyno_ControlMode = Dyno_enum_ControlMode_Idle) | (g_Dyno_ControlMode = Dyno_enum_ControlMode_Speed)| (g_Dyno_ControlMode = Dyno_enum_ControlMode_Torque))
								g_Dyno_Control_Operable = 1;
								
								! *** MMI Show Message
								IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Check PID condition On.;Normal.", BufferProgrmaNumber; END
					
							END!IF(g_Dyno_ControlMode <> g_Dyno_enum_ControlMode_Default)		
						END	! IF((g_ControlMode <> g_CtMo_enum_Default) & (g_DriveManual_Mode <> g_DrMn_enum_Default))			
					!END !IF(g_SystemState_Dyno_On = 1)				
				END ! IF(g_Error_Exists_Bit = 0)
				
			
			END !IF ((g_stop_StopMode = g_stop_enum_SoftStopping) | (g_stop_StopMode = g_stop_enum_HardStopping))
			
			IF((g_stop_StopMode = g_stop_enum_IdleStopping)| (g_stop_StopMode = g_stop_enum_SoftStopping) | (g_stop_StopMode = g_stop_enum_HardStopping))
				
				g_Dyno_Control_Operable = 0;
				
				! *** MMI Show Message
				!IF (g_Buffer5_displayCmdLevel <> 0); 
				DISP "[#%d buffer] [req] ATOM Sw Idle Stop; PID Operable Off. %d", BufferProgrmaNumber,g_stop_StopMode; 
				!END
				
			END ! (g_stop_StopMode = g_stop_enum_IdleStopping)
			
			
			IF(g_Dyno_Control_Operable <> 1)
				! *** Error register ***
				g_Alarm_Register_Request_AlarmID = enum_aID_Buf5_W_PIDOperable_Fault;		
				g_Alarm_Register_Request_Bit = 1;
				
				! *** MMI Show Message
				DISP "[#%d buffer] [req] PID Operable Off.", BufferProgrmaNumber; 
					
			END !IF(g_Dyno_Control_Operable <> 1)
				
				! *** Measurement of PID control time 
				Pid_Time_Measure_1 = TIME
				
				IF((Pid_Time_Measure_1 - Pid_Time_Measure_2) >= Dyno_PID_Excution_Interval)					
			
					! *** measure the pid execution time.
					Pid_Time_Difference = Pid_Time_Measure_1 - Pid_Time_Measure_2
					Pid_Time_Measure_2 = Pid_Time_Measure_1				
					g_Dyno_PID_Process_ValList(0)(index_PidList_PidIntervals) = Pid_Time_Difference;
															
					! ----------------------------------------------------------------------------------------------	
					!disp 0, g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref),g_Dyno_PID_Process_ValList(0)(index_PidList_C_PrevOutput),g_Dyno_PID_Process_ValList(0)(index_PidList_C),g_Dyno_PID_Process_ValList(0)(index_PidList_Y),g_Dyno_PID_Process_ValList(0)(index_PidList_Target),g_Dyno_PID_Process_ValList(0)(index_PidList_I_Portion),g_Dyno_PID_Process_ValList(0)(index_PidList_P_Portion) ;
									
					! *** Drive Mode Change Process
					IF((g_DriveManual_Mode <> g_DrMn_enum_Default) & (g_DriveManual_Mode <> Dyno_DrvMd_Previous_Mode ))
						
							! *** MMI Show Message
							IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Drive Mode Change Process. ChgReqMode= <%d>.", BufferProgrmaNumber, g_DriveManual_Mode; END
						
							IF(g_DriveManual_Mode = g_DrMn_enum_Idle)
								Dyno_Prev_ControlMode = g_Dyno_ControlMode;

								g_Dyno_ControlMode = Dyno_enum_ControlMode_Idle;	
								
								! *** MMI Show Message
								IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Drive Mode Change Process. Dyno. Control Mode = Idle.", BufferProgrmaNumber; END

							ELSEIF((g_DriveManual_Mode = g_DrMn_enum_IdleContrl) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngRPM))
							
								if (g_Dyno_ControlMode<>Dyno_enum_ControlMode_Torque)
									Dyno_Prev_ControlMode = g_Dyno_ControlMode;
									g_Dyno_ControlMode = Dyno_enum_ControlMode_Torque;	
									CALL sr_g_Dyno_PID_Process_ValList_INIT_TORQUE;
									!COPY(Dyno_T_PID_ValList, g_Dyno_PID_Process_ValList);			

									g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_ControlModeChange;
									! *** MMI Show Message
									IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Drive Mode Change Process. Dyno. Control Mode = Torque.", BufferProgrmaNumber; END
								end
							ELSEIF((g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTo) | (g_stop_enum_HardStopping = g_stop_enum_HardStopping))
								if (g_Dyno_ControlMode<>Dyno_enum_ControlMode_Speed)
									Dyno_Prev_ControlMode = g_Dyno_ControlMode;
									g_Dyno_ControlMode = Dyno_enum_ControlMode_Speed;		
									CALL sr_g_Dyno_PID_Process_ValList_INIT_SPEED;
									!COPY(Dyno_S_PID_ValList, g_Dyno_PID_Process_ValList);			

									g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_ControlModeChange;
									! *** MMI Show Message
									IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Drive Mode Change Process. Dyno. Control Mode = Speed.", BufferProgrmaNumber; END
								end
							ELSE
								Dyno_Prev_ControlMode = g_Dyno_ControlMode;
								g_Dyno_ControlMode = Dyno_enum_ControlMode_Default;
							END ! IF((g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngRPM))
					END ! IF((g_DriveManual_Mode <> g_DrMn_enum_Default) & (g_DriveManual_Mode <> Dyno_DrvMd_Previous_Mode ))
					!disp 1, g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref),g_Dyno_PID_Process_ValList(0)(index_PidList_C_PrevOutput),g_Dyno_PID_Process_ValList(0)(index_PidList_C),g_Dyno_PID_Process_ValList(0)(index_PidList_Y),g_Dyno_PID_Process_ValList(0)(index_PidList_Target),g_Dyno_PID_Process_ValList(0)(index_PidList_I_Portion),g_Dyno_PID_Process_ValList(0)(index_PidList_P_Portion) ;
					
					Dyno_DrvMd_Previous_Mode = g_DriveManual_Mode;						
						
					! ----------------------------------------------------------------------------------------------	
					
					! *** ref & y update
					IF(g_DriveManual_Mode = g_DrMn_enum_Idle) 
						g_Dyno_PID_Process_ValList(0)(index_PidList_Target) = 0;
						
						g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Idle;
					ELSE						
						
						IF( g_Dyno_ControlMode = Dyno_enum_ControlMode_Idle) 
							g_Dyno_PID_Process_ValList(0)(index_PidList_Target) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_Y) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_C) = 0;
						ELSEIF( g_Dyno_ControlMode = Dyno_enum_ControlMode_Speed) 
							! *** ref
							g_Dyno_PID_Process_ValList(0)(index_PidList_Target) = g_Target_Dyno_Speed;
							
							! *** y - Counter							
							g_Dyno_PID_Process_ValList(0)(index_PidList_Y) = g_Feedback_Speed;
							!disp 2, g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref),g_Dyno_PID_Process_ValList(0)(index_PidList_C_PrevOutput),g_Dyno_PID_Process_ValList(0)(index_PidList_C),g_Dyno_PID_Process_ValList(0)(index_PidList_Y),g_Dyno_PID_Process_ValList(0)(index_PidList_Target),g_Dyno_PID_Process_ValList(0)(index_PidList_I_Portion),g_Dyno_PID_Process_ValList(0)(index_PidList_P_Portion) ;
																				
						ELSEIF( g_Dyno_ControlMode = Dyno_enum_ControlMode_Torque)							
							! *** ref
							g_Dyno_PID_Process_ValList(0)(index_PidList_Target) = g_Target_Dyno_Torque;							
							! *** y
							g_Dyno_PID_Process_ValList(0)(index_PidList_Y) = g_Feedback_Torque;
							!disp 3, g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref),g_Dyno_PID_Process_ValList(0)(index_PidList_C_PrevOutput),g_Dyno_PID_Process_ValList(0)(index_PidList_C),g_Dyno_PID_Process_ValList(0)(index_PidList_Y),g_Dyno_PID_Process_ValList(0)(index_PidList_Target),g_Dyno_PID_Process_ValList(0)(index_PidList_I_Portion),g_Dyno_PID_Process_ValList(0)(index_PidList_P_Portion) ;
						ELSE
						
							g_Dyno_PID_Process_ValList(0)(index_PidList_Target) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_Y) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_C) = 0;
							
						END
					END
					
					! ----------------------------------------------------------------------------------------------	
					
					! *** Soft/Hard Stopping Ref/Control Mode
					IF((g_stop_StopMode = g_stop_enum_SoftStopping) | (g_stop_StopMode = g_stop_enum_HardStopping))
					
						IF (g_Dyno_ControlMode <> Dyno_enum_ControlMode_Speed)						
								g_Dyno_ControlMode = Dyno_enum_ControlMode_Speed;		
											
								g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_ControlModeChange;
								! *** MMI Show Message
								IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Stop; Drive Mode Change Process. Dyno. Control Mode = Speed.", BufferProgrmaNumber; END											
							
						END !IF (g_Dyno_ControlMode <> Dyno_enum_ControlMode_Speed)	
						
						! *** ref
						g_Dyno_PID_Process_ValList(0)(index_PidList_Target) = 0;
						
						! *** y - Counter							
						g_Dyno_PID_Process_ValList(0)(index_PidList_Y) = g_Feedback_Speed;
					
					ELSEIF((g_SystemState_Eng_Starting = 1) & (g_SystemState_Eng_Started = 0))
					
						IF(g_dynoType = g_dynoType_AC)	
						
							IF (g_Dyno_ControlMode <> Dyno_enum_ControlMode_Torque)						
									g_Dyno_ControlMode = Dyno_enum_ControlMode_Torque;		
												
									g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_ControlModeChange;
									! *** MMI Show Message
									IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Eng Start; Drive Mode Change Process. Dyno. Control Mode = Torque.", BufferProgrmaNumber; END											
								
							END !IF (g_Dyno_ControlMode <> Dyno_enum_ControlMode_Speed)	
							
							! *** ref
							g_Dyno_PID_Process_ValList(0)(index_PidList_Target) = g_Hmi_bs3001_Eng_DynoStartTorqueSt;
							g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) = g_Hmi_bs3001_Eng_DynoStartTorqueSt;
							! *** y
							g_Dyno_PID_Process_ValList(0)(index_PidList_Y) = g_Feedback_Torque;
							
						END ! IF(g_dynoType = g_dynoType_AC)	
						
					END ! IF((g_stop_enum_HardStopping = g_stop_enum_HardStopping) | (g_stop_enum_HardStopping = g_stop_enum_HardStopping))
					
					! ----------------------------------------------------------------------------------------------	
					
				IF (1) !g_Dyno_ControlMode <> Dyno_enum_ControlMode_Speed)	
					! *** pid Controller state machine
					IF(g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Error)					
						! *** MMI Show Message
						IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State = Error.", BufferProgrmaNumber; END
						
						g_Dyno_PID_Process_ValList(0)(index_PidList_Y) = 0;
						Dyno_Pid_Control_Operable = 0;						
						
					ELSEIF(g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Idle)
						! *** MMI Show Message
						IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State = Idle.", BufferProgrmaNumber; END
						
						g_Dyno_PID_Process_ValList(0)(index_PidList_Y) = 0;
						g_AO_DynoControlSignal = 25;

						Dyno_Pid_Control_Operable = 0;
						
						IF(g_Dyno_PID_Process_ValList(0)(index_PidList_Target) > (g_Dyno_PID_Process_ValList(0)(index_PidList_Ymin) + 2))
							! *** MMI Show Message
							IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State Change. Idle >>> Init.", BufferProgrmaNumber; END
							
							IF(g_Dyno_ControlMode = Dyno_enum_ControlMode_Speed)							
								
								IF (g_dynoType = g_dynoType_AC)	
									g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Init;
								ELSE								
									IF (g_Feedback_Speed > (g_Dyno_PID_Process_ValList(0)(index_PidList_Ref)))
										g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Init;
									END ! IF (g_Feedback_Speed > (g_Dyno_PID_Process_ValList(0)(index_PidList_Ref)))
								END ! IF (g_dynoType = g_dynoType_AC)	
								
							ELSE
								g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Init;
							END
						END
						IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State  = Idle. %d", BufferProgrmaNumber, g_Dyno_PID_Process_ValList(0)(index_PidList_C);
					 	END
					ELSEIF(g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Init)					
						! *** MMI Show Message
						IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State = Init.", BufferProgrmaNumber; END
						
						Dyno_Pid_Control_Operable = 0;					
						
						Dyno_Pid_Control_CErrorCnt = 0;
						Dyno_Pid_Control_YErrorCnt = 0;
						Dyno_T_Pid_Control_TdCnt = 0;
						Dyno_Pid_Control_YPrevValue = 0;
										
						! *** Speed PID Controller Variable Initialization	
						IF( g_Dyno_ControlMode = Dyno_enum_ControlMode_Idle) 
							g_Dyno_PID_Process_ValList(0)(index_PidList_C) = 0;	
							g_Dyno_PID_Process_ValList(0)(index_PidList_Y) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_Target) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_P_Error) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_P_PrevError) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_I_Error) = 0;	
							g_Dyno_PID_Process_ValList(0)(index_PidList_D_Error) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_P_Portion) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_I_Portion) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_D_Portion) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_PidIntervals) = 0;			
							g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref) = g_Hmi_bsHeader1_Fb_Speed;			
							g_Dyno_PID_Process_ValList(0)(index_PidList_C_PrevOutput) = g_Hmi_bsHeader1_Fb_Speed;			
						! *** Speed PID Controller Variable Initialization	
						ELSEIF( g_Dyno_ControlMode = Dyno_enum_ControlMode_Speed) 
							g_Dyno_PID_Process_ValList(0)(index_PidList_C) = 0;	
							g_Dyno_PID_Process_ValList(0)(index_PidList_Y) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_Target) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) = g_Feedback_Speed;
							g_Dyno_PID_Process_ValList(0)(index_PidList_P_Error) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_P_PrevError) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_I_Error) = 0;	
							g_Dyno_PID_Process_ValList(0)(index_PidList_D_Error) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_P_Portion) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_I_Portion) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_D_Portion) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_PidIntervals) = 0;		
							g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref) = g_Hmi_bsHeader1_Fb_Speed;			
							g_Dyno_PID_Process_ValList(0)(index_PidList_C_PrevOutput) = g_Hmi_bsHeader1_Fb_Speed;			
						! *** Torque PID Control Variable List Setting		
						ELSEIF( g_Dyno_ControlMode = Dyno_enum_ControlMode_Torque)
							g_Dyno_PID_Process_ValList(0)(index_PidList_C) = 0;						
							g_Dyno_PID_Process_ValList(0)(index_PidList_Y) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_Target) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) = g_Feedback_Torque;
							g_Dyno_PID_Process_ValList(0)(index_PidList_P_Error) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_P_PrevError) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_I_Error) = 0;	
							g_Dyno_PID_Process_ValList(0)(index_PidList_D_Error) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_P_Portion) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_I_Portion) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_D_Portion) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_PidIntervals) = 0;
							g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref) = g_Hmi_bsHeader1_Fb_Speed;			
							g_Dyno_PID_Process_ValList(0)(index_PidList_C_PrevOutput) = g_Hmi_bsHeader1_Fb_Speed;			
						END
						! *** MMI Show Message
						IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State Change. Init >>> Change.", BufferProgrmaNumber; END
						
						g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Change;
												
					ELSEIF(g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Steady)
						! *** MMI Show Message
						IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State = Steady.", BufferProgrmaNumber; END
						
						IF((g_Dyno_PID_Process_ValList(0)(index_PidList_Target)) < (g_Dyno_PID_Process_ValList(0)(index_PidList_Ymin) + 2))
								! *** MMI Show Message
							IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State Change. Steady >>> Stop.", BufferProgrmaNumber; END
							
							g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Stop;
						ELSE					
						
							IF(ABS(g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) - g_Dyno_PID_Process_ValList(0)(index_PidList_Target)) > 2)
								! *** MMI Show Message
								IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State Change. Steady >>> Change.", BufferProgrmaNumber; END
							
								g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Change;
							ELSE
								g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) = g_Dyno_PID_Process_ValList(0)(index_PidList_Target);
								
								! *** Dyno Torque Correction TdCnt increase
								!IF( g_Dyno_ControlMode = Dyno_enum_ControlMode_Torque)
									Dyno_T_Pid_Control_TdCnt = Dyno_T_Pid_Control_TdCnt + 1;
								!END ! IF( g_Dyno_ControlMode = Dyno_enum_ControlMode_Torque)
							END ! IF(ABS(g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) - g_Dyno_PID_Process_ValList(0)(index_PidList_Target)) > 2)
							
							Dyno_Pid_Control_Operable = 1;
						END ! IF((g_Dyno_PID_Process_ValList(0)(index_PidList_Target)) < (g_Dyno_PID_Process_ValList(0)(index_PidList_Ymin) + 2))
						
					ELSEIF(g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Change)
						
						! *** MMI Show Message
						IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State = Change. Target = <%d>, Ref = <%d> ", BufferProgrmaNumber, g_Dyno_PID_Process_ValList(0)(index_PidList_Target), g_Dyno_PID_Process_ValList(0)(index_PidList_Ref); END
						
						IF((g_Dyno_PID_Process_ValList(0)(index_PidList_Target) - g_Dyno_PID_Process_ValList(0)(index_PidList_Ref)) > 2)
							
							IF(g_SystemState_Eng_Started = 1)							
								g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) = (g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) + (g_Dyno_PID_Process_ValList(0)(index_PidList_RefADelta)));
							ELSE
								g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) = g_Dyno_PID_Process_ValList(0)(index_PidList_Target);
							END ! IF(g_SystemState_Eng_Started = 1)
							
							IF(g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) > g_Dyno_PID_Process_ValList(0)(index_PidList_Target))
								g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) = g_Dyno_PID_Process_ValList(0)(index_PidList_Target);
							END ! IF(g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) > g_Dyno_PID_Process_ValList(0)(index_PidList_Target))
							
							! *** MMI Show Message
							IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State  = Change. Acc Target Ref = <%d>", BufferProgrmaNumber, g_Dyno_PID_Process_ValList(0)(index_PidList_Ref); END
						
						ELSEIF((g_Dyno_PID_Process_ValList(0)(index_PidList_Target) - g_Dyno_PID_Process_ValList(0)(index_PidList_Ref)) < -2)
							
							IF(g_SystemState_Eng_Started = 1)	
								IF(g_stop_StopMode <> g_stop_enum_HardStopping)
									g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) = ((g_Dyno_PID_Process_ValList(0)(index_PidList_Ref)) - (g_Dyno_PID_Process_ValList(0)(index_PidList_RefDDelta)));
								ELSE
									g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) = g_Dyno_PID_Process_ValList(0)(index_PidList_Target);
								END ! IF(g_stop_StopMode <> g_stop_enum_HardStopping)
							ELSE
								g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) = g_Dyno_PID_Process_ValList(0)(index_PidList_Target);
							END !IF(g_SystemState_Eng_Started = 1)	
							
							IF(g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) < g_Dyno_PID_Process_ValList(0)(index_PidList_Target))
								g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) = g_Dyno_PID_Process_ValList(0)(index_PidList_Target);
							END ! IF(g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) < g_Dyno_PID_Process_ValList(0)(index_PidList_Target))
							
							! *** MMI Show Message
							IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State  = Change. Dec Target Ref = <%d>", BufferProgrmaNumber, g_Dyno_PID_Process_ValList(0)(index_PidList_Ref); END
						
						ELSE
							IF((g_Dyno_PID_Process_ValList(0)(index_PidList_Target)) < (g_Dyno_PID_Process_ValList(0)(index_PidList_Ymin) + 2))
								! *** MMI Show Message
								IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State Change. Change >>> Stop.", BufferProgrmaNumber; END
								
								g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Stop;
							ELSE
								! *** MMI Show Message
								IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State Change. Change >>> Steady.", BufferProgrmaNumber; END
								!DISP "[#%d buffer] Steady "								
								g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Steady;
								g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) = g_Dyno_PID_Process_ValList(0)(index_PidList_Target);
								
							END ! IF(g_Dyno_PID_Process_ValList(0)(index_PidList_Target) = 0)								
							
						END ! IF((g_Dyno_PID_Process_ValList()(index_PidList_gTarget) - g_Dyno_PID_Process_ValList()(index_PidList_gRef)) = 0)
				
						Dyno_Pid_Control_Operable = 1;
						
					ELSEIF(g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Stop)						
						! *** MMI Show Message
						IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State  = Stop.", BufferProgrmaNumber; END
						
						IF(g_Dyno_PID_Process_ValList(0)(index_PidList_Y) <= g_Dyno_PID_Process_ValList(0)(index_PidList_Ymin))
							! *** MMI Show Message
							IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State Change. Stop >>> Idle", BufferProgrmaNumber; END
						
							g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Idle;
							g_Dyno_PID_Process_ValList(0)(index_PidList_C) = 0;
							!DISP "[#%d buffer] Controller State  = Stop. %d", BufferProgrmaNumber, g_Dyno_PID_Process_ValList(0)(index_PidList_C);
						    g_AO_DynoControlSignal = 25;
							Dyno_Pid_Control_Operable = 0;
						ELSEIF((g_Dyno_PID_Process_ValList(0)(index_PidList_Target)) > (g_Dyno_PID_Process_ValList(0)(index_PidList_Ymin) + 2))
							g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Change;
							!Dyno_Pid_Control_Operable = 1;
						ELSE
							g_Dyno_PID_Process_ValList(0)(index_PidList_C) = 0;
							!DISP "[#%d buffer] Controller State  = Stop. %d", BufferProgrmaNumber, g_Dyno_PID_Process_ValList(0)(index_PidList_C);
						  g_AO_DynoControlSignal = 25;
							Dyno_Pid_Control_Operable = 0;
						END !IF(g_Dyno_PID_Process_ValList(0)(index_PidList_Y) <= g_Dyno_PID_Process_ValList(0)(index_PidList_Ymin))
					ELSEIF(g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_ControlModeChange)
						! *** MMI Show Message
							IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State  = ControlModeChange.", BufferProgrmaNumber; END
							!disp Pid_Time_Measure_1, g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref),g_Dyno_PID_Process_ValList(0)(index_PidList_C_PrevOutput),g_Dyno_PID_Process_ValList(0)(index_PidList_C),g_Dyno_PID_Process_ValList(0)(index_PidList_Y),g_Dyno_PID_Process_ValList(0)(index_PidList_Target),g_Dyno_PID_Process_ValList(0)(index_PidList_I_Portion),g_Dyno_PID_Process_ValList(0)(index_PidList_P_Portion) ;
							IF(g_DriveManual_Mode = g_DrMn_enum_Idle)
								g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Init;								
							ELSEIF((g_DriveManual_Mode = g_DrMn_enum_IdleContrl) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngRPM))
									
								CALL sr_g_Dyno_PID_Process_ValList_INIT_TORQUE;
								!COPY(Dyno_T_PID_ValList, g_Dyno_PID_Process_ValList);
								! *** MMI Show Message
								IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] ControlModeChange. Copy:Torque >> Process.", BufferProgrmaNumber; END
						
							ELSEIF((g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTo) | (g_stop_enum_HardStopping = g_stop_enum_HardStopping))
																
								CALL sr_g_Dyno_PID_Process_ValList_INIT_SPEED;
								!COPY(Dyno_S_PID_ValList, g_Dyno_PID_Process_ValList);
								! *** MMI Show Message
								IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] ControlModeChange. Copy:Speed >> Process.", BufferProgrmaNumber; END
						
							END ! IF((g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngRPM))
							
							if (Dyno_Prev_ControlMode=Dyno_enum_ControlMode_Default)
								g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Init;
							ELSE
								g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Change;
							END
							! *** MMI Show Message
							IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State Change. ControlModeChange >>> Init", BufferProgrmaNumber; END						
					ELSE
						! *** MMI Show Message
						IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State = Default.", BufferProgrmaNumber; END
						
						g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Default;
						
						g_Dyno_PID_Process_ValList(0)(index_PidList_Y) = 0;
						Dyno_Pid_Control_Operable = 0;
					END
					
					! *** PID Controller
					IF(Dyno_Pid_Control_Operable = 1)

						! *** Error calculation
						g_Dyno_PID_Process_ValList(0)(index_PidList_P_Error) = (g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) - g_Dyno_PID_Process_ValList(0)(index_PidList_Y));
						IF (g_dynoType = g_dynoType_AC)	! Control DIrection Setting
							IF(g_Dyno_ControlMode = Dyno_enum_ControlMode_Speed)
								g_Dyno_PID_Process_ValList(0)(index_PidList_P_Error) = -1 * g_Dyno_PID_Process_ValList(0)(index_PidList_P_Error);
							ELSE
								!g_Dyno_PID_Process_ValList(0)(index_PidList_P_Error) = g_Dyno_PID_Process_ValList(0)(index_PidList_P_Error);
							END ! IF(g_Dyno_ControlMode = Dyno_enum_ControlMode_Speed)
						ELSE								
							!g_Dyno_PID_Process_ValList(0)(index_PidList_P_Error) = (g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) - g_Dyno_PID_Process_ValList(0)(index_PidList_Y));
						END ! IF (g_dynoType = g_dynoType_AC)	
						
						!g_Dyno_PID_Process_ValList(0)(index_PidList_D_Error) = g_Dyno_PID_Process_ValList(0)(index_PidList_P_PrevError) - g_Dyno_PID_Process_ValList(0)(index_PidList_P_Error);

						IF (Dyno_T_Pid_Control_TdCnt > g_Hmi_bs4101_DynoTpid_TdCntMaxst)
							G1 = g_Dyno_PID_Process_ValList(0)(index_PidList_Kp) + g_Dyno_PID_Process_ValList(0)(index_PidList_Ki);
							G2 = -g_Dyno_PID_Process_ValList(0)(index_PidList_Kp)
							! *** P/I calculation							
							g_Dyno_PID_Process_ValList(0)(index_PidList_P_Portion) = G1 * g_Dyno_PID_Process_ValList(0)(index_PidList_P_Error)+G2 * g_Dyno_PID_Process_ValList(0)(index_PidList_P_PrevError);
							g_Dyno_PID_Process_ValList(0)(index_PidList_I_Portion) = 0;
																
							! *** P/I Portion Sum
							g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref) = g_Dyno_PID_Process_ValList(0)(index_PidList_C_PrevOutput) + g_Dyno_PID_Process_ValList(0)(index_PidList_P_Portion) + g_Dyno_PID_Process_ValList(0)(index_PidList_I_Portion);
							!DISP "Control";
						Else
							g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref) = g_Dyno_PID_Process_ValList(0)(index_PidList_C_PrevOutput);
							!DISP "None Control";
						End
						
						! *** C Maximum limit check g_Hmi_bs3005_Eng_MaxTorqueSt
						IF ((g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref) >=(g_Hmi_bs3005_Eng_MaxTorqueSt * 5 * g_Dyno_PID_Process_ValList(0)(index_PidList_Cmax) / 100)) )						
								g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref) = g_Hmi_bs3005_Eng_MaxTorqueSt * 5 * g_Dyno_PID_Process_ValList(0)(index_PidList_Cmax) / 100 !g_Dyno_PID_Process_ValList(0)(index_PidList_Cmax) * g_PID_MaxRatio
								!DISP "Max : Cref = %d", g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref);
  							if (g_Dyno_PID_Process_ValList(0)(index_PidList_Y) = Dyno_Pid_Control_YPrevValue)
  								Dyno_Pid_Control_CErrorCnt = Dyno_Pid_Control_CErrorCnt + 1;
  							end
						ELSEIF ((g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref) <=(g_Hmi_bs3005_Eng_MinTorqueSt * g_Dyno_PID_Process_ValList(0)(index_PidList_Cmax) / 100)) )						
								g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref) = g_Hmi_bs3005_Eng_MinTorqueSt * g_Dyno_PID_Process_ValList(0)(index_PidList_Cmax) / 100 !g_Dyno_PID_Process_ValList(0)(index_PidList_Cmax) * g_PID_MaxRatio
								!DISP "Min : Cref = %d", g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref);
  							if (g_Dyno_PID_Process_ValList(0)(index_PidList_Y) = Dyno_Pid_Control_YPrevValue)
  								Dyno_Pid_Control_CErrorCnt = Dyno_Pid_Control_CErrorCnt + 1;
  							end
						ELSE
							Dyno_Pid_Control_CErrorCnt = Dyno_Pid_Control_CErrorCnt - 1;
							IF (Dyno_Pid_Control_CErrorCnt < 0)
								Dyno_Pid_Control_CErrorCnt = 0;
							END
						END !IF ((ABS(g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref)) >= ABS(g_Dyno_PID_Process_ValList(0)(index_PidList_Cmax))) & (g_Dyno_PID_Process_ValList(0)(index_PidList_Y) = Dyno_Pid_Control_YPrevValue))						

						g_Dyno_PID_Process_ValList(0)(index_PidList_P_PrevError) = g_Dyno_PID_Process_ValList(0)(index_PidList_P_Error);			
																									
								
					END	! IF(Dyno_Pid_Control_Operable = 1)
					
					! *** C Maximum error register
					IF(Dyno_Pid_Control_CErrorCnt >= g_Dyno_PID_Process_ValList(0)(index_PidList_CNTmax))
						
						! *** MMI Show Message
						IF (g_Buffer5_displayCmdLevel <> 0); DISP "[#%d buffer] C Maximum error program Stop. CErrorCnt=<%d>, SetCNTmax=<%d>, C_Ref=<%d>", BufferProgrmaNumber, Dyno_Pid_Control_CErrorCnt, g_Dyno_PID_Process_ValList(0)(index_PidList_CNTmax),g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref); END
			
						Dyno_Pid_Control_CErrorCnt = 0;
						g_Dyno_PidControl_StateMachine = Dyno_enum_PidStateMachine_Error;
						
						! *** Error register ***
						g_Alarm_Register_Request_AlarmID = enum_aID_Buf5_W_PID_cLimit_Fault;		
						g_Alarm_Register_Request_Bit = 1;
						
						! *** C Maximum error Program Stop ***
						g_Dyno_Control_Operable = 0;									
						
					END	! IF(Dyno_Pid_Control_CErrorCnt >= g_Dyno_PID_Process_ValList(0)(index_PidList_CNTmax))							
					
					Dyno_Pid_Control_YPrevValue = g_Dyno_PID_Process_ValList(0)(index_PidList_Y);
				ELSE							
					! *** C Ref
					g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref) = g_Dyno_PID_Process_ValList(0)(index_PidList_Target);
					!DISP "Ref : %d , Target : %d : ", g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref), g_Dyno_PID_Process_ValList(0)(index_PidList_Target);
														
				END ! IF(g_Eng_ControlMode <>  Eng_enum_ControlMode_ThrAlpha)		

				! *** C Ref Min/Max Check								
				IF((g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref)) < g_Hmi_bs3005_Eng_MinTorqueSt)
					g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref) = g_Hmi_bs3005_Eng_MinTorqueSt;
				ELSEIF((g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref)) > g_Hmi_bs3005_Eng_MaxTorqueSt * 5)
					g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref) = g_Hmi_bs3005_Eng_MaxTorqueSt;
				END ! IF((g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref)) < 0)
				
				! *** PID C_Ref >> C Previous Update
				g_Dyno_PID_Process_ValList(0)(index_PidList_C_PrevOutput) = g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref);
										
				! *** C Ref >> C
				g_Dyno_PID_Process_ValList(0)(index_PidList_C) = g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref)/g_Hmi_Target_Dyno_Torque_Range*32767;	!(g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref)+10)/g_Hmi_Target_Dyno_Speed_Range*32767/2010*2000; 
				
				
				!DISP Dyno_Pid_Control_Operable, g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref), g_Dyno_PID_Process_ValList(0)(index_PidList_P_Portion), g_Dyno_PID_Process_ValList(0)(index_PidList_I_Portion)
				IF (g_PID_Change_Ramp>0)
					g_PID_Change_Ramp = g_PID_Change_Ramp-1;
					!DISP "RAMP %d", g_PID_Change_Ramp;
					!DISP "TARGET %d", g_Dyno_PID_Process_ValList(0)(index_PidList_Target);
				else
					g_AO_DynoControlSignal = g_Dyno_PID_Process_ValList(0)(index_PidList_C);
					!DISP "TARGET %d", g_Dyno_PID_Process_ValList(0)(index_PidList_Target);
				end
				
				!disp "Time : %d , Real Output : %d , PID Output : %d, Control : %d, Prev Control : %d, FeedBack : %d, Target : %d", Pid_Time_Measure_1, g_AO_DynoControlSignal ,g_Dyno_PID_Process_ValList(0)(index_PidList_C), g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref),g_Dyno_PID_Process_ValList(0)(index_PidList_C_PrevOutput),g_Dyno_PID_Process_ValList(0)(index_PidList_Y),g_Dyno_PID_Process_ValList(0)(index_PidList_Target);
				!,g_Dyno_PID_Process_ValList(0)(index_PidList_I_Portion),g_Dyno_PID_Process_ValList(0)(index_PidList_P_Portion) ;
				!g_eCAT_Output_Data(0)(g_Hmi_bs5501_Dyno_CIndexSt) = g_Dyno_PID_Process_ValList(0)(index_PidList_Target)/8000*32767;
								
				! *** PID Data Logger
				!COPY(g_Dyno_PID_Process_ValList, g_Dyno_PID_DataLogger_Matrix, 0, 0, 0, Pid_DataLogger_Matrix_MaxColumnIndex, Pid_DataLogger_Matrix_RowCopy_Index, Pid_DataLogger_Matrix_RowCopy_Index, 0, Pid_DataLogger_Matrix_MaxColumnIndex)
				!Pid_DataLogger_Matrix_RowCopy_Index = Pid_DataLogger_Matrix_RowCopy_Index + 1;
				
				!IF( Pid_DataLogger_Matrix_RowCopy_Index > Pid_DataLogger_Matrix_MaxRowIndex)
				!	Pid_DataLogger_Matrix_RowCopy_Index = 0;
				!END
							
			
			END	! IF((Pid_Time_Measure_1 - Pid_Time_Measure_2) >= Dyno_PID_Excution_Interval)
		END ! BLOCK !Dynamometer PID		
	END ! WHILE (g_Dyno_Control_Operable = 1)

STOP
! *************** Autoroutine  *************** !
! ***  Mode Control (#3) Program Stop processing
ON ^PST(3).#RUN

	IF (PST(5).#RUN = 1)
		g_Dyno_Control_Operable = 0
		g_AO_DynoControlSignal = 0;
		g_SystemState_Dyno_On = 0;
		g_DO_DynoOnSignal = 0;

		DISP "[#%d buffer] Mode Control (#3)Program Stop Autoroutine.", BufferProgrmaNumber
	
	END
	
RET

! ----------------------------------------------------------------------------------------------

! ***  Target/Feedback processing (#4) Program Stop 
ON ^PST(4).#RUN

	IF (PST(5).#RUN = 1)
		g_Dyno_Control_Operable = 0
		g_AO_DynoControlSignal = 0;
		g_SystemState_Dyno_On = 0;
		g_DO_DynoOnSignal = 0;
				
		DISP "[#%d buffer] Target/Feedback processing (#4)Program Stop Autoroutine.", BufferProgrmaNumber
	
	END
	
RET

! ----------------------------------------------------------------------------------------------

! *** Program Stop processing
ON ^PST(5).#RUN
	
	g_Dyno_Control_Operable = 0
	g_AO_DynoControlSignal = 0;
	g_SystemState_Dyno_On = 0;
	g_DO_DynoOnSignal = 0;

	CALL sr_PidControler_VariableList_Initial;
	
	DISP "[#%d buffer] Program Stop Autoroutine.", BufferProgrmaNumber;	
RET

! ----------------------------------------------------------------------------------------------

! *************** Subroutine  *************** !
sr_PidControler_VariableList_Initial:
	
	! *** Compansation Variable 
	Torque_CompanRefeVar = 0;
		
	! *** PID Controler Variable 		
	Dyno_Pid_Control_Operable = 0;	
	Dyno_Pid_Control_CErrorCnt = 0;
	Dyno_Pid_Control_YErrorCnt = 0;
	Dyno_T_Pid_Control_TdCnt = 0;
	Dyno_Pid_Control_YPrevValue = 0;	
	
	! *** PID Controler Variable List Initial
	FILL(0, Dyno_S_PID_ValList);
	FILL(0, Dyno_T_PID_ValList);
	FILL(0, g_Dyno_PID_Process_ValList);
	
	! *** PID Controler Variable List Index Initial	
	index_PidList_Kp = 0;
	index_PidList_Ki = 1;
	index_PidList_Kd = 2;
	index_PidList_CIndex = 3
	index_PidList_C = 4;
	index_PidList_Cmax = 5;
	index_PidList_CNTmax = 6;
	index_PidList_RefADelta = 7;
	index_PidList_RefDDelta = 8;
	index_PidList_YIndex = 9;
	index_PidList_Y = 10;
	index_PidList_Ymin = 11;
	index_PidList_Ymax = 12;
	index_PidList_YCNTmax = 13;
	index_PidList_TargetIndex = 14;	
	index_PidList_Target = 15;
	index_PidList_Ref = 16;		
	index_PidList_P_Error = 17;
	index_PidList_P_PrevError = 18;
	index_PidList_I_Error = 19;
	index_PidList_D_Error = 20;
	index_PidList_P_Portion = 21;
	index_PidList_I_Portion = 22;	
	index_PidList_D_Portion = 23;
	index_PidList_PidIntervals = 24;
	index_PidList_CADelta = 25;
	index_PidList_CDDelta = 26;
	index_PidList_C_Ref = 27;
	index_PidList_C_PrevOutput = 28;
	
	! *** Speed PID Controller Constant & Variable Initialization	
	Dyno_S_PID_ValList(0)(index_PidList_Kp) = g_Hmi_bs4001_DynoSpid_KpSt;!Constant
	Dyno_S_PID_ValList(0)(index_PidList_Ki) = g_Hmi_bs4001_DynoSpid_KiSt;!Constant
	Dyno_S_PID_ValList(0)(index_PidList_Kd) = g_Hmi_bs4001_DynoSpid_Kdst;!Constant
	Dyno_S_PID_ValList(0)(index_PidList_CIndex) = g_Hmi_bs5501_Dyno_CIndexSt;!Constant
	Dyno_S_PID_ValList(0)(index_PidList_C) = 0;!Variable
	Dyno_S_PID_ValList(0)(index_PidList_Cmax) = g_Hmi_bs4003_DynoSpid_CMaxSt;!Constant
	Dyno_S_PID_ValList(0)(index_PidList_CNTmax) = g_Hmi_bs4003_DynoSpid_CcntMaxSt;!Constant
	Dyno_S_PID_ValList(0)(index_PidList_RefADelta) = (g_Hmi_bs4002_DynoSpid_RefADeltaSt / (1000/Dyno_PID_Excution_Interval));!Constant
	Dyno_S_PID_ValList(0)(index_PidList_RefDDelta) = (g_Hmi_bs4002_DynoSpid_RefDDeltaSt / (1000/Dyno_PID_Excution_Interval));!Constant
	Dyno_S_PID_ValList(0)(index_PidList_YIndex) = g_Hmi_bs5001_FB_SpeedYIndexSt;!Constant
	Dyno_S_PID_ValList(0)(index_PidList_Y) = 0;!Variable
	Dyno_S_PID_ValList(0)(index_PidList_Ymin) = g_Hmi_bs4002_DynoSpid_YMinSt;!Constant
	Dyno_S_PID_ValList(0)(index_PidList_Ymax) = 0;!Constant
	Dyno_S_PID_ValList(0)(index_PidList_YCNTmax) = 0;!Constant
	Dyno_S_PID_ValList(0)(index_PidList_TargetIndex) = 0;!Constant
	Dyno_S_PID_ValList(0)(index_PidList_Target) = 0;!Variable
	Dyno_S_PID_ValList(0)(index_PidList_Ref) = 0;!Variable
	Dyno_S_PID_ValList(0)(index_PidList_P_Error) = 0;!Variable
	Dyno_S_PID_ValList(0)(index_PidList_P_PrevError) = 0;!Variable
	Dyno_S_PID_ValList(0)(index_PidList_I_Error) = 0;	!Variable
	Dyno_S_PID_ValList(0)(index_PidList_D_Error) = 0;!Variable
	Dyno_S_PID_ValList(0)(index_PidList_P_Portion) = 0;!Variable
	Dyno_S_PID_ValList(0)(index_PidList_I_Portion) = 0;!Variable
	Dyno_S_PID_ValList(0)(index_PidList_D_Portion) = 0;!Variable
	Dyno_S_PID_ValList(0)(index_PidList_PidIntervals) = 0;!Variable
	Dyno_S_PID_ValList(0)(index_PidList_CADelta) = (g_Hmi_bs4004_DynoSpid_C_ADeltaSt / (1000/Dyno_PID_Excution_Interval));!Constant
	Dyno_S_PID_ValList(0)(index_PidList_CDDelta) = (g_Hmi_bs4004_DynoSpid_C_DDeltaSt / (1000/Dyno_PID_Excution_Interval));!Constant
	Dyno_S_PID_ValList(0)(index_PidList_C_Ref) = 0;!Variable
	Dyno_S_PID_ValList(0)(index_PidList_C_PrevOutput) = 0;!Variable
	
	! *** Torque PID Controller Constant & Variable Initialization	
	Dyno_T_PID_ValList(0)(index_PidList_Kp) = g_Hmi_bs4101_DynoTpid_KpSt;!Constant
	Dyno_T_PID_ValList(0)(index_PidList_Ki) = g_Hmi_bs4101_DynoTpid_KiSt;!Constant
	Dyno_T_PID_ValList(0)(index_PidList_Kd) = g_Hmi_bs4101_DynoTpid_Kdst;!Constant
	Dyno_T_PID_ValList(0)(index_PidList_CIndex) = g_Hmi_bs5501_Dyno_CIndexSt;!Variable
	Dyno_T_PID_ValList(0)(index_PidList_C) = 0;!Variable
	Dyno_T_PID_ValList(0)(index_PidList_Cmax) = g_Hmi_bs4103_DynoTpid_CMaxSt;!Constant
	Dyno_T_PID_ValList(0)(index_PidList_CNTmax) = g_Hmi_bs4103_DynoTpid_CcntMaxSt;!Constant
	Dyno_T_PID_ValList(0)(index_PidList_RefADelta) = (g_Hmi_bs4102_DynoTpid_RefADeltaSt / (1000/Dyno_PID_Excution_Interval));!Constant
	Dyno_T_PID_ValList(0)(index_PidList_RefDDelta) = (g_Hmi_bs4102_DynoTpid_RefDDeltaSt / (1000/Dyno_PID_Excution_Interval));!Constant
	Dyno_T_PID_ValList(0)(index_PidList_YIndex) = g_Hmi_bs5101_FB_TorqueYIndexSt;!Constant
	Dyno_T_PID_ValList(0)(index_PidList_Y) = 0;!Variable
	Dyno_T_PID_ValList(0)(index_PidList_Ymin) = g_Hmi_bs4102_DynoTpid_YMinSt;!Constant
	Dyno_T_PID_ValList(0)(index_PidList_Ymax) = 0;!Constant
	Dyno_T_PID_ValList(0)(index_PidList_YCNTmax) = 0;!Constant
	Dyno_T_PID_ValList(0)(index_PidList_TargetIndex) = 0;!Constant
	Dyno_T_PID_ValList(0)(index_PidList_Target) = 0;!Variable
	Dyno_T_PID_ValList(0)(index_PidList_Ref) = 0;!Variable
	Dyno_T_PID_ValList(0)(index_PidList_P_Error) = 0;!Variable
	Dyno_T_PID_ValList(0)(index_PidList_P_PrevError) = 0;!Variable
	Dyno_T_PID_ValList(0)(index_PidList_I_Error) = 0;	!Variable
	Dyno_T_PID_ValList(0)(index_PidList_D_Error) = 0;!Variable
	Dyno_T_PID_ValList(0)(index_PidList_P_Portion) = 0;!Variable
	Dyno_T_PID_ValList(0)(index_PidList_I_Portion) = 0;!Variable
	Dyno_T_PID_ValList(0)(index_PidList_D_Portion) = 0;!Variable
	Dyno_T_PID_ValList(0)(index_PidList_PidIntervals) = 0;!Variable
	Dyno_T_PID_ValList(0)(index_PidList_CADelta) = (g_Hmi_bs4104_DynoTpid_C_ADeltaSt / (1000/Dyno_PID_Excution_Interval));!Constant
	Dyno_T_PID_ValList(0)(index_PidList_CDDelta) = (g_Hmi_bs4104_DynoTpid_C_DDeltaSt / (1000/Dyno_PID_Excution_Interval));!Constant
	Dyno_T_PID_ValList(0)(index_PidList_C_Ref) = 0;!Variable
	Dyno_T_PID_ValList(0)(index_PidList_C_PrevOutput) = 0;!Variable
	!disp "sr_PidControler_VariableList_Initial";
	!disp Pid_Time_Measure_1, g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref),g_Dyno_PID_Process_ValList(0)(index_PidList_C_PrevOutput),g_Dyno_PID_Process_ValList(0)(index_PidList_C),g_Dyno_PID_Process_ValList(0)(index_PidList_Y),g_Dyno_PID_Process_ValList(0)(index_PidList_Target),g_Dyno_PID_Process_ValList(0)(index_PidList_I_Portion),g_Dyno_PID_Process_ValList(0)(index_PidList_P_Portion) ;
RET
! ----------------------------------------------------------------------------------------------
sr_g_Dyno_PID_Process_ValList_INIT_TORQUE:
	!disp "sr_g_Dyno_PID_Process_ValList_INIT_TORQUE Start";
	!disp Pid_Time_Measure_1, g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref),g_Dyno_PID_Process_ValList(0)(index_PidList_C_PrevOutput),g_Dyno_PID_Process_ValList(0)(index_PidList_C),g_Dyno_PID_Process_ValList(0)(index_PidList_Y),g_Dyno_PID_Process_ValList(0)(index_PidList_Target),g_Dyno_PID_Process_ValList(0)(index_PidList_I_Portion),g_Dyno_PID_Process_ValList(0)(index_PidList_P_Portion) ;

	! *** Torque PID Controller Constant & Variable Initialization	
	g_Dyno_PID_Process_ValList(0)(index_PidList_Kp) = Dyno_T_PID_ValList(0)(index_PidList_Kp);
	g_Dyno_PID_Process_ValList(0)(index_PidList_Ki) = Dyno_T_PID_ValList(0)(index_PidList_Ki);
	g_Dyno_PID_Process_ValList(0)(index_PidList_Kd) = Dyno_T_PID_ValList(0)(index_PidList_Kd);
	g_Dyno_PID_Process_ValList(0)(index_PidList_CIndex) = Dyno_T_PID_ValList(0)(index_PidList_CIndex);
	g_Dyno_PID_Process_ValList(0)(index_PidList_Cmax) = Dyno_T_PID_ValList(0)(index_PidList_Cmax);
	g_Dyno_PID_Process_ValList(0)(index_PidList_CNTmax) = Dyno_T_PID_ValList(0)(index_PidList_CNTmax);
	g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) = g_Target_Dyno_Torque;!Variable
	g_Dyno_PID_Process_ValList(0)(index_PidList_RefADelta) = Dyno_T_PID_ValList(0)(index_PidList_RefADelta);
	g_Dyno_PID_Process_ValList(0)(index_PidList_RefDDelta) = Dyno_T_PID_ValList(0)(index_PidList_RefDDelta);
	g_Dyno_PID_Process_ValList(0)(index_PidList_YIndex) = Dyno_T_PID_ValList(0)(index_PidList_YIndex);
	g_Dyno_PID_Process_ValList(0)(index_PidList_Ymin) = Dyno_T_PID_ValList(0)(index_PidList_Ymin);
	g_Dyno_PID_Process_ValList(0)(index_PidList_CADelta) = Dyno_T_PID_ValList(0)(index_PidList_CADelta);
	g_Dyno_PID_Process_ValList(0)(index_PidList_CDDelta) = Dyno_T_PID_ValList(0)(index_PidList_CDDelta);
	g_Dyno_PID_Process_ValList(0)(index_PidList_P_Error) = 0;!Variable
	g_Dyno_PID_Process_ValList(0)(index_PidList_P_PrevError) = 0;!Variable
	!g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref) = g_Hmi_bsHeader1_Fb_Torque;!Variable
	!g_Dyno_PID_Process_ValList(0)(index_PidList_C_PrevOutput) = g_Hmi_bsHeader1_Fb_Torque;!Variable

	G1 = g_Dyno_PID_Process_ValList(0)(index_PidList_Kp) + g_Dyno_PID_Process_ValList(0)(index_PidList_Ki);
	G2 = -g_Dyno_PID_Process_ValList(0)(index_PidList_Kp)
	
	!disp "sr_g_Dyno_PID_Process_ValList_INIT_TORQUE End";
	!disp Pid_Time_Measure_1, g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref),g_Dyno_PID_Process_ValList(0)(index_PidList_C_PrevOutput),g_Dyno_PID_Process_ValList(0)(index_PidList_C),g_Dyno_PID_Process_ValList(0)(index_PidList_Y),g_Dyno_PID_Process_ValList(0)(index_PidList_Target),g_Dyno_PID_Process_ValList(0)(index_PidList_I_Portion),g_Dyno_PID_Process_ValList(0)(index_PidList_P_Portion) ;

RET
! ----------------------------------------------------------------------------------------------


sr_g_Dyno_PID_Process_ValList_INIT_SPEED:
	g_Dyno_PID_Process_ValList(0)(index_PidList_Kp) = Dyno_S_PID_ValList(0)(index_PidList_Kp);
	g_Dyno_PID_Process_ValList(0)(index_PidList_Ki) = Dyno_S_PID_ValList(0)(index_PidList_Ki);
	g_Dyno_PID_Process_ValList(0)(index_PidList_Kd) = Dyno_S_PID_ValList(0)(index_PidList_Kd);
	g_Dyno_PID_Process_ValList(0)(index_PidList_CIndex) = Dyno_S_PID_ValList(0)(index_PidList_CIndex);
	g_Dyno_PID_Process_ValList(0)(index_PidList_Cmax) = Dyno_S_PID_ValList(0)(index_PidList_Cmax);
	g_Dyno_PID_Process_ValList(0)(index_PidList_CNTmax) = Dyno_S_PID_ValList(0)(index_PidList_CNTmax);
	g_Dyno_PID_Process_ValList(0)(index_PidList_Ref) = g_Target_Dyno_Speed;!Variable
	g_Dyno_PID_Process_ValList(0)(index_PidList_RefADelta) = Dyno_S_PID_ValList(0)(index_PidList_RefADelta);
	g_Dyno_PID_Process_ValList(0)(index_PidList_RefDDelta) = Dyno_S_PID_ValList(0)(index_PidList_RefDDelta);
	g_Dyno_PID_Process_ValList(0)(index_PidList_YIndex) = Dyno_S_PID_ValList(0)(index_PidList_YIndex);
	g_Dyno_PID_Process_ValList(0)(index_PidList_Ymin) = Dyno_S_PID_ValList(0)(index_PidList_Ymin);
	g_Dyno_PID_Process_ValList(0)(index_PidList_CADelta) = Dyno_S_PID_ValList(0)(index_PidList_CADelta);
	g_Dyno_PID_Process_ValList(0)(index_PidList_CDDelta) = Dyno_S_PID_ValList(0)(index_PidList_CDDelta);
	g_Dyno_PID_Process_ValList(0)(index_PidList_P_Error) = 0;!Variable
	g_Dyno_PID_Process_ValList(0)(index_PidList_P_PrevError) = 0;!Variable
	!g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref) = g_Hmi_bsHeader1_Fb_Torque;!Variable
	!g_Dyno_PID_Process_ValList(0)(index_PidList_C_PrevOutput) = g_Hmi_bsHeader1_Fb_Torque;!Variable

	G1 = g_Dyno_PID_Process_ValList(0)(index_PidList_Kp) + g_Dyno_PID_Process_ValList(0)(index_PidList_Ki);
	G2 = -g_Dyno_PID_Process_ValList(0)(index_PidList_Kp)

	!disp "sr_g_Dyno_PID_Process_ValList_INIT_SPEED";
	!disp Pid_Time_Measure_1, g_Dyno_PID_Process_ValList(0)(index_PidList_C_Ref),g_Dyno_PID_Process_ValList(0)(index_PidList_C_PrevOutput),g_Dyno_PID_Process_ValList(0)(index_PidList_C),g_Dyno_PID_Process_ValList(0)(index_PidList_Y),g_Dyno_PID_Process_ValList(0)(index_PidList_Target),g_Dyno_PID_Process_ValList(0)(index_PidList_I_Portion),g_Dyno_PID_Process_ValList(0)(index_PidList_P_Portion) ;
RET

! ----------------------------------------------------------------------------------------------

sr_PidControler_ExecutionContition_Check:

	INT rstConditionCheck;	
		rstConditionCheck = 0;
		
	IF(g_Error_Exists_Bit = 0)		
		!IF(g_SystemState_Dyno_On = 1)
			IF((g_ControlMode <> g_CtMo_enum_Default) & ((g_DriveManual_Mode <> g_DrMn_enum_Default)|g_SystemState_Eng_Starting=1|g_PID_Change_Ramp>0))	
				IF((g_Dyno_ControlMode = Dyno_enum_ControlMode_Idle) | (g_Dyno_ControlMode = Dyno_enum_ControlMode_Speed) | (g_Dyno_ControlMode = Dyno_enum_ControlMode_Torque))
					rstConditionCheck = 1;
				END!IF(g_Dyno_ControlMode <> g_Dyno_enum_ControlMode_Default)
			END ! IF((g_ControlMode <> g_CtMo_enum_Default) & (g_DriveManual_Mode <> g_DrMn_enum_Default))	
		!END ! IF((g_ControlMode <> g_CtMo_enum_Default) & (g_DriveManual_Mode <> g_DrMn_enum_Default))		
	END ! IF(g_Error_Exists_Bit = 0)
	
	IF ((g_stop_StopMode = g_stop_enum_HardStopping) | (g_stop_StopMode = g_stop_enum_HardStopping))
		IF(g_SystemState_Dyno_On = 1)
			rstConditionCheck = 1;
		END ! IF(g_SystemState_Dyno_On = 1)
	END !IF ((g_stop_enum_HardStopping = g_stop_enum_HardStopping) | (g_stop_enum_HardStopping = g_stop_enum_HardStopping))
	
	g_Dyno_Control_Operable = rstConditionCheck;
	!	DISP "[#%d buffer] rstConditionCheck [#%d].", BufferProgrmaNumber,rstConditionCheck

RET

! ----------------------------------------------------------------------------------------------

sr_PidControler_ControlModeSelect_PidParaAppend:
	
	g_Dyno_ControlMode = Dyno_enum_ControlMode_Default;
	
	IF(g_ControlMode <> g_CtMo_enum_Default)
		
		IF(g_DriveManual_Mode = g_DrMn_enum_Idle)
		
			g_Dyno_ControlMode = Dyno_enum_ControlMode_Idle;	
			
		ELSEIF((g_DriveManual_Mode = g_DrMn_enum_IdleContrl) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngRPM) | (g_SystemState_Eng_Starting=1))
		
			g_Dyno_ControlMode = Dyno_enum_ControlMode_Torque;			
			CALL sr_g_Dyno_PID_Process_ValList_INIT_TORQUE;			
			
		ELSEIF((g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTo) | (g_stop_enum_HardStopping = g_stop_enum_HardStopping))
		
			g_Dyno_ControlMode = Dyno_enum_ControlMode_Speed;		
			CALL sr_g_Dyno_PID_Process_ValList_INIT_SPEED;
		
		END ! IF(g_DriveManual_Mode = g_DrMn_enum_Idle)

	END !IF(g_ControlMode <> g_CtMo_enum_Default)
	DISP "[#%d buffer] g_Dyno_ControlMode [#%d].", BufferProgrmaNumber,g_Dyno_ControlMode
RET

#6
! *** Engine Control Program

INT BufferProgrmaNumber
	BufferProgrmaNumber = 6
		
! *** MMI Show Message
DISP "[#%d buffer] program started.", BufferProgrmaNumber;

! Alarm ID Define (Warning Alarm ID = (Buffer No * 10000) + Warning No, Error Alarm ID = Warning Alarm ID * -1)	
INT enum_aID_Buf6_W_PIDOperable_Fault;
	enum_aID_Buf6_W_PIDOperable_Fault = -60001;
INT enum_aID_Buf6_W_PID_cLimit_Fault;
	enum_aID_Buf6_W_PID_cLimit_Fault = -61001;
INT enum_aID_Buf6_W_PID_yLimit_Fault;
	enum_aID_Buf6_W_PID_yLimit_Fault = 61002;
		
	GLOBAL INT g_Buffer6_displayCmdLevel;
		g_Buffer6_displayCmdLevel = 0;
	
	! *** Engine Control Variable
	!GLOBAL INT g_Eng_Control_Operable;
		g_Eng_Control_Operable = 0;
	
	!GLOBAL INT g_Eng_ControlMode;	
	REAL Eng_PID_Excution_Interval;
		Eng_PID_Excution_Interval = 10;
	
	INT Eng_DrvMd_Previous_Mode;
		Eng_DrvMd_Previous_Mode = g_DrMn_enum_Default;
		
	INT Eng_enum_ControlMode_Default;
	INT Eng_enum_ControlMode_Idle;
	INT Eng_enum_ControlMode_Speed;
	INT Eng_enum_ControlMode_Torque;	
	INT Eng_enum_ControlMode_ThrAlpha;	
		
		Eng_enum_ControlMode_Default = 10;
		Eng_enum_ControlMode_Idle = 100
		Eng_enum_ControlMode_Speed = 200;
		Eng_enum_ControlMode_Torque = 300;
		Eng_enum_ControlMode_ThrAlpha = 400;
		
		g_Eng_ControlMode = Eng_enum_ControlMode_Default;
	
	! *** PID Control State Machine Variable
	!GLOBAL INT g_Eng_PidControl_StateMachine;	
	INT Eng_enum_PidStateMachine_Default;
	INT Eng_enum_PidStateMachine_Error;
	INT Eng_enum_PidStateMachine_Idle;
	INT Eng_enum_PidStateMachine_Init;
	INT Eng_enum_PidStateMachine_Steady;
	INT Eng_enum_PidStateMachine_Change;
	INT Eng_enum_PidStateMachine_Stop;
	
		Eng_enum_PidStateMachine_Default = 10;
		Eng_enum_PidStateMachine_Error = 100;
		Eng_enum_PidStateMachine_Idle = 200;
		Eng_enum_PidStateMachine_Init = 300
		Eng_enum_PidStateMachine_Steady = 400;
		Eng_enum_PidStateMachine_Change = 500;
		Eng_enum_PidStateMachine_Stop = 600;
	
	! *** Compansation Variable 
	REAL Torque_CompanRefeVar;
	
	! *** PID Controler Variable 	
	INT Eng_Pid_Control_Operable;	
	INT Eng_Pid_Control_CErrorCnt;
	INT Eng_Pid_Control_YErrorCnt;
	REAL Eng_Pid_Control_YPrevValue;
	REAL Eng_S_Pid_Control_KP_MAX;
	REAL Eng_S_Pid_Control_iPotion_Delta;
	REAL Eng_T_Pid_Control_iPotion_Delta;
	REAL Eng_Pid_Calc_Alpha_1_DaVal;
	REAL Eng_Pid_Calc_Alpha_2_DaVal;
	
	REAL Eng_S_PID_ValList(1)(29);
	REAL Eng_T_PID_ValList(1)(29);
	REAL Eng_A_PID_ValList(1)(29);
	!GLOBAL REAL g_Eng_PID_Process_ValList(1)(29);		
		
	! *** PID Controler Variable List Index
	INT index_PidList_Kp;
	INT index_PidList_Ki;
	INT index_PidList_Kd;
	INT index_PidList_CIndex;
	INT index_PidList_C;
	INT index_PidList_Cmax;
	INT index_PidList_CNTmax;
	INT index_PidList_RefADelta;
	INT index_PidList_RefDDelta;
	INT index_PidList_YIndex;
	INT index_PidList_Y;
	INT index_PidList_Ymin;
	INT index_PidList_Ymax;	
	INT index_PidList_YCNTmax;
	INT index_PidList_TargetIndex;	
	INT index_PidList_Target;	
	INT index_PidList_Ref;
	INT index_PidList_P_Error;
	INT index_PidList_P_PrevError;
	INT index_PidList_I_Error;
	INT index_PidList_D_Error;
	INT index_PidList_P_Portion;
	INT index_PidList_I_Portion;
	INT index_PidList_D_Portion;
	INT index_PidList_PidIntervals;
	INT index_PidList_CADelta;
	INT index_PidList_CDDelta;
	INT index_PidList_C_Ref;
	INT index_PidList_C_PrevOutput;
	
		CALL sr_PidControler_VariableList_Initial;
	
	! *** PID Data Logger Variable
	!GLOBAL REAL g_Eng_PID_DataLogger_Matrix(10000)(29)	
	INT Pid_DataLogger_Matrix_MaxRowIndex
	INT Pid_DataLogger_Matrix_MaxColumnIndex
	INT Pid_DataLogger_Matrix_RowCopy_Index	
		
		FILL(0, g_Eng_PID_DataLogger_Matrix)
		Pid_DataLogger_Matrix_MaxRowIndex = 10000 - 1
		Pid_DataLogger_Matrix_MaxColumnIndex = 29 - 1
		Pid_DataLogger_Matrix_RowCopy_Index = 0;		
	
	! *** PID Time Measure
	REAL Pid_Time_Measure_1;
	REAL Pid_Time_Measure_2;
	REAL Pid_Time_Difference;
		
		Pid_Time_Measure_1 = 0;
		Pid_Time_Measure_2 = 0;
		Pid_Time_Difference = 0;
	
		
	CALL sr_PidControler_ControlModeSelect_PidParaAppend;	
	
	CALL sr_PidControler_ExecutionContition_Check;
	
	! ***
	! *** PID Controller 
	! ***	
	g_Eng_PidControl_StateMachine = Eng_enum_PidStateMachine_Idle;	
	
	WHILE ((g_Eng_Control_Operable = 1) & (PST(3).#RUN = 1)& (PST(4).#RUN = 1))
	
				
		BLOCK !Dynamometer PID
		
			! PID Control excution condition check
			g_Eng_Control_Operable = 0;
			
			IF(g_Error_Exists_Bit = 0)				
				!IF(g_SystemState_Eng_Started = 1)
					IF(((g_ControlMode <> g_CtMo_enum_Default) & (g_DriveManual_Mode <> g_DrMn_enum_Default)) | g_SystemState_Eng_Starting = 1|g_PID_Change_Ramp>0)	
						IF((g_Eng_ControlMode = Eng_enum_ControlMode_Idle) | (g_Eng_ControlMode = Eng_enum_ControlMode_Speed) | (g_Eng_ControlMode = Eng_enum_ControlMode_Torque) | (g_Eng_ControlMode = Eng_enum_ControlMode_ThrAlpha) | (g_SystemState_Eng_Starting = 1))
							g_Eng_Control_Operable = 1;
						END!IF(g_Dyno_ControlMode <> g_Dyno_enum_ControlMode_Default)
					END ! IF((g_ControlMode <> g_CtMo_enum_Default) & (g_DriveManual_Mode <> g_DrMn_enum_Default))	
				!END !IF(g_SystemState_Eng_Started = 1)	
			END ! IF(g_Error_Exists_Bit = 0)		
				
				
			IF((g_stop_StopMode = g_stop_enum_IdleStopping)| (g_stop_StopMode = g_stop_enum_SoftStopping) | (g_stop_StopMode = g_stop_enum_HardStopping))
				
				g_Eng_Control_Operable = 0;
				
				! *** MMI Show Message
				IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] [req] ATOM Sw Idle Stop; PID Operable Off.", BufferProgrmaNumber; END
				
			END ! (g_stop_StopMode = g_stop_enum_IdleStopping)
			
				
			IF(g_Eng_Control_Operable <> 1)
				! *** Error register ***
				!g_Alarm_Register_Request_AlarmID = enum_aID_Buf6_W_PIDOperable_Fault;		
				!g_Alarm_Register_Request_Bit = 1;
				
			END !IF(g_Dyno_Control_Operable <> 1)
				Pid_Time_Measure_1 = TIME
				
				! ----------------------------------------------------------------------------------------------
					
				IF((Pid_Time_Measure_1 - Pid_Time_Measure_2) >= Eng_PID_Excution_Interval)					
			
					! *** measure the pid execution time.
					Pid_Time_Difference = Pid_Time_Measure_1 - Pid_Time_Measure_2
					Pid_Time_Measure_2 = Pid_Time_Measure_1				
					g_Eng_PID_Process_ValList(0)(index_PidList_PidIntervals) = Pid_Time_Difference;
					
					! ----------------------------------------------------------------------------------------------					
					
					! *** Drive Mode Change Process
					IF((g_DriveManual_Mode <> g_DrMn_enum_Default) & (g_DriveManual_Mode <> Eng_DrvMd_Previous_Mode ))
						
						! *** MMI Show Message
						IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] [req] Drive Mode Change Process. ChgReqMode= <%d>.", BufferProgrmaNumber, g_DriveManual_Mode; END
						
							IF((g_DriveManual_Mode = g_DrMn_enum_Idle) | (g_DriveManual_Mode = g_DrMn_enum_IdleContrl) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTA) | (g_SystemState_Eng_Starting = 1))
								IF (g_Eng_ControlMode <> Eng_enum_ControlMode_ThrAlpha)
									g_Eng_ControlMode = Eng_enum_ControlMode_ThrAlpha ;			
									COPY(Eng_A_PID_ValList, g_Eng_PID_Process_ValList);										
									
									! *** MMI Show Message
									IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Drive Mode Change Process. Eng Control Mode = ThrAlpha.", BufferProgrmaNumber; END
								END
							ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyTo_EngRPM)
								IF (g_Eng_ControlMode <> Eng_enum_ControlMode_Speed)
									g_Eng_ControlMode = Eng_enum_ControlMode_Speed;		
									COPY(Eng_S_PID_ValList, g_Eng_PID_Process_ValList);
									
									g_Eng_PidControl_StateMachine = Eng_enum_PidStateMachine_Idle;	
									
									! *** MMI Show Message
									IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Drive Mode Change Process. Eng Control Mode = Speed.", BufferProgrmaNumber; END
								END
							ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTo)
								IF (g_Eng_ControlMode <> Eng_enum_ControlMode_Torque)
									g_Eng_ControlMode = Eng_enum_ControlMode_Torque;			
									COPY(Eng_T_PID_ValList, g_Eng_PID_Process_ValList);			
									
									g_Eng_PidControl_StateMachine = Eng_enum_PidStateMachine_Idle;	
							
									! *** MMI Show Message
									IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] [cmp] Drive Mode Change Process. Eng Control Mode = Torque.", BufferProgrmaNumber; END
								END
							ELSE
								! *** MMI Show Message
								IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] [fail] Drive Mode Change Process.", BufferProgrmaNumber; END
								g_Eng_ControlMode = Eng_enum_ControlMode_Default;

							END ! IF((g_DriveManual_Mode = g_DrMn_enum_Idle) | (g_DriveManual_Mode = g_DrMn_enum_IdleContrl) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTA))
									
					END ! IF((g_DriveManual_Mode <> g_DrMn_enum_Idle) & (g_DriveManual_Mode <> DrvMd_Previous_Mode ))				
					 
					Eng_DrvMd_Previous_Mode = g_DriveManual_Mode;
										
					! ----------------------------------------------------------------------------------------------					
										
					! *** ref & y update
					IF(g_DriveManual_Mode = g_DrMn_enum_Idle) 
						! *** ref
						g_Eng_PID_Process_ValList(0)(index_PidList_Target) = 0;
						
						g_Eng_PidControl_StateMachine = Eng_enum_PidStateMachine_Idle
						
					ELSE						
						
						IF( g_Eng_ControlMode = Eng_enum_ControlMode_Idle)
							
							g_Eng_PID_Process_ValList(0)(index_PidList_Target) = 0;
							g_Eng_PID_Process_ValList(0)(index_PidList_Y) = 0;
							
						ELSEIF( g_Eng_ControlMode = Eng_enum_ControlMode_Speed) 
							! *** ref &  y
							g_Eng_PID_Process_ValList(0)(index_PidList_Target) = g_Target_Engine_Speed;
							g_Eng_PID_Process_ValList(0)(index_PidList_Y) = g_Feedback_Speed;
							
							! *** Y limit check
							!IF (g_Eng_PID_Process_ValList(0)(index_PidList_Y) >= g_Eng_PID_Process_ValList(0)(index_PidList_Ymax))
							!	Eng_Pid_Control_YErrorCnt = Eng_Pid_Control_YErrorCnt + 1;
							!	g_Eng_PID_Process_ValList(0)(index_PidList_Y) = Eng_Pid_Control_YPrevValue;
							!END ! IF (g_Eng_PID_Process_ValList(0)(index_PidList_Y) >= g_Eng_PID_Process_ValList(0)(index_PidList_Ymax))
							
							! *** Y limit error check
							!IF(Eng_Pid_Control_YErrorCnt >= g_Eng_PID_Process_ValList(0)(index_PidList_YCNTmax))
							!	Eng_Pid_Control_YErrorCnt = 0;
							!	g_Eng_PidControl_StateMachine = g_Eng_enum_PidStateMachine_Error;
								
								! *** Error register ***
							!	g_Alarm_Register_Request_AlarmID = enum_aID_Buf6_W_PID_yLimit_Fault ;		
							!	g_Alarm_Register_Request_Bit = 1;
							!END ! IF(Eng_Pid_Control_YErrorCnt >= g_Eng_PID_Process_ValList(0)(index_PidList_YCNTmax))
							
						ELSEIF( g_Eng_ControlMode = Eng_enum_ControlMode_Torque)
						
							! *** ref &  y
							g_Eng_PID_Process_ValList(0)(index_PidList_Target) = g_Target_Engine_Torque
							g_Eng_PID_Process_ValList(0)(index_PidList_Y) = g_Feedback_Torque
						
						ELSEIF(g_Eng_ControlMode =  Eng_enum_ControlMode_ThrAlpha)
						
							! *** ref &  y
							g_Eng_PID_Process_ValList(0)(index_PidList_Target) = g_Target_Engine_AlphaPosi;		
							g_Eng_PID_Process_ValList(0)(index_PidList_Y) = g_Feedback_AlphaPosi;
							! *** PID Disable						
							!Eng_Pid_Control_Operable = 0;
							
						ELSE
							! *** ref &  y
							g_Eng_PID_Process_ValList(0)(index_PidList_Target) = 0;
							g_Eng_PID_Process_ValList(0)(index_PidList_Y) = 0;
							! *** PID Disable						
							Eng_Pid_Control_Operable = 0;
							
						END
					END
					
					! ----------------------------------------------------------------------------------------------
					
					IF(g_Eng_ControlMode <>  Eng_enum_ControlMode_ThrAlpha)
					
						! *** pid Controller state machine
						IF(g_Eng_PidControl_StateMachine = Eng_enum_PidStateMachine_Error)					
							! *** MMI Show Message
							IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State = Error.", BufferProgrmaNumber; END
							
							g_Eng_PID_Process_ValList(0)(index_PidList_Y) = 0;
							Eng_Pid_Control_Operable = 0;						
							
						ELSEIF(g_Eng_PidControl_StateMachine = Eng_enum_PidStateMachine_Idle)
							! *** MMI Show Message
							IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State = Idle.", BufferProgrmaNumber; END
							
							g_Eng_PID_Process_ValList(0)(index_PidList_Y) = 0;
							Eng_Pid_Control_Operable = 0;
							
							IF(g_Eng_PID_Process_ValList(0)(index_PidList_Target) > (g_Eng_PID_Process_ValList(0)(index_PidList_Ymin) + 2 ))
								! *** MMI Show Message
								IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State Change. Idle >>> Init.", BufferProgrmaNumber; END
								
								g_Eng_PidControl_StateMachine = Eng_enum_PidStateMachine_Init;
							END
													
						ELSEIF(g_Eng_PidControl_StateMachine = Eng_enum_PidStateMachine_Init)					
							! *** MMI Show Message
							IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State = Init.", BufferProgrmaNumber; END
							
							Eng_Pid_Control_Operable = 0;						
							
							Eng_Pid_Control_CErrorCnt = 0;
							Eng_Pid_Control_YErrorCnt = 0;
							Eng_Pid_Control_YPrevValue = 0;
							Eng_S_Pid_Control_KP_MAX = 0;
							Eng_S_Pid_Control_iPotion_Delta = 0;
							Eng_T_Pid_Control_iPotion_Delta = 0;
		
							! *** Idle PID Controller Variable Initialization		
							IF( g_Eng_ControlMode = Eng_enum_ControlMode_Idle) 
								g_Eng_PID_Process_ValList(0)(index_PidList_C) = 0;	
								g_Eng_PID_Process_ValList(0)(index_PidList_Y) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_Target) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_Ref) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_P_Error) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_P_PrevError) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_I_Error) = 0;	
								g_Eng_PID_Process_ValList(0)(index_PidList_D_Error) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_P_Portion) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_D_Portion) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_PidIntervals) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref) = 0;			
								g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput) = 0;	
								
							! *** Speed PID Controller Variable Initialization		
							ELSEIF( g_Eng_ControlMode = Eng_enum_ControlMode_Speed) 
								g_Eng_PID_Process_ValList(0)(index_PidList_C) = 0;	
								g_Eng_PID_Process_ValList(0)(index_PidList_Y) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_Target) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_Ref) = g_Feedback_Speed;
								g_Eng_PID_Process_ValList(0)(index_PidList_P_Error) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_P_PrevError) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_I_Error) = 0;	
								g_Eng_PID_Process_ValList(0)(index_PidList_D_Error) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_P_Portion) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_D_Portion) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_PidIntervals) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref) = 0;			
								g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput) = 0;	
							
							! *** Torque PID Control Variable List Setting							
							ELSEIF( g_Eng_ControlMode = Eng_enum_ControlMode_Torque)
								g_Eng_PID_Process_ValList(0)(index_PidList_C) = 0;						
								g_Eng_PID_Process_ValList(0)(index_PidList_Y) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_Target) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_Ref) = g_Feedback_Torque;
								g_Eng_PID_Process_ValList(0)(index_PidList_P_Error) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_P_PrevError) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_I_Error) = 0;	
								g_Eng_PID_Process_ValList(0)(index_PidList_D_Error) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_P_Portion) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_D_Portion) = 0;
								g_Eng_PID_Process_ValList(0)(index_PidList_PidIntervals) = 0;	
								g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref) = 0;			
								g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput) = 0;									
						
							END
							
							! *** MMI Show Message
							IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State Change. Init >>> Change.", BufferProgrmaNumber; END
							
							g_Eng_PidControl_StateMachine = Eng_enum_PidStateMachine_Change;
																								
						ELSEIF(g_Eng_PidControl_StateMachine = Eng_enum_PidStateMachine_Steady)
							! *** MMI Show Message
							IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State = Steady.", BufferProgrmaNumber; END
							
							IF(g_Eng_PID_Process_ValList(0)(index_PidList_Target) < (g_Eng_PID_Process_ValList(0)(index_PidList_Ymin) + 2))
								! *** MMI Show Message
								IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State Change. Steady >>> Stop.", BufferProgrmaNumber; END
						
								g_Eng_PidControl_StateMachine = Eng_enum_PidStateMachine_Stop;
							ELSE
							
								IF(ABS(g_Eng_PID_Process_ValList(0)(index_PidList_Ref) - g_Eng_PID_Process_ValList(0)(index_PidList_Target)) > 2)
									! *** MMI Show Message
									IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State Change. Steady >>> Change.", BufferProgrmaNumber; END
								
									g_Eng_PidControl_StateMachine = Eng_enum_PidStateMachine_Change;
								ELSE
									g_Eng_PID_Process_ValList(0)(index_PidList_Ref) = g_Eng_PID_Process_ValList(0)(index_PidList_Target)
								END
								
								Eng_Pid_Control_Operable = 1;
																
							END ! IF(g_Eng_PID_Process_ValList(0)(index_PidList_Target) < (g_Eng_PID_Process_ValList(0)(index_PidList_Ymin) + 2))
							
						ELSEIF(g_Eng_PidControl_StateMachine = Eng_enum_PidStateMachine_Change)
							
							! *** MMI Show Message
							IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State = Change. Target = <%d>, Ref = <%d> ", BufferProgrmaNumber, g_Eng_PID_Process_ValList(0)(index_PidList_Target), g_Eng_PID_Process_ValList(0)(index_PidList_Ref); END
							
							IF((g_Eng_PID_Process_ValList(0)(index_PidList_Target) - g_Eng_PID_Process_ValList(0)(index_PidList_Ref)) > 2)
								
								g_Eng_PID_Process_ValList(0)(index_PidList_Ref) = (g_Eng_PID_Process_ValList(0)(index_PidList_Ref) + (g_Eng_PID_Process_ValList(0)(index_PidList_RefADelta)));
								
								IF(g_Eng_PID_Process_ValList(0)(index_PidList_Ref) > g_Eng_PID_Process_ValList(0)(index_PidList_Target))
									g_Eng_PID_Process_ValList(0)(index_PidList_Ref) = g_Eng_PID_Process_ValList(0)(index_PidList_Target);
								END ! IF(g_Eng_PID_Process_ValList(0)(index_PidList_Ref) > g_Eng_PID_Process_ValList(0)(index_PidList_Target))
								
								! *** MMI Show Message
								IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State  = Change. Acc Target Ref = <%d>", BufferProgrmaNumber, g_Eng_PID_Process_ValList(0)(index_PidList_Ref); END
							
							ELSEIF((g_Eng_PID_Process_ValList(0)(index_PidList_Target) - g_Eng_PID_Process_ValList(0)(index_PidList_Ref)) < -2)
																						
								g_Eng_PID_Process_ValList(0)(index_PidList_Ref) = ((g_Eng_PID_Process_ValList(0)(index_PidList_Ref)) - g_Eng_PID_Process_ValList(0)(index_PidList_RefDDelta));
								
								IF(g_Eng_PID_Process_ValList(0)(index_PidList_Ref) < g_Eng_PID_Process_ValList(0)(index_PidList_Target))
									g_Eng_PID_Process_ValList(0)(index_PidList_Ref) = g_Eng_PID_Process_ValList(0)(index_PidList_Target);
								END ! IF(g_Eng_PID_Process_ValList(0)(index_PidList_Ref) < g_Eng_PID_Process_ValList(0)(index_PidList_Target))							
								
								! *** MMI Show Message
								IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State  = Change. Dec Target Ref = <%d>", BufferProgrmaNumber, g_Eng_PID_Process_ValList(0)(index_PidList_Ref); END
							
							ELSE 
							
								IF(g_Eng_PID_Process_ValList(0)(index_PidList_Target) < (g_Eng_PID_Process_ValList(0)(index_PidList_Ymin) + 2))
									! *** MMI Show Message
									IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State Change. Change >>> Stop.", BufferProgrmaNumber; END
							
									g_Eng_PidControl_StateMachine = Eng_enum_PidStateMachine_Stop;
								ELSE
									! *** MMI Show Message
									IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State Change. Change >>> Steady.", BufferProgrmaNumber; END
																		
									g_Eng_PidControl_StateMachine = Eng_enum_PidStateMachine_Steady;
									g_Eng_PID_Process_ValList(0)(index_PidList_Ref) = g_Eng_PID_Process_ValList(0)(index_PidList_Target);

								END ! IF(g_Eng_PID_Process_ValList(0)(index_PidList_Target) = 0)	
								
							END ! IF((g_Eng_PID_Process_ValList()(index_PidList_gTarget) - g_Eng_PID_Process_ValList()(index_PidList_gRef)) = 0)
							
							Eng_Pid_Control_Operable = 1;							
													
						ELSEIF(g_Eng_PidControl_StateMachine = Eng_enum_PidStateMachine_Stop)						
							! *** MMI Show Message
							IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State  = Stop.", BufferProgrmaNumber; END
							
							IF(g_Eng_PID_Process_ValList(0)(index_PidList_Y) <= g_Eng_PID_Process_ValList(0)(index_PidList_Ymin))
								! *** MMI Show Message
								IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State Change. Stop >>> Idle", BufferProgrmaNumber; END
							
								g_Eng_PidControl_StateMachine = Eng_enum_PidStateMachine_Idle;
								g_Eng_PID_Process_ValList(0)(index_PidList_C) = 0;
							ELSE
								g_Eng_PID_Process_ValList(0)(index_PidList_C) = 0;
							END !IF(g_Eng_PID_Process_ValList(0)(index_PidList_Y) <= g_Eng_PID_Process_ValList(0)(index_PidList_Ymin))
							
							Eng_Pid_Control_Operable = 0;				
							
						ELSE
							! *** MMI Show Message
							IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Controller State = Default.", BufferProgrmaNumber; END
							
							g_Eng_PidControl_StateMachine = Eng_enum_PidStateMachine_Default;
							
							g_Eng_PID_Process_ValList(0)(index_PidList_Y) = 0;
							Eng_Pid_Control_Operable = 0;
						END !IF(g_Eng_PidControl_StateMachine = g_Eng_enum_PidStateMachine_Error)			
						
					
						! ----------------------------------------------------------------------------------------------
						
						! *** PID Controller
						IF(Eng_Pid_Control_Operable = 1)
						
							! *** Error calculation
							g_Eng_PID_Process_ValList(0)(index_PidList_P_Error) = g_Eng_PID_Process_ValList(0)(index_PidList_Ref) - g_Eng_PID_Process_ValList(0)(index_PidList_Y);
							g_Eng_PID_Process_ValList(0)(index_PidList_D_Error) = g_Eng_PID_Process_ValList(0)(index_PidList_P_PrevError) - g_Eng_PID_Process_ValList(0)(index_PidList_P_Error);
								
								IF( g_Eng_ControlMode = Eng_enum_ControlMode_Speed) 
									! *** Engine Progressive Adaptive Controller - Speed
									
									! *** P calculation(Progression limiter)
									g_Eng_PID_Process_ValList(0)(index_PidList_P_Portion) = g_Eng_PID_Process_ValList(0)(index_PidList_P_Error) * g_Eng_PID_Process_ValList(0)(index_PidList_Kp);
									
									Eng_S_Pid_Control_KP_MAX = g_Eng_PID_Process_ValList(0)(index_PidList_Cmax) * 0.05
									IF(g_Eng_PID_Process_ValList(0)(index_PidList_P_Portion) < Eng_S_Pid_Control_KP_MAX)
										g_Eng_PID_Process_ValList(0)(index_PidList_P_Portion) = 0;
										g_Eng_PID_Process_ValList(0)(index_PidList_P_Portion) = (g_Eng_PID_Process_ValList(0)(index_PidList_P_Error) * g_Eng_PID_Process_ValList(0)(index_PidList_P_Error));
										g_Eng_PID_Process_ValList(0)(index_PidList_P_Portion) = g_Eng_PID_Process_ValList(0)(index_PidList_P_Portion) + (g_Eng_PID_Process_ValList(0)(index_PidList_P_Error) * g_Hmi_bs4202_EngSpid_KpA_St);
										g_Eng_PID_Process_ValList(0)(index_PidList_P_Portion) = g_Eng_PID_Process_ValList(0)(index_PidList_P_Portion) + g_Hmi_bs4202_EngSpid_KpB_St;
										
									END ! IF(g_Eng_PID_Process_ValList(0)(index_PidList_P_Portion) < Eng_Pid_Control_KP_MAX)
																		
									! *** I calculation(speed scheduled I gradient limiter(Higher speed, Lower delta, prevent overshooting))
									IF(g_Eng_PID_Process_ValList(0)(index_PidList_Y) < g_Hmi_bs4203_EngSpid_IgSpeed_1St)
										Eng_S_Pid_Control_iPotion_Delta = g_Hmi_bs4204_EngSpid_Ig_iDelta1St;
									ELSEIF((g_Eng_PID_Process_ValList(0)(index_PidList_Y) > g_Hmi_bs4203_EngSpid_IgSpeed_1St) & (g_Eng_PID_Process_ValList(0)(index_PidList_Y) < g_Hmi_bs4203_EngSpid_IgSpeed_2St))
										Eng_S_Pid_Control_iPotion_Delta = g_Hmi_bs4204_EngSpid_Ig_iDelta2St;
									ELSEIF((g_Eng_PID_Process_ValList(0)(index_PidList_Y) > g_Hmi_bs4203_EngSpid_IgSpeed_2St) & (g_Eng_PID_Process_ValList(0)(index_PidList_Y) < g_Hmi_bs4203_EngSpid_IgSpeed_3St))
										Eng_S_Pid_Control_iPotion_Delta = g_Hmi_bs4204_EngSpid_Ig_iDelta3St;
									ELSEIF(g_Eng_PID_Process_ValList(0)(index_PidList_Y) > g_Hmi_bs4203_EngSpid_IgSpeed_3St)
										Eng_S_Pid_Control_iPotion_Delta = g_Hmi_bs4205_EngSpid_Ig_iDelta4St;
									END !IF(g_Eng_PID_Process_ValList(0)(index_PidList_Y) < g_Hmi_bs4202_EngSpid_IgSpeed_1St)
									
									g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion) = g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput) + (g_Eng_PID_Process_ValList(0)(index_PidList_P_Error) * g_Eng_PID_Process_ValList(0)(index_PidList_Ki));
									
									IF(ABS(g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion) - g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput)) > Eng_S_Pid_Control_iPotion_Delta)
										IF(g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion) > g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput))
											g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion) = g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput) + Eng_S_Pid_Control_iPotion_Delta;
										ELSEIF(g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion) < g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput))
											g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion) = g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput) - Eng_S_Pid_Control_iPotion_Delta;
										END	!IF(g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion) > g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput))
									END !IF(ABS(g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion) - g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput)) > Eng_S_Pid_Control_iPotion_Delta)
									
									! *** P/I Portion Sum
									g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref) = g_Eng_PID_Process_ValList(0)(index_PidList_P_Portion) + g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion);
									
								ELSEIF( g_Eng_ControlMode = Eng_enum_ControlMode_Torque)
								
									! *** Engine Progressive Adaptive Controller - Torque
									
									! *** P calculation(Progression limiter)
									g_Eng_PID_Process_ValList(0)(index_PidList_P_Portion) = g_Eng_PID_Process_ValList(0)(index_PidList_P_Error) * g_Eng_PID_Process_ValList(0)(index_PidList_Kp);
																			
									! *** I calculation(speed scheduled I gradient limiter(Higher speed, Lower delta, prevent overshooting))
									IF(g_Eng_PID_Process_ValList(0)(index_PidList_Y) < g_Hmi_bs4302_EngTpid_IgSpeed_1St)
										Eng_T_Pid_Control_iPotion_Delta = g_Hmi_bs4303_EngTpid_Ig_iDelta1St;
									ELSEIF((g_Eng_PID_Process_ValList(0)(index_PidList_Y) > g_Hmi_bs4302_EngTpid_IgSpeed_1St) & (g_Eng_PID_Process_ValList(0)(index_PidList_Y) < g_Hmi_bs4302_EngTpid_IgSpeed_2St))
										Eng_T_Pid_Control_iPotion_Delta = g_Hmi_bs4303_EngTpid_Ig_iDelta2St;
									ELSEIF((g_Eng_PID_Process_ValList(0)(index_PidList_Y) > g_Hmi_bs4302_EngTpid_IgSpeed_2St) & (g_Eng_PID_Process_ValList(0)(index_PidList_Y) < g_Hmi_bs4302_EngTpid_IgSpeed_3St))
										Eng_T_Pid_Control_iPotion_Delta = g_Hmi_bs4303_EngTpid_Ig_iDelta3St;
									ELSEIF(g_Eng_PID_Process_ValList(0)(index_PidList_Y) > g_Hmi_bs4302_EngTpid_IgSpeed_3St)
										Eng_T_Pid_Control_iPotion_Delta = g_Hmi_bs4304_EngTpid_Ig_iDelta4St;
									END !IF(g_Eng_PID_Process_ValList(0)(index_PidList_Y) < g_Hmi_bs4202_EngSpid_IgSpeed_1St)
									
									g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion) = g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput) + (g_Eng_PID_Process_ValList(0)(index_PidList_P_Error) * g_Eng_PID_Process_ValList(0)(index_PidList_Ki));
									
									IF(ABS(g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion) - g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput)) > Eng_T_Pid_Control_iPotion_Delta)
										IF(g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion) > g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput))
											g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion) = g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput) + Eng_T_Pid_Control_iPotion_Delta;
										ELSEIF(g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion) < g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput))
											g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion) = g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput) - Eng_T_Pid_Control_iPotion_Delta;
										END	!IF(g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion) > g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput))
									END !IF(ABS(g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion) - g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput)) > Eng_S_Pid_Control_iPotion_Delta)
									
									! *** P/I Portion Sum
									g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref) = g_Eng_PID_Process_ValList(0)(index_PidList_P_Portion) + g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion);
										
								END ! IF( g_Eng_ControlMode = Eng_enum_ControlMode_Speed) 
										
									
								g_Eng_PID_Process_ValList(0)(index_PidList_P_PrevError) = g_Eng_PID_Process_ValList(0)(index_PidList_P_Error);								
								
						END ! IF(Eng_Pid_Control_Operable = 1)	
						
						! *** C limit check
						IF ((g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref) >= g_Eng_PID_Process_ValList(0)(index_PidList_Cmax)) & (g_Eng_PID_Process_ValList(0)(index_PidList_Y) = Eng_Pid_Control_YPrevValue))
							Eng_Pid_Control_CErrorCnt = Eng_Pid_Control_CErrorCnt + 1;
							
							IF((g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref)) > 0)
								g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref) = g_Eng_PID_Process_ValList(0)(index_PidList_Cmax)
							ELSE
								g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref) = -g_Eng_PID_Process_ValList(0)(index_PidList_Cmax)
							END ! IF((g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref)) > 0)
						ELSE
							Eng_Pid_Control_CErrorCnt = Eng_Pid_Control_CErrorCnt - 1;  
							IF(Eng_Pid_Control_CErrorCnt < 0)
								Eng_Pid_Control_CErrorCnt = 0;
							END
						END ! IF ((g_Eng_PID_Process_ValList(0)(index_PidList_C) >= g_Eng_PID_Process_ValList(0)(index_PidList_Cmax)) & (g_Eng_PID_Process_ValList(0)(index_PidList_Y) = Eng_Pid_Control_YPrevValue))
						
						! *** C limit error check
						IF(Eng_Pid_Control_CErrorCnt >= g_Eng_PID_Process_ValList(0)(index_PidList_CNTmax))
							! *** MMI Show Message
							IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] C Maximum error program Stop. CErrorCnt=<%d>, SetCNTmax=<%d>, C_Ref=<%d>", BufferProgrmaNumber, Eng_Pid_Control_CErrorCnt, g_Eng_PID_Process_ValList(0)(index_PidList_CNTmax),g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref); END
				
							Eng_Pid_Control_CErrorCnt = 0;
							g_Eng_PidControl_StateMachine = Eng_enum_PidStateMachine_Error;
							
							! *** Error register ***
							g_Alarm_Register_Request_AlarmID = enum_aID_Buf6_W_PID_cLimit_Fault;		
							g_Alarm_Register_Request_Bit = 1;
						END	! IF(Eng_Pid_Control_CErrorCnt >= g_Eng_PID_Process_ValList(0)(index_PidList_CNTmax))
						
						Eng_Pid_Control_YPrevValue = g_Eng_PID_Process_ValList(0)(index_PidList_Y);						
						
					! ----------------------------------------------------------------------------------------------													
					
					! *** Alpha Mode Control Output Process
					ELSE							
						! *** C Ref
						g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref) = g_Target_Engine_AlphaPosi;				
														
					END ! IF(g_Eng_ControlMode <>  Eng_enum_ControlMode_ThrAlpha)		
					
					! ----------------------------------------------------------------------------------------------													
					
					! *** C Ref Min/Max Check
					IF((g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref)) < 0)
						g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref) = 0;
					ELSEIF((g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref)) > 100)
						g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref) = 100;
					END ! IF((g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref)) < 0)
					
					! *** C Step Output							
					! *** MMI Show Message
					IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Step Output C_Ref = <%d>, C_PrevOutput = <%d> ", BufferProgrmaNumber, g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref), g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput); END
					
					IF((g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref) - g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput)) > 2)
						
						g_Eng_PID_Process_ValList(0)(index_PidList_C) = (g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput) + (g_Eng_PID_Process_ValList(0)(index_PidList_CADelta)));
						
						IF(g_Eng_PID_Process_ValList(0)(index_PidList_C) > g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref))
							g_Eng_PID_Process_ValList(0)(index_PidList_C) = g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref);
						END ! IF(g_Eng_PID_Process_ValList(0)(index_PidList_C) > g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref))
						
						! *** MMI Show Message
						IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Step Output Acc C = <%d>", BufferProgrmaNumber, g_Eng_PID_Process_ValList(0)(index_PidList_C); END
					
					ELSEIF((g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref) - g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput)) < -2)
																				
						g_Eng_PID_Process_ValList(0)(index_PidList_C) = ((g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput)) - g_Eng_PID_Process_ValList(0)(index_PidList_CDDelta));
						
						IF(g_Eng_PID_Process_ValList(0)(index_PidList_C) < g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref))
							g_Eng_PID_Process_ValList(0)(index_PidList_C) = g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref);
						END ! IF(g_Eng_PID_Process_ValList(0)(index_PidList_C) < g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref))
						
						! *** MMI Show Message
						IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Step Output Dec C = <%d>", BufferProgrmaNumber, g_Eng_PID_Process_ValList(0)(index_PidList_C); END
					
					ELSE 
						
						g_Eng_PID_Process_ValList(0)(index_PidList_C) = g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref);
						
						! *** MMI Show Message
						IF (g_Buffer6_displayCmdLevel <> 0); DISP "[#%d buffer] Step Output Steady C = <%d>", BufferProgrmaNumber, g_Eng_PID_Process_ValList(0)(index_PidList_C); END
									
						
					END ! IF((g_Eng_PID_Process_ValList(0)(index_PidList_C_Ref) - g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput)) > 2)
					
					IF((g_ControlMode = g_CtMo_enum_Automatic) & (g_AutoMd_ProfilePlay_Enable = 0))	
						g_Eng_PID_Process_ValList(0)(index_PidList_C) = 0;
					END ! IF((g_ControlMode = g_CtMo_enum_Automatic) & (g_AutoMd_ProfilePlay_Enable = 0))	
					
					! *** C >> C Previous Update	
					g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput) = g_Eng_PID_Process_ValList(0)(index_PidList_C);
					
					!DISP Eng_Pid_Control_Operable, g_Eng_PID_Process_ValList(0)(index_PidList_C), g_Eng_PID_Process_ValList(0)(index_PidList_P_Portion), g_Eng_PID_Process_ValList(0)(index_PidList_I_Portion)
					
					! *** Output validation 								
					Eng_Pid_Calc_Alpha_1_DaVal = g_unit_Cmd_Alpha_1_0Pct_ZeroOffset_DaVal + g_unit_Cmd_Alpha_1_0Pct_DaVal + (g_Eng_PID_Process_ValList(0)(index_PidList_C) * g_unit_Cmd_100PctToAlpha_1_DARation);
					Eng_Pid_Calc_Alpha_2_DaVal = g_unit_Cmd_Alpha_2_0Pct_ZeroOffset_DaVal + g_unit_Cmd_Alpha_2_0Pct_DaVal + (g_Eng_PID_Process_ValList(0)(index_PidList_C) * g_unit_Cmd_100PctToAlpha_2_DARation);
					
					IF(g_unit_Cmd_Alpha_1_0Pct_DaVal < g_unit_Cmd_Alpha_1_100Pct_DaVal)
						IF(Eng_Pid_Calc_Alpha_1_DaVal < g_unit_Cmd_Alpha_1_0Pct_DaVal); Eng_Pid_Calc_Alpha_1_DaVal = g_unit_Cmd_Alpha_1_0Pct_DaVal; END
						IF(Eng_Pid_Calc_Alpha_1_DaVal > g_unit_Cmd_Alpha_1_100Pct_DaVal); Eng_Pid_Calc_Alpha_1_DaVal = g_unit_Cmd_Alpha_1_100Pct_DaVal; END
					ELSE
						IF(Eng_Pid_Calc_Alpha_1_DaVal > g_unit_Cmd_Alpha_1_0Pct_DaVal); Eng_Pid_Calc_Alpha_1_DaVal = g_unit_Cmd_Alpha_1_0Pct_DaVal; END
						IF(Eng_Pid_Calc_Alpha_1_DaVal < g_unit_Cmd_Alpha_1_100Pct_DaVal); Eng_Pid_Calc_Alpha_1_DaVal = g_unit_Cmd_Alpha_1_100Pct_DaVal; END
					END !IF(g_unit_Cmd_Alpha_1_0Pct_DaVal < g_unit_Cmd_Alpha_1_100Pct_DaVal)
					
					IF(g_unit_Cmd_Alpha_2_0Pct_DaVal < g_unit_Cmd_Alpha_2_100Pct_DaVal)
						IF(Eng_Pid_Calc_Alpha_2_DaVal < g_unit_Cmd_Alpha_2_0Pct_DaVal); Eng_Pid_Calc_Alpha_2_DaVal = g_unit_Cmd_Alpha_2_0Pct_DaVal; END
						IF(Eng_Pid_Calc_Alpha_2_DaVal > g_unit_Cmd_Alpha_2_100Pct_DaVal); Eng_Pid_Calc_Alpha_2_DaVal = g_unit_Cmd_Alpha_2_100Pct_DaVal; END
					ELSE
						IF(Eng_Pid_Calc_Alpha_2_DaVal > g_unit_Cmd_Alpha_2_0Pct_DaVal); Eng_Pid_Calc_Alpha_2_DaVal = g_unit_Cmd_Alpha_2_0Pct_DaVal; END
						IF(Eng_Pid_Calc_Alpha_2_DaVal < g_unit_Cmd_Alpha_2_100Pct_DaVal); Eng_Pid_Calc_Alpha_2_DaVal = g_unit_Cmd_Alpha_2_100Pct_DaVal; END
					END !IF(g_unit_Cmd_Alpha_2_0Pct_DaVal < g_unit_Cmd_Alpha_2_100Pct_DaVal)
													
					! *** c Output & C_Prev Update					
					g_AO_AlphaASignal = Eng_Pid_Calc_Alpha_1_DaVal;	
					g_AO_AlphaBSignal = Eng_Pid_Calc_Alpha_2_DaVal;
					! *** c Output to 10V Range 20170414
					int Eng_Pid_Calc_10V;
					
					!Eng_Pid_Calc_10V = (g_Eng_PID_Process_ValList(0)(index_PidList_C)/g_Hmi_Target_Engine_AlphaPosi_Range*32767);
					Eng_Pid_Calc_10V = (g_Eng_PID_Process_ValList(0)(index_PidList_C)/100*32767);
					IF (g_PID_Change_Ramp>0)
						g_PID_Change_Ramp = g_PID_Change_Ramp-1;
					else
						g_AO_ThrottleControlSignal = Eng_Pid_Calc_10V;	
					end

					IF((g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt >= 0) & (g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt >= 0))
						IF(Eng_Pid_Calc_Alpha_1_DaVal > g_unit_Cmd_Alpha_DO_IdleVali_DaVal)				
							g_DO_ThrottleIVSSignal = 1;
						ELSE
							g_DO_ThrottleIVSSignal = 0;
						END ! IF(ServiceMd_Target_Calc_Alpha_1_DaVal > g_unit_Cmd_Alpha_DO_IdleVali_DaVal)	
					END !IF((g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt >= 0) & (g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt >= 0))							

					g_Eng_PID_Process_ValList(0)(index_PidList_C_PrevOutput) = g_Eng_PID_Process_ValList(0)(index_PidList_C);
					
					
					! *** PID Data Logger
					!COPY(g_Eng_PID_Process_ValList, g_Eng_PID_DataLogger_Matrix, 0, 0, 0, Pid_DataLogger_Matrix_MaxColumnIndex, Pid_DataLogger_Matrix_RowCopy_Index, Pid_DataLogger_Matrix_RowCopy_Index, 0, Pid_DataLogger_Matrix_MaxColumnIndex)
					!Pid_DataLogger_Matrix_RowCopy_Index = Pid_DataLogger_Matrix_RowCopy_Index + 1;
					
					!IF( Pid_DataLogger_Matrix_RowCopy_Index > Pid_DataLogger_Matrix_MaxRowIndex)
					!	Pid_DataLogger_Matrix_RowCopy_Index = 0;					
					!END
											
				END	! IF((Pid_Time_Measure_1 - Pid_Time_Measure_2) >= Eng_PID_Excution_Interval)
		END ! BLOCK !Dynamometer PID		
	END ! WHILE (g_Eng_Control_Operable = 1)
	
	
STOP
! *************** Autoroutine  *************** !
! ***  Mode Control (#3) Program Stop processing
ON ^PST(3).#RUN

	IF (PST(6).#RUN = 1)
		g_Eng_Control_Operable = 0
				
		! *** c Output & C_Prev Update					
		g_AO_AlphaASignal = g_unit_Cmd_Alpha_1_0Pct_DaVal;	
		g_AO_AlphaBSignal = g_unit_Cmd_Alpha_2_0Pct_DaVal;
		g_AO_ThrottleControlSignal = 0;
		g_SystemState_Eng_IG_On = 0;
		g_DO_IgnitionSignal = 0;  
		
		DISP "[3 <%d>",  g_AO_AlphaASignal;
		IF((g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt >= 0) & (g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt >= 0))
			g_DO_ThrottleIVSSignal = 0;			
		END !IF((g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt >= 0) & (g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt >= 0))	
		
		DISP "[#%d buffer] Mode Control (#3)Program Stop Autoroutine.", BufferProgrmaNumber
		
		STOP 6;
	END
	
RET

! ----------------------------------------------------------------------------------------------

! ***  Target/Feedback processing (#4) Program Stop 
ON ^PST(4).#RUN

	IF (PST(6).#RUN = 1)
		g_Eng_Control_Operable = 0
			
		! *** c Output & C_Prev Update					
		g_AO_AlphaASignal = g_unit_Cmd_Alpha_1_0Pct_DaVal;	
		g_AO_AlphaBSignal = g_unit_Cmd_Alpha_2_0Pct_DaVal;
		g_AO_ThrottleControlSignal = 0;
		g_SystemState_Eng_IG_On = 0;
		g_DO_IgnitionSignal = 0;  
		
		IF((g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt >= 0) & (g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt >= 0))
			g_DO_ThrottleIVSSignal = 0;			
		END !IF((g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt >= 0) & (g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt >= 0))	
		
		DISP "[#%d buffer] Target/Feedback processing (#4)Program Stop Autoroutine.", BufferProgrmaNumber
	
	END
	
RET

! ----------------------------------------------------------------------------------------------

! *** Program Stop processing
ON ^PST(6).#RUN
	
	g_Eng_Control_Operable = 0
	
	! *** c Output & C_Prev Update					
	g_AO_AlphaASignal = g_unit_Cmd_Alpha_1_0Pct_DaVal;	
	g_AO_AlphaBSignal = g_unit_Cmd_Alpha_2_0Pct_DaVal;
	g_AO_ThrottleControlSignal = 0;
	g_SystemState_Eng_IG_On = 0;
	g_DO_IgnitionSignal = 0;  
	
	IF((g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt >= 0) & (g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt >= 0))
		g_DO_ThrottleIVSSignal = 0;			
	END !IF((g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt >= 0) & (g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt >= 0))	
	
	CALL sr_PidControler_VariableList_Initial;
	
	DISP "[#%d buffer] Program Stop Autoroutine.", BufferProgrmaNumber;	
RET

! ----------------------------------------------------------------------------------------------

! *************** Subroutine  *************** !
sr_PidControler_VariableList_Initial:
	
	Eng_Pid_Control_Operable = 0;	
	Eng_Pid_Control_CErrorCnt = 0;
	Eng_Pid_Control_YErrorCnt = 0;
	Eng_Pid_Control_YPrevValue = 0;
	Eng_S_Pid_Control_KP_MAX = 0;
	Eng_S_Pid_Control_iPotion_Delta = 0;
	Eng_T_Pid_Control_iPotion_Delta = 0;
	Eng_Pid_Calc_Alpha_1_DaVal = 0;
	Eng_Pid_Calc_Alpha_2_DaVal = 0;
	
	! *** PID Controler Variable List Initial
	FILL(0, Eng_S_PID_ValList);
	FILL(0, Eng_T_PID_ValList);
	FILL(0, Eng_A_PID_ValList);
	FILL(0, g_Eng_PID_Process_ValList);
	
	! *** PID Controler Variable List Index Initial	
	index_PidList_Kp = 0;
	index_PidList_Ki = 1;
	index_PidList_Kd = 2;
	index_PidList_CIndex = 3
	index_PidList_C = 4;
	index_PidList_Cmax = 5;
	index_PidList_CNTmax = 6;
	index_PidList_RefADelta = 7;
	index_PidList_RefDDelta = 8;
	index_PidList_YIndex = 9;
	index_PidList_Y = 10;
	index_PidList_Ymin = 11;
	index_PidList_Ymax = 12;
	index_PidList_YCNTmax = 13;
	index_PidList_TargetIndex = 14;	
	index_PidList_Target = 15;
	index_PidList_Ref = 16;		
	index_PidList_P_Error = 17;
	index_PidList_P_PrevError = 18;
	index_PidList_I_Error = 19;
	index_PidList_D_Error = 20;
	index_PidList_P_Portion = 21;
	index_PidList_I_Portion = 22;	
	index_PidList_D_Portion = 23;
	index_PidList_PidIntervals = 24;
	index_PidList_CADelta = 25;
	index_PidList_CDDelta = 26;
	index_PidList_C_Ref = 27
	index_PidList_C_PrevOutput = 28;
	
	! *** Speed PID Controller Constant & Variable Initialization	
	Eng_S_PID_ValList(0)(index_PidList_Kp) = g_Hmi_bs4201_EngSpid_KpSt;!Constant
	Eng_S_PID_ValList(0)(index_PidList_Ki) = g_Hmi_bs4201_EngSpid_KiSt;!Constant
	Eng_S_PID_ValList(0)(index_PidList_Kd) = 0;!Constant
	Eng_S_PID_ValList(0)(index_PidList_CIndex) = 0;!Constant
	Eng_S_PID_ValList(0)(index_PidList_C) = 0;!Variable
	Eng_S_PID_ValList(0)(index_PidList_Cmax) = g_Hmi_bs4207_EngSpid_CMaxSt;!Constant
	Eng_S_PID_ValList(0)(index_PidList_CNTmax) = g_Hmi_bs4207_EngSpid_CcntMaxSt;!Constant
	Eng_S_PID_ValList(0)(index_PidList_RefADelta) = (g_Hmi_bs4206_EngSpid_RefADeltaSt / (1000/Eng_PID_Excution_Interval));!Constant
	Eng_S_PID_ValList(0)(index_PidList_RefDDelta) = (g_Hmi_bs4206_EngSpid_RefDDeltaSt / (1000/Eng_PID_Excution_Interval));!Constant
	Eng_S_PID_ValList(0)(index_PidList_YIndex) = g_Hmi_bs5001_FB_SpeedYIndexSt;!Constant
	Eng_S_PID_ValList(0)(index_PidList_Y) = 0;!Variable
	Eng_S_PID_ValList(0)(index_PidList_Ymin) = g_Hmi_bs4205_EngSpid_YMinSt;!Constant
	Eng_S_PID_ValList(0)(index_PidList_Ymax) = 0;!Constant
	Eng_S_PID_ValList(0)(index_PidList_YCNTmax) = 0;!Constant
	Eng_S_PID_ValList(0)(index_PidList_TargetIndex) = 0;!Constant
	Eng_S_PID_ValList(0)(index_PidList_Target) = 0;!Variable
	Eng_S_PID_ValList(0)(index_PidList_Ref) = 0;!Variable
	Eng_S_PID_ValList(0)(index_PidList_P_Error) = 0;!Variable
	Eng_S_PID_ValList(0)(index_PidList_P_PrevError) = 0;!Variable
	Eng_S_PID_ValList(0)(index_PidList_I_Error) = 0;	!Variable
	Eng_S_PID_ValList(0)(index_PidList_D_Error) = 0;!Variable
	Eng_S_PID_ValList(0)(index_PidList_P_Portion) = 0;!Variable
	Eng_S_PID_ValList(0)(index_PidList_I_Portion) = 0;!Variable
	Eng_S_PID_ValList(0)(index_PidList_D_Portion) = 0;!Variable
	Eng_S_PID_ValList(0)(index_PidList_PidIntervals) = 0;!Variable
	Eng_S_PID_ValList(0)(index_PidList_CADelta) = (g_Hmi_bs4208_EngSpid_C_ADeltaSt / (1000/Eng_PID_Excution_Interval));!Constant
	Eng_S_PID_ValList(0)(index_PidList_CDDelta) = (g_Hmi_bs4208_EngSpid_C_DDeltaSt / (1000/Eng_PID_Excution_Interval));!Constant
	Eng_S_PID_ValList(0)(index_PidList_C_Ref) = 0;!Variable	
	Eng_S_PID_ValList(0)(index_PidList_C_PrevOutput) = 0;!Variable	
	
	! *** Torque PID Controller Constant & Variable Initialization	
	Eng_T_PID_ValList(0)(index_PidList_Kp) = g_Hmi_bs4301_EngTpid_KpSt;!Constant
	Eng_T_PID_ValList(0)(index_PidList_Ki) = g_Hmi_bs4301_EngTpid_KiSt;!Constant
	Eng_T_PID_ValList(0)(index_PidList_Kd) = 0;!Constant
	Eng_T_PID_ValList(0)(index_PidList_CIndex) = 0;!Constant
	Eng_T_PID_ValList(0)(index_PidList_C) = 0;!Variable
	Eng_T_PID_ValList(0)(index_PidList_Cmax) = g_Hmi_bs4306_EngTpid_CMaxSt;!Constant
	Eng_T_PID_ValList(0)(index_PidList_CNTmax) = g_Hmi_bs4306_EngTpid_CcntMaxSt;!Constant
	Eng_T_PID_ValList(0)(index_PidList_RefADelta) = (g_Hmi_bs4305_EngTpid_RefADeltaSt / (1000/Eng_PID_Excution_Interval));!Constant
	Eng_T_PID_ValList(0)(index_PidList_RefDDelta) = (g_Hmi_bs4305_EngTpid_RefDDeltaSt / (1000/Eng_PID_Excution_Interval));!Constant
	Eng_T_PID_ValList(0)(index_PidList_YIndex) = g_Hmi_bs5101_FB_TorqueYIndexSt;!Constant
	Eng_T_PID_ValList(0)(index_PidList_Y) = 0;!Variable
	Eng_T_PID_ValList(0)(index_PidList_Ymin) = g_Hmi_bs4305_EngTpid_YMinSt;!Constant
	Eng_T_PID_ValList(0)(index_PidList_Ymax) = 0;!Constant
	Eng_T_PID_ValList(0)(index_PidList_YCNTmax) = 0;!Constant
	Eng_T_PID_ValList(0)(index_PidList_TargetIndex) = 0;!Constant
	Eng_T_PID_ValList(0)(index_PidList_Target) = 0;!Variable
	Eng_T_PID_ValList(0)(index_PidList_Ref) = 0;!Variable
	Eng_T_PID_ValList(0)(index_PidList_P_Error) = 0;!Variable
	Eng_T_PID_ValList(0)(index_PidList_P_PrevError) = 0;!Variable
	Eng_T_PID_ValList(0)(index_PidList_I_Error) = 0;	!Variable
	Eng_T_PID_ValList(0)(index_PidList_D_Error) = 0;!Variable
	Eng_T_PID_ValList(0)(index_PidList_P_Portion) = 0;!Variable
	Eng_T_PID_ValList(0)(index_PidList_I_Portion) = 0;!Variable
	Eng_T_PID_ValList(0)(index_PidList_D_Portion) = 0;!Variable
	Eng_T_PID_ValList(0)(index_PidList_PidIntervals) = 0;!Variable
	Eng_T_PID_ValList(0)(index_PidList_CADelta) = (g_Hmi_bs4307_EngTpid_C_ADeltaSt / (1000/Eng_PID_Excution_Interval));!Constant
	Eng_T_PID_ValList(0)(index_PidList_CDDelta) = (g_Hmi_bs4307_EngTpid_C_DDeltaSt / (1000/Eng_PID_Excution_Interval));!Constant
	Eng_T_PID_ValList(0)(index_PidList_C_Ref) = 0;!Variable
	Eng_T_PID_ValList(0)(index_PidList_C_PrevOutput) = 0;!Variable
	
	! *** Torque PID Controller Constant & Variable Initialization	
	Eng_A_PID_ValList(0)(index_PidList_Kp) = 0;!Constant
	Eng_A_PID_ValList(0)(index_PidList_Ki) = 0;!Constant
	Eng_A_PID_ValList(0)(index_PidList_Kd) = 0;!Constant
	Eng_A_PID_ValList(0)(index_PidList_CIndex) = 0;!Constant
	Eng_A_PID_ValList(0)(index_PidList_C) = 0;!Variable
	Eng_A_PID_ValList(0)(index_PidList_Cmax) = 0;!Constant
	Eng_A_PID_ValList(0)(index_PidList_CNTmax) = 0;!Constant
	Eng_A_PID_ValList(0)(index_PidList_RefADelta) = 0;!Constant
	Eng_A_PID_ValList(0)(index_PidList_RefDDelta) = 0;!Constant
	Eng_A_PID_ValList(0)(index_PidList_YIndex) = 0;!Constant
	Eng_A_PID_ValList(0)(index_PidList_Y) = 0;!Variable
	Eng_A_PID_ValList(0)(index_PidList_Ymin) = 0;!Constant
	Eng_A_PID_ValList(0)(index_PidList_Ymax) = 0;!Constant
	Eng_A_PID_ValList(0)(index_PidList_YCNTmax) = 0;!Constant
	Eng_A_PID_ValList(0)(index_PidList_TargetIndex) = 0;!Constant
	Eng_A_PID_ValList(0)(index_PidList_Target) = 0;!Variable
	Eng_A_PID_ValList(0)(index_PidList_Ref) = 0;!Variable
	Eng_A_PID_ValList(0)(index_PidList_P_Error) = 0;!Variable
	Eng_A_PID_ValList(0)(index_PidList_P_PrevError) = 0;!Variable
	Eng_A_PID_ValList(0)(index_PidList_I_Error) = 0;	!Variable
	Eng_A_PID_ValList(0)(index_PidList_D_Error) = 0;!Variable
	Eng_A_PID_ValList(0)(index_PidList_P_Portion) = 0;!Variable
	Eng_A_PID_ValList(0)(index_PidList_I_Portion) = 0;!Variable
	Eng_A_PID_ValList(0)(index_PidList_D_Portion) = 0;!Variable
	Eng_A_PID_ValList(0)(index_PidList_PidIntervals) = 0;!Variable
	Eng_A_PID_ValList(0)(index_PidList_CADelta) = (g_Hmi_bs4401_EngApid_C_ADeltaSt / (1000/Eng_PID_Excution_Interval));!Constant
	Eng_A_PID_ValList(0)(index_PidList_CDDelta) = (g_Hmi_bs4401_EngApid_C_DDeltaSt / (1000/Eng_PID_Excution_Interval));!Constant
	Eng_A_PID_ValList(0)(index_PidList_C_Ref) = 0;!Variable
	Eng_A_PID_ValList(0)(index_PidList_C_PrevOutput) = 0;!Variable
	
RET

! ----------------------------------------------------------------------------------------------

sr_PidControler_ExecutionContition_Check:

	INT rstConditionCheck;	
		rstConditionCheck = 0;	
	IF(g_Error_Exists_Bit = 0)		
		!IF(g_SystemState_Eng_Started  = 1)
			IF(((g_ControlMode <> g_CtMo_enum_Default) & (g_DriveManual_Mode <> g_DrMn_enum_Default)) | (g_SystemState_Eng_Starting = 1))
				IF((g_Eng_ControlMode = Eng_enum_ControlMode_Idle) | (g_Eng_ControlMode = Eng_enum_ControlMode_Speed) | (g_Eng_ControlMode = Eng_enum_ControlMode_Torque) | (g_Eng_ControlMode = Eng_enum_ControlMode_ThrAlpha) | (g_SystemState_Eng_Starting = 1))
					rstConditionCheck = 1;
				END!IF(g_Dyno_ControlMode <> g_Dyno_enum_ControlMode_Default)
			END ! IF((g_ControlMode <> g_CtMo_enum_Default) & (g_DriveManual_Mode <> g_DrMn_enum_Default))	
		!END ! IF(g_SystemState_Eng_Started  = 1)	
	END ! IF(g_Error_Exists_Bit = 0)
		
	g_Eng_Control_Operable = rstConditionCheck;
			DISP "[#%d buffer] g_Eng_Control_Operable [#%d].", BufferProgrmaNumber,g_Eng_Control_Operable

RET

! ----------------------------------------------------------------------------------------------

sr_PidControler_ControlModeSelect_PidParaAppend:
	
	g_Eng_ControlMode = Eng_enum_ControlMode_Default;
	
	IF(g_ControlMode <> g_CtMo_enum_Default)
		
		IF((g_DriveManual_Mode = g_DrMn_enum_Idle) | (g_DriveManual_Mode = g_DrMn_enum_IdleContrl) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTA) | (g_SystemState_Eng_Starting = 1))
		
			g_Eng_ControlMode = Eng_enum_ControlMode_ThrAlpha ;			
			COPY(Eng_A_PID_ValList, g_Eng_PID_Process_ValList);			
			
		ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyTo_EngRPM)
		
			g_Eng_ControlMode = Eng_enum_ControlMode_Speed;		
			COPY(Eng_S_PID_ValList, g_Eng_PID_Process_ValList);
			
		ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTo)
		
			g_Eng_ControlMode = Eng_enum_ControlMode_Torque;			
			COPY(Eng_T_PID_ValList, g_Eng_PID_Process_ValList);			
			
		END ! IF((g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA) | (g_DriveManual_Mode = g_DrMn_enum_DyTo_EngRPM))

	END !IF(g_ControlMode <> g_CtMo_enum_Default)
	
	Eng_DrvMd_Previous_Mode = g_DriveManual_Mode;
		DISP "[#%d buffer] g_DriveManual_Mode [#%d].", BufferProgrmaNumber,g_Eng_ControlMode
		DISP "[#%d buffer] Eng_DrvMd_Previous_Mode [#%d].", BufferProgrmaNumber,Eng_DrvMd_Previous_Mode

RET


#7
! *** Data Acquisition Program

INT BufferProgrmaNumber
	BufferProgrmaNumber = 7
! MMI Show Message
DISP "[#%d buffer] program started.", BufferProgrmaNumber;
	
	!GLOBAL INT g_DAQ_Enable;
		g_DAQ_Enable = 1;
	!GLOBAL REAL g_DAQ_Period;
		g_DAQ_Period = 100;
		
	! *** DAQ Time Measure
	REAL DAQ_Time_Measure_1;
	REAL DAQ_Time_Measure_2;
	REAL DAQ_Time_Difference;
		
		DAQ_Time_Measure_1 = 0;
		DAQ_Time_Measure_2 = 0;
		DAQ_Time_Difference = 0;
	
	! *** EtherCAT Input Data Sorting Variables list
	INT eCAT_InputData_SortingList(1)(120) !changed from 100 to 120 @9/12 for updating calculation data, must change also LN 97,99,895
	INT Input_DataNumber;
	
	INT g_eCAT_Input_Data(1)(600);
	FILL(-1, g_eCAT_Input_Data);

	! *** Input Data Buffer Variables
	INT index_DAQProgressBufferNo;
		index_DAQProgressBufferNo = 0;	
	INT index_DAQBufferMaxCount;
		index_DAQBufferMaxCount = 20;		
	INT index_DAQUniqueNo;
		index_DAQUniqueNo = 0;	
	
	call sr_Variable_Init;
	
	! *** PC Read Data Fill Zero
	FILL(0, g_PC_Read_Data);
	
	! *** PC Read Data Update Index & Variables
		! *** Controller Status Variables 	
		INT index_ControllerStatus;
			index_ControllerStatus = 0x0000;
		
		! *** Control Mode Variables 	
		INT index_ControlMode;
			index_ControlMode = 0x0030;
			
		! *** Drive Mode Variables 	
		INT index_DriveMode;
			index_DriveMode = 0x0050;
			
		! *** Controller Program State Variables 			
		INT index_sL_ProgResult_EndCol
			index_sL_ProgResult_EndCol = (g_bufferProgramResultListSize - 1);
		INT index_dL_ProgResultCopy_StartCol;
			index_dL_ProgResultCopy_StartCol = 0x0070;
		INT index_dL_ProgResultCopy_EndCol;
			index_dL_ProgResultCopy_EndCol = index_dL_ProgResultCopy_StartCol + index_sL_ProgResult_EndCol;	
			
		! *** Read DAQ State Variables
		INT index_DAQState;
			index_DAQState = 0x00B0;
		
		! *** OP HMI Data Variables
		INT index_sL_rIoMapData_HMI
			index_sL_rIoMapData_HMI = 0xD0;
			
			
		! *** PID Data Variables
		INT index_PID_Data
			index_PID_Data = 0x100;
		
		! *** Averaging data
		INT Avg_eCAT_InputData_SortingList(1)(120);
			FILL(0,Avg_eCAT_InputData_SortingList);
		INT Avg_prev_eCAT_InputData_SortingList(1)(120);
			FILL(0,Avg_prev_eCAT_InputData_SortingList);			
		INT index_Averaging;
			index_Averaging=0;	
		INT index_SortingList;
			index_SortingList=0;
		INT DAQ_Period_For_Averaging;
			DAQ_Period_For_Averaging=1;
				
		! *** EtherCAT Input Data Clean up Variables
		INT index_Orderinputdata;
			index_Orderinputdata = 0;
		INT index_eCAT_Inputdata;		
			index_eCAT_Inputdata = 0;
		
		! *** EtherCAT Input Data Variables
		INT index_dL_eCatInDataCopy_StartCol;
			index_dL_eCatInDataCopy_StartCol = 0x0200;
		INT index_dL_eCatInDataCopy_EndCol;
			index_dL_eCatInDataCopy_EndCol = index_dL_eCatInDataCopy_StartCol + Input_DataNumber;		
		
		! *** Dynamometer Constant Variables
		
		! *** Engine Constant Variables
		
		
		! *** Remote IO Mappint Data Update Variables
		INT index_sL_rIoMapData_EndCol		
		INT index_dL_rIoMapDataCopy_StartCol;		
		INT index_dL_rIoMapDataCopy_EndCol;
		
		! *** Daq Data Update Variables
		INT index_sL_daqData_EndCol		
		INT index_dL_daqDataCopy_StartCol;		
		INT index_dL_daqDataCopy_EndCol;
	
	WHILE g_DAQ_Enable = 1
		
		BLOCK
		
			CALL sr_Variable_Update;
			
			DAQ_Time_Measure_1 = TIME
			
			IF((DAQ_Time_Measure_1 - DAQ_Time_Measure_2) >= g_DAQ_Period)
			
				! *** measure the DAQ execution time.
				DAQ_Time_Difference = DAQ_Time_Measure_1 - DAQ_Time_Measure_2
				DAQ_Time_Measure_2 = DAQ_Time_Measure_1			
			
				
				! *** PC Read Data Update
				!CALL sr_pcRead_ControllerStatus;
				!CALL sr_pcRead_ControlMode;
				!CALL sr_pcRead_DriveMode;
				!CALL sr_pcRead_ControllerProgramState;
				!CALL sr_pcRead_DAQState;
				!CALL sr_EtherCATInputData_CleanUp;	
				!CALL sr_pcRead_EtherCATInputData;
				!CALL sr_pcRead_DynoConstant;
				!CALL sr_pcRead_EngineConstant;
				!CALL sr_pcRead_RemoteIOMappintDataUpdate;
				!CALL sr_pcRead_DaqDataUpdate;
				
				! ----------------------------------------------------------------------------------------------				
				! *** Controller Status  
				
				! *** Power-Up Elapsed Time
				g_PC_Read_Data(0)((index_ControllerStatus + 0x00)) = TIME;
					
				! *** Control On
				IF (g_SystemState_Dyno_On <> 0)
					g_PC_Read_Data(0)((index_ControllerStatus + 0x04)) = 1;	
				ELSE
					g_PC_Read_Data(0)((index_ControllerStatus + 0x04)) = 0;
				END
				
				! *** NORMAL	
				IF (g_Alarm_Exists_Bit = 0)		
					g_PC_Read_Data(0)((index_ControllerStatus + 0x06)) = 1;
					g_PC_Read_Data(0)((index_ControllerStatus + 0x07)) = 0;	
				! *** FAULT
				ELSE		
					g_PC_Read_Data(0)((index_ControllerStatus + 0x06)) = 0;
					g_PC_Read_Data(0)((index_ControllerStatus + 0x07)) = 1;		
				END
				
				! *** DOWN
				IF(g_Error_Exists_Bit <> 0)
					g_PC_Read_Data(0)((index_ControllerStatus + 0x09)) = 0;
					g_PC_Read_Data(0)((index_ControllerStatus + 0x0A)) = 0;
					g_PC_Read_Data(0)((index_ControllerStatus + 0x0B)) = 0;
					g_PC_Read_Data(0)((index_ControllerStatus + 0x0C)) = 0;
					g_PC_Read_Data(0)((index_ControllerStatus + 0x0D)) = 1;
				! *** INIT
				ELSEIF((PST(0).#RUN) | (PST(1).#RUN))
					g_PC_Read_Data(0)((index_ControllerStatus + 0x09)) = 1;
					g_PC_Read_Data(0)((index_ControllerStatus + 0x0A)) = 0;
					g_PC_Read_Data(0)((index_ControllerStatus + 0x0B)) = 0;
					g_PC_Read_Data(0)((index_ControllerStatus + 0x0C)) = 0;
					g_PC_Read_Data(0)((index_ControllerStatus + 0x0D)) = 0;
				! *** RUN	
				ELSEIF((PST(5).#RUN) & (PST(6).#RUN))
					g_PC_Read_Data(0)((index_ControllerStatus + 0x09)) = 0;
					g_PC_Read_Data(0)((index_ControllerStatus + 0x0A)) = 0;
					g_PC_Read_Data(0)((index_ControllerStatus + 0x0B)) = 1;	
					g_PC_Read_Data(0)((index_ControllerStatus + 0x0C)) = 0;
					g_PC_Read_Data(0)((index_ControllerStatus + 0x0D)) = 0;
				! *** IDLE
				ELSEIF(PST(3).#RUN)
					g_PC_Read_Data(0)((index_ControllerStatus + 0x09)) = 0;
					g_PC_Read_Data(0)((index_ControllerStatus + 0x0A)) = 1;
					g_PC_Read_Data(0)((index_ControllerStatus + 0x0B)) = 0;	
					g_PC_Read_Data(0)((index_ControllerStatus + 0x0C)) = 0;
					g_PC_Read_Data(0)((index_ControllerStatus + 0x0D)) = 0;
				END	
				! *** PAUSE
				
				! *** Alarm Exists
				IF (g_Alarm_Exists_Bit <> 0)
					g_PC_Read_Data(0)((index_ControllerStatus + 0x10)) = 1;
				ELSE
					g_PC_Read_Data(0)((index_ControllerStatus + 0x10)) = 0;
				END
				
				! *** Error Exists
				IF (g_Error_Exists_Bit <> 0)
					g_PC_Read_Data(0)((index_ControllerStatus + 0x11)) = 1;
				ELSE
					g_PC_Read_Data(0)((index_ControllerStatus + 0x11)) = 0;
				END
				
				! *** Warning Exists
				IF (g_Warning_Exists_Bit <> 0)
					g_PC_Read_Data(0)((index_ControllerStatus + 0x13)) = 1;
				ELSE
					g_PC_Read_Data(0)((index_ControllerStatus + 0x13)) = 0;
				END
				
				! *** Current Alarm ID
				IF (g_Alarm_Exists_Bit <> 0)
					g_PC_Read_Data(0)((index_ControllerStatus + 0x14)) = g_Alarm_Register_Request_AlarmID;
				END
							
				! ----------------------------------------------------------------------------------------------				
				! *** Control Mode 	
				
				! *** Monitor
				IF(g_ControlMode = g_CtMo_enum_Monitor)	
					g_PC_Read_Data(0)((index_ControlMode + 0x00)) = 1;
					g_PC_Read_Data(0)((index_ControlMode + 0x01)) = 0;
					g_PC_Read_Data(0)((index_ControlMode + 0x02)) = 0;
					g_PC_Read_Data(0)((index_ControlMode + 0x03)) = 0;
				! *** DynoService
				ELSEIF(g_ControlMode = g_CtMo_enum_OpenLoopTest)	
					g_PC_Read_Data(0)((index_ControlMode + 0x00)) = 0;
					g_PC_Read_Data(0)((index_ControlMode + 0x01)) = 1;
					g_PC_Read_Data(0)((index_ControlMode + 0x02)) = 0;
					g_PC_Read_Data(0)((index_ControlMode + 0x03)) = 0;
				! *** Manual
				ELSEIF(g_ControlMode = g_CtMo_enum_Manual)	
					g_PC_Read_Data(0)((index_ControlMode + 0x00)) = 0;
					g_PC_Read_Data(0)((index_ControlMode + 0x01)) = 0;
					g_PC_Read_Data(0)((index_ControlMode + 0x02)) = 1;
					g_PC_Read_Data(0)((index_ControlMode + 0x03)) = 0;
				! *** Automatic
				ELSEIF(g_ControlMode = g_CtMo_enum_Automatic)	
					g_PC_Read_Data(0)((index_ControlMode + 0x00)) = 0;
					g_PC_Read_Data(0)((index_ControlMode + 0x01)) = 0;
					g_PC_Read_Data(0)((index_ControlMode + 0x02)) = 0;
					g_PC_Read_Data(0)((index_ControlMode + 0x03)) = 1;
				END
	
				! ----------------------------------------------------------------------------------------------				
				! *** Drive Mode  	
				
				! *** Idle
				IF(g_DriveManual_Mode = g_DrMn_enum_Idle)	
					g_PC_Read_Data(0)((index_DriveMode + 0x00)) = 1;
					g_PC_Read_Data(0)((index_DriveMode + 0x01)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x02)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x03)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x04)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x05)) = 0;
				! *** IdleContrl
				ELSEIF(g_DriveManual_Mode = g_DrMn_enum_IdleContrl)	
					g_PC_Read_Data(0)((index_DriveMode + 0x00)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x01)) = 1;
					g_PC_Read_Data(0)((index_DriveMode + 0x02)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x03)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x04)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x05)) = 0;
				! *** DyTo_EngTA
				ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA)	
					g_PC_Read_Data(0)((index_DriveMode + 0x00)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x01)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x02)) = 1;
					g_PC_Read_Data(0)((index_DriveMode + 0x03)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x04)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x05)) = 0;
				! *** DyRPM_EngTA
				ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTA)	
					g_PC_Read_Data(0)((index_DriveMode + 0x00)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x01)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x02)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x03)) = 1;
					g_PC_Read_Data(0)((index_DriveMode + 0x04)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x05)) = 0;
				! *** DyTo_EngRPM
				ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyTo_EngRPM)	
					g_PC_Read_Data(0)((index_DriveMode + 0x00)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x01)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x02)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x03)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x04)) = 1;
					g_PC_Read_Data(0)((index_DriveMode + 0x05)) = 0;
				! *** DyRPM_EngTo
				ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTo)	
					g_PC_Read_Data(0)((index_DriveMode + 0x00)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x01)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x02)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x03)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x04)) = 0;
					g_PC_Read_Data(0)((index_DriveMode + 0x05)) = 1;
				END
							
				! ----------------------------------------------------------------------------------------------
				! *** Controller Program State 			
				COPY(g_bufferProgramResult, g_PC_Read_Data,0,0,0,index_sL_ProgResult_EndCol,0,0,index_dL_ProgResultCopy_StartCol,index_dL_ProgResultCopy_EndCol)
					
				! ----------------------------------------------------------------------------------------------
				! *** Read DAQ State Variables
				
				! *** DAQ Run
				IF(g_DAQ_Enable = 1)	
					g_PC_Read_Data(0)((index_DAQState + 0x00)) = 1;
					g_PC_Read_Data(0)((index_DAQState + 0x01)) = 0;	
				! *** DAQ 
				ELSE
					g_PC_Read_Data(0)((index_DAQState + 0x00)) = 0;
					g_PC_Read_Data(0)((index_DAQState + 0x01)) = 1;	
				END
						
				! *** DAQ Buffer Max Count
				g_PC_Read_Data(0)((index_DAQState + 0x03)) = index_DAQBufferMaxCount;		
				
				! *** DAQ Progress Buffer No.
				g_PC_Read_Data(0)((index_DAQState + 0x04)) = index_DAQProgressBufferNo;		
				
				! *** DAQ Interval
				g_PC_Read_Data(0)((index_DAQState + 0x06)) = g_DAQ_Period;	
	
				! ----------------------------------------------------------------------------------------------
				! *** EtherCAT Input Data Address Copy ***
				index_dL_rIoMapDataCopy_StartCol = 0x0200;
				index_dL_rIoMapDataCopy_EndCol = index_dL_rIoMapDataCopy_StartCol + Input_DataNumber;			
				
				! ----------------------------------------------------------------------------------------------
				! *** EtherCAT Input Data Update
				COPY(eCAT_InputData_SortingList, g_PC_Read_Data,0,0,0,Input_DataNumber,0,0,index_dL_rIoMapDataCopy_StartCol,index_dL_rIoMapDataCopy_EndCol)
				
				
				! ----------------------------------------------------------------------------------------------
				! *** Dynamometer Constant				
				! ----------------------------------------------------------------------------------------------
				! *** Engine Constant					
				! ----------------------------------------------------------------------------------------------				
				! *** Feedback data
				g_PC_Read_Data(0)((index_sL_rIoMapData_HMI + 0)) = g_Hmi_bsHeader1_Fb_Speed * 10;	
				g_PC_Read_Data(0)((index_sL_rIoMapData_HMI + 1)) = g_Hmi_bsHeader1_Fb_AlphaPosi * 10;	
				g_PC_Read_Data(0)((index_sL_rIoMapData_HMI + 2)) = g_Hmi_bsHeader1_Fb_Torque * 10;	
				g_PC_Read_Data(0)((index_sL_rIoMapData_HMI + 3)) = g_Hmi_bsHeader1_Fb_Power * 10;	
				g_PC_Read_Data(0)((index_sL_rIoMapData_HMI + 4)) = g_Hmi_bsHeader2_Fb_DynoCurrent * 10;	
				! *** Demand data
				g_PC_Read_Data(0)((index_sL_rIoMapData_HMI + 0x10)) = g_Hmi_bs10x_Demand_Speed * 10;	
				g_PC_Read_Data(0)((index_sL_rIoMapData_HMI + 0x11)) = g_Hmi_bs10x_Demand_AlphaPosi * 10;	
				g_PC_Read_Data(0)((index_sL_rIoMapData_HMI + 0x12)) = g_Hmi_bs10x_Demand_Torque * 10;
				
				g_PC_Read_Data(0)((index_sL_rIoMapData_HMI + 0x14)) = g_Hmi_bs110_Demand_DynoCurrent * 10;			
				
				
				! ----------------------------------------------------------------------------------------------				
				! ***  PID Target  data
				g_PC_Read_Data(0)((index_PID_Data + 0)) = g_Target_Dyno_Speed * 10;	
				g_PC_Read_Data(0)((index_PID_Data + 1)) = g_Target_Dyno_Torque * 10;
				g_PC_Read_Data(0)((index_PID_Data + 2)) = g_Target_Engine_Speed * 10;		
				g_PC_Read_Data(0)((index_PID_Data + 3)) = g_Target_Engine_Torque * 10;	
				g_PC_Read_Data(0)((index_PID_Data + 4)) = g_Target_Engine_AlphaPosi * 10;							
	
				! ----------------------------------------------------------------------------------------------
				! *** Remote IO Mappint Data Update				
				IF (Input_DataNumber < 0)
					DISP "[#%d buffer] It can not Perform a data copy function. eCAT_Input_DataNumber=<%d>",BufferProgrmaNumber, Input_DataNumber;	
					
						! *** Program Resule Update
						g_bufferProgramResult(0)(BufferProgrmaNumber) = -100;
					
				END ! IF (eCAT_Input_DataNumber > -1)
				
				! ----------------------------------------------------------------------------------------------
				! *** Averaging all data with user given averaging period		
				DAQ_Period_For_Averaging = g_Hmi_bs2301_DAQ_Avg_NumberSt; ! Get period count from user setting HMI or PC: 1~999
				IF( DAQ_Period_For_Averaging = 0)
					DAQ_Period_For_Averaging=1; !protection of inserting 0 from HMI
				END
				IF( DAQ_Period_For_Averaging = 1 ) !no averaging						
					! ----------------------------------------------------------------------------------------------
					! *** Daq Data Update				
					index_sL_daqData_EndCol = Input_DataNumber;
					index_dL_daqDataCopy_StartCol = 0x0900 + (0x0100 * index_DAQProgressBufferNo);	
						
					! *** DAQ Buffer No.
					g_PC_Read_Data(0)((index_dL_daqDataCopy_StartCol + 0x00)) = index_DAQProgressBufferNo;
					
					! *** DAQ Unique No.
					g_PC_Read_Data(0)((index_dL_daqDataCopy_StartCol + 0x01)) = index_DAQUniqueNo;
						
					! *** Start Time(ACSPL+)
					g_PC_Read_Data(0)((index_dL_daqDataCopy_StartCol + 0x05)) = TIME;
						
					! *** DAQ Process Time(mSec)
					g_PC_Read_Data(0)((index_dL_daqDataCopy_StartCol + 0x07)) = DAQ_Time_Difference*DAQ_Period_For_Averaging;	
						
					! *** Input data copy
					index_dL_daqDataCopy_StartCol = index_dL_daqDataCopy_StartCol + 0x0010;
					index_dL_daqDataCopy_EndCol = index_dL_daqDataCopy_StartCol + index_sL_daqData_EndCol;	
					!
					COPY(eCAT_InputData_SortingList, g_PC_Read_Data,0,0,0,index_sL_daqData_EndCol,0,0,index_dL_daqDataCopy_StartCol,index_dL_daqDataCopy_EndCol)
												
						index_DAQProgressBufferNo = index_DAQProgressBufferNo + 1;
						IF(index_DAQProgressBufferNo >= index_DAQBufferMaxCount) index_DAQProgressBufferNo = 0; END
								
						index_DAQUniqueNo = index_DAQUniqueNo + 1;
						IF(index_DAQUniqueNo >= 32767) index_DAQUniqueNo = 0; END
										
						
					! *** Program Resule Update
					g_bufferProgramResult(0)(BufferProgrmaNumber) = 700;
					
				ELSE ! do average	 
					IF(index_Averaging < DAQ_Period_For_Averaging) 					
						IF(index_Averaging = 0) ! initialize variables
							FILL(0,Avg_eCAT_InputData_SortingList);
							FILL(0,Avg_prev_eCAT_InputData_SortingList);
						END
						index_SortingList = 0;
						LOOP(120) !size of eCAT_InputData_SortingList is 120
							Avg_eCAT_InputData_SortingList(0)(index_SortingList) = Avg_prev_eCAT_InputData_SortingList(0)(index_SortingList) + eCAT_InputData_SortingList(0)(index_SortingList)/DAQ_Period_For_Averaging;
							Avg_prev_eCAT_InputData_SortingList(0)(index_SortingList)= Avg_eCAT_InputData_SortingList(0)(index_SortingList);
							index_SortingList = index_SortingList + 1;
						END 
						index_Averaging = index_Averaging + 1;	
					END	!IF(index_Averaging < DAQ_Period_For_Averaging)
									
					IF(index_Averaging = DAQ_Period_For_Averaging)!write to DAQ buffer				
						
						COPY(Avg_eCAT_InputData_SortingList, eCAT_InputData_SortingList);
						index_Averaging = 0;
						
						! ----------------------------------------------------------------------------------------------
						! *** Daq Data Update				
						index_sL_daqData_EndCol = Input_DataNumber;
						index_dL_daqDataCopy_StartCol = 0x0900 + (0x0100 * index_DAQProgressBufferNo);	
								
						! *** DAQ Buffer No.
						g_PC_Read_Data(0)((index_dL_daqDataCopy_StartCol + 0x00)) = index_DAQProgressBufferNo;
							
						! *** DAQ Unique No.
						g_PC_Read_Data(0)((index_dL_daqDataCopy_StartCol + 0x01)) = index_DAQUniqueNo;
								
						! *** Start Time(ACSPL+)
						g_PC_Read_Data(0)((index_dL_daqDataCopy_StartCol + 0x05)) = TIME;
								
						! *** DAQ Process Time(mSec)
						g_PC_Read_Data(0)((index_dL_daqDataCopy_StartCol + 0x07)) = DAQ_Time_Difference*DAQ_Period_For_Averaging;	
								
						! *** Input data copy
						index_dL_daqDataCopy_StartCol = index_dL_daqDataCopy_StartCol + 0x0010;
						index_dL_daqDataCopy_EndCol = index_dL_daqDataCopy_StartCol + index_sL_daqData_EndCol;	
						!
						COPY(eCAT_InputData_SortingList, g_PC_Read_Data,0,0,0,index_sL_daqData_EndCol,0,0,index_dL_daqDataCopy_StartCol,index_dL_daqDataCopy_EndCol)
														
							index_DAQProgressBufferNo = index_DAQProgressBufferNo + 1;
							IF(index_DAQProgressBufferNo >= index_DAQBufferMaxCount) index_DAQProgressBufferNo = 0; END
									
							index_DAQUniqueNo = index_DAQUniqueNo + 1;
							IF(index_DAQUniqueNo >= 32767) index_DAQUniqueNo = 0; END
												
								
						! *** Program Resule Update
						g_bufferProgramResult(0)(BufferProgrmaNumber) = 700;
				
					END !IF(index_Averaging = DAQ_Period_For_Averaging)
					
				END !IF( DAQ_Period_For_Averaging = 1 )
			
			END !IF((DAQ_Time_Measure_1 - DAQ_Time_Measure_2) >= g_DAQ_Period)
		
		END !BLOCK
	END !WHILE 1
! *** Program Resule Update
g_bufferProgramResult(0)(BufferProgrmaNumber) = 9999;

! MMI Show Message
DISP "[#%d buffer] program end.", BufferProgrmaNumber;
STOP
















!-----------------------------------------------------------------------------------
sr_Variable_Init: 
	Input_DataNumber = 104; 
	
ret
!-----------------------------------------------------------------------------------
sr_Variable_Update:
BLOCK
	
	eCAT_InputData_SortingList(0)(0)=g_HMI_DI_DynoService_Button;
	eCAT_InputData_SortingList(0)(1)=g_HMI_DI_Monitor_Button;
	eCAT_InputData_SortingList(0)(2)=g_HMI_DI_Misc_Button;
	eCAT_InputData_SortingList(0)(3)=g_HMI_DI_Manual_Button;
	eCAT_InputData_SortingList(0)(4)=PANEL_SW;
	eCAT_InputData_SortingList(0)(5)=g_HMI_DI_Automatic_Button;
	eCAT_InputData_SortingList(0)(6)=SP1_SW;
	eCAT_InputData_SortingList(0)(7)=g_HMI_DI_Remote_Button;
	eCAT_InputData_SortingList(0)(8)=g_HMI_DI_EngPreheatOn_Button;
	eCAT_InputData_SortingList(0)(9)=g_HMI_DI_EngIGOn_Button;
	eCAT_InputData_SortingList(0)(10)=g_HMI_DI_Stop_Button;
	eCAT_InputData_SortingList(0)(11)=g_HMI_DI_EngStart_Button;
	eCAT_InputData_SortingList(0)(12)=SP3_SW;
	eCAT_InputData_SortingList(0)(13)=SP4_SW;
	eCAT_InputData_SortingList(0)(14)=SP5_SW;
	eCAT_InputData_SortingList(0)(15)=SP6_SW;
	eCAT_InputData_SortingList(0)(16)=g_HMI_DI_BKReady_Button;
	eCAT_InputData_SortingList(0)(17)=SP8_SW;
	eCAT_InputData_SortingList(0)(18)=SP9_SW;
	eCAT_InputData_SortingList(0)(19)=SP10_SW;
	eCAT_InputData_SortingList(0)(20)=g_HMI_DI_DynoOn_Button;
	eCAT_InputData_SortingList(0)(21)=g_HMI_DI_Reset_Button;
	eCAT_InputData_SortingList(0)(22)=g_HMI_DI_ConfigMenu_Button;
	eCAT_InputData_SortingList(0)(23)=g_HMI_DI_AlarmHistory_Button;
	eCAT_InputData_SortingList(0)(24)=SP11_SW;
	eCAT_InputData_SortingList(0)(25)=g_HMI_DI_HornOff_Button;
	eCAT_InputData_SortingList(0)(26)=g_HMI_DI_IdleContrl_Button;
	eCAT_InputData_SortingList(0)(27)=g_HMI_DI_Idle_Button;
	eCAT_InputData_SortingList(0)(28)=g_HMI_DI_DyRPM_EngTA_Button;
	eCAT_InputData_SortingList(0)(29)=g_HMI_DI_DyTo_EngTA_Button;
	eCAT_InputData_SortingList(0)(30)=g_HMI_DI_DyRPM_EngTo_Button;
	eCAT_InputData_SortingList(0)(31)=g_HMI_DI_DyTo_EngRPM_Button;
	eCAT_InputData_SortingList(0)(32)=g_HMI_DO_DynoService_Lamp;
	eCAT_InputData_SortingList(0)(33)=g_HMI_DO_Monitor_Lamp;
	eCAT_InputData_SortingList(0)(34)=g_HMI_DO_MiscOn_Lamp;
	eCAT_InputData_SortingList(0)(35)=g_HMI_DO_Manual_Lamp;
	eCAT_InputData_SortingList(0)(36)=g_HMI_DO_PanelActive_Lamp;
	eCAT_InputData_SortingList(0)(37)=g_HMI_DO_Automatic_Lamp;
	eCAT_InputData_SortingList(0)(38)=SP1_LP;
	eCAT_InputData_SortingList(0)(39)=g_HMI_DO_Remote_Lamp;
	eCAT_InputData_SortingList(0)(40)=g_HMI_DO_EngPreheatOn_Lamp;
	eCAT_InputData_SortingList(0)(41)=g_HMI_DO_EngIGOn_Lamp;
	eCAT_InputData_SortingList(0)(42)=g_HMI_DO_Stop_Lamp;
	eCAT_InputData_SortingList(0)(43)=g_HMI_DO_EngStarting_Lamp;
	eCAT_InputData_SortingList(0)(44)=SP3_LP;
	eCAT_InputData_SortingList(0)(45)=SP4_LP;
	eCAT_InputData_SortingList(0)(46)=SP5_LP;
	eCAT_InputData_SortingList(0)(47)=SP6_LP;
	eCAT_InputData_SortingList(0)(48)=SP7_LP;
	eCAT_InputData_SortingList(0)(49)=SP8_LP;
	eCAT_InputData_SortingList(0)(50)=SP9_LP;
	eCAT_InputData_SortingList(0)(51)=SP10_LP;
	eCAT_InputData_SortingList(0)(52)=g_HMI_DO_DynoOn_Lamp;
	eCAT_InputData_SortingList(0)(53)=g_HMI_DO_AlarmReset_Lamp;
	eCAT_InputData_SortingList(0)(54)=g_HMI_DO_ConfigMenu_Lamp;
	eCAT_InputData_SortingList(0)(55)=g_HMI_DO_AlarmHistory_Lamp;
	eCAT_InputData_SortingList(0)(56)=SP11_LP;
	eCAT_InputData_SortingList(0)(57)=g_HMI_DO_HornOff_Lamp;
	eCAT_InputData_SortingList(0)(58)=g_HMI_DO_IdleControl_Lamp;
	eCAT_InputData_SortingList(0)(59)=g_HMI_DO_Idle_Lamp;
	eCAT_InputData_SortingList(0)(60)=g_HMI_DO_DyRPM_EngTA_Lamp;
	eCAT_InputData_SortingList(0)(61)=g_HMI_DO_DyTo_EngTA_Lamp;
	eCAT_InputData_SortingList(0)(62)=g_HMI_DO_DyRPM_EngTo_Lamp;
	eCAT_InputData_SortingList(0)(63)=g_HMI_DO_DyTo_EngRPM_Lamp;
	eCAT_InputData_SortingList(0)(64)=g_HMI_AI_Dyno_CalRefCnt;
	eCAT_InputData_SortingList(0)(65)=g_HMI_AI_Eng_CalRefCnt;
	eCAT_InputData_SortingList(0)(66)=KNOB_DYNO_Period;
	eCAT_InputData_SortingList(0)(67)=KNOB_ENGINE_Period;
	eCAT_InputData_SortingList(0)(68)=g_DI_DynoTroubleSignal;
	eCAT_InputData_SortingList(0)(69)=g_DI_RemoteOnSignal;
	eCAT_InputData_SortingList(0)(70)=g_DI_DynoSpeedModeSignal;
	eCAT_InputData_SortingList(0)(71)=g_DI_DynoTorqueModeSignal;
	eCAT_InputData_SortingList(0)(72)=g_DI_EngineAlphaModeSignal;
	eCAT_InputData_SortingList(0)(73)=g_DI_EngineTorqueModeSignal;
	eCAT_InputData_SortingList(0)(74)=g_DI_EMSButton;
	eCAT_InputData_SortingList(0)(75)=g_DI_SafetyGuardSignal;
	eCAT_InputData_SortingList(0)(76)=g_DI_SafetyRelaySignal;
	eCAT_InputData_SortingList(0)(77)=g_AI_Target_Dyno_Remote*1000;
	eCAT_InputData_SortingList(0)(78)=g_AI_Target_Engine_Remote*1000;
	eCAT_InputData_SortingList(0)(79)=g_AI_Feedback_Alpha*1000;
	if (g_AI_Feedback_DynoSpeed > g_Hmi_Target_Dyno_Speed_Range) ! Why?
		eCAT_InputData_SortingList(0)(80)=g_Hmi_Target_Dyno_Speed_Range*1000;
	else
		eCAT_InputData_SortingList(0)(80)=g_AI_Feedback_DynoSpeed*1000;
	end
	eCAT_InputData_SortingList(0)(81)=g_AI_Feedback_Vdiff*1000;
	eCAT_InputData_SortingList(0)(82)=g_AI_Feedback_Vref*1000;
	eCAT_InputData_SortingList(0)(83)=g_DO_BKReadySignal;
	eCAT_InputData_SortingList(0)(84)=g_DO_DynoOnSignal;
	eCAT_InputData_SortingList(0)(85)=DO_TS_VALVE1;
	eCAT_InputData_SortingList(0)(86)=DO_TS_VALVE2;
	eCAT_InputData_SortingList(0)(87)=DO_TS_HEATER;
	eCAT_InputData_SortingList(0)(88)=g_DO_DynoResetSignal;
	eCAT_InputData_SortingList(0)(89)=g_DO_ThrottleIVSSignal;
	eCAT_InputData_SortingList(0)(90)=g_DO_PreHeatSignal;
	eCAT_InputData_SortingList(0)(91)=g_DO_StartSignal;
	eCAT_InputData_SortingList(0)(92)=g_DO_IgnitionSignal;
	eCAT_InputData_SortingList(0)(93)=g_DO_HornSignal;
	eCAT_InputData_SortingList(0)(94)=g_DO_Alarm_Lamp;
	eCAT_InputData_SortingList(0)(95)=g_AO_DynoSpeedActualSignal;
	eCAT_InputData_SortingList(0)(96)=g_AO_DynoTorqueActualSignal;
	eCAT_InputData_SortingList(0)(97)=g_AO_DynoControlSignal;
	eCAT_InputData_SortingList(0)(98)=g_AO_CWTEMP_SV;
	eCAT_InputData_SortingList(0)(99)=g_AO_Feedback_Alpha;
	eCAT_InputData_SortingList(0)(100)=g_AO_ThrottleControlSignal;
	eCAT_InputData_SortingList(0)(101)=g_AO_AlphaASignal;
	eCAT_InputData_SortingList(0)(102)=g_AO_AlphaBSignal;
	eCAT_InputData_SortingList(0)(103)=g_AI_Feedback_Torque*1000;
END
ret
!-----------------------------------------------------------------------------------


#8
! *** HMI Interface Program
	
INT BufferProgrmaNumber
	BufferProgrmaNumber = 8
		
! MMI Show Message
DISP "[#%d buffer] program started.", BufferProgrmaNumber;;

! Alarm ID Define (Error Alarm ID = (Buffer No * 10000) + Error No, Error Alarm ID = Error Alarm ID * -1)
! Alarm ID Define (Warning Alarm ID = (Buffer No * 10000) + Warning No, Error Alarm ID = Warning Alarm ID)	

! *** Fixed-time Measure
REAL PeriodicControlTime_Measure_1;
REAL PeriodicControlTime_Measure_2;
REAL PeriodicControlTime_Difference;
REAL PeriodicControlTime_ProgExcution_Interval;

	PeriodicControlTime_Measure_1 = 0;
	PeriodicControlTime_Measure_2 = 0;
	PeriodicControlTime_Difference = 0;
	PeriodicControlTime_ProgExcution_Interval = 10; ! [msec]
	
! *** HMI Variable	
INT index_HmiTagVar_FlashAccess;	
INT index_Hmi_EdtValToStoreValSave;
INT Hmi_EdtValToStoreValSaveCount;

! *** HMI >>> Linear Interpolation List Copy Variable	
INT index_LinearInterpolation_StValAccess;
INT index_LinearInterpolation_List;
INT indexOffset_RefeVal_AppliedVal;
INT Hmi_LinearInterpolationSaveCount;

! *** HMI Save Time-Out Time Measure
    IF (g_Hmi_bs2201_EditSaveTOutSt < 3000)
		g_Hmi_bs2201_EditSaveTOutSt = 10000;
	END
REAL Hmi_SaveTimeOut_Measure_1;
REAL Hmi_SaveTimeOut_Measure_2;
REAL Hmi_SaveTimeOut_Difference;
	
	Hmi_SaveTimeOut_Measure_1 = 0;
	Hmi_SaveTimeOut_Measure_2 = 0;
	Hmi_SaveTimeOut_Difference = 0;

! *** O/P Switch or HMI Screen change control	
INT OpBox_HmiScreenChgCheck_PreviousControlMode
	OpBox_HmiScreenChgCheck_PreviousControlMode = g_CtMo_enum_Default;
INT OpBox_HmiScreenChgCheck_PreviousDriveMode
	OpBox_HmiScreenChgCheck_PreviousDriveMode = g_DrMn_enum_Default;
INT OpBox_EmoSwitch_PreviousStatus;
	OpBox_EmoSwitch_PreviousStatus = 0;
	
INT OpBox_HmiScreenChangeCommand_Input;
INT OpBox_Previous_HmiScreenChangeCommand_Input;
	OpBox_HmiScreenChangeCommand_Input = 0;
	OpBox_Previous_HmiScreenChangeCommand_Input = 0;

! *** Engine Stop LED 
INT OpBox_Previous_EngStopInput;
	OpBox_Previous_EngStopInput = 0;
INT OpBox_StopLED_OnCondition;	
	OpBox_StopLED_OnCondition = 0;

INT OpScrCh_refreshBitIndex_AlarmHistory;
INT OpScrCh_refreshBitIndex_ConfigMenu;
INT OpScrCh_refreshBitIndex_DynoOnOffScreenChg;
	OpScrCh_refreshBitIndex_AlarmHistory = 0;
	OpScrCh_refreshBitIndex_ConfigMenu = 1;
	OpScrCh_refreshBitIndex_DynoOnOffScreenChg = 2;

REAL Hmi_ResetLed_FlashTime;
REAL Hmi_ResetLed_FlashTime_2;
	Hmi_ResetLed_FlashTime = 0;
REAL Hmi_MiscOnLed_FlashTime
	Hmi_MiscOnLed_FlashTime = 0;
! *** Feedback A/D Val Average
REAL Hmi_bs500x_FB_SpeedADValList(1000);
REAL Hmi_bs500x_FB_TorqueADValList(1000);
REAL Hmi_bs500x_FB_CurrentADValList(1000);
INT index_Hmi_bs500x_FB_SpeedADValMaxIndex;
INT index_Hmi_bs500x_FB_TorqueADValMaxIndex;
INT index_Hmi_bs500x_FB_CurrentADValMaxIndex;
	FILL(0, Hmi_bs500x_FB_SpeedADValList);
	FILL(0, Hmi_bs500x_FB_TorqueADValList);
	FILL(0, Hmi_bs500x_FB_CurrentADValList);
	index_Hmi_bs500x_FB_SpeedADValMaxIndex = 999;
	index_Hmi_bs500x_FB_TorqueADValMaxIndex = 999;
	index_Hmi_bs500x_FB_CurrentADValMaxIndex = 999;

! *** HMI Banner Display Auto Routine 
REAL Hmi_BannerDisplay_AutoR_StartOnTime;
	Hmi_BannerDisplay_AutoR_StartOnTime = 0;

	CALL sr_HMI_Variable_Read_fromFlashMemory;
	
	! *** Program Resule Update
	g_bufferProgramResult(0)(BufferProgrmaNumber) = 100;	
	
		WHILE (1)
		
			BLOCK 
		
				PeriodicControlTime_Measure_1 = TIME
							
					IF((PeriodicControlTime_Measure_1 - PeriodicControlTime_Measure_2) >= PeriodicControlTime_ProgExcution_Interval)					
				
						! *** measure the pid execution time.
						PeriodicControlTime_Difference = PeriodicControlTime_Measure_1 - PeriodicControlTime_Measure_2
						PeriodicControlTime_Measure_2 = PeriodicControlTime_Measure_1	
						
						! ----------------------------------------------------------------------------------------------

						!sr_OpPannel_HmiScreenChangeProcess:
							
							! *** Input Cmd  Refresh
							OpBox_HmiScreenChangeCommand_Input = 0;	
							
							OpBox_HmiScreenChangeCommand_Input.(OpScrCh_refreshBitIndex_AlarmHistory) = g_HMI_DI_AlarmHistory_Button;
							OpBox_HmiScreenChangeCommand_Input.(OpScrCh_refreshBitIndex_ConfigMenu) = g_HMI_DI_ConfigMenu_Button;
							OpBox_HmiScreenChangeCommand_Input.(OpScrCh_refreshBitIndex_DynoOnOffScreenChg) = g_SystemState_Dyno_On;
							
							! ----------------------------------------------------------------------------------------------

							IF( OpBox_HmiScreenChangeCommand_Input <> OpBox_Previous_HmiScreenChangeCommand_Input)
								
								OpBox_Previous_HmiScreenChangeCommand_Input = OpBox_HmiScreenChangeCommand_Input;
								
								IF(OpBox_HmiScreenChangeCommand_Input <> 0)
									IF(g_ControlMode <> g_CtMo_enum_Manual)			
										g_Hmi_Banner_MsgLineNum = 6; g_Hmi_Banner_Enable = 1; WAIT(g_Hmi_BannerSetAndResetInterval);g_Hmi_Banner_MsgLineNum = 0; g_Hmi_Banner_Enable = 0;					
									ELSE
										IF(g_DriveManual_Mode <> g_DrMn_enum_Default)							
											g_Hmi_Banner_MsgLineNum = 27; g_Hmi_Banner_Enable = 1; WAIT(g_Hmi_BannerSetAndResetInterval);g_Hmi_Banner_MsgLineNum = 0; g_Hmi_Banner_Enable = 0;					
										ELSE
											IF((OpBox_HmiScreenChangeCommand_Input.(OpScrCh_refreshBitIndex_AlarmHistory) = 1) & ((OpBox_HmiScreenChangeCommand_Input - POW(2,OpScrCh_refreshBitIndex_AlarmHistory)) = 0))
												
												g_Hmi_Sys_ChangeToScreed = 7001; ! Alarm History Screen
												
												! *** MMI Show Message							
												DISP "[#%d buffer] Op Screen Change - [cmp]Open Alarm History Screen.", BufferProgrmaNumber;				
											END !IF((OpBox_HmiScreenChangeCommand_Input.(OpScrCh_refreshBitIndex_AlarmHistory) = 1) & ((OpBox_HmiScreenChangeCommand_Input - POW(2,OpScrCh_refreshBitIndex_AlarmHistory)) = 0))

											! ----------------------------------------------------------------------------------------------

											IF((OpBox_HmiScreenChangeCommand_Input.(OpScrCh_refreshBitIndex_ConfigMenu) = 1) & ((OpBox_HmiScreenChangeCommand_Input - POW(2,OpScrCh_refreshBitIndex_ConfigMenu)) = 0))
												
												g_Hmi_Sys_ChangeToScreed = 1000; ! Config Initial Screen
												
												! *** MMI Show Message							
												DISP "[#%d buffer] Op Screen Change - [cmp]Open Alarm History Screen.", BufferProgrmaNumber;				
											END !IF((OpBox_HmiScreenChangeCommand_Input.(OpScrCh_refreshBitIndex_AlarmHistory) = 1) & ((OpBox_HmiScreenChangeCommand_Input - POW(2,OpScrCh_refreshBitIndex_AlarmHistory)) = 0))
											
											IF((OpBox_HmiScreenChangeCommand_Input.(OpScrCh_refreshBitIndex_DynoOnOffScreenChg) = 1) & ((OpBox_HmiScreenChangeCommand_Input - POW(2,OpScrCh_refreshBitIndex_DynoOnOffScreenChg)) = 0))
												
												g_Hmi_Sys_ChangeToScreed = 6101; ! Config Initial Screen
												!g_Hmi_Sys_ChangeToScreed = 106; ! Config Initial Screen
												
												! *** MMI Show Message							
												DISP "[#%d buffer] Op Screen Change - [cmp]Open Dyno. On Monitoring Screen.", BufferProgrmaNumber;				
											END !IF((OpBox_HmiScreenChangeCommand_Input.(OpScrCh_refreshBitIndex_AlarmHistory) = 1) & ((OpBox_HmiScreenChangeCommand_Input - POW(2,OpScrCh_refreshBitIndex_AlarmHistory)) = 0))
											
										END ! IF((g_DriveManual_Mode = g_DrMn_enum_Default) & (g_ControlMode <> g_CtMo_enum_DynoService))
									END ! IF(OpBox_HmiScreenChangeCommand_Input <> 0)
									END ! IF(g_ControlMode <> g_CtMo_enum_Manual) 	
							END ! IF( OpBox_HmiScreenChangeCommand_Input <> OpBox_Previous_HmiScreenChangeCommand_Input)
						!RET

						! ----------------------------------------------------------------------------------------------

						!sr_OpPannel_HmiAndLEDChangeProcess:	
								
							! *** System State (Dyno) LED Update 
							IF((g_Error_Exists_Bit = 1) & (g_SystemState_Reset <> 1))
							 
								IF((TIME - Hmi_ResetLed_FlashTime) > 2000)
								!IF(Hmi_ResetLed_FlashTime > 20)		
									IF(g_HMI_DO_AlarmReset_Lamp = 0) 
										g_HMI_DO_AlarmReset_Lamp = 1;
									ELSE
										g_HMI_DO_AlarmReset_Lamp = 0;
									END
									Hmi_ResetLed_FlashTime = 0;
									Hmi_ResetLed_FlashTime = TIME;
								END
								!Hmi_ResetLed_FlashTime = Hmi_ResetLed_FlashTime + 1
							ELSE
								g_HMI_DO_AlarmReset_Lamp = g_SystemState_Reset;
							END
							! *** System State (Dyno) LED Update 
							g_HMI_DO_DynoOn_Lamp = g_SystemState_Dyno_On;	
							
							! *** System State (Engine) LED Update 
							g_HMI_DO_EngIGOn_Lamp = g_SystemState_Eng_IG_On;
							g_HMI_DO_EngPreheatOn_Lamp = g_SystemState_Eng_Preheat_On; 
							g_HMI_DO_EngStarting_Lamp = g_SystemState_Eng_Starting;							
							!g_HMI_DO_Stop_Lamp = g_SystemState_Misc_On;
							IF (^g_Flashing_Misc_On)
								g_HMI_DO_MiscOn_Lamp = g_SystemState_Misc_On;
							ELSE
								IF((TIME - Hmi_MiscOnLed_FlashTime) > 1000)
								!IF(Hmi_ResetLed_FlashTime > 20)		
									IF(g_HMI_DO_MiscOn_Lamp = 0) 
										g_HMI_DO_MiscOn_Lamp = 1;
									ELSE
										g_HMI_DO_MiscOn_Lamp = 0;
									END
									Hmi_MiscOnLed_FlashTime = TIME;
								END
							END
							!g_eCAT_Output_Data(0)(0x21).(2) = g_eCAT_Input_Data(0)(0x01).(2);
							IF((g_HMI_DI_Stop_Button = 1) & (g_HMI_DI_Stop_Button <> OpBox_Previous_EngStopInput))
								OpBox_StopLED_OnCondition = 1;
							END ! IF((g_eCAT_Input_Data(0)(0x01).(2) = 1) & (g_eCAT_Input_Data(0)(0x01).(2) <> OpBox_Previous_EngStopInput)							
							IF(g_Feedback_Speed < g_Hmi_bs3002_Eng_StartCompSpeedSt)
								OpBox_StopLED_OnCondition = 0;
							END ! IF(g_Feedback_Speed < g_Hmi_bs3004_Eng_StartCompSpeedSt)							
							g_HMI_DO_Stop_Lamp = OpBox_StopLED_OnCondition;							
							OpBox_Previous_EngStopInput = g_HMI_DI_Stop_Button;
							
							! *** PANEL ACTIVE LAMP
							g_HMI_DO_PanelActive_Lamp = 1;
							
							! *** HMI Screen Change Switch LED LAMP
							g_HMI_DO_AlarmHistory_Lamp = g_HMI_DI_AlarmHistory_Button!OpBox_HmiScreenChangeCommand_Input.(OpScrCh_refreshBitIndex_AlarmHistory);
							g_HMI_DO_ConfigMenu_Lamp = g_HMI_DI_ConfigMenu_Button!OpBox_HmiScreenChangeCommand_Input.(OpScrCh_refreshBitIndex_ConfigMenu);
							
							
							IF((OpBox_HmiScreenChgCheck_PreviousControlMode <> g_ControlMode) | (OpBox_HmiScreenChgCheck_PreviousDriveMode <> g_DriveManual_Mode))
							
								OpBox_HmiScreenChgCheck_PreviousControlMode = g_ControlMode;
								OpBox_HmiScreenChgCheck_PreviousDriveMode = g_DriveManual_Mode;
								! *** Control Mode LED Update
								IF(g_ControlMode <> g_CtMo_enum_Default)	
									
									IF(g_ControlMode = g_CtMo_enum_Monitor)
										g_HMI_DO_Monitor_Lamp = 1; ! Monitor
										g_HMI_DO_Automatic_Lamp = 0; ! Automatic
										g_HMI_DO_DynoService_Lamp = 0; ! Dyno Service
										g_HMI_DO_Manual_Lamp = 0; ! Manual
										g_HMI_DO_Remote_Lamp = 0; ! Remote

										g_Hmi_Sys_ChangeToScreed = 6101; ! Monitoring Screen
									ELSEIF(g_ControlMode = g_CtMo_enum_Automatic)
										g_HMI_DO_Monitor_Lamp = 0; ! Monitor
										g_HMI_DO_Automatic_Lamp = 1; ! Automatic
										g_HMI_DO_DynoService_Lamp = 0; ! Dyno Service
										g_HMI_DO_Manual_Lamp = 0; ! Manual
										g_HMI_DO_Remote_Lamp = 0; ! Remote
										
										g_Hmi_Sys_ChangeToScreed = 200; ! Automatic Screen
									ELSEIF(g_ControlMode = g_CtMo_enum_OpenLoopTest)
										g_HMI_DO_Monitor_Lamp = 0; ! Monitor
										g_HMI_DO_Automatic_Lamp = 0; ! Automatic
										g_HMI_DO_DynoService_Lamp = 1; ! Dyno Service
										g_HMI_DO_Manual_Lamp = 0; ! Manual
										g_HMI_DO_Remote_Lamp = 0; ! Remote
										
										g_Hmi_Sys_ChangeToScreed = 110; ! Dyno Service Screen
									ELSEIF(g_ControlMode = g_CtMo_enum_Manual)
										g_HMI_DO_Monitor_Lamp = 0; ! Monitor
										g_HMI_DO_Automatic_Lamp = 0; ! Automatic
										g_HMI_DO_DynoService_Lamp = 0; ! Dyno Service
										g_HMI_DO_Manual_Lamp = 1; ! Manual
										g_HMI_DO_Remote_Lamp = 0; ! Remote
									ELSEIF(g_ControlMode = g_CtMo_enum_Remote)
										g_HMI_DO_Monitor_Lamp = 0; ! Monitor
										g_HMI_DO_Automatic_Lamp = 0; ! Automatic
										g_HMI_DO_DynoService_Lamp = 0; ! Dyno Service
										g_HMI_DO_Manual_Lamp = 0; ! Manual
										g_HMI_DO_Remote_Lamp = 1; ! Remote
									END
								ELSE
										g_HMI_DO_Monitor_Lamp = 0; ! Monitor
										g_HMI_DO_Automatic_Lamp = 0; ! Automatic
										g_HMI_DO_DynoService_Lamp = 0; ! Dyno Service
										g_HMI_DO_Manual_Lamp = 0; ! Manual
										g_HMI_DO_Remote_Lamp = 0; ! Remote
								END ! IF(g_ControlMode <> g_CtMo_enum_Default)
								
								! *** Drive Mode LED Update
								IF((g_ControlMode = g_CtMo_enum_Manual) | (g_ControlMode = g_CtMo_enum_Automatic) | (g_ControlMode = g_CtMo_enum_Remote))
									IF(g_DriveManual_Mode = g_DrMn_enum_Idle)				
										g_HMI_DO_Idle_Lamp = 1; ! Idle
										g_HMI_DO_IdleControl_Lamp = 0; ! Idle control
										g_HMI_DO_DyTo_EngTA_Lamp = 0; ! T/Alpha
										g_HMI_DO_DyRPM_EngTA_Lamp = 0; ! N/Alpha
										g_HMI_DO_DyTo_EngRPM_Lamp = 0; ! T/N
										g_HMI_DO_DyRPM_EngTo_Lamp = 0; ! N/T

										IF(g_ControlMode = g_CtMo_enum_Automatic)
											g_Hmi_Sys_ChangeToScreed = 200; ! Idle Screen
										ELSE
											g_Hmi_Sys_ChangeToScreed = 100; ! Idle Screen
										END! IF(g_ControlMode = g_CtMo_enum_Automatic)
									ELSEIF(g_DriveManual_Mode = g_DrMn_enum_IdleContrl)
										
										g_HMI_DO_Idle_Lamp = 0; ! Idle
										g_HMI_DO_IdleControl_Lamp = 1; ! Idle control
										g_HMI_DO_DyTo_EngTA_Lamp = 0; ! T/Alpha
										g_HMI_DO_DyRPM_EngTA_Lamp = 0; ! N/Alpha
										g_HMI_DO_DyTo_EngRPM_Lamp = 0; ! T/N
										g_HMI_DO_DyRPM_EngTo_Lamp = 0; ! N/T
										
										IF(g_ControlMode = g_CtMo_enum_Automatic)
											g_Hmi_Sys_ChangeToScreed = 200; ! Idle Screen
										ELSE
											g_Hmi_Sys_ChangeToScreed = 101; ! Idle Control Screen
										END! IF(g_ControlMode = g_CtMo_enum_Automatic)
									ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyTo_EngTA)

										g_HMI_DO_Idle_Lamp = 0; ! Idle
										g_HMI_DO_IdleControl_Lamp = 0; ! Idle control
										g_HMI_DO_DyTo_EngTA_Lamp = 1; ! T/Alpha
										g_HMI_DO_DyRPM_EngTA_Lamp = 0; ! N/Alpha
										g_HMI_DO_DyTo_EngRPM_Lamp = 0; ! T/N
										g_HMI_DO_DyRPM_EngTo_Lamp = 0; ! N/T
										
										IF(g_ControlMode = g_CtMo_enum_Automatic)
											g_Hmi_Sys_ChangeToScreed = 200; ! Idle Screen
										ELSE
											g_Hmi_Sys_ChangeToScreed = 102; ! T/A Screen
										END! IF(g_ControlMode = g_CtMo_enum_Automatic)
									ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTA)

										g_HMI_DO_Idle_Lamp = 0; ! Idle
										g_HMI_DO_IdleControl_Lamp = 0; ! Idle control
										g_HMI_DO_DyTo_EngTA_Lamp = 0; ! T/Alpha
										g_HMI_DO_DyRPM_EngTA_Lamp = 1; ! N/Alpha
										g_HMI_DO_DyTo_EngRPM_Lamp = 0; ! T/N
										g_HMI_DO_DyRPM_EngTo_Lamp = 0; ! N/T
										
										IF(g_ControlMode = g_CtMo_enum_Automatic)
											g_Hmi_Sys_ChangeToScreed = 200; ! Idle Screen
										ELSE
											g_Hmi_Sys_ChangeToScreed = 103; ! N/A Screen
										END! IF(g_ControlMode = g_CtMo_enum_Automatic)
									ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyTo_EngRPM)

										g_HMI_DO_Idle_Lamp = 0; ! Idle
										g_HMI_DO_IdleControl_Lamp = 0; ! Idle control
										g_HMI_DO_DyTo_EngTA_Lamp = 0; ! T/Alpha
										g_HMI_DO_DyRPM_EngTA_Lamp = 0; ! N/Alpha
										g_HMI_DO_DyTo_EngRPM_Lamp = 1; ! T/N
										g_HMI_DO_DyRPM_EngTo_Lamp = 0; ! N/T

										IF(g_ControlMode = g_CtMo_enum_Automatic)
											g_Hmi_Sys_ChangeToScreed = 200; ! Idle Screen
										ELSE
											g_Hmi_Sys_ChangeToScreed = 105; ! S/T Screen
										END! IF(g_ControlMode = g_CtMo_enum_Automatic)
									ELSEIF(g_DriveManual_Mode = g_DrMn_enum_DyRPM_EngTo	)
										g_HMI_DO_Idle_Lamp = 0; ! Idle
										g_HMI_DO_IdleControl_Lamp = 0; ! Idle control
										g_HMI_DO_DyTo_EngTA_Lamp = 0; ! T/Alpha
										g_HMI_DO_DyRPM_EngTA_Lamp = 0; ! N/Alpha
										g_HMI_DO_DyTo_EngRPM_Lamp = 0; ! T/N
										g_HMI_DO_DyRPM_EngTo_Lamp = 1; ! N/T
										
										IF(g_ControlMode = g_CtMo_enum_Automatic)
											g_Hmi_Sys_ChangeToScreed = 200; ! Idle Screen
										ELSE
											g_Hmi_Sys_ChangeToScreed = 104; ! T/S Screen
										END! IF(g_ControlMode = g_CtMo_enum_Automatic)
									
									ELSE
										g_HMI_DO_Idle_Lamp = 0; ! Idle
										g_HMI_DO_IdleControl_Lamp = 0; ! Idle control
										g_HMI_DO_DyTo_EngTA_Lamp = 0; ! T/Alpha
										g_HMI_DO_DyRPM_EngTA_Lamp = 0; ! N/Alpha
										g_HMI_DO_DyTo_EngRPM_Lamp = 0; ! T/N
										g_HMI_DO_DyRPM_EngTo_Lamp = 0; ! N/T
										
										IF(g_SystemState_Dyno_On = 1)
											g_Hmi_Sys_ChangeToScreed = 6101; ! Dyno. On Feedback Monitoring Screen
										ELSE										
											g_Hmi_Sys_ChangeToScreed = 6101; ! Monitoring Screen
										END
									
									END !IF(g_DriveManual_Mode = g_DrMn_enum_Idle)
								ELSE
										g_HMI_DO_Idle_Lamp = 0; ! Idle
										g_HMI_DO_IdleControl_Lamp = 0; ! Idle control
										g_HMI_DO_DyTo_EngTA_Lamp = 0; ! T/Alpha
										g_HMI_DO_DyRPM_EngTA_Lamp = 0; ! N/Alpha
										g_HMI_DO_DyTo_EngRPM_Lamp = 0; ! T/N
										g_HMI_DO_DyRPM_EngTo_Lamp = 0; ! N/T
									
								END !IF(g_ControlMode = g_CtMo_enum_Manual)
								
							END !IF(OpBox_HmiScreenChgCheck_PreviousControlMode = g_ControlMode)
						!RET

						! ----------------------------------------------------------------------------------------------

						!sr_DynoControl_OnOff_Command_DigiIO_Processing:
							
							IF(g_SystemState_Dyno_On = 1)
								! *** CTRL On CMD >> Power unit LSE 513
								!g_eCAT_Output_Data(0)(0x20).2 = 1;	
																
							ELSE
								! *** CTRL On CMD >> Power unit LSE 513
								!g_eCAT_Output_Data(0)(0x20).2 = 0;	
							END
						!RET

						! ----------------------------------------------------------------------------------------------
						!sr_BS220x_HMI_Sys_OpBoxConfig_SaveProcess:
							
							! *** Base Screen 220x Data Edit Time-out & Save 
							IF (g_Hmi_bs220x_CtrlVal.0)
								! *** Step 1
								Hmi_SaveTimeOut_Measure_1 = TIME;
								g_Hmi_bs220x_CtrlVal.0 = 0;
								g_Hmi_bs220x_CtrlVal.1 = 1;
								
							ELSEIF(g_Hmi_bs220x_CtrlVal.1)
								! *** Step 2
								Hmi_SaveTimeOut_Measure_2 = TIME;		
								Hmi_SaveTimeOut_Difference = Hmi_SaveTimeOut_Measure_2 - Hmi_SaveTimeOut_Measure_1;
								
								! *** Step 3 Save Time-out & Screen Change Check
								IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs2201_EditSaveTOutSt) | (gHmi_Sys_CurrentScreen < 2201) | (gHmi_Sys_CurrentScreen > 2203))
									
									CALL sr_HMI_Variable_Read_fromFlashMemory
									
									Hmi_SaveTimeOut_Measure_1 = 0;
									Hmi_SaveTimeOut_Measure_2 = 0;
									Hmi_SaveTimeOut_Difference = 0;
									
									g_Hmi_bs220x_CtrlVal = 0;
								
								END !IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs2201_EditSaveTOut) | (gHmi_Sys_CurrentScreen < 1000) | (gHmi_Sys_CurrentScreen > 1000))
								
							END !IF (g_Hmi_bs220x_CtrlVal.0)
								
							! ----------------------------------------------------------------------------------------------	
							
							! *** HMI Save Command 
							IF (g_Hmi_bs220x_CtrlVal.15)	
								
								index_Hmi_EdtValToStoreValSave = 1141;
								Hmi_EdtValToStoreValSaveCount = 10;
								
								LOOP Hmi_EdtValToStoreValSaveCount
									SETVAR(GETVAR(index_Hmi_EdtValToStoreValSave), (index_Hmi_EdtValToStoreValSave + 1));
									index_Hmi_EdtValToStoreValSave = index_Hmi_EdtValToStoreValSave + 2;
								END
								
								CALL sr_HMI_Variable_Write_toFlashMemory;		
								CALL sr_HMI_Variable_Read_fromFlashMemory
								
								g_Hmi_bs220x_CtrlVal = 0;
								
							END ! IF(g_Hmi_bs220x_CtrlVal.15)		
							
						!RET
						
						! ----------------------------------------------------------------------------------------------

						!sr_BS230x_HMI_DAQConfig_SaveProcess:
							
							! *** Base Screen 230x Data Edit Time-out & Save 
							IF (g_Hmi_bs230x_CtrlVal.0)
								! *** Step 1
								Hmi_SaveTimeOut_Measure_1 = TIME;
								g_Hmi_bs230x_CtrlVal.0 = 0;
								g_Hmi_bs230x_CtrlVal.1 = 1;
								
							ELSEIF(g_Hmi_bs230x_CtrlVal.1)
								! *** Step 2
								Hmi_SaveTimeOut_Measure_2 = TIME;		
								Hmi_SaveTimeOut_Difference = Hmi_SaveTimeOut_Measure_2 - Hmi_SaveTimeOut_Measure_1;
								
								! *** Step 3 Save Time-out & Screen Change Check
								IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs2201_EditSaveTOutSt) | (gHmi_Sys_CurrentScreen < 2301) | (gHmi_Sys_CurrentScreen > 2303))	
									CALL sr_HMI_Variable_Read_fromFlashMemory
									
									Hmi_SaveTimeOut_Measure_1 = 0;
									Hmi_SaveTimeOut_Measure_2 = 0;
									Hmi_SaveTimeOut_Difference = 0;
									
									g_Hmi_bs230x_CtrlVal = 0;
								
								END !IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs2201_EditSaveTOut) | (gHmi_Sys_CurrentScreen < 1000) | (gHmi_Sys_CurrentScreen > 1000))
								
							END !IF (g_Hmi_bs230x_CtrlVal.0)
								
							! ----------------------------------------------------------------------------------------------	
							
							! *** HMI Save Command 
							IF (g_Hmi_bs230x_CtrlVal.15)	
								
	
								index_Hmi_EdtValToStoreValSave = 1171;
								Hmi_EdtValToStoreValSaveCount = 14;
								
								LOOP Hmi_EdtValToStoreValSaveCount
									SETVAR(GETVAR(index_Hmi_EdtValToStoreValSave), (index_Hmi_EdtValToStoreValSave + 1));
									index_Hmi_EdtValToStoreValSave = index_Hmi_EdtValToStoreValSave + 2;
								END
								CALL sr_HMI_Variable_Write_toFlashMemory;		
								CALL sr_HMI_Variable_Read_fromFlashMemory
								
								g_Hmi_bs230x_CtrlVal = 0;
								
							END ! IF(g_Hmi_bs230x_CtrlVal.15)		
							
						!RET

						! ----------------------------------------------------------------------------------------------

						!sr_BS300x_HMI_EngineConfig_SaveProcess:
							
							! *** Base Screen 300x Data Edit Time-out & Save 
							IF (g_Hmi_bs300x_CtrlVal.0)
								! *** Step 1
								Hmi_SaveTimeOut_Measure_1 = TIME;
								g_Hmi_bs300x_CtrlVal.0 = 0;
								g_Hmi_bs300x_CtrlVal.1 = 1;
								
							ELSEIF(g_Hmi_bs300x_CtrlVal.1)
								! *** Step 2
								Hmi_SaveTimeOut_Measure_2 = TIME;		
								Hmi_SaveTimeOut_Difference = Hmi_SaveTimeOut_Measure_2 - Hmi_SaveTimeOut_Measure_1;
								
								! *** Step 3 Save Time-out & Screen Change Check
								IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs2201_EditSaveTOutSt) | (gHmi_Sys_CurrentScreen < 3001) | (gHmi_Sys_CurrentScreen > 3008))
									
									CALL sr_HMI_Variable_Read_fromFlashMemory
									
									Hmi_SaveTimeOut_Measure_1 = 0;
									Hmi_SaveTimeOut_Measure_2 = 0;
									Hmi_SaveTimeOut_Difference = 0;
									
									g_Hmi_bs300x_CtrlVal = 0;
								
								END !IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs2201_EditSaveTOut) | (gHmi_Sys_CurrentScreen < 1000) | (gHmi_Sys_CurrentScreen > 1000))
								
							END !IF (g_Hmi_bs300x_CtrlVal.0)
								
							! ----------------------------------------------------------------------------------------------	
							
							! *** HMI Save Command 
							IF (g_Hmi_bs300x_CtrlVal.15)	
								
								index_Hmi_EdtValToStoreValSave = 1201;
								Hmi_EdtValToStoreValSaveCount = 19;
								
								LOOP Hmi_EdtValToStoreValSaveCount
									SETVAR(GETVAR(index_Hmi_EdtValToStoreValSave), (index_Hmi_EdtValToStoreValSave + 1));
									index_Hmi_EdtValToStoreValSave = index_Hmi_EdtValToStoreValSave + 2;
								END
						
								CALL sr_HMI_Variable_Write_toFlashMemory;		
								CALL sr_HMI_Variable_Read_fromFlashMemory
								
								g_Hmi_bs300x_CtrlVal = 0;
								
							END ! IF(g_Hmi_bs300x_CtrlVal.15)		
							
						!RET

						! ----------------------------------------------------------------------------------------------

						!sr_BS400x_HMI_DynoNModePID_SaveProcess:	
							
							! *** Base Screen 400x Data Edit Time-out & Save 
							IF (g_Hmi_bs400x_CtrlVal.0)
								! *** Step 1
								Hmi_SaveTimeOut_Measure_1 = TIME;
								g_Hmi_bs400x_CtrlVal.0 = 0;
								g_Hmi_bs400x_CtrlVal.1 = 1;
								
							ELSEIF(g_Hmi_bs400x_CtrlVal.1)
								! *** Step 2
								Hmi_SaveTimeOut_Measure_2 = TIME;		
								Hmi_SaveTimeOut_Difference = Hmi_SaveTimeOut_Measure_2 - Hmi_SaveTimeOut_Measure_1;
								
								! *** Step 3 Save Time-out & Screen Change Check
								IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs2201_EditSaveTOutSt) | (gHmi_Sys_CurrentScreen < 4001) | (gHmi_Sys_CurrentScreen > 4004))
									
									CALL sr_HMI_Variable_Read_fromFlashMemory
									
									Hmi_SaveTimeOut_Measure_1 = 0;
									Hmi_SaveTimeOut_Measure_2 = 0;
									Hmi_SaveTimeOut_Difference = 0;
									
									g_Hmi_bs400x_CtrlVal = 0;
								
								END !IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs1000_EditSaveTOut) | (gHmi_Sys_CurrentScreen < 1321) | (gHmi_Sys_CurrentScreen > 1321))
								
							END !IF (g_Hmi_bs400x_CtrlVal.0)
							
								
							! *** HMI Save Command (HMI Edit Value -> Store Value Update)
							IF (g_Hmi_bs400x_CtrlVal.15)	
							
								index_Hmi_EdtValToStoreValSave = 1301;
								Hmi_EdtValToStoreValSaveCount = 17;
								
								LOOP Hmi_EdtValToStoreValSaveCount
									SETVAR(GETVAR(index_Hmi_EdtValToStoreValSave), (index_Hmi_EdtValToStoreValSave + 1));
									index_Hmi_EdtValToStoreValSave = index_Hmi_EdtValToStoreValSave + 2;
								END
								
								CALL sr_HMI_Variable_Write_toFlashMemory;		
								CALL sr_HMI_Variable_Read_fromFlashMemory
										
								g_Hmi_bs400x_CtrlVal = 0;
								
							END ! IF (g_Hmi_bs400x_CtrlVal.15)	

						!RET

						! ----------------------------------------------------------------------------------------------

						!sr_BS410x_HMI_DynoTModePID_SaveProcess:	
							
							! *** Base Screen 410x Data Edit Time-out & Save 
							IF (g_Hmi_bs410x_CtrlVal.0)
								! *** Step 1
								Hmi_SaveTimeOut_Measure_1 = TIME;
								g_Hmi_bs410x_CtrlVal.0 = 0;
								g_Hmi_bs410x_CtrlVal.1 = 1;
								
							ELSEIF(g_Hmi_bs410x_CtrlVal.1)
								! *** Step 2
								Hmi_SaveTimeOut_Measure_2 = TIME;		
								Hmi_SaveTimeOut_Difference = Hmi_SaveTimeOut_Measure_2 - Hmi_SaveTimeOut_Measure_1;
								
								! *** Step 3 Save Time-out & Screen Change Check
								IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs2201_EditSaveTOutSt) | (gHmi_Sys_CurrentScreen < 4101) | (gHmi_Sys_CurrentScreen > 4104))
									
									CALL sr_HMI_Variable_Read_fromFlashMemory
									
									Hmi_SaveTimeOut_Measure_1 = 0;
									Hmi_SaveTimeOut_Measure_2 = 0;
									Hmi_SaveTimeOut_Difference = 0;
									
									g_Hmi_bs410x_CtrlVal = 0;
								
								END !IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs1000_EditSaveTOut) | (gHmi_Sys_CurrentScreen < 1321) | (gHmi_Sys_CurrentScreen > 1321))
								
							END !IF (g_Hmi_bs410x_CtrlVal.0)
							
								
							! *** HMI Save Command (HMI Edit Value -> Store Value Update)
							IF (g_Hmi_bs410x_CtrlVal.15)	
							
								index_Hmi_EdtValToStoreValSave = 1341;
								Hmi_EdtValToStoreValSaveCount = 17;
								
								LOOP Hmi_EdtValToStoreValSaveCount
									SETVAR(GETVAR(index_Hmi_EdtValToStoreValSave), (index_Hmi_EdtValToStoreValSave + 1));
									index_Hmi_EdtValToStoreValSave = index_Hmi_EdtValToStoreValSave + 2;
								END
										
								CALL sr_HMI_Variable_Write_toFlashMemory;		
								CALL sr_HMI_Variable_Read_fromFlashMemory
										
								g_Hmi_bs410x_CtrlVal = 0;
								
							END ! IF (g_Hmi_bs410x_CtrlVal.15)	

						!RET

						! ----------------------------------------------------------------------------------------------

						!sr_BS420x_HMI_EngineNModePID_SaveProcess:	
							
							! *** Base Screen 420x Data Edit Time-out & Save 
							IF (g_Hmi_bs420x_CtrlVal.0)
								! *** Step 1
								Hmi_SaveTimeOut_Measure_1 = TIME;
								g_Hmi_bs420x_CtrlVal.0 = 0;
								g_Hmi_bs420x_CtrlVal.1 = 1;
								
							ELSEIF(g_Hmi_bs420x_CtrlVal.1)
								! *** Step 2
								Hmi_SaveTimeOut_Measure_2 = TIME;		
								Hmi_SaveTimeOut_Difference = Hmi_SaveTimeOut_Measure_2 - Hmi_SaveTimeOut_Measure_1;
								
								! *** Step 3 Save Time-out & Screen Change Check
								IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs2201_EditSaveTOutSt) | (gHmi_Sys_CurrentScreen < 4201) | (gHmi_Sys_CurrentScreen > 4208))
									
									CALL sr_HMI_Variable_Read_fromFlashMemory
									
									Hmi_SaveTimeOut_Measure_1 = 0;
									Hmi_SaveTimeOut_Measure_2 = 0;
									Hmi_SaveTimeOut_Difference = 0;
									
									g_Hmi_bs420x_CtrlVal = 0;
								
								END !IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs1000_EditSaveTOut) | (gHmi_Sys_CurrentScreen < 1321) | (gHmi_Sys_CurrentScreen > 1321))
								
							END !IF (g_Hmi_bs420x_CtrlVal.0)
							
								
							! *** HMI Save Command (HMI Edit Value -> Store Value Update)
							IF (g_Hmi_bs420x_CtrlVal.15)	
							
								index_Hmi_EdtValToStoreValSave = 1381;
								Hmi_EdtValToStoreValSaveCount = 19;
								
								LOOP Hmi_EdtValToStoreValSaveCount
									SETVAR(GETVAR(index_Hmi_EdtValToStoreValSave), (index_Hmi_EdtValToStoreValSave + 1));
									index_Hmi_EdtValToStoreValSave = index_Hmi_EdtValToStoreValSave + 2;
								END
										
								CALL sr_HMI_Variable_Write_toFlashMemory;		
								CALL sr_HMI_Variable_Read_fromFlashMemory
										
								g_Hmi_bs420x_CtrlVal = 0;
								
							END ! IF (g_Hmi_bs420x_CtrlVal.15)	

						!RET

						! ----------------------------------------------------------------------------------------------

						!sr_BS430x_HMI_EngineTModePID_SaveProcess:	
							
							! *** Base Screen 430x Data Edit Time-out & Save 
							IF (g_Hmi_bs430x_CtrlVal.0)
								! *** Step 1
								Hmi_SaveTimeOut_Measure_1 = TIME;
								g_Hmi_bs430x_CtrlVal.0 = 0;
								g_Hmi_bs430x_CtrlVal.1 = 1;
								
							ELSEIF(g_Hmi_bs430x_CtrlVal.1)
								! *** Step 2
								Hmi_SaveTimeOut_Measure_2 = TIME;		
								Hmi_SaveTimeOut_Difference = Hmi_SaveTimeOut_Measure_2 - Hmi_SaveTimeOut_Measure_1;
								
								! *** Step 3 Save Time-out & Screen Change Check
								IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs2201_EditSaveTOutSt) | (gHmi_Sys_CurrentScreen < 4301) | (gHmi_Sys_CurrentScreen > 4307))
									
									CALL sr_HMI_Variable_Read_fromFlashMemory
									
									Hmi_SaveTimeOut_Measure_1 = 0;
									Hmi_SaveTimeOut_Measure_2 = 0;
									Hmi_SaveTimeOut_Difference = 0;
									
									g_Hmi_bs430x_CtrlVal = 0;
								
								END !IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs1000_EditSaveTOut) | (gHmi_Sys_CurrentScreen < 1321) | (gHmi_Sys_CurrentScreen > 1321))
								
							END !IF (g_Hmi_bs430x_CtrlVal.0)
							
								
							! *** HMI Save Command (HMI Edit Value -> Store Value Update)
							IF (g_Hmi_bs430x_CtrlVal.15)	
							
								index_Hmi_EdtValToStoreValSave = 1421;
								Hmi_EdtValToStoreValSaveCount = 17;
								
								LOOP Hmi_EdtValToStoreValSaveCount
									SETVAR(GETVAR(index_Hmi_EdtValToStoreValSave), (index_Hmi_EdtValToStoreValSave + 1));
									index_Hmi_EdtValToStoreValSave = index_Hmi_EdtValToStoreValSave + 2;
								END
										
								CALL sr_HMI_Variable_Write_toFlashMemory;		
								CALL sr_HMI_Variable_Read_fromFlashMemory
										
								g_Hmi_bs430x_CtrlVal = 0;
								
							END ! IF (g_Hmi_bs143x_CtrlVal.15)	

						!RET

						! ----------------------------------------------------------------------------------------------

						!sr_BS440x_HMI_EngineAModePID_SaveProcess:	
							
							! *** Base Screen 440x Data Edit Time-out & Save 
							IF (g_Hmi_bs440x_CtrlVal.0)
								! *** Step 1
								Hmi_SaveTimeOut_Measure_1 = TIME;
								g_Hmi_bs440x_CtrlVal.0 = 0;
								g_Hmi_bs440x_CtrlVal.1 = 1;
								
							ELSEIF(g_Hmi_bs440x_CtrlVal.1)
								! *** Step 2
								Hmi_SaveTimeOut_Measure_2 = TIME;		
								Hmi_SaveTimeOut_Difference = Hmi_SaveTimeOut_Measure_2 - Hmi_SaveTimeOut_Measure_1;
								
								! *** Step 3 Save Time-out & Screen Change Check
								IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs2201_EditSaveTOutSt) | (gHmi_Sys_CurrentScreen < 4401) | (gHmi_Sys_CurrentScreen > 4401))
									
									CALL sr_HMI_Variable_Read_fromFlashMemory
									
									Hmi_SaveTimeOut_Measure_1 = 0;
									Hmi_SaveTimeOut_Measure_2 = 0;
									Hmi_SaveTimeOut_Difference = 0;
									
									g_Hmi_bs440x_CtrlVal = 0;
								
								END !IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs1000_EditSaveTOut) | (gHmi_Sys_CurrentScreen < 1321) | (gHmi_Sys_CurrentScreen > 1321))
								
							END !IF (g_Hmi_bs440x_CtrlVal.0)
							
								
							! *** HMI Save Command (HMI Edit Value -> Store Value Update)
							IF (g_Hmi_bs440x_CtrlVal.15)	
							
								index_Hmi_EdtValToStoreValSave = 1461;
								Hmi_EdtValToStoreValSaveCount = 17;
								
								LOOP Hmi_EdtValToStoreValSaveCount
									SETVAR(GETVAR(index_Hmi_EdtValToStoreValSave), (index_Hmi_EdtValToStoreValSave + 1));
									index_Hmi_EdtValToStoreValSave = index_Hmi_EdtValToStoreValSave + 2;
								END
								
								CALL sr_HMI_Variable_Write_toFlashMemory;		
								CALL sr_HMI_Variable_Read_fromFlashMemory
										
								g_Hmi_bs440x_CtrlVal = 0;
								
							END ! IF (g_Hmi_bs440x_CtrlVal.15)	

						!RET

						! ----------------------------------------------------------------------------------------------

						!sr_BS500x_HMI_Calibration_SpeedFb_SaveProcess:	
							
							! *** Base Screen 500x Data Edit Time-out & Save 
							IF (g_Hmi_bs500x_CtrlVal.0)
								! *** Step 1
								Hmi_SaveTimeOut_Measure_1 = TIME;
								g_Hmi_bs500x_CtrlVal.0 = 0;
								g_Hmi_bs500x_CtrlVal.1 = 1;
								
							ELSEIF(g_Hmi_bs500x_CtrlVal.1)
								! *** Step 2
								Hmi_SaveTimeOut_Measure_2 = TIME;		
								Hmi_SaveTimeOut_Difference = Hmi_SaveTimeOut_Measure_2 - Hmi_SaveTimeOut_Measure_1;
								
								! *** Step 3 Save Time-out & Screen Change Check
								IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs2201_EditSaveTOutSt) | (gHmi_Sys_CurrentScreen < 5001) | (gHmi_Sys_CurrentScreen > 5004))
									
									CALL sr_HMI_Variable_Read_fromFlashMemory
									
									Hmi_SaveTimeOut_Measure_1 = 0;
									Hmi_SaveTimeOut_Measure_2 = 0;
									Hmi_SaveTimeOut_Difference = 0;
									
									g_Hmi_bs500x_CtrlVal = 0;
								
								END !IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs1000_EditSaveTOut) | (gHmi_Sys_CurrentScreen < 1321) | (gHmi_Sys_CurrentScreen > 1321))
								
							END !IF (g_Hmi_bs500x_CtrlVal.0)
							
								
							! *** HMI Save Command (HMI Edit Value -> Store Value Update)
							IF (g_Hmi_bs500x_CtrlVal.15)	
							
								index_Hmi_EdtValToStoreValSave = 1501;
								Hmi_EdtValToStoreValSaveCount = 34;
								
								LOOP Hmi_EdtValToStoreValSaveCount
									SETVAR(GETVAR(index_Hmi_EdtValToStoreValSave), (index_Hmi_EdtValToStoreValSave + 1));
									index_Hmi_EdtValToStoreValSave = index_Hmi_EdtValToStoreValSave + 2;
								END
								
								CALL sr_HMI_Variable_Write_toFlashMemory;		
								CALL sr_HMI_Variable_Read_fromFlashMemory
								
								! *** Linear Interporation
								index_LinearInterpolation_StValAccess = 1522;	
								Hmi_LinearInterpolationSaveCount = 11;
								index_LinearInterpolation_List = 0;
								indexOffset_RefeVal_AppliedVal = 24;
								
								LOOP Hmi_LinearInterpolationSaveCount 
									g_lInter_fbSpeed_RefeVal_List(index_LinearInterpolation_List) = GETVAR(index_LinearInterpolation_StValAccess);
									g_lInter_fbSpeed_AppliedVal_List(index_LinearInterpolation_List) = GETVAR((index_LinearInterpolation_StValAccess + indexOffset_RefeVal_AppliedVal));
									
									index_LinearInterpolation_StValAccess = index_LinearInterpolation_StValAccess + 2;
									index_LinearInterpolation_List = index_LinearInterpolation_List + 1;
								END
								
								g_Hmi_bs500x_CtrlVal = 0;
								
							END ! IF (g_Hmi_bs510x_CtrlVal.15)	

						!RET

						! ----------------------------------------------------------------------------------------------

						!sr_BS510x_HMI_Calibration_TorqueFb_SaveProcess:	
							
							! *** Base Screen 510x Data Edit Time-out & Save 
							IF (g_Hmi_bs510x_CtrlVal.0)
								! *** Step 1
								Hmi_SaveTimeOut_Measure_1 = TIME;
								g_Hmi_bs510x_CtrlVal.0 = 0;
								g_Hmi_bs510x_CtrlVal.1 = 1;
								
							ELSEIF(g_Hmi_bs510x_CtrlVal.1)
								! *** Step 2
								Hmi_SaveTimeOut_Measure_2 = TIME;		
								Hmi_SaveTimeOut_Difference = Hmi_SaveTimeOut_Measure_2 - Hmi_SaveTimeOut_Measure_1;
								
								! *** Step 3 Save Time-out & Screen Change Check
								IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs2201_EditSaveTOutSt) | (gHmi_Sys_CurrentScreen < 5101) | (gHmi_Sys_CurrentScreen > 5104))
									
									CALL sr_HMI_Variable_Read_fromFlashMemory
									
									Hmi_SaveTimeOut_Measure_1 = 0;
									Hmi_SaveTimeOut_Measure_2 = 0;
									Hmi_SaveTimeOut_Difference = 0;
									
									g_Hmi_bs510x_CtrlVal = 0;
								
								END !IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs1000_EditSaveTOut) | (gHmi_Sys_CurrentScreen < 1321) | (gHmi_Sys_CurrentScreen > 1321))
								
							END !IF (g_Hmi_bs510x_CtrlVal.0)
							
								
							! *** HMI Save Command (HMI Edit Value -> Store Value Update)
							IF (g_Hmi_bs510x_CtrlVal.15)	
							
								index_Hmi_EdtValToStoreValSave = 1571;
								Hmi_EdtValToStoreValSaveCount = 34;
								
								LOOP Hmi_EdtValToStoreValSaveCount
									SETVAR(GETVAR(index_Hmi_EdtValToStoreValSave), (index_Hmi_EdtValToStoreValSave + 1));
									index_Hmi_EdtValToStoreValSave = index_Hmi_EdtValToStoreValSave + 2;
								END
								
								CALL sr_HMI_Variable_Write_toFlashMemory;		
								CALL sr_HMI_Variable_Read_fromFlashMemory
								
								! *** Linear Interporation
								index_LinearInterpolation_StValAccess = 1592;	
								Hmi_LinearInterpolationSaveCount = 11;
								index_LinearInterpolation_List = 0;
								indexOffset_RefeVal_AppliedVal = 24;
								
								LOOP Hmi_LinearInterpolationSaveCount 
									g_lInter_fbTorque_RefeVal_List(index_LinearInterpolation_List) = GETVAR(index_LinearInterpolation_StValAccess)/10 ;
									g_lInter_fbTorque_AppliedVal_List(index_LinearInterpolation_List) = GETVAR((index_LinearInterpolation_StValAccess + indexOffset_RefeVal_AppliedVal))/ 10;
									
									index_LinearInterpolation_StValAccess = index_LinearInterpolation_StValAccess + 2;
									index_LinearInterpolation_List = index_LinearInterpolation_List + 1;
								END
								
								g_Hmi_bs510x_CtrlVal = 0;
								
							END ! IF (g_Hmi_bs510x_CtrlVal.15)	

						!RET

						! ----------------------------------------------------------------------------------------------

						!sr_BS520x_HMI_Calibration_AlphaFb_SaveProcess:	
							
							! *** Base Screen 520x Data Edit Time-out & Save 
							IF (g_Hmi_bs520x_CtrlVal.0)
								! *** Step 1
								Hmi_SaveTimeOut_Measure_1 = TIME;
								g_Hmi_bs520x_CtrlVal.0 = 0;
								g_Hmi_bs520x_CtrlVal.1 = 1;
								
							ELSEIF(g_Hmi_bs520x_CtrlVal.1)
								! *** Step 2
								Hmi_SaveTimeOut_Measure_2 = TIME;		
								Hmi_SaveTimeOut_Difference = Hmi_SaveTimeOut_Measure_2 - Hmi_SaveTimeOut_Measure_1;
								
								! *** Step 3 Save Time-out & Screen Change Check
								IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs2201_EditSaveTOutSt) | (gHmi_Sys_CurrentScreen < 5201) | (gHmi_Sys_CurrentScreen > 5201))
									
									CALL sr_HMI_Variable_Read_fromFlashMemory
									
									Hmi_SaveTimeOut_Measure_1 = 0;
									Hmi_SaveTimeOut_Measure_2 = 0;
									Hmi_SaveTimeOut_Difference = 0;
									
									g_Hmi_bs520x_CtrlVal = 0;
								
								END !IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs1000_EditSaveTOut) | (gHmi_Sys_CurrentScreen < 1321) | (gHmi_Sys_CurrentScreen > 1321))
								
							END !IF (g_Hmi_bs520x_CtrlVal.0)
							
								
							! *** HMI Save Command (HMI Edit Value -> Store Value Update)
							IF (g_Hmi_bs520x_CtrlVal.15)	
							
								index_Hmi_EdtValToStoreValSave = 1641;
								Hmi_EdtValToStoreValSaveCount = 9;
								
								LOOP Hmi_EdtValToStoreValSaveCount
									SETVAR(GETVAR(index_Hmi_EdtValToStoreValSave), (index_Hmi_EdtValToStoreValSave + 1));
									index_Hmi_EdtValToStoreValSave = index_Hmi_EdtValToStoreValSave + 2;
								END
								
								CALL sr_HMI_Variable_Write_toFlashMemory;		
								CALL sr_HMI_Variable_Read_fromFlashMemory
										
								g_Hmi_bs520x_CtrlVal = 0;
								
							END ! IF (g_Hmi_bs520x_CtrlVal.15)	

						!RET

						! ----------------------------------------------------------------------------------------------

						!sr_BS530x_HMI_Calibration_DynoFbCurrent_SaveProcess:	
							
							! *** Base Screen 530x Data Edit Time-out & Save 
							IF (g_Hmi_bs530x_CtrlVal.0)
								! *** Step 1
								Hmi_SaveTimeOut_Measure_1 = TIME;
								g_Hmi_bs530x_CtrlVal.0 = 0;
								g_Hmi_bs530x_CtrlVal.1 = 1;
								
							ELSEIF(g_Hmi_bs530x_CtrlVal.1)
								! *** Step 2
								Hmi_SaveTimeOut_Measure_2 = TIME;		
								Hmi_SaveTimeOut_Difference = Hmi_SaveTimeOut_Measure_2 - Hmi_SaveTimeOut_Measure_1;
								
								! *** Step 3 Save Time-out & Screen Change Check
								IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs2201_EditSaveTOutSt) | (gHmi_Sys_CurrentScreen < 5301) | (gHmi_Sys_CurrentScreen > 5304))
									
									CALL sr_HMI_Variable_Read_fromFlashMemory
									
									Hmi_SaveTimeOut_Measure_1 = 0;
									Hmi_SaveTimeOut_Measure_2 = 0;
									Hmi_SaveTimeOut_Difference = 0;
									
									g_Hmi_bs530x_CtrlVal = 0;
								
								END !IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs1000_EditSaveTOut) | (gHmi_Sys_CurrentScreen < 1321) | (gHmi_Sys_CurrentScreen > 1321))
								
							END !IF (g_Hmi_bs530x_CtrlVal.0)
							
								
							! *** HMI Save Command (HMI Edit Value -> Store Value Update)
							IF (g_Hmi_bs530x_CtrlVal.15)	
							
								index_Hmi_EdtValToStoreValSave = 1711;
								Hmi_EdtValToStoreValSaveCount = 34;
								
								LOOP Hmi_EdtValToStoreValSaveCount
									SETVAR(GETVAR(index_Hmi_EdtValToStoreValSave), (index_Hmi_EdtValToStoreValSave + 1));
									index_Hmi_EdtValToStoreValSave = index_Hmi_EdtValToStoreValSave + 2;
								END
								
								CALL sr_HMI_Variable_Write_toFlashMemory;		
								CALL sr_HMI_Variable_Read_fromFlashMemory
								
								! *** Linear Interporation
								index_LinearInterpolation_StValAccess = 1732;	
								Hmi_LinearInterpolationSaveCount = 11;
								index_LinearInterpolation_List = 0;
								indexOffset_RefeVal_AppliedVal = 24;
								
								LOOP Hmi_LinearInterpolationSaveCount 
									g_lInter_fbDynoCurrent_RefeVal_List(index_LinearInterpolation_List) = GETVAR(index_LinearInterpolation_StValAccess);
									g_lInter_fbDynoCurrent_AppliedVal_List(index_LinearInterpolation_List) = GETVAR((index_LinearInterpolation_StValAccess + indexOffset_RefeVal_AppliedVal));
									
									index_LinearInterpolation_StValAccess = index_LinearInterpolation_StValAccess + 2;
									index_LinearInterpolation_List = index_LinearInterpolation_List + 1;
								END
								
								g_Hmi_bs530x_CtrlVal = 0;
								
							END ! IF (g_Hmi_bs530x_CtrlVal.15)	

						!RET

						! ----------------------------------------------------------------------------------------------

						!sr_BS540x_HMI_Calibration_AlphaFb_SaveProcess:	
							
							! *** Base Screen 540x Data Edit Time-out & Save 
							IF (g_Hmi_bs540x_CtrlVal.0)
								! *** Step 1
								Hmi_SaveTimeOut_Measure_1 = TIME;
								g_Hmi_bs540x_CtrlVal.0 = 0;
								g_Hmi_bs540x_CtrlVal.1 = 1;
								
							ELSEIF(g_Hmi_bs540x_CtrlVal.1)
								! *** Step 2
								Hmi_SaveTimeOut_Measure_2 = TIME;		
								Hmi_SaveTimeOut_Difference = Hmi_SaveTimeOut_Measure_2 - Hmi_SaveTimeOut_Measure_1;
								
								! *** Step 3 Save Time-out & Screen Change Check
								IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs2201_EditSaveTOutSt) | (gHmi_Sys_CurrentScreen < 5401) | (gHmi_Sys_CurrentScreen > 5404))
									
									CALL sr_HMI_Variable_Read_fromFlashMemory
									
									Hmi_SaveTimeOut_Measure_1 = 0;
									Hmi_SaveTimeOut_Measure_2 = 0;
									Hmi_SaveTimeOut_Difference = 0;
									
									g_Hmi_bs540x_CtrlVal = 0;
								
								END !IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs1000_EditSaveTOut) | (gHmi_Sys_CurrentScreen < 1321) | (gHmi_Sys_CurrentScreen > 1321))
								
							END !IF (g_Hmi_bs520x_CtrlVal.0)
							
								
							! *** HMI Save Command (HMI Edit Value -> Store Value Update)
							IF (g_Hmi_bs540x_CtrlVal.15)	
							
								index_Hmi_EdtValToStoreValSave = 1661;
								Hmi_EdtValToStoreValSaveCount = 14;
								
								LOOP Hmi_EdtValToStoreValSaveCount
									SETVAR(GETVAR(index_Hmi_EdtValToStoreValSave), (index_Hmi_EdtValToStoreValSave + 1));
									index_Hmi_EdtValToStoreValSave = index_Hmi_EdtValToStoreValSave + 2;
								END
								
								CALL sr_HMI_Variable_Write_toFlashMemory;		
								CALL sr_HMI_Variable_Read_fromFlashMemory
								
									! *** Dyno. Command D/A Conversion Ration ***	
									g_unit_Cmd_100PctToDynoCurrent_DARation = 32767 / 100 / 2;
									
									! *** Engine Command D/A Conversion Ration ***		
									g_unit_Cmd_Alpha_1_100Pct_DaVal = 3276.7 * g_Hmi_bs5402_Eng_Alpha_1_MaxVoltSt;
									g_unit_Cmd_Alpha_1_0Pct_DaVal = 3276.7 * g_Hmi_bs5402_Eng_Alpha_1_MinVoltSt;
									g_unit_Cmd_Alpha_1_0Pct_ZeroOffset_DaVal = 3276.7 * g_Hmi_bs5402_Eng_Alpha_1_OffsetVoltSt;
									g_unit_Cmd_100PctToAlpha_1_DARation = (g_unit_Cmd_Alpha_1_100Pct_DaVal - g_unit_Cmd_Alpha_1_0Pct_DaVal) / 100;	
									
									g_unit_Cmd_Alpha_2_100Pct_DaVal = 3276.7 * g_Hmi_bs5403_Eng_Alpha_2_MaxVoltSt;
									g_unit_Cmd_Alpha_2_0Pct_DaVal = 3276.7 * g_Hmi_bs5403_Eng_Alpha_2_MinVoltSt;
									g_unit_Cmd_Alpha_2_0Pct_ZeroOffset_DaVal = 3276.7 * g_Hmi_bs5403_Eng_Alpha_2_OffsetVoltSt;
									g_unit_Cmd_100PctToAlpha_2_DARation = (g_unit_Cmd_Alpha_2_100Pct_DaVal - g_unit_Cmd_Alpha_2_0Pct_DaVal) / 100;								
									g_unit_Cmd_Alpha_DO_IdleVali_DaVal = 3276.7 * g_Hmi_bs5404_Op_Alpha_IdleValiDo_VoltSt;
									
								g_Hmi_bs540x_CtrlVal = 0;
								
							END ! IF (g_Hmi_bs520x_CtrlVal.15)	

						!RET

						! ----------------------------------------------------------------------------------------------

						!sr_BS550x_HMI_Calibration_DynoOutputCurrent_SaveProcess:	
							
							! *** Base Screen 540x Data Edit Time-out & Save 
							IF (g_Hmi_bs550x_CtrlVal.0)
								! *** Step 1
								Hmi_SaveTimeOut_Measure_1 = TIME;
								g_Hmi_bs550x_CtrlVal.0 = 0;
								g_Hmi_bs550x_CtrlVal.1 = 1;
								
							ELSEIF(g_Hmi_bs550x_CtrlVal.1)
								! *** Step 2
								Hmi_SaveTimeOut_Measure_2 = TIME;		
								Hmi_SaveTimeOut_Difference = Hmi_SaveTimeOut_Measure_2 - Hmi_SaveTimeOut_Measure_1;
								
								! *** Step 3 Save Time-out & Screen Change Check
								IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs2201_EditSaveTOutSt) | (gHmi_Sys_CurrentScreen < 5501) | (gHmi_Sys_CurrentScreen > 5504))
									
									CALL sr_HMI_Variable_Read_fromFlashMemory
									
									Hmi_SaveTimeOut_Measure_1 = 0;
									Hmi_SaveTimeOut_Measure_2 = 0;
									Hmi_SaveTimeOut_Difference = 0;
									
									g_Hmi_bs550x_CtrlVal = 0;
								
								END !IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs1000_EditSaveTOut) | (gHmi_Sys_CurrentScreen < 1321) | (gHmi_Sys_CurrentScreen > 1321))
								
							END !IF (g_Hmi_bs530x_CtrlVal.0)
							
								
							! *** HMI Save Command (HMI Edit Value -> Store Value Update)
							IF (g_Hmi_bs550x_CtrlVal.15)	
							
								index_Hmi_EdtValToStoreValSave = 1071;
								Hmi_EdtValToStoreValSaveCount = 34;
								
								LOOP Hmi_EdtValToStoreValSaveCount
									SETVAR(GETVAR(index_Hmi_EdtValToStoreValSave), (index_Hmi_EdtValToStoreValSave + 1));
									index_Hmi_EdtValToStoreValSave = index_Hmi_EdtValToStoreValSave + 2;
								END
								
								CALL sr_HMI_Variable_Write_toFlashMemory;		
								CALL sr_HMI_Variable_Read_fromFlashMemory
								
								! *** Linear Interporation
								index_LinearInterpolation_StValAccess = 1092;	
								Hmi_LinearInterpolationSaveCount = 11;
								index_LinearInterpolation_List = 0;
								indexOffset_RefeVal_AppliedVal = 24;
								
								LOOP Hmi_LinearInterpolationSaveCount 
									g_lInter_opDynoCurrent_RefeVal_List(index_LinearInterpolation_List) = GETVAR(index_LinearInterpolation_StValAccess);
									g_lInter_opDynoCurrent_AppliedVal_List(index_LinearInterpolation_List) = GETVAR((index_LinearInterpolation_StValAccess + indexOffset_RefeVal_AppliedVal));
									
									index_LinearInterpolation_StValAccess = index_LinearInterpolation_StValAccess + 2;
									index_LinearInterpolation_List = index_LinearInterpolation_List + 1;
								END
								
								g_Hmi_bs550x_CtrlVal = 0;
								
							END ! IF (g_Hmi_bs540x_CtrlVal.15)	

						!RET

						! ----------------------------------------------------------------------------------------------

						!sr_BS600x_HMI_MonitoringInputIndexSetting_SaveProcess:	
							
							! *** Base Screen 600x Data Edit Time-out & Save 
							IF (g_Hmi_bs600x_CtrlVal.0)
								! *** Step 1
								Hmi_SaveTimeOut_Measure_1 = TIME;
								g_Hmi_bs600x_CtrlVal.0 = 0;
								g_Hmi_bs600x_CtrlVal.1 = 1;
								
							ELSEIF(g_Hmi_bs600x_CtrlVal.1)
								! *** Step 2
								Hmi_SaveTimeOut_Measure_2 = TIME;		
								Hmi_SaveTimeOut_Difference = Hmi_SaveTimeOut_Measure_2 - Hmi_SaveTimeOut_Measure_1;
								
								! *** Step 3 Save Time-out & Screen Change Check
								IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs2201_EditSaveTOutSt) | (gHmi_Sys_CurrentScreen < 6001) | (gHmi_Sys_CurrentScreen > 6004))
									
									CALL sr_HMI_Variable_Read_fromFlashMemory
									
									Hmi_SaveTimeOut_Measure_1 = 0;
									Hmi_SaveTimeOut_Measure_2 = 0;
									Hmi_SaveTimeOut_Difference = 0;
									
									g_Hmi_bs600x_CtrlVal = 0;
								
								END !IF((Hmi_SaveTimeOut_Difference > g_Hmi_bs1000_EditSaveTOut) | (gHmi_Sys_CurrentScreen < 1321) | (gHmi_Sys_CurrentScreen > 1321))
								
							END !IF (g_Hmi_bs600x_CtrlVal.0)
							
								
							! *** HMI Save Command (HMI Edit Value -> Store Value Update)
							IF (g_Hmi_bs600x_CtrlVal.15)	
							
								index_Hmi_EdtValToStoreValSave = 1781;
								Hmi_EdtValToStoreValSaveCount = 19;
								
								LOOP Hmi_EdtValToStoreValSaveCount
									SETVAR(GETVAR(index_Hmi_EdtValToStoreValSave), (index_Hmi_EdtValToStoreValSave + 1));
									index_Hmi_EdtValToStoreValSave = index_Hmi_EdtValToStoreValSave + 2;
								END
								
								CALL sr_HMI_Variable_Write_toFlashMemory;		
								CALL sr_HMI_Variable_Read_fromFlashMemory
										
								g_Hmi_bs600x_CtrlVal = 0;
								
							END ! IF (g_Hmi_bs600x_CtrlVal.15)	

						!RET

						! ----------------------------------------------------------------------------------------------
							
					END ! IF((PeriodicControlTime_Measure_1 - PeriodicControlTime_Measure_2) >= PeriodicControlTime_ProgExcution_Interval)					
					
					
				! ----------------------------------------------------------------------------------------------
				! *** HMI Banner Message Time out Check
				IF((PeriodicControlTime_Measure_1 - Hmi_BannerDisplay_AutoR_StartOnTime) > g_Hmi_BannerSetAndResetInterval)
							
					g_Bf8_AutoR_Banner_Display = 0;
				END!IF((PeriodicControlTime_Measure_1 - Hmi_BannerDisplay_AutoR_StartOnTime) > g_Hmi_BannerSetAndResetInterval)
				
			END ! BLOCK 
			
		END !WHILE 1
			
! *** Program Resule Update
g_bufferProgramResult(0)(BufferProgrmaNumber) = 99999;

! *** MMI Terminal display
DISP "[#%d buffer] program end.", BufferProgrmaNumber;

STOP


! ----------------------------------------------------------------------------------------------

! *** HMI Banner Message Display Auto Routine
ON g_Bf8_AutoR_Banner_Display	
	
	Hmi_BannerDisplay_AutoR_StartOnTime = TIME;
	g_Hmi_Banner_Enable = 1; 
	
RET

ON ^g_Bf8_AutoR_Banner_Display	
	
	g_Hmi_Banner_MsgLineNum = 0; 
	g_Hmi_Banner_Enable = 0;
	
RET


! *************** Subroutine  *************** !

sr_HMI_Variable_Read_fromFlashMemory:

	! *** Variable zero clear
	FILL(0, gl_HmiVar_FlashRead)

	! *** Read Variable From Nonvolatile Memory
	READ gl_HmiVar_FlashRead, HmiVal_FlashWrite
	
	! *** List Index & Tag Max Number Set
	index_HmiTagVar_FlashAccess = 70;	
	g_Hmi_UserDefine_TagCount = 880 - index_HmiTagVar_FlashAccess; ! = MAX TAG Number - 1000 
	
	! *** Check Read Variable 
	IF (MAX(gl_HmiVar_FlashRead) > 0)		
		
		LOOP (g_Hmi_UserDefine_TagCount)
			
			SETVAR(gl_HmiVar_FlashRead(index_HmiTagVar_FlashAccess), (index_HmiTagVar_FlashAccess + 1000))
			index_HmiTagVar_FlashAccess = index_HmiTagVar_FlashAccess + 1;
			
		END !LOOP (g_Hmi_UserDefine_TagCount)
		
		IF( g_Hmi_bs2201_EditSaveTOutSt < 5000) g_Hmi_bs2201_EditSaveTOutSt = 5000; END;
		
	ELSE 
		
		! *** MMI Show Message						
		DISP "[#%d buffer] HMI Variable Read - [Error] Threr is no data.", BufferProgrmaNumber;	
		
	END !IF (MAX(gl_HmiVar_FlashRead) > 0)
	
RET

! ----------------------------------------------------------------------------------------------

sr_HMI_Variable_Write_toFlashMemory:
	
	! *** Variable zero clear
	FILL(0, gl_HmiVar_FlashWrite)
		
		! *** List Index & Tag Max Number Set
		index_HmiTagVar_FlashAccess = 0;	
		g_Hmi_UserDefine_TagCount = 880; ! = MAX TAG Number - 1000 
	
		LOOP (g_Hmi_UserDefine_TagCount)
			
			gl_HmiVar_FlashWrite(index_HmiTagVar_FlashAccess) = GETVAR((index_HmiTagVar_FlashAccess + 1000));			
			index_HmiTagVar_FlashAccess = index_HmiTagVar_FlashAccess + 1;
			
		END !LOOP (g_Hmi_UserDefine_TagCount)
				
			
	! *** Write Variable From Nonvolatile Memory
	WRITE gl_HmiVar_FlashWrite, HmiVal_FlashWrite
	
RET

#9
	! Signal Filter On / Encoder Mode Setting
	!COEWRITE/1 (5,0x8000,0x03,0)  !Frequency Window. Default 10000 -> 10ms
	!COEWRITE/1 (5,0x8010,0x03,0)  !Frequency Window. Default 10000 -> 10ms
	COEWRITE/1 (5,0x8000,0x0F,1)  !Frequency Window. Default 10000 -> 10ms
	COEWRITE/2 (5,0x8000,0x11,300)  !Frequency Window. Default 10000 -> 10ms
	COEWRITE/1 (5,0x8010,0x0F,1)  !Frequency Window. Default 10000 -> 10ms
	COEWRITE/2 (5,0x8010,0x11,300)  !Frequency Window. Default 10000 -> 10ms

	!COEWRITE/1 (11,0x8010,0x08,0)  !Signal Filter. Default 1 -> If set 0 Filter on
	!COEWRITE/1 (11,0x8010,0x03,0)  !If set 1 then act as a Updown Counter. Default 0
	!COEWRITE/1 (11,0x8000,0x11,10000)  !Frequency Window. Default 10000 -> 10ms
	!COEWRITE/1 (12,0x8010,0x08,0)
	!COEWRITE/1 (12,0x8010,0x03,1)

!	***	Base	Screens	#5501	cf_Cal_Current_Calibration-1
g_Hmi_bs5501_Dyno_CIndexSt	 = 0;	
!	***	Base	Screens	#5502	cf_Cal_Current_Calibration-2~4
g_Hmi_bs5502_Output_CurrentCalAd_Val0St	=0;	
g_Hmi_bs5502_Output_CurrentCalAd_Val1St	=0;	
g_Hmi_bs5502_Output_CurrentCalAd_Val2St	=0;			
g_Hmi_bs5502_Output_CurrentCalAd_Val3St	=0;			
g_Hmi_bs5502_Output_CurrentCalAd_Val4St	=0;			
g_Hmi_bs5503_Output_CurrentCalAd_Val5St	=0;			
g_Hmi_bs5503_Output_CurrentCalAd_Val6St	=0;			
g_Hmi_bs5503_Output_CurrentCalAd_Val7St	=0;			
g_Hmi_bs5503_Output_CurrentCalAd_Val8St	=0;			
g_Hmi_bs5503_Output_CurrentCalAd_Val9St	=0;			
g_Hmi_bs5504_Output_CurrentCalAd_Val10St=0;			
g_Hmi_bs5502_Output_CurrentCal_Val0St	=0;			
g_Hmi_bs5502_Output_CurrentCal_Val1St	=0;		
g_Hmi_bs5502_Output_CurrentCal_Val2St	=0;			
g_Hmi_bs5502_Output_CurrentCal_Val3St	=0;			
g_Hmi_bs5502_Output_CurrentCal_Val4St	=0;			
g_Hmi_bs5503_Output_CurrentCal_Val5St	=0;			
g_Hmi_bs5503_Output_CurrentCal_Val6St	=0;			
g_Hmi_bs5503_Output_CurrentCal_Val7St	=0;			
g_Hmi_bs5503_Output_CurrentCal_Val8St	=0;			
g_Hmi_bs5503_Output_CurrentCal_Val9St	=0;			
g_Hmi_bs5504_Output_CurrentCal_Val10St	=0;			
!	***	Base	Screens	#2201	cf_Sys_OperationPannelConfig-1
g_Hmi_bs2201_EditSaveTOutSt = 10000;		
!	***	Base	Screens	#2202	cf_Sys_OperationPannelConfig-2			
g_Hmi_bs2202_SpeedCngPerKnobPulseSt	= 1;	
g_Hmi_bs2202_TorqueCngPerKnobPulseSt = 1;		
g_Hmi_bs2202_AlphaCngPerKnobPulseSt	= 1;	!*10
g_Hmi_bs2202_CurrentCngPerKnobPulseSt = 0.1;		
g_Hmi_bs2301_DAQ_Avg_NumberSt	=1;	
g_Hmi_bs2302_Emission_FuelDensitySt		=0;		
g_Hmi_bs2302_Emission_AtmCo2ppmSt	=0;			
g_Hmi_bs2303_Emission_TestCycleSt		=0;		
!	***	Base	Screens	#3001	cf_Engine_VarSetting-1
g_Hmi_bs3001_Eng_StartProcedureSt	=0;			
g_Hmi_bs3001_Eng_DynoStartTorqueSt	=0;		
!	***	Base	Screens	#3002	cf_Engine_VarSetting-2
g_Hmi_bs3002_Eng_StartTimeOutSt		=10;	
g_Hmi_bs3002_Eng_StartCompSpeedSt	=600;		
!	***	Base	Screens	#3003	cf_Engine_VarSetting-3
g_Hmi_bs3003_Eng_PreheatTimeSt	=0;
g_Hmi_bs3003_Eng_StopTorqueSt	=0;
g_Hmi_bs3003_Eng_StopAlphaSt	=0;	
!!	***	Base	Screens	#3004	cf_Engine_VarSetting-4
g_Hmi_bs3004_Eng_MaxSpeedSt		=6200;	
g_Hmi_bs3004_Eng_IdleMinSpeedSt	=800;	
!!	***	Base	Screens	#3005	cf_Engine_VarSetting-5
g_Hmi_bs3005_Eng_MaxTorqueSt	=450;			
g_Hmi_bs3005_Eng_MinTorqueSt	=-100;	
!!	***	Base	Screens	#3006	cf_Engine_VarSetting-6
g_Hmi_bs3006_Eng_Num_StrokeCycleSt	=0;			
g_Hmi_bs3006_Eng_Num_EngCylindersSt	=0;		
g_Hmi_bs3006_Eng_CylinderBoreSt	=0;			
!!	***	Base	Screens	#3007	cf_Engine_VarSetting-7
g_Hmi_bs3007_Eng_CylinderStrokeSt	=0;			
g_Hmi_bs3007_Eng_Num_EngFuelTypeSt	=0;			
!	***	Base	Screens	#3007	cf_Engine_VarSetting-7
g_Hmi_bs3008_Eng_StopRPMSt	=0;			
g_Hmi_bs3008_PID_Change_Ramp	=10;	
!	***	Base	Screens	#4001	cf_Ctrl_Dynamometer_N	PID-1
g_Hmi_bs4001_DynoSpid_KpSt	=1.5;			
g_Hmi_bs4001_DynoSpid_KiSt	=0.016;			
g_Hmi_bs4001_DynoSpid_Kdst	=0;	
!	***	Base	Screens	#4002	cf_Ctrl_Dynamometer_N	PID-2
g_Hmi_bs4002_DynoSpid_YMinSt	=0;			
g_Hmi_bs4002_DynoSpid_RefADeltaSt	=10;			
g_Hmi_bs4002_DynoSpid_RefDDeltaSt	=10;			
!	***	Base	Screens	#4003	cf_Ctrl_Dynamometer_N	PID-3
g_Hmi_bs4003_DynoSpid_CMaxSt = 80;		
g_Hmi_bs4003_DynoSpid_CcntMaxSt	 = 500;	
!	***	Base	Screens	#4004	cf_Ctrl_Dynamometer_N	PID-4
g_Hmi_bs4004_DynoSpid_C_ADeltaSt	=1;			
g_Hmi_bs4004_DynoSpid_C_DDeltaSt	=1;			
!	***	Base	Screens	#4101	cf_Ctrl_Dynamometer_T	PID-1
g_Hmi_bs4101_DynoTpid_KpSt	=5;			
g_Hmi_bs4101_DynoTpid_KiSt	=0.006;			
g_Hmi_bs4101_DynoTpid_Kdst	=0;			
!	***	Base	Screens	#4102	cf_Ctrl_Dynamometer_T	PID-2
g_Hmi_bs4101_DynoTpid_TdCntMaxst	=0;			
g_Hmi_bs4102_DynoTpid_YMinSt	=-100;			
g_Hmi_bs4102_DynoTpid_RefADeltaSt	=10;			
g_Hmi_bs4102_DynoTpid_RefDDeltaSt	=10;			
!	***	Base	Screens	#4103	cf_Ctrl_Dynamometer_T	PID-3
g_Hmi_bs4103_DynoTpid_CMaxSt = 80;		
g_Hmi_bs4103_DynoTpid_CcntMaxSt	 = 500;	
!	***	Base	Screens	#4104	cf_Ctrl_Dynamometer_T	PID-4
g_Hmi_bs4104_DynoTpid_C_ADeltaSt	=1;		
g_Hmi_bs4104_DynoTpid_C_DDeltaSt	=1;		
g_Hmi_bs4201_EngSpid_KpSt	=0.1;			
g_Hmi_bs4201_EngSpid_KiSt	=0.001;	
!	***	Base	Screens	#4203	cf_Ctrl_Engine_N	PID-3
g_Hmi_bs4203_EngSpid_IgSpeed_1St	=0;	
g_Hmi_bs4203_EngSpid_IgSpeed_2St	=0;			
g_Hmi_bs4203_EngSpid_IgSpeed_3St	=0;	
!	***	Base	Screens	#4204	cf_Ctrl_Engine_N	PID-4
g_Hmi_bs4204_EngSpid_Ig_iDelta1St	=0;	
g_Hmi_bs4204_EngSpid_Ig_iDelta2St	=0;			
g_Hmi_bs4204_EngSpid_Ig_iDelta3St	=0;		
!!	***	Base	Screens	#4205	cf_Ctrl_Engine_N	PID-5
g_Hmi_bs4205_EngSpid_Ig_iDelta4St	=0;	
g_Hmi_bs4205_EngSpid_YMinSt	=0;			
!!	***	Base	Screens	#4206	cf_Ctrl_Engine_N	PID-6
g_Hmi_bs4206_EngSpid_RefADeltaSt	=2;			
g_Hmi_bs4206_EngSpid_RefDDeltaSt	=2;			
!	***	Base	Screens	#4207	cf_Ctrl_Engine_N	PID-7
g_Hmi_bs4207_EngSpid_CMaxSt	=50;		
g_Hmi_bs4207_EngSpid_CcntMaxSt	=500;			
!!!!	***	Base	Screens	#4208	cf_Ctrl_Engine_N	PID-8
g_Hmi_bs4208_EngSpid_C_ADeltaSt		=1;		
g_Hmi_bs4208_EngSpid_C_DDeltaSt		=1;		
!!!	***	Base	Screens	#4202	cf_Ctrl_Engine_N	PID-2
g_Hmi_bs4202_EngSpid_KpA_St	=0;			
g_Hmi_bs4202_EngSpid_KpB_St	=0;			
g_Hmi_bs420x_CtrlVal		=0;
!	***	Base	Screens	#4301	cf_Ctrl_Engine_T	PID-1
g_Hmi_bs4301_EngTpid_KpSt	=0.1;			
g_Hmi_bs4301_EngTpid_KiSt	=0.001;		
!!	***	Base	Screens	#4302	cf_Ctrl_Engine_T	PID-2
g_Hmi_bs4302_EngTpid_IgSpeed_1St	=0;	
g_Hmi_bs4302_EngTpid_IgSpeed_2St	=0;			
g_Hmi_bs4302_EngTpid_IgSpeed_3St	=0;			
!!	***	Base	Screens	#4303	cf_Ctrl_Engine_T	PID-3
g_Hmi_bs4303_EngTpid_Ig_iDelta1St	=0;		
g_Hmi_bs4303_EngTpid_Ig_iDelta2St	=0;			
g_Hmi_bs4303_EngTpid_Ig_iDelta3St	=0;		
!!	***	Base	Screens	#4304	cf_Ctrl_Engine_T	PID-4
g_Hmi_bs4304_EngTpid_Ig_iDelta4St	=0;		
!	***	Base	Screens	#4305	cf_Ctrl_Engine_T	PID-5
g_Hmi_bs4305_EngTpid_YMinSt	=0;			
g_Hmi_bs4305_EngTpid_RefADeltaSt	=20;			
g_Hmi_bs4305_EngTpid_RefDDeltaSt	=20;			
!!!!	***	Base	Screens	#4306	cf_Ctrl_Engine_T	PID-6
g_Hmi_bs4306_EngTpid_CMaxSt		=50;		
g_Hmi_bs4306_EngTpid_CcntMaxSt		=500;		
!!	***	Base	Screens	#4307	cf_Ctrl_Engine_T	PID-7
g_Hmi_bs4307_EngTpid_C_ADeltaSt	=1;			
g_Hmi_bs4307_EngTpid_C_DDeltaSt	=1;			
g_Hmi_bs430x_CtrlVal		=0;
!	***	Base	Screens	#4401	cf_Ctrl_Engine_Appha	PID-1
g_Hmi_bs4401_EngApid_C_ADeltaSt	=1;			
g_Hmi_bs4401_EngApid_C_DDeltaSt	=1;			
g_Hmi_bs440x_CtrlVal		=0
!	***	Base	Screens	#5001	cf_Cal_Speed_Calibration-1
g_Hmi_bs5001_FB_SpeedYIndexSt	=4;	
g_Hmi_bs5001_FB_SpeedMaxSpeedSt	=10000;	
!	***	Base	Screens	#5002	cf_Cal_Speed_Calibration-2
g_Hmi_bs5002_FB_SpeedCalAd_Val0St	=0;	
g_Hmi_bs5002_FB_SpeedCalAd_Val1St	=0;	
g_Hmi_bs5002_FB_SpeedCalAd_Val2St	=0;	
g_Hmi_bs5002_FB_SpeedCalAd_Val3St	=0;	
g_Hmi_bs5002_FB_SpeedCalAd_Val4St	=0;	
g_Hmi_bs5003_FB_SpeedCalAd_Val5St	=0;	
g_Hmi_bs5003_FB_SpeedCalAd_Val6St	=0;	
g_Hmi_bs5003_FB_SpeedCalAd_Val7St	=0;	
g_Hmi_bs5003_FB_SpeedCalAd_Val8St	=0;	
g_Hmi_bs5003_FB_SpeedCalAd_Val9St	=0;	
g_Hmi_bs5004_FB_SpeedCalAd_Val10St	=0;	
g_Hmi_bs5002_FB_SpeedCal_Val0St	=0;	
g_Hmi_bs5002_FB_SpeedCal_Val1St	=0;	
g_Hmi_bs5002_FB_SpeedCal_Val2St	=0;	
g_Hmi_bs5002_FB_SpeedCal_Val3St	=0;	
g_Hmi_bs5002_FB_SpeedCal_Val4St	=0;	
g_Hmi_bs5003_FB_SpeedCal_Val5St	=0;	
g_Hmi_bs5003_FB_SpeedCal_Val6St	=0;	
g_Hmi_bs5003_FB_SpeedCal_Val7St	=0;	!
g_Hmi_bs5003_FB_SpeedCal_Val8St	=0;	
g_Hmi_bs5003_FB_SpeedCal_Val9St	=0;	
g_Hmi_bs5004_FB_SpeedCal_Val10St	=0;	
!	***	Base	Screens	#5101	cf_Cal_Torque_Calibration-1
g_Hmi_bs5101_FB_TorqueYIndexSt	=1;	
g_Hmi_bs5101_FB_TorqueMaxTorqueSt	=1000;	
!	***	Base	Screens	#5102	cf_Cal_Torque_Calibration-2
g_Hmi_bs5102_FB_TorqueCalAd_Val0St	=-6982;	
g_Hmi_bs5102_FB_TorqueCalAd_Val1St	=-4983;	
g_Hmi_bs5102_FB_TorqueCalAd_Val2St	=-2983;	
g_Hmi_bs5102_FB_TorqueCalAd_Val3St	=-1984;	
g_Hmi_bs5102_FB_TorqueCalAd_Val4St	=-983;	
g_Hmi_bs5102_FB_TorqueCalAd_Val5St	=14;	
g_Hmi_bs5103_FB_TorqueCalAd_Val6St	=1015;	
g_Hmi_bs5103_FB_TorqueCalAd_Val7St	=2014;	
g_Hmi_bs5103_FB_TorqueCalAd_Val8St	=3014;	
g_Hmi_bs5103_FB_TorqueCalAd_Val9St	=5012;	
g_Hmi_bs5104_FB_TorqueCalAd_Val10St	=7012;	
g_Hmi_bs5102_FB_TorqueCal_Val0St	=-7000;	
g_Hmi_bs5102_FB_TorqueCal_Val1St	=-5000;	
g_Hmi_bs5102_FB_TorqueCal_Val2St	=-3000;	
g_Hmi_bs5102_FB_TorqueCal_Val3St	=-2000;	
g_Hmi_bs5102_FB_TorqueCal_Val4St	=-1000;	
g_Hmi_bs5102_FB_TorqueCal_Val5St	=0;	
g_Hmi_bs5103_FB_TorqueCal_Val6St	=1000;	
g_Hmi_bs5103_FB_TorqueCal_Val7St	=2000;	
g_Hmi_bs5103_FB_TorqueCal_Val8St	=3000;	
g_Hmi_bs5103_FB_TorqueCal_Val9St	=5000;	
g_Hmi_bs5104_FB_TorqueCal_Val10St	=7000;	

!	***	Base	Screens	#5201	cf_Cal_Alpha_Calibration-1
g_Hmi_bs5201_FB_AlphaYIndexSt	=2;	
g_Hmi_bs5201_FB_AlphaSourceSt	=0;	
g_Hmi_bs5201_FB_AlphaZeroOffsetSd	=0;	

!	***	Base	Screens	#5401	cf_Cal_Alpha_Output Calibration-1
g_Hmi_bs5401_Op_Alpha_1_CIndexSt	=2;	
g_Hmi_bs5401_Op_Alpha_2_CIndexSt	=3;	
!	***	Base	Screens	#5402	cf_Cal_Alpha_Output Calibration-2
g_Hmi_bs5402_Eng_Alpha_1_MinVoltSt	=0.8;			
g_Hmi_bs5402_Eng_Alpha_1_MaxVoltSt	=4.2;				
g_Hmi_bs5402_Eng_Alpha_1_OffsetVoltSt	=0;		
!	***	Base	Screens	#5403	cf_Cal_Alpha_Output Calibration-3		
g_Hmi_bs5403_Eng_Alpha_2_MinVoltSt	=0.4;				
g_Hmi_bs5403_Eng_Alpha_2_MaxVoltSt		=2.1;			
g_Hmi_bs5403_Eng_Alpha_2_OffsetVoltSt	=0;		
!	***	Base	Screens	#5404	cf_Cal_Alpha_Output Calibration-4	
g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt	=1;				
g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt		=1;			
g_Hmi_bs5404_Op_Alpha_IdleValiDo_VoltSt		=1.2;			

!	***	Base	Screens	#5301	cf_Cal_Current_Calibration-1
g_Hmi_bs5301_FB_CurrentIndexSt	=0;	
g_Hmi_bs5301_FB_MaxCurrentSt	=0;	
!	***	Base	Screens	#5302	cf_Cal_Current_Calibration-2
g_Hmi_bs5302_FB_CurrentCalAd_Val0St	=0;	
g_Hmi_bs5302_FB_CurrentCalAd_Val1St	=0;	
g_Hmi_bs5302_FB_CurrentCalAd_Val2St	=0;	
g_Hmi_bs5302_FB_CurrentCalAd_Val3St	=0;	
g_Hmi_bs5302_FB_CurrentCalAd_Val4St	=0;	
g_Hmi_bs5302_FB_CurrentCalAd_Val5St	=0;	
g_Hmi_bs5303_FB_CurrentCalAd_Val6St	=0;	
g_Hmi_bs5303_FB_CurrentCalAd_Val7St	=0;	
g_Hmi_bs5303_FB_CurrentCalAd_Val8St	=0;	
g_Hmi_bs5303_FB_CurrentCalAd_Val9St	=0;	
g_Hmi_bs5304_FB_CurrentCalAd_Val10St	=0;	
g_Hmi_bs5302_FB_CurrentCal_Val0St	=0;	
g_Hmi_bs5302_FB_CurrentCal_Val1St	=0;	
g_Hmi_bs5302_FB_CurrentCal_Val2St	=0;	
g_Hmi_bs5302_FB_CurrentCal_Val3St	=0;	
g_Hmi_bs5302_FB_CurrentCal_Val4St	=0;	
g_Hmi_bs5303_FB_CurrentCal_Val5St	=0;	
g_Hmi_bs5303_FB_CurrentCal_Val6St	=0;	
g_Hmi_bs5303_FB_CurrentCal_Val7St	=0;	
g_Hmi_bs5303_FB_CurrentCal_Val8St	=0;	
g_Hmi_bs5303_FB_CurrentCal_Val9St	=0;	
g_Hmi_bs5304_FB_CurrentCal_Val10St	=0;	

!ACS Controller Setting Program written by ATOM UI

	g_Hmi_Target_Dyno_Torque_Range=4000;
	g_Hmi_Target_Dyno_Speed_Range=8000;
	g_Hmi_Target_Engine_AlphaPosi_Range=100;
	g_Hmi_Target_Engine_Speed_Range=8000;
	g_Hmi_Target_Engine_Torque_Range=4000;

!	g_Hmi_bs5001_FB_SpeedYIndexSt = 4;
!	g_Hmi_bs5101_FB_TorqueYIndexSt = 1;
	!g_Hmi_bs5301_FB_CurrentIndexSt = #8#;
!	g_Hmi_bs5201_FB_AlphaYIndexSt = 2;
!	g_Hmi_bs5501_Dyno_CIndexSt = 2;
!	g_Hmi_bs5401_Op_Alpha_1_CIndexSt = 6;
!	g_Hmi_bs5401_Op_Alpha_2_CIndexSt = 7;
!	g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt = 1;
!	g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt = 1;

!	g_Hmi_bs5001_FB_SpeedYIndexEd = 4;
!	g_Hmi_bs5201_FB_AlphaYIndexEd = 2;
!	g_Hmi_bs5501_Dyno_CIndexEd = 2;
!	g_Hmi_bs5401_Op_Alpha_1_CIndexEd = 6;
!	g_Hmi_bs5401_Op_Alpha_2_CIndexEd = 7;
!	g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexEd = 1;
!	g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexEd = 1;

	INT ResultINT;
	REAL ResultReal;
	
	! *** Variable zero clear
	FILL(0, gl_HmiVar_FlashWrite)

		int index_HmiTagVar_FlashAccess
	
		! *** List Index & Tag Max Number Set
		index_HmiTagVar_FlashAccess = 0;	
		g_Hmi_UserDefine_TagCount = 880; ! = MAX TAG Number - 1000 
	
		LOOP (g_Hmi_UserDefine_TagCount)
			ResultINT = (index_HmiTagVar_FlashAccess + 1000)/2;
			ResultReal = (index_HmiTagVar_FlashAccess + 1000)/2;
			IF ((index_HmiTagVar_FlashAccess + 1000)<=1820) & ((index_HmiTagVar_FlashAccess + 1000)>=1071) & (ResultINT <> ResultReal) 
				gl_HmiVar_FlashWrite(index_HmiTagVar_FlashAccess) = GETVAR((index_HmiTagVar_FlashAccess + 1001));	
			ELSE
				gl_HmiVar_FlashWrite(index_HmiTagVar_FlashAccess) = GETVAR((index_HmiTagVar_FlashAccess + 1000));	
			END
			index_HmiTagVar_FlashAccess = index_HmiTagVar_FlashAccess + 1;
			
		END !LOOP (g_Hmi_UserDefine_TagCount)
				
			
	! *** Write Variable From Nonvolatile Memory
	WRITE gl_HmiVar_FlashWrite, HmiVal_FlashWrite

	! *** Variable zero clear
	FILL(0, gl_HmiVar_FlashRead)

	! *** Read Variable From Nonvolatile Memory
	READ gl_HmiVar_FlashRead, HmiVal_FlashWrite
	
	! *** List Index & Tag Max Number Set
	index_HmiTagVar_FlashAccess = 70;	
	g_Hmi_UserDefine_TagCount = 880 - index_HmiTagVar_FlashAccess; ! = MAX TAG Number - 1000 
	
	! *** Check Read Variable 
	IF (MAX(gl_HmiVar_FlashRead) > 0)		
		
		LOOP (g_Hmi_UserDefine_TagCount)
			
			SETVAR(gl_HmiVar_FlashRead(index_HmiTagVar_FlashAccess), (index_HmiTagVar_FlashAccess + 1000))
			index_HmiTagVar_FlashAccess = index_HmiTagVar_FlashAccess + 1;
			
		END !LOOP (g_Hmi_UserDefine_TagCount)
		
		
	ELSE 
	
		LOOP (g_Hmi_UserDefine_TagCount)
			
			SETVAR(gl_HmiVar_FlashRead(index_HmiTagVar_FlashAccess), (index_HmiTagVar_FlashAccess + 1000))
			index_HmiTagVar_FlashAccess = index_HmiTagVar_FlashAccess + 1;
			
		END !LOOP (g_Hmi_UserDefine_TagCount)
		
	END !IF (MAX(gl_HmiVar_FlashRead) > 0)
	
	IF( g_Hmi_bs2201_EditSaveTOutSt < 5000) g_Hmi_bs2201_EditSaveTOutSt = 5000; END;

STOP


#A
!axisdef X=0,Y=1,Z=2,T=3,A=4,B=5,C=6,D=7
!axisdef x=0,y=1,z=2,t=3,a=4,b=5,c=6,d=7
global int I(100),I0,I1,I2,I3,I4,I5,I6,I7,I8,I9,I90,I91,I92,I93,I94,I95,I96,I97,I98,I99
global real V(100),V0,V1,V2,V3,V4,V5,V6,V7,V8,V9,V90,V91,V92,V93,V94,V95,V96,V97,V98,V99

XARRSIZE = 1000000;

! *** Dyno Type ***
GLOBAL INT g_dynoType_EC
GLOBAL INT g_dynoType_AC
GLOBAL INT g_dynoType

! *** Buffer Program Result *** 
GLOBAL INT g_bufferProgramResult(1)(10)
GLOBAL INT g_bufferProgramResultListSize

! *** I/O DATA Variable *** 
GLOBAL 	INT g_eCAT_HMI_index_DI;
GLOBAL 	INT g_eCAT_HMI_index_DO;
GLOBAL 	INT g_eCAT_HMI_index_AI;
GLOBAL 	INT g_eCAT_HMI_index_AO;
GLOBAL 	INT g_eCAT_index_DI;
GLOBAL 	INT g_eCAT_index_DO;
GLOBAL 	INT g_eCAT_index_AI;
GLOBAL 	INT g_eCAT_index_AO;

GLOBAL INT g_eCAT_HMI_DI_Data(1)(10)
GLOBAL INT g_eCAT_HMI_DI_Address(1)(10)
GLOBAL INT g_eCAT_HMI_DI_SlaveIndex(1)(10)
GLOBAL INT g_eCAT_HMI_DO_Data(1)(10)
GLOBAL INT g_eCAT_HMI_DO_Address(1)(10)
GLOBAL INT g_eCAT_HMI_DO_SlaveIndex(1)(10)
GLOBAL INT g_eCAT_HMI_AI_Data(1)(10)
GLOBAL INT g_eCAT_HMI_AI_Address(1)(10)
GLOBAL INT g_eCAT_HMI_AI_SlaveIndex(1)(10)
GLOBAL INT g_eCAT_HMI_AO_Data(1)(10)
GLOBAL INT g_eCAT_HMI_AO_Address(1)(10)
GLOBAL INT g_eCAT_HMI_AO_SlaveIndex(1)(10)

GLOBAL INT g_eCAT_DI_Data(1)(40)
GLOBAL INT g_eCAT_DI_Address(1)(40)
GLOBAL INT g_eCAT_DI_SlaveIndex(1)(40)
GLOBAL INT g_eCAT_DO_Data(1)(40)
GLOBAL INT g_eCAT_DO_Address(1)(40)
GLOBAL INT g_eCAT_DO_SlaveIndex(1)(40)
GLOBAL INT g_eCAT_AI_Data(1)(40)
GLOBAL INT g_eCAT_AI_Address(1)(40)
GLOBAL INT g_eCAT_AI_SlaveIndex(1)(40)
GLOBAL INT g_eCAT_AO_Data(1)(40)
GLOBAL INT g_eCAT_AO_Address(1)(40)
GLOBAL INT g_eCAT_AO_SlaveIndex(1)(40)

! *** Calibration Data *** 
GLOBAL REAL g_lInter_fbTorque_RefeVal_List(11)
GLOBAL REAL g_lInter_fbSpeed_RefeVal_List(11)
GLOBAL REAL g_lInter_fbAlpha_RefeVal_List(11)
GLOBAL REAL g_lInter_fbDynoCurrent_RefeVal_List(11)
GLOBAL REAL g_lInter_opDynoCurrent_RefeVal_List(11)
GLOBAL REAL g_lInter_fbTorque_AppliedVal_List(11)
GLOBAL REAL g_lInter_fbSpeed_AppliedVal_List(11)
GLOBAL REAL g_lInter_fbAlpha_AppliedVal_List(11)
GLOBAL REAL g_lInter_fbDynoCurrent_AppliedVal_List(11)
GLOBAL REAL g_lInter_opDynoCurrent_AppliedVal_List(11)

! *** Dyno & Engine DA conversion ***
GLOBAL REAL g_unit_Cmd_100PctToDynoCurrent_DARation
GLOBAL REAL g_unit_Cmd_100PctToAlpha_1_DARation
GLOBAL REAL g_unit_Cmd_Alpha_1_0Pct_DaVal
GLOBAL REAL g_unit_Cmd_Alpha_1_0Pct_ZeroOffset_DaVal
GLOBAL REAL g_unit_Cmd_Alpha_1_100Pct_DaVal
GLOBAL REAL g_unit_Cmd_100PctToAlpha_2_DARation
GLOBAL REAL g_unit_Cmd_Alpha_2_0Pct_DaVal
GLOBAL REAL g_unit_Cmd_Alpha_2_0Pct_ZeroOffset_DaVal
GLOBAL REAL g_unit_Cmd_Alpha_2_100Pct_DaVal
GLOBAL REAL g_unit_Cmd_Alpha_DO_IdleVali_DaVal

! *** Automatic Mode Profile Control ***
GLOBAL INT g_AutoMd_ProfilePlay_Enable;	
GLOBAL INT g_pf_IndexStart_Infomation
GLOBAL INT g_pf_IndexSize_Infomation
GLOBAL INT g_pf_IndexStart_DynoProfile
GLOBAL INT g_pf_IndexStart_EngineProfile
GLOBAL INT g_pf_IndexSize_DynoEngProfile
GLOBAL INT g_pf_ProfileListSize

! *** 
! *** etc
GLOBAL INT g_Hmi_BannerSetAndResetInterval

! *** Buffer #1 (System Initialization) ***

GLOBAL INT enum_DataType_Bool
GLOBAL INT enum_DataType_Voltage
GLOBAL INT enum_DataType_Current
GLOBAL INT enum_DataType_RTD
GLOBAL INT enum_DataType_Thermocouples
GLOBAL INT enum_DataType_StrainGauge
GLOBAL INT enum_DataType_EncoderCounterValue
GLOBAL INT enum_DataType_EncoderPeriodValue
GLOBAL INT enum_DataType_EncoderFrequency

!GLOBAL INT g_eCAT_Input_DataNumber
		
!GLOBAL INT g_eCAT_Input_Data(1)(600)
!GLOBAL INT g_eCAT_Input_Data_ExistIndex(1)(600)
!GLOBAL INT g_eCAT_Input_Address(1)(600)
!GLOBAL INT g_eCAT_Input_ByteSize(1)(600)
!GLOBAL INT g_eCAT_Input_Type(1)(600)
!GLOBAL INT g_eCAT_Input_SlaveIndex(1)(600)

!GLOBAL INT g_eCAT_RS232_Input_Data(1)(200)

!GLOBAL INT g_eCAT_Output_DataNumber

!GLOBAL INT g_eCAT_Output_Data(1)(600)
!GLOBAL INT g_eCAT_Output_Data_ExistIndex(1)(600)
!GLOBAL INT g_eCAT_Output_Address(1)(600)
!GLOBAL INT g_eCAT_Output_ByteSize(1)(600)
!GLOBAL INT g_eCAT_Output_Type(1)(600)
!GLOBAL INT g_eCAT_Output_SlaveIndex(1)(600)

!GLOBAL INT g_eCAT_RS232_Output_Data(1)(200)

! *** Buffer #1 (IO Mapping Variable) ***
GLOBAL INT g_HMI_DI_PanelActive_Button
GLOBAL INT g_HMI_DI_DynoService_Button
GLOBAL INT g_HMI_DI_Monitor_Button
GLOBAL INT g_HMI_DI_Misc_Button
GLOBAL INT g_HMI_DI_Manual_Button
GLOBAL INT g_HMI_DI_Automatic_Button
GLOBAL INT g_HMI_DI_Remote_Button
GLOBAL INT g_HMI_DI_Seant_Button

GLOBAL INT g_HMI_DI_EngPreheatOn_Button
GLOBAL INT g_HMI_DI_EngIGOn_Button
GLOBAL INT g_HMI_DI_Stop_Button
GLOBAL INT g_HMI_DI_EngStart_Button
GLOBAL INT g_HMI_DI_EMS_Button
GLOBAL INT g_HMI_DI_BKReady_Button
GLOBAL INT g_HMI_DI_DynoOn_Button
GLOBAL INT g_HMI_DI_Reset_Button
GLOBAL INT g_HMI_DI_ConfigMenu_Button
GLOBAL INT g_HMI_DI_AlarmHistory_Button

GLOBAL INT g_HMI_DI_HornOff_Button
GLOBAL INT g_HMI_DI_IdleContrl_Button
GLOBAL INT g_HMI_DI_Idle_Button
GLOBAL INT g_HMI_DI_DyRPM_EngTA_Button
GLOBAL INT g_HMI_DI_DyTo_EngTA_Button
GLOBAL INT g_HMI_DI_DyRPM_EngTo_Button
GLOBAL INT g_HMI_DI_DyTo_EngRPM_Button
GLOBAL INT g_Flashing_Misc_On
GLOBAL INT g_HMI_DO_MiscOn_Lamp
GLOBAL INT g_HMI_DO_PanelActive_Lamp
GLOBAL INT g_HMI_DO_DynoService_Lamp
GLOBAL INT g_HMI_DO_Monitor_Lamp
GLOBAL INT g_HMI_DO_Manual_Lamp
GLOBAL INT g_HMI_DO_Automatic_Lamp
GLOBAL INT g_HMI_DO_Remote_Lamp
GLOBAL INT g_HMI_DO_Seant_Lamp

GLOBAL INT g_HMI_DO_EngPreheatOn_Lamp
GLOBAL INT g_HMI_DO_EngIGOn_Lamp
GLOBAL INT g_HMI_DO_Stop_Lamp
GLOBAL INT g_HMI_DO_EngStarting_Lamp

GLOBAL INT g_HMI_DO_DynoOn_Lamp
GLOBAL INT g_HMI_DO_AlarmReset_Lamp
GLOBAL INT g_HMI_DO_ConfigMenu_Lamp
GLOBAL INT g_HMI_DO_AlarmHistory_Lamp

GLOBAL INT g_HMI_DO_IdleControl_Lamp
GLOBAL INT g_HMI_DO_Idle_Lamp
GLOBAL INT g_HMI_DO_DyRPM_EngTA_Lamp
GLOBAL INT g_HMI_DO_DyTo_EngTA_Lamp
GLOBAL INT g_HMI_DO_DyRPM_EngTo_Lamp
GLOBAL INT g_HMI_DO_DyTo_EngRPM_Lamp

GLOBAL INT g_HMI_DO_HornOff_Lamp

GLOBAL INT g_HMI_AI_Dyno_CalRefCnt
GLOBAL INT g_HMI_AI_Eng_CalRefCnt
	
GLOBAL INT g_DI_DynoTroubleSignal
GLOBAL INT g_DI_RemoteOnSignal
GLOBAL INT g_DI_DynoSpeedModeSignal
GLOBAL INT g_DI_DynoTorqueModeSignal
GLOBAL INT g_DI_EngineAlphaModeSignal
GLOBAL INT g_DI_EngineTorqueModeSignal
GLOBAL INT g_DI_DynoOn_FeedBack

GLOBAL INT g_TEMP_WARNING
GLOBAL INT g_STO_FEEDBACK
GLOBAL INT g_DI_EMSButton
GLOBAL INT g_DI_SafetyGuardSignal
GLOBAL INT g_DI_SafetyRelaySignal

GLOBAL INT g_DO_BKReadySignal
GLOBAL INT g_DO_FANSignal; !Fan On
GLOBAL INT g_DO_DynoOnSignal
GLOBAL INT g_DO_ThermalShockValve1
GLOBAL INT g_DO_ThermalShockValve2
GLOBAL INT g_DO_ThermalShockHeater
GLOBAL INT g_DO_DynoResetSignal
GLOBAL INT g_DO_Seant_On; !BK Ready

GLOBAL INT g_DO_ThrottleIVSSignal
GLOBAL INT g_DO_PreHeatSignal
GLOBAL INT g_DO_StartSignal
GLOBAL INT g_DO_IgnitionSignal
GLOBAL INT g_DO_X11PResetOnSignal
GLOBAL INT g_DO_HornSignal
GLOBAL INT g_DO_Alarm_Lamp

GLOBAL REAL g_AI_Target_Dyno_Remote
GLOBAL REAL g_AI_Target_Engine_Remote
GLOBAL REAL g_AI_Feedback_Alpha

GLOBAL INT g_AI_Feedback_DynoSpeed

GLOBAL REAL g_AI_Feedback_Vdiff
GLOBAL REAL g_AI_Feedback_Vref
GLOBAL REAL g_AI_Feedback_Torque
GLOBAL REAL g_AI_Feedback_TorqueAvg

GLOBAL INT g_AO_DynoSpeedActualSignal
GLOBAL INT g_AO_DynoTorqueActualSignal
GLOBAL INT g_AO_DynoControlSignal
GLOBAL INT g_AO_ThrottleControlSignal
GLOBAL INT g_AO_Feedback_Alpha

GLOBAL INT g_AO_AlphaASignal
GLOBAL INT g_AO_AlphaBSignal

GLOBAL INT g_PID_MaxRatio;

GLOBAL 	INT PANEL_SW;
GLOBAL 	INT SP1_SW;
GLOBAL 	INT SP3_SW;
GLOBAL 	INT SP4_SW;
GLOBAL 	INT SP5_SW;
GLOBAL 	INT SP6_SW;
GLOBAL 	INT SP8_SW;
GLOBAL 	INT SP9_SW;
GLOBAL 	INT SP10_SW;
GLOBAL 	INT SP11_SW;
GLOBAL 	INT SP1_LP;
GLOBAL 	INT SP3_LP;
GLOBAL 	INT SP4_LP;
GLOBAL 	INT SP5_LP;
GLOBAL 	INT SP6_LP;
GLOBAL 	INT SP7_LP;
GLOBAL 	INT SP8_LP;
GLOBAL 	INT SP9_LP;
GLOBAL 	INT SP10_LP;
GLOBAL 	INT SP11_LP;
GLOBAL 	INT KNOB_DYNO_Period;
GLOBAL 	INT KNOB_ENGINE_Period;
GLOBAL 	INT DO_TS_VALVE1;
GLOBAL 	INT DO_TS_VALVE2;
GLOBAL 	INT DO_TS_HEATER;
GLOBAL 	INT g_AO_CWTEMP_SV;
! *** Buffer #2 (Alarm) ***
GLOBAL INT g_Alarm_Exists_Bit;
GLOBAL INT g_Error_Exists_Bit;
GLOBAL INT g_Warning_Exists_Bit;

GLOBAL INT g_Alarm_Register_Request_AlarmID;
GLOBAL INT g_Alarm_Register_Request_Bit;

! *** Buffer #3 (Mode Control Program) ***
GLOBAL INT g_SystemState_Dyno_On;
GLOBAL INT g_SystemState_Reset;
GLOBAL INT g_SystemState_EMS;
GLOBAL INT g_SystemState_Eng_IG_On;
GLOBAL INT g_SystemState_Eng_Preheat_On; 
GLOBAL INT g_SystemState_Eng_Starting;
GLOBAL INT g_SystemState_Eng_Started;
GLOBAL INT g_SystemState_Misc_On; !BK Ready

GLOBAL INT g_PID_Change_Ramp; ! PID Change Ramp Timer

GLOBAL INT g_stop_StopMode;
GLOBAL INT g_stop_enum_Default
GLOBAL INT g_stop_enum_Stopping;
GLOBAL INT g_stop_enum_IdleStopping
GLOBAL INT g_stop_enum_SoftStopping
GLOBAL INT g_stop_enum_HardStopping
GLOBAL INT g_stop_enum_Stopped;

GLOBAL INT g_ControlMode
GLOBAL INT g_CtMo_enum_Default
GLOBAL INT g_CtMo_enum_Monitor
GLOBAL INT g_CtMo_enum_OpenLoopTest
GLOBAL INT g_CtMo_enum_Manual
GLOBAL INT g_CtMo_enum_Remote
GLOBAL INT g_CtMo_enum_Automatic

GLOBAL INT g_DriveManual_Mode;
GLOBAL INT g_DrMn_enum_Default;
GLOBAL INT g_DrMn_enum_Idle;
GLOBAL INT g_DrMn_enum_IdleContrl;
GLOBAL INT g_DrMn_enum_DyTo_EngTA;
GLOBAL INT g_DrMn_enum_DyRPM_EngTA;
GLOBAL INT g_DrMn_enum_DyTo_EngRPM;
GLOBAL INT g_DrMn_enum_DyRPM_EngTo;	

GLOBAL INT g_dynoCtrl_OilPumpOnRPM;

! Automation S/W Write Data
GLOBAL INT g_PC_Write_Data(0x2FF);
GLOBAL REAL g_rPC_Write_Data(0x2FF);
GLOBAL INT g_index_PcWrite_PcStatus;
GLOBAL INT g_index_PcWrite_ControlCMD;
GLOBAL INT g_index_PcWrite_RunDriveMode;
GLOBAL INT g_index_PcWrite_ProfileWrite;
GLOBAL INT g_index_PcWrite_MeasureEqpCMD;
GLOBAL INT g_index_PcWrite_eCatOutputData;
GLOBAL INT g_Automatic_Eng_IG_On;

! *** Buffer #3 (HMI Variable) ***
GLOBAL REAL gl_HmiVar_FlashWrite(900);
GLOBAL REAL gl_HmiVar_FlashRead(900);
GLOBAL INT g_Hmi_UserDefine_TagCount;

!GLOBAL REAL TAG 112 Reserved_112
! *** Power 
! P[hp] = ((T[ft lbf])(w[RPM)) / 5252
! P[hp] = ((T[ft lbf] * 0.738)(w[RPM)) / 5252
! Power[Kw] = P[hp] * 0.74569

! *** Buffer #4 (Dynamometer & Engine PID Controller Management Program) ***
GLOBAL REAL g_Target_Dyno_Speed;
GLOBAL REAL g_Target_Dyno_Torque;
GLOBAL REAL g_Target_Dyno_Current;
GLOBAL REAL g_Target_Engine_Speed;
GLOBAL REAL g_Target_Engine_Torque;
GLOBAL REAL g_Target_Engine_AlphaPosi;
!jhkim
GLOBAL REAL g_Calc_Torque_VoltToTorque_CalcResult;
GLOBAL REAL g_Ave_Calc_Torque_VoltToTorque_CalcResult;

GLOBAL REAL g_Feedback_Speed;
GLOBAL REAL g_Feedback_SpeedUpdateTime;

GLOBAL REAL g_Feedback_Torque;
GLOBAL REAL g_Feedback_AlphaPosi;
GLOBAL REAL g_Monitoring_TorqueAvg
! Operation Pannel
GLOBAL REAL g_Op_Demand_Initial_Speed;
GLOBAL REAL g_Op_Demand_Initial_Torque;
GLOBAL REAL g_Op_Demand_Initial_AlphaPosi;
GLOBAL REAL g_Op_Demand_EngSpeed_CalRefCnt;
GLOBAL REAL g_Op_Demand_EngTorque_CalRefCnt;
GLOBAL REAL g_Op_Demand_EngAlphaPosi_CalRefCnt;
GLOBAL REAL g_Op_Demand_DynoSpeed_CalRefCnt;
GLOBAL REAL g_Op_Demand_DynoTorque_CalRefCnt;

GLOBAL REAL g_Op_Demand_Initial_Current;
GLOBAL REAL g_Op_Demand_DynoCurrent_CalRefCnt;

!PC Profile Write & Process
GLOBAL REAL g_PC_Write_Data_TqAlphaProfile(5100);
GLOBAL REAL g_PC_Write_Data_SpAlphaProfile(5100);
GLOBAL REAL g_PC_Write_Data_TqSpProfile(5100);
GLOBAL REAL g_PC_Write_Data_SpTqProfile(5100);
GLOBAL REAL g_PC_Process_Data_TqAlphaProfile(5100);
GLOBAL REAL g_PC_Process_Data_SpAlphaProfile(5100);
GLOBAL REAL g_PC_Process_Data_TqSpProfile(5100);
GLOBAL REAL g_PC_Process_Data_SpTqProfile(5100);

! *** Buffer #5 (Dynamometer PID Control Program) ***	
GLOBAL INT g_Dyno_Control_Operable;	
GLOBAL INT g_Dyno_ControlMode;	
GLOBAL INT g_Dyno_PidControl_StateMachine;	
GLOBAL REAL g_Dyno_PID_Process_ValList(1)(29);	
GLOBAL REAL g_Dyno_PID_DataLogger_Matrix(10000)(29);	

! *** Buffer #6 (Engine PID Control Program) ***	
GLOBAL INT g_Eng_Control_Operable;	
GLOBAL INT g_Eng_ControlMode;	
GLOBAL INT g_Eng_PidControl_StateMachine;	
GLOBAL REAL g_Eng_PID_Process_ValList(1)(29);	
GLOBAL REAL g_Eng_PID_DataLogger_Matrix(10000)(29);	


! *** Buffer #7 (DAQ) ***
GLOBAL INT g_DAQ_Enable;
GLOBAL REAL g_DAQ_Period;

GLOBAL INT g_PC_Read_Data(1)(7400)

! *** Buffer #8 (HMI Interface Program) ***
GLOBAL INT g_EcDynoCurPI_Operable;
GLOBAL REAL g_EcDynoCurPI_Process_ValList(1)(28);
GLOBAL REAL g_EcDynoCurPI_DataLogger_Matrix(10000)(28);	

GLOBAL INT g_Bf8_AutoR_Banner_Display;
!	***	HMI	System	Variable		
GLOBAL	INT	TAG	1000	gHmi_Sys_CurrentScreen		
GLOBAL	INT	TAG	1001	gHmi_Sys_ErrorStatus		
GLOBAL	INT	TAG	1002	gHmi_Sys_ClockData_0		
GLOBAL	INT	TAG	1003	gHmi_Sys_ClockData_1		
GLOBAL	INT	TAG	1004	gHmi_Sys_ClockData_2		
GLOBAL	INT	TAG	1005	gHmi_Sys_ClockData_3		
GLOBAL	INT	TAG	1006	gHmi_Sys_Status		
GLOBAL	INT	TAG	1007	Reserved_1007		
GLOBAL	INT	TAG	1008	g_Hmi_Sys_ChangeToScreed		
GLOBAL	INT	TAG	1009	g_Hmi_Sys_ScreenDisplayOnOff		
GLOBAL	INT	TAG	1010	g_Hmi_Sys_ClockData_0		
GLOBAL	INT	TAG	1011	g_Hmi_Sys_ClockData_1		
GLOBAL	INT	TAG	1012	g_Hmi_Sys_ClockData_2		
GLOBAL	INT	TAG	1013	g_Hmi_Sys_ClockData_3		
GLOBAL	INT	TAG	1014	g_Hmi_Sys_Control		
GLOBAL	INT	TAG	1015	Reserved_1015		
GLOBAL	INT	TAG	1016	g_Hmi_Sys_WindowControl		
GLOBAL	INT	TAG	1017	g_Hmi_Sys_WindowScreen		
GLOBAL	INT	TAG	1018	g_Hmi_Sys_WindowsDisPosi_1		
GLOBAL	INT	TAG	1019	g_Hmi_Sys_WindowsDisPosi_2		
!	***	Base	Screens	Header	#1	
GLOBAL	REAL	TAG	1020	g_Hmi_bsHeader1_Fb_Speed		
GLOBAL	REAL	TAG	1021	g_Hmi_bsHeader1_Fb_AlphaPosi		
GLOBAL	REAL	TAG	1022	g_Hmi_bsHeader1_Fb_Torque		
GLOBAL	REAL	TAG	1023	g_Hmi_bsHeader1_Fb_Power		
!	***	Base	Screens	#100	~	
GLOBAL	REAL	TAG	1024	g_Hmi_bs10x_Demand_Speed		
GLOBAL	REAL	TAG	1025	g_Hmi_bs10x_Demand_AlphaPosi		
GLOBAL	REAL	TAG	1026	g_Hmi_bs10x_Demand_Torque		
GLOBAL	REAL	TAG	1027	g_Hmi_bs10x_Demand_GraphZeroVal;		
GLOBAL	REAL	TAG	1028	Reserved_1028		
GLOBAL	REAL	TAG	1029	Reserved_1029		
GLOBAL	REAL	TAG	1030	Reserved_1030		
!	***	Base	Screens	Header	#2	
GLOBAL	REAL	TAG	1031	g_Hmi_bsHeader2_Fb_DynoCurrent		
!	***	Base	Screens	#110	~	
GLOBAL	REAL	TAG	1032	g_Hmi_bs110_Demand_DynoCurrent	
!	***	Base	Screens	#500x	~
GLOBAL	INT 	TAG	1033	g_Hmi_bs500x_FB_SpeedADValMon		
GLOBAL	INT 	TAG	1034	g_Hmi_bs510x_FB_TorqueADValMon		
GLOBAL	INT 	TAG	1035	Reserved_1035		
GLOBAL	INT 	TAG	1036	g_Hmi_bs530x_FB_CurrentADValMon		
GLOBAL	REAL	TAG	1037	Reserved_1037		
GLOBAL	REAL	TAG	1038	Reserved_1038		
GLOBAL	REAL	TAG	1039	Reserved_1039		
GLOBAL	REAL	TAG	1040	Reserved_1040		
GLOBAL	REAL	TAG	1041	Reserved_1041		
GLOBAL	REAL	TAG	1042	Reserved_1042		
GLOBAL	REAL	TAG	1043	Reserved_1043		
GLOBAL	REAL	TAG	1044	Reserved_1044		
GLOBAL	REAL	TAG	1045	Reserved_1045		
GLOBAL	REAL	TAG	1046	Reserved_1046		
GLOBAL	REAL	TAG	1047	Reserved_1047		
GLOBAL	REAL	TAG	1048	Reserved_1048		
GLOBAL	REAL	TAG	1049	Reserved_1049		
GLOBAL	REAL	TAG	1050	Reserved_1050		
GLOBAL	REAL	TAG	1051	Reserved_1051		
GLOBAL	REAL	TAG	1052	Reserved_1052		
GLOBAL	REAL	TAG	1053	Reserved_1053		
GLOBAL	REAL	TAG	1054	Reserved_1054		
GLOBAL	REAL	TAG	1055	Reserved_1055		
GLOBAL	REAL	TAG	1056	Reserved_1056		
GLOBAL	REAL	TAG	1057	Reserved_1057		
GLOBAL	REAL	TAG	1058	Reserved_1058		
GLOBAL	REAL	TAG	1059	Reserved_1059		
GLOBAL	REAL	TAG	1060	Reserved_1060
!	***	BS700x Alarm
GLOBAL	INT	TAG	1061	g_Hmi_bs700x_Alarm_ID		
GLOBAL	REAL	TAG	1062	Reserved_1062		
GLOBAL	REAL	TAG	1063	Reserved_1063		
GLOBAL	REAL	TAG	1064	Reserved_1064		
GLOBAL	REAL	TAG	1065	Reserved_1065		
GLOBAL	REAL	TAG	1066	Reserved_1066		
GLOBAL	REAL	TAG	1067	Reserved_1067		
GLOBAL	REAL	TAG	1068	Reserved_1068		
GLOBAL	REAL	TAG	1069	Reserved_1069		
GLOBAL	REAL	TAG	1070	Reserved_1070		
!	***	Base	Screens	#5501	cf_Cal_Current_Calibration-2
GLOBAL	INT 	TAG	1071	g_Hmi_bs5501_Dyno_CIndexEd		
GLOBAL	INT 	TAG	1072	g_Hmi_bs5501_Dyno_CIndexSt		
GLOBAL	REAL	TAG	1073	Reserved_1073		
GLOBAL	REAL	TAG	1074	Reserved_1074		
GLOBAL	REAL	TAG	1075	Reserved_1075		
GLOBAL	REAL	TAG	1076	Reserved_1076		
GLOBAL	REAL	TAG	1077	Reserved_1077		
GLOBAL	REAL	TAG	1078	Reserved_1078		
GLOBAL	REAL	TAG	1079	Reserved_1079		
GLOBAL	REAL	TAG	1080	Reserved_1080		
GLOBAL	REAL	TAG	1081	Reserved_1081		
GLOBAL	REAL	TAG	1082	Reserved_1082		
GLOBAL	REAL	TAG	1083	Reserved_1083		
GLOBAL	REAL	TAG	1084	Reserved_1084		
GLOBAL	REAL	TAG	1085	Reserved_1085		
GLOBAL	REAL	TAG	1086	Reserved_1086		
GLOBAL	REAL	TAG	1087	Reserved_1087		
GLOBAL	REAL	TAG	1088	Reserved_1088		
GLOBAL	REAL	TAG	1089	Reserved_1089		
GLOBAL	REAL	TAG	1090	Reserved_1090	
!	***	Base	Screens	#5502	cf_Cal_Current_Calibration-2~4
GLOBAL	INT		TAG	1091	g_Hmi_bs5502_Output_CurrentCalAd_Val0Ed		
GLOBAL	INT		TAG	1092	g_Hmi_bs5502_Output_CurrentCalAd_Val0St		
GLOBAL	INT		TAG	1093	g_Hmi_bs5502_Output_CurrentCalAd_Val1Ed		
GLOBAL	INT		TAG	1094	g_Hmi_bs5502_Output_CurrentCalAd_Val1St		
GLOBAL	INT		TAG	1095	g_Hmi_bs5502_Output_CurrentCalAd_Val2Ed		
GLOBAL	INT		TAG	1096	g_Hmi_bs5502_Output_CurrentCalAd_Val2St		
GLOBAL	INT		TAG	1097	g_Hmi_bs5502_Output_CurrentCalAd_Val3Ed		
GLOBAL	INT		TAG	1098	g_Hmi_bs5502_Output_CurrentCalAd_Val3St		
GLOBAL	INT		TAG	1099	g_Hmi_bs5502_Output_CurrentCalAd_Val4Ed		
GLOBAL	INT		TAG	1100	g_Hmi_bs5502_Output_CurrentCalAd_Val4St		
GLOBAL	INT		TAG	1101	g_Hmi_bs5503_Output_CurrentCalAd_Val5Ed		
GLOBAL	INT		TAG	1102	g_Hmi_bs5503_Output_CurrentCalAd_Val5St		
GLOBAL	INT		TAG	1103	g_Hmi_bs5503_Output_CurrentCalAd_Val6Ed		
GLOBAL	INT		TAG	1104	g_Hmi_bs5503_Output_CurrentCalAd_Val6St		
GLOBAL	INT		TAG	1105	g_Hmi_bs5503_Output_CurrentCalAd_Val7Ed		
GLOBAL	INT		TAG	1106	g_Hmi_bs5503_Output_CurrentCalAd_Val7St		
GLOBAL	INT		TAG	1107	g_Hmi_bs5503_Output_CurrentCalAd_Val8Ed		
GLOBAL	INT		TAG	1108	g_Hmi_bs5503_Output_CurrentCalAd_Val8St		
GLOBAL	INT		TAG	1109	g_Hmi_bs5503_Output_CurrentCalAd_Val9Ed		
GLOBAL	INT		TAG	1110	g_Hmi_bs5503_Output_CurrentCalAd_Val9St		
GLOBAL	INT		TAG	1111	g_Hmi_bs5504_Output_CurrentCalAd_Val10Ed		
GLOBAL	INT		TAG	1112	g_Hmi_bs5504_Output_CurrentCalAd_Val10St		
GLOBAL	INT		TAG	1113	Reserved_1113		
GLOBAL	INT		TAG	1114	Reserved_1114		
GLOBAL	INT		TAG	1115	g_Hmi_bs5502_Output_CurrentCal_Val0Ed		
GLOBAL	INT		TAG	1116	g_Hmi_bs5502_Output_CurrentCal_Val0St		
GLOBAL	INT		TAG	1117	g_Hmi_bs5502_Output_CurrentCal_Val1Ed		
GLOBAL	INT		TAG	1118	g_Hmi_bs5502_Output_CurrentCal_Val1St	
GLOBAL	INT		TAG	1119	g_Hmi_bs5502_Output_CurrentCal_Val2Ed		
GLOBAL	INT		TAG	1120	g_Hmi_bs5502_Output_CurrentCal_Val2St		
GLOBAL	INT		TAG	1121	g_Hmi_bs5502_Output_CurrentCal_Val3Ed		
GLOBAL	INT		TAG	1122	g_Hmi_bs5502_Output_CurrentCal_Val3St		
GLOBAL	INT		TAG	1123	g_Hmi_bs5502_Output_CurrentCal_Val4Ed		
GLOBAL	INT		TAG	1124	g_Hmi_bs5502_Output_CurrentCal_Val4St		
GLOBAL	INT		TAG	1125	g_Hmi_bs5503_Output_CurrentCal_Val5Ed		
GLOBAL	INT		TAG	1126	g_Hmi_bs5503_Output_CurrentCal_Val5St		
GLOBAL	INT		TAG	1127	g_Hmi_bs5503_Output_CurrentCal_Val6Ed		
GLOBAL	INT		TAG	1128	g_Hmi_bs5503_Output_CurrentCal_Val6St		
GLOBAL	INT		TAG	1129	g_Hmi_bs5503_Output_CurrentCal_Val7Ed		
GLOBAL	INT		TAG	1130	g_Hmi_bs5503_Output_CurrentCal_Val7St		
GLOBAL	INT		TAG	1131	g_Hmi_bs5503_Output_CurrentCal_Val8Ed		
GLOBAL	INT		TAG	1132	g_Hmi_bs5503_Output_CurrentCal_Val8St		
GLOBAL	INT		TAG	1133	g_Hmi_bs5503_Output_CurrentCal_Val9Ed		
GLOBAL	INT		TAG	1134	g_Hmi_bs5503_Output_CurrentCal_Val9St		
GLOBAL	INT		TAG	1135	g_Hmi_bs5504_Output_CurrentCal_Val10Ed		
GLOBAL	INT		TAG	1136	g_Hmi_bs5504_Output_CurrentCal_Val10St		
GLOBAL	INT		TAG	1137	Reserved_1137		
GLOBAL	INT		TAG	1138	Reserved_1138		
GLOBAL	INT		TAG	1139	Reserved_1139		
GLOBAL	INT		TAG	1140	g_Hmi_bs550x_CtrlVal		
!	***	Base	Screens	#2201	cf_Sys_OperationPannelConfig-1
GLOBAL	INT 	TAG	1141	g_Hmi_bs2201_EditSaveTOutEd		
GLOBAL	INT 	TAG	1142	g_Hmi_bs2201_EditSaveTOutSt		
GLOBAL	REAL	TAG	1143	Reserved_1143		
GLOBAL	REAL	TAG	1144	Reserved_1144		
GLOBAL	REAL	TAG	1145	Reserved_1145		
GLOBAL	REAL	TAG	1146	Reserved_1146		
GLOBAL	REAL	TAG	1147	Reserved_1147		
GLOBAL	REAL	TAG	1148	Reserved_1148		
GLOBAL	REAL	TAG	1149	Reserved_1149		
GLOBAL	REAL	TAG	1150	Reserved_1150		
!	***	Base	Screens	#2202	cf_Sys_OperationPannelConfig-2			
GLOBAL	INT		TAG	1151	g_Hmi_bs2202_SpeedCngPerKnobPulseEd		
GLOBAL	INT		TAG	1152	g_Hmi_bs2202_SpeedCngPerKnobPulseSt		
GLOBAL	INT		TAG	1153	g_Hmi_bs2202_TorqueCngPerKnobPulseEd		
GLOBAL	INT		TAG	1154	g_Hmi_bs2202_TorqueCngPerKnobPulseSt		
GLOBAL	INT 	TAG	1155	g_Hmi_bs2202_AlphaCngPerKnobPulseEd		
GLOBAL	INT 	TAG	1156	g_Hmi_bs2202_AlphaCngPerKnobPulseSt		
GLOBAL	INT 	TAG	1157	g_Hmi_bs2202_CurrentCngPerKnobPulseEd		
GLOBAL	INT 	TAG	1158	g_Hmi_bs2202_CurrentCngPerKnobPulseSt		
GLOBAL	REAL	TAG	1159	Reserved_1159		
GLOBAL	REAL	TAG	1160	Reserved_1160		
GLOBAL	REAL	TAG	1161	Reserved_1161		
GLOBAL	REAL	TAG	1162	Reserved_1162		
GLOBAL	REAL	TAG	1163	Reserved_1163		
GLOBAL	REAL	TAG	1164	Reserved_1164		
GLOBAL	REAL	TAG	1165	Reserved_1165		
GLOBAL	REAL	TAG	1166	Reserved_1166		
GLOBAL	REAL	TAG	1167	Reserved_1167		
GLOBAL	REAL	TAG	1168	Reserved_1168		
GLOBAL	REAL	TAG	1169	Reserved_1169		
GLOBAL	INT 	TAG	1170	g_Hmi_bs220x_CtrlVal
!	***	Base	Screens	#2301	cf_Sys_DAQConfig-1
GLOBAL	INT		TAG	1171	g_Hmi_bs2301_DAQ_Avg_NumberEd		
GLOBAL	INT		TAG	1172	g_Hmi_bs2301_DAQ_Avg_NumberSt
GLOBAL	REAL	TAG	1173	Reserved_1173		
GLOBAL	REAL	TAG	1174	Reserved_1174		
GLOBAL	REAL	TAG	1175	Reserved_1175		
GLOBAL	REAL	TAG	1176	Reserved_1176		
GLOBAL	REAL	TAG	1177	g_Hmi_bs2302_Emission_FuelDensityEd		
GLOBAL	REAL	TAG	1178	g_Hmi_bs2302_Emission_FuelDensitySt		
GLOBAL	INT 	TAG	1179	g_Hmi_bs2302_Emission_AtmCo2ppmEd		
GLOBAL	INT 	TAG	1180	g_Hmi_bs2302_Emission_AtmCo2ppmSt		
GLOBAL	REAL	TAG	1181	Reserved_1181		
GLOBAL	REAL	TAG	1182	Reserved_1182		
GLOBAL	INT 	TAG	1183	g_Hmi_bs2303_Emission_TestCycleEd		
GLOBAL	INT 	TAG	1184	g_Hmi_bs2303_Emission_TestCycleSt		
GLOBAL	REAL	TAG	1185	Reserved_1185		
GLOBAL	REAL	TAG	1186	Reserved_1186		
GLOBAL	REAL	TAG	1187	Reserved_1187		
GLOBAL	REAL	TAG	1188	Reserved_1188		
GLOBAL	REAL	TAG	1189	Reserved_1189		
GLOBAL	REAL	TAG	1190	Reserved_1190		
GLOBAL	REAL	TAG	1191	Reserved_1191		
GLOBAL	REAL	TAG	1192	Reserved_1192		
GLOBAL	REAL	TAG	1193	Reserved_1193		
GLOBAL	REAL	TAG	1194	Reserved_1194		
GLOBAL	REAL	TAG	1195	Reserved_1195		
GLOBAL	REAL	TAG	1196	Reserved_1196		
GLOBAL	REAL	TAG	1197	Reserved_1197		
GLOBAL	REAL	TAG	1198	Reserved_1198		
GLOBAL	REAL	TAG	1199	Reserved_1199		
GLOBAL	INT		TAG	1200	g_Hmi_bs230x_CtrlVal	
!	***	Base	Screens	#3001	cf_Engine_VarSetting-1
GLOBAL	INT	    TAG	1201	g_Hmi_bs3001_Eng_StartProcedureEd		
GLOBAL	INT 	TAG	1202	g_Hmi_bs3001_Eng_StartProcedureSt		
GLOBAL	REAL	TAG	1203	g_Hmi_bs3001_Eng_DynoStartTorqueEd		
GLOBAL	REAL	TAG	1204	g_Hmi_bs3001_Eng_DynoStartTorqueSt	
!	***	Base	Screens	#3002	cf_Engine_VarSetting-2
GLOBAL	REAL	TAG	1205	g_Hmi_bs3002_Eng_StartTimeOutEd		
GLOBAL	REAL	TAG	1206	g_Hmi_bs3002_Eng_StartTimeOutSt	
GLOBAL	REAL	TAG	1207	g_Hmi_bs3002_Eng_StartCompSpeedEd		
GLOBAL	REAL	TAG	1208	g_Hmi_bs3002_Eng_StartCompSpeedSt	
!	***	Base	Screens	#3003	cf_Engine_VarSetting-3
GLOBAL	REAL	TAG	1209	g_Hmi_bs3003_Eng_PreheatTimeEd		
GLOBAL	REAL	TAG	1210	g_Hmi_bs3003_Eng_PreheatTimeSt	
GLOBAL	REAL	TAG	1211	g_Hmi_bs3003_Eng_StopTorqueEd		
GLOBAL	REAL	TAG	1212	g_Hmi_bs3003_Eng_StopTorqueSt		
GLOBAL	REAL	TAG	1213	g_Hmi_bs3003_Eng_StopAlphaEd		
GLOBAL	REAL	TAG	1214	g_Hmi_bs3003_Eng_StopAlphaSt
!	***	Base	Screens	#3004	cf_Engine_VarSetting-4
GLOBAL	REAL 	TAG	1215	g_Hmi_bs3004_Eng_MaxSpeedEd		
GLOBAL	REAL 	TAG	1216	g_Hmi_bs3004_Eng_MaxSpeedSt	
GLOBAL	REAL 	TAG	1217	g_Hmi_bs3004_Eng_IdleMinSpeedEd	
GLOBAL	REAL 	TAG	1218	g_Hmi_bs3004_Eng_IdleMinSpeedSt
!	***	Base	Screens	#3005	cf_Engine_VarSetting-5
GLOBAL	REAL	TAG	1219	g_Hmi_bs3005_Eng_MaxTorqueEd		
GLOBAL	REAL	TAG	1220	g_Hmi_bs3005_Eng_MaxTorqueSt		
GLOBAL	REAL	TAG	1221	g_Hmi_bs3005_Eng_MinTorqueEd		
GLOBAL	REAL	TAG	1222	g_Hmi_bs3005_Eng_MinTorqueSt
!	***	Base	Screens	#3006	cf_Engine_VarSetting-6
GLOBAL	INT		TAG	1223	g_Hmi_bs3006_Eng_Num_StrokeCycleEd		
GLOBAL	INT		TAG	1224	g_Hmi_bs3006_Eng_Num_StrokeCycleSt		
GLOBAL	INT		TAG	1225	g_Hmi_bs3006_Eng_Num_EngCylindersEd		
GLOBAL	INT		TAG	1226	g_Hmi_bs3006_Eng_Num_EngCylindersSt	
GLOBAL	REAL	TAG	1227	g_Hmi_bs3006_Eng_CylinderBoreEd		
GLOBAL	REAL	TAG	1228	g_Hmi_bs3006_Eng_CylinderBoreSt		
!	***	Base	Screens	#3007	cf_Engine_VarSetting-7
GLOBAL	REAL	TAG	1229	g_Hmi_bs3007_Eng_CylinderStrokeEd		
GLOBAL	REAL	TAG	1230	g_Hmi_bs3007_Eng_CylinderStrokeSt		
GLOBAL	INT 	TAG	1231	g_Hmi_bs3007_Eng_Num_EngFuelTypeEd		
GLOBAL	INT 	TAG	1232	g_Hmi_bs3007_Eng_Num_EngFuelTypeSt		
!	***	Base	Screens	#3007	cf_Engine_VarSetting-7
GLOBAL	INT		TAG	1233	g_Hmi_bs3008_Eng_StopRPMEd		
GLOBAL	INT		TAG	1234	g_Hmi_bs3008_Eng_StopRPMSt		
GLOBAL	INT		TAG	1235	Reserved_1235		
GLOBAL	REAL	TAG	1236	g_Hmi_bs3008_PID_Change_Ramp		
GLOBAL	REAL	TAG	1237	Reserved_1237		
GLOBAL	REAL	TAG	1238	Reserved_1238		
GLOBAL	REAL	TAG	1239	Reserved_1239		
GLOBAL	INT		TAG	1240	g_Hmi_bs300x_CtrlVal		
GLOBAL	REAL	TAG	1241	Reserved_1241		
GLOBAL	REAL	TAG	1242	Reserved_1242		
GLOBAL	REAL	TAG	1243	Reserved_1243		
GLOBAL	REAL	TAG	1244	Reserved_1244		
GLOBAL	REAL	TAG	1245	Reserved_1245		
GLOBAL	REAL	TAG	1246	Reserved_1246		
GLOBAL	REAL	TAG	1247	Reserved_1247		
GLOBAL	REAL	TAG	1248	Reserved_1248		
GLOBAL	REAL	TAG	1249	Reserved_1249		
GLOBAL	REAL	TAG	1250	Reserved_1250		
GLOBAL	REAL	TAG	1251	Reserved_1251		
GLOBAL	REAL	TAG	1252	Reserved_1252		
GLOBAL	REAL	TAG	1253	Reserved_1253		
GLOBAL	REAL	TAG	1254	Reserved_1254		
GLOBAL	REAL	TAG	1255	Reserved_1255		
GLOBAL	REAL	TAG	1256	Reserved_1256		
GLOBAL	REAL	TAG	1257	Reserved_1257		
GLOBAL	REAL	TAG	1258	Reserved_1258		
GLOBAL	REAL	TAG	1259	Reserved_1259		
GLOBAL	REAL	TAG	1260	Reserved_1260		
GLOBAL	REAL	TAG	1261	Reserved_1261		
GLOBAL	REAL	TAG	1262	Reserved_1262		
GLOBAL	REAL	TAG	1263	Reserved_1263		
GLOBAL	REAL	TAG	1264	Reserved_1264		
GLOBAL	REAL	TAG	1265	Reserved_1265		
GLOBAL	REAL	TAG	1266	Reserved_1266		
GLOBAL	REAL	TAG	1267	Reserved_1267		
GLOBAL	REAL	TAG	1268	Reserved_1268		
GLOBAL	REAL	TAG	1269	Reserved_1269		
GLOBAL	REAL	TAG	1270	Reserved_1270		
GLOBAL	REAL	TAG	1271	Reserved_1271		
GLOBAL	REAL	TAG	1272	Reserved_1272		
GLOBAL	REAL	TAG	1273	Reserved_1273		
GLOBAL	REAL	TAG	1274	Reserved_1274		
GLOBAL	REAL	TAG	1275	Reserved_1275		
GLOBAL	REAL	TAG	1276	Reserved_1276		
GLOBAL	REAL	TAG	1277	Reserved_1277		
GLOBAL	REAL	TAG	1278	Reserved_1278		
GLOBAL	REAL	TAG	1279	Reserved_1279		
GLOBAL	REAL	TAG	1280	Reserved_1280		
GLOBAL	REAL	TAG	1281	Reserved_1281		
GLOBAL	REAL	TAG	1282	Reserved_1282		
GLOBAL	REAL	TAG	1283	Reserved_1283		
GLOBAL	REAL	TAG	1284	Reserved_1284		
GLOBAL	REAL	TAG	1285	Reserved_1285		
GLOBAL	REAL	TAG	1286	Reserved_1286		
GLOBAL	REAL	TAG	1287	Reserved_1287		
GLOBAL	REAL	TAG	1288	Reserved_1288		
GLOBAL	REAL	TAG	1289	Reserved_1289		
GLOBAL	REAL	TAG	1290	Reserved_1290		
GLOBAL	REAL	TAG	1291	Reserved_1291		
GLOBAL	REAL	TAG	1292	Reserved_1292		
GLOBAL	REAL	TAG	1293	Reserved_1293		
GLOBAL	REAL	TAG	1294	Reserved_1294		
GLOBAL	REAL	TAG	1295	Reserved_1295		
GLOBAL	REAL	TAG	1296	Reserved_1296		
GLOBAL	REAL	TAG	1297	Reserved_1297		
GLOBAL	REAL	TAG	1298	Reserved_1298		
GLOBAL	REAL	TAG	1299	Reserved_1299		
GLOBAL	REAL	TAG	1300	Reserved_1300	
!	***	Base	Screens	#4001	cf_Ctrl_Dynamometer_N	PID-1
GLOBAL	REAL	TAG	1301	g_Hmi_bs4001_DynoSpid_KpEd		
GLOBAL	REAL	TAG	1302	g_Hmi_bs4001_DynoSpid_KpSt		
GLOBAL	REAL	TAG	1303	g_Hmi_bs4001_DynoSpid_KiEd		
GLOBAL	REAL	TAG	1304	g_Hmi_bs4001_DynoSpid_KiSt		
GLOBAL	REAL	TAG	1305	g_Hmi_bs4001_DynoSpid_KdEd		
GLOBAL	REAL	TAG	1306	g_Hmi_bs4001_DynoSpid_Kdst
GLOBAL	INT		TAG	1307	Reserved_1307		
GLOBAL	INT		TAG	1308	Reserved_1308		
GLOBAL	REAL	TAG	1309	Reserved_1309		
GLOBAL	REAL	TAG	1310	Reserved_1310		
GLOBAL	REAL	TAG	1311	Reserved_1311		
GLOBAL	REAL	TAG	1312	Reserved_1312		
!	***	Base	Screens	#4002	cf_Ctrl_Dynamometer_N	PID-2
GLOBAL	INT		TAG	1313	Reserved_1313		
GLOBAL	INT		TAG	1314	Reserved_1314		
GLOBAL	REAL	TAG	1315	g_Hmi_bs4002_DynoSpid_YMinEd		
GLOBAL	REAL	TAG	1316	g_Hmi_bs4002_DynoSpid_YMinSt		
GLOBAL	REAL	TAG	1317	Reserved_1317		
GLOBAL	REAL	TAG	1318	Reserved_1318	
GLOBAL	REAL	TAG	1319	Reserved_1319		
GLOBAL	REAL	TAG	1320	Reserved_1320		
GLOBAL	REAL	TAG	1321	g_Hmi_bs4002_DynoSpid_RefADeltaEd		
GLOBAL	REAL	TAG	1322	g_Hmi_bs4002_DynoSpid_RefADeltaSt		
GLOBAL	REAL	TAG	1323	g_Hmi_bs4002_DynoSpid_RefDDeltaEd		
GLOBAL	REAL	TAG	1324	g_Hmi_bs4002_DynoSpid_RefDDeltaSt		
!	***	Base	Screens	#4003	cf_Ctrl_Dynamometer_N	PID-3
GLOBAL	INT		TAG	1325	Reserved_1325		
GLOBAL	INT		TAG	1326	Reserved_1326		
GLOBAL	REAL	TAG	1327	g_Hmi_bs4003_DynoSpid_CMaxEd		
GLOBAL	REAL	TAG	1328	g_Hmi_bs4003_DynoSpid_CMaxSt		
GLOBAL	REAL	TAG	1329	g_Hmi_bs4003_DynoSpid_CcntMaxEd		
GLOBAL	REAL	TAG	1330	g_Hmi_bs4003_DynoSpid_CcntMaxSt		
!	***	Base	Screens	#4004	cf_Ctrl_Dynamometer_N	PID-4
GLOBAL	REAL	TAG	1331	g_Hmi_bs4004_DynoSpid_C_ADeltaEd		
GLOBAL	REAL	TAG	1332	g_Hmi_bs4004_DynoSpid_C_ADeltaSt		
GLOBAL	REAL	TAG	1333	g_Hmi_bs4004_DynoSpid_C_DDeltaEd		
GLOBAL	REAL	TAG	1334	g_Hmi_bs4004_DynoSpid_C_DDeltaSt		
GLOBAL	REAL	TAG	1335	Reserved_1335		
GLOBAL	REAL	TAG	1336	Reserved_1336		
GLOBAL	REAL	TAG	1337	Reserved_1337		
GLOBAL	REAL	TAG	1338	Reserved_1338		
GLOBAL	REAL	TAG	1339	Reserved_1339		
GLOBAL	INT		TAG	1340	g_Hmi_bs400x_CtrlVal		
!	***	Base	Screens	#4101	cf_Ctrl_Dynamometer_T	PID-1
GLOBAL	REAL	TAG	1341	g_Hmi_bs4101_DynoTpid_KpEd		
GLOBAL	REAL	TAG	1342	g_Hmi_bs4101_DynoTpid_KpSt		
GLOBAL	REAL	TAG	1343	g_Hmi_bs4101_DynoTpid_KiEd		
GLOBAL	REAL	TAG	1344	g_Hmi_bs4101_DynoTpid_KiSt		
GLOBAL	REAL	TAG	1345	g_Hmi_bs4101_DynoTpid_KdEd		
GLOBAL	REAL	TAG	1346	g_Hmi_bs4101_DynoTpid_Kdst		
!	***	Base	Screens	#4102	cf_Ctrl_Dynamometer_T	PID-2
GLOBAL	INT		TAG	1347	g_Hmi_bs4101_DynoTpid_TdCntMaxEd		
GLOBAL	INT		TAG	1348	g_Hmi_bs4101_DynoTpid_TdCntMaxst		
GLOBAL	REAL	TAG	1349	Reserved_1349		
GLOBAL	REAL	TAG	1350	Reserved_1350		
GLOBAL	REAL	TAG	1351	Reserved_1351		
GLOBAL	REAL	TAG	1352	Reserved_1352
GLOBAL	INT		TAG	1353	Reserved_1353		
GLOBAL	INT		TAG	1354	Reserved_1354		
GLOBAL	REAL	TAG	1355	g_Hmi_bs4102_DynoTpid_YMinEd		
GLOBAL	REAL	TAG	1356	g_Hmi_bs4102_DynoTpid_YMinSt		
GLOBAL	REAL	TAG	1357	Reserved_1357		
GLOBAL	REAL	TAG	1358	Reserved_1358
GLOBAL	REAL	TAG	1359	Reserved_1359		
GLOBAL	REAL	TAG	1360	Reserved_1360		
GLOBAL	REAL	TAG	1361	g_Hmi_bs4102_DynoTpid_RefADeltaEd		
GLOBAL	REAL	TAG	1362	g_Hmi_bs4102_DynoTpid_RefADeltaSt		
GLOBAL	REAL	TAG	1363	g_Hmi_bs4102_DynoTpid_RefDDeltaEd		
GLOBAL	REAL	TAG	1364	g_Hmi_bs4102_DynoTpid_RefDDeltaSt		
!	***	Base	Screens	#4103	cf_Ctrl_Dynamometer_T	PID-3
GLOBAL	INT		TAG	1365	Reserved_1365	
GLOBAL	INT		TAG	1366	Reserved_1366		
GLOBAL	REAL	TAG	1367	g_Hmi_bs4103_DynoTpid_CMaxEd		
GLOBAL	REAL	TAG	1368	g_Hmi_bs4103_DynoTpid_CMaxSt		
GLOBAL	REAL	TAG	1369	g_Hmi_bs4103_DynoTpid_CcntMaxEd		
GLOBAL	REAL	TAG	1370	g_Hmi_bs4103_DynoTpid_CcntMaxSt		
!	***	Base	Screens	#4104	cf_Ctrl_Dynamometer_T	PID-4
GLOBAL	REAL	TAG	1371	g_Hmi_bs4104_DynoTpid_C_ADeltaEd		
GLOBAL	REAL	TAG	1372	g_Hmi_bs4104_DynoTpid_C_ADeltaSt		
GLOBAL	REAL	TAG	1373	g_Hmi_bs4104_DynoTpid_C_DDeltaEd		
GLOBAL	REAL	TAG	1374	g_Hmi_bs4104_DynoTpid_C_DDeltaSt		
GLOBAL	REAL	TAG	1375	Reserved_1375		
GLOBAL	REAL	TAG	1376	Reserved_1376		
GLOBAL	REAL	TAG	1377	Reserved_1377		
GLOBAL	REAL	TAG	1378	Reserved_1378		
GLOBAL	REAL	TAG	1379	Reserved_1379		
GLOBAL	INT		TAG	1380	g_Hmi_bs410x_CtrlVal		
!	***	Base	Screens	#4201	cf_Ctrl_Engine_N	PID-1
GLOBAL	REAL	TAG	1381	g_Hmi_bs4201_EngSpid_KpEd		
GLOBAL	REAL	TAG	1382	g_Hmi_bs4201_EngSpid_KpSt		
GLOBAL	REAL	TAG	1383	g_Hmi_bs4201_EngSpid_KiEd		
GLOBAL	REAL	TAG	1384	g_Hmi_bs4201_EngSpid_KiSt	
!	***	Base	Screens	#4203	cf_Ctrl_Engine_N	PID-3
GLOBAL	INT 	TAG	1385	g_Hmi_bs4203_EngSpid_IgSpeed_1Ed		
GLOBAL	INT 	TAG	1386	g_Hmi_bs4203_EngSpid_IgSpeed_1St	
GLOBAL	INT 	TAG	1387	g_Hmi_bs4203_EngSpid_IgSpeed_2Ed		
GLOBAL	INT 	TAG	1388	g_Hmi_bs4203_EngSpid_IgSpeed_2St		
GLOBAL	INT 	TAG	1389	g_Hmi_bs4203_EngSpid_IgSpeed_3Ed		
GLOBAL	INT 	TAG	1390	g_Hmi_bs4203_EngSpid_IgSpeed_3St
!	***	Base	Screens	#4204	cf_Ctrl_Engine_N	PID-4
GLOBAL	REAL	TAG	1391	g_Hmi_bs4204_EngSpid_Ig_iDelta1Ed		
GLOBAL	REAL	TAG	1392	g_Hmi_bs4204_EngSpid_Ig_iDelta1St
GLOBAL	REAL	TAG	1393	g_Hmi_bs4204_EngSpid_Ig_iDelta2Ed		
GLOBAL	REAL	TAG	1394	g_Hmi_bs4204_EngSpid_Ig_iDelta2St		
GLOBAL	REAL	TAG	1395	g_Hmi_bs4204_EngSpid_Ig_iDelta3Ed		
GLOBAL	REAL	TAG	1396	g_Hmi_bs4204_EngSpid_Ig_iDelta3St	
!	***	Base	Screens	#4205	cf_Ctrl_Engine_N	PID-5
GLOBAL	REAL	TAG	1397	g_Hmi_bs4205_EngSpid_Ig_iDelta4Ed		
GLOBAL	REAL	TAG	1398	g_Hmi_bs4205_EngSpid_Ig_iDelta4St
GLOBAL	INT 	TAG	1399	g_Hmi_bs4205_EngSpid_YMinEd		
GLOBAL	INT 	TAG	1400	g_Hmi_bs4205_EngSpid_YMinSt		
!	***	Base	Screens	#4206	cf_Ctrl_Engine_N	PID-6
GLOBAL	REAL	TAG	1401	g_Hmi_bs4206_EngSpid_RefADeltaEd		
GLOBAL	REAL	TAG	1402	g_Hmi_bs4206_EngSpid_RefADeltaSt		
GLOBAL	REAL	TAG	1403	g_Hmi_bs4206_EngSpid_RefDDeltaEd		
GLOBAL	REAL	TAG	1404	g_Hmi_bs4206_EngSpid_RefDDeltaSt		
!	***	Base	Screens	#4207	cf_Ctrl_Engine_N	PID-7
GLOBAL	INT		TAG	1405	Reserved_1405		
GLOBAL	INT		TAG	1406	Reserved_1406		
GLOBAL	REAL	TAG	1407	g_Hmi_bs4207_EngSpid_CMaxEd		
GLOBAL	REAL	TAG	1408	g_Hmi_bs4207_EngSpid_CMaxSt		
GLOBAL	REAL	TAG	1409	g_Hmi_bs4207_EngSpid_CcntMaxEd		
GLOBAL	REAL	TAG	1410	g_Hmi_bs4207_EngSpid_CcntMaxSt		
!	***	Base	Screens	#4208	cf_Ctrl_Engine_N	PID-8
GLOBAL	REAL	TAG	1411	g_Hmi_bs4208_EngSpid_C_ADeltaEd		
GLOBAL	REAL	TAG	1412	g_Hmi_bs4208_EngSpid_C_ADeltaSt		
GLOBAL	REAL	TAG	1413	g_Hmi_bs4208_EngSpid_C_DDeltaEd		
GLOBAL	REAL	TAG	1414	g_Hmi_bs4208_EngSpid_C_DDeltaSt		
!	***	Base	Screens	#4202	cf_Ctrl_Engine_N	PID-2
GLOBAL	REAL	TAG	1415	g_Hmi_bs4202_EngSpid_KpA_Ed	
GLOBAL	REAL	TAG	1416	g_Hmi_bs4202_EngSpid_KpA_St		
GLOBAL	REAL	TAG	1417	g_Hmi_bs4202_EngSpid_KpB_Ed		
GLOBAL	REAL	TAG	1418	g_Hmi_bs4202_EngSpid_KpB_St		
GLOBAL	REAL	TAG	1419	Reserved_1419		
GLOBAL	INT		TAG	1420	g_Hmi_bs420x_CtrlVal		
!	***	Base	Screens	#4301	cf_Ctrl_Engine_T	PID-1
GLOBAL	REAL	TAG	1421	g_Hmi_bs4301_EngTpid_KpEd		
GLOBAL	REAL	TAG	1422	g_Hmi_bs4301_EngTpid_KpSt		
GLOBAL	REAL	TAG	1423	g_Hmi_bs4301_EngTpid_KiEd		
GLOBAL	REAL	TAG	1424	g_Hmi_bs4301_EngTpid_KiSt	
!	***	Base	Screens	#4302	cf_Ctrl_Engine_T	PID-2
GLOBAL	INT		TAG	1425	g_Hmi_bs4302_EngTpid_IgSpeed_1Ed		
GLOBAL	INT		TAG	1426	g_Hmi_bs4302_EngTpid_IgSpeed_1St
GLOBAL	INT		TAG	1427	g_Hmi_bs4302_EngTpid_IgSpeed_2Ed		
GLOBAL	INT		TAG	1428	g_Hmi_bs4302_EngTpid_IgSpeed_2St		
GLOBAL	INT		TAG	1429	g_Hmi_bs4302_EngTpid_IgSpeed_3Ed		
GLOBAL	INT		TAG	1430	g_Hmi_bs4302_EngTpid_IgSpeed_3St		
!	***	Base	Screens	#4303	cf_Ctrl_Engine_T	PID-3
GLOBAL	REAL	TAG	1431	g_Hmi_bs4303_EngTpid_Ig_iDelta1Ed		
GLOBAL	REAL	TAG	1432	g_Hmi_bs4303_EngTpid_Ig_iDelta1St	
GLOBAL	REAL	TAG	1433	g_Hmi_bs4303_EngTpid_Ig_iDelta2Ed		
GLOBAL	REAL	TAG	1434	g_Hmi_bs4303_EngTpid_Ig_iDelta2St		
GLOBAL	REAL	TAG	1435	g_Hmi_bs4303_EngTpid_Ig_iDelta3Ed		
GLOBAL	REAL	TAG	1436	g_Hmi_bs4303_EngTpid_Ig_iDelta3St	
!	***	Base	Screens	#4304	cf_Ctrl_Engine_T	PID-4
GLOBAL	REAL	TAG	1437	g_Hmi_bs4304_EngTpid_Ig_iDelta4Ed		
GLOBAL	REAL	TAG	1438	g_Hmi_bs4304_EngTpid_Ig_iDelta4St	
!	***	Base	Screens	#4305	cf_Ctrl_Engine_T	PID-5
GLOBAL	REAL	TAG	1439	g_Hmi_bs4305_EngTpid_YMinEd		
GLOBAL	REAL	TAG	1440	g_Hmi_bs4305_EngTpid_YMinSt		
GLOBAL	REAL	TAG	1441	g_Hmi_bs4305_EngTpid_RefADeltaEd		
GLOBAL	REAL	TAG	1442	g_Hmi_bs4305_EngTpid_RefADeltaSt		
GLOBAL	REAL	TAG	1443	g_Hmi_bs4305_EngTpid_RefDDeltaEd		
GLOBAL	REAL	TAG	1444	g_Hmi_bs4305_EngTpid_RefDDeltaSt		
!	***	Base	Screens	#4306	cf_Ctrl_Engine_T	PID-6
GLOBAL	INT		TAG	1445	Reserved_1445		
GLOBAL	INT		TAG	1446	Reserved_1446		
GLOBAL	REAL	TAG	1447	g_Hmi_bs4306_EngTpid_CMaxEd		
GLOBAL	REAL	TAG	1448	g_Hmi_bs4306_EngTpid_CMaxSt		
GLOBAL	REAL	TAG	1449	g_Hmi_bs4306_EngTpid_CcntMaxEd		
GLOBAL	REAL	TAG	1450	g_Hmi_bs4306_EngTpid_CcntMaxSt		
!	***	Base	Screens	#4307	cf_Ctrl_Engine_T	PID-7
GLOBAL	REAL	TAG	1451	g_Hmi_bs4307_EngTpid_C_ADeltaEd		
GLOBAL	REAL	TAG	1452	g_Hmi_bs4307_EngTpid_C_ADeltaSt		
GLOBAL	REAL	TAG	1453	g_Hmi_bs4307_EngTpid_C_DDeltaEd		
GLOBAL	REAL	TAG	1454	g_Hmi_bs4307_EngTpid_C_DDeltaSt		
GLOBAL	REAL	TAG	1455	Reserved_1455		
GLOBAL	REAL	TAG	1456	Reserved_1456		
GLOBAL	REAL	TAG	1457	Reserved_1457		
GLOBAL	REAL	TAG	1458	Reserved_1458		
GLOBAL	REAL	TAG	1459	Reserved_1459		
GLOBAL	INT		TAG	1460	g_Hmi_bs430x_CtrlVal		
!	***	Base	Screens	#4401	cf_Ctrl_Engine_Appha	PID-1
GLOBAL	REAL	TAG	1461	Reserved_1461		
GLOBAL	REAL	TAG	1462	Reserved_1462		
GLOBAL	REAL	TAG	1463	Reserved_1463		
GLOBAL	REAL	TAG	1464	Reserved_1464		
GLOBAL	REAL	TAG	1465	Reserved_1465		
GLOBAL	REAL	TAG	1466	Reserved_1466
GLOBAL	INT		TAG	1467	Reserved_1467		
GLOBAL	INT		TAG	1468	Reserved_1468		
GLOBAL	REAL	TAG	1469	Reserved_1469		
GLOBAL	REAL	TAG	1470	Reserved_1470		
GLOBAL	REAL	TAG	1471	Reserved_1471		
GLOBAL	REAL	TAG	1472	Reserved_1472
GLOBAL	INT		TAG	1473	Reserved_1473		
GLOBAL	INT		TAG	1474	Reserved_1474		
GLOBAL	REAL	TAG	1475	Reserved_1475		
GLOBAL	REAL	TAG	1476	Reserved_1476		
GLOBAL	REAL	TAG	1477	Reserved_1477		
GLOBAL	REAL	TAG	1478	Reserved_1478
GLOBAL	REAL	TAG	1479	Reserved_1479		
GLOBAL	REAL	TAG	1480	Reserved_1480		
GLOBAL	REAL	TAG	1481	Reserved_1481		
GLOBAL	REAL	TAG	1482	Reserved_1482		
GLOBAL	REAL	TAG	1483	Reserved_1483		
GLOBAL	REAL	TAG	1484	Reserved_1484
GLOBAL	INT		TAG	1485	Reserved_1485		
GLOBAL	INT		TAG	1486	Reserved_1486		
GLOBAL	REAL	TAG	1487	Reserved_1487		
GLOBAL	REAL	TAG	1488	Reserved_1488		
GLOBAL	REAL	TAG	1489	Reserved_1489		
GLOBAL	REAL	TAG	1490	Reserved_1490
GLOBAL	REAL	TAG	1491	g_Hmi_bs4401_EngApid_C_ADeltaEd		
GLOBAL	REAL	TAG	1492	g_Hmi_bs4401_EngApid_C_ADeltaSt		
GLOBAL	REAL	TAG	1493	g_Hmi_bs4401_EngApid_C_DDeltaEd		
GLOBAL	REAL	TAG	1494	g_Hmi_bs4401_EngApid_C_DDeltaSt		
GLOBAL	REAL	TAG	1495	Reserved_1495		
GLOBAL	REAL	TAG	1496	Reserved_1496		
GLOBAL	REAL	TAG	1497	Reserved_1497		
GLOBAL	REAL	TAG	1498	Reserved_1498		
GLOBAL	REAL	TAG	1499	Reserved_1499		
GLOBAL	INT		TAG	1500	g_Hmi_bs440x_CtrlVal		
!	***	Base	Screens	#5001	cf_Cal_Speed_Calibration-1
GLOBAL	INT 	TAG	1501	g_Hmi_bs5001_FB_SpeedYIndexEd
GLOBAL	INT 	TAG	1502	g_Hmi_bs5001_FB_SpeedYIndexSt
GLOBAL	REAL	TAG	1503	Reserved_1503
GLOBAL	REAL	TAG	1504	Reserved_1504
GLOBAL	REAL	TAG	1505	Reserved_1505
GLOBAL	REAL	TAG	1506	Reserved_1506
GLOBAL	REAL	TAG	1507	g_Hmi_bs5001_FB_SpeedMaxSpeedEd
GLOBAL	REAL	TAG	1508	g_Hmi_bs5001_FB_SpeedMaxSpeedSt
GLOBAL	REAL	TAG	1509	Reserved_1509
GLOBAL	REAL	TAG	1510	Reserved_1510
GLOBAL	REAL	TAG	1511	Reserved_1511
GLOBAL	REAL	TAG	1512	Reserved_1512
GLOBAL	REAL	TAG	1513	Reserved_1513
GLOBAL	REAL	TAG	1514	Reserved_1514
GLOBAL	REAL	TAG	1515	Reserved_1515
GLOBAL	REAL	TAG	1516	Reserved_1516
GLOBAL	REAL	TAG	1517	Reserved_1517
GLOBAL	REAL	TAG	1518	Reserved_1518
GLOBAL	REAL	TAG	1519	Reserved_1519
GLOBAL	REAL	TAG	1520	Reserved_1520
!	***	Base	Screens	#5002	cf_Cal_Speed_Calibration-2
GLOBAL	INT 	TAG	1521	g_Hmi_bs5002_FB_SpeedCalAd_Val0Ed
GLOBAL	INT 	TAG	1522	g_Hmi_bs5002_FB_SpeedCalAd_Val0St
GLOBAL	INT 	TAG	1523	g_Hmi_bs5002_FB_SpeedCalAd_Val1Ed
GLOBAL	INT 	TAG	1524	g_Hmi_bs5002_FB_SpeedCalAd_Val1St
GLOBAL	INT 	TAG	1525	g_Hmi_bs5002_FB_SpeedCalAd_Val2Ed
GLOBAL	INT 	TAG	1526	g_Hmi_bs5002_FB_SpeedCalAd_Val2St
GLOBAL	INT 	TAG	1527	g_Hmi_bs5002_FB_SpeedCalAd_Val3Ed
GLOBAL	INT 	TAG	1528	g_Hmi_bs5002_FB_SpeedCalAd_Val3St
GLOBAL	INT 	TAG	1529	g_Hmi_bs5002_FB_SpeedCalAd_Val4Ed
GLOBAL	INT 	TAG	1530	g_Hmi_bs5002_FB_SpeedCalAd_Val4St
GLOBAL	INT		TAG	1531	g_Hmi_bs5003_FB_SpeedCalAd_Val5Ed
GLOBAL	INT 	TAG	1532	g_Hmi_bs5003_FB_SpeedCalAd_Val5St
GLOBAL	INT 	TAG	1533	g_Hmi_bs5003_FB_SpeedCalAd_Val6Ed
GLOBAL	INT		TAG	1534	g_Hmi_bs5003_FB_SpeedCalAd_Val6St
GLOBAL	INT		TAG	1535	g_Hmi_bs5003_FB_SpeedCalAd_Val7Ed
GLOBAL	INT		TAG	1536	g_Hmi_bs5003_FB_SpeedCalAd_Val7St
GLOBAL	INT		TAG	1537	g_Hmi_bs5003_FB_SpeedCalAd_Val8Ed
GLOBAL	INT		TAG	1538	g_Hmi_bs5003_FB_SpeedCalAd_Val8St
GLOBAL	INT		TAG	1539	g_Hmi_bs5003_FB_SpeedCalAd_Val9Ed
GLOBAL	INT		TAG	1540	g_Hmi_bs5003_FB_SpeedCalAd_Val9St
GLOBAL	INT		TAG	1541	g_Hmi_bs5004_FB_SpeedCalAd_Val10Ed
GLOBAL	INT		TAG	1542	g_Hmi_bs5004_FB_SpeedCalAd_Val10St
GLOBAL	INT		TAG	1543	Reserved_1543
GLOBAL	INT		TAG	1544	Reserved_1544
GLOBAL	INT		TAG	1545	g_Hmi_bs5002_FB_SpeedCal_Val0Ed
GLOBAL	INT		TAG	1546	g_Hmi_bs5002_FB_SpeedCal_Val0St
GLOBAL	INT		TAG	1547	g_Hmi_bs5002_FB_SpeedCal_Val1Ed
GLOBAL	INT		TAG	1548	g_Hmi_bs5002_FB_SpeedCal_Val1St
GLOBAL	INT		TAG	1549	g_Hmi_bs5002_FB_SpeedCal_Val2Ed
GLOBAL	INT		TAG	1550	g_Hmi_bs5002_FB_SpeedCal_Val2St
GLOBAL	INT		TAG	1551	g_Hmi_bs5002_FB_SpeedCal_Val3Ed
GLOBAL	INT		TAG	1552	g_Hmi_bs5002_FB_SpeedCal_Val3St
GLOBAL	INT		TAG	1553	g_Hmi_bs5002_FB_SpeedCal_Val4Ed
GLOBAL	INT		TAG	1554	g_Hmi_bs5002_FB_SpeedCal_Val4St
GLOBAL	INT		TAG	1555	g_Hmi_bs5003_FB_SpeedCal_Val5Ed
GLOBAL	INT		TAG	1556	g_Hmi_bs5003_FB_SpeedCal_Val5St
GLOBAL	INT		TAG	1557	g_Hmi_bs5003_FB_SpeedCal_Val6Ed
GLOBAL	INT		TAG	1558	g_Hmi_bs5003_FB_SpeedCal_Val6St
GLOBAL	INT		TAG	1559	g_Hmi_bs5003_FB_SpeedCal_Val7Ed
GLOBAL	INT		TAG	1560	g_Hmi_bs5003_FB_SpeedCal_Val7St
GLOBAL	INT		TAG	1561	g_Hmi_bs5003_FB_SpeedCal_Val8Ed
GLOBAL	INT		TAG	1562	g_Hmi_bs5003_FB_SpeedCal_Val8St
GLOBAL	INT		TAG	1563	g_Hmi_bs5003_FB_SpeedCal_Val9Ed
GLOBAL	INT		TAG	1564	g_Hmi_bs5003_FB_SpeedCal_Val9St
GLOBAL	INT		TAG	1565	g_Hmi_bs5004_FB_SpeedCal_Val10Ed
GLOBAL	INT		TAG	1566	g_Hmi_bs5004_FB_SpeedCal_Val10St
GLOBAL	INT		TAG	1567	Reserved_1567
GLOBAL	INT		TAG	1568	Reserved_1568
GLOBAL	INT		TAG	1569	Reserved_1569
GLOBAL	INT		TAG	1570	g_Hmi_bs500x_CtrlVal
!	***	Base	Screens	#5101	cf_Cal_Torque_Calibration-1
GLOBAL	INT 	TAG	1571	g_Hmi_bs5101_FB_TorqueYIndexEd
GLOBAL	INT 	TAG	1572	g_Hmi_bs5101_FB_TorqueYIndexSt
GLOBAL	REAL	TAG	1573	Reserved_1573
GLOBAL	REAL	TAG	1574	Reserved_1574
GLOBAL	REAL	TAG	1575	Reserved_1575
GLOBAL	REAL	TAG	1576	Reserved_1576
GLOBAL	REAL	TAG	1577	g_Hmi_bs5101_FB_TorqueMaxTorqueEd
GLOBAL	REAL	TAG	1578	g_Hmi_bs5101_FB_TorqueMaxTorqueSt
GLOBAL	REAL	TAG	1579	Reserved_1579
GLOBAL	REAL	TAG	1580	Reserved_1580
GLOBAL	REAL	TAG	1581	Reserved_1581
GLOBAL	REAL	TAG	1582	Reserved_1582
GLOBAL	REAL	TAG	1583	Reserved_1583
GLOBAL	REAL	TAG	1584	Reserved_1584
GLOBAL	REAL	TAG	1585	Reserved_1585
GLOBAL	REAL	TAG	1586	Reserved_1586
GLOBAL	REAL	TAG	1587	Reserved_1587
GLOBAL	REAL	TAG	1588	Reserved_1588
GLOBAL	REAL	TAG	1589	Reserved_1589
GLOBAL	INT		TAG	1590	Reserved_1590
!	***	Base	Screens	#5102	cf_Cal_Torque_Calibration-2
GLOBAL	INT 	TAG	1591	g_Hmi_bs5102_FB_TorqueCalAd_Val0Ed
GLOBAL	INT     TAG	1592	g_Hmi_bs5102_FB_TorqueCalAd_Val0St
GLOBAL	INT		TAG	1593	g_Hmi_bs5102_FB_TorqueCalAd_Val1Ed
GLOBAL	INT		TAG	1594	g_Hmi_bs5102_FB_TorqueCalAd_Val1St
GLOBAL	INT 	TAG	1595	g_Hmi_bs5102_FB_TorqueCalAd_Val2Ed
GLOBAL	INT 	TAG	1596	g_Hmi_bs5102_FB_TorqueCalAd_Val2St
GLOBAL	INT 	TAG	1597	g_Hmi_bs5102_FB_TorqueCalAd_Val3Ed
GLOBAL	INT 	TAG	1598	g_Hmi_bs5102_FB_TorqueCalAd_Val3St
GLOBAL	INT		TAG	1599	g_Hmi_bs5102_FB_TorqueCalAd_Val4Ed
GLOBAL	INT		TAG	1600	g_Hmi_bs5102_FB_TorqueCalAd_Val4St
GLOBAL	INT 	TAG	1601	g_Hmi_bs5102_FB_TorqueCalAd_Val5Ed
GLOBAL	INT 	TAG	1602	g_Hmi_bs5102_FB_TorqueCalAd_Val5St
GLOBAL	INT 	TAG	1603	g_Hmi_bs5103_FB_TorqueCalAd_Val6Ed
GLOBAL	INT 	TAG	1604	g_Hmi_bs5103_FB_TorqueCalAd_Val6St
GLOBAL	INT		TAG	1605	g_Hmi_bs5103_FB_TorqueCalAd_Val7Ed
GLOBAL	INT		TAG	1606	g_Hmi_bs5103_FB_TorqueCalAd_Val7St
GLOBAL	INT 	TAG	1607	g_Hmi_bs5103_FB_TorqueCalAd_Val8Ed
GLOBAL	INT 	TAG	1608	g_Hmi_bs5103_FB_TorqueCalAd_Val8St
GLOBAL	INT  	TAG	1609	g_Hmi_bs5103_FB_TorqueCalAd_Val9Ed
GLOBAL	INT 	TAG	1610	g_Hmi_bs5103_FB_TorqueCalAd_Val9St
GLOBAL	INT 	TAG	1611	g_Hmi_bs5104_FB_TorqueCalAd_Val10Ed
GLOBAL	INT 	TAG	1612	g_Hmi_bs5104_FB_TorqueCalAd_Val10St
GLOBAL	INT 	TAG	1613	Reserved_1613
GLOBAL	INT 	TAG	1614	Reserved_1614
GLOBAL	INT 	TAG	1615	g_Hmi_bs5102_FB_TorqueCal_Val0Ed
GLOBAL	INT 	TAG	1616	g_Hmi_bs5102_FB_TorqueCal_Val0St
GLOBAL	INT 	TAG	1617	g_Hmi_bs5102_FB_TorqueCal_Val1Ed
GLOBAL	INT 	TAG	1618	g_Hmi_bs5102_FB_TorqueCal_Val1St
GLOBAL	INT 	TAG	1619	g_Hmi_bs5102_FB_TorqueCal_Val2Ed
GLOBAL	INT		TAG	1620	g_Hmi_bs5102_FB_TorqueCal_Val2St
GLOBAL	INT		TAG	1621	g_Hmi_bs5102_FB_TorqueCal_Val3Ed
GLOBAL	INT		TAG	1622	g_Hmi_bs5102_FB_TorqueCal_Val3St
GLOBAL	INT		TAG	1623	g_Hmi_bs5102_FB_TorqueCal_Val4Ed
GLOBAL	INT		TAG	1624	g_Hmi_bs5102_FB_TorqueCal_Val4St
GLOBAL	INT		TAG	1625	g_Hmi_bs5102_FB_TorqueCal_Val5Ed
GLOBAL	INT		TAG	1626	g_Hmi_bs5102_FB_TorqueCal_Val5St
GLOBAL	INT		TAG	1627	g_Hmi_bs5103_FB_TorqueCal_Val6Ed
GLOBAL	INT		TAG	1628	g_Hmi_bs5103_FB_TorqueCal_Val6St
GLOBAL	INT		TAG	1629	g_Hmi_bs5103_FB_TorqueCal_Val7Ed
GLOBAL	INT		TAG	1630	g_Hmi_bs5103_FB_TorqueCal_Val7St
GLOBAL	INT		TAG	1631	g_Hmi_bs5103_FB_TorqueCal_Val8Ed
GLOBAL	INT		TAG	1632	g_Hmi_bs5103_FB_TorqueCal_Val8St
GLOBAL	INT		TAG	1633	g_Hmi_bs5103_FB_TorqueCal_Val9Ed
GLOBAL	INT		TAG	1634	g_Hmi_bs5103_FB_TorqueCal_Val9St
GLOBAL	INT		TAG	1635	g_Hmi_bs5104_FB_TorqueCal_Val10Ed
GLOBAL	INT		TAG	1636	g_Hmi_bs5104_FB_TorqueCal_Val10St
GLOBAL	INT		TAG	1637	Reserved_1637
GLOBAL	INT		TAG	1638	Reserved_1638
GLOBAL	INT		TAG	1639	Reserved_1639
GLOBAL	INT		TAG	1640	g_Hmi_bs510x_CtrlVal
!	***	Base	Screens	#5201	cf_Cal_Alpha_Calibration-1
GLOBAL	INT		TAG	1641	g_Hmi_bs5201_FB_AlphaYIndexEd
GLOBAL	INT		TAG	1642	g_Hmi_bs5201_FB_AlphaYIndexSt
GLOBAL	INT		TAG	1643	g_Hmi_bs5201_FB_AlphaSourceEd
GLOBAL	INT		TAG	1644	g_Hmi_bs5201_FB_AlphaSourceSt
GLOBAL	INT		TAG	1645	Reserved_1645
GLOBAL	INT		TAG	1646	Reserved_1646
GLOBAL	REAL	TAG	1647	g_Hmi_bs5201_FB_AlphaZeroOffsetEd
GLOBAL	REAL	TAG	1648	g_Hmi_bs5201_FB_AlphaZeroOffsetSd
GLOBAL	INT		TAG	1649	Reserved_1649
GLOBAL	INT		TAG	1650	Reserved_1650
GLOBAL	INT		TAG	1651	Reserved_1651
GLOBAL	INT		TAG	1652	Reserved_1652
GLOBAL	INT		TAG	1653	Reserved_1653
GLOBAL	INT		TAG	1654	Reserved_1654
GLOBAL	INT		TAG	1655	Reserved_1655
GLOBAL	INT		TAG	1656	Reserved_1656
GLOBAL	INT		TAG	1657	Reserved_1657
GLOBAL	INT		TAG	1658	Reserved_1658
GLOBAL	INT		TAG	1659	Reserved_1659
GLOBAL	INT		TAG	1660	g_Hmi_bs520x_CtrlVal
!	***	Base	Screens	#5401	cf_Cal_Alpha_Output Calibration-1
GLOBAL	INT 	TAG	1661	g_Hmi_bs5401_Op_Alpha_1_CIndexEd
GLOBAL	INT 	TAG	1662	g_Hmi_bs5401_Op_Alpha_1_CIndexSt
GLOBAL	INT 	TAG	1663	g_Hmi_bs5401_Op_Alpha_2_CIndexEd
GLOBAL	INT 	TAG	1664	g_Hmi_bs5401_Op_Alpha_2_CIndexSt
GLOBAL	REAL	TAG	1665	Reserved_1665
GLOBAL	REAL	TAG	1666	Reserved_1666
!	***	Base	Screens	#5402	cf_Cal_Alpha_Output Calibration-2
GLOBAL	REAL	TAG	1667	g_Hmi_bs5402_Eng_Alpha_1_MinVoltEd			
GLOBAL	REAL	TAG	1668	g_Hmi_bs5402_Eng_Alpha_1_MinVoltSt			
GLOBAL	REAL	TAG	1669	g_Hmi_bs5402_Eng_Alpha_1_MaxVoltEd			
GLOBAL	REAL	TAG	1670	g_Hmi_bs5402_Eng_Alpha_1_MaxVoltSt			
GLOBAL	REAL	TAG	1671	g_Hmi_bs5402_Eng_Alpha_1_OffsetVoltEd			
GLOBAL	REAL	TAG	1672	g_Hmi_bs5402_Eng_Alpha_1_OffsetVoltSt	
!	***	Base	Screens	#5403	cf_Cal_Alpha_Output Calibration-3		
GLOBAL	REAL	TAG	1673	g_Hmi_bs5403_Eng_Alpha_2_MinVoltEd			
GLOBAL	REAL	TAG	1674	g_Hmi_bs5403_Eng_Alpha_2_MinVoltSt			
GLOBAL	REAL	TAG	1675	g_Hmi_bs5403_Eng_Alpha_2_MaxVoltEd			
GLOBAL	REAL	TAG	1676	g_Hmi_bs5403_Eng_Alpha_2_MaxVoltSt			
GLOBAL	REAL	TAG	1677	g_Hmi_bs5403_Eng_Alpha_2_OffsetVoltEd			
GLOBAL	REAL	TAG	1678	g_Hmi_bs5403_Eng_Alpha_2_OffsetVoltSt	
!	***	Base	Screens	#5404	cf_Cal_Alpha_Output Calibration-4	
GLOBAL	INT 	TAG	1679	g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexEd			
GLOBAL	INT 	TAG	1680	g_Hmi_bs5404_Op_Alpha_IdleValiDo_NodeIndexSt			
GLOBAL	INT 	TAG	1681	g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexEd			
GLOBAL	INT 	TAG	1682	g_Hmi_bs5404_Op_Alpha_IdleValiDo_BitIndexSt			
GLOBAL	REAL	TAG	1683	g_Hmi_bs5404_Op_Alpha_IdleValiDo_VoltEd			
GLOBAL	REAL	TAG	1684	g_Hmi_bs5404_Op_Alpha_IdleValiDo_VoltSt			
GLOBAL	REAL	TAG	1685	Reserved_1685			
GLOBAL	REAL	TAG	1686	Reserved_1686			
GLOBAL	REAL	TAG	1687	Reserved_1687			
GLOBAL	REAL	TAG	1688	Reserved_1688			
GLOBAL	REAL	TAG	1689	Reserved_1689			
GLOBAL	INT 	TAG	1690	g_Hmi_bs540x_CtrlVal	
GLOBAL	INT		TAG	1691	Reserved_1691	
GLOBAL	INT		TAG	1692	Reserved_1692
GLOBAL	INT		TAG	1693	Reserved_1693
GLOBAL	INT		TAG	1694	Reserved_1694
GLOBAL	INT		TAG	1695	Reserved_1695
GLOBAL	INT		TAG	1696	Reserved_1696
GLOBAL	INT		TAG	1697	Reserved_1697
GLOBAL	INT		TAG	1698	Reserved_1698
GLOBAL	INT		TAG	1699	Reserved_1699
GLOBAL	INT		TAG	1700	Reserved_1700
GLOBAL	INT		TAG	1701	Reserved_1701
GLOBAL	INT		TAG	1702	Reserved_1702
GLOBAL	INT		TAG	1703	Reserved_1703
GLOBAL	INT		TAG	1704	Reserved_1704
GLOBAL	INT		TAG	1705	Reserved_1705
GLOBAL	INT		TAG	1706	Reserved_1706
GLOBAL	INT		TAG	1707	Reserved_1707
GLOBAL	INT		TAG	1708	Reserved_1708
GLOBAL	INT		TAG	1709	Reserved_1709
GLOBAL	INT		TAG	1710	Reserved_1710
!	***	Base	Screens	#5301	cf_Cal_Current_Calibration-1
GLOBAL	INT		TAG	1711	g_Hmi_bs5301_FB_CurrentIndexEd
GLOBAL	INT		TAG	1712	g_Hmi_bs5301_FB_CurrentIndexSt
GLOBAL	INT		TAG	1713	Reserved_1713
GLOBAL	INT		TAG	1714	Reserved_1714
GLOBAL	INT		TAG	1715	Reserved_1715
GLOBAL	INT		TAG	1716	Reserved_1716
GLOBAL	INT		TAG	1717	g_Hmi_bs5301_FB_MaxCurrentEd
GLOBAL	INT		TAG	1718	g_Hmi_bs5301_FB_MaxCurrentSt
GLOBAL	INT		TAG	1719	Reserved_1719
GLOBAL	INT		TAG	1720	Reserved_1720
GLOBAL	INT		TAG	1721	Reserved_1721	
GLOBAL	INT		TAG	1722	Reserved_1722
GLOBAL	INT		TAG	1723	Reserved_1723
GLOBAL	INT		TAG	1724	Reserved_1724
GLOBAL	INT		TAG	1725	Reserved_1725
GLOBAL	INT		TAG	1726	Reserved_1726
GLOBAL	INT		TAG	1727	Reserved_1727
GLOBAL	INT		TAG	1728	Reserved_1728
GLOBAL	INT		TAG	1729	Reserved_1729
GLOBAL	INT		TAG	1730	Reserved_1730
!	***	Base	Screens	#5302	cf_Cal_Current_Calibration-2
GLOBAL	INT		TAG	1731	g_Hmi_bs5302_FB_CurrentCalAd_Val0Ed
GLOBAL	INT		TAG	1732	g_Hmi_bs5302_FB_CurrentCalAd_Val0St
GLOBAL	INT		TAG	1733	g_Hmi_bs5302_FB_CurrentCalAd_Val1Ed
GLOBAL	INT		TAG	1734	g_Hmi_bs5302_FB_CurrentCalAd_Val1St
GLOBAL	INT		TAG	1735	g_Hmi_bs5302_FB_CurrentCalAd_Val2Ed
GLOBAL	INT		TAG	1736	g_Hmi_bs5302_FB_CurrentCalAd_Val2St
GLOBAL	INT		TAG	1737	g_Hmi_bs5302_FB_CurrentCalAd_Val3Ed
GLOBAL	INT		TAG	1738	g_Hmi_bs5302_FB_CurrentCalAd_Val3St
GLOBAL	INT		TAG	1739	g_Hmi_bs5302_FB_CurrentCalAd_Val4Ed
GLOBAL	INT		TAG	1740	g_Hmi_bs5302_FB_CurrentCalAd_Val4St
GLOBAL	INT		TAG	1741	g_Hmi_bs5302_FB_CurrentCalAd_Val5Ed
GLOBAL	INT		TAG	1742	g_Hmi_bs5302_FB_CurrentCalAd_Val5St
GLOBAL	INT		TAG	1743	g_Hmi_bs5303_FB_CurrentCalAd_Val6Ed
GLOBAL	INT		TAG	1744	g_Hmi_bs5303_FB_CurrentCalAd_Val6St
GLOBAL	INT		TAG	1745	g_Hmi_bs5303_FB_CurrentCalAd_Val7Ed
GLOBAL	INT		TAG	1746	g_Hmi_bs5303_FB_CurrentCalAd_Val7St
GLOBAL	INT		TAG	1747	g_Hmi_bs5303_FB_CurrentCalAd_Val8Ed
GLOBAL	INT		TAG	1748	g_Hmi_bs5303_FB_CurrentCalAd_Val8St
GLOBAL	INT		TAG	1749	g_Hmi_bs5303_FB_CurrentCalAd_Val9Ed
GLOBAL	INT		TAG	1750	g_Hmi_bs5303_FB_CurrentCalAd_Val9St
GLOBAL	INT		TAG	1751	g_Hmi_bs5304_FB_CurrentCalAd_Val10Ed
GLOBAL	INT		TAG	1752	g_Hmi_bs5304_FB_CurrentCalAd_Val10St
GLOBAL	INT		TAG	1753	Reserved_1753
GLOBAL	INT		TAG	1754	Reserved_1754
GLOBAL	INT		TAG	1755	g_Hmi_bs5302_FB_CurrentCal_Val0Ed
GLOBAL	INT		TAG	1756	g_Hmi_bs5302_FB_CurrentCal_Val0St
GLOBAL	INT		TAG	1757	g_Hmi_bs5302_FB_CurrentCal_Val1Ed
GLOBAL	INT		TAG	1758	g_Hmi_bs5302_FB_CurrentCal_Val1St
GLOBAL	INT		TAG	1759	g_Hmi_bs5302_FB_CurrentCal_Val2Ed
GLOBAL	INT		TAG	1760	g_Hmi_bs5302_FB_CurrentCal_Val2St
GLOBAL	INT		TAG	1761	g_Hmi_bs5302_FB_CurrentCal_Val3Ed
GLOBAL	INT		TAG	1762	g_Hmi_bs5302_FB_CurrentCal_Val3St
GLOBAL	INT		TAG	1763	g_Hmi_bs5302_FB_CurrentCal_Val4Ed
GLOBAL	INT		TAG	1764	g_Hmi_bs5302_FB_CurrentCal_Val4St
GLOBAL	INT		TAG	1765	g_Hmi_bs5303_FB_CurrentCal_Val5Ed
GLOBAL	INT		TAG	1766	g_Hmi_bs5303_FB_CurrentCal_Val5St
GLOBAL	INT		TAG	1767	g_Hmi_bs5303_FB_CurrentCal_Val6Ed
GLOBAL	INT		TAG	1768	g_Hmi_bs5303_FB_CurrentCal_Val6St
GLOBAL	INT		TAG	1769	g_Hmi_bs5303_FB_CurrentCal_Val7Ed
GLOBAL	INT		TAG	1770	g_Hmi_bs5303_FB_CurrentCal_Val7St
GLOBAL	INT		TAG	1771	g_Hmi_bs5303_FB_CurrentCal_Val8Ed
GLOBAL	INT		TAG	1772	g_Hmi_bs5303_FB_CurrentCal_Val8St
GLOBAL	INT		TAG	1773	g_Hmi_bs5303_FB_CurrentCal_Val9Ed
GLOBAL	INT		TAG	1774	g_Hmi_bs5303_FB_CurrentCal_Val9St
GLOBAL	INT		TAG	1775	g_Hmi_bs5304_FB_CurrentCal_Val10Ed
GLOBAL	INT		TAG	1776	g_Hmi_bs5304_FB_CurrentCal_Val10St
GLOBAL	INT		TAG	1777	Reserved_1777
GLOBAL	INT		TAG	1778	Reserved_1778
GLOBAL	INT		TAG	1779	Reserved_1779
GLOBAL	INT		TAG	1780	g_Hmi_bs530x_CtrlVal
!	***	Base	Screens	#6001	cf_Mon_MonitoringSetting-1
GLOBAL	INT		TAG	1781	g_Hmi_bs6001_InIndex_EngOilPressureEd
GLOBAL	INT		TAG	1782	g_Hmi_bs6001_InIndex_EngOilPressureSt
GLOBAL	INT		TAG	1783	g_Hmi_bs6001_InIndex_EngOilSenMaxPressureEd
GLOBAL	INT		TAG	1784	g_Hmi_bs6001_InIndex_EngOilSenMaxPressureSt
!	***	Base	Screens	#6002	cf_Mon_MonitoringSetting-2
GLOBAL	INT		TAG	1785	g_Hmi_bs6001_InIndex_EngOilTempEd
GLOBAL	INT		TAG	1786	g_Hmi_bs6001_InIndex_EngOilTempSt
GLOBAL	INT		TAG	1787	g_Hmi_bs6001_InIndex_DisturDynoEd
GLOBAL	INT		TAG	1788	g_Hmi_bs6001_InIndex_DisturDynoSt
!	***	Base	Screens	#6003	cf_Mon_MonitoringSetting-3
GLOBAL	INT		TAG	1789	Reserved_1789
GLOBAL	INT		TAG	1790	Reserved_1790
GLOBAL	INT		TAG	1791	g_Hmi_bs6001_InIndex_cWaterPressureEd
GLOBAL	INT		TAG	1792	g_Hmi_bs6001_InIndex_cWaterPressureSt
GLOBAL	INT		TAG	1793	g_Hmi_bs6001_InIndex_cWaterSenMaxPressureEd
GLOBAL	INT		TAG	1794	g_Hmi_bs6001_InIndex_cWaterSenMaxPressureSt
!	***	Base	Screens	#6004	cf_Mon_MonitoringSetting-4
GLOBAL	INT		TAG	1795	g_Hmi_bs6001_InIndex_cWaterTempEd
GLOBAL	INT		TAG	1796	g_Hmi_bs6001_InIndex_cWaterTempSt
GLOBAL	INT		TAG	1797	g_Hmi_bs6001_InIndex_ExhaustTempEd
GLOBAL	INT		TAG	1798	g_Hmi_bs6001_InIndex_ExhaustTempSt
GLOBAL	INT		TAG	1799	Reserved_1799
GLOBAL	INT		TAG	1800	Reserved_1800
GLOBAL	INT		TAG	1801	Reserved_1801
GLOBAL	INT		TAG	1802	Reserved_1802
GLOBAL	INT		TAG	1803	Reserved_1803
GLOBAL	INT		TAG	1804	Reserved_1804
GLOBAL	INT		TAG	1805	Reserved_1805
GLOBAL	INT		TAG	1806	Reserved_1806
GLOBAL	INT		TAG	1807	Reserved_1807
GLOBAL	INT		TAG	1808	Reserved_1808
GLOBAL	INT		TAG	1809	Reserved_1809
GLOBAL	INT		TAG	1810	Reserved_1810
GLOBAL	INT		TAG	1811	Reserved_1811
GLOBAL	INT		TAG	1812	Reserved_1812
GLOBAL	INT		TAG	1813	Reserved_1813
GLOBAL	INT		TAG	1814	Reserved_1814
GLOBAL	INT		TAG	1815	Reserved_1815
GLOBAL	INT		TAG	1816	Reserved_1816
GLOBAL	INT		TAG	1817	Reserved_1817
GLOBAL	INT		TAG	1818	Reserved_1818
GLOBAL	INT		TAG	1819	Reserved_1819
GLOBAL	INT		TAG	1820	g_Hmi_bs600x_CtrlVal
!	***	Base	Screens	#6101	cf__Monitoring-1
GLOBAL	REAL	TAG	1821	g_Hmi_bs6101_Mon_EngOilPressure
GLOBAL	REAL	TAG	1822	g_Hmi_bs6101_Mon_EngOilTemp
GLOBAL	REAL	TAG	1823	g_Hmi_bs6101_Mon_DisturDyno
GLOBAL	REAL	TAG	1824	g_Hmi_bs6101_Mon_cWaterPressure
GLOBAL	REAL	TAG	1825	g_Hmi_bs6101_Mon_cWaterTemp
GLOBAL	REAL	TAG	1826	g_Hmi_bs6101_Mon_ExhaustTemp
GLOBAL	INT		TAG	1827	Reserved_1827
GLOBAL	INT		TAG	1828	Reserved_1828
GLOBAL	INT		TAG	1829	Reserved_1829
GLOBAL	INT		TAG	1830	Reserved_1830
GLOBAL	INT		TAG	1831	Reserved_1831
GLOBAL	INT		TAG	1832	Reserved_1832
GLOBAL	INT		TAG	1833	Reserved_1833
GLOBAL	INT		TAG	1834	Reserved_1834
GLOBAL	INT		TAG	1835	Reserved_1835
GLOBAL	INT		TAG	1836	Reserved_1836
GLOBAL	INT		TAG	1837	Reserved_1837
GLOBAL	INT		TAG	1838	Reserved_1838
GLOBAL	INT		TAG	1839	Reserved_1839
GLOBAL	INT		TAG	1840	Reserved_1840
GLOBAL	INT		TAG	1841	Reserved_1841
GLOBAL	INT		TAG	1842	Reserved_1842
GLOBAL	INT		TAG	1843	Reserved_1843
GLOBAL	INT		TAG	1844	Reserved_1844
GLOBAL	INT		TAG	1845	Reserved_1845
GLOBAL	INT		TAG	1846	Reserved_1846
GLOBAL	INT		TAG	1847	Reserved_1847
GLOBAL	INT		TAG	1848	Reserved_1848
GLOBAL	INT		TAG	1849	Reserved_1849
GLOBAL	INT		TAG	1850	Reserved_1850	
!	***	System Parameter
GLOBAL	INT		TAG	1851	g_Hmi_Target_Dyno_Torque_Range	
GLOBAL	INT		TAG	1852	g_Hmi_Target_Dyno_Speed_Range	
GLOBAL	INT		TAG	1853	g_Hmi_Target_Engine_AlphaPosi_Range
GLOBAL	INT		TAG	1854	g_Hmi_Target_Engine_Speed_Range
GLOBAL	INT		TAG	1855	g_Hmi_Target_Engine_Torque_Range
GLOBAL	INT		TAG	1856	Reserved_1856
GLOBAL	INT		TAG	1857	Reserved_1857
GLOBAL	INT		TAG	1858	Reserved_1858
GLOBAL	INT		TAG	1859	Reserved_1859
GLOBAL	INT		TAG	1860	Reserved_1860
GLOBAL	INT		TAG	1861	Reserved_1861
GLOBAL	INT		TAG	1862	Reserved_1862
GLOBAL	INT		TAG	1863	Reserved_1863
GLOBAL	INT		TAG	1864	Reserved_1864
GLOBAL	INT		TAG	1865	Reserved_1865
GLOBAL	INT		TAG	1866	Reserved_1866
GLOBAL	INT		TAG	1867	Reserved_1867
GLOBAL	INT		TAG	1868	Reserved_1868
GLOBAL	INT		TAG	1869	Reserved_1869
GLOBAL	INT		TAG	1870	Reserved_1870
GLOBAL	INT		TAG	1871	Reserved_1871
GLOBAL	INT		TAG	1872	Reserved_1872
GLOBAL	INT		TAG	1873	Reserved_1873
GLOBAL	INT		TAG	1874	Reserved_1874
GLOBAL	INT		TAG	1875	Reserved_1875
GLOBAL	INT		TAG	1876	Reserved_1876
GLOBAL	INT		TAG	1877	Reserved_1877
GLOBAL	INT		TAG	1878	Reserved_1878
GLOBAL	INT		TAG	1879	Reserved_1879
GLOBAL	INT		TAG	1880	Reserved_1880
!	***	System Banner
GLOBAL	INT		TAG	1881	g_Hmi_Banner_Enable	
GLOBAL	INT		TAG	1882	g_Hmi_Banner_MsgLineNum
GLOBAL	INT		TAG	1883	Reserved_1883
GLOBAL	INT		TAG	1884	Reserved_1884
GLOBAL	INT		TAG	1885	Reserved_1885
GLOBAL	INT		TAG	1886	Reserved_1886
GLOBAL	INT		TAG	1887	Reserved_1887
GLOBAL	INT		TAG	1888	Reserved_1888
GLOBAL	INT		TAG	1889	Reserved_1889
GLOBAL	INT		TAG	1890	Reserved_1890
GLOBAL	INT		TAG	1891	Reserved_1891

! *** Buffer #9 (Serial RS-232C Interface Program) ***
GLOBAL INT g_RS232C_ComPortStatus(10);

GLOBAL INT EL6002_CH1_IN(22), EL6002_CH1_STATUS
GLOBAL INT EL6002_CH1_OUT(22), EL6002_CH1_CTRL

GLOBAL INT g_enum_MesureEqpCmd_Default;
GLOBAL INT g_enum_MesureEqpCmd_InitRequest;
GLOBAL INT g_enum_MesureEqpCmd_MeasurementRequest;
GLOBAL INT g_enum_MesureEqpCmd_MeasurStopRequest;

GLOBAL INT g_enum_MesureEqpStatus_Default;
GLOBAL INT g_enum_MesureEqpStatus_InitComplete;
GLOBAL INT g_enum_MesureEqpStatus_Measuring;
GLOBAL INT g_enum_MesureEqpStatus_Error;

GLOBAL INT g_BlowByMeter_442_CtrlCMD; 
GLOBAL INT g_BlowByMeter_442_Status; 
GLOBAL INT g_BlowByMeter_442_ErrorCode;

GLOBAL REAL g_BlowByMean_Value;
GLOBAL REAL g_BlowByMean_Value_MesureTime;

GLOBAL INT g_SmokeMeter_415_CtrlCMD; 
GLOBAL INT g_SmokeMeter_415_Status; 
GLOBAL INT g_SmokeMeter_415_ErrorCode;

GLOBAL REAL g_SmokeMeterMeasureResult_ItemNo;
GLOBAL REAL g_SmokeMeterMeasureResult_ValueList(6);

GLOBAL INT g_FuelMeter_733_CtrlCMD;
GLOBAL INT g_FuelMeter_733_Status;
GLOBAL INT g_FuelMeter_733_ErrorCode;
GLOBAL REAL g_FuelMeterMeasureResult_ItemNo;
GLOBAL REAL g_FuelMeterMeasureResult_MeanConsumption;
GLOBAL REAL g_FuelMeterMeasureResult_AveTime;
GLOBAL REAL g_FuelMeterMeasureResult_MeasuringWeight;
GLOBAL REAL g_FuelMeterMeasureResult_Filling_level;

