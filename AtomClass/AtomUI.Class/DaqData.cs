﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;

namespace AtomUI.Class
{
    public class DaqData
    {
        public static object ojbLockAvg = new object();

        public static Thread m_trdDequeueCtn = null;
        public static Thread m_trdDequeueConstant = null;
        public static Queue<DEF_QUEQE> qBuffConstant = new Queue<DEF_QUEQE>();
        public static Queue<DEF_QUEQE> qBuffCtn = new Queue<DEF_QUEQE>();
        public static bool DataLoggingAvg = false;
        public static bool DataLoggingCtn = false;
        public static bool DataLoggingConstant = false;
        public static bool DataLogging = false;
        private static List<VariableInfo> InstantDataLogging_Variables = new List<VariableInfo>();
        private static List<VariableInfo> ConstantDataLogging_Variables = new List<VariableInfo>();

        public static SysVarInfo sysVarInfo = new SysVarInfo();
        public static bool dataIndexParsed = false;

        public static SysStatus SystemStatus = new SysStatus();
        static int InitialTime = 0;

        public static void GetSysStatus()
        {
            if (Ethercat.g_PC_Read_Data != null)
            {
                foreach (SysStatusVariable sys in SystemStatus.Status)
                {
                    if (sys.Address < 0x60)
                        sys.Value = Ethercat.g_PC_Read_Data[sys.Address];
                    else
                        sys.Value = Ethercat.g_PC_Read_Data[sys.Address] / 10;

                }

                SystemStatus.ParseStatus();
            }
        }

        public static List<string> getVarTypeList()
        {
            List<string> varList = new List<string>();
            for (int i = 0; i < 13; i++)
            {
                varList.Add(((VariableType)i).ToString());
            }
            return varList;
        }

        public static VariableType getVarType(string VarTypeString)
        {
            for (int i = 0; i < 13; i++)
            {
                if (((VariableType)i).ToString() == VarTypeString)
                {
                    return (VariableType)i;
                }
            }
            return VariableType.Calculation;
        }

        public static void ParseValue()
        {
            //GetValuesDigital(sysVarInfo.FrontPanel_Variables_Input, Ethercat.g_PC_Read_Data);
            //GetValuesDigital(sysVarInfo.FrontPanel_Variables_Output, Ethercat.g_PC_Read_Data);
            //GetValuesDigital(sysVarInfo.HMI_IOVariables, Ethercat.g_PC_Read_Data);
            //GetValuesDigital(sysVarInfo.DI_Variables, Ethercat.g_PC_Read_Data);
            //GetValuesAnalog(sysVarInfo.AI_Variables, Ethercat.g_PC_Read_Data);
            //GetValuesDigital(sysVarInfo.DO_Variables, Ethercat.g_PC_Read_Data);
            //GetValuesDigital(sysVarInfo.AO_Variables, Ethercat.g_PC_Read_Data);
            //GetValuesAnalog(sysVarInfo.Calc_Variables, Ethercat.g_PC_Read_Data);
            //try
            //{
            //    //G = UDiff / URef x Emax / Cn = 2.79995 mV / 9.9997 V x 50 kg / 2(mV / V)) = 7.00009 kg
            //    sysVarInfo.Calc_Variables[0].IOValue = (int)((sysVarInfo.AI_Variables[4].RealValue / sysVarInfo.AI_Variables[5].RealValue * 967 / 2 - 58.4) * 100);
            //}
            //catch { }

            //GetSysStatus();


        }

        public static string strAvgFileName;
        public static string strAvgFileNameOnly;
        public static string strCtnFileName;
        public static string strCtnFileNameOnly;
        public static string strConstantFileName;
        public static string strConstantFileNameOnly;
        
        public static int rowTotalCountCtn = 0;
        public static int rowTotalCountConstant = 0;

        public static int fileCountConstant = 0;
        public static int rowCountConstant = 0;
        public static long prevStopWatchTickCtn = 0;
        public static long nowStopWatchTickCtn = 0;
        public static long prevStopWatchTickConstant = 0;
        public static long nowStopWatchTickConstant = 0;
        public static bool interreptCtnFromStepCtn = true;
        public static bool m_bUseFirstOnce = false;
        public static DateTime dtAvgStartTime;

        public static string strFileName;
        public static string strFileNameOnly;
        private static int BufferStart = 0;
        private static int BufferStartIndex = 0x900;
        private static int BufferIncreaseIndex = 0x100;
        private static int BufferSize = 20;
        private static int LastBufferUniqueNo = 0;
        private static int LastBufferNo = 0;
        public static double[] sum;
        public static int count = 0;


        public static void InitDaqDataLogging()
        {
            m_bUseFirstOnce = false;
        }

        public static void DataSaveStartAvg(bool bUseFirstOnce = false)
        {
            dtAvgStartTime = DateTime.Now;
            MakeAvgFile(!m_bUseFirstOnce);

            if (bUseFirstOnce)
                m_bUseFirstOnce = true;

            DaqData.DataLoggingAvg = true;
        }
        public static void DataSaveStartCtn(bool binterreptCtnFromStepCtn = true)
        {
            interreptCtnFromStepCtn = binterreptCtnFromStepCtn;

            DaqData.DataLoggingCtn = true;
            InstantDataLogging_Variables = sysVarInfo.InstantDataLogging_Variables;

            //sum = new double[DaqData.sysVarInfo.InstantDataLogging_Variables.Count];
            //for (int i = 0; i < DaqData.sysVarInfo.InstantDataLogging_Variables.Count; i++)
            //{
            //    sum[i] = 0;
            //}

            string Header;
            ///////////////////////
            Header = "StartTime";
            ///////////////////////


            foreach (VariableInfo var in InstantDataLogging_Variables)
                Header = Header + "," + var.VariableName;
            strCtnFileNameOnly = Ethercat.sysInfo.sysSettings.DataLoggingPrefix + "_Ctn_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";

            if (interreptCtnFromStepCtn)
            {
                strCtnFileName = Ethercat.sysInfo.sysSettings.DataLoggingDir + "\\Instant\\" + strCtnFileNameOnly;
            }
            else
            {
                strCtnFileName = Ethercat.sysInfo.sysSettings.DataLoggingDir + "\\StepMode\\" + strCtnFileNameOnly;
            }

            try
            {
                System.IO.File.AppendAllText(strCtnFileName, Header + "\r\n", Encoding.UTF8);
            }
            catch (DirectoryNotFoundException e)
            {
                System.IO.Directory.CreateDirectory(Ethercat.sysInfo.sysSettings.DataLoggingDir + "\\Instant");
                System.IO.Directory.CreateDirectory(Ethercat.sysInfo.sysSettings.DataLoggingDir + "\\Constant");
                //System.IO.Directory.CreateDirectory(Ethercat.sysInfo.sysSettings.DataLoggingDir + "\\StepMode");
                System.IO.File.AppendAllText(strCtnFileName, Header + "\r\n", Encoding.UTF8);
            }
        }
        public static void DataSaveStartConstant()
        {
            DaqData.DataLoggingConstant = true;
            ConstantDataLogging_Variables = sysVarInfo.ConstantDataLogging_Variables;

            //for (int i = 0; i < DaqData.sysVarInfo.ConstantDataLogging_Variables.Count; i++)
            //{
            //    sum[i] = 0;
            //}

            string Header;
            ///////////////////////
            Header = "StartTime";
            ///////////////////////
            foreach (VariableInfo var in ConstantDataLogging_Variables)
                Header = Header + "," + var.VariableName;
            strConstantFileNameOnly = Ethercat.sysInfo.sysSettings.DataLoggingPrefix + "_Constant_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + fileCountConstant + ".csv";
            strConstantFileName = Ethercat.sysInfo.sysSettings.DataLoggingDir + "\\Constant\\" + strConstantFileNameOnly;

            try
            {
                System.IO.File.AppendAllText(strConstantFileName, Header + "\r\n", Encoding.UTF8);
            }
            catch (DirectoryNotFoundException e)
            {
                Directory.CreateDirectory(Ethercat.sysInfo.sysSettings.DataLoggingDir + "\\Constant");
                System.IO.File.AppendAllText(strConstantFileName, Header + "\r\n", Encoding.UTF8);
            }
            ///////////////////////
        }


        public static void MakeAvgFile(bool bUseNewFile = true)
        {
            try
            {
                DaqData.sum = new double[DaqData.sysVarInfo.InstantDataLogging_Variables.Count];
                for (int i = 0; i < DaqData.sysVarInfo.InstantDataLogging_Variables.Count; i++)
                {
                    DaqData.sum[i] = 0;
                }
                if (!bUseNewFile)
                    return;

                string Header;
                ///////////////////////
                Header = "StartTime, EndTime";
                ///////////////////////
                foreach (VariableInfo var in DaqData.sysVarInfo.InstantDataLogging_Variables)
                    Header = Header + "," + var.VariableName;
                DaqData.strAvgFileNameOnly = Ethercat.sysInfo.sysSettings.DataLoggingPrefix + "_Avg_" + dtAvgStartTime.ToString("yyyyMMddHHmmss") + ".csv";

                DaqData.strAvgFileName = Ethercat.sysInfo.sysSettings.DataLoggingDir + "\\Instant\\" + DaqData.strAvgFileNameOnly;
                try
                {
                    System.IO.File.AppendAllText(DaqData.strAvgFileName, Header + "\r\n", Encoding.UTF8);
                }
                catch (DirectoryNotFoundException e)
                {
                    System.IO.Directory.CreateDirectory(Ethercat.sysInfo.sysSettings.DataLoggingDir + "\\Instant");
                    System.IO.Directory.CreateDirectory(Ethercat.sysInfo.sysSettings.DataLoggingDir + "\\Constant");
                    System.IO.Directory.CreateDirectory(Ethercat.sysInfo.sysSettings.DataLoggingDir + "\\StepMode");
                    System.IO.File.AppendAllText(DaqData.strAvgFileName, Header + "\r\n", Encoding.UTF8);
                }
            }
            catch (Exception ex)
            {
                CommonFunction.LogStackTrace(ex.Message, ex.StackTrace);
                MessageBox.Show(new Form { TopMost = true }, ex.Message + ex.StackTrace + "SaveSysInfo()");
            }
        }
        public static string DataSaveStopAvg()
        {
            string Value = "";
            try
            {
                lock (ojbLockAvg)
                {
                    if (DaqData.DataLoggingAvg)
                    {
                        DaqData.DataLoggingAvg = false;

                        Value = dtAvgStartTime.ToString("yyyy_MM_dd HH:mm:ss") + "." + dtAvgStartTime.Millisecond.ToString("000");
                        Value += "," + DateTime.Now.ToString("yyyy_MM_dd HH:mm:ss") + "." + DateTime.Now.Millisecond.ToString("000");

                        for (int i = 0; i < DaqData.sysVarInfo.InstantDataLogging_Variables.Count; i++)
                        {
                            if (DaqData.sysVarInfo.InstantDataLogging_Variables[i].Round == 0)
                                Value += "," + Math.Round(sum[i] / count, DaqData.sysVarInfo.InstantDataLogging_Variables[i].Round).ToString();
                            else if (DaqData.sysVarInfo.InstantDataLogging_Variables[i].Round == 1)
                                Value += "," + Math.Round(sum[i] / count, DaqData.sysVarInfo.InstantDataLogging_Variables[i].Round).ToString("F1");
                            else if (DaqData.sysVarInfo.InstantDataLogging_Variables[i].Round == 2)
                                Value += "," + Math.Round(sum[i] / count, DaqData.sysVarInfo.InstantDataLogging_Variables[i].Round).ToString("F2");
                            else if (DaqData.sysVarInfo.InstantDataLogging_Variables[i].Round == 3)
                                Value += "," + Math.Round(sum[i] / count, DaqData.sysVarInfo.InstantDataLogging_Variables[i].Round).ToString("F3");
                            else
                                Value += "," + Math.Round(sum[i] / count, DaqData.sysVarInfo.InstantDataLogging_Variables[i].Round).ToString();
                        }

                        count = 0;
                        for (int i = 0; i < InstantDataLogging_Variables.Count; i++)
                        {
                            sum[i] = 0;
                        }
                        System.IO.File.AppendAllText(strAvgFileName, Value + "\r\n", Encoding.UTF8);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonFunction.LogStackTrace(ex.Message, ex.StackTrace);
                MessageBox.Show(new Form { TopMost = true }, ex.Message + ex.StackTrace + "SaveSysInfo()");
            }

            return Value;
        }

        public static void DataSaveStopCtn()
        {
            DaqData.DataLoggingCtn = false;
        }



        public static void DataSaveAvg()
        {
            string Value = "";
            if (DataLoggingAvg)
            {

                for (int i = 0; i < DaqData.sysVarInfo.InstantDataLogging_Variables.Count; i++)
                {
                    sum[i] += DaqData.sysVarInfo.InstantDataLogging_Variables[i].RealValue;
                }

                count++;

            }
        }
        public static string[] str = new string[100];
        public static int a = 0;

        public static void DataSaveCtn(bool pause)
        {
            if (pause)
                return;

            if (DataLoggingCtn)
            {
                DaqData.rowTotalCountCtn++;
                //if (nowStopWatchTickCtn - prevStopWatchTickCtn > SaveData.saveData.DataCtnLoggingInterval)
                //{
                string Value = DateTime.Now.ToString("yyyy_MM_dd HH:mm:ss") + "." + DateTime.Now.Millisecond.ToString("000");
                //string Value = DateTime.Now.ToString("yyyy_MM_dd HH:mm:ss");
                prevStopWatchTickCtn = nowStopWatchTickCtn;

                foreach (VariableInfo var in InstantDataLogging_Variables)
                {
                    if (var.Round == 0)
                        Value = Value + "," + var.RealValue.ToString();
                    else if (var.Round == 1)
                        Value = Value + "," + var.RealValue.ToString("F1");
                    else if (var.Round == 2)
                        Value = Value + "," + var.RealValue.ToString("F2");
                    else
                        Value = Value + "," + var.RealValue.ToString();
                } //foreach (VariableInfo AI in DataLogging_Variables)

                int nSaveCnt = 1000 / SaveData.saveData.DataCtnLoggingInterval; // 1초마다 파일쓰기
                stBuffCtn.nCnt++;
                stBuffCtn.strFileName = strCtnFileName;
                if (stBuffCtn.nCnt < nSaveCnt)
                    stBuffCtn.stRaw += Value + "\r\n";
                else
                    stBuffCtn.stRaw += Value;


                //Console.WriteLine("ctn : " +  " / " + stBuffCtn.nCnt + "     " + stBuffCtn.stRaw);
                if (stBuffCtn.nCnt >= nSaveCnt)
                {
                    qBuffCtn.Enqueue(stBuffCtn);
                    stBuffCtn = new DEF_QUEQE();
                }
            }
            else
            {
                if (stBuffCtn.nCnt > 0)
                {
                    qBuffCtn.Enqueue(stBuffCtn);
                    stBuffCtn = new DEF_QUEQE();
                }
            }
        }

        private static void DequeueCtn()
        {
            DEF_QUEQE stRecv = new DEF_QUEQE();
            while (true)
            {

                if (qBuffCtn.Count > 0)
                {
                    stRecv = qBuffCtn.Dequeue();
                    try
                    {
                        System.IO.File.AppendAllText(stRecv.strFileName, stRecv.stRaw + "\r\n", Encoding.UTF8);
                    }
                    catch (Exception ex)
                    {
                        CommonFunction.LogStackTrace(ex.Message, ex.StackTrace);
                    }
                }
                else
                {
                    Thread.Sleep(10);
                }
            }
        }




        public static uint unConstantLoggingCnt = 0;
        public static DEF_QUEQE stBuffConstant = new DEF_QUEQE();
        public static DEF_QUEQE stBuffCtn = new DEF_QUEQE();
        public static void DataSaveConstant()
        {
            if (DataLoggingConstant)
            {
                DaqData.rowTotalCountConstant++;
                //if (nowStopWatchTickConstant - prevStopWatchTickConstant > SaveData.saveData.DataConstantLoggingInterval)
                //{
                string Value = DateTime.Now.ToString("yyyy_MM_dd HH:mm:ss") + "." + DateTime.Now.Millisecond.ToString("000");
                //string Value = DateTime.Now.ToString("yyyy_MM_dd HH:mm:ss") + "." + DateTime.Now.Millisecond.ToString("000") + "-" + nowStopWatchTickConstant;
                //string Value = DateTime.Now.ToString("yyyy_MM_dd HH:mm:ss");
                prevStopWatchTickConstant = nowStopWatchTickConstant;

                foreach (VariableInfo var in ConstantDataLogging_Variables)
                {
                    if (var.Round == 0)
                        Value = Value + "," + var.RealValue.ToString();
                    else if (var.Round == 1)
                        Value = Value + "," + var.RealValue.ToString("F1");
                    else if (var.Round == 2)
                        Value = Value + "," + var.RealValue.ToString("F2");
                    else
                        Value = Value + "," + var.RealValue.ToString();
                }

                int nSaveCnt = 1000 / SaveData.saveData.DataConstantLoggingInterval;
                stBuffConstant.nCnt++;
                stBuffConstant.strFileName = strConstantFileName;
                unConstantLoggingCnt++;
                if (stBuffConstant.nCnt < nSaveCnt && unConstantLoggingCnt < SaveData.saveData.DataMaxRowsPerFile)
                    stBuffConstant.stRaw += Value + "\r\n";
                else
                    stBuffConstant.stRaw += Value;

                //Console.WriteLine("constant : " + unConstantLoggingCnt + " / " + stBuffConstant.nCnt + "     " + stBuffConstant.stRaw);
                if (stBuffConstant.nCnt >= nSaveCnt || unConstantLoggingCnt >= SaveData.saveData.DataMaxRowsPerFile)
                {
                    qBuffConstant.Enqueue(stBuffConstant);
                    stBuffConstant = new DEF_QUEQE();
                    if (unConstantLoggingCnt >= SaveData.saveData.DataMaxRowsPerFile)
                    {
                        unConstantLoggingCnt = 0;
                        Console.WriteLine("Reset unConstantLoggingCnt");
                    }
                }
            }
            else
            {
                if (stBuffConstant.nCnt > 0)
                {
                    qBuffConstant.Enqueue(stBuffConstant);
                    stBuffConstant = new DEF_QUEQE();
                    unConstantLoggingCnt = 0;
                }
            }
        }




        private static void DequeueConstant()
        {
            DEF_QUEQE stRecv = new DEF_QUEQE();
            while (true)
            {

                if (qBuffConstant.Count > 0)
                {
                    stRecv = qBuffConstant.Dequeue();
                    rowCountConstant += stRecv.nCnt;
                    //Console.WriteLine("rowCountConstant : " + rowCountConstant);
                    if (DaqData.rowCountConstant <= SaveData.saveData.DataMaxRowsPerFile)
                    {
                        //swCtn.Start();
                        //stopWatchStartTime = swCtn.ElapsedMilliseconds;

                        try
                        {
                            System.IO.File.AppendAllText(stRecv.strFileName, stRecv.stRaw + "\r\n", Encoding.UTF8);
                        }
                        catch (Exception ex)
                        {
                            CommonFunction.LogStackTrace(ex.Message, ex.StackTrace);
                        }

                        //swCtn.Stop();
                        //Console.WriteLine("RemainQ : " + qBuffConstant.Count + "  / ProcTime(ms):" + (swCtn.ElapsedMilliseconds - stopWatchStartTime).ToString("F3"));
                        //swCtn.Reset();

                    }

                    if (DaqData.rowCountConstant >= SaveData.saveData.DataMaxRowsPerFile)
                    {
                        DaqData.rowCountConstant = 0;
                        DaqData.prevStopWatchTickConstant = 0;
                        DaqData.fileCountConstant++;
                        DaqData.DataSaveStartConstant();
                    }

                }
                else
                {
                    Thread.Sleep(10);
                }
            }
        }




        //public static long stopWatchStartTime = 0;
        //public static Stopwatch swCtn = new Stopwatch();

        public static void StartThr_Save()
        {
            m_trdDequeueCtn = new Thread(new ThreadStart(DequeueCtn));
            m_trdDequeueCtn.IsBackground = true;
            m_trdDequeueCtn.Start();
            m_trdDequeueConstant = new Thread(new ThreadStart(DequeueConstant));
            m_trdDequeueConstant.IsBackground = true;
            m_trdDequeueConstant.Start();
        }

        public static void StopThr_Save()
        {
            if (m_trdDequeueCtn != null)
            {
                m_trdDequeueCtn.Abort(1000);
                m_trdDequeueCtn.Join(1000);
            }
            if (m_trdDequeueConstant != null)
            {
                m_trdDequeueConstant.Abort(1000);
                m_trdDequeueConstant.Join(1000);
            }
        }

        public struct DEF_QUEQE
        {
            public int nCnt;
            public string strFileName;
            public string stRaw;
        }


        public static void GetValuesAnalog(List<VariableInfo> Variables, int[] Vector)
        {
            if (Vector != null)
            {
                foreach (VariableInfo var in Variables)
                {
                    if (var.IOIndex != 0)
                    {
                        var.IOValue = ((double)Vector[var.IOIndex]) / 1000;
                    }
                }
            }
        }

        public static void GetValuesDigital(List<VariableInfo> Variables, int[] Vector)
        {
            if (Vector != null)
            {
                foreach (VariableInfo var in Variables)
                {
                    if (var.IOIndex != 0)
                    {
                        //int Mask = (int)(Math.Pow(2, var.ChannelInfo.ChannelBit));
                        var.IOValue = Vector[var.IOIndex];//(Vector[var.IOIndex] & Mask) / Mask;

                    }
                }
            }
        }

        public static void GetValue(VariableInfo var, int[] Vector)
        {
            if (var.IOIndex != 0)
            {
                if (var.ChannelInfo.ModuleInfo.ChannelType == ChannelType.Analog)
                {
                    var.IOValue = Vector[var.IOIndex];
                }
                else
                {
                    int Mask = (int)(Math.Pow(2, var.ChannelInfo.ChannelBit));
                    var.IOValue = (Vector[var.IOIndex] & Mask) / Mask;
                }
            }
        }

        public static void GetValue(VariableInfo var, int[] Vector, int Offset)
        {
            if (var.IOIndex != 0)
            {
                if (var.ChannelInfo.ModuleInfo.ChannelType == ChannelType.Analog)
                {
                    var.IOValue = Vector[var.IOIndex- Offset];
                }
                else
                {
                    int Mask = (int)(Math.Pow(2, var.ChannelInfo.ChannelBit));
                    var.IOValue = (Vector[var.IOIndex] & Mask) / Mask;
                }
            }
        }

        public static void GetAddressIndex()
        {
            sysVarInfo.FrontPanel_Variables_Output.Clear();
            sysVarInfo.FrontPanel_Variables_Input.Clear();
            sysVarInfo.FrontPanel_Variables_Output.Add(FindVariable("g_HMI_DO_DynoOn_Lamp", sysVarInfo.HMI_IOVariables));
            sysVarInfo.FrontPanel_Variables_Output.Add(FindVariable("g_HMI_DO_EngIGOn_Lamp", sysVarInfo.HMI_IOVariables));
            sysVarInfo.FrontPanel_Variables_Output.Add(FindVariable("g_HMI_DO_Manual_Lamp", sysVarInfo.HMI_IOVariables));
            sysVarInfo.FrontPanel_Variables_Output.Add(FindVariable("g_HMI_DO_Remote_Lamp", sysVarInfo.HMI_IOVariables));
            sysVarInfo.FrontPanel_Variables_Output.Add(FindVariable("g_HMI_DO_Automatic_Lamp", sysVarInfo.HMI_IOVariables));

            //VarAddressMappingInput(sysVarInfo.FrontPanel_Variables_Input);
            //VarAddressMappingOutput(sysVarInfo.FrontPanel_Variables_Output);
            //VarAddressMappingInput(sysVarInfo.HMI_IOVariables);
            //VarAddressMappingInput(sysVarInfo.DI_Variables);
            //VarAddressMappingInput(sysVarInfo.AI_Variables);
            //VarAddressMappingOutput(sysVarInfo.DO_Variables);
            //VarAddressMappingOutput(sysVarInfo.AO_Variables);

            dataIndexParsed = true;
        }

        public static VariableInfo FindVariable(string VarName, List<VariableInfo> VarList)
        {
            VariableInfo resVar = null;
            foreach (VariableInfo Var in VarList)
            {
                if (Var.NorminalName == VarName)
                {
                    resVar = Var;
                    break;
                }
            }
            return resVar;
        }

        public static void VarAddressMappingInput(List<VariableInfo> Variables)
        {
            int DataBaseAddress = 512;
            int OffsetBaseAddress = 1024;
            foreach (VariableInfo var in Variables)
            {
                if (var.ChannelInfo != null)
                {
                    for (int ind = 0; ind < 36; ind++)
                    {
                        if (var.ChannelInfo.Offset == Ethercat.g_PC_Read_Data[OffsetBaseAddress + ind])
                        {
                            var.IOIndex = DataBaseAddress + ind;
                            break;
                        }
                    }
                }
            }
        }

        public static void ClearLists()
        {
            sysVarInfo.ClearLists();
        }

        public static void Init()
        {
            Init_HMI_IOVariables();
            if (Ethercat.chInfo.HMI_Channels.Count == sysVarInfo.HMI_IOVariables.Count)
            {
                for (int i = 0; i < sysVarInfo.HMI_IOVariables.Count; i++)
                {
                    sysVarInfo.HMI_IOVariables[i].ChannelInfo = Ethercat.chInfo.HMI_Channels[i];
                }
            }
            Init_HMI_Variables();
            Init_DI_Variables();
            Init_DO_Variables();
            Init_AI_Variables();
            Init_AO_Variables();
            Init_Calc_Variables();

            SaveSysInfo("VarList.xml");
            Ethercat.sysInfo.sysSettings.VarInfoFileName = "VarList.xml";
            Ethercat.sysInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SysInfoDir + "\\" + Ethercat.sysInfo.sysSettings.SysInfoFileName);
        }

        public static void SaveSysInfo()
        {
            try
            {
                sysVarInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.VarInfoDir + "\\" + Class.Ethercat.sysInfo.sysSettings.VarInfoFileName);
            }
            catch (Exception ex)
            {
            }
        }

        public static void LoadSysInfo()
        {
            try
            {
                sysVarInfo.Load(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.VarInfoDir + "\\" + Ethercat.sysInfo.sysSettings.VarInfoFileName);
            }
            catch (Exception ex)
            { }
        }

        public static void SaveSysInfo(string FileName)
        {
            FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write);
            XmlSerializer xs = new XmlSerializer(sysVarInfo.GetType());
            xs.Serialize(fs, sysVarInfo);
            fs.Close();
        }

        public static void LoadSysInfo(string FileName)
        {
            sysVarInfo.ClearLists();
            FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
            XmlSerializer xs = new XmlSerializer(sysVarInfo.GetType());
            Object dd = xs.Deserialize(fs);
            sysVarInfo = (SysVarInfo)dd;
            fs.Close();
        }

        public static void Init_HMI_Variables()
        {
            sysVarInfo.HMI_Variables.Add(new VariableInfo("-", "-", "-", 111, sysVarInfo.HMI_Variables.Count));

        }

        public static void Init_HMI_IOVariables()
        {
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("OPEN_LOOP_SW", "g_HMI_DI_DynoService_Button", "OPEN LOOP TEST SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ENG_MON_SW", "g_HMI_DI_Monitor_Button", "ENG MONITOR SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("MISC_SW", "g_HMI_DI_Misc_Button", "MISC ON/OFF  SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("MAN_SW", "g_HMI_DI_Manual_Button", "MANUAL SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("PANEL_SW", "PANEL_SW", "PANEL ENABLE  SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("AUTO_SW", "g_HMI_DI_Automatic_Button", "AUTOMATIC SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP1_SW", "SP1_SW", "SP1_SW", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("REM_SW", "g_HMI_DI_Remote_Button", "REMOTE SWITCH for BK DAS", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("PREHEAT_SW", "g_HMI_DI_EngPreheatOn_Button", "PREHEAT SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("IG_SW", "g_HMI_DI_EngIGOn_Button", "IG ON/OFF SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ENG_STOP_SW", "g_HMI_DI_Stop_Button", "ENG STOP SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ENG_START_SW", "g_HMI_DI_EngStart_Button", "ENG START SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP3_SW", "SP3_SW", "SPARE3 SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP4_SW", "SP4_SW", "SPARE4 SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP5_SW", "SP5_SW", "SPARE5 SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP6_SW", "SP6_SW", "SPARE6 SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("BKREADY", "g_HMI_DI_BKReady_Button", "READY SWITCH for BK DAS", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP8_SW", "SP8_SW", "SPARE8 SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP9_SW", "SP9_SW", "SPARE9 SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP10_SW", "SP10_SW", "SPARE10 SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("DYNO_SW", "g_HMI_DI_DynoOn_Button", "DYNO ON/OFF SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ALM_RESET_SW", "g_HMI_DI_Reset_Button", "ALARM RESET SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("CFG_SW", "g_HMI_DI_ConfigMenu_Button", "CONFIG MENU SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ALM HIST_SW", "g_HMI_DI_AlarmHistory_Button", "ALARM HIST SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP11_SW", "SP11_SW", "SPARE11 SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP12_SW", "g_HMI_DI_HornOff_Button", "SPARE12 SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("IDLE CTR_SW", "g_HMI_DI_IdleContrl_Button", "IDLE CONTROL SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("IDLE_SW", "g_HMI_DI_Idle_Button", "IDLE SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("N/A_SW", "g_HMI_DI_DyRPM_EngTA_Button", "N/A SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("T/A_SW", "g_HMI_DI_DyTo_EngTA_Button", "T/A SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("N/T_SW", "g_HMI_DI_DyRPM_EngTo_Button", "N/T SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("T/N_SW", "g_HMI_DI_DyTo_EngRPM_Button", "T/N SWITCH", sysVarInfo.HMI_IOVariables.Count));

            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("OPEN_LOOP_LP", "g_HMI_DO_DynoService_Lamp", "OPEN LOOP TEST LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ENG_MON_LP", "g_HMI_DO_Monitor_Lamp", "ENG MONITOR LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("MISC_LP", "g_HMI_DO_MiscOn_Lamp", "MISC ON/OFF  LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("MAN_LP", "g_HMI_DO_Manual_Lamp", "MANUAL LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("PANEL_LP", "g_HMI_DO_PanelActive_Lamp", "PANEL ENABLE  LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("AUTO_LP", "g_HMI_DO_Automatic_Lamp", "AUTO MATIC LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("REM_LP", "SP1_LP", "REMOTE LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP2_LP", "g_HMI_DO_Remote_Lamp", "SPARE2 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("PREHEAT_LP", "g_HMI_DO_EngPreheatOn_Lamp", "PREHEAT LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("IG_LP", "g_HMI_DO_EngIGOn_Lamp", "IG ON/OFF LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ENG_STOP_LP", "g_HMI_DO_Stop_Lamp", "ENG STOP LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ENG_START_LP", "g_HMI_DO_EngStarting_Lamp", "ENG START LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP3_LP", "SP3_LP", "SPARE3 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP4_LP", "SP4_LP", "SPARE4 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP5_LP", "SP5_LP", "SPARE5 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP6_LP", "SP6_LP", "SPARE6 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP7_LP", "SP7_LP", "READY", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP8_LP", "SP8_LP", "SPARE8 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP9_LP", "SP9_LP", "SPARE9 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP10_LP", "SP10_LP", "SPARE10 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("DYNO_LP", "g_HMI_DO_DynoOn_Lamp", "DYNO ON/OFF LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ALM_RESET_LP", "g_HMI_DO_AlarmReset_Lamp", "ALARM RESET LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("CFG_MENU_LP", "g_HMI_DO_ConfigMenu_Lamp", "CONFIG MENU LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ALM HIST_LP", "g_HMI_DO_AlarmHistory_Lamp", "ALARM HIST LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP11_LP", "SP11_LP", "SPARE11 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP12_LP", "g_HMI_DO_HornOff_Lamp", "SPARE12 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("IDLE_CTR_LP", "g_HMI_DO_IdleControl_Lamp", "IDLE CONTROL LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("IDLE_LP", "g_HMI_DO_Idle_Lamp", "IDLE LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("N/A_LP", "g_HMI_DO_DyRPM_EngTA_Lamp", "N/A LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("T/A_LP", "g_HMI_DO_DyTo_EngTA_Lamp", "T/A LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("N/T_LP", "g_HMI_DO_DyRPM_EngTo_Lamp", "N/T LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("T/N_LP", "g_HMI_DO_DyTo_EngRPM_Lamp", "T/N LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("KNOB_DYNO_Counter", "g_HMI_AI_Dyno_CalRefCnt", "KNOB DYNO A", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("KNOB_ENGINE_Counter", "g_HMI_AI_Eng_CalRefCnt", "KNOB ENGINE A", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("KNOB_DYNO_Period", "KNOB_DYNO_Period", "KNOB DYNO B", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("KNOB_ENGINE_Period", "KNOB_ENGINE_Period", "KNOB ENGINE B", sysVarInfo.HMI_IOVariables.Count));
        }

        public static void Init_DI_Variables()
        {
            sysVarInfo.DI_Variables.Add(new VariableInfo("RMT_DY_F", "g_DI_DynoTroubleSignal", "DYNO TROUBLE", sysVarInfo.DI_Variables.Count));
            sysVarInfo.DI_Variables.Add(new VariableInfo("RMT_ON", "g_DI_RemoteOnSignal", "REMOTE ON from BK DAS", sysVarInfo.DI_Variables.Count));
            sysVarInfo.DI_Variables.Add(new VariableInfo("RMT_DY_SCS", "g_DI_DynoSpeedModeSignal", "DY SPEED MODE from BK DAS", sysVarInfo.DI_Variables.Count));
            sysVarInfo.DI_Variables.Add(new VariableInfo("RMT_DY_TC", "g_DI_DynoTorqueModeSignal", "DY TORQUE MODE from BK DAS", sysVarInfo.DI_Variables.Count));
            sysVarInfo.DI_Variables.Add(new VariableInfo("RMT_ENG_AC", "g_DI_EngineAlphaModeSignal", "ENG ALPHA MODE from BK DAS", sysVarInfo.DI_Variables.Count));
            sysVarInfo.DI_Variables.Add(new VariableInfo("RMT_ENG_TC", "g_DI_EngineTorqueModeSignal", "ENG TORQUE MODE from BK DAS", sysVarInfo.DI_Variables.Count));
            sysVarInfo.DI_Variables.Add(new VariableInfo("OP_EMO", "g_DI_EMSButton", "OP PANNEL EMERGENCY STOP", sysVarInfo.DI_Variables.Count));
            sysVarInfo.DI_Variables.Add(new VariableInfo("SG_SW", "g_DI_SafetyGuardSignal", "SHAFT GUARD SWITCH", sysVarInfo.DI_Variables.Count));
            sysVarInfo.DI_Variables.Add(new VariableInfo("S_REL", "g_DI_SafetyRelaySignal", "SAFETY RELAY", sysVarInfo.DI_Variables.Count));
        }

        public static void Init_DO_Variables()
        {
            sysVarInfo.DO_Variables.Add(new VariableInfo("DO_READY", "g_DO_BKReadySignal", "READY for BK", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("DO_CTL_ON", "g_DO_DynoOnSignal", "DYNO CONTROL ON", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("DO_TS_VALVE1", "DO_TS_VALVE1", "Thermal shock Valve 1", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("DO_TS_VALVE2", "DO_TS_VALVE2", "Thermal shock Valve 2", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("DO_TS_HEATER", "DO_TS_HEATER", "Thermal shock Heater On", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("DO_RESET", "g_DO_DynoResetSignal", "DYNO RESET", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("DO_IDLE_SW", "g_DO_ThrottleIVSSignal", "ELECTRONIC THROTTLE IVS", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("DO_PREHEAT", "g_DO_PreHeatSignal", "PREHEAT", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("START", "g_DO_StartSignal", "START", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("IG", "g_DO_IgnitionSignal", "IGNITION", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("HORN", "g_DO_HornSignal", "HORN", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("ALARM_LAMP", "g_DO_Alarm_Lamp", "ALARM LAMP", sysVarInfo.DO_Variables.Count));
        }

        public static void Init_AI_Variables()
        {
            VariableInfo newVI = null;
            ValuePair vpValue = null;
            newVI = new VariableInfo("DY_SV_RMT", "g_AI_Target_Dyno_Remote", "DYNO SET VALUE_REMOTE", sysVarInfo.AI_Variables.Count);
            sysVarInfo.AI_Variables.Add(newVI);
            vpValue = new ValuePair(32767, 1);
            newVI.CalibrationTable.Add(vpValue);
            newVI.Unit = "-";

            newVI = new VariableInfo("ENG_SV_RMT", "g_AI_Target_Engine_Remote", "ENG SET VALUE REMOTE", sysVarInfo.AI_Variables.Count);
            sysVarInfo.AI_Variables.Add(newVI);
            vpValue = new ValuePair(32767, 1);
            newVI.CalibrationTable.Add(vpValue);
            newVI.Unit = "-";

            newVI = new VariableInfo("THROTTLE_FB", "g_AI_Feedback_Alpha", "THOTTLE POSITION FEEDBACK", sysVarInfo.AI_Variables.Count);
            sysVarInfo.AI_Variables.Add(newVI);
            vpValue = new ValuePair(32767, 1);
            newVI.CalibrationTable.Add(vpValue);
            newVI.Unit = "%";

            newVI = new VariableInfo("DY_SPEED_FB", "g_AI_Feedback_DynoSpeed", "DYNO SPEED ACTUAL_FEEDBACK", sysVarInfo.AI_Variables.Count);
            sysVarInfo.AI_Variables.Add(newVI);
            vpValue = new ValuePair(100, 1);
            newVI.CalibrationTable.Add(vpValue);
            newVI.Unit = "RPM";

            newVI = new VariableInfo("LC_VDIFF", "g_AI_Feedback_Vdiff", "TORQUE FEEDBACK", sysVarInfo.AI_Variables.Count);
            vpValue = new ValuePair(2147483647, 20);
            newVI.CalibrationTable.Add(vpValue);
            newVI.Unit = "mV";
            sysVarInfo.AI_Variables.Add(newVI);
            newVI = new VariableInfo("LC_VREF", "g_AI_Feedback_Vref", "TORQUE FEEDBACK", sysVarInfo.AI_Variables.Count);
            vpValue = new ValuePair(2147483647, 12);
            newVI.Unit = "V";
            newVI.CalibrationTable.Add(vpValue);
            sysVarInfo.AI_Variables.Add(newVI);
        }

        public static void Init_AO_Variables()
        {
            sysVarInfo.AO_Variables.Add(new VariableInfo("DY_SPEED_ACT", "g_AO_DynoSpeedActualSignal", "DYNO SPEED ACTUAL_OUTPUT", sysVarInfo.AO_Variables.Count));
            sysVarInfo.AO_Variables.Add(new VariableInfo("TORQUE_ACT", "g_AO_DynoTorqueActualSignal", "TORQUE ACTUAL_OUTPUT", sysVarInfo.AO_Variables.Count));
            sysVarInfo.AO_Variables.Add(new VariableInfo("DYNO_CTL", "g_AO_DynoControlSignal", "DYNO CONTROL VALE", sysVarInfo.AO_Variables.Count));
            sysVarInfo.AO_Variables.Add(new VariableInfo("CWTEMP_SV", "g_AO_CWTEMP_SV", "WTC TEMPERATURE SET VALUE", sysVarInfo.AO_Variables.Count));
            sysVarInfo.AO_Variables.Add(new VariableInfo("THROTTLE_ACT", "g_AO_Feedback_Alpha", "THROTTLE POSITION ACTUAL", sysVarInfo.AO_Variables.Count));
            sysVarInfo.AO_Variables.Add(new VariableInfo("THROTTLE_SV", "g_AO_ThrottleControlSignal", "THROTTLE CONTROL VALUE", sysVarInfo.AO_Variables.Count));
            sysVarInfo.AO_Variables.Add(new VariableInfo("THROTTLE_EL1", "g_AO_AlphaASignal", "ELECTRIC THROTTLE A", sysVarInfo.AO_Variables.Count));
            sysVarInfo.AO_Variables.Add(new VariableInfo("THROTTLE_EL2", "g_AO_AlphaBSignal", "ELECTRIC THROTTLE B", sysVarInfo.AO_Variables.Count));
        }

        public static void Init_Calc_Variables()
        {
            VariableInfo newVI = (new VariableInfo("TORQUE_FB", "g_AI_Feedback_Torque", "TORQUE FEEDBACK", sysVarInfo.Calc_Variables.Count));
            ValuePair vpValue = new ValuePair(98, 1);
            newVI.CalibrationTable.Add(vpValue);
            newVI.Unit = "Nm";
            newVI.Formula = "(g_AI_Feedback_Vdiff / g_AI_Feedback_Vref * 967 / 2 - 58.4)";
            newVI.Averaging = true;
            newVI.AveragingCount = 100;
            sysVarInfo.Calc_Variables.Add(newVI);
        }
    }

}
