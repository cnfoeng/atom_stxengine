﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ACS.SPiiPlusNET;
using System.Runtime.InteropServices; //for COMException class
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;

namespace AtomUI.Class
{
    public class Ethercat
    {
        public static int AlarmALLCNT = 0;
        public static internalVariables inVariables = new internalVariables();
        public static Dictionary<int, acsAlarmClass> acsAlarm;
        public static Api Ch = new Api(); //  For communcating between the UMD and the Host Application. All communication commands are methods within the AsyncChannel Class.

        public static bool bConnected = false;
        public static ACSC_CONNECTION_INFO info;
        public static string SerialNumber;

        public static SysInfo sysInfo = new SysInfo();
        public static SysChannelInfo chInfo = new SysChannelInfo();

        public static int[] g_PC_Read_Data = null;
        public static int[] g_SystemState = null;
        public static double[] g_FeedbackValue = null;
        public static double[] g_TargetValue = null;
        
        public static int[] g_eCAT_Output_Data;
        public static int[] g_eCAT_Output_Data_ExistIndex;
        public static int[] g_eCAT_Output_Address;
        
        public static void ExecuteCommand(ProgramBuffer Buf, int LineNum, string CMD)
        {
            Ethercat.Ch.StopBuffer(Buf);
            Ethercat.Ch.ClearBuffer(Buf, LineNum, 9999);
            Ethercat.Ch.AppendBuffer(Buf, CMD);
            Ethercat.Ch.CompileBuffer(Buf);
            Ethercat.Ch.RunBuffer(Buf, null);
        }

        public static acsConnectionInfo FindHardWare(HardWareType HWType)
        {
            foreach (acsConnectionInfo info in Ethercat.sysInfo.HardWareInfo)
            {
                if (info.HWType == HWType)
                {
                    return info;
                }
            }

            return null;
        }

        public static List<string> getHWList()
        {
            List<string> hwList = new List<string>();
            for (int i = 1; i < 10; i++)
            {
                hwList.Add(((HardWareType)i).ToString());
            }
            return hwList;
        }

        public static HardWareType getHWType(string hwTypeString)
        {
            for (int i = 0; i < 9; i++)
            {
                if (((HardWareType)i).ToString() == hwTypeString)
                {
                    return (HardWareType)i;
                }
            }
            return HardWareType.SERVER;
        }

        public static void SaveToFlash()
        {
            ACS.SPiiPlusNET.Axis[] parameters = new ACS.SPiiPlusNET.Axis[2];
            ProgramBuffer[] buffers = new ProgramBuffer[2];
            ServoProcessor[] sPPrograms = new ServoProcessor[2];
            parameters[0] = ACS.SPiiPlusNET.Axis.ACSC_NONE;
            buffers[0] = ProgramBuffer.ACSC_BUFFER_ALL;
            buffers[1] = ProgramBuffer.ACSC_NONE;
            sPPrograms[0] = ServoProcessor.ACSC_NONE;
            Ethercat.Ch.ControllerSaveToFlash(parameters, buffers, sPPrograms, null);

        }

        public static void ClearLists()
        {
            sysInfo.ClearLists();
        }

        public static void DisConnect()
        {
            if (Ch.IsConnected)
                Ch.CloseComm();
        }

        public static acsConnectionInfo FindACS(List<acsConnectionInfo> HardWareInfo)
        {
            foreach (acsConnectionInfo hw in HardWareInfo)
            {
                if (hw.HWType == HardWareType.ACS)
                {
                    return hw;
                }
            }

            return null;
        }

        public static void Connect()
        {
            acsConnectionInfo acs = FindACS(Ethercat.sysInfo.HardWareInfo);

            if (acs != null)
            {
                int BaudRateCmB = acs.Baud_Rate;   //  Baud rate
                int ConnTypeCmB = acs.ConnType;   //  Ethernet connection type
                int ComTypeCmB = acs.ComType;    //  Communication medium (e.g. Serial, Ethernet)
                int CommPortCmB = acs.CommPort;   //  COM channel
                string RemoteAddressTB = acs.RemoteAddress;
                string SlotNumberTB = acs.SlotNumber;
                string BaudRateCmBValue = acs.Baud_RateValue;   //  Baud rate
                //string BaudRateCmBValue = AtomUI.Ethercat.sysInfo.sysSettings.Baud_RateValue;   //  Baud rate

                if (!Ch.IsConnected)
                {
                    if (ComTypeCmB == 0)                  //  Serial
                    {
                        int Baud_Rate;
                        if ((string)BaudRateCmBValue == "Auto")
                            Baud_Rate = -1;
                        else
                            Baud_Rate = Convert.ToInt32(BaudRateCmBValue);

                        // Open serial communuication.
                        // CommPortCmB.SelectedIndex + 1 defines the COM port.
                        Ch.OpenCommSerial(CommPortCmB + 1, Baud_Rate);
                    }
                    else if (ComTypeCmB == 1 && !CommonFunction.bUseDebug)            //  Ethernet
                    {
                        int Protocol;
                        if (ConnTypeCmB == 0)             //  Point to Point
                            Protocol = (int)EthernetCommOption.ACSC_SOCKET_DGRAM_PORT;
                        else
                            Protocol = (int)EthernetCommOption.ACSC_SOCKET_STREAM_PORT;
                        // Open ethernet communuication.
                        // RemoteAddress.Text defines the controller's TCP/IP address.
                        // Protocol is TCP/IP in case of network connection, and UDP in case of point-to-point connection.
                        Ch.OpenCommEthernet(RemoteAddressTB, Protocol);
                    }
                    else if (ComTypeCmB == 2)             //  PCI
                    {
                        //Open PCI Bus communuication
                        int SlotNumber = Convert.ToInt32(SlotNumberTB);
                        Ch.OpenCommPCI(SlotNumber);
                    }
                    else //(CComTypeCmB.SelectedIndex == 3)
                        //Open communuication with Simulator
                        Ch.OpenCommSimulator();

                    info = Ch.GetConnectionInfo();
                    SerialNumber = Ethercat.Ch.GetSerialNumber();
                    bConnected = true;
                }
            }
        }

        public static void makeSystemInfo(SysInfo sysInfo, SysChannelInfo chInfo)
        {
            int EK1100 = 0;
            String Slaves = Ch.Transaction("?ECGETSLAVES()");
            int SLAVES = 0;
            int.TryParse(Slaves, out SLAVES);
            List<acsModuleInfo> Lists = new List<acsModuleInfo>();

            //if (sysInfo.HardWareInfo)
            acsConnectionInfo acs = FindACS(Ethercat.sysInfo.HardWareInfo);

            if (acs.HMIExist == false)
            {
                EK1100 = 1;
            }

            for (int i = 0; i < SLAVES; i++)
            {
                acsModuleInfo NewModule = Ethercat.GetModule((int)(Ch.GetEtherCATSlaveVendorID(i)), (int)(Ch.GetEtherCATSlaveProductID(i)), i);
                Lists.Add(NewModule);
                if (NewModule.ProductName == "EK1100")
                {
                    EK1100 = EK1100 + 1;
                    if (EK1100 < 2)
                    {
                        NewModule.UseFor = ChannelUseFor.HMI;
                        sysInfo.HMIModuleInfos.Add(NewModule);
                    } else
                    {
                        NewModule.UseFor = ChannelUseFor.DAQ;
                        sysInfo.ModuleInfos.Add(NewModule);
                    }
                }
                else
                {
                    List<acsChannelInfo> CHList = null;
                    if (EK1100 < 2)
                    {
                        NewModule.UseFor = ChannelUseFor.HMI;
                        sysInfo.HMIModuleInfos.Add(NewModule);
                        CHList = chInfo.HMI_Channels;
                    }
                    else
                    {
                        NewModule.UseFor = ChannelUseFor.DAQ;
                        sysInfo.ModuleInfos.Add(NewModule);
                        switch (NewModule.ChannelIO)
                        {
                            case ChannelIO.Input:
                                switch (NewModule.ChannelType)
                                {
                                    case ChannelType.Digital:
                                        CHList = chInfo.DI_Channels;
                                        break;
                                    case ChannelType.Analog:
                                        CHList = chInfo.AI_Channels;
                                        break;
                                }
                                break;
                            case ChannelIO.Output:
                                switch (NewModule.ChannelType)
                                {
                                    case ChannelType.Digital:
                                        CHList = chInfo.DO_Channels;
                                        break;
                                    case ChannelType.Analog:
                                        CHList = chInfo.AO_Channels;
                                        break;
                                }
                                break;
                        }
                    }
                    if (CHList != null)
                        GetChannels(ref CHList, NewModule);
                }
            }
        }

        public static void SaveSysInfo()
        {
            try
            {
                sysInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Class.Ethercat.sysInfo.sysSettings.SysInfoDir + "\\" + Class.Ethercat.sysInfo.sysSettings.SysInfoFileName);
                chInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Class.Ethercat.sysInfo.sysSettings.ChannelInfoDir + "\\" + Class.Ethercat.sysInfo.sysSettings.ChannelInfoFileName);
            }
            catch (Exception ex)
            {
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n AddMexaChannels Error!!");
            }
        }

        private static void defaultDirectoryCheck()
        {
            if (Ethercat.sysInfo.sysSettings.DefaultDirectory == "" || !Directory.Exists(Ethercat.sysInfo.sysSettings.DefaultDirectory))
            {
                Ethercat.sysInfo.sysSettings.DefaultDirectory = Application.StartupPath + "\\";
            }
            if (Ethercat.sysInfo.sysSettings.DataLoggingDir.IndexOf("\\") < 0 || !Directory.Exists(Ethercat.sysInfo.sysSettings.DataLoggingDir))

            {
                Ethercat.sysInfo.sysSettings.DataLoggingDir = Application.StartupPath + "\\Data";
            }
        }

        public static void LoadSysInfo()
        {
            try
            {
                defaultDirectoryCheck();
                sysInfo.Load(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Class.Ethercat.sysInfo.sysSettings.SysInfoDir + "\\" + Class.Ethercat.sysInfo.sysSettings.SysInfoFileName);
                defaultDirectoryCheck();
                chInfo.Load(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Class.Ethercat.sysInfo.sysSettings.ChannelInfoDir + "\\" + Class.Ethercat.sysInfo.sysSettings.ChannelInfoFileName);
            } catch(Exception ex)
            {
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n AddMexaChannels Error!!");
            }
}
        static int BaseOffset = 0;

        public static void GetChannels(ref List<acsChannelInfo> CHList, acsModuleInfo NewModule)
        {
            List<int> Offsets = new List<int>();
            foreach (string Variable in NewModule.Variables)
            {
                double offset = Ch.GetEtherCATSlaveOffset(Variable, NewModule.SlaveNo);
                Offsets.Add((int)offset);
            }

            for (int ind = 0; ind < NewModule.ChannelEa; ind++)
            {
                if (NewModule.ChannelType == ChannelType.Analog && NewModule.ChannelIO == ChannelIO.Input)
                {
                    for (int varInd = 0; varInd < Offsets.Count; varInd++)
                    {
                        acsChannelInfo ChInfo = new acsChannelInfo(NewModule, 0, CHList.Count, NewModule.Variables[varInd], BaseOffset);
                        CHList.Add(ChInfo);
                    }
                    //BaseOffset += NewModule.OffsetBase;
                    BaseOffset++;
                }
                else if (NewModule.ChannelType == ChannelType.Analog && NewModule.ChannelIO == ChannelIO.Output)
                {
                    for (int varInd = 0; varInd < Offsets.Count; varInd++)
                    {
                        acsChannelInfo ChInfo = new acsChannelInfo(NewModule, 0, CHList.Count, NewModule.Variables[varInd], BaseOffset);
                        CHList.Add(ChInfo);
                    }
                    //BaseOffset += NewModule.OffsetBase;
                    BaseOffset++;
                }
                else if (NewModule.ChannelType == ChannelType.Digital && NewModule.ChannelIO == ChannelIO.Input)
                {
                    acsChannelInfo ChInfo = new acsChannelInfo(NewModule, ind % 8, CHList.Count, NewModule.Variables[0], BaseOffset);
                    CHList.Add(ChInfo);
                    if ((ind % 8) == 7)
                    {
                        BaseOffset++;
                    }
                }
                else if (NewModule.ChannelType == ChannelType.Digital && NewModule.ChannelIO == ChannelIO.Output)
                {
                    acsChannelInfo ChInfo = new acsChannelInfo(NewModule, ind % 8, CHList.Count, NewModule.Variables[0], BaseOffset);
                    CHList.Add(ChInfo);
                    if ((ind % 8) == 7)
                    {
                        BaseOffset++;
                    }
                }
            }
            BaseOffset = 0;
        }

        public static acsModuleInfo GetModule(int VendorID, int ProductID, int SlaveNo)
        {
            acsModuleInfo CH_Info;
            string ProductName = "";
            string ProductDesc = "";
            ChannelIO channelIO = ChannelIO.None;
            ChannelType channelType = ChannelType.None;
            int OffsetBase = 0;
            int ChannelNo = 0;
            List<string> Variables = new List<string>();

            switch (VendorID)
            {
                // BeckHoff Module
                case 0x02:
                    switch (ProductID)
                    {
                        case 0x44C2C52:
                            ProductName = "EK1100";
                            ProductDesc = "EK1100 EtherCAT Coupler";
                            channelIO = ChannelIO.None;
                            channelType = ChannelType.None;
                            OffsetBase = 0;
                            ChannelNo = 0;
                            break;
                        case 0x03F03052:
                            ProductName = "EL1008";
                            ProductDesc = "EL1008 8CH. Digital Input 24V, 3ms";
                            channelIO = ChannelIO.Input;
                            channelType = ChannelType.Digital;
                            OffsetBase = 1;
                            ChannelNo = 8;
                            Variables.Add("Input");
                            break;
                        case 0x07113052:
                            ProductName = "EL1809";
                            ProductDesc = "EL1809 16CH. Digital Input 24V, 3ms";
                            channelIO = ChannelIO.Input;
                            channelType = ChannelType.Digital;
                            OffsetBase = 1;
                            ChannelNo = 16;
                            Variables.Add("Input");
                            break;
                        case 0x0BBC3052:
                            ProductName = "EL3004";
                            ProductDesc = "EL3004 4CH. Analog Input +/- 10V";
                            channelIO = ChannelIO.Input;
                            channelType = ChannelType.Analog;
                            OffsetBase = 4;
                            ChannelNo = 4;
                            Variables.Add("Value");
                            break;
                        case 0x0C203052:
                            ProductName = "EL3104";
                            ProductDesc = "EL3104 4CH. Analog Input +/- 10V Diff";
                            channelIO = ChannelIO.Input;
                            channelType = ChannelType.Analog;
                            OffsetBase = 4;
                            ChannelNo = 4;
                            Variables.Add("Value");
                            break;
                        case 0x0BF83052:
                            ProductName = "EL3064";
                            ProductDesc = "EL3064 4CH.Analog Input +/ -10V * **0x0BF83052    Revision: 0x00130000";
                            channelIO = ChannelIO.Input;
                            channelType = ChannelType.Analog;
                            OffsetBase = 4;
                            ChannelNo = 4;
                            Variables.Add("Value");
                            break;
                        case 0x0BC03052:
                            ProductName = "EL3008";
                            ProductDesc = "EL3008 8CH. Analog Input +/- 10V sin";
                            channelIO = ChannelIO.Input;
                            channelType = ChannelType.Analog;
                            OffsetBase = 4;
                            ChannelNo = 8;
                            Variables.Add("Value");
                            break;
                        case 0x0D173052:
                            ProductName = "EL3351";
                            ProductDesc = "EL3351 1Ch. Ana. Input Resistor Bridge Terminal, Diff. 16bit";
                            channelIO = ChannelIO.Input;
                            channelType = ChannelType.Analog;
                            OffsetBase = 6;
                            ChannelNo = 2;
                            Variables.Add("Value");
                            break;
                        case 0x0C343052:
                            ProductName = "EL3124";
                            ProductDesc = "EL3124 4Ch. Ana. Input 4-20mA Diff";
                            channelIO = ChannelIO.Input;
                            channelType = ChannelType.Analog;
                            OffsetBase = 4;
                            ChannelNo = 4;
                            Variables.Add("Value");
                            break;
                        case 0x0BF23052:
                            ProductName = "EL3508";
                            ProductDesc = "EL3508 4Ch. Ana. Input 4-20mA sin";
                            channelIO = ChannelIO.Input;
                            channelType = ChannelType.Analog;
                            OffsetBase = 4;
                            ChannelNo = 4;
                            Variables.Add("Value");
                            break;
                        case 0x0C883052:
                            ProductName = "EL3208";
                            ProductDesc = "EL3208 8Ch. Ana. Input PT100(RTD)";
                            channelIO = ChannelIO.Input;
                            channelType = ChannelType.Analog;
                            OffsetBase = 4;
                            ChannelNo = 8;
                            Variables.Add("Value");
                            break;
                        case 0x0CF63052:
                            ProductName = "EL3318";
                            ProductDesc = "EL3318 8Ch. Ana. Thermocouple(TC)";
                            channelIO = ChannelIO.Input;
                            channelType = ChannelType.Analog;
                            OffsetBase = 4;
                            ChannelNo = 8;
                            Variables.Add("Value");
                            break;
                        case 0x14203052:
                            ProductName = "EL5152";
                            ProductDesc = "EL5152 2CH. Position Measurement";
                            channelIO = ChannelIO.Input;
                            channelType = ChannelType.Analog;
                            OffsetBase = 10;
                            ChannelNo = 2;
                            Variables.Add("Counter value");
                            Variables.Add("Period value");
                            break;
                        case 0x141F3052:
                            ProductName = "EL5151";
                            ProductDesc = "EL5151 1CH. Position Measurement";
                            channelIO = ChannelIO.Input;
                            channelType = ChannelType.Analog;
                            OffsetBase = 2;
                            ChannelNo = 2;
                            Variables.Add("Frequency value");
                            Variables.Add("Period value");
                            break;
                        case 0x13ED3052:
                            ProductName = "EL5101";
                            ProductDesc = "EL5101 Incremental Encoder Interface";
                            channelIO = ChannelIO.Input;
                            channelType = ChannelType.Analog;
                            OffsetBase = 2;
                            ChannelNo = 1; //Frequency value, Period value
                            Variables.Add("Frequency");
                            break;
                        case 0x07D83052:
                            ProductName = "EL2008";
                            ProductDesc = "EL2008 8CH. Digital Output 24V, 0.5A";
                            channelIO = ChannelIO.Output;
                            channelType = ChannelType.Digital;
                            OffsetBase = 1;
                            ChannelNo = 8;
                            Variables.Add("Output");
                            break;
                        case 0x0AF93052:
                            ProductName = "EL2809";
                            ProductDesc = "EL2809 16CH. Digital Output 24V, 0.5A";
                            channelIO = ChannelIO.Output;
                            channelType = ChannelType.Digital;
                            OffsetBase = 1;
                            ChannelNo = 16;
                            Variables.Add("Output");
                            break;
                        case 0x0FA43052:
                            ProductName = "EL4004";
                            ProductDesc = "EL4004 4CH. Analog Output +/- 10V 12bit *** 0x0FA43052    Revision:   0x00140000";
                            channelIO = ChannelIO.Output;
                            channelType = ChannelType.Analog;
                            OffsetBase = 2;
                            ChannelNo = 4;
                            Variables.Add("Analog output");
                            break;
                        case 0x0FC23052:
                            ProductName = "EL4034";
                            ProductDesc = "EL4034 4CH. Analog Output +/- 10V 12bit";
                            channelIO = ChannelIO.Output;
                            channelType = ChannelType.Analog;
                            OffsetBase = 2;
                            ChannelNo = 4;
                            Variables.Add("Analog output");
                            break;
                        case 0x10263052:
                            ProductName = "EL4134";
                            ProductDesc = "EL4134 4CH. Analog Output +/- 10V 16bit";
                            channelIO = ChannelIO.Output;
                            channelType = ChannelType.Analog;
                            OffsetBase = 2;
                            ChannelNo = 4;
                            Variables.Add("Analog output");
                            break;
                        case 0x0FC63052:
                            ProductName = "EL4138";
                            ProductDesc = "EL4138 8CH. Analog Output +/- 10V 12bit";
                            channelIO = ChannelIO.Output;
                            channelType = ChannelType.Analog;
                            OffsetBase = 2;
                            ChannelNo = 8;
                            Variables.Add("Analog output");
                            break;
                        case 0x17723052:
                            ProductName = "EL6002";
                            ProductDesc = "EL6002 Interface 2Ch. (RS232)";
                            channelIO = ChannelIO.Input;
                            channelType = ChannelType.Analog;
                            OffsetBase = 24;
                            ChannelNo = 2;
                            Variables.Add("Status");
                            Variables.Add("Data In 0");
                            Variables.Add("Ctrl");
                            Variables.Add("Data Out 0");
                            break;
                        default:
                            ProductName = "UNKNOWN";
                            ProductDesc = "UNKNOWN";
                            channelIO = ChannelIO.None;
                            channelType = ChannelType.None;
                            OffsetBase = 0;
                            ChannelNo = 0;
                            break;
                    }
                    break;
            }
            CH_Info = new acsModuleInfo(ProductName, ProductDesc, VendorID, ProductID, channelIO, channelType, OffsetBase, SlaveNo, ChannelNo);
            CH_Info.Variables = Variables;
            return CH_Info;
        }

        #region ALARMandSTRING
        public static void AlarmDefine()
        {
            acsAlarm = new Dictionary<int, acsAlarmClass>();
            acsAlarm.Add(1, new acsAlarmClass(0, 1, "enum_aID_Buf0_W_SysInitProgram_Excution_Fault"));
            acsAlarm.Add(-1, new acsAlarmClass(0, -1, "enum_aID_Buf0_E_SysInitProgram_Restart_Fault"));
            acsAlarm.Add(300, new acsAlarmClass(0, 300, "enum_aID_Buf0_W_MoCon_H0InteProgram_Excution_Fault"));
            acsAlarm.Add(-300, new acsAlarmClass(0, -300, "enum_aID_Buf0_E_MoCon_H0InteProgram_Restart_Fault"));
            acsAlarm.Add(400, new acsAlarmClass(0, 400, "enum_aID_Buf0_W_Dyno_Engine_PID_Controller_ManagementProgram_Excution_Fault"));
            acsAlarm.Add(-400, new acsAlarmClass(0, -400, "enum_aID_Buf0_E_Dyno_Engine_PID_Controller_ManagementProgram_Restart_Fault"));
            acsAlarm.Add(500, new acsAlarmClass(0, 500, "enum_aID_Buf0_W_DAQ_Program_Excution_Fault"));
            acsAlarm.Add(-500, new acsAlarmClass(0, -500, "enum_aID_Buf0_E_DAQ_Program_Restart_Fault"));
            acsAlarm.Add(600, new acsAlarmClass(0, 600, "enum_aID_Buf0_W_HMIInterface_Program_Excution_Fault"));
            acsAlarm.Add(-600, new acsAlarmClass(0, -600, "enum_aID_Buf0_E_HMIInterface_Program_Restart_Fault"));
            acsAlarm.Add(700, new acsAlarmClass(0, 700, "enum_aID_Buf0_W_MesurementInterface_Program_Excution_Fault"));
            acsAlarm.Add(-700, new acsAlarmClass(0, -700, "enum_aID_Buf0_E_MesurementInterface_Program_Restart_Fault"));
            acsAlarm.Add(-10001, new acsAlarmClass(1, -10001, "enum_aID_Buf1_E_SysInitProgram_IoMapping_Fault"));
            acsAlarm.Add(-10101, new acsAlarmClass(1, -10101, "enum_aID_Buf1_E_CalibrationDataInit_FbSpeedReferanceInit_Fail"));
            acsAlarm.Add(-10102, new acsAlarmClass(1, -10102, "enum_aID_Buf1_E_CalibrationDataInit_FbSpeedAppliedInit_Fail"));
            acsAlarm.Add(-10103, new acsAlarmClass(1, -10103, "enum_aID_Buf1_E_CalibrationDataInit_FbTorqueReferanceInit_Fail"));
            acsAlarm.Add(-10104, new acsAlarmClass(1, -10104, "enum_aID_Buf1_E_CalibrationDataInit_FbTorqueAppliedInit_Fail"));
            acsAlarm.Add(-10105, new acsAlarmClass(1, -10105, "enum_aID_Buf1_E_CalibrationDataInit_FbCurrentReferanceInit_Fail"));
            acsAlarm.Add(-10106, new acsAlarmClass(1, -10106, "enum_aID_Buf1_E_CalibrationDataInit_FbCurrentAppliedInit_Fail"));
            acsAlarm.Add(-10107, new acsAlarmClass(1, -10107, "enum_aID_Buf1_E_CalibrationDataInit_OpCurrentReferanceInit_Fail"));
            acsAlarm.Add(-10108, new acsAlarmClass(1, -10108, "enum_aID_Buf1_E_CalibrationDataInit_OpCurrentAppliedInit_Fail"));
            acsAlarm.Add(-20001, new acsAlarmClass(2, -20001, "enum_aID_Buf2_E_Dyno_AMP_Fault"));
            acsAlarm.Add(-20002, new acsAlarmClass(2, -20002, "enum_aID_Buf2_E_Dyno_OilPump_Level_Fault"));
            acsAlarm.Add(-20003, new acsAlarmClass(2, -20003, "enum_aID_Buf2_E_Dyno_Water_Temp_Fault"));
            acsAlarm.Add(-20004, new acsAlarmClass(2, -20004, "enum_aID_Buf2_E_Dyno_CasingRing_Fault"));
            acsAlarm.Add(30001, new acsAlarmClass(3, 30001, "enum_aID_Buf3_W_SysCMD_Reset_ReqOn"));
            acsAlarm.Add(30002, new acsAlarmClass(3, 30002, "enum_aID_Buf3_W_SysCMD_Reset_ReqOff"));
            acsAlarm.Add(-30001, new acsAlarmClass(3, -30001, "enum_aID_Buf3_E_SysCMD_Dyno_On_Fail"));
            acsAlarm.Add(-30002, new acsAlarmClass(3, -30002, "enum_aID_Buf3_E_SysCMD_Dyno_Off_Fail"));
            acsAlarm.Add(-30003, new acsAlarmClass(3, -30003, "enum_aID_Buf3_W_SysCMD_EMS_ReqOn"));
            acsAlarm.Add(30003, new acsAlarmClass(3, 30003, "enum_aID_Buf3_W_SysCMD_EMS_ReqOff"));
            acsAlarm.Add(30004, new acsAlarmClass(3, -30004, "enum_aID_Buf3_W_SysCMD_DynaFualt_ReqOn"));
            acsAlarm.Add(-30011, new acsAlarmClass(3, -30011, "enum_aID_Buf3_E_SysCMD_IG_On_Fail"));
            acsAlarm.Add(-30012, new acsAlarmClass(3, -30012, "enum_aID_Buf3_E_SysCMD_IG_Off_Fail"));
            acsAlarm.Add(-30021, new acsAlarmClass(3, -30021, "enum_aID_Buf3_E_SysCMD_Engine_PreheatOn_Fail"));
            acsAlarm.Add(-30022, new acsAlarmClass(3, -30022, "enum_aID_Buf3_E_SysCMD_Engine_PreheatOff_Fail"));
            acsAlarm.Add(-30023, new acsAlarmClass(3, -30023, "enum_aID_Buf3_E_SysCMD_Engine_PreheatTOutOff_Fail"));
            acsAlarm.Add(-30031, new acsAlarmClass(3, -30031, "enum_aID_Buf3_E_SysCMD_Engine_StartCmdOn_Fail"));
            acsAlarm.Add(-30032, new acsAlarmClass(3, -30032, "enum_aID_Buf3_W_SysCMD_Engine_StartStopSameTimeSwOn"));
            acsAlarm.Add(-30033, new acsAlarmClass(3, -30033, "enum_aID_Buf3_E_SysCMD_Engine_StartCmdTOutOff_Fail"));
            acsAlarm.Add(-30034, new acsAlarmClass(3, -30034, "enum_aID_Buf3_E_SysCMD_Engine_StartCmdTSReachedOff_Fail"));
            acsAlarm.Add(-30041, new acsAlarmClass(3, -30041, "enum_aID_Buf3_E_SysCMD_Engine_StopCmdOff_Fail"));
            acsAlarm.Add(32001, new acsAlarmClass(3, 32001, "enum_aID_Buf3_W_DrMn_ChgReq_ManualModeCheck_Fail"));
            acsAlarm.Add(32002, new acsAlarmClass(3, 32002, "enum_aID_Buf3_W_DrMn_IdleChgReq_EngStartCheck_Fail"));
            acsAlarm.Add(32003, new acsAlarmClass(3, 32003, "enum_aID_Buf3_W_DrMn_IdleExceptChgReq_EngStartCheck_Fail"));
            acsAlarm.Add(32004, new acsAlarmClass(3, 32004, "enum_aID_Buf3_W_DrMn_IdleExceptChgReq_DynoOnCheck_Fail"));
            acsAlarm.Add(32005, new acsAlarmClass(3, 32005, "enum_aID_Buf3_W_DrMn_IdleExceptChgReq_IdleMdCheck_Fail"));
            acsAlarm.Add(32006, new acsAlarmClass(3, 32006, "enum_aID_Buf3_W_DrMn_ChgReq_AutomaticModeCheck_Fail"));
            acsAlarm.Add(32011, new acsAlarmClass(3, 32011, "enum_aID_Buf3_W_DrMn_ChgReq_InterlockCheck_Fail"));
            acsAlarm.Add(33001, new acsAlarmClass(3, 33001, "enum_aID_Buf3_W_AuSoftW_ChgReq_SoftWare_Fault"));
            acsAlarm.Add(33002, new acsAlarmClass(3, 33002, "enum_aID_Buf3_W_AuSoftW_ChgReq_SoftWare_NotIdle"));
            acsAlarm.Add(33003, new acsAlarmClass(3, 33003, "enum_aID_Buf3_W_AuSoftW_ChgReq_SoftWare_NotOn"));
            acsAlarm.Add(33004, new acsAlarmClass(3, 33004, "enum_aID_Buf3_W_AuSoftW_TqApUsingProfile_WriteCompleteCheckFail"));
            acsAlarm.Add(33005, new acsAlarmClass(3, 33005, "enum_aID_Buf3_W_AuSoftW_SpApUsingProfile_WriteCompleteCheckFail"));
            acsAlarm.Add(33006, new acsAlarmClass(3, 33006, "enum_aID_Buf3_W_AuSoftW_TqSpUsingProfile_WriteCompleteCheckFail"));
            acsAlarm.Add(33007, new acsAlarmClass(3, 33007, "enum_aID_Buf3_W_AuSoftW_SpTqUsingProfile_WriteCompleteCheckFail"));
            acsAlarm.Add(33008, new acsAlarmClass(3, 33008, "enum_aID_Buf3_W_AuSoftW_UsingProfile_Fail_AutoMd_ProfilePlay_On"));
            acsAlarm.Add(33009, new acsAlarmClass(3, 33009, "enum_aID_Buf3_W_AuSoftW_TqApProfile_StepCountCheckFail"));
            acsAlarm.Add(33010, new acsAlarmClass(3, 33010, "enum_aID_Buf3_W_AuSoftW_SpApProfile_StepCountCheckFail"));
            acsAlarm.Add(33011, new acsAlarmClass(3, 33011, "enum_aID_Buf3_W_AuSoftW_TqSpProfile_StepCountCheckFail"));
            acsAlarm.Add(33012, new acsAlarmClass(3, 33012, "enum_aID_Buf3_W_AuSoftW_SpTqProfile_StepCountCheckFail"));
            acsAlarm.Add(33013, new acsAlarmClass(3, 33013, "enum_aID_Buf3_W_Automatic_ControlOnOffBit_Fail"));
            acsAlarm.Add(33014, new acsAlarmClass(3, 33014, "enum_aID_Buf3_W_Automatic_CtrlOn_AutomaticModeCheckFail"));
            acsAlarm.Add(33015, new acsAlarmClass(3, 33015, "enum_aID_Buf3_W_DrMn_IdleMdCheck_SpeedCheck_Fail"));
            acsAlarm.Add(33016, new acsAlarmClass(3, 33016, "enum_aID_Buf3_W_DrMn_IdleMdCheck_TorqueCheck_Fail"));
            acsAlarm.Add(33017, new acsAlarmClass(3, 33017, "enum_aID_Buf3_W_DrMn_IdleMdCheck_AlphaCheck_Fail"));
            acsAlarm.Add(33018, new acsAlarmClass(3, 33018, "enum_aID_Buf3_W_DynoService_IdleAndDefaultMdCheck_Fail"));
            acsAlarm.Add(34001, new acsAlarmClass(3, 34001, "enum_aID_Buf3_W_MeasurEqp_bbm_ResetFail_OtherRequestOn"));
            acsAlarm.Add(34002, new acsAlarmClass(3, 34002, "enum_aID_Buf3_W_MeasurEqp_bbm_MeasurStartFail_OtherRequestOn"));
            acsAlarm.Add(34003, new acsAlarmClass(3, 34003, "enum_aID_Buf3_W_MeasurEqp_bbm_MeasurStopFail_OtherRequestOn"));
            acsAlarm.Add(34011, new acsAlarmClass(3, 34011, "enum_aID_Buf3_W_MeasurEqp_smkm_ResetFail_OtherRequestOn"));
            acsAlarm.Add(34012, new acsAlarmClass(3, 34012, "enum_aID_Buf3_W_MeasurEqp_smkm_MeasurStartFail_OtherRequestOn"));
            acsAlarm.Add(34013, new acsAlarmClass(3, 34013, "enum_aID_Buf3_W_MeasurEqp_smkm_MeasurStopFail_OtherRequestOn"));
            acsAlarm.Add(34021, new acsAlarmClass(3, 34021, "enum_aID_Buf3_W_MeasurEqp_FuelMeter_ResetFail_OtherRequestOn"));
            acsAlarm.Add(34022, new acsAlarmClass(3, 34022, "enum_aID_Buf3_W_MeasurEqp_FuelMeter_MeasurStartFail_OtherRequestOn"));
            acsAlarm.Add(34023, new acsAlarmClass(3, 34023, "enum_aID_Buf3_W_MeasurEqp_FuelMeter_MeasurStopFail_OtherRequestOn"));
            acsAlarm.Add(-40001, new acsAlarmClass(4, -40001, "enum_aID_Buf4_E_IdleMode_EngDynoPID_NotCompiles"));
            acsAlarm.Add(-40002, new acsAlarmClass(4, -40002, "enum_aID_Buf4_E_IdleMode_DynoPIDStopCMD_Fail"));
            acsAlarm.Add(-40003, new acsAlarmClass(4, -40003, "enum_aID_Buf4_E_IdleMode_EnginePIDStartCMD_Fail"));
            acsAlarm.Add(-40011, new acsAlarmClass(4, -40011, "enum_aID_Buf4_E_ExceptIdleMode_EngDynoPID_NotCompiles"));
            acsAlarm.Add(-40012, new acsAlarmClass(4, -40012, "enum_aID_Buf4_E_ExceptIdleMode_DynoPIDStartCMD_Fail"));
            acsAlarm.Add(-40013, new acsAlarmClass(4, -40013, "enum_aID_Buf4_E_ExceptIdleMode_EnginePIDStartCMD_Fail"));
            acsAlarm.Add(-40021, new acsAlarmClass(4, -40021, "enum_aID_Buf4_E_OpStopCmd_EngPID_StopFail"));
            acsAlarm.Add(-40022, new acsAlarmClass(4, -40022, "enum_aID_Buf4_E_OpStopCmd_DynoPID_StopFail"));
            acsAlarm.Add(-40031, new acsAlarmClass(4, -40031, "enum_aID_Buf4_E_EngineStart_DynoPIDStartCMD_Fail"));
            acsAlarm.Add(-40032, new acsAlarmClass(4, -40032, "enum_aID_Buf4_E_EngineStart_DynoPID_NotCompiles"));
            acsAlarm.Add(-50001, new acsAlarmClass(5, -50001, "enum_aID_Buf5_W_PIDOperable_Fault"));
            acsAlarm.Add(-51001, new acsAlarmClass(5, -51001, "enum_aID_Buf5_W_PID_cLimit_Fault"));
            acsAlarm.Add(-51002, new acsAlarmClass(5, -51002, "enum_aID_Buf0_E_MoCon_H0I"));
            acsAlarm.Add(-60001, new acsAlarmClass(6, -60001, "enum_aID_Buf6_W_PIDOperable_Fault"));
            acsAlarm.Add(-61001, new acsAlarmClass(6, -61001, "enum_aID_Buf6_W_PID_cLimit_Fault"));
            acsAlarm.Add(-61002, new acsAlarmClass(6, -61002, "enum_aID_Buf6_W_PID_yLimit_Fault"));
            acsAlarm.Add(90001, new acsAlarmClass(9, 90001, "enum_aID_Buf9_W_RS232_BlowByMeter_PortInit_Fail"));
            acsAlarm.Add(90002, new acsAlarmClass(9, 90002, "enum_aID_Buf9_W_RS232_FuelMeter_PortInit_Fail"));
            acsAlarm.Add(90003, new acsAlarmClass(9, 90003, "enum_aID_Buf9_W_RS232_SmokeMeter_PortInit_Fail"));
            acsAlarm.Add(90004, new acsAlarmClass(9, 90004, "enum_aID_Buf9_W_RS232_PortInit_Fail"));
            acsAlarm.Add(90011, new acsAlarmClass(9, 90011, "enum_aID_Buf9_W_BlowByMeter_Transmit_Fail"));
            acsAlarm.Add(90012, new acsAlarmClass(9, 90012, "enum_aID_Buf9_W_BlowByMeter_ReceiveHeader_Notconsistent"));
            acsAlarm.Add(90013, new acsAlarmClass(9, 90013, "enum_aID_Buf9_W_BlowByMeter_ReceiveIndexOutofRange"));
            acsAlarm.Add(90100, new acsAlarmClass(9, 90100, "enum_aID_Buf9_W_BlowByMeter_SRES_Receive_NAK"));
            acsAlarm.Add(90200, new acsAlarmClass(9, 90200, "enum_aID_Buf9_W_BlowByMeter_STBY_Receive_NAK"));
            acsAlarm.Add(90300, new acsAlarmClass(9, 90300, "enum_aID_Buf9_W_BlowByMeter_SINT_Receive_NAK"));
            acsAlarm.Add(90400, new acsAlarmClass(9, 90400, "enum_aID_Buf9_W_BlowByMeter_ASTZ_Receive_NAK"));
            acsAlarm.Add(90500, new acsAlarmClass(9, 90500, "enum_aID_Buf9_W_BlowByMeter_ASTF_Receive_NAK"));
            acsAlarm.Add(90600, new acsAlarmClass(9, 90600, "enum_aID_Buf9_W_BlowByMeter_AMES_Receive_NAK"));
            acsAlarm.Add(90700, new acsAlarmClass(9, 90700, "enum_aID_Buf9_W_BlowByMeter_AIMW_Receive_NAK"));
            acsAlarm.Add(90020, new acsAlarmClass(9, 90020, "enum_aID_Buf9_W_BlowByMeter_MesureReqFail_NotInit"));
            acsAlarm.Add(90021, new acsAlarmClass(9, 90021, "enum_aID_Buf9_W_BlowByMeter_SRES_Ans_TimeOut"));
            acsAlarm.Add(90022, new acsAlarmClass(9, 90022, "enum_aID_Buf9_W_BlowByMeter_STBY_Ans_TimeOut"));
            acsAlarm.Add(90023, new acsAlarmClass(9, 90023, "enum_aID_Buf9_W_BlowByMeter_SINT_Ans_TimeOut"));
            acsAlarm.Add(90024, new acsAlarmClass(9, 90024, "enum_aID_Buf9_W_BlowByMeter_ASTZ_Ans_TimeOut"));
            acsAlarm.Add(90025, new acsAlarmClass(9, 90025, "enum_aID_Buf9_W_BlowByMeter_ASTF_Ans_TimeOut"));
            acsAlarm.Add(90026, new acsAlarmClass(9, 90026, "enum_aID_Buf9_W_BlowByMeter_AMES_Ans_TimeOut"));
            acsAlarm.Add(90027, new acsAlarmClass(9, 90027, "enum_aID_Buf9_W_BlowByMeter_AIMW_Ans_TimeOut"));
            acsAlarm.Add(92011, new acsAlarmClass(9, 92011, "enum_aID_Buf9_W_SmokeMeter_Transmit_Fail"));
            acsAlarm.Add(92012, new acsAlarmClass(9, 92012, "enum_aID_Buf9_W_SmokeMeter_ReceiveHeader_Notconsistent"));
            acsAlarm.Add(92013, new acsAlarmClass(9, 92013, "enum_aID_Buf9_W_SmokeMeter_ReceiveIndexOutofRange"));
            acsAlarm.Add(92100, new acsAlarmClass(9, 92100, "enum_aID_Buf9_W_SmokeMeter_SRES_Receive_NAK"));
            acsAlarm.Add(92200, new acsAlarmClass(9, 92200, "enum_aID_Buf9_W_SmokeMeter_SREM_Receive_NAK"));
            acsAlarm.Add(92290, new acsAlarmClass(9, 92290, "enum_aID_Buf9_W_SmokeMeter_SwitchRemoteMode_ASTZ_Receive_NAK"));
            acsAlarm.Add(92300, new acsAlarmClass(9, 92300, "enum_aID_Buf9_W_SmokeMeter_EMZY_Receive_NAK"));
            acsAlarm.Add(92400, new acsAlarmClass(9, 92400, "enum_aID_Buf9_W_SmokeMeter_ASTZ_Receive_NAK"));
            acsAlarm.Add(92500, new acsAlarmClass(9, 92500, "enum_aID_Buf9_W_SmokeMeter_ASTF_Receive_NAK"));
            acsAlarm.Add(92600, new acsAlarmClass(9, 92600, "enum_aID_Buf9_W_SmokeMeter_AMES_Receive_NAK"));
            acsAlarm.Add(92700, new acsAlarmClass(9, 92700, "enum_aID_Buf9_W_SmokeMeter_SASB_Receive_NAK"));
            acsAlarm.Add(92790, new acsAlarmClass(9, 92790, "enum_aID_Buf9_W_SmokeMeter_SetReadySampling_ASTZ_Receive_NAK"));
            acsAlarm.Add(92800, new acsAlarmClass(9, 92800, "enum_aID_Buf9_W_SmokeMeter_AFSN_Receive_NAK"));
            acsAlarm.Add(92020, new acsAlarmClass(9, 92020, "enum_aID_Buf9_W_SmokeMeter_MesureReqFail_NotInit"));
            acsAlarm.Add(92021, new acsAlarmClass(9, 92021, "enum_aID_Buf9_W_SmokeMeter_SMES_Receive_NAK"));
            acsAlarm.Add(92022, new acsAlarmClass(9, 92022, "enum_aID_Buf9_W_SmokeMeter_StartSampling_ASTZ_Receive_NAK"));
            acsAlarm.Add(92023, new acsAlarmClass(9, 92023, "enum_aID_Buf9_W_SmokeMeter_SRES_Ans_TimeOut"));
            acsAlarm.Add(92024, new acsAlarmClass(9, 92024, "enum_aID_Buf9_W_SmokeMeter_SREM_Ans_TimeOut"));
            acsAlarm.Add(92025, new acsAlarmClass(9, 92025, "enum_aID_Buf9_W_SmokeMeter_SREM_ASTZAns_TimeOut"));
            acsAlarm.Add(92026, new acsAlarmClass(9, 92026, "enum_aID_Buf9_W_SmokeMeter_EMZY_Ans_TimeOut"));
            acsAlarm.Add(92027, new acsAlarmClass(9, 92027, "enum_aID_Buf9_W_SmokeMeter_SASB_Ans_TimeOut"));
            acsAlarm.Add(92028, new acsAlarmClass(9, 92028, "enum_aID_Buf9_W_SmokeMeter_SASB_ASTZAns_TimeOut"));
            acsAlarm.Add(92029, new acsAlarmClass(9, 92029, "enum_aID_Buf9_W_SmokeMeter_SMES_Ans_TimeOut"));
            acsAlarm.Add(92030, new acsAlarmClass(9, 92030, "enum_aID_Buf9_W_SmokeMeter_SMES_ASTZAns_TimeOut"));
            acsAlarm.Add(92031, new acsAlarmClass(9, 92031, "enum_aID_Buf9_W_SmokeMeter_AFSN_Ans_TimeOut"));
            acsAlarm.Add(93011, new acsAlarmClass(9, 93011, "enum_aID_Buf9_W_FuelMeter_Transmit_Fail"));
            acsAlarm.Add(93012, new acsAlarmClass(9, 93012, "enum_aID_Buf9_W_FuelMeter_ReceiveHeader_Notconsistent"));
            acsAlarm.Add(93013, new acsAlarmClass(9, 93013, "enum_aID_Buf9_W_FuelMeter_ReceiveIndexOutofRange"));
            acsAlarm.Add(93100, new acsAlarmClass(9, 93100, "enum_aID_Buf9_W_FuelMeter_SRES_Receive_NAK"));
            acsAlarm.Add(93200, new acsAlarmClass(9, 93200, "enum_aID_Buf9_W_FuelMeter_SREM_Receive_NAK"));
            acsAlarm.Add(93290, new acsAlarmClass(9, 93290, "enum_aID_Buf9_W_FuelMeter_SwitchRemoteMode_ASTZ_Receive_NAK"));
            acsAlarm.Add(93300, new acsAlarmClass(9, 93300, "enum_aID_Buf9_W_FuelMeter_Init_STBY_Receive_NAK"));
            acsAlarm.Add(93400, new acsAlarmClass(9, 93400, "enum_aID_Buf9_W_FuelMeter_Init_ActivateStandbyMode_ASTZ_Receive_NAK"));
            acsAlarm.Add(93500, new acsAlarmClass(9, 93500, "enum_aID_Buf9_W_FuelMeter_SBVZ_Receive_NAK"));
            acsAlarm.Add(93600, new acsAlarmClass(9, 93600, "enum_aID_Buf9_W_FuelMeter_EMPA_Receive_NAK"));
            acsAlarm.Add(93700, new acsAlarmClass(9, 93700, "enum_aID_Buf9_W_FuelMeter_SetMeasurePara_ASTZ_Receive_NAK"));
            acsAlarm.Add(93790, new acsAlarmClass(9, 93790, "enum_aID_Buf9_W_FuelMeter_Measure_STBY_Receive_NAK"));
            acsAlarm.Add(93800, new acsAlarmClass(9, 93800, "enum_aID_Buf9_W_FuelMeter_Measure_ActivateStandbyMode_ASTZ_Receive_NAK"));
            acsAlarm.Add(93020, new acsAlarmClass(9, 93020, "enum_aID_Buf9_W_FuelMeter_MesureReqFail_NotInit"));
            acsAlarm.Add(93021, new acsAlarmClass(9, 93021, "enum_aID_Buf9_W_FuelMeter_SMGV_Receive_NAK"));
            acsAlarm.Add(93022, new acsAlarmClass(9, 93022, "enum_aID_Buf9_W_FuelMeter_StartMeasuring_AWRT_Receive_NAK"));
            acsAlarm.Add(93023, new acsAlarmClass(9, 93023, "enum_aID_Buf9_W_FuelMeter_MeasureResult_AERG_Receive_NAK"));
            acsAlarm.Add(93024, new acsAlarmClass(9, 93024, "enum_aID_Buf9_W_FuelMeter_SRES_Ans_TimeOut"));
            acsAlarm.Add(-93024, new acsAlarmClass(9, 93024, "enum_aID_Buf9_W_FuelMeter_SREM_Ans_TimeOut"));
            acsAlarm.Add(93025, new acsAlarmClass(9, 93025, "enum_aID_Buf9_W_FuelMeter_SREM_ASTZAns_TimeOut"));
            acsAlarm.Add(93026, new acsAlarmClass(9, 93026, "enum_aID_Buf9_W_FuelMeter_Init_STBY_Ans_TimeOut"));
            acsAlarm.Add(93027, new acsAlarmClass(9, 93027, "enum_aID_Buf9_W_FuelMeter_Init_STBY_ASTZAns_TimeOut"));
            acsAlarm.Add(93028, new acsAlarmClass(9, 93028, "enum_aID_Buf9_W_FuelMeter_SBVZ_Ans_TimeOut"));
            acsAlarm.Add(93029, new acsAlarmClass(9, 93029, "enum_aID_Buf9_W_FuelMeter_EMPA_Ans_TimeOut"));
            acsAlarm.Add(93030, new acsAlarmClass(9, 93030, "enum_aID_Buf9_W_FuelMeter_EMPA_ASTZAns_TimeOut"));
            acsAlarm.Add(93031, new acsAlarmClass(9, 93031, "enum_aID_Buf9_W_FuelMeter_Mesure_STBY_Ans_TimeOut"));
            acsAlarm.Add(93032, new acsAlarmClass(9, 93032, "enum_aID_Buf9_W_FuelMeter_Measure_STBY_ASTZAns_TimeOut"));
            acsAlarm.Add(93033, new acsAlarmClass(9, 93033, "enum_aID_Buf9_W_FuelMeter_Mesure_SMGV_Ans_TimeOut"));
            acsAlarm.Add(93034, new acsAlarmClass(9, 93034, "enum_aID_Buf9_W_FuelMeter_Measure_SMGV_AERTAns_TimeOut"));
            acsAlarm.Add(92034, new acsAlarmClass(9, 92034, "enum_aID_Buf9_W_FuelMeter_AERG_Ans_TimeOut"));
        }

        public static string AlarmCheck()
        {
            string str = "";
            object Result;

            try
            {
                int Count = (int)Ethercat.Ch.ReadVariable("alarm_RegisterRequest_Index", ProgramBuffer.ACSC_BUFFER_2, -1, -1, -1, -1);
                if (Count < AlarmALLCNT)
                    AlarmALLCNT = 0;
                if (Count > AlarmALLCNT)
                {
                    Result = Ethercat.Ch.ReadVariable("list_Alarm_History", ProgramBuffer.ACSC_BUFFER_2, AlarmALLCNT, Count - 1, -1, -1);
                    str = Ethercat.AlarmToString(Result, Count - AlarmALLCNT);
                    AlarmALLCNT = Count;
                }
            }
            catch { }
            return str;
        }

        //  This function sends the error message when an exception is thrown
        public static void ErrorMsg(COMException Ex)
        {
            string Str = "Error from " + Ex.Source + "\n\r";
            Str = Str + Ex.Message + "\n\r";
            Str = Str + "HRESULT:" + String.Format("0x{0:X}", (Ex.ErrorCode));
            MessageBox.Show(Str, "Ethercat Error!");
        }

        //  This function sends the error message when an exception is thrown - same as before just for ACSException
        public static void ErrorMsg(ACSException Ex)
        {
            string Str = "Error from " + Ex.Source + "\n\r";
            Str = Str + Ex.Message + "\n\r";
            Str = Str + "HRESULT:" + String.Format("0x{0:X}", (Ex.ErrorCode));
            MessageBox.Show(Str, "Ethercat Error!");
        }

        public static String MatrixToString(object Matrix, bool doubleType)
        {
            String Str = "";
            int To1, To2;

            To1 = doubleType ? ((double[,])Matrix).GetUpperBound(0) : ((int[,])Matrix).GetUpperBound(0);
            To2 = doubleType ? ((double[,])Matrix).GetUpperBound(1) : ((int[,])Matrix).GetUpperBound(1);
            for (int i = 0; i <= To1; i++)
            {
                for (int j = 0; j <= To2; j++)
                {
                    Str = Str + (doubleType ? ((double[,])Matrix)[i, j] : ((int[,])Matrix)[i, j]) + " ";
                }
                Str = Str + "\n";
            }
            return Str;
        }

        public static String ControlToString(double[] Vector)
        {
            String Str = "";

            Str = Str + "index_PidList_C = \t" + (Vector[4]) + "\n";
            Str = Str + "index_PidList_Y = \t" + (Vector[10]) + "\n";
            Str = Str + "index_PidList_Target = \t" + (Vector[15]) + "\n";
            Str = Str + "index_PidList_Ref = \t" + (Vector[16]) + "\n";
            Str = Str + "index_PidList_P_Error = \t" + (Vector[17]) + "\n";
            Str = Str + "index_PidList_P_PrevError = \t" + (Vector[18]) + "\n";
            Str = Str + "index_PidList_I_Error = \t" + (Vector[19]) + "\n";
            Str = Str + "index_PidList_D_Error = \t" + (Vector[20]) + "\n";
            Str = Str + "index_PidList_P_Portion = \t" + (Vector[21]) + "\n";
            Str = Str + "index_PidList_I_Portion = \t" + (Vector[22]) + "\n";
            Str = Str + "index_PidList_D_Portion = \t" + (Vector[23]) + "\n";
            Str = Str + "index_PidList_C_Ref = \t" + (Vector[27]) + "\n";
            Str = Str + "index_PidList_C_PrevOutput = \t" + (Vector[28]) + "\n";

            return Str;
        }

        public static void GetACS_PCData()
        {
            try
            {
                if (Ch.IsConnected)
                {
                    g_SystemState = (int[])Ch.ReadVariable("g_SystemState", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);
                    g_FeedbackValue = (double[])Ch.ReadVariable("g_FeedbackValue", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);
                    double[] g_TargetValue_Temp = (double[])Ch.ReadVariable("g_Target", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);
                    g_TargetValue = new double[3]
                    {
                        GetRealValue(g_TargetValue_Temp[0], DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).CalibrationTable),
                        GetRealValue(g_TargetValue_Temp[1], DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).CalibrationTable),
                        GetRealValue(g_TargetValue_Temp[2], DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).CalibrationTable)
                    };
                }
            }

            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        public static double GetRealValue(double IOValue, List<ValuePair> CalibrationTable)
        {
            double RValue = 0;

            if (CalibrationTable.Count == 0)
            {
                RValue = IOValue;
            }
            else if (CalibrationTable.Count == 1)//ValuePair=(Voltage.Max,PhysicalValue.Max)=(c,d)
            {
                RValue = (double)CalibrationTable[0].RealValue / (double)CalibrationTable[0].IOValue * (double)IOValue; // physical value = d / c * Rawdata.
            }
            else//2개인 경우. 첫번째 ValuePair=(Voltage.Max,PhysicalValue.Max)=(c,d). 두번째 ValuePair=(Voltage.Min,PhysicalValue.Min)=(a,c). 큰값을 먼저 ValuePair에 넣어주고 작은 값을 나중에 넣어주어야 함. 자동으로 바꾸게 할지는 보류.
            {
                RValue = ((double)CalibrationTable[0].RealValue - (double)CalibrationTable[1].RealValue) / ((double)CalibrationTable[0].IOValue - (double)CalibrationTable[1].IOValue) * ((double)IOValue - (double)CalibrationTable[1].IOValue) + (double)CalibrationTable[1].RealValue; // physical value = (d - b) / (c - a) * (Rawdata - a) + b
            }

            return RValue;
        }

        public static double GetIOValue(double RealValue, List<ValuePair> CalibrationTable)
        {
            double RValue = 0;

            if (CalibrationTable.Count == 0)
            {
                RValue = RealValue;
            }
            else if (CalibrationTable.Count == 1)//ValuePair=(Voltage.Max,PhysicalValue.Max)=(c,d)
            {
                RValue = (double)CalibrationTable[0].IOValue / (double)CalibrationTable[0].RealValue * (double)RealValue; // physical value = d / c * Rawdata.
            }
            else//2개인 경우. 첫번째 ValuePair=(Voltage.Max,PhysicalValue.Max)=(c,d). 두번째 ValuePair=(Voltage.Min,PhysicalValue.Min)=(a,c). 큰값을 먼저 ValuePair에 넣어주고 작은 값을 나중에 넣어주어야 함. 자동으로 바꾸게 할지는 보류.
            {
                RValue = ((double)CalibrationTable[0].IOValue - (double)CalibrationTable[1].IOValue) / ((double)CalibrationTable[0].RealValue - (double)CalibrationTable[1].RealValue) * ((double)RealValue - (double)CalibrationTable[1].RealValue) + (double)CalibrationTable[1].IOValue; // physical value = (d - b) / (c - a) * (Rawdata - a) + b
            }

            return RValue;
        }


        public enum DriveMode
        {
            Default = 0,
            Idle = 1,
            IdleControl = 2,
            TA=3,
            NA=4,
            TN=5,
            NT=6
        }

        public static DriveMode CurrentDriveMode = DriveMode.Default;
        private static void ParsingDriveMode()
        {
            if (g_PC_Read_Data[0x50] == 1)
            {
                CurrentDriveMode = DriveMode.Idle;
            }
            else if (g_PC_Read_Data[0x51] == 1)
            {
                CurrentDriveMode = DriveMode.IdleControl;
            }
            else if (g_PC_Read_Data[0x52] == 1)
            {
                CurrentDriveMode = DriveMode.TA;
            }
            else if (g_PC_Read_Data[0x53] == 1)
            {
                CurrentDriveMode = DriveMode.NA;
            }
            else if (g_PC_Read_Data[0x54] == 1)
            {
                CurrentDriveMode = DriveMode.TN;
            }
            else if (g_PC_Read_Data[0x55] == 1)
            {
                CurrentDriveMode = DriveMode.NT;
            }
            else
            {
                CurrentDriveMode = DriveMode.Default;
            }
        }

        public static String OutputToString(int[] Vector)
        {
            String Str = "";

            Str = Str + "DI(60) = \t" + (Vector[4]) + "\n";
            Str = Str + "DI(61) = \t" + (Vector[5]) + "\n";

            Str = Str + "Alpha (100) = \t" + (float)(Vector[20]) / 32767.0 * 10 + "\n";
            Str = Str + "Dyno Set Torque(82) = \t" + (float)(Vector[7]) / 32767.0 * 10 + "\n";
            Str = Str + "WTC 553 SET + (D0) = \t" + (Vector[12]) + "\n";
            Str = Str + "WTC 553 SET - (D1) = \t" + (Vector[13]) + "\n";
            Str = Str + "FTC 753H SET + (D4) = \t" + (Vector[16]) + "\n";
            Str = Str + "FTC 753H SET - (D5) = \t" + (Vector[17]) + "\n";
            Str = Str + "TARGET ALPHA DA = \t" + (float)(Vector[256]) / 32767.0 * 10 + "\n";

            return Str;
        }

        public static String InputToString(int[] Vector)
        {
            String Str = "";

            Str = Str + "MD = \t" + (Vector[512 + 11]) + "\n";
            Str = Str + "N  = \t" + (Vector[512 + 13]) + "\n";
            Str = Str + "SP_E = \t" + (Vector[512 + 14]) + "\n";
            Str = Str + "WT_PV = \t" + (Vector[512 + 18]) + "\n";
            Str = Str + "T_FUEL_I = \t" + (Vector[512 + 20]) + "\n";
            Str = Str + "FM_PV = \t" + (Vector[512 + 21]) + "\n";
            Str = Str + "BB_PV = \t" + (Vector[512 + 22]) + "\n";
            Str = Str + "BB_PPV = \t" + (Vector[512 + 23]) + "\n";
            Str = Str + "MF_GAS = \t" + (Vector[512 + 25]) + "\n";
            Str = Str + "PA0 = \t" + (Vector[512 + 26]) + "\n";
            Str = Str + "PA1 = \t" + (Vector[512 + 27]) + "\n";
            Str = Str + "PA2_2 = \t" + (Vector[512 + 28]) + "\n";
            Str = Str + "PA3 = \t" + (Vector[512 + 29]) + "\n";
            Str = Str + "PA2_1 = \t" + (Vector[512 + 30]) + "\n";
            Str = Str + "PE2_1 = \t" + (Vector[512 + 31]) + "\n";
            Str = Str + "P_CYL = \t" + (Vector[512 + 32]) + "\n";
            Str = Str + "P_EGRHEO = \t" + (Vector[512 + 33]) + "\n";
            Str = Str + "PE1_1 = \t" + (Vector[512 + 34]) + "\n";
            Str = Str + "P14 = \t" + (Vector[512 + 35]) + "\n";
            Str = Str + "PE1_2 = \t" + (Vector[512 + 36]) + "\n";
            Str = Str + "PE2_2 = \t" + (Vector[512 + 37]) + "\n";
            Str = Str + "PE3 = \t" + (Vector[512 + 38]) + "\n";
            Str = Str + "P_OIL = \t" + (Vector[512 + 39]) + "\n";
            Str = Str + "P15 = \t" + (Vector[512 + 40]) + "\n";
            Str = Str + "P16 = \t" + (Vector[512 + 41]) + "\n";
            Str = Str + "HR_IR = \t" + (Vector[512 + 42]) + "\n";
            Str = Str + "BT = \t" + (Vector[512 + 43]) + "\n";
            Str = Str + "TA_ = \t" + (Vector[512 + 46]) + "\n";
            Str = Str + "TE_C1 = \t" + (Vector[512 + 47]) + "\n";

            return Str;
        }

        public static String VectorToString(object Vector, bool doubleType)
        {
            String Str = "";
            int To;
            To = doubleType ? ((double[])Vector).GetUpperBound(0) : ((int[])Vector).GetUpperBound(0);
            for (int i = 0; i <= To; i++)
                Str = Str + (doubleType ? ((double[])Vector)[i] : ((int[])Vector)[i]) + " ";
            return Str;
        }

        public static String AlarmToString(object Vector, int AlarmCNT)
        {
            String Str = "";
            int AlarmCode;

            if (AlarmCNT == 1)
            {
                AlarmCode = ((int)Vector);
                if (AlarmCode > 0)
                {
                    acsAlarmClass Alarm = Ethercat.acsAlarm[AlarmCode];
                    Str = string.Format("Buffer{0} : {1} - {2}", Alarm.BufferNo, Alarm.AlarmCode, Alarm.AlarmDescription);
                }
            }
            else
            {
                for (int i = AlarmCNT - 1; i >= 0; i--)
                {
                    AlarmCode = ((int[])Vector)[i];
                    if (AlarmCode > 0)
                    {
                        acsAlarmClass Alarm = Ethercat.acsAlarm[AlarmCode];
                        Str = string.Format("Buffer{0} : {1} - {2}", Alarm.BufferNo, Alarm.AlarmCode, Alarm.AlarmDescription);
                    }
                }
            }

            return Str;
        }
        #endregion

        public static void MakeModuleNode(TreeNode tn, List<acsModuleInfo> Modules)
        {
            foreach (acsModuleInfo CH_Info in Modules)
            {
                TreeNode tn_mds = tn.Nodes.Add(
                    String.Format("Slave{0} - {1} (0x{2:X} : 0x{3:X})", CH_Info.SlaveNo, CH_Info.ProductName, CH_Info.VendorID, CH_Info.ProductID)
                    );
            }
        }
        public static void MakeChannelNode(TreeNode tn, List<acsChannelInfo> Channels)
        {
            foreach (acsChannelInfo CH_Info in Channels)
            {
                TreeNode tn_mds = tn.Nodes.Add(
                    String.Format("CH{0} - {1} ({2} : {3} : 0x{4:X})", CH_Info.ChannelIndex, CH_Info.VariableName, CH_Info.ModuleInfo.SlaveNo, CH_Info.ModuleInfo.ProductName, CH_Info.Offset)
                    );
            }
        }
        public static void MakeVarNode(TreeNode tn, List<VariableInfo> Variables, string pre)
        {
            foreach (VariableInfo Var_Info in Variables)
            {
                if (pre.Length > 0)
                {
                    if (Var_Info.ChannelInfo != null)
                    {
                        TreeNode tn_mds = tn.Nodes.Add(
                            String.Format("{0}{1:00} - {2} ( {0} CH{3} )", pre, Var_Info.Index, Var_Info.VariableName, Var_Info.ChannelInfo.ChannelIndex)
                            );
                    } else
                    {
                        TreeNode tn_mds = tn.Nodes.Add(
                            String.Format("{0}{1:00} - {2}", pre, Var_Info.Index, Var_Info.VariableName)
                            );
                    }
                } else if (Var_Info.Formula.Length > 0)
                {
                    TreeNode tn_mds = tn.Nodes.Add(
                        String.Format("{0}{1:00} - {2} ( {3} )", pre, Var_Info.Index, Var_Info.VariableName, Var_Info.Formula)
                        );
                }
                else
                {
                    TreeNode tn_mds = tn.Nodes.Add(
                        String.Format("{0}{1:00} - {2}", pre, Var_Info.Index, Var_Info.VariableName)
                        );
                }
            }
        }

        public static string GetIOString(ChannelIO IO)
        {
            string ret = "";
            switch (IO)
            {
                case ChannelIO.None:
                    ret = "NONE";
                    break;
                case ChannelIO.Input:
                    ret = "INPUT";
                    break;
                case ChannelIO.Output:
                    ret = "OUTPUT";
                    break;
            }

            return ret;
        }

        public static string GetTypeString(ChannelType IO)
        {
            string ret = "";
            switch (IO)
            {
                case ChannelType.None:
                    ret = "NONE";
                    break;
                case ChannelType.Analog:
                    ret = "ANALOG";
                    break;
                case ChannelType.Digital:
                    ret = "DIGITAL";
                    break;
                case ChannelType.Calculated:
                    ret = "Calculated";
                    break;
            }

            return ret;
        }

        public static string GetUseForString(ChannelUseFor IO)
        {
            string ret = "";
            switch (IO)
            {
                case ChannelUseFor.HMI:
                    ret = "HMI";
                    break;
                case ChannelUseFor.DAQ:
                    ret = "DAQ";
                    break;
            }

            return ret;
        }

        public static string GetVarString(List<string> Vars)
        {
            string ret = "";

            if (Vars != null)
            {
                foreach (string str in Vars)
                {
                    if (ret == "")
                        ret = str;
                    else
                        ret = ret + ", " + str;
                }
            }
            return ret;
        }
    }


    public class acsAlarmClass
    {
        public int BufferNo;
        public int AlarmCode;
        public string AlarmDescription;

        public acsAlarmClass(int BufferNo, int AlarmCD, string AlarmDesc)
        {
            this.BufferNo = BufferNo;
            this.AlarmCode = AlarmCD;
            this.AlarmDescription = AlarmDesc;
        }
    }

    public class internalVariables
    {
        public int Dyno_Torque_Range = 4000;
        public int Dyno_Speed_Range = 8000;
        public int Engine_AlphaPosi_Range = 100;
        public int Engine_Speed_Range = 8000;
        public int Engine_Torque_Range = 4000;

    }

}
