﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtomUI.Class
{
    public class MexaParsing
    {
        public static bool AddMexaChannels(string FileName)
        {
            try
            {
                int Index = 0;
                acsChannelInfo channelItem;
                VariableInfo varItem;

                string[] MexaAllValue = System.IO.File.ReadAllLines(FileName);
                List<string> varlist = MexaAllValue.ToList<string>();
                varlist.RemoveAt(0);

                acsModuleInfo item = new acsModuleInfo("Line1", "Mexa 9x00", 0, 0, ChannelIO.Input, ChannelType.Analog, 0, 0, varlist.Count);
                item.UseFor = ChannelUseFor.MEXA;
                Ethercat.sysInfo.MEXAModuleInfos.Add(item);

                foreach (string MexaChannel in varlist)
                {
                    string[] ChannelData = MexaChannel.Split(',');
                    channelItem = new acsChannelInfo(item, 0, Index, ChannelData[1], 0);
                    Ethercat.chInfo.MEXA9000_Channels.Add(channelItem);
                    varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
                    varItem.Unit = ChannelData[2];
                    varItem.Index = Index;
                    DaqData.sysVarInfo.MEXA9000_Variables.Add(varItem);
                    Index++;
                }

                return true;
            } catch(Exception ex)
            {
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n AddMexaChannels Error!!");
                return false;
            }
        }

        public static void AddChannel(acsModuleInfo item, int index, string chName, string Unit)
        {
            acsChannelInfo channelItem;
            VariableInfo varItem;

            channelItem = new acsChannelInfo(item, 0, index, chName, 0);
            Ethercat.chInfo.MEXA9000_Channels.Add(channelItem);
            varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
            varItem.Unit = Unit;
            varItem.Index = index;
            DaqData.sysVarInfo.MEXA9000_Variables.Add(varItem);
        }

        public static bool AddMexaChannels()
        {


            int index = 0;

            acsModuleInfo item = new acsModuleInfo("Line1", "Mexa 9x00", 0,0,ChannelIO.Input, ChannelType.Analog, 0,0,15);
            item.UseFor = ChannelUseFor.MEXA;
            Ethercat.sysInfo.MEXAModuleInfos.Add(item);

            AddChannel(item, index, "CO-L", "ppm");
            index++;
            AddChannel(item, index, "CO-H", "vol%");
            index++;
            AddChannel(item, index, "CO2", "vol%");
            index++;
            AddChannel(item, index, "ECO2", "ppm");
            index++;
            AddChannel(item, index, "THC", "ppm");
            index++;
            AddChannel(item, index, "O2", "vol%");
            index++;
            AddChannel(item, index, "NOX", "ppm");
            index++;
            AddChannel(item, index, "CO-L(R)", "ppm");
            index++;
            AddChannel(item, index, "CO-H(R)", "ppm");
            index++;
            AddChannel(item, index, "CO2(R)", "ppm");
            index++;
            AddChannel(item, index, "THC(R)", "ppm");
            index++;
            AddChannel(item, index, "O2(R)", "ppm");
            index++;
            AddChannel(item, index, "NO(R)", "ppm");
            index++;
            AddChannel(item, index, "A/F", "-");
            index++;
            AddChannel(item, index, "EGR", "%");
            index++;

            return true;
        }
    }
}
