﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtomUI.Class
{
    public class ModbusParsing
    {
        public static bool AddModbusChannels(string fpath)
        {
            try
            {
                int Index = 0;
                acsChannelInfo channelItem;
                VariableInfo varItem;

                string[] ModbusAllValue = System.IO.File.ReadAllLines(fpath,Encoding.Default);
                List<string> varlist = ModbusAllValue.ToList<string>();
                varlist.RemoveAt(0);

                Ethercat.chInfo.ModBus_Channels.Clear();
                DaqData.sysVarInfo.ModBus_Variables.Clear();

                acsModuleInfo item = new acsModuleInfo("MODBUS DAQ", "MODBUS DAQ", 0, 0, ChannelIO.Input, ChannelType.Analog, 0, 0, varlist.Count);
                item.UseFor = ChannelUseFor.MODBUS;
                Ethercat.sysInfo.ModBusModuleInfos.Add(item);

                foreach (string ModbusChannel in varlist)
                {
                    string[] ChannelData = ModbusChannel.Split(',');
                    channelItem = new acsChannelInfo(item, int.Parse(ChannelData[1])*8, Index, ChannelData[3], int.Parse(ChannelData[0]));
                    channelItem.ValueType = (ChannelData[8] == "unsigned") ? 0 : 1;
                    Ethercat.chInfo.ModBus_Channels.Add(channelItem);
                    varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
                    //varItem.IOIndex = int.Parse(ChannelData[0]);
                    varItem.Offset = 0;
                    varItem.Factor = 1/double.Parse(ChannelData[7]);
                    varItem.Range_Low = double.Parse(ChannelData[4]);
                    varItem.Range_High = double.Parse(ChannelData[5]);
                    varItem.NorminalDescription = ChannelData[2];
                    varItem.Unit = ChannelData[6];
                    varItem.Index = Index;
                    DaqData.sysVarInfo.ModBus_Variables.Add(varItem);
                    Index++;
                }

                return true;
            }
            catch (Exception ex)
            {
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n AddModbusChannels Error!!");
                return false;
            }
        }

    }
}
