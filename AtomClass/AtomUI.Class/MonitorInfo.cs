﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;

namespace AtomUI.Class
{
    [Serializable]
    public enum MonitorStatus
    {
        Disable = 1,
        Tab = 2,
        Windows = 3
    }

    [Serializable]
    public class MonitorInfo
    {
        public MonitorItemList MonitorList = new MonitorItemList();
        public MonitorItemList MonitorDefine = new MonitorItemList(true);

        public void SaveMonInfo(string _MonitorName)
        {
            MonitorList.monName = _MonitorName;
            string FileName = Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Class.Ethercat.sysInfo.sysSettings.MonInfoDir + "\\" + _MonitorName + ".xml";
            FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write);
            XmlSerializer xs = new XmlSerializer(MonitorList.GetType());
            xs.Serialize(fs, MonitorList);
            fs.Close();
        }

        public void LoadMonInfo(string _MonitorName)
        {
            MonitorList.monName = _MonitorName;
            string FileName = Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Class.Ethercat.sysInfo.sysSettings.MonInfoDir + "\\" + _MonitorName + ".xml";

            if (!File.Exists(FileName))
            {
                MonitorList = new MonitorItemList();
            }
            else
            {
                FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
                XmlSerializer xs = new XmlSerializer(MonitorList.GetType());
                Object dd = xs.Deserialize(fs);
                MonitorList = (MonitorItemList)dd;

                foreach (MonitorItem mon in MonitorList.Monitor)
                {
                    MatchingVariables(mon);
                }
                fs.Close();
            }
        }

        private void MatchingVariables(MonitorItem mon)
        {
            for (int i = 0; i < mon.VARs.Count; i++)
            {
                bool Founded = false;
                VariableInfo var = mon.VARs[i];
                foreach (VariableInfo varTarget in DaqData.sysVarInfo.ALL_Variables)
                {
                    if (var.VariableName.Trim() == varTarget.VariableName.Trim() && var.NorminalName.Trim() == varTarget.NorminalName.Trim())
                    {
                        mon.VARs[i] = varTarget;
                        Founded = true;
                        break;
                    }
                }
                if (!Founded)
                {
                    foreach (VariableInfo varTarget in DaqData.sysVarInfo.Calc_Variables)
                    {
                        if (var.VariableName.Trim() == varTarget.VariableName.Trim() && var.NorminalName.Trim() == varTarget.NorminalName.Trim())
                        {
                            mon.VARs[i] = varTarget;
                            Founded = true;
                            break;
                        }
                    }

                }
            }

        }
    }


    [Serializable]
    public class MonitorItemList
    {
        public string monName = "";
        public MonitorStatus monStatus = MonitorStatus.Disable;

        public List<MonitorItem> Monitor = new List<MonitorItem>();

        public MonitorItemList()
        { }
        public MonitorItemList(bool Initialize)
        {
            if (Initialize)
            {
                Init();
            }
        }

        public void Init()
        {
            MonitorItem MI = new MonitorItem();
            MI.monitorType = NextUI.BaseUI.NextUIType.Speedo;
            Monitor.Add(MI);

            MI = new MonitorItem();
            MI.monitorType = NextUI.BaseUI.NextUIType.HalfSpeedo;
            Monitor.Add(MI);

            MI = new MonitorItem();
            MI.monitorType = NextUI.BaseUI.NextUIType.Graph;
            Monitor.Add(MI);

            MI = new MonitorItem();
            MI.monitorType = NextUI.BaseUI.NextUIType.Digital;
            Monitor.Add(MI);
        }
    }

    [Serializable]
    [DefaultPropertyAttribute("Name")]
    public class MonitorItem
    {
        private bool ColorStop = true;

        public int _TextBackGroundColor = Color.FromArgb(0x5a, 0x84, 0xdd).ToArgb();
        public int _TextBackGroundColorWarning = Color.FromArgb(0xfe, 0xc3, 0x1a).ToArgb();
        public int _TextBackGroundColorError = Color.FromArgb(0xff, 0x00, 0x00).ToArgb();

        public int _UnitTextColor = Color.FromArgb(0x3b, 0x59, 0x98).ToArgb();
        public int _TitleTextColor = Color.FromArgb(0x0d, 0x25, 0x56).ToArgb();
        public int _ValueTextColor = Color.FromArgb(0x00, 0x00, 0x00).ToArgb();

        public int _ValueScaleColor = Color.FromArgb(0x3b, 0x59, 0x98).ToArgb();

        public int _BackGroundColor = Color.FromArgb(0xf3, 0xf5, 0xfd).ToArgb();

        private NextUI.BaseUI.NextUIType _monitorType = NextUI.BaseUI.NextUIType.Digital;
        private int _Left = 0;
        private int _Top = 0;
        private int _Width = 0;
        private int _Height = 0;
        private string _Unit = "UNIT";
        private double _Range_Low = 0;
        private double _Range_High = 0;
        private double _Warning_Low = 0;
        private double _Warning_High = 0;
        private double _Alarm_Low = 0;
        private double _Alarm_High = 0;
        private string _Formula = "";
        private string _name = "NAME";
        private int _VarEA = 1;

        public List<VariableInfo> VARs;

        public MonitorItem()
        {
        }

        [CategoryAttribute("Color"), DescriptionAttribute("Scale Main Color")]
        public Color ValueScaleColor
        {
            get
            {
                return Color.FromArgb(_ValueScaleColor);
            }
            set
            {
                if (!ColorStop)
                    _ValueScaleColor = value.ToArgb();
            }
        }

        [CategoryAttribute("Color"), DescriptionAttribute("Background Fill Color")]
        public Color BackGroundColor
        {
            get
            {
                return Color.FromArgb(_BackGroundColor);
            }
            set
            {
                if (!ColorStop)
                    _BackGroundColor = value.ToArgb();
            }
        }

        [CategoryAttribute("Color"), DescriptionAttribute("Unit Text Color")]
        public Color UnitTextColor
        {
            get
            {
                return Color.FromArgb(_UnitTextColor);
            }
            set
            {
                if (!ColorStop)
                    _UnitTextColor = value.ToArgb();
            }
        }

        [CategoryAttribute("Color"), DescriptionAttribute("Title Color")]
        public Color TitleTextColor
        {
            get
            {
                return Color.FromArgb(_TitleTextColor);
            }
            set
            {
                if (!ColorStop)
                    _TitleTextColor = value.ToArgb();
            }

        }

        [CategoryAttribute("Color"), DescriptionAttribute("Value Text Color")]
        public Color ValueTextColor
        {
            get
            {
                return Color.FromArgb(_ValueTextColor);
            }
            set
            {
                if (!ColorStop)
                    _ValueTextColor = value.ToArgb();
            }

        }

        [CategoryAttribute("Color"), DescriptionAttribute("Text Background Color & Value Range Fill Color - Normal Value")]
        public Color TextBackGroundColor
        {
            get
            {
                return Color.FromArgb(_TextBackGroundColor);
            }
            set
            {
                if (!ColorStop)
                    _TextBackGroundColor = value.ToArgb();
            }

        }

        [CategoryAttribute("Color"), DescriptionAttribute("Text Background Color & Value Range Fill Color - Warning Value")]
        public Color TextBackGroundColorWarning
        {
            get
            {
                return Color.FromArgb(_TextBackGroundColorWarning);
            }
            set
            {
                if (!ColorStop)
                    _TextBackGroundColorWarning = value.ToArgb();
            }
        }

        [CategoryAttribute("Color"), DescriptionAttribute("Text Background Color & Value Range Fill Color - Error Value")]
        public Color TextBackGroundColorError
        {
            get
            {
                return Color.FromArgb(_TextBackGroundColorError);
            }
            set
            {
                if (!ColorStop)
                    _TextBackGroundColorError = value.ToArgb();
            }
        }

        [CategoryAttribute("Monitor Setup"), DescriptionAttribute("Type of the Monitor Item")]
        public NextUI.BaseUI.NextUIType monitorType
        {
            get
            {
                return _monitorType;
            }
            set
            {
                _monitorType = value;

                switch (_monitorType)
                {
                    case NextUI.BaseUI.NextUIType.Digital:
                        _VarEA = 10;
                        break;
                    case NextUI.BaseUI.NextUIType.Graph:
                        _VarEA = 2;
                        break;
                    default:
                        _VarEA = 1;
                        break;
                }
            }
        }

        [CategoryAttribute("Behavior"), DescriptionAttribute("Units of the Monitor Variable")]
        public string Unit
        {
            get
            {
                return _Unit;
            }
            set
            {
                _Unit = value;
            }
        }

        [CategoryAttribute("Behavior"), DescriptionAttribute("Warning Limits of the Monitor Variable")]
        public double Warning_Low
        {
            get
            {
                return _Warning_Low;
            }
            set
            {
                _Warning_Low = value;
            }
        }

        [CategoryAttribute("Behavior"), DescriptionAttribute("Alarm Limits of the Monitor Variable")]
        public double Alarm_Low
        {
            get
            {
                return _Alarm_Low;
            }
            set
            {
                _Alarm_Low = value;
            }
        }
        [CategoryAttribute("Behavior"), DescriptionAttribute("Alarm Limits of the Monitor Variable")]
        public double Range_Low
        {
            get
            {
                return _Range_Low;
            }
            set
            {
                _Range_Low = value;
            }
        }

        [CategoryAttribute("Behavior"), DescriptionAttribute("Warning Limits of the Monitor Variable")]
        public double Warning_High
        {
            get
            {
                return _Warning_High;
            }
            set
            {
                _Warning_High = value;
            }
        }

        [CategoryAttribute("Behavior"), DescriptionAttribute("Alarm Limits of the Monitor Variable")]
        public double Alarm_High
        {
            get
            {
                return _Alarm_High;
            }
            set
            {
                _Alarm_High = value;
            }
        }

        [CategoryAttribute("Behavior"), DescriptionAttribute("Alarm Limits of the Monitor Variable")]
        public double Range_High
        {
            get
            {
                return _Range_High;
            }
            set
            {
                _Range_High = value;
            }
        }

        [CategoryAttribute("Behavior"), DescriptionAttribute("Formula of the Monitor Variable")]
        public string Formula
        {
            get
            {
                return _Formula;
            }
            set
            {
                _Formula = value;
            }
        }

        [CategoryAttribute("Monitor Setup"), DescriptionAttribute("Name of the Monitor Item")]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                ColorStop = false;

            }
        }
        [CategoryAttribute("Appearance"), DescriptionAttribute("Left position of Control")]
        public int Left
        {
            get
            {
                return _Left;
            }
            set
            {
                _Left = value;
            }
        }
        [CategoryAttribute("Appearance"), DescriptionAttribute("Top position of Control")]
        public int Top
        {
            get
            {
                return _Top;
            }
            set
            {
                _Top = value;
            }
        }
        [CategoryAttribute("Appearance"), DescriptionAttribute("Width of Control")]
        public int Width
        {
            get
            {
                return _Width;
            }
            set
            {
                _Width = value;
            }
        }
        [CategoryAttribute("Appearance"), DescriptionAttribute("Height of Control")]
        public int Height
        {
            get
            {
                return _Height;
            }
            set
            {
                _Height = value;
            }
        }
    }
}
