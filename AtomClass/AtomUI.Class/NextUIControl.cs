﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NextUI.Component;
using NextUI.Frame;
using System.Drawing;
using System.Windows.Forms.DataVisualization.Charting;

namespace AtomUI.Class
{
    public enum NextUIType
    {
        Speedo = 0,
        HalfSpeedo = 1,
        Graph = 2,
        Digital = 3
    }

    public class NextUIControl
    {
        NextUI.BaseUI.BaseUI baseUI;
        Chart chart1;
        CircularFrame frame;
        NumericalFrame nframe;
        NextUIType UIType;
        string Title = "";

        public float Value
        {
            get
            {
                float ReturnValue = 0;
                switch (this.UIType)
                {
                    case NextUIType.Speedo:
                        ReturnValue = frame.ScaleCollection[0].Pointer[0].Value;
                        break;
                    case NextUIType.HalfSpeedo:
                        ReturnValue = frame.ScaleCollection[0].Pointer[0].Value;
                        break;
                    case NextUIType.Graph:
                        break;
                    case NextUIType.Digital:
                        ReturnValue = float.Parse(nframe.Indicator.DisplayValue);
                        break;
                }
                return ReturnValue;
            }
            set
            {
                switch (this.UIType)
                {
                    case NextUIType.Speedo:
                        frame.ScaleCollection[0].Pointer[0].Value = value;
                        break;
                    case NextUIType.HalfSpeedo:
                        frame.ScaleCollection[0].Pointer[0].Value = value;
                        break;
                    case NextUIType.Graph:
                        break;
                    case NextUIType.Digital:
                        nframe.Indicator.DisplayValue = Convert.ToString(value);
                        break;
                }
            }
        }
        public DataPoint AddPoint
        {
            get
            {
                DataPoint ReturnValue = null;
                switch (this.UIType)
                {
                    case NextUIType.Speedo:
                        break;
                    case NextUIType.HalfSpeedo:
                        break;
                    case NextUIType.Graph:
                        ReturnValue = chart1.Series[0].Points[chart1.Series[0].Points.Count];
                        break;
                    case NextUIType.Digital:
                        break;
                }
                return ReturnValue;
            }
            set
            {
                switch (this.UIType)
                {
                    case NextUIType.Speedo:
                        break;
                    case NextUIType.HalfSpeedo:
                        break;
                    case NextUIType.Graph:
                        chart1.Series[0].Points.Add(value);
                        break;
                    case NextUIType.Digital:
                        break;
                }
            }
        }

        public NextUIControl()
        {

        }

        public NextUIControl(NextUI.BaseUI.BaseUI baseUI, NextUIType UIType, string Title)
        {
            this.baseUI = baseUI;
            this.UIType = UIType;
            this.Title = Title;
            Redraw();
        }

        public NextUIControl(Chart chart1, NextUIType UIType, string Title)
        {
            this.chart1 = chart1;
            this.UIType = UIType;
            this.Title = Title;
            Redraw();
        }


        public void Redraw()
        {

            switch (this.UIType)
            {
                case NextUIType.Speedo:
                    baseUI.Frame.Clear();
                    Speedo();
                    break;
                case NextUIType.HalfSpeedo:
                    baseUI.Frame.Clear();
                    HalfSpeedo();
                    break;
                case NextUIType.Graph:
                    if (chart1.Series.Count > 0)
                       chart1.Series.Clear();
                    ChartSetup();
                    break;
                case NextUIType.Digital:
                    baseUI.Frame.Clear();
                    Digital();
                    break;
            }

        }

        private void ChartSetup()
        {
            //X축 데이터 타입
            chart1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(223)))), ((int)(((byte)(193)))));
            chart1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(64)))), ((int)(((byte)(1)))));
            this.chart1.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.chart1.BorderlineWidth = 2;

            chart1.ChartAreas.Add(new ChartArea());
            chart1.ChartAreas[0].BackColor = System.Drawing.Color.OldLace;
            chart1.ChartAreas[0].BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.ChartAreas[0].BackSecondaryColor = System.Drawing.Color.White;
            chart1.ChartAreas[0].BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chart1.ChartAreas[0].AxisY2.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.DashDotDot;
            chart1.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].AxisY2.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));

            chart1.ChartAreas[0].CursorX.LineColor = Color.Gray;
            chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;

            chart1.Series.Clear();
            chart1.Series.Add(this.Title);

            chart1.Series[0].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            //Y축 데이터 타입
            chart1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;  //분산형 그래프
            chart1.Series[0].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            //chart1.Series[1].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;

            //차트 타입
            chart1.Series[0].IsXValueIndexed = false;
            chart1.Series[0].Color = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(65)))), ((int)(((byte)(140)))), ((int)(((byte)(240)))));
        }

        private void HalfSpeedo()
        {
            int width;
            if (baseUI.Width > baseUI.Height * 1.8)
                width = (int)(baseUI.Height * 1.8);
            else
                width = baseUI.Width;

            CircularFrame righframe = new CircularFrame(new Point((baseUI.Width - width) / 2, 0), width);
            this.baseUI.Frame.Add(righframe);
            this.frame = righframe;
            righframe.BackRenderer.CenterColor = Color.Black;
            righframe.BackRenderer.EndColor = Color.Black;
            righframe.FrameRenderer.Outline = NextUI.Renderer.FrameRender.FrameOutline.None;
            righframe.Type = CircularFrame.FrameType.HalfCircle3;

            CircularScaleBar rightbar = new CircularScaleBar(righframe);
            rightbar.FillGradientType = NextUI.Renderer.RendererGradient.GradientType.Solid;
            rightbar.FillColor = Color.White;
            rightbar.ScaleBarSize = 2;
            rightbar.TickMajor.FillColor = Color.White;
            rightbar.TickMajor.TickPosition = TickBase.Position.Inner;
            rightbar.TickMajor.Width = 10;
            rightbar.TickMajor.Height = 15;
            rightbar.TickMinor.FillColor = Color.White;
            rightbar.TickMinor.TickPosition = TickBase.Position.Inner;
            rightbar.MinorTicknumber = 5;
            rightbar.MajorTickNumber = 5;
            rightbar.StartValue = 0;
            rightbar.EndValue = 5000;
            //rightbar.CustomLabel = new string[] { "1", "2", "3", "4", "5", "6", "7", "8" };
            rightbar.SweepAngle = 180;
            rightbar.StartAngle = 0;
            rightbar.TickLabel.TextDirection = CircularLabel.Direction.Horizontal;
            rightbar.TickLabel.OffsetFromScale = width / 15;
            rightbar.TickLabel.LabelFont = new Font(FontFamily.GenericMonospace, 13, FontStyle.Bold);
            rightbar.TickLabel.FontColor = Color.White;
            righframe.ScaleCollection.Add(rightbar);

            CircularPointer rightpointer = new CircularPointer(righframe);
            rightpointer.BasePointer.Width = width / 40;
            rightpointer.BasePointer.Length = width / 2;
            rightpointer.BasePointer.FillColor = Color.Red;
            rightpointer.BasePointer.PointerShapeType = Pointerbase.PointerType.Type2;
            rightpointer.CapOnTop = false;
            rightpointer.BasePointer.OffsetFromCenter = -width / 20;
            rightbar.Pointer.Add(rightpointer);

            FrameLabel rightlabel = new FrameLabel(new Point(righframe.Rect.Width / 2 - 10, righframe.Rect.Height / 2 - 30), righframe);
            rightlabel.LabelText = this.Title;
            rightlabel.LabelFont = new Font(FontFamily.GenericMonospace, 13, FontStyle.Bold);
            rightlabel.FontColor = Color.White;

            righframe.FrameLabelCollection.Add(rightlabel);
        }

        private void Digital()
        {
            nframe = new NumericalFrame(new Rectangle(0, 0, baseUI.Width, baseUI.Height));
            this.baseUI.Frame.Add(nframe);
            nframe.FrameRenderer.Outline = NextUI.Renderer.FrameRender.FrameOutline.Type1;
            nframe.FrameRenderer.FrameWidth = 1;

            for (int i = 0; i < 6; i++)
            {
                DigitalPanel7Segment seg = new DigitalPanel7Segment(nframe);
                seg.BackColor = Color.DarkBlue;
                seg.FontThickness = 2;
                seg.MainColor = Color.White;
                nframe.Indicator.Panels.Add(seg);
            }
        }

        private void Speedo()
        {
            int width;
            if (baseUI.Width > baseUI.Height)
                width = baseUI.Height;
            else
                width = baseUI.Width;

            frame = new CircularFrame(new Point(0 , 0), width);
            this.baseUI.Frame.Add(frame);
            frame.BackRenderer.CenterColor = Color.Black;
            frame.BackRenderer.EndColor = Color.Black;
            frame.FrameRenderer.Outline = NextUI.Renderer.FrameRender.FrameOutline.None;

            CircularScaleBar bar = new CircularScaleBar(frame);
            bar.FillGradientType = NextUI.Renderer.RendererGradient.GradientType.Solid;
            bar.ScaleBarSize = 2;
            bar.FillColor = Color.White;
            bar.StartValue = 0;
            bar.EndValue = 200;
            bar.StartAngle = 50;
            bar.SweepAngle = 80;
            bar.MajorTickNumber = 11;
            bar.MinorTicknumber = 2;
            bar.TickMajor.EnableGradient = false;
            bar.TickMajor.EnableBorder = false;
            bar.TickMajor.FillColor = Color.White;
            bar.TickMajor.Height = 6;
            bar.TickMajor.Width = 3;
            bar.TickMajor.Type = TickBase.TickType.RoundedRect;
            bar.TickMinor.EnableGradient = false;
            bar.TickMinor.EnableBorder = false;
            bar.TickMinor.FillColor = Color.White;
            bar.TickMajor.TickPosition = TickBase.Position.Inner;
            bar.TickMinor.TickPosition = TickBase.Position.Inner;
            bar.TickLabel.TextDirection = CircularLabel.Direction.Horizontal;
            bar.TickLabel.OffsetFromScale = 15;
            bar.TickLabel.LabelFont = new Font(FontFamily.GenericMonospace, 10, FontStyle.Bold);
            bar.TickLabel.FontColor = Color.White;

            frame.ScaleCollection.Add(bar);
            //frame.ScaleCollection.Add(bar2);

            CircularRange range = new CircularRange(frame);
            range.EnableGradient = false;
            range.StartValue = 160;
            range.EndValue = 200;
            range.StartWidth = 2;
            range.EndWidth = 20;
            range.RangePosition = RangeBase.Position.Inner;

            bar.Range.Add(range);

            CircularPointer pointer = new CircularPointer(frame);
            pointer.CapPointer.Visible = true;
            pointer.CapOnTop = false;
            pointer.BasePointer.Length = width/2;
            pointer.BasePointer.FillColor = Color.Red;
            pointer.BasePointer.PointerShapeType = Pointerbase.PointerType.Type2;
            pointer.BasePointer.OffsetFromCenter = -width/6;

            bar.Pointer.Add(pointer);

        }
    }
}
