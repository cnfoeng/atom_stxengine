﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtomUI.Class
{
    public class OsirisParsing
    {
        public static bool AddOsirisChannels(string fpath)
        {
            try
            {
                int Index = 0;
                acsChannelInfo channelItem;
                VariableInfo varItem;

                string[] OsirisAllValue = System.IO.File.ReadAllLines(fpath, Encoding.Default);
                List<string> varlist = OsirisAllValue.ToList<string>();
                varlist.RemoveAt(0);

                Ethercat.chInfo.Osiris_Channels.Clear();
                DaqData.sysVarInfo.Osiris_Variables.Clear();

                acsModuleInfo item = new acsModuleInfo("Osiris DAQ", "Osiris DAQ", 0, 0, ChannelIO.Input, ChannelType.None, 0, 0, varlist.Count);
                item.UseFor = ChannelUseFor.OSIRIS;
                Ethercat.sysInfo.OsirisModuleInfos.Add(item);

                foreach (string OsirisChannel in varlist)
                {
                    string[] ChannelData = OsirisChannel.Split(',');
                    channelItem = new acsChannelInfo(item, 0 , Index, ChannelData[0], 0);
                    Ethercat.chInfo.Osiris_Channels.Add(channelItem);
                    varItem = new VariableInfo(channelItem, ChannelData[0], ChannelData[1]);

                    varItem.Unit = ChannelData[2];
                    varItem.Averaging = bool.Parse(ChannelData[3]);
                    varItem.AveragingCount = int.Parse(ChannelData[4]);
                    varItem.Index = Index;
                    DaqData.sysVarInfo.Osiris_Variables.Add(varItem);
                    Index++;
                    
                }

                return true;
            }
            catch (Exception ex)
            {
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n AddOsirisChannels Error!!");
                
                return false;
            }

        }

        
    }
}
