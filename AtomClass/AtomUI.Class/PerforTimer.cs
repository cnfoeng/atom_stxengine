﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Threading;


namespace AtomUI.Class
{
    public class PerforTimer
    {
        [DllImport("Kernel32.dll")]
        private static extern bool QueryPerformanceCounter(out long lpPerformanceCount);

        [DllImport("Kernel32.dll")]
        private static extern bool QueryPerformanceFrequency(out long lpFrequency);

        private static long freq;
        private static double ufreq;
        private static double mfreq;
        private ulong ulCnt = 0;
        private long startTime = 0;
        private long nowTime = 0;
        private double dbDuration = 0;
        private bool bUseFirstPass = false;

        static PerforTimer()
        {
            if (QueryPerformanceFrequency(out freq) == false)
            {
                throw new Win32Exception();
            }
            ufreq = freq / 1000000;
            mfreq = freq / 1000;
        }

        public PerforTimer()
        {
            USleep(0);
        }

        public void USleep(double us)    // 슬립
        {
            long startTime = 0;
            long nowTime = 0;

            QueryPerformanceCounter(out startTime);

            while (((nowTime - startTime) / ufreq) < us)
            {
                QueryPerformanceCounter(out nowTime);
            }
        }

        public void MSleep(double ms)  // 슬립
        {
            long startTime = 0;
            long nowTime = 0;

            QueryPerformanceCounter(out startTime);

            while (((nowTime - startTime) / mfreq) < ms)
            {
                QueryPerformanceCounter(out nowTime);
            }
        }

        public long GetStartTime()
        {
            return startTime;
        }
        public long GetProceedTime()
        {
            return (nowTime - startTime) / (long)mfreq;
        }


        public void MTimeoutInit(double _dbDuration, bool _bUseFirstPass = false)  // 타임아웃 초기화
        {
            dbDuration = _dbDuration;
            ulCnt = 0;
            bUseFirstPass = _bUseFirstPass;
            QueryPerformanceCounter(out nowTime);
            QueryPerformanceCounter(out startTime);
        }


        private double calc()
        {
            return (nowTime - startTime - mfreq * ulCnt * dbDuration) / mfreq;
        }

        int nSleep = 0;
        public void MTimeoutStart()   // 타임아웃
        {
            if (bUseFirstPass)
            {
                QueryPerformanceCounter(out nowTime);
                QueryPerformanceCounter(out startTime);
                bUseFirstPass = false;
                return;
            }

            while (calc() < dbDuration)
            {
                QueryPerformanceCounter(out nowTime);
                if ((nSleep = (int)(dbDuration - calc() - 5)) > 10)
                {
                    Thread.Sleep(nSleep);
                }
            }

            if (calc() > dbDuration) // 싸이클 안돌고 너무 밀리면 Cnt 최근으로 초기화 
            {
                if (calc() > dbDuration * 2 && calc() > 10)
                {
                    ulCnt = (uint)((nowTime - startTime) / mfreq / dbDuration);
                    Console.WriteLine("ReCnt:" + ulCnt);
                    return;
                }
            }

            ulCnt++;
        }




    }







    ////////////////////  이하 예제 코드     ////////////////////////

    //PerforTimer timerTest2 = null;
    //Thread m_Thr = null;
    //private void tmrDataLoggingCtn_Tick()
    //{
    //    if (DaqData.DataLoggingCtn == true)
    //    {
    //        timerTest2 = new PerforTimer();
    //        timerTest2.MTimeoutInit(1); // 100ms주기

    //        DaqData.Init_Log();

    //        swCtn.Start();

    //        while (true)
    //        {

    //            timerTest2.MTimeoutStart();
    //            if (swCtn.ElapsedMilliseconds < TotalCtnTime)
    //            {
    //                DaqData.nowStopWatchTickCtn = swCtn.ElapsedMilliseconds;

    //                DaqData.DataSaveCtn(false);
    //                ElapsedTimeLoggingCtn = swCtn.ElapsedMilliseconds;
    //            }
    //            else
    //            {
    //                DataLoggingCtnStop();
    //            }
    //        }
    //    }
    //}



    //public static string[] str = new string[100];
    //public static int a = 0;
    //public static Queue<string> que = new Queue<string>();

    //public static Thread m_Thr = null;

    //public static void Test()
    //{
    //    m_Thr = new Thread(new ThreadStart(dequeThr));
    //    m_Thr.Start();//쓰레드 시작
    //    Thread.Sleep(300);
    //}
    //public static int nCnt = 0;

    //public static void dequeThr()
    //{
    //    string strtmp = "";

    //    while (que.Count > 0)
    //        que.Dequeue();


    //    while (true)
    //    {
    //        if (DataLoggingCtn)
    //        {
    //            if (que.Count > 1)
    //            {
    //                if (nCnt++ > 1000)
    //                {
    //                    nCnt = 0;
    //                    System.IO.File.AppendAllText(strCtnFileName, strtmp, Encoding.UTF8);
    //                }
    //                else
    //                {
    //                    strtmp += que.Dequeue() + "\r\n";
    //                }
    //            }

    //        }
    //        else
    //        {
    //            while (que.Count > 0)
    //            {
    //                if (nCnt++ > 1000 || que.Count == 0)
    //                {
    //                    nCnt = 0;
    //                    System.IO.File.AppendAllText(strCtnFileName, strtmp, Encoding.UTF8);
    //                }
    //                else
    //                {
    //                    strtmp += que.Dequeue() + "\r\n";
    //                }
    //            }
    //            m_Thr.Abort();
    //            m_Thr.Join();
    //        }

    //    }
    //}

    //public static void DataSaveCtn(bool pause)
    //{
    //    if (DataLoggingCtn)
    //    {
    //        //if (nowStopWatchTickCtn - prevStopWatchTickCtn > SaveData.saveData.DataCtnLoggingInterval)
    //        //{
    //        string Value = DateTime.Now.ToString("yyyy_MM_dd HH:mm:ss") + "." + DateTime.Now.Millisecond.ToString("000");
    //        //string Value = DateTime.Now.ToString("yyyy_MM_dd HH:mm:ss:ms");
    //        prevStopWatchTickCtn = nowStopWatchTickCtn;

    //        foreach (VariableInfo var in InstantDataLogging_Variables)
    //        {
    //            Value = Value + "," + var.RealValue.ToString();
    //        } //foreach (VariableInfo AI in DataLogging_Variables)

    //        try
    //        {
    //            if (!pause)
    //            {
    //                que.Enqueue(Value);
    //                //m_cLog[0].AddLog(Value);
    //                //System.IO.File.AppendAllText(strCtnFileName, Value + "\r\n", Encoding.UTF8);
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            CommonFunction.LogStackTrace(ex.Message, ex.StackTrace);
    //        }
    //        //}
    //    }
    //}













}
