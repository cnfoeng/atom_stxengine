﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Data;
using System.Diagnostics;
using System.ComponentModel;

namespace AtomUI.Class
{
    [Serializable]
    public class SaveData
    {


        public static SaveData saveData = new SaveData();

        public int DataCtnLoggingInterval = 100;
        public int DataAvgLoggingInterval = 100;
        public int DataConstantLoggingInterval = 1000;
        public int DataMaxRowsPerFile = 1000;
        public short glHour;
        public short glMinute;
        public short glSecond;

        public bool DataLoggingIntervalData = false;

        public void Save(string FileName)
        {
            FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write);
            XmlSerializer xs = new XmlSerializer(this.GetType());
            xs.Serialize(fs, this);
            fs.Close();
        }

        public void Load(string FileName)
        {
            if (File.Exists(FileName))
            {
                FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
                XmlSerializer xs = new XmlSerializer(this.GetType());
                Object dd = xs.Deserialize(fs);
                fs.Close();
                SaveData saveData = new SaveData();
                saveData = (SaveData)dd;

                this.DataCtnLoggingInterval = saveData.DataCtnLoggingInterval;
                this.DataAvgLoggingInterval = saveData.DataAvgLoggingInterval;
                this.DataConstantLoggingInterval = saveData.DataConstantLoggingInterval;
                this.DataMaxRowsPerFile = saveData.DataMaxRowsPerFile;
                this.glHour = saveData.glHour;
                this.glMinute = saveData.glMinute;
                this.glSecond = saveData.glSecond;
            }

        }
    }



}
