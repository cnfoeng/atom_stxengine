﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtomUI.Class
{
    public class SerialParsing
    {
        public static bool AddChannels(HardWareType hwType)
        {
            try
            {
                if (hwType == HardWareType.SmokeMeter)
                    return AddSMChannels();
                else if (hwType == HardWareType.FuelMeter)
                    return AddFMChannels();
                else if (hwType == HardWareType.BlowByMeter)
                    return AddBBChannels();
            }
            catch
            {
            }
            return false;
        }

        public static bool AddSMChannels()
        {
            try
            {
                int Index = 0;
                acsChannelInfo channelItem;
                VariableInfo varItem;

                acsModuleInfo item = new acsModuleInfo("SM", "SM AVL 415S", 0, 0, ChannelIO.Input, ChannelType.Analog, 0, 0, 6);
                item.UseFor = ChannelUseFor.SmokeMeter;
                Ethercat.sysInfo.SMModuleInfos.Add(item);
                List<string> varlist = new List<string> { "SMAverage", "SM Measure 1", "SM Measure 2", "SM Measure 3", "SM Measure 4", "SM Measure 5" };
                Ethercat.chInfo.SmokeMeter_Channels.Clear();
                DaqData.sysVarInfo.SmokeMeter_Variables.Clear();

                foreach (string SMChannel in varlist)
                {
                    channelItem = new acsChannelInfo(item, 0, Index, SMChannel, 0);
                    Ethercat.chInfo.SmokeMeter_Channels.Add(channelItem);
                    varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
                    varItem.Unit = "";
                    varItem.Index = Index;
                    DaqData.sysVarInfo.SmokeMeter_Variables.Add(varItem);
                    Index++;
                }

                return true;
            }
            catch (Exception ex)
            {
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n AddSMChannels Error!!");
                return false;
            }
        }
        public static bool AddBBChannels()
        {
            try
            {
                //int Index = 0;
                //acsChannelInfo channelItem;
                //VariableInfo varItem;

                //acsModuleInfo item = new acsModuleInfo("SM", "SM AVL 415S", 0, 0, ChannelIO.Input, ChannelType.Analog, 0, 0, 6);
                //item.UseFor = ChannelUseFor.BlowByMeter;
                //Ethercat.sysInfo.BBModuleInfos.Add(item);
                //List<string> varlist = new List<string> { "SMAverage", "SM Measure 1", "SM Measure 2", "SM Measure 3", "SM Measure 4", "SM Measure 5" };
                //Ethercat.chInfo.BlowByMeter_Channels.Clear();
                //DaqData.sysVarInfo.BlowByMeter_Variables.Clear();

                //foreach (string SMChannel in varlist)
                //{
                //    channelItem = new acsChannelInfo(item, 0, Index, SMChannel, 0);
                //    Ethercat.chInfo.BlowByMeter_Channels.Add(channelItem);
                //    varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
                //    varItem.Unit = "FSN";
                //    varItem.Index = Index;
                //    DaqData.sysVarInfo.BlowByMeter_Variables.Add(varItem);
                //    Index++;
                //}

                return true;
            }
            catch (Exception ex)
            {
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n AddBBChannels Error!!");
                return false;
            }
        }
        public static bool AddFMChannels()
        {
            try
            {
                int Index = 0;
                acsChannelInfo channelItem;
                VariableInfo varItem;

                acsModuleInfo item = new acsModuleInfo("FM", "FM AVL 433S", 0, 0, ChannelIO.Input, ChannelType.Analog, 0, 0, 6);
                item.UseFor = ChannelUseFor.FuelMeter;
                Ethercat.sysInfo.FMModuleInfos.Add(item);
                //List<string> varlist = new List<string> { "FM MeasureNo,Count", "FM Mean Comsumption,kg/h", "FM Avg Time,sec", "FM Meas. Weight,g", "FM Maximum Value,kg/h", "FM Minimum Value,kg/h", "FM STD Dev.,kg/h" };
                List<string> varlist = new List<string> { "FM Mean Comsumption,kg/h", "FM Avg Time,sec", "FM Meas. Weight,g"};
                Ethercat.chInfo.FuelMeter_Channels.Clear();
                DaqData.sysVarInfo.FuelMeter_Variables.Clear();

                foreach (string SMChannelData in varlist)
                {
                    string[] SMChannel = SMChannelData.Split(',');
                    channelItem = new acsChannelInfo(item, 0, Index, SMChannel[0], 0);
                    Ethercat.chInfo.FuelMeter_Channels.Add(channelItem);
                    varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
                    varItem.Unit = SMChannel[1];
                    varItem.Index = Index;
                    DaqData.sysVarInfo.FuelMeter_Variables.Add(varItem);
                    Index++;
                }

                return true;
            }
            catch (Exception ex)
            {
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n AddFMChannels Error!!");
                return false;
            }
        }
    }
}
