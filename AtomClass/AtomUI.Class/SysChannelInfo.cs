﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace AtomUI.Class
{
    [Serializable]
    public class SysChannelInfo
    {
        public List<acsChannelInfo> All_Channels = new List<acsChannelInfo>();

        public List<acsChannelInfo> HMI_Channels = new List<acsChannelInfo>();
        public List<acsChannelInfo> DI_Channels = new List<acsChannelInfo>();
        public List<acsChannelInfo> AI_Channels = new List<acsChannelInfo>();
        public List<acsChannelInfo> DO_Channels = new List<acsChannelInfo>();
        public List<acsChannelInfo> AO_Channels = new List<acsChannelInfo>();
        public List<acsChannelInfo> MEXA9000_Channels = new List<acsChannelInfo>();
        public List<acsChannelInfo> ECU_Channels = new List<acsChannelInfo>();
        public List<acsChannelInfo> ModBus_Channels = new List<acsChannelInfo>();
        public List<acsChannelInfo> SmokeMeter_Channels = new List<acsChannelInfo>();
        public List<acsChannelInfo> BlowByMeter_Channels = new List<acsChannelInfo>();
        public List<acsChannelInfo> FuelMeter_Channels = new List<acsChannelInfo>();
        public List<acsChannelInfo> Osiris_Channels = new List<acsChannelInfo>();

        //public static Dictionary<int, acsModuleInfo> HMIModuleInfos = new Dictionary<int, acsModuleInfo>();
        //public static Dictionary<int, acsModuleInfo> ModuleInfos = new Dictionary<int, acsModuleInfo>();
        //public static Dictionary<int, acsChannelInfo> HMI_Channels = new Dictionary<int, acsChannelInfo>();
        //public static Dictionary<int, acsChannelInfo> DI_Channels = new Dictionary<int, acsChannelInfo>();
        //public static Dictionary<int, acsChannelInfo> AI_Channels = new Dictionary<int, acsChannelInfo>();
        //public static Dictionary<int, acsChannelInfo> DO_Channels = new Dictionary<int, acsChannelInfo>();
        //public static Dictionary<int, acsChannelInfo> AO_Channels = new Dictionary<int, acsChannelInfo>();

        public SysChannelInfo()
        { }


        public List<acsChannelInfo> getChannels(int Index)
        {
            List<acsChannelInfo> Channels = null;

            switch ((VariableType)Index)
            {
                case VariableType.HMI_IO:
                    Channels = HMI_Channels;
                    break;
                case VariableType.HMI_VAR:
                    Channels = null;
                    break;
                case VariableType.ACS_DI:
                    Channels = DI_Channels;
                    break;
                case VariableType.ACS_AI:
                    Channels = AI_Channels;
                    break;
                case VariableType.ACS_DO:
                    Channels = DO_Channels;
                    break;
                case VariableType.ACS_AO:
                    Channels = AO_Channels;
                    break;
                case VariableType.Mexa9000:
                    Channels = MEXA9000_Channels;
                    break;
                case VariableType.ECU:
                    Channels = ECU_Channels;
                    break;
                case VariableType.Modbus:
                    Channels = ModBus_Channels;
                    break;
                case VariableType.SmokeMeter:
                    Channels = SmokeMeter_Channels;
                    break;
                case VariableType.BlowByMeter:
                    Channels = BlowByMeter_Channels;
                    break;
                case VariableType.FuelMeter:
                    Channels = FuelMeter_Channels;
                    break;
                case VariableType.OSIRIS:
                    Channels = Osiris_Channels;
                    break;
                case VariableType.Calculation:
                    Channels = null;
                    break;
            }

            return Channels;

        }

        public void RenewAllChannels()
        {
            All_Channels.Clear();

            foreach (acsChannelInfo var in HMI_Channels)
            {
                All_Channels.Add(var);
            }
            foreach (acsChannelInfo var in DI_Channels)
            {
                All_Channels.Add(var);
            }
            foreach (acsChannelInfo var in AI_Channels)
            {
                All_Channels.Add(var);
            }
            foreach (acsChannelInfo var in DO_Channels)
            {
                All_Channels.Add(var);
            }
            foreach (acsChannelInfo var in AO_Channels)
            {
                All_Channels.Add(var);
            }
            foreach (acsChannelInfo var in MEXA9000_Channels)
            {
                All_Channels.Add(var);
            }
            foreach (acsChannelInfo var in ECU_Channels)
            {
                All_Channels.Add(var);
            }
            foreach (acsChannelInfo var in ModBus_Channels)
            {
                All_Channels.Add(var);
            }
            foreach (acsChannelInfo var in BlowByMeter_Channels)
            {
                All_Channels.Add(var);
            }
            foreach (acsChannelInfo var in FuelMeter_Channels)
            {
                All_Channels.Add(var);
            }
            foreach (acsChannelInfo var in SmokeMeter_Channels)
            {
                All_Channels.Add(var);
            }
            foreach (acsChannelInfo var in Osiris_Channels)
            {
                All_Channels.Add(var);
            }
        }
        public void ClearLists()
        {
            All_Channels.Clear();
            HMI_Channels.Clear();
            DI_Channels.Clear();
            AI_Channels.Clear();
            DO_Channels.Clear();
            AO_Channels.Clear();
            MEXA9000_Channels.Clear();
            ECU_Channels.Clear();
            ModBus_Channels.Clear();
            SmokeMeter_Channels.Clear();
            FuelMeter_Channels.Clear();
            BlowByMeter_Channels.Clear();
            Osiris_Channels.Clear();
        }

        public void Save(string FileName)
        {
            All_Channels.Clear();

            FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write);
            XmlSerializer xs = new XmlSerializer(this.GetType());
            xs.Serialize(fs, this);
            fs.Close();

            RenewAllChannels();
        }

        public void Load(string FileName)
        {
            this.ClearLists();
            FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
            XmlSerializer xs = new XmlSerializer(this.GetType());
            Object dd = xs.Deserialize(fs);
            fs.Close();
            SysChannelInfo chinfo = new SysChannelInfo();
            chinfo = (SysChannelInfo)dd;
            this.HMI_Channels = chinfo.HMI_Channels;
            this.DI_Channels = chinfo.DI_Channels;
            this.AI_Channels = chinfo.AI_Channels;
            this.DO_Channels = chinfo.DO_Channels;
            this.AO_Channels = chinfo.AO_Channels;
            this.MEXA9000_Channels = chinfo.MEXA9000_Channels;
            this.ECU_Channels = chinfo.ECU_Channels;
            this.ModBus_Channels = chinfo.ModBus_Channels;
            this.SmokeMeter_Channels = chinfo.SmokeMeter_Channels;
            this.FuelMeter_Channels = chinfo.FuelMeter_Channels;
            this.BlowByMeter_Channels = chinfo.BlowByMeter_Channels;
            this.Osiris_Channels = chinfo.BlowByMeter_Channels;

            RenewAllChannels();

            chinfo = null;
        }
    }

    [Serializable]
    public class acsChannelInfo
    {
        public acsModuleInfo ModuleInfo;
        public int ChannelBit;
        public int ChannelIndex;
        public int Offset;
        public int ValueType = 1;
        public string VariableName;

        public acsChannelInfo()
        { }

        public acsChannelInfo(acsModuleInfo ModuleInfo, int ChannelBit, int ChannelIndex, string VariableName, int Offset)
        {
            this.ModuleInfo = ModuleInfo;
            this.ChannelBit = ChannelBit;
            this.ChannelIndex = ChannelIndex;
            this.VariableName = VariableName;
            this.Offset = Offset;
        }
    }
}
