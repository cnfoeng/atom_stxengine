﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace AtomUI.Class
{

    [Serializable]
    public class SysMapInfo
    {
        public List<MappingInfo> HMI_IOMaps = new List<MappingInfo>();

        public List<MappingInfo> HMI_Maps = new List<MappingInfo>();

        public List<MappingInfo> DI_Maps = new List<MappingInfo>();
        public List<MappingInfo> AI_Maps = new List<MappingInfo>();
        public List<MappingInfo> DO_Maps = new List<MappingInfo>();
        public List<MappingInfo> AO_Maps = new List<MappingInfo>();

        public List<MappingInfo> Calc_Maps = new List<MappingInfo>();

        public SysMapInfo()
        { }

        public void ClearLists()
        {
            HMI_IOMaps.Clear();
            HMI_Maps.Clear();
            DI_Maps.Clear();
            AI_Maps.Clear();
            DO_Maps.Clear();
            AO_Maps.Clear();
            Calc_Maps.Clear();
        }

        public void Save(string FileName)
        {
            FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write);
            XmlSerializer xs = new XmlSerializer(this.GetType());
            xs.Serialize(fs, this);
            fs.Close();
        }

        public void Load(string FileName)
        {
            this.ClearLists();
            FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
            XmlSerializer xs = new XmlSerializer(this.GetType());
            Object dd = xs.Deserialize(fs);
            fs.Close();
            SysMapInfo mapinfo = new SysMapInfo();
            mapinfo = (SysMapInfo)dd;
            this.HMI_IOMaps = mapinfo.HMI_IOMaps;
            this.HMI_Maps = mapinfo.HMI_Maps;
            this.DI_Maps = mapinfo.DI_Maps;
            this.AI_Maps = mapinfo.AI_Maps;
            this.DO_Maps = mapinfo.DO_Maps;
            this.AO_Maps = mapinfo.AO_Maps;
            this.Calc_Maps = mapinfo.Calc_Maps;
            mapinfo = null;
        }
    }

    [Serializable]
    public class MappingInfo
    {
        public VariableInfo Variable = new VariableInfo();
        public SysChannelInfo Channel = new SysChannelInfo();
        public int Index;

        public MappingInfo()
        { }
        public MappingInfo(int Index, VariableInfo Variable, SysChannelInfo Channel)
        {
            this.Variable = Variable;
            this.Channel = Channel;
        }
    }
}
