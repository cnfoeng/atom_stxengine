﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;
using System.Data;
using System.Diagnostics;
using System.ComponentModel;

namespace AtomUI.Class
{
    public enum CalibrationType
    {
        CalByValue = 0,
        CalByTable = 1
    }

    public enum VariableType
    {
        HMI_IO = 0,
        HMI_VAR = 1,
        ACS_DI = 2,
        ACS_AI = 3,
        ACS_DO = 4,
        ACS_AO = 5,
        ECU = 6,
        Modbus = 7,
        Mexa9000 = 8,
        SmokeMeter = 9,
        FuelMeter = 10,
        BlowByMeter = 11,
        OSIRIS = 12,
        Calculation = 13
    }

    [Serializable]
    public class SysVarInfo

    {
        public int DataLoggingInterval = 100;
        public bool DataLoggingIntervalData = false;

        public List<VariableInfo> ALL_Variables = new List<VariableInfo>();

        public List<VariableInfo> FrontPanel_Variables_Output = new List<VariableInfo>();
        public List<VariableInfo> FrontPanel_Variables_Input = new List<VariableInfo>();
        //public List<VariableInfo> DataLogging_Variables = new List<VariableInfo>();
        public List<VariableInfo> InstantDataLogging_Variables = new List<VariableInfo>();
        public List<VariableInfo> ConstantDataLogging_Variables = new List<VariableInfo>();

        public List<VariableInfo> HMI_IOVariables = new List<VariableInfo>();
        public List<VariableInfo> HMI_Variables = new List<VariableInfo>();

        public List<VariableInfo> DI_Variables = new List<VariableInfo>();
        public List<VariableInfo> AI_Variables = new List<VariableInfo>();
        public List<VariableInfo> DO_Variables = new List<VariableInfo>();
        public List<VariableInfo> AO_Variables = new List<VariableInfo>();

        public List<VariableInfo> SmokeMeter_Variables = new List<VariableInfo>();
        public List<VariableInfo> BlowByMeter_Variables = new List<VariableInfo>();
        public List<VariableInfo> FuelMeter_Variables = new List<VariableInfo>();
        public List<VariableInfo> Osiris_Variables = new List<VariableInfo>();

        public List<VariableInfo> MEXA9000_Variables = new List<VariableInfo>();
        public List<VariableInfo> ECU_Variables = new List<VariableInfo>();
        public List<VariableInfo> ModBus_Variables = new List<VariableInfo>();

        public List<VariableInfo> CalcRef_Variables = new List<VariableInfo>();
        public List<VariableInfo> Calc_Variables = new List<VariableInfo>();

        public DataTable table = new DataTable();

        public string getPredefineLabel(VariableInfo var)
        {
            string pre = "";

            foreach (VariableInfo IOvar in HMI_IOVariables)
            {
                if (var == IOvar)
                    return VariableType.HMI_IO.ToString();
            }
            foreach (VariableInfo IOvar in HMI_Variables)
            {
                if (var == IOvar)
                    return VariableType.HMI_VAR.ToString();
            }

            foreach (VariableInfo IOvar in DI_Variables)
            {
                if (var == IOvar)
                    return VariableType.ACS_DI.ToString();
            }

            foreach (VariableInfo IOvar in AI_Variables)
            {
                if (var == IOvar)
                    return VariableType.ACS_AI.ToString();
            }

            foreach (VariableInfo IOvar in DO_Variables)
            {
                if (var == IOvar)
                    return VariableType.ACS_DO.ToString();
            }

            foreach (VariableInfo IOvar in AO_Variables)
            {
                if (var == IOvar)
                    return VariableType.ACS_AO.ToString();
            }

            foreach (VariableInfo IOvar in MEXA9000_Variables)
            {
                if (var == IOvar)
                    return VariableType.Mexa9000.ToString();
            }

            foreach (VariableInfo IOvar in ECU_Variables)
            {
                if (var == IOvar)
                    return VariableType.ECU.ToString();
            }

            foreach (VariableInfo IOvar in ModBus_Variables)
            {
                if (var == IOvar)
                    return VariableType.Modbus.ToString();
            }
            foreach (VariableInfo IOvar in SmokeMeter_Variables)
            {
                if (var == IOvar)
                    return VariableType.SmokeMeter.ToString();
            }
            foreach (VariableInfo IOvar in BlowByMeter_Variables)
            {
                if (var == IOvar)
                    return VariableType.BlowByMeter.ToString();
            }
            foreach (VariableInfo IOvar in FuelMeter_Variables)
            {
                if (var == IOvar)
                    return VariableType.FuelMeter.ToString();
            }
            foreach (VariableInfo IOvar in Osiris_Variables)
            {
                if (var == IOvar)
                    return VariableType.OSIRIS.ToString();
            }
            foreach (VariableInfo IOvar in Calc_Variables)
            {
                if (var == IOvar)
                    return VariableType.Calculation.ToString();
            }

            return pre;

        }

        public List<VariableInfo> getVariables(int Index)
        {
            List<VariableInfo> Variables = null;

            switch ((VariableType)Index)
            {
                case VariableType.HMI_IO:
                    Variables = HMI_IOVariables;
                    break;
                case VariableType.HMI_VAR:
                    Variables = HMI_Variables;
                    break;
                case VariableType.ACS_DI:
                    Variables = DI_Variables;
                    break;
                case VariableType.ACS_AI:
                    Variables = AI_Variables;
                    break;
                case VariableType.ACS_DO:
                    Variables = DO_Variables;
                    break;
                case VariableType.ACS_AO:
                    Variables = AO_Variables;
                    break;
                case VariableType.Mexa9000:
                    Variables = MEXA9000_Variables;
                    break;
                case VariableType.ECU:
                    Variables = ECU_Variables;
                    break;
                case VariableType.Modbus:
                    Variables = ModBus_Variables;
                    break;
                case VariableType.SmokeMeter:
                    Variables = SmokeMeter_Variables;
                    break;
                case VariableType.BlowByMeter:
                    Variables = BlowByMeter_Variables;
                    break;
                case VariableType.FuelMeter:
                    Variables = FuelMeter_Variables;
                    break;
                case VariableType.OSIRIS:
                    Variables = Osiris_Variables;
                    break;
                case VariableType.Calculation:
                    Variables = Calc_Variables;
                    break;
            }

            return Variables;

        }

        public void RenewAllvariables()
        {
            try
            {
                ALL_Variables.Clear();

                foreach (VariableInfo var in HMI_IOVariables)
                {
                    ALL_Variables.Add(var);
                }
                foreach (VariableInfo var in HMI_Variables)
                {
                    ALL_Variables.Add(var);
                }
                foreach (VariableInfo var in DI_Variables)
                {
                    ALL_Variables.Add(var);
                }
                foreach (VariableInfo var in AI_Variables)
                {
                    ALL_Variables.Add(var);
                }
                foreach (VariableInfo var in DO_Variables)
                {
                    ALL_Variables.Add(var);
                }
                foreach (VariableInfo var in AO_Variables)
                {
                    ALL_Variables.Add(var);
                }
                foreach (VariableInfo var in MEXA9000_Variables)
                {
                    ALL_Variables.Add(var);
                }
                foreach (VariableInfo var in ECU_Variables)
                {
                    ALL_Variables.Add(var);
                }
                foreach (VariableInfo var in ModBus_Variables)
                {
                    ALL_Variables.Add(var);
                }
                foreach (VariableInfo var in SmokeMeter_Variables)
                {
                    ALL_Variables.Add(var);
                }
                foreach (VariableInfo var in BlowByMeter_Variables)
                {
                    ALL_Variables.Add(var);
                }
                foreach (VariableInfo var in FuelMeter_Variables)
                {
                    ALL_Variables.Add(var);
                }
                foreach (VariableInfo var in Osiris_Variables)
                {
                    ALL_Variables.Add(var);
                }
                foreach (VariableInfo var in Calc_Variables)
                {
                    ALL_Variables.Add(var);
                }

                // TODO : BlowBy 단위가 1/10으로 나옴. 10배할 것.
                // TODO : 소수점 자리 지정.

                // TODO : Smoke Meter 저장이 안됨.

                // TODO : 저장 변수 재지정 필요없도록 수정
                foreach (VariableInfo var in ALL_Variables)
                {
                    if (var.ChannelInfo != null)
                    {
                        foreach (acsChannelInfo varTarget in Ethercat.chInfo.All_Channels)
                        {

                            //Debug.WriteLine(var.ChannelInfo.ModuleInfo.SlaveNo + " - " + varTarget.ModuleInfo.SlaveNo + " - " + var.ChannelInfo.ChannelIndex + " - " + varTarget.ChannelIndex);
                            if (var.ChannelInfo != null && var.ChannelInfo.ModuleInfo.ProductID == varTarget.ModuleInfo.ProductID && var.ChannelInfo.ChannelIndex == varTarget.ChannelIndex
                                 && var.ChannelInfo.ValueType == varTarget.ValueType && var.ChannelInfo.VariableName == varTarget.VariableName
                                 && var.ChannelInfo.Offset == varTarget.Offset && var.ChannelInfo.ChannelBit == varTarget.ChannelBit
                                 && var.ChannelInfo.ModuleInfo.SlaveNo == varTarget.ModuleInfo.SlaveNo)
                            {
                                var.ChannelInfo = varTarget;
                                break;
                            }
                        }

                    }
                    //for (int ind = 0; ind < DataLogging_Variables.Count; ind++)
                    //{
                    //    if (var.NorminalName == DataLogging_Variables[ind].NorminalName)
                    //    {
                    //        DataLogging_Variables[ind] = var;
                    //    }
                    //}

                    for (int ind = 0; ind < InstantDataLogging_Variables.Count; ind++)
                    {
                        if (var.NorminalName == InstantDataLogging_Variables[ind].NorminalName)
                        {
                            InstantDataLogging_Variables[ind] = var;
                        }
                    }
                    for (int ind = 0; ind < ConstantDataLogging_Variables.Count; ind++)
                    {
                        if (var.NorminalName == ConstantDataLogging_Variables[ind].NorminalName)
                        {
                            ConstantDataLogging_Variables[ind] = var;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n connect Error!!");
            }
        }

        public SysVarInfo()
        { }

        public void ClearLists()
        {
            ALL_Variables.Clear();

            FrontPanel_Variables_Output.Clear();
            FrontPanel_Variables_Input.Clear();
            //DataLogging_Variables.Clear();
            InstantDataLogging_Variables.Clear();
            ConstantDataLogging_Variables.Clear();

            HMI_IOVariables.Clear();
            HMI_Variables.Clear();

            MEXA9000_Variables.Clear();
            ECU_Variables.Clear();
            ModBus_Variables.Clear();
            SmokeMeter_Variables.Clear();
            BlowByMeter_Variables.Clear();
            FuelMeter_Variables.Clear();
            Osiris_Variables.Clear();

            DI_Variables.Clear();
            AI_Variables.Clear();
            DO_Variables.Clear();
            AO_Variables.Clear();
            Calc_Variables.Clear();
        }

        public void Save(string FileName)
        {
            table = null;

            FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write);
            XmlSerializer xs = new XmlSerializer(this.GetType());
            xs.Serialize(fs, this);
            fs.Close();
        }

        private void AddAllvariables(List<VariableInfo> newVariables)
        {
            foreach(VariableInfo var in newVariables)
            {
                ALL_Variables.Add(var);
            }
        }

        public void Load(string FileName)
        {
            FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
            XmlSerializer xs = new XmlSerializer(this.GetType());
            Object dd = xs.Deserialize(fs);
            fs.Close();
            SysVarInfo varinfo = new SysVarInfo();
            varinfo = (SysVarInfo)dd;

            this.DataLoggingInterval = varinfo.DataLoggingInterval;

            this.HMI_IOVariables = varinfo.HMI_IOVariables;
            this.HMI_Variables = varinfo.HMI_Variables;

            this.DI_Variables = varinfo.DI_Variables;
            this.AI_Variables = varinfo.AI_Variables;
            this.DO_Variables = varinfo.DO_Variables;
            this.AO_Variables = varinfo.AO_Variables;

            this.MEXA9000_Variables = varinfo.MEXA9000_Variables;
            this.ECU_Variables = varinfo.ECU_Variables;
            this.ModBus_Variables = varinfo.ModBus_Variables;
            this.SmokeMeter_Variables = varinfo.SmokeMeter_Variables;
            this.BlowByMeter_Variables = varinfo.BlowByMeter_Variables;
            this.FuelMeter_Variables = varinfo.FuelMeter_Variables;
            this.Osiris_Variables = varinfo.Osiris_Variables;

            //this.DataLogging_Variables = varinfo.DataLogging_Variables;
            this.InstantDataLogging_Variables = varinfo.InstantDataLogging_Variables;
            this.ConstantDataLogging_Variables = varinfo.ConstantDataLogging_Variables;

            RenewAllvariables();

            if (varinfo.CalcRef_Variables != null)
            {
                this.CalcRef_Variables = varinfo.CalcRef_Variables;
                MatchingVariables(CalcRef_Variables);
            }

            RenewDataTable();

            //this.Calc_Variables = varinfo.Calc_Variables;
            //MatchingCalcVariables(Calc_Variables);

            this.FrontPanel_Variables_Output = varinfo.FrontPanel_Variables_Output;
            MatchingVariables(FrontPanel_Variables_Output);
            this.FrontPanel_Variables_Input = varinfo.FrontPanel_Variables_Input;
            MatchingVariables(FrontPanel_Variables_Input);
            //this.InstantDataLogging_Variables = varinfo.DataLogging_Variables;
            //MatchingVariables(DataLogging_Variables);
            this.InstantDataLogging_Variables = varinfo.InstantDataLogging_Variables;
            MatchingVariables(InstantDataLogging_Variables);
            this.ConstantDataLogging_Variables = varinfo.ConstantDataLogging_Variables;
            MatchingVariables(ConstantDataLogging_Variables);


            varinfo = null;

        }

        public void RenewDataTable()
        {
            table = new DataTable();
            table.TableName = "Calculation";
            foreach (VariableInfo var in CalcRef_Variables)
            {
                table.Columns.Add(var.abbreviation, typeof(double));
            }

            foreach (VariableInfo var in Calc_Variables)
            {
                DataColumn col = table.Columns.Add(var.VariableName, typeof(double));
                col.Expression = var.Formula;
            }

            DataRow row = table.Rows.Add();
            RenewDataValue();
        }

        public void RenewDataValue()
        {
            DataRow row = table.Rows[0];
            foreach (VariableInfo var in CalcRef_Variables)
            {
                if (var.abbreviation != null && var.abbreviation.Length > 0 && table.Columns.Contains(var.abbreviation))
                {
                    row[var.abbreviation] = var.RealValue;
                }
            }

            table.BeginLoadData();
            table.EndLoadData();
            //double test = (double)row[1];
            //Debug.WriteLine(row[1]);
        }

        private void MatchingVariables(List<VariableInfo> varList)
        {
            for (int i= 0; i< varList.Count; i++)
            {
                VariableInfo var = varList[i];
                foreach (VariableInfo varTarget in ALL_Variables)
                {
                    if (var.VariableName == varTarget.VariableName && var.Unit == varTarget.Unit && var.NorminalName == varTarget.NorminalName)
                    {
                        varList[i] = varTarget;
                        break;
                    }
                }

            }

        }

        //private void MatchingCalcVariables(List<VariableInfo> varList)
        //{
        //    foreach (VariableInfo varCalc in varList)
        //    {
        //        for (int i = 0; i < varCalc.CalcVariables.Count; i++)
        //        {
        //            VariableInfo var = varCalc.CalcVariables[i];

        //            foreach (VariableInfo varTarget in CalcRef_Variables)
        //            {
        //                if (var.VariableName == varTarget.VariableName && var.Unit == varTarget.Unit && var.NorminalName == varTarget.NorminalName)
        //                {
        //                    varCalc.CalcVariables[i] = varTarget;
        //                    break;
        //                }
        //            }
        //        }
        //    }
        //}

        public void SaveVariables(string FileName, List<VariableInfo> Variables)
        {
            ALL_Variables.Clear();
            FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write);
            XmlSerializer xs = new XmlSerializer(Variables.GetType());
            xs.Serialize(fs, Variables);
            fs.Close();

            RenewAllvariables();

        }

        public List<VariableInfo> LoadVariables(string FileName)
        {
            List<VariableInfo> Variables = new List<VariableInfo>();
            try
            {
                FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
                XmlSerializer xs = new XmlSerializer(Variables.GetType());
                Object dd = xs.Deserialize(fs);
                fs.Close();
                Variables = (List<VariableInfo>)dd;
                RenewAllvariables();
            }
            catch {
                MessageBox.Show("File Format Not Matched. Try other file.");
            }
            return Variables;
        }
    }

    [Serializable]
    public class ValuePair
    {
        public double IOValue = 0;
        public double RealValue = 0.0;
        public ValuePair()
        {
        }

        public ValuePair(double IOValue, double RealValue)
        {
            this.IOValue = IOValue;
            this.RealValue = RealValue;
        }
    }

    public enum AlarmReaction
    {
        Warninng = 0,
        Alarm = 1,
        Stop = 2
    }

    [Serializable]
    [DefaultPropertyAttribute("VariableName")]
    public class VariableInfo
    {
        private double _IOValue = 0.0;
        private string _Unit;
        private string _Formula;
        private string _VariableName;
        public string _NorminalName;
        private string _NorminalDescription;

        private double _Range_High = 0;
        private double _Range_Low = 0;

        private bool _AlarmUse;
        private bool _WarningUse;

        private double _Warning_Low;
        private double _Warning_High;
        private double _Alarm_Low;
        private double _Alarm_High;
        private int _Round = 2;
        private double _Offset = 0;
        private double _Factor = 1;
        private CalibrationType _CalType = CalibrationType.CalByValue;

        [CategoryAttribute("Value"), DescriptionAttribute("Calibration Type. by Table or by Value")]
        public CalibrationType CalType
        {
            get { return _CalType; }
            set { _CalType = value; }
        }

        [CategoryAttribute("Value"), DescriptionAttribute("Converting Offset for Real Value")]
        public double Offset
        {
            get { return _Offset; }
            set { _Offset = value; }
        }
        [CategoryAttribute("Value"), DescriptionAttribute("Converting Factor for Real Value")]
        public double Factor
        {
            get { return _Factor; }
            set { _Factor = value; }
        }
        [CategoryAttribute("Limit"), DescriptionAttribute("Warning Limit at Low Level")]
        public double Warning_Low
        {
            get { return _Warning_Low; }
            set { _Warning_Low = value; }
        }

        [CategoryAttribute("Limit"), DescriptionAttribute("Warning Limit at High Level")]
        public double Warning_High
        {
            get { return _Warning_High; }
            set { _Warning_High = value; }
        }

        [CategoryAttribute("Limit"), DescriptionAttribute("Alarm Limit at Low Level")]
        public double Alarm_Low
        {
            get { return _Alarm_Low; }
            set { _Alarm_Low = value; }
        }

        [CategoryAttribute("Limit"), DescriptionAttribute("Alarm Limit at High Level")]
        public double Alarm_High
        {
            get { return _Alarm_High; }
            set { _Alarm_High = value; }
        }

        [CategoryAttribute("Limit"), DescriptionAttribute("Minumum Value")]
        public double Range_Low
        {
            get { return _Range_Low; }
            set { _Range_Low = value; }
        }

        [CategoryAttribute("Limit"), DescriptionAttribute("Maximum Value")]
        public double Range_High
        {
            get { return _Range_High; }
            set { _Range_High = value; }
        }

        [CategoryAttribute("Limit"), DescriptionAttribute("Use Alarm Limit")]
        public bool AlarmUse
        {
            get { return _AlarmUse; }
            set { _AlarmUse = value; }
        }

        [CategoryAttribute("Limit"), DescriptionAttribute("Use Warning Limit")]
        public bool WarningUse
        {
            get { return _WarningUse; }
            set { _WarningUse = value; }
        }

        [CategoryAttribute("Behavior"), DescriptionAttribute("Round Number below decimal point.")]
        public int Round
        {
            get { return _Round; }
            set { _Round = value; }
        }

        [CategoryAttribute("Basic Property"), DescriptionAttribute("Alarm Limit at High Level")]
        public string VariableName
        {
            get { return _VariableName; }
            set { _VariableName = value; }
        }

        [CategoryAttribute("Basic Property"), DescriptionAttribute("Norminal Name for the Variable. Read Only")]
        public string NorminalName
        {
            get { return _NorminalName; }
        }
        [CategoryAttribute("Basic Property"), DescriptionAttribute("Description for the Variable. Read Only")]
        public string NorminalDescription
        {
            get { return _NorminalDescription; }
            set { _NorminalDescription = value; }
        }
        [CategoryAttribute("Behavior"), DescriptionAttribute("Calculation Formula for Calculating Variable. Not Permitable For Other Type Variable")]
        public string Formula
        {
            get { return _Formula; }
            set { _Formula = value; }
        }
        [CategoryAttribute("Basic Property"), DescriptionAttribute("Unit of the Variable. ")]
        public string Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }
        [CategoryAttribute("Behavior"), DescriptionAttribute("If true, Calculating Average Value. ")]
        public bool Averaging
        {
            get
            {
                return _Averaging;
            }
            set
            {
                _Averaging = value;
                if (!value)
                {
                    _IOValueList.Clear();
                }
            }
        }
        [CategoryAttribute("Behavior"), DescriptionAttribute("Averaging Count")]
        public int AveragingCount
        {
            get
            {
                return _AveragingCount;
            }
            set
            {
                _AveragingCount = value;
                if (value < 2)
                {
                    _Averaging = false;
                }
                _IOValueList.Clear();
            }
        }

        public acsChannelInfo ChannelInfo;

        public int Tag;
        public int Index;
        public bool Removable;

        public int IOIndex;
        public bool _Averaging = false;
        public int _AveragingCount = 5;
        public List<ValuePair> CalibrationTable = new List<ValuePair>();



        public AlarmReaction Reaction;
        //public List<VariableInfo> CalcVariables = new List<VariableInfo>();
        public List<double> _IOValueList = new List<double>();
        public string abbreviation;



        [CategoryAttribute("Value"), DescriptionAttribute("Value by Hardware")]
        public double IOValue
        {
            get
            {
                if (!_Averaging || _IOValueList.Count == 0)
                    return _IOValue;
                else
                    return _IOValueList[0];
            }
            set
            {
                if (!_Averaging)
                    _IOValue = value;
                else
                    _IOValueList.Insert(0, value);
            }
        }


        [CategoryAttribute("Value"), DescriptionAttribute("Value by Calculating")]
        public double RealValue
        {
            get
            {
                double sum = 0;
                if (!_Averaging || _IOValueList.Count == 0)
                    return Math.Round(GetRealValue(_IOValue), Round);
                else
                {

                    int maxNum = (_IOValueList.Count > _AveragingCount ? _AveragingCount : _IOValueList.Count);
                    for (int i = 0; i < maxNum; i++)
                    {
                        sum += GetRealValue(_IOValueList[i]);
                    }

                    return Math.Round(sum / maxNum, Round);
                }
            }
        }

        public double GetRealValue(double IOValue)
        {
            double RValue = 0;

            if (ChannelInfo != null)
            {
                if (ChannelInfo.ModuleInfo.UseFor.ToString() == "HMI")//ACS
                {
                    if (CalibrationTable.Count == 0)
                    {
                        RValue = IOValue;
                    }
                    else if (CalibrationTable.Count == 1)//ValuePair=(Voltage.Max,PhysicalValue.Max)=(c,d)
                    {
                        RValue = (double)CalibrationTable[0].RealValue / (double)CalibrationTable[0].IOValue * (double)IOValue; // physical value = d / c * Rawdata.
                    }
                    else//2개인 경우. 첫번째 ValuePair=(Voltage.Max,PhysicalValue.Max)=(c,d). 두번째 ValuePair=(Voltage.Min,PhysicalValue.Min)=(a,c). 큰값을 먼저 ValuePair에 넣어주고 작은 값을 나중에 넣어주어야 함. 자동으로 바꾸게 할지는 보류.
                    {
                        RValue = ((double)CalibrationTable[0].RealValue - (double)CalibrationTable[1].RealValue) / ((double)CalibrationTable[0].IOValue - (double)CalibrationTable[1].IOValue) * ((double)IOValue - (double)CalibrationTable[1].IOValue) + (double)CalibrationTable[1].RealValue; // physical value = (d - b) / (c - a) * (Rawdata - a) + b
                    }
                }
                else if (ChannelInfo.ModuleInfo.UseFor.ToString() == "DAQ")//ACS
                {
                    if (_CalType == CalibrationType.CalByTable)
                    {
                        if (CalibrationTable.Count == 0)
                        {
                            RValue = IOValue;
                        }
                        else if (CalibrationTable.Count == 1)//ValuePair=(Voltage.Max,PhysicalValue.Max)=(c,d)
                        {
                            RValue = (double)CalibrationTable[0].RealValue / (double)CalibrationTable[0].IOValue * (double)IOValue; // physical value = d / c * Rawdata.
                        }
                        else//2개인 경우. 첫번째 ValuePair=(Voltage.Max,PhysicalValue.Max)=(c,d). 두번째 ValuePair=(Voltage.Min,PhysicalValue.Min)=(a,c). 큰값을 먼저 ValuePair에 넣어주고 작은 값을 나중에 넣어주어야 함. 자동으로 바꾸게 할지는 보류.
                        {
                            RValue = ((double)CalibrationTable[0].RealValue - (double)CalibrationTable[1].RealValue) / ((double)CalibrationTable[0].IOValue - (double)CalibrationTable[1].IOValue) * ((double)IOValue - (double)CalibrationTable[1].IOValue) + (double)CalibrationTable[1].RealValue; // physical value = (d - b) / (c - a) * (Rawdata - a) + b
                        }
                    } else
                    {
                        RValue = IOValue * _Factor + _Offset;
                    }
                }
                else if (ChannelInfo.ModuleInfo.UseFor.ToString() == "MEXA")//GPIB. 그대로 넣어줌.
                {
                    RValue = (IOValue * Factor) + Offset;
                }
                else if (ChannelInfo.ModuleInfo.UseFor.ToString() == "ECU")//CAN. dbc문서참고
                {
                    RValue = (IOValue * Factor) + Offset;
                }
                else if (ChannelInfo.ModuleInfo.UseFor.ToString() == "MODBUS")
                {
                    RValue = (IOValue * Factor) + Offset;
                }
                else
                {
                    RValue = (IOValue * Factor) + Offset;
                }
            }
            else
            {
                if (Formula !=null && Formula.Length > 0 && DaqData.sysVarInfo.table.Columns.Contains(VariableName))
                {
                    RValue = (double)DaqData.sysVarInfo.table.Rows[0][VariableName];
                }
                else
                    RValue = IOValue;
            }

            return RValue;
        }

        public VariableInfo Clone()
        {
            VariableInfo var = new VariableInfo();
            var.ChannelInfo = this.ChannelInfo;
            var.VariableName = this.VariableName;
            var._NorminalName = this.NorminalName;
            var.NorminalDescription = this.NorminalDescription;
            var.Tag = this.Tag;
            var.Index = this.Index;
            var.Removable = this.Removable;
            var.Formula = this.Formula;
            var.IOIndex = this.IOIndex;
            var.IOValue = this.IOValue;
            var.CalibrationTable = this.CalibrationTable;
            var.Unit = this.Unit;
            var.Warning_Low = this.Warning_Low;
            var.Warning_High = this.Warning_High;
            var.Alarm_Low = this.Alarm_Low;
            var.Alarm_High = this.Alarm_High;
            var.Reaction = this.Reaction;
            var.Round = this.Round;
            foreach(ValuePair vp in this.CalibrationTable)
            {
                var.CalibrationTable.Add(new ValuePair(vp.IOValue, vp.RealValue));
            }

            return var;
        }
        public VariableInfo()
        { }

        public VariableInfo(string VariableName, string NorminalName)
        {
            this._NorminalName = NorminalName;
            this.VariableName = VariableName;
        }
        public VariableInfo(string VariableName, string NorminalName, string NorminalDescription, int Tag, int Index)
        {
            this._NorminalName = NorminalName;
            this.NorminalDescription = NorminalDescription;
            this.VariableName = VariableName;
            this.Tag = Tag;
            this.Index = Index;
            this.Removable = false;
        }

        public VariableInfo(string VariableName, string NorminalName, string NorminalDescription, int Index)
        {
            this._NorminalName = NorminalName;
            this.NorminalDescription = NorminalDescription;
            this.VariableName = VariableName;
            this.Index = Index;
            this.Removable = false;
        }

        public VariableInfo(acsChannelInfo ChannelInfo, string VariableName, string NorminalName)
        {
            this.ChannelInfo = ChannelInfo;
            this._NorminalName = NorminalName;
            this.VariableName = VariableName;
            this.Removable = false;
        }

        public VariableInfo(string VariableName)
        {
            this.VariableName = VariableName;
            this.Removable = true;
        }
    }

}
