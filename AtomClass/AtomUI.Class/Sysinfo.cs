﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace AtomUI.Class
{
    public enum ChannelType
    {
        None = 0,
        Digital = 1,
        Analog = 2,
        Calculated = 3
    }

    public enum ChannelIO
    {
        None = 0,
        Input = 1,
        Output = 2
    }

    public enum ChannelUseFor
    {
        HMI = 0,
        DAQ = 1,
        MEXA = 2,
        ECU = 3,
        MODBUS = 4,
        SmokeMeter = 5,
        FuelMeter = 6,
        BlowByMeter = 7,
        OSIRIS = 8
    }

    public enum HardWareType
    {
        SERVER = 0,
        ACS = 1,
        MEXA = 2,
        MEXA_SERIAL = 8,
        ECU = 3,
        MODBUS = 4,
        SmokeMeter = 5,
        FuelMeter = 6,
        BlowByMeter = 7,
        OSIRIS = 9
    }

    [Serializable]
    public class SysInfo
    {
        public acsConnectionInfo ConnectionInfo = new acsConnectionInfo();
        public List<acsConnectionInfo> HardWareInfo = new List<acsConnectionInfo>();
        public List<acsModuleInfo> HMIModuleInfos = new List<acsModuleInfo>();
        public List<acsModuleInfo> ModuleInfos = new List<acsModuleInfo>();
        public List<acsModuleInfo> ECUModuleInfos = new List<acsModuleInfo>();
        public List<acsModuleInfo> ModBusModuleInfos = new List<acsModuleInfo>();
        public List<acsModuleInfo> MEXAModuleInfos = new List<acsModuleInfo>();
        public List<acsModuleInfo> SMModuleInfos = new List<acsModuleInfo>();
        public List<acsModuleInfo> BBModuleInfos = new List<acsModuleInfo>();
        public List<acsModuleInfo> FMModuleInfos = new List<acsModuleInfo>();
        public List<acsModuleInfo> OsirisModuleInfos = new List<acsModuleInfo>();
        public SysSettings sysSettings = new SysSettings();

        public SysInfo()
        {
            //ConnectionInfo.AutoConnect = false;
            //ConnectionInfo.ComType = 1;
            //ConnectionInfo.Baud_Rate = 0;
            //ConnectionInfo.CommPort = 0;
            //ConnectionInfo.ConnType = 1;
            //ConnectionInfo.RemoteAddress = "10.0.0.100";
            //ConnectionInfo.SlotNumber = "-1";
            //ConnectionInfo.Baud_RateValue = "AUTO";
        }

        public void ClearLists()
        {
            HMIModuleInfos.Clear();
            ModuleInfos.Clear();
        }

        public void Save(string FileName)
        {
            FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write);
            XmlSerializer xs = new XmlSerializer(this.GetType());
            xs.Serialize(fs, this);
            fs.Close();
        }

        public void Load(string FileName)
        {
            this.ClearLists();
            FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
            XmlSerializer xs = new XmlSerializer(this.GetType());
            Object dd = xs.Deserialize(fs);
            fs.Close();
            SysInfo sysinfo = new SysInfo();
            sysinfo = (SysInfo)dd;
            this.ConnectionInfo = sysinfo.ConnectionInfo;
            this.HardWareInfo = sysinfo.HardWareInfo;
            this.HMIModuleInfos = sysinfo.HMIModuleInfos;
            this.ModuleInfos = sysinfo.ModuleInfos;
            this.ECUModuleInfos = sysinfo.ECUModuleInfos;
            this.ModBusModuleInfos = sysinfo.ModBusModuleInfos;
            this.MEXAModuleInfos = sysinfo.MEXAModuleInfos;
            this.BBModuleInfos = sysinfo.BBModuleInfos;
            this.FMModuleInfos = sysinfo.FMModuleInfos;
            this.SMModuleInfos = sysinfo.SMModuleInfos; 
            this.sysSettings = sysinfo.sysSettings; 
             sysinfo = null;
        }

    }

    [Serializable]
    public class acsModuleInfo
    {
        public String ProductName;
        public String ProductDesc;
        public int VendorID;
        public int ProductID;
        public ChannelIO ChannelIO;
        public ChannelType ChannelType;
        public ChannelUseFor UseFor;
        public int OffsetBase;
        public int SlaveNo;
        public int ChannelEa;
        public List<String> Variables;

        public acsModuleInfo()
        { }

        public acsModuleInfo(String ProductName, String ProductDesc, int VendorID, int ProductID, ChannelIO ChannelIO, ChannelType ChannelType, int OffsetBase, int SlaveNo, int ChannelEa)
        {
            this.ProductName = ProductName;
            this.ProductDesc = ProductDesc;
            this.VendorID = VendorID;
            this.ProductID = ProductID;
            this.ChannelIO = ChannelIO;
            this.ChannelType = ChannelType;
            this.OffsetBase = OffsetBase;
            this.SlaveNo = SlaveNo;
            this.ChannelEa = ChannelEa;
        }
    }

    [Serializable]
    public class acsConnectionInfo
    {
        public HardWareType HWType;
        public bool HMIExist = false;
        public int ComType = -1;
        public int Baud_Rate = -1;
        public int CommPort = -1;
        public int ConnType = -1;
        public string RemoteAddress = "";
        public string SlotNumber = "";
        public string SlaveUnitIdentifier = "";
        public string Baud_RateValue = "";
        public bool AutoConnect = false;
        public string Description = "";

        public acsConnectionInfo()
        { }

        public acsConnectionInfo(int ComType, int Baud_Rate, int CommPort, int ConnType, string RemoteAddress, string SlotNumber, string Baud_RateValue, bool AutoConnect, bool HMIExist, string Description)
        {
            this.HWType = HardWareType.ACS;
            this.ComType = ComType;
            this.Baud_Rate = Baud_Rate;
            this.CommPort = CommPort;
            this.ConnType = ConnType;
            this.RemoteAddress = RemoteAddress;
            this.SlotNumber = SlotNumber;
            this.Baud_RateValue = Baud_RateValue;
            this.AutoConnect = AutoConnect;
            this.HMIExist = HMIExist;
        }

        //public acsConnectionInfo(string RemoteAddress, string Description)
        //{
        //    if (RemoteAddress.IndexOf("PCAN")>0)
        //        this.HWType = HardWareType.ECU;
        //    else if (RemoteAddress.IndexOf("GPIB") > 0)
        //        this.HWType = HardWareType.MEXA;
        //    else
        //        this.HWType = HardWareType.SERVER;

        //    this.RemoteAddress = RemoteAddress;
        //}

        public acsConnectionInfo(string RemoteAddress, string SlaveUnitIdentifier, string Description)
        {
            this.HWType = HardWareType.MODBUS;
            this.SlaveUnitIdentifier = SlaveUnitIdentifier;
            this.RemoteAddress = RemoteAddress;
        }

    }

    [Serializable]
    public class SysSettings
    {
        public string SaveDataDir = "Config\\Datainfo";
        public string SaveDataFileName = "DataInfo.xml";
        public string SysInfoFileName = "SysInfo.xml";
        public string ChannelInfoFileName = "ChInfo.xml";
        public string VarInfoFileName = "VarInfo.xml";
        public string MapInfoFileName = "MapInfo.xml";
        public string SysInfoDir = "Config\\Sysinfo";
        public string ChannelInfoDir = "Config\\ChInfo";
        public string VarInfoDir = "Config\\VarInfo";
        public string MapInfoDir = "Config\\MapInfo";
        public string DataLoggingDir = "Data";
        public string StepModeDir = "Config\\StepMode";
        public string TranModeDir = "Config\\TranMode";
        public string StepModeFileName = "StepMode.xml";
        public string TranModeFileName = "TranMode.xml";
        public string MonInfoDir = "Config\\MonInfo";
        public string MonInfoFileName = "MonInfo.xml";
        public string TemplateDir = "Config\\Template";
        public string MonitorList = "MonInfo,";
        public string DefaultDirectory = "";
        public string DataLoggingPrefix = "Data";

        public SysSettings()
        { }
    }
}
