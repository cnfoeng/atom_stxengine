﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Xml.Serialization;
using System.IO;

namespace AtomUI.Class
{
    [Serializable]
    public class TransientMode
    {
        public static TransientModeDefine TransientModeData = new TransientModeDefine();

        public static void Save(string FileName)
        {
            FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write);
            XmlSerializer xs = new XmlSerializer(TransientModeData.GetType());
            xs.Serialize(fs, TransientModeData);
            fs.Close();
        }

        public static void Load(string FileName)
        {
            FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
            XmlSerializer xs = new XmlSerializer(TransientModeData.GetType());
            Object dd = xs.Deserialize(fs);
            fs.Close();
            TransientModeData = (TransientModeDefine)dd;
        }
    }

    [Serializable]
    public class TransientModeDefine
    {
        public DataSet Data = new DataSet();
        public DynoMode DM;
        public EngineMode EM;

        public TransientModeDefine()
        {
            MakeDataSet();
        }

        public void MakeDataSet()
        {
            DataTable dt = new DataTable();
            dt.TableName = "TransientMode";

            DataColumn dc = dt.Columns.Add("Time");
            dc.DataType = Type.GetType("System.Int32");

            dc = dt.Columns.Add("Dyno_Target");
            dc.DataType = Type.GetType("System.Double");

            dc = dt.Columns.Add("Engine_Target");
            dc.DataType = Type.GetType("System.Double");

            Data.Tables.Add(dt);
        }

        public void LoadCSVFile(string FileName)
        {
            try
            {
                StreamReader sr = new StreamReader(new FileStream(FileName, FileMode.Open));
                string[] Lines = sr.ReadToEnd().Split('\n');
                sr.Close();
                sr.Dispose();
                DataTable dt = Data.Tables[0];
                dt.Rows.Clear();
                string[] Label = Lines[1].Split(',');

                if (Label[1].Trim() == "RPM")
                    DM = DynoMode.Speed_Mode;
                else
                    DM = DynoMode.Torque_Mode;

                if (Label[2].Trim() == "%")
                    EM = EngineMode.Alpha_Mode;
                else
                    EM = EngineMode.Torque_Mode;

                for (int i = 2; i < Lines.Length; i++)
                {
                    DataRow dr = dt.NewRow();
                    string[] Values = Lines[i].Split(',');
                    int iValue;
                    int.TryParse(Values[0], out iValue);
                    dr[0] = iValue;
                    double Value;
                    double.TryParse(Values[5], out Value);
                    dr[1] = Value;
                    double.TryParse(Values[6], out Value);
                    dr[2] = Value;
                    dt.Rows.Add(dr);
                }
            }
            catch (Exception ex)
            {
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n connect Error!!");
            }

        }
    }
}
