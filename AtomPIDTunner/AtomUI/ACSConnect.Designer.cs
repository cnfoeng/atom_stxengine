﻿namespace AtomUI
{
    partial class ACSConnect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ACSConnect));
            this.GreenPB = new System.Windows.Forms.PictureBox();
            this.GreyPB = new System.Windows.Forms.PictureBox();
            this.ChannelLb = new System.Windows.Forms.Label();
            this.ComTypeCmB = new System.Windows.Forms.ComboBox();
            this.PortLb = new System.Windows.Forms.Label();
            this.RateLb = new System.Windows.Forms.Label();
            this.CommPortCmB = new System.Windows.Forms.ComboBox();
            this.SlotNumberTB = new System.Windows.Forms.TextBox();
            this.RemoteAddressTB = new System.Windows.Forms.TextBox();
            this.BaudRateCmB = new System.Windows.Forms.ComboBox();
            this.ConnTypeCmB = new System.Windows.Forms.ComboBox();
            this.ConnectBtn = new System.Windows.Forms.Button();
            this.DisconnectBtn = new System.Windows.Forms.Button();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.AutoConnectchk = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.GreenPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GreyPB)).BeginInit();
            this.SuspendLayout();
            // 
            // GreenPB
            // 
            this.GreenPB.Image = ((System.Drawing.Image)(resources.GetObject("GreenPB.Image")));
            this.GreenPB.Location = new System.Drawing.Point(155, 169);
            this.GreenPB.Name = "GreenPB";
            this.GreenPB.Size = new System.Drawing.Size(39, 34);
            this.GreenPB.TabIndex = 27;
            this.GreenPB.TabStop = false;
            // 
            // GreyPB
            // 
            this.GreyPB.Image = ((System.Drawing.Image)(resources.GetObject("GreyPB.Image")));
            this.GreyPB.Location = new System.Drawing.Point(155, 169);
            this.GreyPB.Name = "GreyPB";
            this.GreyPB.Size = new System.Drawing.Size(39, 34);
            this.GreyPB.TabIndex = 28;
            this.GreyPB.TabStop = false;
            // 
            // ChannelLb
            // 
            this.ChannelLb.Location = new System.Drawing.Point(12, 9);
            this.ChannelLb.Name = "ChannelLb";
            this.ChannelLb.Size = new System.Drawing.Size(75, 28);
            this.ChannelLb.TabIndex = 30;
            this.ChannelLb.Text = "Channel";
            this.ChannelLb.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ComTypeCmB
            // 
            this.ComTypeCmB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComTypeCmB.Items.AddRange(new object[] {
            "Serial",
            "Network",
            "PCI Bus",
            "Simulator"});
            this.ComTypeCmB.Location = new System.Drawing.Point(133, 13);
            this.ComTypeCmB.Name = "ComTypeCmB";
            this.ComTypeCmB.Size = new System.Drawing.Size(171, 23);
            this.ComTypeCmB.TabIndex = 29;
            this.ComTypeCmB.SelectedIndexChanged += new System.EventHandler(this.ComTypeCmB_SelectedIndexChanged);
            // 
            // PortLb
            // 
            this.PortLb.Location = new System.Drawing.Point(29, 56);
            this.PortLb.Name = "PortLb";
            this.PortLb.Size = new System.Drawing.Size(107, 38);
            this.PortLb.TabIndex = 32;
            this.PortLb.Text = "Port";
            this.PortLb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // RateLb
            // 
            this.RateLb.Location = new System.Drawing.Point(29, 115);
            this.RateLb.Name = "RateLb";
            this.RateLb.Size = new System.Drawing.Size(107, 29);
            this.RateLb.TabIndex = 31;
            this.RateLb.Text = "Rate (bps)";
            this.RateLb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CommPortCmB
            // 
            this.CommPortCmB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CommPortCmB.Items.AddRange(new object[] {
            "Com1",
            "Com2",
            "Com3",
            "Com4",
            "Com5",
            "Com6",
            "Com7",
            "Com8",
            "Com9",
            "Com10",
            "Com11",
            "Com12",
            "Com13",
            "Com14",
            "Com15",
            "Com16"});
            this.CommPortCmB.Location = new System.Drawing.Point(133, 65);
            this.CommPortCmB.Name = "CommPortCmB";
            this.CommPortCmB.Size = new System.Drawing.Size(171, 23);
            this.CommPortCmB.TabIndex = 34;
            // 
            // SlotNumberTB
            // 
            this.SlotNumberTB.Location = new System.Drawing.Point(133, 65);
            this.SlotNumberTB.Name = "SlotNumberTB";
            this.SlotNumberTB.Size = new System.Drawing.Size(118, 25);
            this.SlotNumberTB.TabIndex = 35;
            this.SlotNumberTB.Text = "-1";
            // 
            // RemoteAddressTB
            // 
            this.RemoteAddressTB.Location = new System.Drawing.Point(133, 65);
            this.RemoteAddressTB.Name = "RemoteAddressTB";
            this.RemoteAddressTB.Size = new System.Drawing.Size(171, 25);
            this.RemoteAddressTB.TabIndex = 36;
            this.RemoteAddressTB.Text = "10.0.0.100";
            // 
            // BaudRateCmB
            // 
            this.BaudRateCmB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BaudRateCmB.Items.AddRange(new object[] {
            "Auto",
            "300",
            "1200",
            "4800",
            "9600",
            "19200",
            "57600",
            "115200"});
            this.BaudRateCmB.Location = new System.Drawing.Point(133, 119);
            this.BaudRateCmB.Name = "BaudRateCmB";
            this.BaudRateCmB.Size = new System.Drawing.Size(171, 23);
            this.BaudRateCmB.TabIndex = 37;
            // 
            // ConnTypeCmB
            // 
            this.ConnTypeCmB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ConnTypeCmB.Items.AddRange(new object[] {
            "Point to Point",
            "Network"});
            this.ConnTypeCmB.Location = new System.Drawing.Point(133, 119);
            this.ConnTypeCmB.Name = "ConnTypeCmB";
            this.ConnTypeCmB.Size = new System.Drawing.Size(171, 23);
            this.ConnTypeCmB.TabIndex = 38;
            // 
            // ConnectBtn
            // 
            this.ConnectBtn.Location = new System.Drawing.Point(32, 169);
            this.ConnectBtn.Name = "ConnectBtn";
            this.ConnectBtn.Size = new System.Drawing.Size(104, 29);
            this.ConnectBtn.TabIndex = 40;
            this.ConnectBtn.Text = "Connect";
            this.ConnectBtn.Click += new System.EventHandler(this.ConnectBtn_Click);
            // 
            // DisconnectBtn
            // 
            this.DisconnectBtn.Location = new System.Drawing.Point(200, 169);
            this.DisconnectBtn.Name = "DisconnectBtn";
            this.DisconnectBtn.Size = new System.Drawing.Size(104, 29);
            this.DisconnectBtn.TabIndex = 41;
            this.DisconnectBtn.Text = "DisConnect";
            this.DisconnectBtn.Click += new System.EventHandler(this.DisconnectBtn_Click);
            // 
            // SaveBtn
            // 
            this.SaveBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.SaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveBtn.Location = new System.Drawing.Point(32, 262);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(104, 29);
            this.SaveBtn.TabIndex = 42;
            this.SaveBtn.Text = "Save";
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // CancelBtn
            // 
            this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CancelBtn.Location = new System.Drawing.Point(200, 262);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(104, 29);
            this.CancelBtn.TabIndex = 43;
            this.CancelBtn.Text = "Cancel";
            // 
            // AutoConnectchk
            // 
            this.AutoConnectchk.AutoSize = true;
            this.AutoConnectchk.Location = new System.Drawing.Point(32, 222);
            this.AutoConnectchk.Name = "AutoConnectchk";
            this.AutoConnectchk.Size = new System.Drawing.Size(192, 19);
            this.AutoConnectchk.TabIndex = 44;
            this.AutoConnectchk.Text = "Auto Connect on Startup";
            this.AutoConnectchk.UseVisualStyleBackColor = true;
            // 
            // ACSConnect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelBtn;
            this.ClientSize = new System.Drawing.Size(330, 352);
            this.ControlBox = false;
            this.Controls.Add(this.AutoConnectchk);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.ConnectBtn);
            this.Controls.Add(this.DisconnectBtn);
            this.Controls.Add(this.ConnTypeCmB);
            this.Controls.Add(this.BaudRateCmB);
            this.Controls.Add(this.RemoteAddressTB);
            this.Controls.Add(this.SlotNumberTB);
            this.Controls.Add(this.CommPortCmB);
            this.Controls.Add(this.PortLb);
            this.Controls.Add(this.RateLb);
            this.Controls.Add(this.ChannelLb);
            this.Controls.Add(this.ComTypeCmB);
            this.Controls.Add(this.GreyPB);
            this.Controls.Add(this.GreenPB);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ACSConnect";
            this.Text = "ACS Connection Setup";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ACSConnect_FormClosing);
            this.Load += new System.EventHandler(this.ACSConnect_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GreenPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GreyPB)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox GreenPB;
        private System.Windows.Forms.PictureBox GreyPB;
        private System.Windows.Forms.Label ChannelLb;
        private System.Windows.Forms.ComboBox ComTypeCmB;
        private System.Windows.Forms.Label PortLb;
        private System.Windows.Forms.Label RateLb;
        private System.Windows.Forms.ComboBox CommPortCmB;
        private System.Windows.Forms.TextBox SlotNumberTB;
        private System.Windows.Forms.TextBox RemoteAddressTB;
        private System.Windows.Forms.ComboBox BaudRateCmB;
        private System.Windows.Forms.ComboBox ConnTypeCmB;
        private System.Windows.Forms.Button ConnectBtn;
        private System.Windows.Forms.Button DisconnectBtn;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.CheckBox AutoConnectchk;
    }
}