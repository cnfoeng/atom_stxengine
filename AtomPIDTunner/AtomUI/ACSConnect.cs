﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ACS.SPiiPlusNET;
using System.Runtime.InteropServices; //for COMException class
using AtomUI.Class;

namespace AtomUI
{
    public partial class ACSConnect : Form
    {
        private int ComTypeOld;
        //System.Drawing.Image iGrey;
        //System.Drawing.Image iGreen;

        public ACSConnect()
        {
            InitializeComponent();
        }

        private void ACSConnect_Load(object sender, EventArgs e)
        {
            BaudRateCmB.SelectedIndex = Ethercat.FindHardWare(HardWareType.ACS).Baud_Rate;   //  Baud rate
            ConnTypeCmB.SelectedIndex = Ethercat.FindHardWare(HardWareType.ACS).ConnType;   //  Ethernet connection type
            ComTypeOld = Ethercat.FindHardWare(HardWareType.ACS).ComType;
            ComTypeCmB.SelectedIndex = Ethercat.FindHardWare(HardWareType.ACS).ComType;    //  Communication medium (e.g. Serial, Ethernet)
            CommPortCmB.SelectedIndex = Ethercat.FindHardWare(HardWareType.ACS).CommPort;   //  COM channel
            RemoteAddressTB.Text = Ethercat.FindHardWare(HardWareType.ACS).RemoteAddress;
            SlotNumberTB.Text = Ethercat.FindHardWare(HardWareType.ACS).SlotNumber;
            AutoConnectchk.Checked = Ethercat.FindHardWare(HardWareType.ACS).AutoConnect;

            // Create new object of class Channel
            // Type Channel is defined in Api Type Library
            if (Ethercat.Ch == null)
                Ethercat.Ch = new Api();

            //ACSC_CONNECTION_INFO info = Ch.GetConnectionInfo();
            //ACSC_CONNECTION_DESC[] desc = Ch.GetConnectionsList();
            //  Set communication state as false
            if (Ethercat.Ch.IsConnected)
                Connected(true);
            else
                Connected(false);

            //  Retrieve Color for Green and Grey
            // System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
            //iGreen = GreenPB.Image;// ((System.Drawing.Image)(resources.GetObject("GreenPB.Image")));
            //iGrey = GreyPB.Image; // ((System.Drawing.Image)(resources.GetObject("GreyPB.Image")));
        }

        private void ComTypeCmB_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Show/hide the form's controls relevant for the selected communication media
            // Set all visibility state to false
            CommPortCmB.Visible = false;
            BaudRateCmB.Visible = false;
            ConnTypeCmB.Visible = false;
            RemoteAddressTB.Visible = false;
            SlotNumberTB.Visible = false;
            PortLb.Visible = false;
            RateLb.Visible = false;

            //  Turn on visibility based on user selection
            if (ComTypeCmB.SelectedIndex == 0)       // serial
            {
                CommPortCmB.Visible = true;
                BaudRateCmB.Visible = true;
                PortLb.Visible = true;
                RateLb.Visible = true;
                PortLb.Text = "Port";
                RateLb.Text = "Rate (bps)";
            }
            else if (ComTypeCmB.SelectedIndex == 1)         // Ethernet
            {
                ConnTypeCmB.Visible = true;
                RemoteAddressTB.Visible = true;
                PortLb.Visible = true;
                RateLb.Visible = true;
                PortLb.Text = "Remote Address";
                RateLb.Text = "Connection";
            }
            else if (ComTypeCmB.SelectedIndex == 2)     // PCI
            {
                SlotNumberTB.Visible = true;
                PortLb.Visible = true;
                PortLb.Text = "Slot Number";
            }
            // if the communicatioin media is changed and communication is open,
            //  close communication.
            if (ComTypeCmB.SelectedIndex != ComTypeOld && Ethercat.Ch.IsConnected)
            {
                try
                {
                    Disconnect();
                    ComTypeOld = ComTypeCmB.SelectedIndex;// Save the last communication channel option
                }
                catch (COMException Ex)
                {
                    Ethercat.ErrorMsg(Ex);                    // Throw exception if this fails
                }

            }
        }


        private void Disconnect()
        {
            if (Ethercat.Ch.IsConnected)
            {
                Ethercat.Ch.CloseComm();                 // Close current communication channel
                Connected(false);
            }
        }

        private void ConnectBtn_Click(object sender, EventArgs e)
        {
            //if communication is closed
            if (!Ethercat.Ch.IsConnected)
            {
                try
                {
                    if (ComTypeCmB.SelectedIndex == 0)                  //  Serial
                    {
                        int Baud_Rate;
                        if ((string)BaudRateCmB.SelectedItem == "Auto")
                            Baud_Rate = -1;
                        else
                            Baud_Rate = Convert.ToInt32(BaudRateCmB.SelectedItem);

                        // Open serial communuication.
                        // CommPortCmB.SelectedIndex + 1 defines the COM port.
                        Ethercat.Ch.OpenCommSerial(CommPortCmB.SelectedIndex + 1, Baud_Rate);
                    }
                    else if (ComTypeCmB.SelectedIndex == 1)             //  Ethernet
                    {
                        int Protocol;
                        if (ConnTypeCmB.SelectedIndex == 0)             //  Point to Point
                            Protocol = (int)EthernetCommOption.ACSC_SOCKET_DGRAM_PORT;
                        else
                            Protocol = (int)EthernetCommOption.ACSC_SOCKET_STREAM_PORT;
                        // Open ethernet communuication.
                        // RemoteAddress.Text defines the controller's TCP/IP address.
                        // Protocol is TCP/IP in case of network connection, and UDP in case of point-to-point connection.
                        Ethercat.Ch.OpenCommEthernet(RemoteAddressTB.Text, Protocol);
                    }
                    else if (ComTypeCmB.SelectedIndex == 2)             //  PCI
                    {

                        //Open PCI Bus communuication
                        int SlotNumber = Convert.ToInt32(SlotNumberTB.Text);
                        Ethercat.Ch.OpenCommPCI(SlotNumber);
                    }
                    else //(CComTypeCmB.SelectedIndex == 3)
                         //Open communuication with Simulator
                        Ethercat.Ch.OpenCommSimulator();

                    //  Set connection bit to true
                    Connected(true);
                    // Save new communication media
                    ComTypeOld = ComTypeCmB.SelectedIndex;

                }
                catch (COMException Ex)
                {
                    Ethercat.ErrorMsg(Ex);			//  Throw exception if this fails
                }
                catch (ACSException Ex)
                {
                    Ethercat.ErrorMsg(Ex);			//  Throw exception if this fails
                }
            }
        }

        private void Connected(bool con)
        {
            Ethercat.bConnected = con;
            if (Ethercat.bConnected)
            {
                // Make ConnInd green 
                GreenPB.Visible = true;
                GreyPB.Visible = false;
            }
            else
            {
                GreenPB.Visible = false;
                GreyPB.Visible = true;
            }
        }

    private void DisconnectBtn_Click(object sender, EventArgs e)
        {
            // if communication is open
            if (Ethercat.bConnected)
            {
                try
                {
                    Disconnect();
                }
                catch (COMException Ex)
                {
                    Ethercat.ErrorMsg(Ex);
                }
            }
        }

        private void ACSConnect_FormClosing(object sender, FormClosingEventArgs e)
        {
            //e.Cancel = true;
            //this.Hide();
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            Ethercat.FindHardWare(HardWareType.ACS).Baud_Rate = BaudRateCmB.SelectedIndex;   //  Baud rate
            Ethercat.FindHardWare(HardWareType.ACS).ConnType = ConnTypeCmB.SelectedIndex;   //  Ethernet connection type
            Ethercat.FindHardWare(HardWareType.ACS).ComType = ComTypeCmB.SelectedIndex;    //  Communication medium (e.g. Serial, Ethernet)
            Ethercat.FindHardWare(HardWareType.ACS).CommPort = CommPortCmB.SelectedIndex;   //  COM channel
            Ethercat.FindHardWare(HardWareType.ACS).RemoteAddress = RemoteAddressTB.Text;
            Ethercat.FindHardWare(HardWareType.ACS).SlotNumber = SlotNumberTB.Text;
            Ethercat.FindHardWare(HardWareType.ACS).Baud_RateValue = BaudRateCmB.SelectedText;   //  Baud rate
            Ethercat.FindHardWare(HardWareType.ACS).AutoConnect = AutoConnectchk.Checked;
            Ethercat.sysInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SysInfoDir + "\\" + Ethercat.sysInfo.sysSettings.SysInfoFileName);
        }
    }
}
