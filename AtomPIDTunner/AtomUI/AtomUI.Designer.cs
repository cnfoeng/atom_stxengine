﻿namespace AtomUI
{
    partial class AtomUI
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AtomUI));
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup3 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup4 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.connectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disconnectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitProgramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monitoringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customMonitoringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.predefinedMonitoringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HMIMonitorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inputMonitorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputMonitorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.calculationValueMonitorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customizeMonitoringViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.새로만들기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.기존파일추가ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.controlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stepModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profileDefineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runStepModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transientModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modeDefineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runTransientModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pIDTunnerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iOControlDemoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.controlPadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smokeMeterControlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fuelMeterControlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blowbyMeterControlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mexaControlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.controllerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.systemSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.controllerInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.channelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moduleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configUploadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.variableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.variableDefineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.variableMappingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculatingVariableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.variableInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.predefinedVariablesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alarmDefineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calibrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataLoggingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.remoteSignalRangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ControllerMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.systemSetupToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlControl = new System.Windows.Forms.Panel();
            this.pnlMexa = new System.Windows.Forms.Panel();
            this.pnlStatus = new System.Windows.Forms.Panel();
            this.label47 = new System.Windows.Forms.Label();
            this.btnHornOff = new System.Windows.Forms.Button();
            this.label46 = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.label45 = new System.Windows.Forms.Label();
            this.btnAlarm = new System.Windows.Forms.Button();
            this.label34 = new System.Windows.Forms.Label();
            this.btnNA2 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.btnTN2 = new System.Windows.Forms.Button();
            this.label44 = new System.Windows.Forms.Label();
            this.btnTA2 = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.btnRemote2 = new System.Windows.Forms.Button();
            this.btnConnect2 = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.chkLoggingConstant = new System.Windows.Forms.CheckBox();
            this.chkLoggingCtn = new System.Windows.Forms.CheckBox();
            this.chkLoggingAvg = new System.Windows.Forms.CheckBox();
            this.chkLoggingAvgCtn = new System.Windows.Forms.CheckBox();
            this.tbLoggingTimeAvg = new System.Windows.Forms.TextBox();
            this.tbLoggingTimeCtn = new System.Windows.Forms.TextBox();
            this.progressLoggingCtn = new System.Windows.Forms.ProgressBar();
            this.progressLoggingAvg = new System.Windows.Forms.ProgressBar();
            this.label64 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.btnDataFolderOpen = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.pnlDAQ = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.btnOsirisStatus = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.btnModbusStatus = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.btnACSStatus = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.btnMexaStatus = new System.Windows.Forms.Button();
            this.btnDAQ = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.btnECUStatus = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.btnSMStatus = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.lvOutput = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvInput = new System.Windows.Forms.ListView();
            this.Header1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lbAlarm = new System.Windows.Forms.ListBox();
            this.tbAlarmCNT = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tvSystemInfo = new System.Windows.Forms.TreeView();
            this.tmrDrawing = new System.Windows.Forms.Timer(this.components);
            this.pnlACS = new System.Windows.Forms.Panel();
            this.btnIDLE = new System.Windows.Forms.Button();
            this.btnConnection = new System.Windows.Forms.Button();
            this.btnNT = new System.Windows.Forms.Button();
            this.btnTN = new System.Windows.Forms.Button();
            this.btnNA = new System.Windows.Forms.Button();
            this.btnTA = new System.Windows.Forms.Button();
            this.btnIdleControl = new System.Windows.Forms.Button();
            this.btnDyno = new System.Windows.Forms.Button();
            this.btnEngine = new System.Windows.Forms.Button();
            this.btnManual = new System.Windows.Forms.Button();
            this.btnRemote = new System.Windows.Forms.Button();
            this.btnData = new System.Windows.Forms.Button();
            this.btnAuto = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.pnlTolCenter = new System.Windows.Forms.Panel();
            this.pnlTolRight = new System.Windows.Forms.Panel();
            this.pnlTolLeft = new System.Windows.Forms.Panel();
            this.menuMain.SuspendLayout();
            this.ControllerMenuStrip.SuspendLayout();
            this.pnlControl.SuspendLayout();
            this.pnlMexa.SuspendLayout();
            this.pnlStatus.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlDAQ.SuspendLayout();
            this.tcMain.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlACS.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // menuMain
            // 
            this.menuMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.menuMain.AutoSize = false;
            this.menuMain.BackColor = System.Drawing.Color.LightYellow;
            this.menuMain.Dock = System.Windows.Forms.DockStyle.None;
            this.menuMain.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.menuMain.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectionToolStripMenuItem,
            this.monitoringToolStripMenuItem,
            this.controlToolStripMenuItem,
            this.controllerToolStripMenuItem,
            this.variableToolStripMenuItem});
            this.menuMain.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.menuMain.Location = new System.Drawing.Point(0, 103);
            this.menuMain.Name = "menuMain";
            this.menuMain.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuMain.Size = new System.Drawing.Size(1208, 22);
            this.menuMain.TabIndex = 28;
            this.menuMain.Text = "menuMain";
            // 
            // connectionToolStripMenuItem
            // 
            this.connectionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setupToolStripMenuItem,
            this.connectToolStripMenuItem,
            this.disconnectToolStripMenuItem,
            this.exitProgramToolStripMenuItem});
            this.connectionToolStripMenuItem.Name = "connectionToolStripMenuItem";
            this.connectionToolStripMenuItem.Size = new System.Drawing.Size(81, 19);
            this.connectionToolStripMenuItem.Text = "Connection";
            // 
            // setupToolStripMenuItem
            // 
            this.setupToolStripMenuItem.Name = "setupToolStripMenuItem";
            this.setupToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.setupToolStripMenuItem.Text = "Setup";
            this.setupToolStripMenuItem.Click += new System.EventHandler(this.setupToolStripMenuItem_Click);
            // 
            // connectToolStripMenuItem
            // 
            this.connectToolStripMenuItem.Name = "connectToolStripMenuItem";
            this.connectToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.connectToolStripMenuItem.Text = "Connect";
            this.connectToolStripMenuItem.Click += new System.EventHandler(this.connectToolStripMenuItem_Click);
            // 
            // disconnectToolStripMenuItem
            // 
            this.disconnectToolStripMenuItem.Name = "disconnectToolStripMenuItem";
            this.disconnectToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.disconnectToolStripMenuItem.Text = "Disconnect";
            this.disconnectToolStripMenuItem.Click += new System.EventHandler(this.disconnectToolStripMenuItem_Click);
            // 
            // exitProgramToolStripMenuItem
            // 
            this.exitProgramToolStripMenuItem.Name = "exitProgramToolStripMenuItem";
            this.exitProgramToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.exitProgramToolStripMenuItem.Text = "Exit Program";
            this.exitProgramToolStripMenuItem.Click += new System.EventHandler(this.exitProgramToolStripMenuItem_Click);
            // 
            // monitoringToolStripMenuItem
            // 
            this.monitoringToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customMonitoringToolStripMenuItem,
            this.predefinedMonitoringToolStripMenuItem,
            this.customizeMonitoringViewToolStripMenuItem});
            this.monitoringToolStripMenuItem.Name = "monitoringToolStripMenuItem";
            this.monitoringToolStripMenuItem.Size = new System.Drawing.Size(79, 19);
            this.monitoringToolStripMenuItem.Text = "Monitoring";
            // 
            // customMonitoringToolStripMenuItem
            // 
            this.customMonitoringToolStripMenuItem.Name = "customMonitoringToolStripMenuItem";
            this.customMonitoringToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.customMonitoringToolStripMenuItem.Text = "Custom Monitor";
            this.customMonitoringToolStripMenuItem.Click += new System.EventHandler(this.customMonitoringToolStripMenuItem_Click);
            // 
            // predefinedMonitoringToolStripMenuItem
            // 
            this.predefinedMonitoringToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.HMIMonitorToolStripMenuItem,
            this.inputMonitorToolStripMenuItem,
            this.outputMonitorToolStripMenuItem1,
            this.calculationValueMonitorToolStripMenuItem1});
            this.predefinedMonitoringToolStripMenuItem.Name = "predefinedMonitoringToolStripMenuItem";
            this.predefinedMonitoringToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.predefinedMonitoringToolStripMenuItem.Text = "Predefined Monitor";
            this.predefinedMonitoringToolStripMenuItem.Visible = false;
            // 
            // HMIMonitorToolStripMenuItem
            // 
            this.HMIMonitorToolStripMenuItem.Name = "HMIMonitorToolStripMenuItem";
            this.HMIMonitorToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.HMIMonitorToolStripMenuItem.Text = "HMI Monitor";
            this.HMIMonitorToolStripMenuItem.Click += new System.EventHandler(this.HMIMonitorToolStripMenuItem_Click);
            // 
            // inputMonitorToolStripMenuItem
            // 
            this.inputMonitorToolStripMenuItem.Name = "inputMonitorToolStripMenuItem";
            this.inputMonitorToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.inputMonitorToolStripMenuItem.Text = "Analog Input Monitor";
            this.inputMonitorToolStripMenuItem.Click += new System.EventHandler(this.inputMonitorToolStripMenuItem_Click);
            // 
            // outputMonitorToolStripMenuItem1
            // 
            this.outputMonitorToolStripMenuItem1.Name = "outputMonitorToolStripMenuItem1";
            this.outputMonitorToolStripMenuItem1.Size = new System.Drawing.Size(215, 22);
            this.outputMonitorToolStripMenuItem1.Text = "Output Monitor";
            // 
            // calculationValueMonitorToolStripMenuItem1
            // 
            this.calculationValueMonitorToolStripMenuItem1.Name = "calculationValueMonitorToolStripMenuItem1";
            this.calculationValueMonitorToolStripMenuItem1.Size = new System.Drawing.Size(215, 22);
            this.calculationValueMonitorToolStripMenuItem1.Text = "Calculation Value Monitor";
            // 
            // customizeMonitoringViewToolStripMenuItem
            // 
            this.customizeMonitoringViewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.새로만들기ToolStripMenuItem,
            this.기존파일추가ToolStripMenuItem});
            this.customizeMonitoringViewToolStripMenuItem.Name = "customizeMonitoringViewToolStripMenuItem";
            this.customizeMonitoringViewToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.customizeMonitoringViewToolStripMenuItem.Text = "Customize Monitor";
            // 
            // 새로만들기ToolStripMenuItem
            // 
            this.새로만들기ToolStripMenuItem.Name = "새로만들기ToolStripMenuItem";
            this.새로만들기ToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.새로만들기ToolStripMenuItem.Text = "새로만들기";
            this.새로만들기ToolStripMenuItem.Click += new System.EventHandler(this.새로만들기ToolStripMenuItem_Click);
            // 
            // 기존파일추가ToolStripMenuItem
            // 
            this.기존파일추가ToolStripMenuItem.Name = "기존파일추가ToolStripMenuItem";
            this.기존파일추가ToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.기존파일추가ToolStripMenuItem.Text = "기존파일 추가";
            this.기존파일추가ToolStripMenuItem.Click += new System.EventHandler(this.기존파일추가ToolStripMenuItem_Click);
            // 
            // controlToolStripMenuItem
            // 
            this.controlToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stepModeToolStripMenuItem,
            this.transientModeToolStripMenuItem,
            this.pIDTunnerToolStripMenuItem,
            this.iOControlDemoToolStripMenuItem,
            this.controlPadToolStripMenuItem,
            this.smokeMeterControlToolStripMenuItem,
            this.fuelMeterControlToolStripMenuItem,
            this.blowbyMeterControlToolStripMenuItem,
            this.mexaControlToolStripMenuItem});
            this.controlToolStripMenuItem.Name = "controlToolStripMenuItem";
            this.controlToolStripMenuItem.Size = new System.Drawing.Size(59, 19);
            this.controlToolStripMenuItem.Text = "Control";
            // 
            // stepModeToolStripMenuItem
            // 
            this.stepModeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.profileDefineToolStripMenuItem,
            this.runStepModeToolStripMenuItem});
            this.stepModeToolStripMenuItem.Name = "stepModeToolStripMenuItem";
            this.stepModeToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.stepModeToolStripMenuItem.Text = "Step Mode";
            // 
            // profileDefineToolStripMenuItem
            // 
            this.profileDefineToolStripMenuItem.Name = "profileDefineToolStripMenuItem";
            this.profileDefineToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.profileDefineToolStripMenuItem.Text = "Mode Define";
            this.profileDefineToolStripMenuItem.Click += new System.EventHandler(this.profileDefineToolStripMenuItem_Click);
            // 
            // runStepModeToolStripMenuItem
            // 
            this.runStepModeToolStripMenuItem.Name = "runStepModeToolStripMenuItem";
            this.runStepModeToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.runStepModeToolStripMenuItem.Text = "Run Step Mode";
            this.runStepModeToolStripMenuItem.Click += new System.EventHandler(this.runStepModeToolStripMenuItem_Click);
            // 
            // transientModeToolStripMenuItem
            // 
            this.transientModeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modeDefineToolStripMenuItem,
            this.runTransientModeToolStripMenuItem});
            this.transientModeToolStripMenuItem.Name = "transientModeToolStripMenuItem";
            this.transientModeToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.transientModeToolStripMenuItem.Text = "Transient Mode";
            // 
            // modeDefineToolStripMenuItem
            // 
            this.modeDefineToolStripMenuItem.Name = "modeDefineToolStripMenuItem";
            this.modeDefineToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.modeDefineToolStripMenuItem.Text = "Mode Define";
            this.modeDefineToolStripMenuItem.Click += new System.EventHandler(this.modeDefineToolStripMenuItem_Click);
            // 
            // runTransientModeToolStripMenuItem
            // 
            this.runTransientModeToolStripMenuItem.Name = "runTransientModeToolStripMenuItem";
            this.runTransientModeToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.runTransientModeToolStripMenuItem.Text = "Run Transient Mode";
            this.runTransientModeToolStripMenuItem.Click += new System.EventHandler(this.runTransientModeToolStripMenuItem_Click);
            // 
            // pIDTunnerToolStripMenuItem
            // 
            this.pIDTunnerToolStripMenuItem.Name = "pIDTunnerToolStripMenuItem";
            this.pIDTunnerToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.pIDTunnerToolStripMenuItem.Text = "PID Tuner";
            this.pIDTunnerToolStripMenuItem.Visible = false;
            this.pIDTunnerToolStripMenuItem.Click += new System.EventHandler(this.pIDTunnerToolStripMenuItem_Click);
            // 
            // iOControlDemoToolStripMenuItem
            // 
            this.iOControlDemoToolStripMenuItem.Name = "iOControlDemoToolStripMenuItem";
            this.iOControlDemoToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.iOControlDemoToolStripMenuItem.Text = "IOControl Demo";
            this.iOControlDemoToolStripMenuItem.Visible = false;
            this.iOControlDemoToolStripMenuItem.Click += new System.EventHandler(this.iOControlDemoToolStripMenuItem_Click);
            // 
            // controlPadToolStripMenuItem
            // 
            this.controlPadToolStripMenuItem.Name = "controlPadToolStripMenuItem";
            this.controlPadToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.controlPadToolStripMenuItem.Text = "Control Pad";
            this.controlPadToolStripMenuItem.Click += new System.EventHandler(this.controlPadToolStripMenuItem_Click);
            // 
            // smokeMeterControlToolStripMenuItem
            // 
            this.smokeMeterControlToolStripMenuItem.Name = "smokeMeterControlToolStripMenuItem";
            this.smokeMeterControlToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.smokeMeterControlToolStripMenuItem.Text = "SmokeMeter Control";
            this.smokeMeterControlToolStripMenuItem.Click += new System.EventHandler(this.smokeMeterControlToolStripMenuItem_Click);
            // 
            // fuelMeterControlToolStripMenuItem
            // 
            this.fuelMeterControlToolStripMenuItem.Name = "fuelMeterControlToolStripMenuItem";
            this.fuelMeterControlToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.fuelMeterControlToolStripMenuItem.Text = "FuelMeter Control";
            this.fuelMeterControlToolStripMenuItem.Click += new System.EventHandler(this.fuelMeterControlToolStripMenuItem_Click);
            // 
            // blowbyMeterControlToolStripMenuItem
            // 
            this.blowbyMeterControlToolStripMenuItem.Name = "blowbyMeterControlToolStripMenuItem";
            this.blowbyMeterControlToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.blowbyMeterControlToolStripMenuItem.Text = "BlowbyMeter Control";
            // 
            // mexaControlToolStripMenuItem
            // 
            this.mexaControlToolStripMenuItem.Name = "mexaControlToolStripMenuItem";
            this.mexaControlToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.mexaControlToolStripMenuItem.Text = "Mexa Control";
            this.mexaControlToolStripMenuItem.Click += new System.EventHandler(this.mexaControlToolStripMenuItem_Click_1);
            // 
            // controllerToolStripMenuItem
            // 
            this.controllerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripSeparator1,
            this.systemSetupToolStripMenuItem,
            this.toolStripSeparator2,
            this.controllerInfoToolStripMenuItem,
            this.toolStripMenuItem1,
            this.channelToolStripMenuItem,
            this.moduleToolStripMenuItem,
            this.configUploadToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.controllerToolStripMenuItem.Name = "controllerToolStripMenuItem";
            this.controllerToolStripMenuItem.Size = new System.Drawing.Size(93, 19);
            this.controllerToolStripMenuItem.Text = "System Setup";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(179, 22);
            this.toolStripMenuItem2.Text = "HW Config";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(176, 6);
            // 
            // systemSetupToolStripMenuItem
            // 
            this.systemSetupToolStripMenuItem.Name = "systemSetupToolStripMenuItem";
            this.systemSetupToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.systemSetupToolStripMenuItem.Text = "ACS Setup";
            this.systemSetupToolStripMenuItem.Click += new System.EventHandler(this.systemSetupToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(176, 6);
            // 
            // controllerInfoToolStripMenuItem
            // 
            this.controllerInfoToolStripMenuItem.Name = "controllerInfoToolStripMenuItem";
            this.controllerInfoToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.controllerInfoToolStripMenuItem.Text = "ACS Controller Info";
            this.controllerInfoToolStripMenuItem.Click += new System.EventHandler(this.controllerInfoToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(179, 22);
            this.toolStripMenuItem1.Text = "Module Info";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // channelToolStripMenuItem
            // 
            this.channelToolStripMenuItem.Name = "channelToolStripMenuItem";
            this.channelToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.channelToolStripMenuItem.Text = "Channel Info";
            this.channelToolStripMenuItem.Click += new System.EventHandler(this.channelToolStripMenuItem_Click);
            // 
            // moduleToolStripMenuItem
            // 
            this.moduleToolStripMenuItem.Enabled = false;
            this.moduleToolStripMenuItem.Name = "moduleToolStripMenuItem";
            this.moduleToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.moduleToolStripMenuItem.Text = "Module Verify";
            this.moduleToolStripMenuItem.Visible = false;
            // 
            // configUploadToolStripMenuItem
            // 
            this.configUploadToolStripMenuItem.Enabled = false;
            this.configUploadToolStripMenuItem.Name = "configUploadToolStripMenuItem";
            this.configUploadToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.configUploadToolStripMenuItem.Text = "Config Upload";
            this.configUploadToolStripMenuItem.Visible = false;
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.optionsToolStripMenuItem.Text = "Options";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // variableToolStripMenuItem
            // 
            this.variableToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.variableDefineToolStripMenuItem,
            this.variableMappingToolStripMenuItem,
            this.calculatingVariableToolStripMenuItem,
            this.variableInformationToolStripMenuItem,
            this.predefinedVariablesToolStripMenuItem,
            this.alarmDefineToolStripMenuItem,
            this.calibrationToolStripMenuItem,
            this.dataLoggingToolStripMenuItem,
            this.remoteSignalRangeToolStripMenuItem});
            this.variableToolStripMenuItem.Name = "variableToolStripMenuItem";
            this.variableToolStripMenuItem.Size = new System.Drawing.Size(79, 19);
            this.variableToolStripMenuItem.Text = "Data Setup";
            // 
            // variableDefineToolStripMenuItem
            // 
            this.variableDefineToolStripMenuItem.Name = "variableDefineToolStripMenuItem";
            this.variableDefineToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.variableDefineToolStripMenuItem.Text = "Variable Define";
            this.variableDefineToolStripMenuItem.Click += new System.EventHandler(this.variableDefineToolStripMenuItem_Click);
            // 
            // variableMappingToolStripMenuItem
            // 
            this.variableMappingToolStripMenuItem.Name = "variableMappingToolStripMenuItem";
            this.variableMappingToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.variableMappingToolStripMenuItem.Text = "Variable Load/Save";
            this.variableMappingToolStripMenuItem.Click += new System.EventHandler(this.variableMappingToolStripMenuItem_Click);
            // 
            // calculatingVariableToolStripMenuItem
            // 
            this.calculatingVariableToolStripMenuItem.Name = "calculatingVariableToolStripMenuItem";
            this.calculatingVariableToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.calculatingVariableToolStripMenuItem.Text = "Calculation Ref. Variable";
            this.calculatingVariableToolStripMenuItem.Click += new System.EventHandler(this.calculatingVariableToolStripMenuItem_Click);
            // 
            // variableInformationToolStripMenuItem
            // 
            this.variableInformationToolStripMenuItem.Enabled = false;
            this.variableInformationToolStripMenuItem.Name = "variableInformationToolStripMenuItem";
            this.variableInformationToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.variableInformationToolStripMenuItem.Text = "Variable Information";
            this.variableInformationToolStripMenuItem.Visible = false;
            // 
            // predefinedVariablesToolStripMenuItem
            // 
            this.predefinedVariablesToolStripMenuItem.Enabled = false;
            this.predefinedVariablesToolStripMenuItem.Name = "predefinedVariablesToolStripMenuItem";
            this.predefinedVariablesToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.predefinedVariablesToolStripMenuItem.Text = "Predefined Variables";
            this.predefinedVariablesToolStripMenuItem.Visible = false;
            // 
            // alarmDefineToolStripMenuItem
            // 
            this.alarmDefineToolStripMenuItem.Enabled = false;
            this.alarmDefineToolStripMenuItem.Name = "alarmDefineToolStripMenuItem";
            this.alarmDefineToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.alarmDefineToolStripMenuItem.Text = "Alarm Define";
            this.alarmDefineToolStripMenuItem.Visible = false;
            // 
            // calibrationToolStripMenuItem
            // 
            this.calibrationToolStripMenuItem.Enabled = false;
            this.calibrationToolStripMenuItem.Name = "calibrationToolStripMenuItem";
            this.calibrationToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.calibrationToolStripMenuItem.Text = "Calibration";
            this.calibrationToolStripMenuItem.Visible = false;
            // 
            // dataLoggingToolStripMenuItem
            // 
            this.dataLoggingToolStripMenuItem.Name = "dataLoggingToolStripMenuItem";
            this.dataLoggingToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.dataLoggingToolStripMenuItem.Text = "Data Logging Setup";
            this.dataLoggingToolStripMenuItem.Click += new System.EventHandler(this.dataLoggingToolStripMenuItem_Click);
            // 
            // remoteSignalRangeToolStripMenuItem
            // 
            this.remoteSignalRangeToolStripMenuItem.Enabled = false;
            this.remoteSignalRangeToolStripMenuItem.Name = "remoteSignalRangeToolStripMenuItem";
            this.remoteSignalRangeToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.remoteSignalRangeToolStripMenuItem.Text = "ACS Setting";
            this.remoteSignalRangeToolStripMenuItem.Visible = false;
            this.remoteSignalRangeToolStripMenuItem.Click += new System.EventHandler(this.remoteSignalRangeToolStripMenuItem_Click);
            // 
            // ControllerMenuStrip
            // 
            this.ControllerMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ControllerMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.systemSetupToolStripMenuItem1});
            this.ControllerMenuStrip.Name = "ControllerMenuStrip";
            this.ControllerMenuStrip.Size = new System.Drawing.Size(149, 26);
            // 
            // systemSetupToolStripMenuItem1
            // 
            this.systemSetupToolStripMenuItem1.Name = "systemSetupToolStripMenuItem1";
            this.systemSetupToolStripMenuItem1.Size = new System.Drawing.Size(148, 22);
            this.systemSetupToolStripMenuItem1.Text = "System Setup";
            this.systemSetupToolStripMenuItem1.Click += new System.EventHandler(this.systemSetupToolStripMenuItem1_Click);
            // 
            // pnlControl
            // 
            this.pnlControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlControl.Controls.Add(this.pnlMexa);
            this.pnlControl.Controls.Add(this.label19);
            this.pnlControl.ForeColor = System.Drawing.Color.Black;
            this.pnlControl.Location = new System.Drawing.Point(12, 137);
            this.pnlControl.Name = "pnlControl";
            this.pnlControl.Size = new System.Drawing.Size(219, 905);
            this.pnlControl.TabIndex = 33;
            // 
            // pnlMexa
            // 
            this.pnlMexa.Controls.Add(this.pnlStatus);
            this.pnlMexa.Controls.Add(this.panel3);
            this.pnlMexa.Controls.Add(this.label29);
            this.pnlMexa.Controls.Add(this.pnlDAQ);
            this.pnlMexa.Location = new System.Drawing.Point(3, 24);
            this.pnlMexa.Name = "pnlMexa";
            this.pnlMexa.Size = new System.Drawing.Size(211, 876);
            this.pnlMexa.TabIndex = 94;
            // 
            // pnlStatus
            // 
            this.pnlStatus.Controls.Add(this.label47);
            this.pnlStatus.Controls.Add(this.btnHornOff);
            this.pnlStatus.Controls.Add(this.label46);
            this.pnlStatus.Controls.Add(this.btnReset);
            this.pnlStatus.Controls.Add(this.label45);
            this.pnlStatus.Controls.Add(this.btnAlarm);
            this.pnlStatus.Controls.Add(this.label34);
            this.pnlStatus.Controls.Add(this.btnNA2);
            this.pnlStatus.Controls.Add(this.label25);
            this.pnlStatus.Controls.Add(this.btnTN2);
            this.pnlStatus.Controls.Add(this.label44);
            this.pnlStatus.Controls.Add(this.btnTA2);
            this.pnlStatus.Controls.Add(this.label43);
            this.pnlStatus.Controls.Add(this.label36);
            this.pnlStatus.Controls.Add(this.btnRemote2);
            this.pnlStatus.Controls.Add(this.btnConnect2);
            this.pnlStatus.Controls.Add(this.label35);
            this.pnlStatus.Location = new System.Drawing.Point(0, 4);
            this.pnlStatus.Name = "pnlStatus";
            this.pnlStatus.Size = new System.Drawing.Size(211, 330);
            this.pnlStatus.TabIndex = 117;
            // 
            // label47
            // 
            this.label47.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label47.Location = new System.Drawing.Point(6, 250);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(94, 21);
            this.label47.TabIndex = 130;
            this.label47.Text = "HORN OFF";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnHornOff
            // 
            this.btnHornOff.BackColor = System.Drawing.Color.Silver;
            this.btnHornOff.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnHornOff.BackgroundImage")));
            this.btnHornOff.Enabled = false;
            this.btnHornOff.FlatAppearance.BorderSize = 0;
            this.btnHornOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHornOff.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnHornOff.Location = new System.Drawing.Point(20, 273);
            this.btnHornOff.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnHornOff.Name = "btnHornOff";
            this.btnHornOff.Size = new System.Drawing.Size(65, 45);
            this.btnHornOff.TabIndex = 131;
            this.btnHornOff.UseVisualStyleBackColor = false;
            this.btnHornOff.Click += new System.EventHandler(this.btnHornOff_Click);
            // 
            // label46
            // 
            this.label46.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label46.Location = new System.Drawing.Point(111, 174);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(94, 21);
            this.label46.TabIndex = 128;
            this.label46.Text = "RESET";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.Silver;
            this.btnReset.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnReset.BackgroundImage")));
            this.btnReset.Enabled = false;
            this.btnReset.FlatAppearance.BorderSize = 0;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnReset.Location = new System.Drawing.Point(125, 197);
            this.btnReset.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(65, 45);
            this.btnReset.TabIndex = 129;
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // label45
            // 
            this.label45.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label45.Location = new System.Drawing.Point(111, 250);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(94, 21);
            this.label45.TabIndex = 126;
            this.label45.Text = "ALARM";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAlarm
            // 
            this.btnAlarm.BackColor = System.Drawing.Color.Silver;
            this.btnAlarm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAlarm.BackgroundImage")));
            this.btnAlarm.Enabled = false;
            this.btnAlarm.FlatAppearance.BorderSize = 0;
            this.btnAlarm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlarm.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAlarm.Location = new System.Drawing.Point(125, 273);
            this.btnAlarm.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAlarm.Name = "btnAlarm";
            this.btnAlarm.Size = new System.Drawing.Size(65, 45);
            this.btnAlarm.TabIndex = 127;
            this.btnAlarm.UseVisualStyleBackColor = false;
            this.btnAlarm.Click += new System.EventHandler(this.btnAlarm_Click);
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(109)))), ((int)(((byte)(172)))));
            this.label34.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label34.ForeColor = System.Drawing.Color.White;
            this.label34.Location = new System.Drawing.Point(-1, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(213, 21);
            this.label34.TabIndex = 125;
            this.label34.Text = "Control System";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNA2
            // 
            this.btnNA2.BackColor = System.Drawing.Color.Silver;
            this.btnNA2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNA2.BackgroundImage")));
            this.btnNA2.Enabled = false;
            this.btnNA2.FlatAppearance.BorderSize = 0;
            this.btnNA2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNA2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnNA2.Location = new System.Drawing.Point(19, 197);
            this.btnNA2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnNA2.Name = "btnNA2";
            this.btnNA2.Size = new System.Drawing.Size(65, 45);
            this.btnNA2.TabIndex = 124;
            this.btnNA2.UseVisualStyleBackColor = false;
            this.btnNA2.Click += new System.EventHandler(this.btnNA2_Click);
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label25.Location = new System.Drawing.Point(5, 175);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(94, 21);
            this.label25.TabIndex = 123;
            this.label25.Text = "N/α";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnTN2
            // 
            this.btnTN2.BackColor = System.Drawing.Color.Silver;
            this.btnTN2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTN2.BackgroundImage")));
            this.btnTN2.Enabled = false;
            this.btnTN2.FlatAppearance.BorderSize = 0;
            this.btnTN2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTN2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnTN2.Location = new System.Drawing.Point(125, 122);
            this.btnTN2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnTN2.Name = "btnTN2";
            this.btnTN2.Size = new System.Drawing.Size(65, 45);
            this.btnTN2.TabIndex = 124;
            this.btnTN2.UseVisualStyleBackColor = false;
            this.btnTN2.Click += new System.EventHandler(this.btnNT2_Click);
            // 
            // label44
            // 
            this.label44.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label44.Location = new System.Drawing.Point(111, 100);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(94, 21);
            this.label44.TabIndex = 123;
            this.label44.Text = "T/N";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnTA2
            // 
            this.btnTA2.BackColor = System.Drawing.Color.Silver;
            this.btnTA2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTA2.BackgroundImage")));
            this.btnTA2.Enabled = false;
            this.btnTA2.FlatAppearance.BorderSize = 0;
            this.btnTA2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTA2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnTA2.Location = new System.Drawing.Point(20, 122);
            this.btnTA2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnTA2.Name = "btnTA2";
            this.btnTA2.Size = new System.Drawing.Size(65, 45);
            this.btnTA2.TabIndex = 122;
            this.btnTA2.UseVisualStyleBackColor = false;
            this.btnTA2.Click += new System.EventHandler(this.btnTA2_Click);
            // 
            // label43
            // 
            this.label43.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label43.Location = new System.Drawing.Point(6, 99);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(94, 21);
            this.label43.TabIndex = 121;
            this.label43.Text = "T/α";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label36
            // 
            this.label36.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label36.Location = new System.Drawing.Point(111, 28);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(94, 21);
            this.label36.TabIndex = 119;
            this.label36.Text = "REMOTE";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnRemote2
            // 
            this.btnRemote2.BackColor = System.Drawing.Color.Silver;
            this.btnRemote2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRemote2.BackgroundImage")));
            this.btnRemote2.Enabled = false;
            this.btnRemote2.FlatAppearance.BorderSize = 0;
            this.btnRemote2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemote2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnRemote2.Location = new System.Drawing.Point(125, 51);
            this.btnRemote2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRemote2.Name = "btnRemote2";
            this.btnRemote2.Size = new System.Drawing.Size(65, 45);
            this.btnRemote2.TabIndex = 120;
            this.btnRemote2.UseVisualStyleBackColor = false;
            this.btnRemote2.Click += new System.EventHandler(this.btnRemote2_Click);
            // 
            // btnConnect2
            // 
            this.btnConnect2.BackgroundImage = global::AtomUI.Properties.Resources.off버튼;
            this.btnConnect2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnConnect2.FlatAppearance.BorderSize = 0;
            this.btnConnect2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConnect2.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnConnect2.Location = new System.Drawing.Point(20, 51);
            this.btnConnect2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnConnect2.Name = "btnConnect2";
            this.btnConnect2.Size = new System.Drawing.Size(65, 45);
            this.btnConnect2.TabIndex = 118;
            this.btnConnect2.UseVisualStyleBackColor = false;
            this.btnConnect2.Click += new System.EventHandler(this.btnConnection_Click);
            // 
            // label35
            // 
            this.label35.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label35.Location = new System.Drawing.Point(7, 28);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(94, 21);
            this.label35.TabIndex = 117;
            this.label35.Text = "CONNECTION";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.chkLoggingConstant);
            this.panel3.Controls.Add(this.chkLoggingCtn);
            this.panel3.Controls.Add(this.chkLoggingAvg);
            this.panel3.Controls.Add(this.chkLoggingAvgCtn);
            this.panel3.Controls.Add(this.tbLoggingTimeAvg);
            this.panel3.Controls.Add(this.tbLoggingTimeCtn);
            this.panel3.Controls.Add(this.progressLoggingCtn);
            this.panel3.Controls.Add(this.progressLoggingAvg);
            this.panel3.Controls.Add(this.label64);
            this.panel3.Controls.Add(this.label41);
            this.panel3.Controls.Add(this.label37);
            this.panel3.Controls.Add(this.btnDataFolderOpen);
            this.panel3.Location = new System.Drawing.Point(0, 647);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(211, 226);
            this.panel3.TabIndex = 93;
            // 
            // chkLoggingConstant
            // 
            this.chkLoggingConstant.AutoSize = true;
            this.chkLoggingConstant.Location = new System.Drawing.Point(19, 166);
            this.chkLoggingConstant.Name = "chkLoggingConstant";
            this.chkLoggingConstant.Size = new System.Drawing.Size(72, 16);
            this.chkLoggingConstant.TabIndex = 159;
            this.chkLoggingConstant.Text = "상시저장";
            this.chkLoggingConstant.UseVisualStyleBackColor = true;
            this.chkLoggingConstant.CheckedChanged += new System.EventHandler(this.chkLoggingConstant_CheckedChanged);
            this.chkLoggingConstant.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ModbusSet_MouseClick);
            // 
            // chkLoggingCtn
            // 
            this.chkLoggingCtn.AutoSize = true;
            this.chkLoggingCtn.Location = new System.Drawing.Point(20, 117);
            this.chkLoggingCtn.Name = "chkLoggingCtn";
            this.chkLoggingCtn.Size = new System.Drawing.Size(72, 16);
            this.chkLoggingCtn.TabIndex = 158;
            this.chkLoggingCtn.Text = "연속저장";
            this.chkLoggingCtn.UseVisualStyleBackColor = true;
            this.chkLoggingCtn.CheckedChanged += new System.EventHandler(this.chkLoggingCtn_CheckedChanged);
            this.chkLoggingCtn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ModbusSet_MouseClick);
            // 
            // chkLoggingAvg
            // 
            this.chkLoggingAvg.AutoSize = true;
            this.chkLoggingAvg.Location = new System.Drawing.Point(20, 70);
            this.chkLoggingAvg.Name = "chkLoggingAvg";
            this.chkLoggingAvg.Size = new System.Drawing.Size(72, 16);
            this.chkLoggingAvg.TabIndex = 157;
            this.chkLoggingAvg.Text = "평균저장";
            this.chkLoggingAvg.UseVisualStyleBackColor = true;
            this.chkLoggingAvg.CheckedChanged += new System.EventHandler(this.chkLoggingAvg_CheckedChanged);
            this.chkLoggingAvg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ModbusSet_MouseClick);
            // 
            // chkLoggingAvgCtn
            // 
            this.chkLoggingAvgCtn.AutoSize = true;
            this.chkLoggingAvgCtn.Location = new System.Drawing.Point(20, 36);
            this.chkLoggingAvgCtn.Name = "chkLoggingAvgCtn";
            this.chkLoggingAvgCtn.Size = new System.Drawing.Size(144, 16);
            this.chkLoggingAvgCtn.TabIndex = 156;
            this.chkLoggingAvgCtn.Text = "평균저장  &&  연속저장";
            this.chkLoggingAvgCtn.UseVisualStyleBackColor = true;
            this.chkLoggingAvgCtn.CheckedChanged += new System.EventHandler(this.chkLoggingAvgCtn_CheckedChanged);
            this.chkLoggingAvgCtn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ModbusSet_MouseClick);
            // 
            // tbLoggingTimeAvg
            // 
            this.tbLoggingTimeAvg.Location = new System.Drawing.Point(100, 68);
            this.tbLoggingTimeAvg.Multiline = true;
            this.tbLoggingTimeAvg.Name = "tbLoggingTimeAvg";
            this.tbLoggingTimeAvg.Size = new System.Drawing.Size(59, 20);
            this.tbLoggingTimeAvg.TabIndex = 152;
            this.tbLoggingTimeAvg.Text = "30";
            this.tbLoggingTimeAvg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbLoggingTimeAvg.TextChanged += new System.EventHandler(this.tbLoggingTimeAvg_TextChanged);
            // 
            // tbLoggingTimeCtn
            // 
            this.tbLoggingTimeCtn.Location = new System.Drawing.Point(100, 114);
            this.tbLoggingTimeCtn.Multiline = true;
            this.tbLoggingTimeCtn.Name = "tbLoggingTimeCtn";
            this.tbLoggingTimeCtn.Size = new System.Drawing.Size(60, 20);
            this.tbLoggingTimeCtn.TabIndex = 149;
            this.tbLoggingTimeCtn.Text = "30";
            this.tbLoggingTimeCtn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbLoggingTimeCtn.TextChanged += new System.EventHandler(this.tbLoggingTimeCtn_TextChanged);
            // 
            // progressLoggingCtn
            // 
            this.progressLoggingCtn.Location = new System.Drawing.Point(20, 134);
            this.progressLoggingCtn.Maximum = 30;
            this.progressLoggingCtn.Name = "progressLoggingCtn";
            this.progressLoggingCtn.Size = new System.Drawing.Size(160, 10);
            this.progressLoggingCtn.TabIndex = 155;
            // 
            // progressLoggingAvg
            // 
            this.progressLoggingAvg.Location = new System.Drawing.Point(20, 89);
            this.progressLoggingAvg.Maximum = 30;
            this.progressLoggingAvg.Name = "progressLoggingAvg";
            this.progressLoggingAvg.Size = new System.Drawing.Size(159, 10);
            this.progressLoggingAvg.TabIndex = 151;
            // 
            // label64
            // 
            this.label64.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label64.Location = new System.Drawing.Point(163, 69);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(18, 21);
            this.label64.TabIndex = 153;
            this.label64.Text = "초";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label41
            // 
            this.label41.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label41.Location = new System.Drawing.Point(161, 115);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(19, 21);
            this.label41.TabIndex = 150;
            this.label41.Text = "초";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label37
            // 
            this.label37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(109)))), ((int)(((byte)(172)))));
            this.label37.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label37.ForeColor = System.Drawing.Color.White;
            this.label37.Location = new System.Drawing.Point(-1, 1);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(213, 21);
            this.label37.TabIndex = 131;
            this.label37.Text = "Data Save";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDataFolderOpen
            // 
            this.btnDataFolderOpen.Location = new System.Drawing.Point(93, 162);
            this.btnDataFolderOpen.Name = "btnDataFolderOpen";
            this.btnDataFolderOpen.Size = new System.Drawing.Size(112, 23);
            this.btnDataFolderOpen.TabIndex = 130;
            this.btnDataFolderOpen.Text = "데이터폴더 열기";
            this.btnDataFolderOpen.UseVisualStyleBackColor = true;
            this.btnDataFolderOpen.Click += new System.EventHandler(this.btnDataFolderOpen_Click);
            // 
            // label29
            // 
            this.label29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(109)))), ((int)(((byte)(172)))));
            this.label29.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(-4, 699);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(213, 21);
            this.label29.TabIndex = 90;
            this.label29.Text = "Mexa Status";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label29.Visible = false;
            // 
            // pnlDAQ
            // 
            this.pnlDAQ.Controls.Add(this.label26);
            this.pnlDAQ.Controls.Add(this.btnOsirisStatus);
            this.pnlDAQ.Controls.Add(this.label33);
            this.pnlDAQ.Controls.Add(this.btnModbusStatus);
            this.pnlDAQ.Controls.Add(this.label30);
            this.pnlDAQ.Controls.Add(this.btnACSStatus);
            this.pnlDAQ.Controls.Add(this.label31);
            this.pnlDAQ.Controls.Add(this.btnMexaStatus);
            this.pnlDAQ.Controls.Add(this.btnDAQ);
            this.pnlDAQ.Controls.Add(this.label21);
            this.pnlDAQ.Controls.Add(this.label22);
            this.pnlDAQ.Controls.Add(this.label23);
            this.pnlDAQ.Controls.Add(this.btnECUStatus);
            this.pnlDAQ.Controls.Add(this.label24);
            this.pnlDAQ.Controls.Add(this.btnSMStatus);
            this.pnlDAQ.Location = new System.Drawing.Point(0, 340);
            this.pnlDAQ.Name = "pnlDAQ";
            this.pnlDAQ.Size = new System.Drawing.Size(211, 301);
            this.pnlDAQ.TabIndex = 0;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label26.Location = new System.Drawing.Point(6, 231);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(94, 21);
            this.label26.TabIndex = 129;
            this.label26.Text = "OSIRIS";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnOsirisStatus
            // 
            this.btnOsirisStatus.BackColor = System.Drawing.Color.Silver;
            this.btnOsirisStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOsirisStatus.BackgroundImage")));
            this.btnOsirisStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnOsirisStatus.Enabled = false;
            this.btnOsirisStatus.FlatAppearance.BorderSize = 0;
            this.btnOsirisStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOsirisStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnOsirisStatus.Location = new System.Drawing.Point(20, 254);
            this.btnOsirisStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOsirisStatus.Name = "btnOsirisStatus";
            this.btnOsirisStatus.Size = new System.Drawing.Size(65, 45);
            this.btnOsirisStatus.TabIndex = 130;
            this.btnOsirisStatus.UseVisualStyleBackColor = false;
            // 
            // label33
            // 
            this.label33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(109)))), ((int)(((byte)(172)))));
            this.label33.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label33.ForeColor = System.Drawing.Color.White;
            this.label33.Location = new System.Drawing.Point(0, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(213, 21);
            this.label33.TabIndex = 128;
            this.label33.Text = "Data Acquisition Server";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnModbusStatus
            // 
            this.btnModbusStatus.BackColor = System.Drawing.Color.Silver;
            this.btnModbusStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnModbusStatus.BackgroundImage")));
            this.btnModbusStatus.Enabled = false;
            this.btnModbusStatus.FlatAppearance.BorderSize = 0;
            this.btnModbusStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModbusStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnModbusStatus.Location = new System.Drawing.Point(125, 186);
            this.btnModbusStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnModbusStatus.Name = "btnModbusStatus";
            this.btnModbusStatus.Size = new System.Drawing.Size(65, 45);
            this.btnModbusStatus.TabIndex = 127;
            this.btnModbusStatus.UseVisualStyleBackColor = false;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label30.Location = new System.Drawing.Point(5, 163);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(94, 21);
            this.label30.TabIndex = 124;
            this.label30.Text = "ACS";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnACSStatus
            // 
            this.btnACSStatus.BackColor = System.Drawing.Color.Silver;
            this.btnACSStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnACSStatus.BackgroundImage")));
            this.btnACSStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnACSStatus.Enabled = false;
            this.btnACSStatus.FlatAppearance.BorderSize = 0;
            this.btnACSStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnACSStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnACSStatus.Location = new System.Drawing.Point(19, 186);
            this.btnACSStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnACSStatus.Name = "btnACSStatus";
            this.btnACSStatus.Size = new System.Drawing.Size(65, 45);
            this.btnACSStatus.TabIndex = 125;
            this.btnACSStatus.UseVisualStyleBackColor = false;
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label31.Location = new System.Drawing.Point(111, 163);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(94, 21);
            this.label31.TabIndex = 126;
            this.label31.Text = "화진 DAQ";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMexaStatus
            // 
            this.btnMexaStatus.BackColor = System.Drawing.Color.Silver;
            this.btnMexaStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMexaStatus.BackgroundImage")));
            this.btnMexaStatus.Enabled = false;
            this.btnMexaStatus.FlatAppearance.BorderSize = 0;
            this.btnMexaStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMexaStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMexaStatus.Location = new System.Drawing.Point(125, 115);
            this.btnMexaStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnMexaStatus.Name = "btnMexaStatus";
            this.btnMexaStatus.Size = new System.Drawing.Size(65, 45);
            this.btnMexaStatus.TabIndex = 123;
            this.btnMexaStatus.UseVisualStyleBackColor = false;
            // 
            // btnDAQ
            // 
            this.btnDAQ.BackgroundImage = global::AtomUI.Properties.Resources.off버튼;
            this.btnDAQ.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnDAQ.FlatAppearance.BorderSize = 0;
            this.btnDAQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDAQ.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDAQ.Location = new System.Drawing.Point(20, 45);
            this.btnDAQ.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnDAQ.Name = "btnDAQ";
            this.btnDAQ.Size = new System.Drawing.Size(65, 45);
            this.btnDAQ.TabIndex = 119;
            this.btnDAQ.UseVisualStyleBackColor = false;
            this.btnDAQ.Click += new System.EventHandler(this.btnDAQ_Click);
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.Location = new System.Drawing.Point(5, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(94, 21);
            this.label21.TabIndex = 116;
            this.label21.Text = "DAQ";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label22.Location = new System.Drawing.Point(5, 92);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(94, 21);
            this.label22.TabIndex = 117;
            this.label22.Text = "ECU";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label23.Location = new System.Drawing.Point(111, 22);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(94, 21);
            this.label23.TabIndex = 118;
            this.label23.Text = "Smoke Meter";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnECUStatus
            // 
            this.btnECUStatus.BackColor = System.Drawing.Color.Silver;
            this.btnECUStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnECUStatus.BackgroundImage")));
            this.btnECUStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnECUStatus.Enabled = false;
            this.btnECUStatus.FlatAppearance.BorderSize = 0;
            this.btnECUStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnECUStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnECUStatus.Location = new System.Drawing.Point(19, 115);
            this.btnECUStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnECUStatus.Name = "btnECUStatus";
            this.btnECUStatus.Size = new System.Drawing.Size(65, 45);
            this.btnECUStatus.TabIndex = 120;
            this.btnECUStatus.UseVisualStyleBackColor = false;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label24.Location = new System.Drawing.Point(111, 92);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(94, 21);
            this.label24.TabIndex = 122;
            this.label24.Text = "MEXA";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSMStatus
            // 
            this.btnSMStatus.BackColor = System.Drawing.Color.Silver;
            this.btnSMStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSMStatus.BackgroundImage")));
            this.btnSMStatus.Enabled = false;
            this.btnSMStatus.FlatAppearance.BorderSize = 0;
            this.btnSMStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSMStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSMStatus.Location = new System.Drawing.Point(125, 45);
            this.btnSMStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSMStatus.Name = "btnSMStatus";
            this.btnSMStatus.Size = new System.Drawing.Size(65, 45);
            this.btnSMStatus.TabIndex = 121;
            this.btnSMStatus.UseVisualStyleBackColor = false;
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label19.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(0, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(219, 21);
            this.label19.TabIndex = 89;
            this.label19.Text = "System Status";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(113, 420);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 21);
            this.label12.TabIndex = 109;
            this.label12.Text = "T/N";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.Location = new System.Drawing.Point(113, 502);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 21);
            this.label13.TabIndex = 108;
            this.label13.Text = "N/T";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(113, 338);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(94, 21);
            this.label14.TabIndex = 107;
            this.label14.Text = "N/α";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.Location = new System.Drawing.Point(113, 256);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 21);
            this.label15.TabIndex = 106;
            this.label15.Text = "T/α";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.Location = new System.Drawing.Point(105, 174);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(111, 21);
            this.label16.TabIndex = 104;
            this.label16.Text = "IDLE CONTROL";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.Location = new System.Drawing.Point(113, 92);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(94, 21);
            this.label17.TabIndex = 105;
            this.label17.Text = "IDLE";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(113, 12);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 21);
            this.label11.TabIndex = 96;
            this.label11.Text = "DATA SAVE";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(7, 338);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 21);
            this.label10.TabIndex = 95;
            this.label10.Text = "REMOTE";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(7, 420);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 21);
            this.label9.TabIndex = 94;
            this.label9.Text = "AUTO";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(7, 256);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 21);
            this.label8.TabIndex = 93;
            this.label8.Text = "MANUAL";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(7, 174);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 21);
            this.label7.TabIndex = 92;
            this.label7.Text = "ENGINE ON";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(7, 92);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 21);
            this.label6.TabIndex = 91;
            this.label6.Text = "DYNO ON";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(7, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 21);
            this.label5.TabIndex = 90;
            this.label5.Text = "CONNECTION";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tcMain
            // 
            this.tcMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcMain.Controls.Add(this.tabPage1);
            this.tcMain.Location = new System.Drawing.Point(238, 137);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(959, 905);
            this.tcMain.TabIndex = 94;
            this.tcMain.SelectedIndexChanged += new System.EventHandler(this.tcMain_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.tvSystemInfo);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(951, 879);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "System";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.lvOutput);
            this.panel2.Controls.Add(this.lvInput);
            this.panel2.Controls.Add(this.lbAlarm);
            this.panel2.Controls.Add(this.tbAlarmCNT);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(307, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(638, 867);
            this.panel2.TabIndex = 93;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label18.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(0, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(638, 21);
            this.label18.TabIndex = 87;
            this.label18.Text = "Operating Status";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lvOutput
            // 
            this.lvOutput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lvOutput.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lvOutput.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lvOutput.FullRowSelect = true;
            this.lvOutput.GridLines = true;
            listViewGroup1.Header = "ListViewGroup";
            listViewGroup1.Name = "Global Variables";
            listViewGroup2.Header = "ListViewGroup";
            listViewGroup2.Name = "Local Variables";
            this.lvOutput.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2});
            this.lvOutput.HideSelection = false;
            this.lvOutput.Location = new System.Drawing.Point(325, 48);
            this.lvOutput.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lvOutput.MultiSelect = false;
            this.lvOutput.Name = "lvOutput";
            this.lvOutput.ShowGroups = false;
            this.lvOutput.Size = new System.Drawing.Size(300, 507);
            this.lvOutput.TabIndex = 85;
            this.lvOutput.UseCompatibleStateImageBehavior = false;
            this.lvOutput.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 147;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Values";
            this.columnHeader2.Width = 135;
            // 
            // lvInput
            // 
            this.lvInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lvInput.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Header1,
            this.Header2});
            this.lvInput.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lvInput.FullRowSelect = true;
            this.lvInput.GridLines = true;
            listViewGroup3.Header = "ListViewGroup";
            listViewGroup3.Name = "Global Variables";
            listViewGroup4.Header = "ListViewGroup";
            listViewGroup4.Name = "Local Variables";
            this.lvInput.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup3,
            listViewGroup4});
            this.lvInput.HideSelection = false;
            this.lvInput.Location = new System.Drawing.Point(13, 48);
            this.lvInput.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lvInput.MultiSelect = false;
            this.lvInput.Name = "lvInput";
            this.lvInput.ShowGroups = false;
            this.lvInput.Size = new System.Drawing.Size(300, 507);
            this.lvInput.TabIndex = 84;
            this.lvInput.UseCompatibleStateImageBehavior = false;
            this.lvInput.View = System.Windows.Forms.View.Details;
            // 
            // Header1
            // 
            this.Header1.Text = "Name";
            this.Header1.Width = 149;
            // 
            // Header2
            // 
            this.Header2.Text = "Values";
            this.Header2.Width = 137;
            // 
            // lbAlarm
            // 
            this.lbAlarm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbAlarm.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbAlarm.FormattingEnabled = true;
            this.lbAlarm.ItemHeight = 15;
            this.lbAlarm.Location = new System.Drawing.Point(13, 598);
            this.lbAlarm.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lbAlarm.Name = "lbAlarm";
            this.lbAlarm.Size = new System.Drawing.Size(612, 259);
            this.lbAlarm.TabIndex = 83;
            // 
            // tbAlarmCNT
            // 
            this.tbAlarmCNT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbAlarmCNT.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbAlarmCNT.Location = new System.Drawing.Point(551, 573);
            this.tbAlarmCNT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbAlarmCNT.Name = "tbAlarmCNT";
            this.tbAlarmCNT.Size = new System.Drawing.Size(74, 23);
            this.tbAlarmCNT.TabIndex = 82;
            this.tbAlarmCNT.Text = "0";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(461, 573);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 21);
            this.label4.TabIndex = 81;
            this.label4.Text = "Alarm Count";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(11, 573);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 21);
            this.label3.TabIndex = 80;
            this.label3.Text = "Alarm List";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(323, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 21);
            this.label2.TabIndex = 79;
            this.label2.Text = "Ouput Variables";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(11, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 21);
            this.label1.TabIndex = 78;
            this.label1.Text = "Input Variables";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label20.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(6, 7);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(295, 21);
            this.label20.TabIndex = 92;
            this.label20.Text = "System Configuration";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tvSystemInfo
            // 
            this.tvSystemInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvSystemInfo.Location = new System.Drawing.Point(6, 30);
            this.tvSystemInfo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tvSystemInfo.Name = "tvSystemInfo";
            this.tvSystemInfo.Size = new System.Drawing.Size(295, 843);
            this.tvSystemInfo.TabIndex = 91;
            // 
            // tmrDrawing
            // 
            this.tmrDrawing.Interval = 500;
            this.tmrDrawing.Tick += new System.EventHandler(this.tmrDrawing_Tick);
            // 
            // pnlACS
            // 
            this.pnlACS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(244)))), ((int)(((byte)(253)))));
            this.pnlACS.Controls.Add(this.btnIDLE);
            this.pnlACS.Controls.Add(this.btnConnection);
            this.pnlACS.Controls.Add(this.btnNT);
            this.pnlACS.Controls.Add(this.label5);
            this.pnlACS.Controls.Add(this.btnTN);
            this.pnlACS.Controls.Add(this.label6);
            this.pnlACS.Controls.Add(this.btnNA);
            this.pnlACS.Controls.Add(this.label7);
            this.pnlACS.Controls.Add(this.btnTA);
            this.pnlACS.Controls.Add(this.label8);
            this.pnlACS.Controls.Add(this.btnIdleControl);
            this.pnlACS.Controls.Add(this.label9);
            this.pnlACS.Controls.Add(this.label12);
            this.pnlACS.Controls.Add(this.label10);
            this.pnlACS.Controls.Add(this.label13);
            this.pnlACS.Controls.Add(this.label11);
            this.pnlACS.Controls.Add(this.label14);
            this.pnlACS.Controls.Add(this.btnDyno);
            this.pnlACS.Controls.Add(this.label15);
            this.pnlACS.Controls.Add(this.btnEngine);
            this.pnlACS.Controls.Add(this.label16);
            this.pnlACS.Controls.Add(this.btnManual);
            this.pnlACS.Controls.Add(this.label17);
            this.pnlACS.Controls.Add(this.btnRemote);
            this.pnlACS.Controls.Add(this.btnData);
            this.pnlACS.Controls.Add(this.btnAuto);
            this.pnlACS.Location = new System.Drawing.Point(628, 128);
            this.pnlACS.Name = "pnlACS";
            this.pnlACS.Size = new System.Drawing.Size(211, 592);
            this.pnlACS.TabIndex = 116;
            this.pnlACS.Visible = false;
            // 
            // btnIDLE
            // 
            this.btnIDLE.BackColor = System.Drawing.Color.Silver;
            this.btnIDLE.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnIDLE.BackgroundImage")));
            this.btnIDLE.Enabled = false;
            this.btnIDLE.FlatAppearance.BorderSize = 0;
            this.btnIDLE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIDLE.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIDLE.Location = new System.Drawing.Point(127, 115);
            this.btnIDLE.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnIDLE.Name = "btnIDLE";
            this.btnIDLE.Size = new System.Drawing.Size(65, 45);
            this.btnIDLE.TabIndex = 115;
            this.btnIDLE.UseVisualStyleBackColor = false;
            // 
            // btnConnection
            // 
            this.btnConnection.BackgroundImage = global::AtomUI.Properties.Resources.off버튼;
            this.btnConnection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnConnection.FlatAppearance.BorderSize = 0;
            this.btnConnection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConnection.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnConnection.Location = new System.Drawing.Point(22, 35);
            this.btnConnection.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnConnection.Name = "btnConnection";
            this.btnConnection.Size = new System.Drawing.Size(65, 45);
            this.btnConnection.TabIndex = 97;
            this.btnConnection.UseVisualStyleBackColor = false;
            this.btnConnection.Click += new System.EventHandler(this.btnConnection_Click);
            // 
            // btnNT
            // 
            this.btnNT.BackColor = System.Drawing.Color.Silver;
            this.btnNT.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNT.BackgroundImage")));
            this.btnNT.Enabled = false;
            this.btnNT.FlatAppearance.BorderSize = 0;
            this.btnNT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNT.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnNT.Location = new System.Drawing.Point(127, 524);
            this.btnNT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnNT.Name = "btnNT";
            this.btnNT.Size = new System.Drawing.Size(65, 45);
            this.btnNT.TabIndex = 114;
            this.btnNT.UseVisualStyleBackColor = false;
            // 
            // btnTN
            // 
            this.btnTN.BackColor = System.Drawing.Color.Silver;
            this.btnTN.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTN.BackgroundImage")));
            this.btnTN.Enabled = false;
            this.btnTN.FlatAppearance.BorderSize = 0;
            this.btnTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTN.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnTN.Location = new System.Drawing.Point(127, 443);
            this.btnTN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnTN.Name = "btnTN";
            this.btnTN.Size = new System.Drawing.Size(65, 45);
            this.btnTN.TabIndex = 113;
            this.btnTN.UseVisualStyleBackColor = false;
            // 
            // btnNA
            // 
            this.btnNA.BackColor = System.Drawing.Color.Silver;
            this.btnNA.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNA.BackgroundImage")));
            this.btnNA.Enabled = false;
            this.btnNA.FlatAppearance.BorderSize = 0;
            this.btnNA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNA.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnNA.Location = new System.Drawing.Point(127, 361);
            this.btnNA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnNA.Name = "btnNA";
            this.btnNA.Size = new System.Drawing.Size(65, 45);
            this.btnNA.TabIndex = 112;
            this.btnNA.UseVisualStyleBackColor = false;
            // 
            // btnTA
            // 
            this.btnTA.BackColor = System.Drawing.Color.Silver;
            this.btnTA.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTA.BackgroundImage")));
            this.btnTA.Enabled = false;
            this.btnTA.FlatAppearance.BorderSize = 0;
            this.btnTA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTA.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnTA.Location = new System.Drawing.Point(127, 279);
            this.btnTA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnTA.Name = "btnTA";
            this.btnTA.Size = new System.Drawing.Size(65, 45);
            this.btnTA.TabIndex = 111;
            this.btnTA.UseVisualStyleBackColor = false;
            // 
            // btnIdleControl
            // 
            this.btnIdleControl.BackColor = System.Drawing.Color.Silver;
            this.btnIdleControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnIdleControl.BackgroundImage")));
            this.btnIdleControl.Enabled = false;
            this.btnIdleControl.FlatAppearance.BorderSize = 0;
            this.btnIdleControl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIdleControl.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIdleControl.Location = new System.Drawing.Point(127, 197);
            this.btnIdleControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnIdleControl.Name = "btnIdleControl";
            this.btnIdleControl.Size = new System.Drawing.Size(65, 45);
            this.btnIdleControl.TabIndex = 110;
            this.btnIdleControl.UseVisualStyleBackColor = false;
            // 
            // btnDyno
            // 
            this.btnDyno.BackColor = System.Drawing.Color.Silver;
            this.btnDyno.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDyno.BackgroundImage")));
            this.btnDyno.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnDyno.Enabled = false;
            this.btnDyno.FlatAppearance.BorderSize = 0;
            this.btnDyno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDyno.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDyno.Location = new System.Drawing.Point(21, 115);
            this.btnDyno.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnDyno.Name = "btnDyno";
            this.btnDyno.Size = new System.Drawing.Size(65, 45);
            this.btnDyno.TabIndex = 98;
            this.btnDyno.UseVisualStyleBackColor = false;
            this.btnDyno.Click += new System.EventHandler(this.btnDyno_Click);
            // 
            // btnEngine
            // 
            this.btnEngine.BackColor = System.Drawing.Color.Silver;
            this.btnEngine.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEngine.BackgroundImage")));
            this.btnEngine.Enabled = false;
            this.btnEngine.FlatAppearance.BorderSize = 0;
            this.btnEngine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEngine.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnEngine.Location = new System.Drawing.Point(21, 199);
            this.btnEngine.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnEngine.Name = "btnEngine";
            this.btnEngine.Size = new System.Drawing.Size(65, 45);
            this.btnEngine.TabIndex = 99;
            this.btnEngine.UseVisualStyleBackColor = false;
            // 
            // btnManual
            // 
            this.btnManual.BackColor = System.Drawing.Color.Silver;
            this.btnManual.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnManual.BackgroundImage")));
            this.btnManual.Enabled = false;
            this.btnManual.FlatAppearance.BorderSize = 0;
            this.btnManual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManual.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnManual.Location = new System.Drawing.Point(21, 279);
            this.btnManual.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnManual.Name = "btnManual";
            this.btnManual.Size = new System.Drawing.Size(65, 45);
            this.btnManual.TabIndex = 100;
            this.btnManual.UseVisualStyleBackColor = false;
            // 
            // btnRemote
            // 
            this.btnRemote.BackColor = System.Drawing.Color.Silver;
            this.btnRemote.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRemote.BackgroundImage")));
            this.btnRemote.Enabled = false;
            this.btnRemote.FlatAppearance.BorderSize = 0;
            this.btnRemote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemote.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnRemote.Location = new System.Drawing.Point(21, 361);
            this.btnRemote.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRemote.Name = "btnRemote";
            this.btnRemote.Size = new System.Drawing.Size(65, 45);
            this.btnRemote.TabIndex = 101;
            this.btnRemote.UseVisualStyleBackColor = false;
            // 
            // btnData
            // 
            this.btnData.BackColor = System.Drawing.Color.Silver;
            this.btnData.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnData.BackgroundImage")));
            this.btnData.Enabled = false;
            this.btnData.FlatAppearance.BorderSize = 0;
            this.btnData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnData.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnData.Location = new System.Drawing.Point(127, 35);
            this.btnData.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnData.Name = "btnData";
            this.btnData.Size = new System.Drawing.Size(65, 45);
            this.btnData.TabIndex = 103;
            this.btnData.UseVisualStyleBackColor = false;
            this.btnData.Click += new System.EventHandler(this.btnData_Click);
            // 
            // btnAuto
            // 
            this.btnAuto.BackColor = System.Drawing.Color.Silver;
            this.btnAuto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAuto.BackgroundImage")));
            this.btnAuto.Enabled = false;
            this.btnAuto.FlatAppearance.BorderSize = 0;
            this.btnAuto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAuto.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAuto.Location = new System.Drawing.Point(21, 443);
            this.btnAuto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAuto.Name = "btnAuto";
            this.btnAuto.Size = new System.Drawing.Size(65, 45);
            this.btnAuto.TabIndex = 102;
            this.btnAuto.UseVisualStyleBackColor = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // pnlTolCenter
            // 
            this.pnlTolCenter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlTolCenter.BackgroundImage = global::AtomUI.Properties.Resources.중간1x100_STXENGINE;
            this.pnlTolCenter.Location = new System.Drawing.Point(595, 0);
            this.pnlTolCenter.Name = "pnlTolCenter";
            this.pnlTolCenter.Size = new System.Drawing.Size(86, 100);
            this.pnlTolCenter.TabIndex = 91;
            // 
            // pnlTolRight
            // 
            this.pnlTolRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlTolRight.BackgroundImage = global::AtomUI.Properties.Resources.우측583x100_STXENGINE;
            this.pnlTolRight.Location = new System.Drawing.Point(675, 0);
            this.pnlTolRight.Name = "pnlTolRight";
            this.pnlTolRight.Size = new System.Drawing.Size(533, 100);
            this.pnlTolRight.TabIndex = 92;
            // 
            // pnlTolLeft
            // 
            this.pnlTolLeft.BackgroundImage = global::AtomUI.Properties.Resources.좌측600x100_STXENGINE;
            this.pnlTolLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlTolLeft.Name = "pnlTolLeft";
            this.pnlTolLeft.Size = new System.Drawing.Size(597, 100);
            this.pnlTolLeft.TabIndex = 31;
            // 
            // AtomUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(244)))), ((int)(((byte)(253)))));
            this.ClientSize = new System.Drawing.Size(1208, 1053);
            this.Controls.Add(this.pnlACS);
            this.Controls.Add(this.pnlControl);
            this.Controls.Add(this.tcMain);
            this.Controls.Add(this.pnlTolCenter);
            this.Controls.Add(this.pnlTolRight);
            this.Controls.Add(this.menuMain);
            this.Controls.Add(this.pnlTolLeft);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "AtomUI";
            this.Text = "Atom UI Ver 0.1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AtomUI_Paint);
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            this.ControllerMenuStrip.ResumeLayout(false);
            this.pnlControl.ResumeLayout(false);
            this.pnlMexa.ResumeLayout(false);
            this.pnlStatus.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.pnlDAQ.ResumeLayout(false);
            this.tcMain.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnlACS.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStripMenuItem connectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem connectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disconnectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem controllerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem controllerInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moduleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem channelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem variableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem variableDefineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem variableMappingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculatingVariableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem variableInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monitoringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customMonitoringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem predefinedMonitoringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inputMonitorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputMonitorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem calculationValueMonitorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customizeMonitoringViewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem predefinedVariablesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alarmDefineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calibrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HMIMonitorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 새로만들기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem controlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stepModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profileDefineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runStepModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transientModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modeDefineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runTransientModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pIDTunnerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataLoggingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitProgramToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem systemSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configUploadToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip ControllerMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem systemSetupToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem remoteSignalRangeToolStripMenuItem;
        private System.Windows.Forms.Panel pnlTolLeft;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Panel pnlControl;
        private System.Windows.Forms.Button btnIDLE;
        private System.Windows.Forms.Button btnNT;
        private System.Windows.Forms.Button btnTN;
        private System.Windows.Forms.Button btnNA;
        private System.Windows.Forms.Button btnTA;
        private System.Windows.Forms.Button btnIdleControl;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnData;
        private System.Windows.Forms.Button btnAuto;
        private System.Windows.Forms.Button btnRemote;
        private System.Windows.Forms.Button btnManual;
        private System.Windows.Forms.Button btnEngine;
        private System.Windows.Forms.Button btnDyno;
        private System.Windows.Forms.Button btnConnection;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel pnlTolCenter;
        private System.Windows.Forms.Panel pnlTolRight;
        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ListView lvOutput;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ListView lvInput;
        private System.Windows.Forms.ColumnHeader Header1;
        private System.Windows.Forms.ColumnHeader Header2;
        private System.Windows.Forms.ListBox lbAlarm;
        private System.Windows.Forms.TextBox tbAlarmCNT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TreeView tvSystemInfo;
        private System.Windows.Forms.Timer tmrDrawing;
        private System.Windows.Forms.Panel pnlACS;
        private System.Windows.Forms.Panel pnlMexa;
        private System.Windows.Forms.Panel pnlDAQ;
        private System.Windows.Forms.Button btnMexaStatus;
        private System.Windows.Forms.Button btnDAQ;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button btnECUStatus;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button btnSMStatus;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button btnModbusStatus;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button btnACSStatus;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ToolStripMenuItem iOControlDemoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 기존파일추가ToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.Button btnDataFolderOpen;
        private System.Windows.Forms.Panel pnlStatus;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Button btnHornOff;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Button btnAlarm;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button btnTN2;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button btnTA2;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Button btnRemote2;
        private System.Windows.Forms.Button btnConnect2;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ToolStripMenuItem controlPadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smokeMeterControlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fuelMeterControlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blowbyMeterControlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mexaControlToolStripMenuItem;
        private System.Windows.Forms.Button btnNA2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button btnOsirisStatus;
        private System.Windows.Forms.CheckBox chkLoggingConstant;
        private System.Windows.Forms.CheckBox chkLoggingCtn;
        private System.Windows.Forms.CheckBox chkLoggingAvg;
        private System.Windows.Forms.CheckBox chkLoggingAvgCtn;
        private System.Windows.Forms.TextBox tbLoggingTimeAvg;
        private System.Windows.Forms.TextBox tbLoggingTimeCtn;
        private System.Windows.Forms.ProgressBar progressLoggingCtn;
        private System.Windows.Forms.ProgressBar progressLoggingAvg;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label41;
    }
}

