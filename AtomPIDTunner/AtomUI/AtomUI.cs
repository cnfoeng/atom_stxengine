﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ACS.SPiiPlusNET;
using System.Runtime.InteropServices; //for COMException class
using AtomUI.Class;
using System.IO;
using System.Threading;

using System.Diagnostics;
namespace AtomUI
{
    public partial class AtomUI : Form
    {
        PerforTimer timerDataLoggingAvg = null;
        PerforTimer timerDataLoggingCtn = null;
        PerforTimer timerDataLoggingConstant = null;
        Thread thAvgLog;
        Thread thInstantLog;
        Thread thConstantLog;
        ControlForm.StepRun m_StepRun;

        bool DAQConnected = false;
        List<MonitorForm> cMonitor = new List<MonitorForm>();

        public void MonitoringLoad(MonitorForm monitor)
        {
            //Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.MonInfoDir + "\\" + Ethercat.sysInfo.sysSettings.MonInfoFileName

            if (monitor.cMonitor == null)
            {
                monitor.cMonitor = new MonitoringForm.CustomMonitor(monitor.MonitorName);
                monitor.cMonitor.FormClosed += CMonitor_FormClosed;
            }

            if (monitor.isDovking)
            {
                if (monitor.tab == null)
                {
                    TabPage tab = new TabPage(monitor.MonitorName);
                    Panel pnl = new Panel();
                    tab.Controls.Add(pnl);
                    pnl.Dock = DockStyle.Fill;
                    tcMain.TabPages.Add(tab);

                    monitor.tab = tab;
                    monitor.pnlMon = pnl;
                }

                if (monitor.cMonitor.TopLevel == true)
                {
                    monitor.cMonitor.FormBorderStyle = FormBorderStyle.None;
                    monitor.cMonitor.TopLevel = false;
                    monitor.cMonitor.Top = 0;
                    monitor.cMonitor.Left = 0;
                    //cMonitor.Height = tcMain.SelectedTab.Height;
                    //cMonitor.Width = tcMain.SelectedTab.Width;
                    monitor.cMonitor.Height = 1080;
                    monitor.cMonitor.Width = 1920;
                    monitor.cMonitor.WindowState = FormWindowState.Normal;

                    //cMonitor.Dock = DockStyle.Fill;
                    //cMonitor.Anchor = AnchorStyles.Left & AnchorStyles.Bottom & AnchorStyles.Right & AnchorStyles.Top;
                    if (monitor.cMonitor.Parent != monitor.pnlMon)
                        monitor.pnlMon.Controls.Add(monitor.cMonitor);
                }
            }
            else
            {
                monitor.cMonitor.FormBorderStyle = FormBorderStyle.Sizable;
                monitor.cMonitor.Parent = null;
                monitor.cMonitor.TopLevel = true;
                monitor.cMonitor.WindowState = FormWindowState.Maximized;

                if (monitor.tab != null)
                    tcMain.TabPages.Remove(monitor.tab);
                monitor.tab = null;
            }

            monitor.cMonitor.Show();

            if (monitor.isDovking)
            {
                monitor.cMonitor.monitor.MonitorList.monStatus = MonitorStatus.Tab;
                monitor.cMonitor.monitor.SaveMonInfo(monitor.MonitorName);
            }
            else
            {
                monitor.cMonitor.monitor.MonitorList.monStatus = MonitorStatus.Windows;
                monitor.cMonitor.monitor.SaveMonInfo(monitor.MonitorName);
            }
        }

        private void CMonitor_FormClosed(object sender, FormClosedEventArgs e)
        {
            foreach(MonitorForm mon in cMonitor)
            {
                if (mon.MonitorName == ((MonitoringForm.CustomMonitor)sender).Text)
                {
                    mon.cMonitor.monitor.MonitorList.monStatus = MonitorStatus.Disable;
                    mon.cMonitor.monitor.SaveMonInfo(mon.MonitorName);
                    mon.cMonitor = null;
                }
            }
        }

        public AtomUI()
        {
            InitializeComponent();

            using (Process p = Process.GetCurrentProcess())
                p.PriorityClass = ProcessPriorityClass.RealTime;

            InitConstantLog();
            InitCtnLog();
            InitAvgLog();
            DaqData.StartThr_Save();


            lvInput.GetType().GetProperty("DoubleBuffered", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).SetValue(lvInput, true, null);
            lvOutput.GetType().GetProperty("DoubleBuffered", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).SetValue(lvOutput, true, null);

        }

        public static void DrawShadow(Graphics g, Control pnlControl)
        {
            //우측상단
            g.DrawImage(Properties.Resources.topRight_Shadow, pnlControl.Right, pnlControl.Top, 5, 5);

            //우측, TextureBrush 사용
            TextureBrush tb = new TextureBrush(Properties.Resources.right_Shadow);
            tb.TranslateTransform(pnlControl.Right, 0); //이미지 반복 시작 위치를 설정합니다. (기본값은 (0,0) 입니다.)
            g.FillRectangle(tb, pnlControl.Right, pnlControl.Top + 5, 5, pnlControl.Height - 5);

            //우측하단
            g.DrawImage(Properties.Resources.bottomRight_Shadow, pnlControl.Right, pnlControl.Bottom, 5, 5);

            //하단
            tb = new TextureBrush(Properties.Resources.bottom_Shadow);
            tb.TranslateTransform(0, pnlControl.Bottom); //이미지 반복 시작 위치를 설정합니다. (기본값은 (0,0) 입니다.)
            g.FillRectangle(tb, pnlControl.Left + 5, pnlControl.Bottom, pnlControl.Width - 5, 5);

            //좌측 하단
            g.DrawImage(Properties.Resources.bottomLeft_Shadow, pnlControl.Left, pnlControl.Bottom, 5, 5);
        }

        private void MonitorSetup()
        {
            string[] MonitorNames = Ethercat.sysInfo.sysSettings.MonitorList.Split(',');

            for (int i=0; i< MonitorNames.Length-1; i++)
                MakeMonitor(MonitorNames[i]);
        }

        private void Form1_Load(object sender, System.EventArgs e)
        {
            ImageCenter = Properties.Resources.중간1x100;
            ImageLeft = Properties.Resources.좌측600x1001;
            ImageRight = Properties.Resources.우측583x100;

            //pnlTolCenter.BackgroundImage = ImageCenter;
            //pnlTolLeft.BackgroundImage = ImageLeft;
            //pnlTolRight.BackgroundImage = ImageRight;

            initSetUp();

            FillTreeView();

            acsConnectionInfo info = Ethercat.FindHardWare(HardWareType.ACS);
            ButtonStatusChange(btnDAQ, false, false);

            if (info != null)
            {
                bool AutoConnectchk = info.AutoConnect;
                if (AutoConnectchk)
                {
                    ACSConnect();
                    DAQConnect();
                 }
            }
        }

        private void initSetUp()
        {
            Ethercat.LoadSysInfo();

            // Control이 포함되어 있는지 판단해야 한다.
            //Ethercat.Ch = new Api();
            //Ethercat.Ch.COMMCHANNELCLOSED += Ch_COMMCHANNELCLOSED;
            //Ethercat.AlarmDefine();

            //DaqData.Init();
            DaqData.LoadSysInfo();

            MonitorSetup();
        }

        private void Ch_COMMCHANNELCLOSED(ulong param)
        {
            throw new NotImplementedException();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DataLoggingCtnThreadStop();
            DataLoggingAvgThreadStop();
            DataLoggingConstantThreadStop();
            DaqData.StopThr_Save();

            if (m_StepRun != null)
                m_StepRun.CloseStepRun();

            // if communication is open
            if (DAQConnected)
            {
                DAQ_CAN.CANCom.IsConnected = false;
                DAQ_CAN.CANCom.FlagCANThread = false;
                DAQDisConnect();
            }

            if (Ethercat.Ch != null && Ethercat.Ch.IsConnected)
            {
                try
                {
                    Ethercat.Ch.CloseComm();         //  Close channel communication 	
                }
                catch (COMException Ex)
                {
                    Ethercat.ErrorMsg(Ex);            //  Throw exception if this fails
                }
                catch (ACSException Ex)
                {
                    Ethercat.ErrorMsg(Ex);            //  Throw exception if this fails
                }
            }

            Ethercat.sysInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SysInfoDir + "\\" + Ethercat.sysInfo.sysSettings.SysInfoFileName);
        }
        
        private void COntrolHWStatusUpdate()
        {
            if (Ethercat.Ch.IsConnected && Ethercat.g_SystemState != null)
            {
                ButtonStatusChange(btnRemote2, (Ethercat.g_SystemState[0] == 0 ? false : true), true);
                ButtonStatusChange(btnNA2, ((Ethercat.g_SystemState[1] == 1 && Ethercat.g_SystemState[2] == 0) ? true : false), true);
                //ButtonStatusChange(btnNT2, ((Ethercat.g_SystemState[1] == 0 && Ethercat.g_SystemState[2] == 1) ? true : false), true);
                ButtonStatusChange(btnTA2, ((Ethercat.g_SystemState[1] == 0 && Ethercat.g_SystemState[2] == 0) ? true : false), true);
                ButtonStatusChange(btnTN2, ((Ethercat.g_SystemState[1] == 1 && Ethercat.g_SystemState[2] == 1) ? true : false), true);
                ButtonStatusChange(btnAlarm, (Ethercat.g_SystemState[3] == 0 ? false : true), true);
                ButtonStatusChange(btnReset, (Ethercat.g_SystemState[4] == 0 ? false : true), true);
                ButtonStatusChange(btnHornOff, (Ethercat.g_SystemState[5] == 0 ? false : true), true);
            } else
            {
                if (Ethercat.Ch.IsConnected)
                {
                    if (DAQConnected)
                    {
                        DAQDisConnect();
                    }
                    ACSDisConnect();

                    MessageBox.Show("ACS Program is invalid!");
                }
            }
            //g_FeedbackValue;
        }
        private void SystemHWStatusUpdate()
        {
            //ButtonStatusChange(btnSMStatus, DAQ.SerialComm.SmokeMeter.IsConnected, true);
            ButtonStatusChange(btnSMStatus, DAQ.SerialComm.SmokeMeter.IsConnected, true);
            ButtonStatusChange(btnMexaStatus, (DAQ_GPIB.VisaCom.IsConnected || DAQ_GPIB.GPIBCom.IsConnected), true);
            ButtonStatusChange(btnECUStatus, DAQ_CAN.CANCom.IsConnected, true);
            ButtonStatusChange(btnACSStatus, DAQ_ACS.ACSCom.IsConnected, true);
            ButtonStatusChange(btnModbusStatus, DAQ_ModbusTCP.ModbusTCPCom.IsConnected, true);
            ButtonStatusChange(btnOsirisStatus, DAQ.AKProtocol.Osiris.IsConnected, true);
        }

        double ElapsedTime = 0;
        double TotalTime = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            //try
            //{
            // System Staus Update
            SystemHWStatusUpdate();
            int Index = 0;


            //String Str = "";

            //Reading whole Variable, if the Variable is two-dimensional array
            Ethercat.GetACS_PCData();
            COntrolHWStatusUpdate();
            if (ACSExist)
            {
                for (int ind = Index; ind < Index + DaqData.sysVarInfo.AI_Variables.Count; ind++)
                {
                    try
                    {
                        string Value = string.Format("{0} {1} ({2})", DaqData.sysVarInfo.AI_Variables[ind].RealValue.ToString()
                            , DaqData.sysVarInfo.AI_Variables[ind].Unit, DaqData.sysVarInfo.AI_Variables[ind].IOValue.ToString());
                        if (lvInput.Items[ind].SubItems[1].Text == Value)
                            lvInput.Items[ind].SubItems[0].ForeColor = Color.Black;
                        else
                        {
                            lvInput.Items[ind].SubItems[1].Text = Value;
                            lvInput.Items[ind].SubItems[0].ForeColor = Color.Red;
                        }
                    }
                    catch { }
                }
                Index = Index + DaqData.sysVarInfo.AI_Variables.Count;
            }

            if (ECUExist)
            {
                for (int ind = Index; ind < Index + DaqData.sysVarInfo.ECU_Variables.Count; ind++)
                {
                    try
                    {
                        string Value = string.Format("{0} {1} ({2})", DaqData.sysVarInfo.ECU_Variables[ind - Index].RealValue.ToString()
                            , DaqData.sysVarInfo.ECU_Variables[ind - Index].Unit, DaqData.sysVarInfo.ECU_Variables[ind - Index].IOValue.ToString());
                        if (lvInput.Items[ind].SubItems[1].Text == Value)
                            lvInput.Items[ind].SubItems[0].ForeColor = Color.Black;
                        else
                        {
                            lvInput.Items[ind].SubItems[1].Text = Value;
                            lvInput.Items[ind].SubItems[0].ForeColor = Color.Red;
                        }
                    }
                    catch { }
                }
                Index = Index + DaqData.sysVarInfo.ECU_Variables.Count;
            }
            if (MexaExist)
            {
                for (int ind = Index; ind < Index + DaqData.sysVarInfo.MEXA9000_Variables.Count; ind++)
                {
                    try
                    {
                        string Value = string.Format("{0} {1} ({2})", DaqData.sysVarInfo.MEXA9000_Variables[ind - Index].RealValue.ToString()
                            , DaqData.sysVarInfo.MEXA9000_Variables[ind - Index].Unit, DaqData.sysVarInfo.MEXA9000_Variables[ind - Index].IOValue.ToString());
                        if (lvInput.Items[ind].SubItems[1].Text == Value)
                            lvInput.Items[ind].SubItems[0].ForeColor = Color.Black;
                        else
                        {
                            lvInput.Items[ind].SubItems[1].Text = Value;
                            lvInput.Items[ind].SubItems[0].ForeColor = Color.Red;
                        }
                    }
                    catch { }
                }
                Index = Index + DaqData.sysVarInfo.MEXA9000_Variables.Count;
            }
            //if (MODBUSExist)
            //{
            //    for (int ind = Index; ind < Index + DaqData.sysVarInfo.ModBus_Variables.Count; ind++)
            //    {
            //        try
            //        {
            //            string Value = string.Format("{0} {1} ({2})", DaqData.sysVarInfo.ModBus_Variables[ind - Index].RealValue.ToString()
            //                , DaqData.sysVarInfo.Calc_Variables[ind - Index].Unit, DaqData.sysVarInfo.ModBus_Variables[ind - Index].IOValue.ToString());
            //            if (lvInput.Items[ind].SubItems[1].Text == Value)
            //                lvInput.Items[ind].SubItems[0].ForeColor = Color.Black;
            //            else
            //            {
            //                lvInput.Items[ind].SubItems[1].Text = Value;
            //                lvInput.Items[ind].SubItems[0].ForeColor = Color.Red;
            //            }
            //        }
            //        catch { }
            //    }

            //    Index = Index + DaqData.sysVarInfo.ModBus_Variables.Count;
            //}

            for (int ind = Index; ind < Index + DaqData.sysVarInfo.Calc_Variables.Count; ind++)
            {
                try
                {
                    string Value = string.Format("{0} {1} ({2})", DaqData.sysVarInfo.Calc_Variables[ind - Index].RealValue.ToString()
                        , DaqData.sysVarInfo.Calc_Variables[ind - Index].Unit, DaqData.sysVarInfo.Calc_Variables[ind - Index].IOValue.ToString());
                    if (lvInput.Items[ind].SubItems[1].Text == Value)
                        lvInput.Items[ind].SubItems[0].ForeColor = Color.Black;
                    else
                    {
                        lvInput.Items[ind].SubItems[1].Text = Value;
                        lvInput.Items[ind].SubItems[0].ForeColor = Color.Red;
                    }
                }
                catch { }
            }

            if (ACSExist)
            {

                for (int ind = 0; ind < DaqData.sysVarInfo.AO_Variables.Count; ind++)
                {
                    try
                    {
                        string Value = DaqData.sysVarInfo.AO_Variables[ind].IOValue.ToString();
                        if (lvOutput.Items[ind].SubItems[1].Text == Value)
                            lvOutput.Items[ind].SubItems[0].ForeColor = Color.Black;
                        else
                        {
                            lvOutput.Items[ind].SubItems[1].Text = Value;
                            lvOutput.Items[ind].SubItems[0].ForeColor = Color.Red;
                        }
                    }
                    catch { }
                }
            }
            // Control이 포함되어 있는지 확인해야 한다.
            //SetFrontPanel();
            //SetPIDModePanel();
            ////Reading Alarm
            //Str = Ethercat.AlarmCheck();
            //if (Str.Length > 0)
            //{
            //    lbAlarm.Items.Insert(0, Str);
            //    tbAlarmCNT.Text = lbAlarm.Items.Count.ToString();
            //}

            //}
            //catch (Exception ex)
            //{
            //    timer1.Stop();
            //    DisConnect();
            //}
        }

        private void SetPIDModePanel()
        {
            if (DaqData.SystemStatus.PIDMode_Status == PIDModeStatus.Idle)
                ButtonStatusChange(btnIDLE, true, true);
            else
                ButtonStatusChange(btnIDLE, false, true);

            if (DaqData.SystemStatus.PIDMode_Status == PIDModeStatus.IdleControl)
                ButtonStatusChange(btnIdleControl, true, true);
            else
                ButtonStatusChange(btnIdleControl, false, true);

            if (DaqData.SystemStatus.PIDMode_Status == PIDModeStatus.SpeedAlpha)
                ButtonStatusChange(btnNA, true, true);
            else
                ButtonStatusChange(btnNA, false, true);

            if (DaqData.SystemStatus.PIDMode_Status == PIDModeStatus.SpeedTorque)
                ButtonStatusChange(btnNT, true, true);
            else
                ButtonStatusChange(btnNT, false, true);

            if (DaqData.SystemStatus.PIDMode_Status == PIDModeStatus.TorqueAlpha)
                ButtonStatusChange(btnTA, true, true);
            else
                ButtonStatusChange(btnTA, false, true);

            if (DaqData.SystemStatus.PIDMode_Status == PIDModeStatus.TorqueSpeed)
                ButtonStatusChange(btnTN, true, true);
            else
                ButtonStatusChange(btnTN, false, true);
        }

        private void SetFrontPanel()
        {
            if (DaqData.sysVarInfo.FrontPanel_Variables_Output[0].IOValue == 1)
                ButtonStatusChange(btnDyno, true, true);
            else
                ButtonStatusChange(btnDyno, false, true);

            if (DaqData.sysVarInfo.FrontPanel_Variables_Output[1].IOValue == 1)
                ButtonStatusChange(btnEngine, true, true);
            else
                ButtonStatusChange(btnEngine, false, true);

            if (DaqData.sysVarInfo.FrontPanel_Variables_Output[2].IOValue == 1)
                ButtonStatusChange(btnManual, true, true);
            else
                ButtonStatusChange(btnManual, false, true);

            if (DaqData.sysVarInfo.FrontPanel_Variables_Output[4].IOValue == 1)
                ButtonStatusChange(btnAuto, true, true);
            else
                ButtonStatusChange(btnAuto, false, true);


            if (DaqData.sysVarInfo.FrontPanel_Variables_Output[3].IOValue == 1)
                ButtonStatusChange(btnRemote, true, true);
            else
                ButtonStatusChange(btnRemote, false, true);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NTPID pidTunner = new NTPID();
            pidTunner.Ch = Ethercat.Ch;
            pidTunner.Show(this);
        }

        private void btnVariable_Click(object sender, EventArgs e)
        {
            VariableManage pidTunner = new VariableManage();
            pidTunner.Ch = Ethercat.Ch;
            pidTunner.Show(this);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            VariableWrite pidTunner = new VariableWrite();
            pidTunner.Ch = Ethercat.Ch;
            pidTunner.Show(this);
        }

        private void btnBuffer_Click(object sender, EventArgs e)
        {
            BufferDataSave pidTunner = new BufferDataSave();
            pidTunner.Ch = Ethercat.Ch;
            pidTunner.Show(this);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BufferStatus pidTunner = new BufferStatus();
            pidTunner.Ch = Ethercat.Ch;
            pidTunner.Show(this);
        }

        private void btnMsg_Click(object sender, EventArgs e)
        {
            MessageForm pidTunner = new MessageForm();
            pidTunner.Ch = Ethercat.Ch;
            pidTunner.Show(this);
        }

        private void connectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ACSConnect();
            DAQConnect();
        }

        private void disconnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DAQDisConnect();
            ACSDisConnect();
        }

        private void ButtonStatusChange(Button btn, bool status, bool enable)
        {
            if (status)
            {
                btn.BackgroundImage = global::AtomUI.Properties.Resources.on버튼;
            }
            else
            {
                if (enable)
                    btn.BackgroundImage = global::AtomUI.Properties.Resources.off버튼;
                else
                    btn.BackgroundImage = global::AtomUI.Properties.Resources.off버튼_회색;
            }

            btn.Enabled = enable;
        }

        private void btnControl(bool Enable)
        {
            ButtonStatusChange(btnAlarm, false, Enable);
            ButtonStatusChange(btnReset, false, Enable);
            ButtonStatusChange(btnHornOff, false, Enable);
            ButtonStatusChange(btnAuto, false, Enable);
            ButtonStatusChange(btnData, false, Enable);

            ButtonStatusChange(btnRemote2, false, Enable);
            ButtonStatusChange(btnTA2, false, Enable);
            ButtonStatusChange(btnTN2, false, Enable);


        }

        private void SysbtnControl(bool Enable)
        {
            ButtonStatusChange(btnSMStatus, false, Enable);
            ButtonStatusChange(btnECUStatus, false, Enable);
            ButtonStatusChange(btnMexaStatus, false, Enable);
            ButtonStatusChange(btnACSStatus, false, Enable);
            ButtonStatusChange(btnModbusStatus, false, Enable);
            ButtonStatusChange(btnOsirisStatus, false, Enable);
        }

        private void ConnectTCPDAQ()
        {
            //Ethercat.sysInfo.ConnectionInfo
        }

        bool HWcontrol = false;
        bool MexaExist = false;
        bool ECUExist = false;
        bool ACSExist = false;
        bool MODBUSExist = false;
        bool OSIRISExist = false;

        private void ACSConnect()
        {
            try
            {

                ACSExist = false;

                foreach (acsConnectionInfo conn in Ethercat.sysInfo.HardWareInfo)
                {
                    if (conn.HWType == HardWareType.ACS)
                    {
                        ACSExist = true;
                        HWcontrol = true;
                    }

                }

                if (HWcontrol == true)
                {

                    Ethercat.GetACS_PCData();
                    DaqData.GetAddressIndex();

                }

                if (ACSExist)
                {
                    Ethercat.Connect();
                }

                ButtonStatusChange(btnConnect2, true, true);
                ButtonStatusChange(btnDAQ, false, true);

                // ButtonStatusChange(btnConnection, true, true);
                btnControl(true);

                //MonitoringLoad();
                timer1.Start();
            }
            catch (COMException Ex)
            {
                Ethercat.ErrorMsg(Ex);           //  Throw exception if this fails
                ButtonStatusChange(btnConnect2, false, true);
                ButtonStatusChange(btnDAQ, false, false);
                btnControl(false);
                timer1.Stop();
            }
            catch (ACSException Ex)
            {
                Ethercat.ErrorMsg(Ex);           //  Throw exception if this fails
                ButtonStatusChange(btnConnect2, false, true);
                ButtonStatusChange(btnDAQ, false, false);
                btnControl(false);
                timer1.Stop();
            }
            FillTreeView();
        }

        private void ACSDisConnect()
        {
            timer1.Stop();

            try
            {
                Ethercat.DisConnect();                 // Close current communication channel
            }
            catch (COMException Ex)
            {
                Ethercat.ErrorMsg(Ex);           //  Throw exception if this fails
            }
            catch (ACSException Ex)
            {
                Ethercat.ErrorMsg(Ex);           //  Throw exception if this fails
            }
            FillTreeView();
            ButtonStatusChange(btnConnect2, false, true);
            ButtonStatusChange(btnDAQ, false, false);
            btnControl(false);

            //MonitoringLoad();

            connectMenuControl(false);
            if (DaqData.DataLoggingAvg == true || DaqData.DataLoggingCtn == true)
            {
                DataSave(true);
            }
        }
        private void DAQConnect()
        {
            try
            {
                HWcontrol = false;
                MexaExist = false;
                ECUExist = false;
                ACSExist = false;
                MODBUSExist = false;
                OSIRISExist = false;

                if (Ethercat.sysInfo.ConnectionInfo.ConnType == 0)
                {
                    AsynchronousSocketListener.Run();
                }
                else
                {
                    ConnectTCPDAQ();
                }

                foreach (acsConnectionInfo conn in Ethercat.sysInfo.HardWareInfo)
                {
                    if (conn.HWType == HardWareType.ACS && conn.HMIExist == true)
                    {
                        HWcontrol = true;
                    }
                    else if (conn.HWType == HardWareType.MEXA || conn.HWType == HardWareType.MEXA_SERIAL)
                    {
                        MexaExist = true;
                    }
                    else if (conn.HWType == HardWareType.ECU)
                    {
                        ECUExist = true;
                    }
                    else if (conn.HWType == HardWareType.ACS)
                    {
                        ACSExist = true;
                    }
                    else if (conn.HWType == HardWareType.MODBUS)
                    {
                        MODBUSExist = true;
                    }
                    else if (conn.HWType == HardWareType.OSIRIS)
                    {
                        OSIRISExist = true;
                    }
                }

                if (HWcontrol == true)
                {

                    Ethercat.GetACS_PCData();
                    DaqData.GetAddressIndex();

                }

                //if (MexaExist || ECUExist || ACSExist || MODBUSExist)
                //{
                //    ButtonStatusChange(btnLoggingStart, true, true);
                //}

                lvInput.Items.Clear();
                if (ACSExist)
                {
                    //Ethercat.Connect();
                    foreach (VariableInfo itmx in DaqData.sysVarInfo.AI_Variables)
                    {
                        ListViewItem itm = lvInput.Items.Add(itmx.VariableName);
                        itm.SubItems.Add("0");
                    }
                }
                if (ECUExist)
                    foreach (VariableInfo itmx in DaqData.sysVarInfo.ECU_Variables)
                    {
                        ListViewItem itm = lvInput.Items.Add(itmx.VariableName);
                        itm.SubItems.Add("0");
                    }
                if (MexaExist)
                    foreach (VariableInfo itmx in DaqData.sysVarInfo.MEXA9000_Variables)
                    {
                        ListViewItem itm = lvInput.Items.Add(itmx.VariableName);
                        itm.SubItems.Add("0");
                    }
                //if (MODBUSExist)
                //    foreach (VariableInfo itmx in DaqData.sysVarInfo.ModBus_Variables)
                //    {
                //        ListViewItem itm = lvInput.Items.Add(itmx.VariableName);
                //        itm.SubItems.Add("0");
                //    }
                foreach (VariableInfo itmx in DaqData.sysVarInfo.Calc_Variables)
                {
                    ListViewItem itm = lvInput.Items.Add(itmx.VariableName);
                    itm.SubItems.Add("0");
                }
                lvOutput.Items.Clear();
                foreach (VariableInfo itmx in DaqData.sysVarInfo.AO_Variables)
                {
                    ListViewItem itm = lvOutput.Items.Add(itmx.VariableName);
                    itm.SubItems.Add("0");
                }

                ButtonStatusChange(btnDAQ, true, true);
               // ButtonStatusChange(btnConnection, true, true);
                SysbtnControl(true);

                //MonitoringLoad();
                DAQConnected = true;
                timer1.Start();
            }
            catch (COMException Ex)
            {
                Ethercat.ErrorMsg(Ex);           //  Throw exception if this fails
                ButtonStatusChange(btnDAQ, false, true);
                SysbtnControl(false);
                DAQConnected = false;
                timer1.Stop();
            }
            catch (ACSException Ex)
            {
                Ethercat.ErrorMsg(Ex);           //  Throw exception if this fails
                ButtonStatusChange(btnDAQ, false, true);
                SysbtnControl(false);
                DAQConnected = false;
                timer1.Stop();
            }
            FillTreeView();
        }

        private void DAQDisConnect()
        {
            timer1.Stop();
            AsynchronousSocketListener.Stop();

            //try
            //{
            //    Ethercat.DisConnect();                 // Close current communication channel
            //}
            //catch (COMException Ex)
            //{
            //    Ethercat.ErrorMsg(Ex);           //  Throw exception if this fails
            //}
            //catch (ACSException Ex)
            //{
            //    Ethercat.ErrorMsg(Ex);           //  Throw exception if this fails
            //}
            FillTreeView();
            //ButtonStatusChange(btnLoggingStart, false, false);
            ButtonStatusChange(btnDAQ, false, true);
            SysbtnControl(false);

            //MonitoringLoad();
            DAQConnected = false;

            connectMenuControl(false);
            if (DaqData.DataLoggingAvg == true || DaqData.DataLoggingCtn == true)
            {
                DataSave(true);
            }
        }

        private void connectMenuControl(bool Enable)
        {
            disconnectToolStripMenuItem.Enabled = Enable;
            connectToolStripMenuItem.Enabled = !Enable;
        }

        private void FillTreeView()
        {
            tvSystemInfo.Nodes.Clear();

            TreeNode tn = tvSystemInfo.Nodes.Add("Controller : " + Ethercat.SerialNumber);
            tn.ContextMenuStrip = ControllerMenuStrip;
            TreeNode tn_conn = tn.Nodes.Add("Connection Info");
            tn_conn.Nodes.Add("IP : " + Ethercat.info.EthernetIP);
            tn_conn.Nodes.Add("PORT : " + Ethercat.info.EthernetPort.ToString());
            tn_conn.Nodes.Add("Protocol : " + Ethercat.info.EthernetProtocol.ToString());

            TreeNode tn_conset = tn.Nodes.Add("Connection Setup");
            tn_conset.Nodes.Add("AutoConnect: " + Ethercat.sysInfo.ConnectionInfo.AutoConnect);
            tn_conset.Nodes.Add("ComType: " + Ethercat.sysInfo.ConnectionInfo.ComType);
            tn_conset.Nodes.Add("Baud_Rate: " + Ethercat.sysInfo.ConnectionInfo.Baud_Rate);
            tn_conset.Nodes.Add("CommPort: " + Ethercat.sysInfo.ConnectionInfo.CommPort);
            tn_conset.Nodes.Add("ConnType: " + Ethercat.sysInfo.ConnectionInfo.ConnType);
            tn_conset.Nodes.Add("RemoteAddress: " + Ethercat.sysInfo.ConnectionInfo.RemoteAddress);
            tn_conset.Nodes.Add("SlotNumber: " + Ethercat.sysInfo.ConnectionInfo.SlotNumber);
            tn_conset.Nodes.Add("Baud_RateValue: " + Ethercat.sysInfo.ConnectionInfo.Baud_RateValue);

            // module Node Start
            TreeNode tn_hmi_module = tn.Nodes.Add("Ethercat HMI Slaves");
            Ethercat.MakeModuleNode(tn_hmi_module, Ethercat.sysInfo.HMIModuleInfos);

            TreeNode tn_daq_module = tn.Nodes.Add("Ethercat DAQ Slaves");
            Ethercat.MakeModuleNode(tn_daq_module, Ethercat.sysInfo.ModuleInfos);

            // Channel Node Start
            TreeNode tn_hmi = tn.Nodes.Add("HMI Channels");
            Ethercat.MakeChannelNode(tn_hmi, Ethercat.chInfo.HMI_Channels);

            TreeNode tn_DI = tn.Nodes.Add("Digital Input Channels");
            Ethercat.MakeChannelNode(tn_DI, Ethercat.chInfo.DI_Channels);

            TreeNode tn_AI = tn.Nodes.Add("Alanog Input Channels");
            Ethercat.MakeChannelNode(tn_AI, Ethercat.chInfo.AI_Channels);

            TreeNode tn_DO = tn.Nodes.Add("Digital Output Channels");
            Ethercat.MakeChannelNode(tn_DO, Ethercat.chInfo.DO_Channels);

            TreeNode tn_AO = tn.Nodes.Add("Alanog Output Channels");
            Ethercat.MakeChannelNode(tn_AO, Ethercat.chInfo.AO_Channels);

            // Variables Node Start
            TreeNode tv = tvSystemInfo.Nodes.Add("Variables");

            TreeNode tv_HMIIOV = tv.Nodes.Add("HMI IO Variables");
            Ethercat.MakeVarNode(tv_HMIIOV, DaqData.sysVarInfo.HMI_IOVariables, "HIO");

            TreeNode tv_HMIV = tv.Nodes.Add("HMI Variables");
            Ethercat.MakeVarNode(tv_HMIV, DaqData.sysVarInfo.HMI_Variables, "HMI");

            TreeNode tv_DIV = tv.Nodes.Add("Digital Input Variables");
            Ethercat.MakeVarNode(tv_DIV, DaqData.sysVarInfo.DI_Variables, "DI");

            TreeNode tv_AIV = tv.Nodes.Add("Analog Input Variables");
            Ethercat.MakeVarNode(tv_AIV, DaqData.sysVarInfo.AI_Variables, "AI");

            TreeNode tv_DOV = tv.Nodes.Add("Digital Output Variables");
            Ethercat.MakeVarNode(tv_DOV, DaqData.sysVarInfo.DO_Variables, "DO");

            TreeNode tv_AOV = tv.Nodes.Add("Analog Output Variables");
            Ethercat.MakeVarNode(tv_AOV, DaqData.sysVarInfo.AO_Variables, "AO");

            TreeNode tv_CV = tv.Nodes.Add("Calculating Variables");
            Ethercat.MakeVarNode(tv_CV, DaqData.sysVarInfo.Calc_Variables, "CV");

            tn_AO.EnsureVisible();
            tv_CV.EnsureVisible();

        }

        private void ClearTreeView()
        {
            tvSystemInfo.Nodes.Clear();
        }

        private void exitProgramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConnection_Click(object sender, EventArgs e)
        {
            if (Ethercat.Ch.IsConnected)
            {
                if (DAQConnected)
                {
                    DAQDisConnect();
                }
                ACSDisConnect();
            }
            else
                ACSConnect();
        }

        private void tvSystemInfo_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeView tv = (TreeView)sender;
            tv.SelectedNode = tv.GetNodeAt(e.X, e.Y);
        }

        private void setupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ServerSetup serverSetup = new ServerSetup();
            DialogResult res = serverSetup.ShowDialog();

            //if (Ethercat.Ch.IsConnected)
            //{
            //    Connect();
            //}
            //else
            //{
            //    DisConnect();
            //}
        }

        private void systemSetupToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SystemSetup();
        }

        private void systemSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SystemSetup();
        }

        private void SystemSetup()
        {
            if (Ethercat.Ch.IsConnected)
            {
                SystemSetup ACSSetup = new SystemSetup();
                DialogResult res = ACSSetup.ShowDialog();
                Ethercat.LoadSysInfo();
                FillTreeView();
            }
            else
            {
                MessageBox.Show("Controller Not Connected!", "ATOM_UI");
            }
        }

        private void ControllerInfo()
        {
            if (Ethercat.Ch.IsConnected)
            {
                ControllerForm.ControllerInfo ACSController = new ControllerForm.ControllerInfo();
                DialogResult res = ACSController.ShowDialog();
            }
            else
            {
                MessageBox.Show("Controller Not Connected!", "ATOM_UI");
            }
        }

        private void controllerInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ControllerInfo();
        }

        private void PIDTuner()
        {
            if (Ethercat.Ch.IsConnected)
            {
                NTPID ACSNTPID = new NTPID();
                ACSNTPID.Ch = Ethercat.Ch;
                ACSNTPID.Show();
            }
            else
            {
                MessageBox.Show("Controller Not Connected!", "ATOM_UI");
            }
        }

        private void pIDTunnerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PIDTuner();
        }

        private void ModuleInfo()
        {
            ControllerForm.ModuleInfo ACSNTPID = new ControllerForm.ModuleInfo();
            DialogResult res = ACSNTPID.ShowDialog();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ModuleInfo();
        }

        private void ChannelInfo()
        {
            ControllerForm.ChannelInfo ACSNTPID = new ControllerForm.ChannelInfo();
            ACSNTPID.Show();
        }

        private void channelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChannelInfo();
        }

        private void variableDefine()
        {
            DataForm.VariableDefine ACSVarDefine = new DataForm.VariableDefine();
            DialogResult res = ACSVarDefine.ShowDialog();
            //DaqData.LoadSysInfo();
            FillTreeView();
        }

        private void variableDefineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            variableDefine();
        }

        private void VariableLoadSave()
        {
            DataForm.VariablesManage ACSVarManage = new DataForm.VariablesManage();
            DialogResult res = ACSVarManage.ShowDialog();
            //DaqData.LoadSysInfo();
            FillTreeView();
        }

        private void variableMappingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VariableLoadSave();
        }


        private void profileDefineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ControlForm.StepModeDefine ACSDataLog = new ControlForm.StepModeDefine();
            DialogResult res = ACSDataLog.ShowDialog();
        }

        private void runStepModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Ethercat.Ch.IsConnected || CommonFunction.bUseDebug)
            {
                ControlForm.StepRun ACSTranRun = new ControlForm.StepRun(this);
                m_StepRun = ACSTranRun;
                ACSTranRun.Show();
            }
            else
            {
                MessageBox.Show("Controller Not Connected!", "ATOM_UI");
            }
        }

        private void modeDefineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ControlForm.TransientModeDefine ACSDataLog = new ControlForm.TransientModeDefine();
            DialogResult res = ACSDataLog.ShowDialog();
        }

        private void runTransientModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Ethercat.Ch.IsConnected)
            {
                ControlForm.TransientRun ACSTranRun = new ControlForm.TransientRun();
                ACSTranRun.Show();
            }
            else
            {
                MessageBox.Show("Controller Not Connected!", "ATOM_UI");
            }
        }

        private void inputMonitorToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void customMonitoringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MonitoringLoad();
        }

        private void 새로만들기ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // 이름을 입력받아야함.
            MonitoringForm.TextDialog NameInput = new MonitoringForm.TextDialog();
            DialogResult dr = NameInput.ShowDialog();

            if (dr == DialogResult.OK)
            {
                string NewName = NameInput.name;

                MonitoringForm.CustomMonitorMake ACSDataLog = new MonitoringForm.CustomMonitorMake(NewName);
                ACSDataLog.ShowDialog();

                MakeMonitor(NewName);
                Ethercat.sysInfo.sysSettings.MonitorList = Ethercat.sysInfo.sysSettings.MonitorList + NewName + ",";
                Ethercat.sysInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SysInfoDir + "\\" + Ethercat.sysInfo.sysSettings.SysInfoFileName);

            }
        }

        private void MakeMonitor(string Name)
        {
            if (System.IO.File.Exists(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.MonInfoDir + "\\" + Name + ".xml"))
            {
                MonitorForm mForm = new MonitorForm();
                mForm.MonitorName = Name;

                ToolStripMenuItem menubase = new ToolStripMenuItem(Name);
                customMonitoringToolStripMenuItem.DropDownItems.Add(menubase);

                ToolStripMenuItem dock = new ToolStripMenuItem("Tab");
                menubase.DropDownItems.Add(dock);
                dock.Click += MonitorToolStripMenuItem_Click;
                mForm.RunDockingMenu = dock;

                ToolStripMenuItem win = new ToolStripMenuItem("Monitor Windows");
                menubase.DropDownItems.Add(win);
                win.Click += MonitorToolStripMenuItem_Click;
                mForm.RunFormMenu = win;

                ToolStripMenuItem cls = new ToolStripMenuItem("Close Monitor");
                menubase.DropDownItems.Add(cls);
                cls.Click += MonitorToolStripMenuItem_Click;
                cls.Enabled = false;
                mForm.CloseMenu = cls;

                ToolStripMenuItem editbase = new ToolStripMenuItem(Name);
                customizeMonitoringViewToolStripMenuItem.DropDownItems.Add(editbase);

                ToolStripMenuItem edit = new ToolStripMenuItem("Edit");
                editbase.DropDownItems.Add(edit);
                edit.Click += MonitorToolStripMenuItem_Click;
                mForm.EditMenu = edit;

                ToolStripMenuItem rename = new ToolStripMenuItem("Rename");
                editbase.DropDownItems.Add(rename);
                rename.Click += MonitorToolStripMenuItem_Click;
                mForm.RenameMenu = rename;

                ToolStripMenuItem del = new ToolStripMenuItem("Delete");
                editbase.DropDownItems.Add(del);
                del.Click += MonitorToolStripMenuItem_Click;
                mForm.DeleteMenu = del;

                cMonitor.Add(mForm);

                MonitorInfo mon = new MonitorInfo();
                mon.LoadMonInfo(Name);
                if (mon.MonitorList.monStatus ==MonitorStatus.Tab)
                {
                    dock.PerformClick();
                } else if (mon.MonitorList.monStatus == MonitorStatus.Windows)
                {
                    win.PerformClick();
                }
            }
        }

        private void HMIMonitorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BufferStatus BS = new BufferStatus();
            BS.Show();
        }

        private void remoteSignalRangeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataForm.ControlValueSetting ACSDataLog = new DataForm.ControlValueSetting();
            DialogResult res = ACSDataLog.ShowDialog();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            ControllerForm.HWConfig newForm = new ControllerForm.HWConfig();
            newForm.Show();
        }

        private void mexaControlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools.MexaControl newForm = new Tools.MexaControl();
            newForm.Show();

        }

        private void tcMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcMain.SelectedIndex > 0)
            {
                MonitorForm monForm = FindMonitor(tcMain.TabPages[tcMain.SelectedIndex]);
                if (monForm!=null)
                    MonitoringLoad(monForm);
            }
        }

        private MonitorForm FindMonitor(TabPage tab)
        {
            foreach (MonitorForm mon in cMonitor)
            {
                if (mon.tab == tab)
                    return mon;
            }
            return null;
        }

        private void MonitorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessMonitorCommand((ToolStripMenuItem)sender);
        }

        private void MonitorClose(string MonitorName)
        {
            foreach (MonitorForm mon in cMonitor)
            {
                if (mon.MonitorName == MonitorName)
                {
                    if (mon.isDovking == true)
                    {
                        if (mon.cMonitor != null)
                        {
                            mon.cMonitor.Close();
                            mon.cMonitor = null;
                        }
                        if (mon.tab != null)
                        {
                            tcMain.TabPages.Remove(mon.tab);
                            mon.tab = null;
                        }
                    }
                    else
                    {
                        if (mon.cMonitor != null)
                            mon.cMonitor.Close();
                    }
                    mon.RunFormMenu.Enabled = true;
                    mon.RunDockingMenu.Enabled = true;
                    mon.CloseMenu.Enabled = false;
                }
            }
        }

        private void ProcessMonitorCommand(ToolStripMenuItem menu)
        {
            foreach (MonitorForm mon in cMonitor)
            {
                if (mon.RunFormMenu == menu)
                {
                    mon.isDovking = false;
                    MonitoringLoad(mon);
                    mon.RunFormMenu.Enabled = false;
                    mon.RunDockingMenu.Enabled = true;
                    mon.CloseMenu.Enabled = true;
                    break;
                }
                else if (mon.CloseMenu == menu)
                {
                    if (mon.isDovking == true)
                    {
                        if (mon.cMonitor != null)
                        {
                            mon.cMonitor.Close();
                            mon.cMonitor = null;
                        }
                        if (mon.tab != null)
                        {
                            tcMain.TabPages.Remove(mon.tab);
                            mon.tab = null;
                        }
                    }
                    else
                    {
                        if (mon.cMonitor != null)
                            mon.cMonitor.Close();
                    }
                    mon.RunFormMenu.Enabled = true;
                    mon.RunDockingMenu.Enabled = true;
                    mon.CloseMenu.Enabled = false;
                    break;
                }
                else if (mon.RunDockingMenu == menu)
                {
                    mon.isDovking = true;
                    mon.RunFormMenu.Enabled = true;
                    mon.RunDockingMenu.Enabled = false;
                    mon.CloseMenu.Enabled = true;

                    MonitoringLoad(mon);
                    break;
                }
                else if (mon.EditMenu == menu)
                {
                    MonitoringForm.CustomMonitorMake ACSDataLog = new MonitoringForm.CustomMonitorMake(mon.MonitorName);
                    MonitorClose(mon.MonitorName);
                    ACSDataLog.Show();
                    break;

                    //mon.isDovking = true;
                    //MonitoringLoad(mon);
                }
                else if (mon.RenameMenu == menu)
                {
                    mon.isDovking = true;
                    //MonitoringLoad(mon);
                    break;
                }
                else if (mon.DeleteMenu == menu)
                {
                    if (DialogResult.Yes == MessageBox.Show("Delete Selected Monitor. Continue?", "Delete Monitor", MessageBoxButtons.YesNo))
                    {
                        if (mon.isDovking == true)
                        {
                            if (mon.cMonitor != null)
                            {
                                mon.cMonitor.Close();
                                mon.cMonitor = null;
                            }
                            if (mon.tab != null)
                            {
                                tcMain.TabPages.Remove(mon.tab);
                                mon.tab = null;
                            }
                        }
                        else
                        {
                            if (mon.cMonitor != null)
                                mon.cMonitor.Close();
                        }

                        string MonitorNames = Ethercat.sysInfo.sysSettings.MonitorList;
                        System.IO.File.Delete(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.MonInfoDir + "\\" + mon.MonitorName + ".xml");
                        MonitorNames = MonitorNames.Replace(mon.MonitorName+",", "");
                        Ethercat.sysInfo.sysSettings.MonitorList = MonitorNames;
                        Ethercat.sysInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SysInfoDir + "\\" + Ethercat.sysInfo.sysSettings.SysInfoFileName);

                        customMonitoringToolStripMenuItem.DropDownItems.Remove((ToolStripMenuItem)(mon.RunFormMenu.OwnerItem));
                        customizeMonitoringViewToolStripMenuItem.DropDownItems.Remove((ToolStripMenuItem)(mon.EditMenu.OwnerItem));
                    }
                    break;
                }
            }

    }


        private void AtomUI_Paint(object sender, PaintEventArgs e)
        {
            //mainPanel을 그리기 표면으로 사용하기 위해 변수지정
            Graphics g = e.Graphics;
            DrawShadow(g, pnlControl);
        }

        //int indexOpacity = 0;
        int[] opacity = { 0, 100, 255, 100 };
        Image ImageCenter;
        Image ImageLeft;
        Image ImageRight;

        private void tmrDrawing_Tick(object sender, EventArgs e)
        {
            ////Image imageCenter = (Image)ImageCenter.Clone();
            ////Image imageRight = (Image)ImageRight.Clone();
            ////Image imageLeft = (Image)ImageLeft.Clone();
            //SuspendLayout();

            ////CommonFunction.DrawOpacityImage(panel1.BackgroundImage, opacity[indexOpacity], Color.White);

            ////pnlTolCenter.BackgroundImage = imageCenter;
            ////pnlTolLeft.BackgroundImage = imageLeft;
            ////pnlTolRight.BackgroundImage = imageRight;

            //ResumeLayout();

            //indexOpacity++;
            //if (indexOpacity >= opacity.Length)
            //    indexOpacity = 0;

            ////pnlTolCenter.Invalidate();
            ////pnlTolLeft.Invalidate();
            ////pnlTolRight.Invalidate();
        }

        private void btnMexaNoOP_Click(object sender, EventArgs e)
        {

        }

        private void btnMexaRESET_Click(object sender, EventArgs e)
        {
            AsynchronousSocketListener.visacom.WriteAOP_Reset();
        }

        private void btnMexaMEASURE_Click(object sender, EventArgs e)
        {
            AsynchronousSocketListener.visacom.WriteAOP_Measure();
        }

        private void btnMexaPURGE_Click(object sender, EventArgs e)
        {
            AsynchronousSocketListener.visacom.WriteAOP_Purge();
        }

        private void btnDAQ_Click(object sender, EventArgs e)
        {
            if (DAQConnected)
            {
                DAQDisConnect();
            }
            else
            {
                DAQConnect();
            }
        }


        private void calculatingVariableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataForm.CalcRefVariableDefine Define = new DataForm.CalcRefVariableDefine();
            DialogResult res = Define.ShowDialog();
        }


        private void iOControlDemoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ControlForm.IOControl_Demo Define = new ControlForm.IOControl_Demo();
            Define.Show();
        }

        private void 기존파일추가ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Monitor File Format|*.xml";
            openFileDialog1.Title = "Load an Existing Monitoring Define File";
            openFileDialog1.FileName = "";
            if (DialogResult.OK == openFileDialog1.ShowDialog() && openFileDialog1.FileName != "")
            {
                string file = openFileDialog1.FileName;
                string filename = file.Substring(file.LastIndexOf('\\') + 1, file.Length - file.LastIndexOf('\\') - 1);
                string NewName = filename.Substring(0, filename.LastIndexOf('.'));

                if (Ethercat.sysInfo.sysSettings.MonitorList.IndexOf(NewName + ",") > 0)
                {
                    MessageBox.Show("기존에 존재하는 모니터 이름입니다. 이름을 변경하신 후 다시 시도해 주십시오.");
                }
                string temp = Ethercat.sysInfo.sysSettings.DefaultDirectory + Ethercat.sysInfo.sysSettings.MonInfoDir + "\\" + filename;
                if (openFileDialog1.FileName != temp)
                {
                    if (!File.Exists(temp))
                        File.Copy(openFileDialog1.FileName, temp);
                    else
                        File.Copy(openFileDialog1.FileName, temp, true);

                }
                MakeMonitor(NewName);
                Ethercat.sysInfo.sysSettings.MonitorList = Ethercat.sysInfo.sysSettings.MonitorList + NewName + ",";
                Ethercat.sysInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SysInfoDir + "\\" + Ethercat.sysInfo.sysSettings.SysInfoFileName);
            }
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools.EnvironmentSetup newForm = new Tools.EnvironmentSetup();
            newForm.Show();
        }

        private void btnDataFolderOpen_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("explorer.exe", Ethercat.sysInfo.sysSettings.DataLoggingDir);
        }

        private void btnDyno_Click(object sender, EventArgs e)
        {

        }

        private void btnRemote2_Click(object sender, EventArgs e)
        {
            double[] SendData;
            if (Ethercat.g_SystemState[0]==0)
                SendData = new double[1] { 1 };
            else
                SendData = new double[1] { 0 };

            Ethercat.Ch.WriteVariable(SendData, "g_SystemState_Remote", ProgramBuffer.ACSC_NONE, 0, 0, 0, 0);
        }

        private void btnTA2_Click(object sender, EventArgs e)
        {
            double[] SendData;
            SendData = new double[3] { 0, 1, 0 };
            Ethercat.Ch.WriteVariable(SendData, "g_SystemState_ControlModeSignal", ProgramBuffer.ACSC_NONE, 0, 0, 0, 2);
        }

        private void btnNA2_Click(object sender, EventArgs e)
        {
            double[] SendData;
            SendData = new double[3] { 1, 0, 0 };
            Ethercat.Ch.WriteVariable(SendData, "g_SystemState_ControlModeSignal", ProgramBuffer.ACSC_NONE, 0, 0, 0, 2);
        }

        private void btnNT2_Click(object sender, EventArgs e)
        {
            double[] SendData;
            SendData = new double[3] { 0, 1, 1 };
            Ethercat.Ch.WriteVariable(SendData, "g_SystemState_ControlModeSignal", ProgramBuffer.ACSC_NONE, 0, 0, 0, 2);
        }

        private void btnAlarm_Click(object sender, EventArgs e)
        {

        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            double[] SendData;
            SendData = new double[1] { 1};
            Ethercat.Ch.WriteVariable(SendData, "g_SystemState_Reset", ProgramBuffer.ACSC_NONE, 0, 0, 0, 0);
        }

        private void btnHornOff_Click(object sender, EventArgs e)
        {
            double[] SendData;

            if (Ethercat.g_SystemState[5] == 0)
                SendData = new double[1] { 1 };
            else
                SendData = new double[1] { 0 };

            Ethercat.Ch.WriteVariable(SendData, "g_SystemState_HornOff", ProgramBuffer.ACSC_NONE, 0, 0, 0, 0);
        }

        private void controlPadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Ethercat.Ch.IsConnected)
            {
                ControlForm.ControlPad ACControlRun = new ControlForm.ControlPad();
                ACControlRun.Show();
            }
            else
            {
                MessageBox.Show("Controller Not Connected!", "ATOM_UI");
            }
        }

        private void smokeMeterControlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DAQ.SerialComm.SmokeMeter.IsConnected)
            {
                ControlForm.SMControl controlRun = new ControlForm.SMControl();
                controlRun.Show();
            }
            else
            {
                MessageBox.Show("Smoke Meter Not Connected!", "ATOM_UI");
            }
        }

        private void fuelMeterControlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DAQ.SerialComm.FuelMeter.IsConnected)
            {
                ControlForm.FMControl controlRun = new ControlForm.FMControl();
                controlRun.Show();
            }
            else
            {
                MessageBox.Show("FuelMeter Not Connected!", "ATOM_UI");
            }
        }

        private void mexaControlToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (DAQ_GPIB.GPIBCom.IsConnected)
            {
                ControlForm.Mexa controlRun = new ControlForm.Mexa();
                controlRun.Show();
            }
            else
            {
                MessageBox.Show("Mexa Not Connected!", "ATOM_UI");
            }
        }






        #region Data Logging

        public void SetLoggingEnabled(bool bEnabled)
        {
            if (timerDataLoggingAvg.GetProceedTime() < TotalAvgTime * 1000 || timerDataLoggingCtn.GetProceedTime() < TotalCtnTime * 1000)
                return;
            if (this.InvokeRequired)
            {
                object[] p = { bEnabled };
                this.BeginInvoke(new dlLoggingEnabled(LoggingEnabled), p);
            }
            else
            {
                LoggingEnabled(bEnabled);
            }
        }
        private delegate void dlLoggingEnabled(bool bEnabled);

        private void LoggingEnabled(bool bEnabled)
        {
            if (chkLoggingAvgCtn.Checked && !chkLoggingAvg.Checked && !chkLoggingCtn.Checked)
            {
                if (bEnabled)
                    chkLoggingAvgCtn.Checked = false;
                chkLoggingAvg.Enabled = bEnabled;
                chkLoggingCtn.Enabled = bEnabled;
            }
        }

        public void SetLoggingAvgChecked(bool bEnabled)
        {
            if (this.InvokeRequired)
            {
                object[] p = { bEnabled };
                this.BeginInvoke(new dlLoggingAvgChecked(LoggingAvgChecked), p);
            }
            else
            {
                LoggingAvgChecked(bEnabled);
            }
        }
        private delegate void dlLoggingAvgChecked(bool bEnabled);

        private void LoggingAvgChecked(bool bEnabled)
        {
            chkLoggingAvg.Checked = bEnabled;
        }
        public void SetLoggingCtnChecked(bool bEnabled)
        {
            if (this.InvokeRequired)
            {
                object[] p = { bEnabled };
                this.BeginInvoke(new dlLoggingCtnChecked(LoggingCtnChecked), p);
            }
            else
            {
                LoggingCtnChecked(bEnabled);
            }
        }
        private delegate void dlLoggingCtnChecked(bool bEnabled);

        private void LoggingCtnChecked(bool bEnabled)
        {
            chkLoggingCtn.Checked = bEnabled;
        }

        private void btnLoggingAvg_Click(object sender, EventArgs e)
        {
            DataLoggingAvgStartStop(true, TotalAvgTime);
        }

        private void InitAvgLog()
        {
            timerDataLoggingAvg = new PerforTimer();
            thAvgLog = new Thread(new ThreadStart(RunningAvgLog));
            thAvgLog.Priority = ThreadPriority.Highest;

            using (Process p = Process.GetCurrentProcess())
                p.PriorityClass = ProcessPriorityClass.RealTime;

            thAvgLog.Start();
            //public void DataLoggingAvgStartStop(bool bOn, double nTotalTime = 0)
        }

        //public void DataLoggingAvgStartStop(bool bOn, double nTotalTime = 0)
        //{
        //    if (this.InvokeRequired)
        //    {
        //        object[] p = { bOn, nTotalTime };
        //        this.BeginInvoke(new dlDataLoggingAvgStartStopSet(DataLoggingAvgStartStopSet), p);
        //    }
        //    else
        //    {
        //        DataLoggingAvgStartStopSet(bOn, nTotalTime);
        //    }
        //}
        //private delegate void dlDataLoggingAvgStartStopSet(bool bOn, double nTotalTime = 0);
        bool bAvgStartF = false;
        public void DataLoggingAvgStartStop(bool bOn, double nTotalTime = 0)
        {
            if (bOn)
            {
                TotalAvgTime = nTotalTime;
                Dele_SetProgressBarAvgMax((int)TotalAvgTime);
                bAvgStartF = true;

                SetLoggingEnabled(false);
            }
            else
            {
                DataLoggingAvgStop();
            }
        }


        public void Dele_SetProgressBarAvgMax(int nVal)
        {
            if (this.InvokeRequired)
            {
                object[] p = { nVal };
                this.BeginInvoke(new dlprogressLoggingAvgMax(SetProgressBarAvgMax), p);
            }
            else
            {
                SetProgressBarAvgMax(nVal);
            }
        }
        private delegate void dlprogressLoggingAvgMax(int nVal);
        private void SetProgressBarAvgMax(int nVal)
        {
            progressLoggingAvg.Maximum = nVal;
        }
        public void Dele_SetProgressBarAvg(int nVal)
        {
            if (this.InvokeRequired)
            {
                object[] p = { nVal };
                this.BeginInvoke(new dlprogressLoggingAvg(SetProgressBarAvg), p);
            }
            else
            {
                SetProgressBarAvg(nVal);
            }
        }
        private delegate void dlprogressLoggingAvg(int nVal);
        private void SetProgressBarAvg(int nVal)
        {
            if (nVal >= progressLoggingAvg.Minimum && nVal <= progressLoggingAvg.Maximum)
                progressLoggingAvg.Value = nVal;
        }
        

        private void RunningAvgLog()
        {
            while (true)
            {
                if (bAvgStartF)
                {
                    bAvgStartF = false;
                    DaqData.DataSaveStartAvg(true);
                    timerDataLoggingAvg.MTimeoutInit(SaveData.saveData.DataAvgLoggingInterval, true);  // 처음 바로 시작
                }
                if (DaqData.DataLoggingAvg == true)
                {
                    timerDataLoggingAvg.MTimeoutStart();

                    if (timerDataLoggingAvg.GetProceedTime() < TotalAvgTime * 1000)
                    {
                        //string Value = DateTime.Now.ToString("yyyy_MM_dd HH:mm:ss") + "." + DateTime.Now.Millisecond.ToString("000");
                        //Console.WriteLine("avg : " + Value);
                        Dele_SetProgressBarAvg((int)timerDataLoggingAvg.GetProceedTime() / 1000);
                        DaqData.DataSaveAvg();
                    }
                    else
                    {
                        DataLoggingAvgStop();
                    }
                }
                else
                    Thread.Sleep(1);
            }
        }

        private void DataLoggingAvgStop()
        {
            string strMsg = DaqData.DataSaveStopAvg();
            Dele_SetProgressBarAvg((int)TotalAvgTime);
            SetLoggingEnabled(true);
            SetLoggingAvgChecked(false);
        }

        private void DataLoggingAvgThreadStop()
        {
            progressLoggingAvg.Value = 0;

            //using (Process p = Process.GetCurrentProcess())
            //    p.PriorityClass = ProcessPriorityClass.Normal;
            if (thAvgLog != null)
            {
                thAvgLog.Abort();
                thAvgLog.Join();
            }
        }




        private void InitCtnLog()
        {
            timerDataLoggingCtn = new PerforTimer();
            thInstantLog = new Thread(new ThreadStart(RunningCtnLog));
            thInstantLog.Priority = ThreadPriority.Highest;

            using (Process p = Process.GetCurrentProcess())
                p.PriorityClass = ProcessPriorityClass.RealTime;

            thInstantLog.Start();
        }

        //public void DataLoggingCtnStartStop(bool bOn, double nTotalTime = 0)
        //{
        //    if (this.InvokeRequired)
        //    {
        //        object[] p = { bOn, nTotalTime };
        //        this.BeginInvoke(new dlDataLoggingCtnStartStopSet(DataLoggingCtnStartStopSet), p);
        //    }
        //    else
        //    {
        //        DataLoggingCtnStartStopSet(bOn, nTotalTime);
        //    }
        //}
        //private delegate void dlDataLoggingCtnStartStopSet(bool bOn, double nTotalTime = 0);
        public void DataLoggingCtnStartStop(bool bOn, double nTotalTime = 0)
        {
            if (bOn)
            {
                TotalCtnTime = nTotalTime;
                Dele_SetProgressBarCtnMax((int)TotalCtnTime);
                timerDataLoggingCtn.MTimeoutInit(SaveData.saveData.DataCtnLoggingInterval, true);  // 처음 바로 시작
                DaqData.rowTotalCountCtn = 0;
                DaqData.DataSaveStartCtn();

                SetLoggingEnabled(false);
            }
            else
            {
                DaqData.rowTotalCountCtn = 0;
                DataLoggingCtnStop();
            }
        }

        public void Dele_SetProgressBarCtnMax(int nVal)
        {
            if (this.InvokeRequired)
            {
                object[] p = { nVal };
                this.BeginInvoke(new dlprogressLoggingCtnMax(SetProgressBarCtnMax), p);
            }
            else
            {
                SetProgressBarCtnMax(nVal);
            }
        }
        private delegate void dlprogressLoggingCtnMax(int nVal);
        private void SetProgressBarCtnMax(int nVal)
        {
            progressLoggingCtn.Maximum = nVal;
        }
        public void Dele_SetProgressBarCtn(int nVal)
        {
            if (this.InvokeRequired)
            {
                object[] p = { nVal };
                this.BeginInvoke(new dlprogressLoggingCtn(SetProgressBarCtn), p);
            }
            else
            {
                SetProgressBarCtn(nVal);
            }
        }
        private delegate void dlprogressLoggingCtn(int nVal);
        private void SetProgressBarCtn(int nVal)
        {
            if (nVal >= progressLoggingCtn.Minimum && nVal <= progressLoggingCtn.Maximum)
                progressLoggingCtn.Value = nVal;
        }


        private void btnLoggingCtn_Click(object sender, EventArgs e)
        {
            DataLoggingCtnStartStop(true, TotalCtnTime);
        }


        private void RunningCtnLog()
        {
            while (true)
            {

                if (DaqData.DataLoggingCtn == true)
                {
                    timerDataLoggingCtn.MTimeoutStart();
                    if (timerDataLoggingCtn.GetProceedTime() < TotalCtnTime * 1000
                        || DaqData.rowTotalCountCtn < TotalCtnTime * 1000 / SaveData.saveData.DataCtnLoggingInterval)
                    {
                        Dele_SetProgressBarCtn((int)timerDataLoggingCtn.GetProceedTime() / 1000);
                        DaqData.DataSaveCtn(false);
                    }
                    else
                    {
                        DataLoggingCtnStop();
                    }
                }
                else
                {
                    DaqData.DataSaveCtn(false);
                    Thread.Sleep(1);
                }
            }
        }

        public void DataLoggingCtnStop()
        {
            DaqData.DataSaveStopCtn();
            Dele_SetProgressBarCtn((int)TotalCtnTime);
            SetLoggingCtnChecked(false);
            SetLoggingEnabled(true);
        }

        private void DataLoggingCtnThreadStop()
        {
            progressLoggingCtn.Value = 0;
            DaqData.prevStopWatchTickCtn = 0;
            DaqData.nowStopWatchTickCtn = 0;
            //using (Process p = Process.GetCurrentProcess())
            //    p.PriorityClass = ProcessPriorityClass.Normal;
            if (thInstantLog != null)
            {
                thInstantLog.Abort();
                thInstantLog.Join();
            }
        }

        private void InitConstantLog()
        {
            timerDataLoggingConstant = new PerforTimer();
            thConstantLog = new Thread(new ThreadStart(RunningConstantLog));
            thConstantLog.Priority = ThreadPriority.Highest;

            using (Process p = Process.GetCurrentProcess())
                p.PriorityClass = ProcessPriorityClass.RealTime;
            thConstantLog.Start();
        }

        //public void DataLoggingConstantStartStop(bool bOn, bool bBtnDisEnabled, double nTotalTime = 0)
        //{
        //    if (this.InvokeRequired)
        //    {
        //        object[] p = { bOn, bBtnDisEnabled, nTotalTime };
        //        this.BeginInvoke(new dlDataLoggingConstantStartStop(DataLoggingConstantStartStopSet), p);
        //    }
        //    else
        //    {
        //        DataLoggingConstantStartStopSet(bOn, bBtnDisEnabled, nTotalTime);
        //    }
        //}
        //private delegate void dlDataLoggingConstantStartStop(bool bOn, bool bBtnDisEnabled, double nTotalTime = 0);
        public void DataLoggingConstantStartStop(bool bOn, bool bBtnDisEnabled, double nTotalTime = 0)
        {
            if (bOn)
            {
                TotalConstantTime = nTotalTime;
                timerDataLoggingConstant.MTimeoutInit(SaveData.saveData.DataConstantLoggingInterval, true);  // 처음 바로 시작
                DaqData.rowCountConstant = 0;
                DaqData.rowTotalCountConstant = 0;
                DaqData.fileCountConstant = 0;
                DaqData.prevStopWatchTickConstant = 0;
                DaqData.DataSaveStartConstant();
            }
            else
            {
                DaqData.DataLoggingConstant = false;
                DaqData.rowCountConstant = 0;
                DaqData.rowTotalCountConstant = 0;
                DaqData.fileCountConstant = 0;
                DaqData.prevStopWatchTickConstant = 0;

            }
            if (bBtnDisEnabled)
            {
                SetOfftgConstantSave(bBtnDisEnabled, bOn);
            }
        }



        public void SetOfftgConstantSave(bool bBtnDisEnabled, bool bOn)
        {
            if (this.InvokeRequired)
            {
                object[] p = { bBtnDisEnabled, bOn };
                this.BeginInvoke(new dlOfftgConstantSave(OfftgConstantSave), p);
            }
            else
            {
                OfftgConstantSave(bBtnDisEnabled, bOn);
            }
        }

        private delegate void dlOfftgConstantSave(bool bBtnDisEnabled, bool bOn);
        private void OfftgConstantSave(bool bBtnDisEnabled, bool bOn)
        {
            chkLoggingConstant.Enabled = !bOn;
            if (bBtnDisEnabled) chkLoggingConstant.Checked = false;

        }
        private void RunningConstantLog()
        {
            while (true)
            {
                if (DaqData.DataLoggingConstant == true)
                {
                    timerDataLoggingConstant.MTimeoutStart();
                    if (TotalConstantTime <= 0
                        || timerDataLoggingConstant.GetProceedTime() < TotalConstantTime * 1000
                        || DaqData.rowTotalCountConstant < TotalConstantTime * 1000 / SaveData.saveData.DataConstantLoggingInterval)
                    {
                        //Console.WriteLine("1111 : " + timerDataLoggingAvg.GetProceedTime());
                        //string Value = DateTime.Now.ToString("yyyy_MM_dd HH:mm:ss") + "." + DateTime.Now.Millisecond.ToString("000");
                        //Console.WriteLine("con : " + Value);
                        //DaqData.nowStopWatchTickConstant = timerDataLoggingConstant.GetStartTime();
                        DaqData.DataSaveConstant();

                    }
                    else
                    {
                        //Console.WriteLine("2222 : " + timerDataLoggingAvg.GetProceedTime());
                        //string Value = DateTime.Now.ToString("yyyy_MM_dd HH:mm:ss") + "." + DateTime.Now.Millisecond.ToString("000");
                        //Console.WriteLine("constop : " + Value);
                        DataLoggingConstantStartStop(false, true);
                    }
                }
                else
                {
                    DaqData.DataSaveConstant();
                    Thread.Sleep(1);
                }
            }
        }

        public void DataLoggingConstantThreadStop()
        {
            if (thConstantLog != null)
            {
                thConstantLog.Abort();
                thConstantLog.Join();
            }
        }



        private void DataLoggingSetup()
        {
            DataForm.DataLogSetup ACSDataLog = new DataForm.DataLogSetup();
            DialogResult res = ACSDataLog.ShowDialog();
            //DaqData.LoadSysInfo();
            FillTreeView();
        }

        private void dataLoggingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataLoggingSetup();
        }

        private void DataSave(bool bForceStop)
        {
            if (DaqData.DataLoggingAvg == false && !bForceStop)
            {
                if (DaqData.sysVarInfo.InstantDataLogging_Variables.Count > 0)
                {
                    ButtonStatusChange(btnData, true, true);
                    DaqData.DataSaveStartAvg();
                }
                else
                {
                    MessageBox.Show(new Form { TopMost = true }, "Logging Data Not Defined!", "DATASAVE");
                }
            }
            else
            {
                DaqData.DataSaveStopAvg();
                ButtonStatusChange(btnData, false, true);
            }

            if (DaqData.DataLoggingCtn == false && !bForceStop)
            {
                if (DaqData.sysVarInfo.InstantDataLogging_Variables.Count > 0)
                {
                    ButtonStatusChange(btnData, true, true);
                    DaqData.DataSaveStartCtn();
                }
                else
                {
                    MessageBox.Show(new Form { TopMost = true }, "Logging Data Not Defined!", "DATASAVE");
                }
            }
            else
            {
                DaqData.DataSaveStopCtn();
                ButtonStatusChange(btnData, false, true);
            }
        }

        private void btnData_Click(object sender, EventArgs e)
        {
            DataSave(false);
        }

        private void tbLoggingTimeAvg_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //TotalAvgTime = int.Parse(tbLoggingTimeAvg.Text);
                //progressLoggingAvg.Maximum = (int)TotalAvgTime;
            }
            catch (Exception e1)
            {
                tbLoggingTimeAvg.Text = "0";
            }
        }

        private void tbLoggingTimeCtn_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //TotalCtnTime = int.Parse(tbLoggingTimeCtn.Text);
                //progressLoggingCtn.Maximum = (int)TotalCtnTime;
            }
            catch (Exception e1)
            {
                tbLoggingTimeCtn.Text = "0";
            }
        }


        private void chkLoggingAvgCtn_CheckedChanged(object sender, EventArgs e)
        {
            if (bMouseClicked)
            {
                bMouseClicked = false;
                if (chkLoggingAvgCtn.Checked)
                {
                    if (DaqData.sysVarInfo.InstantDataLogging_Variables.Count > 0)
                    {
                        if (DaqData.DataLoggingAvg || DaqData.DataLoggingCtn)
                        {
                            if (DaqData.DataLoggingAvg)
                            {
                                DataLoggingAvgStartStop(false);
                            }
                            if (DaqData.DataLoggingCtn)
                            {
                                DataLoggingCtnStartStop(false);
                            }
                            chkLoggingAvgCtn.Checked = false;
                            return;
                        }

                        DaqData.InitDaqDataLogging();
                        DataLoggingAvgStartStop(true, int.Parse(tbLoggingTimeAvg.Text));
                        DataLoggingCtnStartStop(true, int.Parse(tbLoggingTimeCtn.Text));

                    }
                    else
                    {
                        MessageBox.Show(new Form { TopMost = true }, "Logging Data Not Defined!", "Data Logging");
                    }
                }
                else
                {
                    if (DaqData.DataLoggingAvg)
                    {
                        DataLoggingAvgStartStop(false);
                    }
                    if (DaqData.DataLoggingCtn)
                    {
                        DataLoggingCtnStartStop(false);
                    }
                    chkLoggingAvg.Enabled = true;
                    chkLoggingCtn.Enabled = true;
                }
            }
        }

        private bool bMouseClicked = false;
        private void chkLoggingAvg_CheckedChanged(object sender, EventArgs e)
        {
            if (bMouseClicked)
            {
                bMouseClicked = false;
                if (chkLoggingAvg.Checked)
                {
                    if (DaqData.sysVarInfo.InstantDataLogging_Variables.Count > 0)
                    {
                        if (DaqData.DataLoggingAvg)
                        {
                            DataLoggingAvgStartStop(false);
                        }
                        DaqData.InitDaqDataLogging();
                        DataLoggingAvgStartStop(true, int.Parse(tbLoggingTimeAvg.Text));
                    }
                    else
                    {
                        MessageBox.Show(new Form { TopMost = true }, "Logging Data Not Defined!", "Data Logging");
                    }
                }
                else
                {
                    if (DaqData.DataLoggingAvg)
                    {
                        DataLoggingAvgStartStop(false);
                    }
                }
            }

        }

        private void chkLoggingCtn_CheckedChanged(object sender, EventArgs e)
        {
            if (bMouseClicked)
            {
                bMouseClicked = false;
                if (chkLoggingCtn.Checked)
                {
                    if (DaqData.sysVarInfo.InstantDataLogging_Variables.Count > 0)
                    {
                        if (DaqData.DataLoggingCtn)
                        {
                            DataLoggingCtnStartStop(false);
                        }
                        DataLoggingCtnStartStop(true, int.Parse(tbLoggingTimeCtn.Text));
                    }
                    else
                    {
                        MessageBox.Show(new Form { TopMost = true }, "Logging Data Not Defined!", "Data Logging");
                    }
                    bMouseClicked = false;
                }
                else
                {
                    if (DaqData.DataLoggingCtn)
                    {
                        DataLoggingCtnStartStop(false);
                    }
                }
            }
        }

        private void ModbusSet_MouseClick(object sender, MouseEventArgs e)
        {
            bMouseClicked = true;
        }

        double TotalAvgTime = 0;
        double TotalCtnTime = 0;
        double TotalConstantTime = 0;


        private void chkLoggingConstant_CheckedChanged(object sender, EventArgs e)
        {
            DataLoggingConstantStartStop(chkLoggingConstant.Checked, false);
        }



        #endregion  Data Logging

    }

    public class MonitorForm
    {
        public Panel pnlMon = null;
        public MonitoringForm.CustomMonitor cMonitor = null;
        public ToolStripMenuItem RunFormMenu = null;
        public ToolStripMenuItem RunDockingMenu = null;
        public ToolStripMenuItem EditMenu = null;
        public ToolStripMenuItem RenameMenu = null;
        public ToolStripMenuItem DeleteMenu = null;
        public ToolStripMenuItem CloseMenu = null;
        public TabPage tab = null;
        public string MonitorName = "";
        public bool isDovking = false;
    }
}
