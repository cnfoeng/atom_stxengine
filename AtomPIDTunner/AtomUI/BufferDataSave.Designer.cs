﻿namespace AtomUI
{
    partial class BufferDataSave
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnAutoQuery = new System.Windows.Forms.Button();
            this.tbControl = new System.Windows.Forms.RichTextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.lblUnique = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblBuffer = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbControl2 = new System.Windows.Forms.RichTextBox();
            this.tbControl3 = new System.Windows.Forms.RichTextBox();
            this.btnMapping = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAutoQuery
            // 
            this.btnAutoQuery.Location = new System.Drawing.Point(12, 12);
            this.btnAutoQuery.Name = "btnAutoQuery";
            this.btnAutoQuery.Size = new System.Drawing.Size(134, 25);
            this.btnAutoQuery.TabIndex = 70;
            this.btnAutoQuery.Text = "시작";
            this.btnAutoQuery.UseVisualStyleBackColor = true;
            this.btnAutoQuery.Click += new System.EventHandler(this.btnAutoQuery_Click);
            // 
            // tbControl
            // 
            this.tbControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tbControl.Location = new System.Drawing.Point(152, 14);
            this.tbControl.Name = "tbControl";
            this.tbControl.Size = new System.Drawing.Size(406, 736);
            this.tbControl.TabIndex = 69;
            this.tbControl.Text = "";
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 15);
            this.label1.TabIndex = 71;
            this.label1.Text = "UniqueNo : ";
            // 
            // lblUnique
            // 
            this.lblUnique.AutoSize = true;
            this.lblUnique.Location = new System.Drawing.Point(100, 80);
            this.lblUnique.Name = "lblUnique";
            this.lblUnique.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblUnique.Size = new System.Drawing.Size(0, 15);
            this.lblUnique.TabIndex = 72;
            this.lblUnique.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(97, 104);
            this.lblTime.Name = "lblTime";
            this.lblTime.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblTime.Size = new System.Drawing.Size(0, 15);
            this.lblTime.TabIndex = 74;
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 15);
            this.label4.TabIndex = 73;
            this.label4.Text = "Start Time : ";
            // 
            // lblBuffer
            // 
            this.lblBuffer.AutoSize = true;
            this.lblBuffer.Location = new System.Drawing.Point(100, 56);
            this.lblBuffer.Name = "lblBuffer";
            this.lblBuffer.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblBuffer.Size = new System.Drawing.Size(0, 15);
            this.lblBuffer.TabIndex = 76;
            this.lblBuffer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 15);
            this.label3.TabIndex = 75;
            this.label3.Text = "Buffer No : ";
            // 
            // tbControl2
            // 
            this.tbControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tbControl2.Location = new System.Drawing.Point(564, 14);
            this.tbControl2.Name = "tbControl2";
            this.tbControl2.Size = new System.Drawing.Size(407, 736);
            this.tbControl2.TabIndex = 77;
            this.tbControl2.Text = "";
            // 
            // tbControl3
            // 
            this.tbControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tbControl3.Location = new System.Drawing.Point(977, 14);
            this.tbControl3.Name = "tbControl3";
            this.tbControl3.Size = new System.Drawing.Size(407, 736);
            this.tbControl3.TabIndex = 78;
            this.tbControl3.Text = "";
            // 
            // btnMapping
            // 
            this.btnMapping.Location = new System.Drawing.Point(12, 162);
            this.btnMapping.Name = "btnMapping";
            this.btnMapping.Size = new System.Drawing.Size(134, 25);
            this.btnMapping.TabIndex = 79;
            this.btnMapping.Text = "변수매핑";
            this.btnMapping.UseVisualStyleBackColor = true;
            this.btnMapping.Click += new System.EventHandler(this.btnMapping_Click);
            // 
            // BufferDataSave
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1390, 753);
            this.Controls.Add(this.btnMapping);
            this.Controls.Add(this.tbControl3);
            this.Controls.Add(this.tbControl2);
            this.Controls.Add(this.lblBuffer);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblUnique);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAutoQuery);
            this.Controls.Add(this.tbControl);
            this.Name = "BufferDataSave";
            this.Text = "BufferDataSave";
            this.Load += new System.EventHandler(this.BufferDataSave_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAutoQuery;
        private System.Windows.Forms.RichTextBox tbControl;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblUnique;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblBuffer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox tbControl2;
        private System.Windows.Forms.RichTextBox tbControl3;
        private System.Windows.Forms.Button btnMapping;
    }
}