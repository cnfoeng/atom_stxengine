﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ACS.SPiiPlusNET;
using System.IO;
using AtomUI.Class;

namespace AtomUI
{
    public partial class BufferDataSave : Form
    {
        public Api Ch; //  For communcating between the UMD and the Host Application. All communication commands are methods within the AsyncChannel Class.
        private int BufferStartIndex = 0x900;
        private int BufferIncreaseIndex = 0x100;
        private int BufferSize = 20;
        private int LastBufferUniqueNo = 0;
        private int LastBufferNo = 0;

        private int BufferIODefinitionIndex = 0x500;
        public List<AIFunction> lstAIDef = new List<AIFunction>();
        public List<AIFunction> lstSaveDef = new List<AIFunction>();

        public BufferDataSave()
        {
            InitializeComponent();
        }

        private void btnAutoQuery_Click(object sender, EventArgs e)
        {
            if (btnAutoQuery.Text == "시작")
            {
                BufferStart = 0;
                btnAutoQuery.Text = "멈춤";
                btnMapping.Enabled = false;
                timer1.Start();
                string Header = "Buffer,UniqueNo.,StartTime";
                foreach (AIFunction AI in lstSaveDef)
                    Header = Header + "," + AI.Name;

                strFileName = Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\Log\\DataSave" + DateTime.Now.ToString("yyyy-MM-dd") + "_" + DateTime.Now.ToFileTime().ToString() + ".csv";
                FileSave(Header + "\r\n");

            }
            else
            {
                timer1.Stop();
                btnAutoQuery.Text = "시작";
                btnMapping.Enabled = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            ReadVariable();
        }
        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, 
                                                      DateTimeKind.Utc);

        public static DateTime UnixTimeToDateTime(double seconds)
        {
            return Epoch.AddSeconds(seconds);
        }

        private int BufferStart = 0;
        private void ReadVariable()
        {
            Object Result;
            try
            {
                Result = Ch.ReadVariable("g_PC_Read_Data", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);
            }
            catch
            {
                MessageBox.Show("정의되지 않은 변수입니다.");
                return;
            }

            int BufferIndex = LastBufferNo + 1;
            if (BufferIndex >= BufferSize)
                BufferIndex = 0;

            for (int i = 0; i < BufferSize; i++)
            {
                int Index = BufferStartIndex + BufferIndex * BufferIncreaseIndex;
                int BufferUniqueNo = ((int[])Result)[Index + 1];
                if (LastBufferUniqueNo==32766)
                    LastBufferUniqueNo = -1;
                if ((BufferStart == 1 & BufferUniqueNo == LastBufferUniqueNo + 1) || (BufferStart == 0 && BufferUniqueNo > LastBufferUniqueNo))
                {
                    int[] DataBuffer = new int[0xb0];
                    int BufferNo = ((int[])Result)[Index];

                    BufferStart = 1;
                    LastBufferUniqueNo = BufferUniqueNo;
                    LastBufferNo = BufferNo;
                    int StartTime = ((int[])Result)[Index + 5];
                    
                    lblUnique.Text = BufferUniqueNo.ToString();
                    lblTime.Text = StartTime.ToString(); //UnixTimeToDateTime((double)StartTime).ToString();
                    lblBuffer.Text = BufferNo.ToString();

                    Array.Copy((int[])Result, Index, DataBuffer, 0, 0xb0);
                    String Value = lblBuffer.Text + "," + lblUnique.Text + "," + lblTime.Text;

                    String strDisplay = "";
                    String strDisplay2 = "";
                    String strDisplay3 = "";
                    foreach (AIFunction AI in lstSaveDef)
                    {
                        if (AI.Data_Index==0)
                        {
                            AI.SetValue(DataBuffer[AI.Buffer_Index]);
                        }
                        else
                        {
                            AI.SetValue(((int[])Result)[AI.Data_Index]);
                        }
                        Value = Value + "," + AI.Value.ToString();
                        string strType = "";
                        if (AI.Data_Index > 0)
                            strType = "(EX)";
                        else if (AI.HWAdress > 9000)
                            strType = "(CV)";
                        else
                            strType = "(AI)";
                        
                        if (AI.index<37)
                            strDisplay = strDisplay + strType + AI.Name.ToString() + " = \t" + AI.Value.ToString() + "(" + AI.intValue.ToString() + ":" + AI.HWAdress.ToString() + ")\n";
                        else if (AI.index < 73)
                            strDisplay2 = strDisplay2 + strType + AI.Name.ToString() + " = \t" + AI.Value.ToString() + "(" + AI.intValue.ToString() + ":" + AI.HWAdress.ToString() + ")\n";
                        else
                            strDisplay3 = strDisplay3 + strType + AI.Name.ToString() + " = \t" + AI.Value.ToString() + "(" + AI.intValue.ToString() + ":" + AI.HWAdress.ToString() + ")\n";

                    } //foreach(AIFunction AI in lstAIDef)
                    FileSave(Value+"\r\n");

                    tbControl.Text = strDisplay;
                    tbControl2.Text = strDisplay2;
                    tbControl3.Text = strDisplay3;
                    //tbControl.Text = tbControl.Text + "," + LastBufferUniqueNo.ToString();
                } //if (BufferUniqueNo > LastBufferUniqueNo)
                BufferIndex++;
                if (BufferIndex >= BufferSize)
                    BufferIndex = 0;
            } //for (int i = 0; i < BufferSize; i++)
        }

        string strFileName;
        private void FileSave(string strMessage)
        {
            System.IO.File.AppendAllText(strFileName, strMessage);
        }

        private String ArrayString(object Result)
        {
            string Str = "";
            Type ResultType = Result.GetType();
            if (ResultType == typeof(int))
            {
                Str = Str + ((int)Result).ToString() + "\n";
            }
            else if (ResultType == typeof(double))
            {
                Str = Str + ((double)Result).ToString() + "\n";
            }
            else if (ResultType == typeof(Double[]))
            {
                Str = Str + VectorToString(Result, true);
            }
            else if (ResultType == typeof(int[]))
            {
                Str = Str + VectorToString(Result, false);
            }
            else if (ResultType == typeof(Double[,]))
            {
                Str = Str + MatrixToString(Result, true);
            }
            else //(ResultType == typeof(int[][]))
            {
                Str = Str + MatrixToString(Result, false);
            }

            return Str;
        }

        private String MatrixToString(object Matrix, bool doubleType)
        {
            String Str = "";
            int To1, To2;

            To1 = doubleType ? ((double[,])Matrix).GetUpperBound(0) : ((int[,])Matrix).GetUpperBound(0);
            To2 = doubleType ? ((double[,])Matrix).GetUpperBound(1) : ((int[,])Matrix).GetUpperBound(1);
            for (int i = 0; i <= To1; i++)
            {
                for (int j = 0; j <= To2; j++)
                {
                    Str = Str + (doubleType ? ((double[,])Matrix)[i, j] : ((int[,])Matrix)[i, j]) + " ";
                }
                Str = Str + "\n";
            }
            return Str;
        }


        private String VectorToString(object Vector, bool doubleType)
        {
            String Str = "";
            int To;
            To = doubleType ? ((double[])Vector).GetUpperBound(0) : ((int[])Vector).GetUpperBound(0);
            for (int i = 0; i <= To; i++)
                Str = Str + (doubleType ? ((double[])Vector)[i] : ((int[])Vector)[i]) + " ";
            return Str;
        }

        private void BufferDataSave_Load(object sender, EventArgs e)
        {
            StreamReader sr = new StreamReader(new FileStream(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\IO Reference\\AI Function Definition.csv", FileMode.Open));
            string[] AllLines = sr.ReadToEnd().Split('\n');
            object Result;
            try
            {
                Result = Ch.ReadVariable("g_PC_Read_Data", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);
            }
            catch
            {
                MessageBox.Show("정의되지 않은 변수입니다.");
                return;
            }
            int Index = BufferIODefinitionIndex;
            int[] BufferIO = new int[256];
            int ValueStartIndex = 0;
            for (int j = 0; j < 256; j++)
            {
                BufferIO[j] = ((int[])Result)[Index + j];
                if (ValueStartIndex==0 && BufferIO[j]==-1)
                    ValueStartIndex = j;
            }
            int ValueIndex = 0;
            for (int i = 1; i < AllLines.Length; i++)
            {
                string[] Values = AllLines[i].Split(',');
                if (Values.Length > 10)
                {
                    AIFunction AI = new AIFunction(i, Values[0], Values[1], Values[2],
                        Values[3], Values[4], Values[5], Values[6], Values[7], Values[8], Values[9], Values[10]);
                    if (AI.Data_Index != 0)
                    {
                        AI.Buffer_Use = 1;
                    }
                    if (AI.HWAdress < 10000)
                        for (int k = 0; k < 256; k++)
                        {
                            if (BufferIO[k] == AI.HWAdress)
                            {
                                AI.Buffer_Index = k+16;
                                break;
                            }
                        }
                    else if (AI.Data_Index == 0)
                    {
                        AI.Buffer_Index = ValueStartIndex + ValueIndex+16;
                        ValueIndex++;
                    }
                    lstAIDef.Add(AI);
                }
            }


            sr.Close();
            sr.Dispose();
        }

        private int GetInt(string sValue)
        {
            int Value = 0;
            try {
                Value = int.Parse(sValue);
            } catch
            {

            }
            return Value;
        }

        private void btnMapping_Click(object sender, EventArgs e)
        {
            Mapping pidTunner = new Mapping();
            pidTunner.lstAIDef = this.lstAIDef;
            pidTunner.lstSaveDef = this.lstSaveDef;
            pidTunner.Show(this);

        }
    }

    public class AIFunction
    {
        public int index;
        public int HWAdress;
        public string Name;
        public string Unit;
        public double S_Min;
        public double S_Max;
        public double P_Min;
        public double P_Max;
        public int Index_1;
        public int Index_2;
        public int Data_Index;
        public int Buffer_Use;
        public int Buffer_Index;
        public int Rounded_Limit;
        public double Value;
        public int intValue;

        public AIFunction(int index, string HWAdress, string Name, string Unit, string S_Min, string S_Max, string P_Min,
            string P_Max, string Index_1, string Index_2, string Data_Index, string Rounded_Limit)
        {
            this.index = index;
            this.HWAdress = GetInt(HWAdress);
            this.Name = Name;
            this.Unit = Unit;
            this.S_Min = GetDouble(S_Min);
            this.S_Max = GetDouble(S_Max);
            this.P_Min = GetDouble(P_Min);
            this.P_Max = GetDouble(P_Max);
            this.Index_1 = GetInt(Index_1);
            this.Index_2 = GetInt(Index_2);
            this.Data_Index = GetInt(Data_Index);
            this.Rounded_Limit = GetInt(Rounded_Limit);
        }

        public void SetValue(int Data)
        {
            this.intValue = Data;
            if (this.HWAdress >= 10000 && this.Data_Index == 0)
                this.Value = (double)Data / 1000;
            else if (this.Data_Index > 0)
                this.Value = (double)Data / 10;
            else if (this.Rounded_Limit == 0)
                this.Value = ((double)Data - this.S_Min) / (this.S_Max - this.S_Min) * (this.P_Max - this.P_Min) + this.P_Min;
            else
                this.Value = Math.Round(((double)Data - this.S_Min) / (this.S_Max - this.S_Min) * (this.P_Max - this.P_Min) + this.P_Min, this.Rounded_Limit);
        }

        private int GetInt(string sValue)
        {
            int Value = 0;
            try
            {
                Value = int.Parse(sValue);
            }
            catch
            {

            }
            return Value;
        }
        private double GetDouble(string sValue)
        {
            double Value = 0.0;
            try
            {
                Value = double.Parse(sValue);
            }
            catch
            {

            }
            return Value;
        }
    }
}
