﻿namespace AtomUI
{
    partial class BufferStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            this.cbVariable = new System.Windows.Forms.ComboBox();
            this.cbBuffer = new System.Windows.Forms.ComboBox();
            this.btnQuery = new System.Windows.Forms.Button();
            this.PortLb = new System.Windows.Forms.Label();
            this.tbControl = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnAutoQuery = new System.Windows.Forms.Button();
            this.lvVariable = new System.Windows.Forms.ListView();
            this.Header1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnExpand = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbVariable
            // 
            this.cbVariable.FormattingEnabled = true;
            this.cbVariable.Items.AddRange(new object[] {
            "g_ControlMode",
            "g_DriveManual_Mode",
            "g_PC_Read_Data",
            "g_PC_Write_Data",
            "g_rPC_Write_Data",
            "g_PC_Process_Data_TqAlphaProfile",
            "g_PC_Process_Data_SpAlphaProfile",
            "g_PC_Process_Data_TqSpProfile",
            "g_PC_Process_Data_SpTqProfile",
            "g_Dyno_PidControl_StateMachine",
            "g_Dyno_PID_Process_ValList",
            "g_Eng_PidControl_StateMachine",
            "g_Eng_PID_Process_ValList"});
            this.cbVariable.Location = new System.Drawing.Point(383, 10);
            this.cbVariable.Name = "cbVariable";
            this.cbVariable.Size = new System.Drawing.Size(635, 23);
            this.cbVariable.TabIndex = 68;
            // 
            // cbBuffer
            // 
            this.cbBuffer.FormattingEnabled = true;
            this.cbBuffer.Items.AddRange(new object[] {
            "Global",
            "Buffer 0",
            "Buffer 1",
            "Buffer 2",
            "Buffer 3",
            "Buffer 4",
            "Buffer 5",
            "Buffer 6",
            "Buffer 7",
            "Buffer 8",
            "Buffer 9"});
            this.cbBuffer.Location = new System.Drawing.Point(105, 10);
            this.cbBuffer.Name = "cbBuffer";
            this.cbBuffer.Size = new System.Drawing.Size(204, 23);
            this.cbBuffer.TabIndex = 69;
            this.cbBuffer.SelectedIndexChanged += new System.EventHandler(this.cbBuffer_SelectedIndexChanged);
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(1024, 10);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(134, 25);
            this.btnQuery.TabIndex = 67;
            this.btnQuery.Text = "조회";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // PortLb
            // 
            this.PortLb.Location = new System.Drawing.Point(5, 3);
            this.PortLb.Name = "PortLb";
            this.PortLb.Size = new System.Drawing.Size(107, 38);
            this.PortLb.TabIndex = 66;
            this.PortLb.Text = "Buffer Select";
            this.PortLb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbControl
            // 
            this.tbControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbControl.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbControl.Location = new System.Drawing.Point(383, 44);
            this.tbControl.Name = "tbControl";
            this.tbControl.Size = new System.Drawing.Size(560, 633);
            this.tbControl.TabIndex = 71;
            this.tbControl.Text = "";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(5, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(353, 38);
            this.label1.TabIndex = 72;
            this.label1.Text = "Buffer Scalar Variable";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnAutoQuery
            // 
            this.btnAutoQuery.Location = new System.Drawing.Point(1164, 12);
            this.btnAutoQuery.Name = "btnAutoQuery";
            this.btnAutoQuery.Size = new System.Drawing.Size(134, 25);
            this.btnAutoQuery.TabIndex = 73;
            this.btnAutoQuery.Text = "자동조회";
            this.btnAutoQuery.UseVisualStyleBackColor = true;
            this.btnAutoQuery.Click += new System.EventHandler(this.btnAutoQuery_Click);
            // 
            // lvVariable
            // 
            this.lvVariable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lvVariable.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Header1,
            this.Header2});
            this.lvVariable.FullRowSelect = true;
            this.lvVariable.GridLines = true;
            listViewGroup1.Header = "ListViewGroup";
            listViewGroup1.Name = "Global Variables";
            listViewGroup2.Header = "ListViewGroup";
            listViewGroup2.Name = "Local Variables";
            this.lvVariable.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2});
            this.lvVariable.Location = new System.Drawing.Point(8, 69);
            this.lvVariable.MultiSelect = false;
            this.lvVariable.Name = "lvVariable";
            this.lvVariable.ShowGroups = false;
            this.lvVariable.Size = new System.Drawing.Size(350, 608);
            this.lvVariable.TabIndex = 74;
            this.lvVariable.UseCompatibleStateImageBehavior = false;
            this.lvVariable.View = System.Windows.Forms.View.Details;
            this.lvVariable.DoubleClick += new System.EventHandler(this.lvVariable_DoubleClick);
            // 
            // Header1
            // 
            this.Header1.Text = "Name";
            this.Header1.Width = 220;
            // 
            // Header2
            // 
            this.Header2.Text = "Values";
            this.Header2.Width = 80;
            // 
            // btnExpand
            // 
            this.btnExpand.Location = new System.Drawing.Point(316, 10);
            this.btnExpand.Name = "btnExpand";
            this.btnExpand.Size = new System.Drawing.Size(42, 23);
            this.btnExpand.TabIndex = 75;
            this.btnExpand.Text = ">>";
            this.btnExpand.UseVisualStyleBackColor = true;
            this.btnExpand.Click += new System.EventHandler(this.btnExpand_Click);
            // 
            // BufferStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(950, 684);
            this.Controls.Add(this.btnExpand);
            this.Controls.Add(this.lvVariable);
            this.Controls.Add(this.btnAutoQuery);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbControl);
            this.Controls.Add(this.cbVariable);
            this.Controls.Add(this.cbBuffer);
            this.Controls.Add(this.btnQuery);
            this.Controls.Add(this.PortLb);
            this.Name = "BufferStatus";
            this.Text = "Monitoriing : ACS Running Status";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.BfferStatus_FormClosed);
            this.Load += new System.EventHandler(this.BfferStatus_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbVariable;
        private System.Windows.Forms.ComboBox cbBuffer;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Label PortLb;
        private System.Windows.Forms.RichTextBox tbControl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnAutoQuery;
        private System.Windows.Forms.ListView lvVariable;
        private System.Windows.Forms.ColumnHeader Header1;
        private System.Windows.Forms.ColumnHeader Header2;
        private System.Windows.Forms.Button btnExpand;
    }
}