﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ACS.SPiiPlusNET;

namespace AtomUI
{
    public partial class BufferStatus : Form
    {
        public Api Ch; //  For communcating between the UMD and the Host Application. All communication commands are methods within the AsyncChannel Class.
        private int CurrentBuffer = 0;
        private ProgramBuffer buffer0 = ProgramBuffer.ACSC_NONE;
        private ProgramBuffer buffer1 = ProgramBuffer.ACSC_NONE;
        string[] buffer_VectorVariable;
        string[] buffer_ScalarGlobal;
        string[] buffer_ScalarVariable;

        #region BufferDVariable
        string[] bufferD_VectorVariable =
        {
            "g_bufferProgramResult",
            "g_lInter_fbTorque_RefeVal_List",
            "g_lInter_fbSpeed_RefeVal_List",
            "g_lInter_fbAlpha_RefeVal_List",
            "g_lInter_fbDynoCurrent_RefeVal_List",
            "g_lInter_opDynoCurrent_RefeVal_List",
            "g_lInter_fbTorque_AppliedVal_List",
            "g_lInter_fbSpeed_AppliedVal_List",
            "g_lInter_fbAlpha_AppliedVal_List",
            "g_lInter_fbDynoCurrent_AppliedVal_List",
            "g_lInter_opDynoCurrent_AppliedVal_List",
            "g_eCAT_Input_Data",
            "g_eCAT_Input_Data_ExistIndex",
            "g_eCAT_Input_Address",
            "g_eCAT_Input_ByteSize",
            "g_eCAT_Input_Type",
            "g_eCAT_Input_SlaveIndex",
            "g_eCAT_RS232_Input_Data",
            "g_eCAT_Output_Data",
            "g_eCAT_Output_Data_ExistIndex",
            "g_eCAT_Output_Address",
            "g_eCAT_Output_ByteSize",
            "g_eCAT_Output_Type",
            "g_eCAT_Output_SlaveIndex",
            "g_eCAT_RS232_Output_Data",
            "g_PC_Write_Data",
            "g_rPC_Write_Data",
            "gl_HmiVar_FlashWrite",
            "gl_HmiVar_FlashRead",
            "g_PC_Write_Data_TqAlphaProfile",
            "g_PC_Write_Data_SpAlphaProfile",
            "g_PC_Write_Data_TqSpProfile",
            "g_PC_Write_Data_SpTqProfile",
            "g_PC_Process_Data_TqAlphaProfile",
            "g_PC_Process_Data_SpAlphaProfile",
            "g_PC_Process_Data_TqSpProfile",
            "g_PC_Process_Data_SpTqProfile",
            "g_Dyno_PID_Process_ValList",
            "g_Eng_PID_Process_ValList",
            "g_PC_Read_Data",
            "g_EcDynoCurPI_Process_ValList",
            "g_RS232C_ComPortStatus"
        };
        string[] bufferD_ScalarGlobal = new string[]
            {
             "g_ControlMode",
            "g_DriveManual_Mode",
                "g_SystemState_Dyno_On",
                "g_SystemState_Reset",
                "g_SystemState_EMS",
                "g_SystemState_Eng_IG_On",
                "g_SystemState_Eng_Preheat_On",
                "g_SystemState_Eng_Starting",
                "g_SystemState_Eng_Started",
                "g_SystemState_Misc_On",
                "g_stop_StopMode",
                "g_ControlMode",
                "g_DriveManual_Mode",
                "g_Dyno_Control_Operable",
                "g_Dyno_ControlMode",
                "g_Dyno_PidControl_StateMachine",
                "g_Eng_Control_Operable",
                "g_Eng_ControlMode",
                "g_Eng_PidControl_StateMachine"
           };

        string[] bufferD_ScalarVariable = new string[]
            {
                "g_Alarm_Exists_Bit",
                "g_Error_Exists_Bit",
                "g_Warning_Exists_Bit",
                "g_Alarm_Register_Request_AlarmID",
                "g_Alarm_Register_Request_Bit",
                "g_Bf8_AutoR_Banner_Display",
            };
        #endregion
        #region Buffer0Variable
        string[] buffer0_VectorVariable =
        {
        };
        string[] buffer0_ScalarGlobal = new string[]
            {
                "g_Alarm_Register_Request_AlarmID",
                "g_Alarm_Register_Request_Bit"
            };

        string[] buffer0_ScalarVariable = new string[]
            {
            };
        #endregion
        #region Buffer1Variable
        string[] buffer1_VectorVariable =
        {
            "g_lInter_fbTorque_RefeVal_List",
            "g_lInter_fbSpeed_RefeVal_List",
            "g_lInter_fbAlpha_RefeVal_List",
            "g_lInter_fbDynoCurrent_RefeVal_List",
            "g_lInter_fbTorque_AppliedVal_List",
            "g_lInter_fbSpeed_AppliedVal_List",
            "g_lInter_fbAlpha_AppliedVal_List",
            "g_lInter_fbDynoCurrent_AppliedVal_List"
        };
        string[] buffer1_ScalarGlobal = new string[]
            {
                "g_dynoType",
                "g_Hmi_bs10x_Demand_Speed",
                "g_Hmi_bs10x_Demand_AlphaPosi",
                "g_Hmi_UserDefine_TagCount"
            };

        string[] buffer1_ScalarVariable = new string[]
            {
                "BufferProgrmaNumber",
                "eCatNetworkSlaveNum",
                "eCatNetworkMappingCompleteNum",
                "eCatNodeIndex",
                "eCatNotCouplerNodeIndex",
                "eCatSerialInterface_EL6002_ComPortDataIndex",
                "eCatSerialInterface_EL6002_NodeCount",
                "eCatNodeAddressOffset",
                "eCatNetworkMappingCompleteNum",
                "eCatInputDataIndex",
                "eCatOutputDataIndex",
                "eCatInputDataPropertyIndex",
                "eCatOutputDataPropertyIndex",
                "index_HmiTagVar_FlashAccess",
                "index_LinearInterpolation_FlashReadValAccess",
                "FlashRead_LinearInterpolationReadCount",
                "index_LinearInterpolation_List",
                "indexOffset_FlashReadRefeVal_AppliedVal"
            };
        #endregion
        #region Buffer2Variable
        string[] buffer2_VectorVariable =
        {
            "list_Alarm_History"
        };
        string[] buffer2_ScalarGlobal = new string[]
            {
                "g_Alarm_Exists_Bit",
                "g_Error_Exists_Bit",
                "g_Warning_Exists_Bit",
                "g_dynoCtrl_OilPumpOnRPM",
                "g_SystemState_Dyno_On",
                "g_Feedback_Speed"
            };

        string[] buffer2_ScalarVariable = new string[]
            {
                "BufferProgrmaNumber",
                "alarm_Filter_E_Dyno_AMP_Fault",
                "alarm_Filter_E_Dyno_OilPump_Level_Fault",
                "alarm_Filter_E_Dyno_Water_Temp_Fault",
                "alarm_Filter_E_Dyno_CasingRing_Fault"
            };
        #endregion
        #region Buffer3Variable
        string[] buffer3_VectorVariable = new string[]
            {
                "DrMn_MoChg_Interlock"
            };
        string[] buffer3_ScalarGlobal = new string[]
            {
                "g_Buffer3_displayCmdLevel",
                "g_ControlMode",
                "g_DriveManual_Mode",
                //"g_Hmi_Banner_MsgLineNum",
                //"g_Bf8_AutoR_Banner_Display",
                "g_Dyno_ControlMode",
                "g_Eng_ControlMode",
                "g_SystemState_Dyno_On",
                "g_SystemState_Eng_IG_On",
                "g_SystemState_Eng_Preheat_On",
                "g_SystemState_Eng_Starting",
                "g_SystemState_Eng_Started",
                "g_stop_StopMode",
                "g_SystemState_Reset",
                "g_SystemState_EMS"
            };

        string[] buffer3_ScalarVariable = new string[]
            {
                "BufferProgrmaNumber",
                "Sys_Current_Command_Input",
                "Sys_Previous_Command_Input",
                "CtMo_Current_ChangeReq_Input",
                "CtMo_Previous_ChangeReq_Input",
                "DrMn_ManualMode_Current_ChangeReq_Input",
                "DrMn_ManualMode_Previous_ChangeReq_Input",
                "DrMn_AutomaticMode_Current_ChangeReq_Input",
                "DrMn_AutomaticMode_Previous_ChangeReq_Input",
                "DrMn_Current_ChangeReq_Process_Input",
                "DrMn_Previous_ChangeReq_Process_Input",
                "DrMn_ChangeReq_Process_Execute",
                "DrMn_bitIndex_Idle",
                "DrMn_bitIndex_IdleContrl",
                "DrMn_bitIndex_DyTo_EngTA",
                "DrMn_bitIndex_DyRPM_EngTA",
                "DrMn_bitIndex_DyTo_EngRPM",
                "DrMn_bitIndex_DyRPM_EngTo",
                "DrMn_Before_IdleContrl_OnTime",
                "DrMn_Before_DyTo_EngTA_OnTime",
                "DrMn_Before_DyRPM_EngTA_OnTime",
                "DrMn_Before_DyTo_EngRPM_OnTime",
                "DrMn_Before_DyRPM_EngTo_OnTime",
                "DrMn_IdleContrl_ModeChangeReqConut",
                "DrMn_DyTo_EngTA_ModeChangeReqConut",
                "DrMn_DyRPM_EngTA_ModeChangeReqConut",
                "DrMn_DyTo_EngRPM_ModeChangeReqConut",
                "DrMn_DyRPM_EngTo_ModeChangeReqConut",
                "DrMn_Current_ChangeReq_Mode",
                "DrMn_Previous_ChangeReq_Mode",
                "DrMn_Function_Enable",
                "DriveModeChangeCondition_IdleExcept",
                "DriveMode_MoChgCompleteTime",
                 "index_HmiTagVar_FlashAccess",
                "index_Hmi_EdtValToStoreValSave",
                "Hmi_EdtValToStoreValSaveCount",
                "Hmi_SaveTimeOut_Measure_1",
                "Hmi_SaveTimeOut_Measure_2",
                "Hmi_SaveTimeOut_Difference",
                "OpBox_HmiScreenChgCheck_PreviousControlMode",
                "OpBox_HmiScreenChgCheck_PreviousDriveMode",
                "OpBox_EmoSwitch_PreviousStatus",
                "Sys_refreshBitIndex_Dyno_On",
                "Sys_refreshBitIndex_Eng_IG_On",
                "Sys_refreshBitIndex_Eng_Start",
                "Sys_refreshBitIndex_Stop",
                "Sys_refreshBitIndex_EMS",
                "Sys_refreshBitIndex_Reset",
                "Dyno_OnOff_Cmd_Processing_Result",
                "Dyno_On_Cmd_Processing_Complete",
                "Dyno_Off_Cmd_Processing_Complete",
                "Engine_Preheat_SwitchOnTimeMeasure_1",
                "Engine_Preheat_SwitchOnTimeMeasure_2",
                "Engine_Start_SwitchOnTimeMeasure_1",
                "Engine_Start_SwitchOnTimeMeasure_2",
                "CtMo_inputBitIndex_Monitor",
                "CtMo_inputBitIndex_DynoService",
                "CtMo_inputBitIndex_Manual",
                "CtMo_inputBitIndex_Automatic",
                "profile_Current_UsingCMD",
                "profile_Prevous_UsingCMD",
                "AutoMd_Current_Control_CMD",
                "AutoMd_Previous_Control_CMD",
                "MesureEqp_BlowByMt_Current_Control_CMD",
                "MesureEqp_BlowByMt_Previous_Control_CMD",
                "MesureEqp_SmokeMt_Current_Control_CMD",
                "MesureEqp_SmokeMt_Previous_Control_CMD",
                "MesureEqp_FuelMt_Current_Control_CMD",
                "MesureEqp_FuelMt_Previous_Control_CMD"
            };
        #endregion

        #region Buffer4Variable
        string[] buffer4_VectorVariable =
        {
            "Average_SpeedList",
            "Average_TorqueList"
        };
        string[] buffer4_ScalarGlobal = new string[]
            {
                "g_Buffer4_displayCmdLevel",
                "g_ControlMode",
                "g_DriveManual_Mode",
                "g_Target_Dyno_Speed",
                "g_Target_Dyno_Torque",
                "g_Target_Engine_Speed",
                "g_Target_Engine_Torque",
                "g_Target_Engine_AlphaPosi",
                "g_SystemState_Eng_Starting",
                "g_SystemState_Eng_Started"
            };

        string[] buffer4_ScalarVariable = new string[]
            {
                "BufferProgrmaNumber",
                "PeriodicControlTime_Measure_1",
                "PeriodicControlTime_Measure_2",
                "pid_Target_Calc_Speed",
                "pid_Target_Calc_Torque",
                "pid_Target_Calc_AlphaPosi"
           };
        #endregion
        #region Buffer5Variable
        string[] buffer5_VectorVariable =
        {
            "Average_SpeedList",
            "Average_TorqueList"
        };
        string[] buffer5_ScalarGlobal = new string[]
            {
                "g_Buffer4_displayCmdLevel",
                "g_Hmi_bs10x_Demand_Speed",
                "g_Hmi_bs10x_Demand_AlphaPosi"
            };

        string[] buffer5_ScalarVariable = new string[]
            {
                "BufferProgrmaNumber",
                "PeriodicControlTime_Measure_1",
                "PeriodicControlTime_Measure_2"
            };
        #endregion
        #region Buffer6Variable
        string[] buffer6_VectorVariable =
        {
            "Average_SpeedList",
            "Average_TorqueList",
            "Eng_S_PID_ValList",
            "Eng_T_PID_ValList",
            "Eng_A_PID_ValList"
        };
        string[] buffer6_ScalarGlobal = new string[]
            {
                "g_Buffer6_displayCmdLevel",
                "g_Eng_Control_Operable",
                "g_Hmi_bs10x_Demand_AlphaPosi",
                "g_Eng_ControlMode",
                "g_Eng_PidControl_StateMachine"
            };

        string[] buffer6_ScalarVariable = new string[]
            {
                "BufferProgrmaNumber",
                "PidList_C_PrevCiportion",
                "PidList_C_PrevCiLimit",
                "Eng_PID_Excution_Interval",
                "Eng_DrvMd_Previous_Mode",
                "Torque_CompanRefeVar",
                "Eng_Pid_Control_Operable",
                "Eng_Pid_Control_CErrorCnt",
                "Eng_Pid_Control_YErrorCnt",
                "Eng_Pid_Control_YPrevValue",
                "Eng_S_Pid_Control_KP_MAX",
                "Eng_S_Pid_Control_iPotion_Delta",
                "Eng_T_Pid_Control_iPotion_Delta",
                "Eng_Pid_Calc_Alpha_1_DaVal",
                "Eng_Pid_Calc_Alpha_2_DaVal",
                "Pid_Time_Measure_1",
                "Pid_Time_Measure_2",
                "Pid_Time_Difference",
                "rstConditionCheck"
            };
        #endregion
        #region Buffer7Variable
        string[] buffer7_VectorVariable =
        {
            "Average_SpeedList",
            "Average_TorqueList"
        };
        string[] buffer7_ScalarGlobal = new string[]
            {
                "g_Buffer4_displayCmdLevel",
                "g_Hmi_bs10x_Demand_Speed",
                "g_Hmi_bs10x_Demand_AlphaPosi"
            };

        string[] buffer7_ScalarVariable = new string[]
            {
                "BufferProgrmaNumber",
                "PeriodicControlTime_Measure_1",
                "PeriodicControlTime_Measure_2"
            };
        #endregion
        #region Buffer8Variable
        string[] buffer8_VectorVariable =
        {
            "Average_SpeedList",
            "Average_TorqueList"
        };
        string[] buffer8_ScalarGlobal = new string[]
            {
                "g_Buffer4_displayCmdLevel",
                "g_Hmi_bs10x_Demand_Speed",
                "g_Hmi_bs10x_Demand_AlphaPosi"
            };

        string[] buffer8_ScalarVariable = new string[]
            {
                "BufferProgrmaNumber",
                "PeriodicControlTime_Measure_1",
                "PeriodicControlTime_Measure_2"
            };
        #endregion
        #region Buffer9Variable
        string[] buffer9_VectorVariable =
        {
            "bbm_ComState_rawRecvData",
            "bbm_BlowByMeanValue_StringList",
            "bbm_BlowByMeanValue_MesureTime_StringList",
            "smkm_ComState_rawRecvData",
            "smkm_SmokeMeter_MesureResult_StringList",
            "smkm_SmokeMeter_MesureValue_StringList",
            "fm_ComState_rawRecvData",
            "fm_FuelMeter_MesureResult_StringList",
            "fm_FuelMeter_MesureValue_StringList",
            "fm_FuelMeter_FuelMeter_Filling_levele_StringList"
        };
        string[] buffer9_ScalarGlobal = new string[]
            {
                "g_Buffer9_displayCmdLevel",
                "g_BlowByMeter_442_CtrlCMD",
                "g_BlowByMeter_442_Status",
                "g_BlowByMeter_442_ErrorCode",
                "g_SmokeMeter_415_CtrlCMD",
                "g_SmokeMeter_415_Status",
                "g_SmokeMeter_415_ErrorCode",
                "g_FuelMeter_733_CtrlCMD",
                "g_FuelMeter_733_Status",
                "g_FuelMeter_733_ErrorCode",
                "g_FuelMeterMeasureResult_Filling_level"
            };

        string[] buffer9_ScalarVariable = new string[]
            {
                "BufferProgrmaNumber",
                "PeriodicControlTime_Measure_1",
                "PeriodicControlTime_Measure_2",
                "PeriodicControlTime_Difference",
                "PeriodicControlTime_ProgExcution_Interval",
                "bbm_Initial_Progress_StepAndStatus",
                "bbm_Measure_Progress_StepAndStatus",
                "bbm_ComCtrl_TransmitRequest",
                "bbm_ComState_TransmitAcceptedOffCheckDelay",
                "bbm_ComCtrl_TransmitRequestOnTime",
                "bbm_ComCtrl_ReceiveListClearRequest",
                "bbm_ComState_ReceiveFail_AlarmDelay",
                "bbm_ResetRequestTime",
                "bbm_ResetRequestExecuteDelayTime",
                "bbm_ResetReqCompTime",
                "bbm_ResetAnsTimeOutCheckTime",
                "bbm_StandByRequestTime",
                "bbm_StandByRequestExecuteDelayTime",
                "bbm_StandByReqCompTime",
                "bbm_StandByAnsTimeOutCheckTime",
                "bbm_StartInterMesurementRequestTime",
                "bbm_StartInterMesurementRequestExecuteDelayTime",
                "bbm_StartInterMesurementReqCompTime",
                "bbm_StartInterMesurementAnsTimeOutCheckTime",
                "bbm_QueryInterMesurementValRequestTime",
                "bbm_QueryInterMesurementValRequestInterval",
                "bbm_QueryInterMesurementValReqCompTime",
                "bbm_QueryInterMesurementValAnsTimeOutCheckTime",
                "bbm_ComState_PreviousRecvRequest",
                "bbm_ComState_ComRecvLength",
                "bbm_index_rawRecvDataList",
                "bbm_index_ComState_RsInputDataList",
                "bbm_ComState_ParserStartRequest",
                "bbm_ComState_RecieveRequestOffCheckDelay",
                "bbm_RecvPacketLength",
                "bbm_index_ParserAnsHederCheck",
                "bbm_Phaser_Result_Ack",
                "bbm_index_Start_BlowByMeanValue_StringList",
                "bbm_size_Start_BlowByMeanValue_StringList",
                "bbm_index_Start_BlowByMeanValue_MesureTime_StringList",
                "bbm_size_Start_BlowByMeanValue_MesureTime_StringList",
                "bbm_Alarm_DetectCount",
                "bbm_Alarm_RegisterRequestCount",
                "smkm_Initial_Progress_StepAndStatus",
                "smkm_Measure_Progress_StepAndStatus",
                "smkm_ComCtrl_TransmitRequest",
                "smkm_ComState_TransmitAcceptedOffCheckDelay",
                "smkm_ComCtrl_TransmitRequestOnTime",
                "smkm_ComCtrl_ReceiveListClearRequest",
                "smkm_ComState_ReceiveFail_AlarmDelay",
                "smkm_ResetRequestTime",
                "smkm_ResetRequestExecuteDelayTime",
                "smkm_ResetReqCompTime",
                "smkm_ResetAnsTimeOutCheckTime",
                "smkm_SwitchRemoteModeReqCompTime",
                "smkm_SwitchRemoteModeAnsTimeOutCheckTime",
                "smkm_SwitchRemoteModeStateQryReqTime",
                "smkm_SwitchRemoteModeStateQryAnsTimeOutCheckTime",
                "smkm_DefineMesureCycleReqCompTime",
                "smkm_DefineMesureCycleAnsTimeOutCheckTime",
                "smkm_SetReadySamplingReqCompTime",
                "smkm_SetReadySamplingAnsTimeOutCheckTime",
                "smkm_SetReadySamplingStateQryReqTime",
                "smkm_SetReadySamplingStateQryAnsTimeOutCheckTime",
                "smkm_StartSamplingReqCompTime",
                "smkm_StartSamplingAnsTimeOutCheckTime",
                "smkm_StartSamplingStateQryReqTime",
                "smkm_StartSamplingStateQryAnsTimeOutCheckTime",
                "smkm_MeasureResultReqCompTime",
                "smkm_MeasureResultAnsTimeOutCheckTime",
                "smkm_CurrentStatusQueryRequestTime",
                "smkm_CurrentStatusQueryRequestExecuteDelayTime",
                "smkm_CurrentStatusQueryReqCompTime",
                "smkm_CurrentStatusQueryAnsTimeOutCheckTime",
                "smkm_ComState_PreviousRecvRequest",
                "smkm_ComState_ComRecvLength",
                "smkm_ComState_rawRecvData",
                "smkm_index_rawRecvDataList",
                "smkm_index_ComState_RsInputDataList",
                "smkm_ComState_ParserStartRequest",
                "smkm_ComState_RecieveRequestOffCheckDelay",
                "smkm_RecvPacketLength",
                "smkm_index_ParserAnsHederCheck",
                "smkm_index_ParserAnsStatusCheck",
                "smkm_index_ParserAnsMeasureValCheck",
                "smkm_Phaser_Result_Ack",
                "smkm_index_SmokeMeter_MesureResult_StringList",
                "smkm_Size_SmokeMeter_MesureResult_StringList",
                "smkm_index_mokeMeter_MesureValue_StringList",
                "smkm_size_mokeMeter_MesureValue_StringList",
                "smkm_Alarm_DetectCount",
                "smkm_Alarm_RegisterRequestCount",
                "fm_Initial_Progress_StepAndStatus",
                "fm_Measure_Progress_StepAndStatus",
                "fm_ComCtrl_TransmitRequest",
                "fm_ComState_TransmitAcceptedOffCheckDelay",
                "fm_ComCtrl_TransmitRequestOnTime",
                "fm_ComCtrl_ReceiveListClearRequest",
                "fm_ComState_ReceiveFail_AlarmDelay",
                "fm_ComState_PreviousRecvRequest",
                "fm_ComState_ComRecvLength",
                "fm_index_rawRecvDataList",
                "fm_index_ComState_RsInputDataList",
                "fm_ComState_ParserStartRequest",
                "fm_ComState_RecieveRequestOffCheckDelay",
                "fm_RecvPacketLength",
                "fm_index_ParserAnsHederCheck",
                "fm_index_ParserAnsStatusCheck",
                "fm_index_ParserAnsMeasureValCheck",
                "fm_Phaser_Result_Ack",
                "fm_index_ParserAnsFillingValCheck",
                "fm_index_FuelMeter_MesureResult_StringList",
                "fm_Size_FuelMeter_MesureResult_StringList",
                "fm_index_mokeMeter_MesureValue_StringList",
                "fm_size_mokeMeter_MesureValue_StringList",
                "fm_index_FuelMeter_Filling_level_StringList",
                "fm_size_FuelMeter_Filling_level_StringList",
                "fm_Alarm_DetectCount",
                "fm_Alarm_RegisterRequestCount"
            };
        #endregion

        public BufferStatus()
        {
            InitializeComponent();
        }

        private void cbBuffer_SelectedIndexChanged(object sender, EventArgs e)
        {
            timer1.Stop();
            CurrentBuffer = cbBuffer.SelectedIndex;
            setBufferType(CurrentBuffer);
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DisplayBufferStatus();
        }

        private void DisplayBufferStatus()
        {
            object Result = null;
            for (int ind = 0; ind < buffer_ScalarGlobal.Length; ind++)
            {
                try
                {
                    Result = Ch.ReadVariable(buffer_ScalarGlobal[ind], buffer0, -1, -1, -1, -1);
                    if (lvVariable.Items[ind].SubItems[1].Text == Result.ToString())
                        lvVariable.Items[ind].SubItems[0].ForeColor = Color.Black;
                    else
                    {
                        lvVariable.Items[ind].SubItems[1].Text = Result.ToString();
                        lvVariable.Items[ind].SubItems[0].ForeColor = Color.Red;
                    }
                }
                catch { }
            }
            for (int ind = 0; ind < buffer_ScalarVariable.Length; ind++)
            {
                try
                {
                    Result = Ch.ReadVariable(buffer_ScalarVariable[ind], buffer1, -1, -1, -1, -1);
                    if (buffer_ScalarVariable[ind] == lvVariable.Items[buffer_ScalarGlobal.Length + ind].Text)
                    {
                        if (lvVariable.Items[buffer_ScalarGlobal.Length + ind].SubItems[1].Text == Result.ToString())
                            lvVariable.Items[buffer_ScalarGlobal.Length + ind].SubItems[0].ForeColor = Color.Blue;
                        else
                        {
                            lvVariable.Items[buffer_ScalarGlobal.Length + ind].SubItems[1].Text = Result.ToString();
                            lvVariable.Items[buffer_ScalarGlobal.Length + ind].SubItems[0].ForeColor = Color.Red;
                        }
                    }
                }
                catch { }
            }
            if (AutoRead)
                ReadVariable();
        }

        private void setBufferType(int Index)
        {
            switch (Index)
            {
                case 0:
                    buffer1 = ProgramBuffer.ACSC_NONE;
                    buffer_VectorVariable = bufferD_VectorVariable;
                    buffer_ScalarGlobal = bufferD_ScalarGlobal;
                    buffer_ScalarVariable = bufferD_ScalarVariable;
                    break;
                case 1:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_0;
                    buffer_VectorVariable = buffer0_VectorVariable;
                    buffer_ScalarGlobal = buffer0_ScalarGlobal;
                    buffer_ScalarVariable = buffer0_ScalarVariable;
                    break;
                case 2:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_1;
                    buffer_VectorVariable = buffer1_VectorVariable;
                    buffer_ScalarGlobal = buffer1_ScalarGlobal;
                    buffer_ScalarVariable = buffer1_ScalarVariable;
                    break;
                case 3:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_2;
                    buffer_VectorVariable = buffer2_VectorVariable;
                    buffer_ScalarGlobal = buffer2_ScalarGlobal;
                    buffer_ScalarVariable = buffer2_ScalarVariable;
                    break;
                case 4:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_3;
                    buffer_VectorVariable = buffer3_VectorVariable;
                    buffer_ScalarGlobal = buffer3_ScalarGlobal;
                    buffer_ScalarVariable = buffer3_ScalarVariable;
                    break;
                case 5:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_4;
                    buffer_VectorVariable = buffer4_VectorVariable;
                    buffer_ScalarGlobal = buffer4_ScalarGlobal;
                    buffer_ScalarVariable = buffer4_ScalarVariable;
                    break;
                case 6:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_5;
                    buffer_VectorVariable = buffer5_VectorVariable;
                    buffer_ScalarGlobal = buffer5_ScalarGlobal;
                    buffer_ScalarVariable = buffer5_ScalarVariable;
                    break;
                case 7:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_6;
                    buffer_VectorVariable = buffer6_VectorVariable;
                    buffer_ScalarGlobal = buffer6_ScalarGlobal;
                    buffer_ScalarVariable = buffer6_ScalarVariable;
                    break;
                case 8:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_7;
                    buffer_VectorVariable = buffer7_VectorVariable;
                    buffer_ScalarGlobal = buffer7_ScalarGlobal;
                    buffer_ScalarVariable = buffer7_ScalarVariable;
                    break;
                case 9:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_8;
                    buffer_VectorVariable = buffer8_VectorVariable;
                    buffer_ScalarGlobal = buffer8_ScalarGlobal;
                    buffer_ScalarVariable = buffer8_ScalarVariable;
                    break;
                case 10:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_9;
                    buffer_VectorVariable = buffer9_VectorVariable;
                    buffer_ScalarGlobal = buffer9_ScalarGlobal;
                    buffer_ScalarVariable = buffer9_ScalarVariable;
                    break;
            } //switch (Index)

            cbVariable.Items.Clear();
            foreach (string itmx in buffer_VectorVariable)
                cbVariable.Items.Add(itmx);

            lvVariable.Items.Clear();
            foreach (string itmx in buffer_ScalarGlobal)
            {
                ListViewItem itm = lvVariable.Items.Add(itmx);
                itm.SubItems.Add("0");
            }
            foreach (string itmx in buffer_ScalarVariable)
            {
                ListViewItem itm = lvVariable.Items.Add(itmx);
                itm.SubItems.Add("0");
                itm.SubItems[0].ForeColor = Color.Blue;
            }

        } //private void setBufferType(int Index)

        private void BfferStatus_Load(object sender, EventArgs e)
        {
            this.Width = 350;

            cbBuffer.SelectedIndex = 0;
            this.Ch = Class.Ethercat.Ch;
        }

        private void BfferStatus_FormClosed(object sender, FormClosedEventArgs e)
        {
            timer1.Stop();
        }

        private void ReadVariable()
        {
            string Str = "";
            Object Result = null;

            if (cbVariable.Text.Length == 0)
            {
                AutoRead = false;
                return;
            }

            try
            {
                Result = Ch.ReadVariable(cbVariable.Text, buffer1, -1, -1, -1, -1);
            }
            catch
            {
                MessageBox.Show("정의되지 않은 변수입니다.");
                AutoRead = false;
                return;
            }

            Type ResultType = Result.GetType();
            if (ResultType == typeof(int))
            {
                Str = Str + ((int)Result).ToString() + "\n";
            }
            else if (ResultType == typeof(double))
            {
                Str = Str + ((double)Result).ToString() + "\n";
            }
            else if (ResultType == typeof(Double[]))
            {
                Str = Str + VectorToString(Result, true);
            }
            else if (ResultType == typeof(int[]))
            {
                Str = Str + VectorToString(Result, false);
            }
            else if (ResultType == typeof(Double[,]))
            {
                Str = Str + MatrixToString(Result, true);
            }
            else //(ResultType == typeof(int[][]))
            {
                Str = Str + MatrixToString(Result, false);
            }

            tbControl.Text = Str;
        }

        private String MatrixToString(object Matrix, bool doubleType)
        {
            String Str = "";
            int To1, To2;

            To1 = doubleType ? ((double[,])Matrix).GetUpperBound(0) : ((int[,])Matrix).GetUpperBound(0);
            To2 = doubleType ? ((double[,])Matrix).GetUpperBound(1) : ((int[,])Matrix).GetUpperBound(1);
            for (int i = 0; i <= To1; i++)
            {
                for (int j = 0; j <= To2; j++)
                {
                    Str = Str + (doubleType ? ((double[,])Matrix)[i, j] : ((int[,])Matrix)[i, j]) + " ";
                }
                Str = Str + "\n";
            }
            return Str;
        }


        private String VectorToString(object Vector, bool doubleType)
        {
            String Str = "";
            int To;
            To = doubleType ? ((double[])Vector).GetUpperBound(0) : ((int[])Vector).GetUpperBound(0);
            for (int i = 0; i <= To; i++)
            {
                if (i - i / 16 * 16 == 0)
                    Str = Str + string.Format("{0:X4}", i) + " : " + string.Format("{0,9:###,##0}", (doubleType ? ((double[])Vector)[i] : ((int[])Vector)[i])) + " ";
                else if (i - i / 16 * 16 == 15)
                    Str = Str + string.Format("{0,9:###,##0}", (doubleType ? ((double[])Vector)[i] : ((int[])Vector)[i])) + "\n";
                else
                    Str = Str + string.Format("{0,9:###,##0}", (doubleType ? ((double[])Vector)[i] : ((int[])Vector)[i])) + " ";
            }
            return Str;
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            ReadVariable();
        }
        private bool AutoRead = false;

        private void btnAutoQuery_Click(object sender, EventArgs e)
        {
            if (btnAutoQuery.Text == "자동조회")
            {
                cbBuffer.Enabled = false;
                cbVariable.Enabled = false;
                btnQuery.Enabled = false;
                btnAutoQuery.Text = "자동조회 멈춤";
                AutoRead = true;
            }
            else
            {
                cbBuffer.Enabled = true;
                cbVariable.Enabled = true;
                btnQuery.Enabled = true;
                btnAutoQuery.Text = "자동조회";
                AutoRead = false;
            }
        }

        private void lvVariable_DoubleClick(object sender, EventArgs e)
        {
            InputForm frm = new InputForm();
            ListViewItem lv = lvVariable.SelectedItems[0];
            frm.varname = lv.Text;
            frm.varvalue = lv.SubItems[1].Text;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (lvVariable.SelectedIndices[0] < buffer_ScalarGlobal.Length)
                        Ch.WriteVariable(frm.strReturn, lv.Text, buffer0, -1, -1, -1, -1);
                    else
                        Ch.WriteVariable(frm.strReturn, lv.Text, buffer1, -1, -1, -1, -1);
                }
                catch
                {
                    MessageBox.Show("Undefined Variable");
                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnExpand_Click(object sender, EventArgs e)
        {
            Button Expander = (Button)sender;
            if (Expander.Text == ">>")
            {
                ((Button)sender).Text = "<<";
                this.Width = 1328;
            } else
            {
                ((Button)sender).Text = ">>";
                AutoRead = false;
                this.Width = 350;
            }
        }
    }
}
