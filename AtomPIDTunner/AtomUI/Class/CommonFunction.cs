﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;
using System.Drawing;

namespace AtomUI.Class
{
    public enum LogType { Info, Error };

    public class CommonFunction
    {
        static string strLogFileName = Application.StartupPath + "\\log";
        static string strStartUpPath = Application.StartupPath;
        static int nErrorIndex = 0;
        static private object lockObject = new object();

        public static void DrawOpacityImage(Image image, int Opacity, Color opacityColor)
        {
            using (Graphics g = Graphics.FromImage(image))
            {
                Color OpacityColor = Color.FromArgb(Opacity, opacityColor);
                SolidBrush brush = new SolidBrush(OpacityColor);

                g.FillRectangle(brush, 0, 0, image.Width, image.Height);

                brush.Dispose();
            }
        }

        public static void DrawShadow(Graphics g, Control pnlControl)
        {
            //우측상단
            g.DrawImage(Properties.Resources.topRight_Shadow, pnlControl.Right, pnlControl.Top, 5, 5);

            //우측, TextureBrush 사용
            TextureBrush tb = new TextureBrush(Properties.Resources.right_Shadow);
            tb.TranslateTransform(pnlControl.Right, 0); //이미지 반복 시작 위치를 설정합니다. (기본값은 (0,0) 입니다.)
            g.FillRectangle(tb, pnlControl.Right, pnlControl.Top + 5, 5, pnlControl.Height - 5);

            //우측하단
            g.DrawImage(Properties.Resources.bottomRight_Shadow, pnlControl.Right, pnlControl.Bottom, 5, 5);

            //하단
            tb = new TextureBrush(Properties.Resources.bottom_Shadow);
            tb.TranslateTransform(0, pnlControl.Bottom); //이미지 반복 시작 위치를 설정합니다. (기본값은 (0,0) 입니다.)
            g.FillRectangle(tb, pnlControl.Left + 5, pnlControl.Bottom, pnlControl.Width - 5, 5);

            //좌측 하단
            g.DrawImage(Properties.Resources.bottomLeft_Shadow, pnlControl.Left, pnlControl.Bottom, 5, 5);
        }

        public static string LogFileName(LogType enLogType)
        {
            string strFileName = "";
            switch (enLogType)
            {
                case LogType.Info:
                    strFileName = strLogFileName + "\\info\\Log_" + DateTime.Now.ToString("yyyy-MM-dd");
                    break;
                case LogType.Error:
                    strFileName = strLogFileName + "\\error\\Error_" + DateTime.Now.ToString("yyyy-MM-dd");
                    break;
            }
            return strFileName;
        }

        private static string LogFileName(LogType enLogType, int UID)
        {
            return LogFileName(enLogType) + UID.ToString();
        }

        public static void LogWrite(string strFileName, DateTime TimeStamp, string strMessage)
        {
            try
            {
                System.IO.File.AppendAllText(strFileName + ".log", TimeStamp.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + strMessage + "\r\n");
            }
            catch
            {
                nErrorIndex++;
                strFileName = strFileName + "_LogError" + nErrorIndex.ToString() + ".log";
                System.IO.File.AppendAllText(strFileName, strMessage);
            }
        }

        public static void LogWrite(LogType enLogType, DateTime TimeStamp, string strMessage)
        {
            string strFileName = LogFileName(enLogType);
            try
            {
                lock (lockObject)
                {
                    System.IO.File.AppendAllText(strFileName + ".log", TimeStamp.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + strMessage + "\r\n");
                }
            }
            catch
            {
                nErrorIndex++;
                strFileName = strFileName + "_LogError" + nErrorIndex.ToString() + ".log";
                System.IO.File.AppendAllText(strFileName, TimeStamp.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + strMessage + "\r\n");
            }
        }

        public static void LogWrite(LogType enLogType, DateTime TimeStamp, int nUID, string strMessage)
        {
            string strFileName = LogFileName(enLogType, nUID);
            try
            {
                System.IO.File.AppendAllText(strFileName + ".log", TimeStamp.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + strMessage + "\r\n");
            }
            catch
            {
                nErrorIndex++;
                strFileName = strFileName + "_LogError" + nErrorIndex.ToString() + ".log";
                System.IO.File.AppendAllText(strFileName, TimeStamp.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + strMessage + "\r\n");
            }
        }

        public static void LogWrite(LogType enLogType, DateTime TimeStamp, int nUID, string strHeader, byte[] strMessage)
        {
            string strFileName = LogFileName(enLogType, nUID);
            try
            {
                System.IO.File.AppendAllText(strFileName + ".log", TimeStamp.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + strHeader + "\t" + GetHexaString(strMessage) + "\r\n");
            }
            catch
            {
                nErrorIndex++;
                strFileName = strFileName + "_LogError" + nErrorIndex.ToString() + ".log";
                System.IO.File.AppendAllText(strFileName, TimeStamp.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + strHeader + "\t" + GetHexaString(strMessage) + "\r\n");
            }
        }

        public static string GetHexaString(byte[] Mesage)
        {
            return BitConverter.ToString(Mesage, 0);
        }

        /// <summary>
        /// 해당 글자를 byte로 변환한다.(글자수에 맞추어)
        /// </summary>
        /// <param name="strMessage"></param>
        /// <param name="nSize"></param>
        /// <returns></returns>
        public static byte[] GetByte(string strMessage, int nSize)
        {
            byte[] dimResult = new byte[nSize];
            byte[] dim = Encoding.UTF8.GetBytes(strMessage);
            for (int i = 0; i < dimResult.Length; i++)
            {
                if (dim.Length > i)
                {
                    dimResult[i] = dim[i];
                }
            }

            return dimResult;
        }


        /// <summary>
        /// byte 를 더한다.
        /// </summary>
        /// <param name="dim1"></param>
        /// <param name="dim2"></param>
        /// <returns></returns>
        public static byte[] BytePlus(byte[] dim1, byte[] dim2)
        {
            byte[] dimResult = new byte[dim1.Length + dim2.Length];
            int nIndex = 0;
            for (int i = 0; i < dim1.Length; i++)
            {
                dimResult[nIndex] = dim1[i];
                nIndex++;
            }
            for (int i = 0; i < dim2.Length; i++)
            {
                dimResult[nIndex] = dim2[i];
                nIndex++;
            }

            return dimResult;
        }

        public static byte[] BytePlus(byte dim1, byte dim2)
        {
            byte[] dimResult = new byte[2];

            dimResult[0] = dim1;
            dimResult[1] = dim2;

            return dimResult;
        }
        public static byte[] BytePlus(byte[] dim1, byte dim2)
        {
            byte[] dimResult = new byte[dim1.Length + 1];
            int nIndex = 0;
            for (int i = 0; i < dim1.Length; i++)
            {
                dimResult[nIndex] = dim1[i];
                nIndex++;
            }

            dimResult[nIndex] = dim2;

            return dimResult;
        }

        public static byte[] BytePlus(byte dim1, byte[] dim2)
        {
            byte[] dimResult = new byte[1 + dim2.Length];
            int nIndex = 0;

            dimResult[nIndex] = dim1;
            nIndex++;
            for (int i = 0; i < dim2.Length; i++)
            {
                dimResult[nIndex] = dim2[i];
                nIndex++;
            }

            return dimResult;
        }


        /// <summary>
        /// 체크썸
        /// </summary>
        /// <param name="dim"></param>
        /// <returns></returns>
        static public byte CheckSum(byte[] dim)
        {
            byte chk = 255;

            for (int i = 0; i < dim.Length; i++)
            {
                chk ^= (byte)dim[i];
            }

            return chk;
        }

        /// <summary>
        /// 체크썸
        /// </summary>
        /// <param name="dim"></param>
        /// <returns></returns>
        static public byte CheckSum(byte[] dim, int Start)
        {
            byte chk = 0;

            for (int i = Start; i < dim.Length; i++)
            {
                chk ^= (byte)dim[i];
            }

            return chk;
        }
        /// <summary>
        /// 체크썸
        /// </summary>
        /// <param name="dim"></param>
        /// <returns></returns>
        static public byte CheckSum(byte[] dim, int Start, int Length)
        {
            byte chk = 255;

            for (int i = Start; i < Start + Length; i++)
            {
                chk ^= (byte)dim[i];
            }

            return chk;
        }

        static public bool FileMove(string Source, string Target)
        {
            try
            {
                System.IO.File.Move(Source, Target);
                return true;
            }
            catch (Exception ex)
            {
                LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "   파일이동 에러");
                return false;
            }

        }

        static public bool CreateDir(string DirName)
        {
            DirectoryInfo drinfo = new DirectoryInfo(DirName);
            if (!drinfo.Exists)
            {
                drinfo.Create();
                return true;
            }
            return false;

            //try
            //{
            //    Directory.CreateDirectory(DirName);
            //    return true;
            //}
            //catch
            //{
            //    return false;
            //}
        }

        static public bool CreateLogDirectory()
        {
            return CreateDir(strStartUpPath + "\\log");
        }

        public static string GetFilePath(string FileName)
        {
            return FileName.Substring(0, FileName.LastIndexOf('\\'));
        }

        public static string GetFileNameOnly(string FileName)
        {
            return FileName.Substring(FileName.LastIndexOf('\\') + 1, FileName.Length - FileName.LastIndexOf('\\') - 1);
        }

        public static string getNewTimeFileName(string FileName)
        {
            string fileNameOnly = FileName.Substring(0, FileName.LastIndexOf('.'));
            string fileExt = FileName.Substring(FileName.LastIndexOf('.') + 1, FileName.Length - FileName.LastIndexOf('.') - 1);
            fileNameOnly = fileNameOnly + "_" + DateTime.Now.Year.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Hour.ToString("00") + DateTime.Now.Minute.ToString("00") + DateTime.Now.Second.ToString("00") + "_" + DateTime.Now.Millisecond;
            return fileNameOnly + "." + fileExt;
        }

        public static void XMLSave(string FileName, object InfoClass)
        {
            try
            {
                FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write);
                XmlSerializer xs = new XmlSerializer(InfoClass.GetType());
                xs.Serialize(fs, InfoClass);
                fs.Close();
            }
            catch
            {

            }
        }

        public static object XMLLoad(string FileName, object InfoClass)
        {
            try
            {
                FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
                XmlSerializer xs = new XmlSerializer(InfoClass.GetType());
                InfoClass = xs.Deserialize(fs);
                fs.Close();
                return InfoClass;
            }
            catch
            {
                return null;
            }
        }
    }




    public class SystemLog
    {
        public int RunID = 0;
        public int LogID = 0;
        public DateTime LogTIme = DateTime.Now;
        public string LogType = "";
        public string LogName = "";
        public string CurrentStatus = "";
        public string StatusDescription = "";

        public SystemLog(int RunID, int LogID, string LogType, string LogName, string CurrentStatus, string StatusDescription)
        {
            this.RunID = RunID;
            this.LogID = LogID;
            this.LogType = LogType;
            this.LogName = LogName;
            this.CurrentStatus = CurrentStatus;
            this.StatusDescription = StatusDescription;
        }
    }

}