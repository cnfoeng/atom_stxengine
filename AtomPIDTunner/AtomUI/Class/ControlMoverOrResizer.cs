﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace AtomUI.Class
{
    internal class ControlMoverOrResizer
    {
        
        private static bool _moving;
        private static Point _cursorStartPoint;
        private static bool _moveIsInterNal;
        private static bool _resizing;
        private static Size _currentControlStartSize;
        internal static bool MouseIsInLeftEdge { get; set; }
        internal static bool MouseIsInRightEdge { get; set; }
        internal static bool MouseIsInTopEdge { get; set; }
        internal static bool MouseIsInBottomEdge { get; set; }
        private static int _Width = 0;
        private static int _Left = 0;
        private static int _Height = 0;
        private static int _Top = 0;

        internal static Control selectedObject;

        internal enum MoveOrResize
        {
            Move,
            Resize,
            MoveAndResize
        }

        internal static MoveOrResize WorkType { get; set; }

        internal static void Init(Control control)
        {
            Init(control, control);
        }

        internal static void Init(Control control, Control container)
        {
            _moving = false;
            _resizing = false;
            _moveIsInterNal = false;
            _cursorStartPoint = Point.Empty;
            MouseIsInLeftEdge = false;
            MouseIsInLeftEdge = false;
            MouseIsInRightEdge = false;
            MouseIsInTopEdge = false;
            MouseIsInBottomEdge = false;
            //WorkType = MoveOrResize.MoveAndResize;
            control.MouseDown += (sender, e) => StartMovingOrResizing(control, e);
            control.MouseUp += (sender, e) => StopDragOrResizing(control);
            control.MouseMove += (sender, e) => MoveControl(container, e);
        }

        private static void UpdateMouseEdgeProperties(Control control, Point mouseLocationInControl)
        {
            if (WorkType == MoveOrResize.Move)
            {
                return;
            }
            MouseIsInLeftEdge = Math.Abs(mouseLocationInControl.X) <= 8;
            MouseIsInRightEdge = Math.Abs(mouseLocationInControl.X - control.Width) <= 8;
            MouseIsInTopEdge = Math.Abs(mouseLocationInControl.Y ) <= 8;
            MouseIsInBottomEdge = Math.Abs(mouseLocationInControl.Y - control.Height) <= 8;
        }

        private static void UpdateMouseCursor(Control control)
        {
            if (WorkType == MoveOrResize.Move)
            {
                return;
            }
            if (MouseIsInLeftEdge )
            {
                if (MouseIsInTopEdge)
                {
                    control.Cursor = Cursors.SizeNWSE;
                }
                else if (MouseIsInBottomEdge)
                {
                    control.Cursor = Cursors.SizeNESW;
                }
                else
                {
                    control.Cursor = Cursors.SizeWE;
                }
            }
            else if (MouseIsInRightEdge)
            {
                if (MouseIsInTopEdge)
                {
                    control.Cursor = Cursors.SizeNESW;
                }
                else if (MouseIsInBottomEdge)
                {
                    control.Cursor = Cursors.SizeNWSE;
                }
                else
                {
                    control.Cursor = Cursors.SizeWE;
                }
            }
            else if (MouseIsInTopEdge || MouseIsInBottomEdge)
            {
                control.Cursor = Cursors.SizeNS;
            }
            else
            {
                control.Cursor = Cursors.Default;
            }
        }

        private static void StartMovingOrResizing(Control control, MouseEventArgs e)
        {
            if (_moving || _resizing)
            {
                return;
            }
            if (WorkType!=MoveOrResize.Move &&
                (MouseIsInRightEdge || MouseIsInLeftEdge || MouseIsInTopEdge || MouseIsInBottomEdge))
            {
                _resizing = true;
                _currentControlStartSize = control.Size;
            }
            else if (WorkType!=MoveOrResize.Resize)
            {
                _moving = true;
                control.Cursor = Cursors.Hand;
            }
            _cursorStartPoint = new Point(e.X, e.Y);

            _Width = control.Width;
            _Left = control.Left;
            _Height = control.Height;
            _Top = control.Top;
            control.SuspendLayout();

            control.BringToFront();
            control.Capture = true;
            System.Diagnostics.Debug.WriteLine("_cursorStartPoint Top = " + _cursorStartPoint.Y + " Top = " + _currentControlStartSize.Height);

        }

        private static void MoveControl(Control control, MouseEventArgs e)
        {

            if (!_resizing && ! _moving)
            {
                selectedObject = control;
                UpdateMouseEdgeProperties(control, new Point(e.X, e.Y));
                UpdateMouseCursor(control);
            }
            if (_resizing)
            {
                if (MouseIsInLeftEdge)
                {
                    if (MouseIsInTopEdge)
                    {
                        _Width -= (e.X - _cursorStartPoint.X);
                        _Left += (e.X - _cursorStartPoint.X);
                        _Height -= (e.Y - _cursorStartPoint.Y);
                        _Top += (e.Y - _cursorStartPoint.Y);
                    }
                    else if (MouseIsInBottomEdge)
                    {
                        _Width -= (e.X - _cursorStartPoint.X);
                        _Left += (e.X - _cursorStartPoint.X);
                        _Height = (e.Y - _cursorStartPoint.Y) + _currentControlStartSize.Height;                    
                    }
                    else
                    {
                        _Width -= (e.X - _cursorStartPoint.X);
                        _Left += (e.X - _cursorStartPoint.X) ;
                    }
                }
                else if (MouseIsInRightEdge)
                {
                    if (MouseIsInTopEdge)
                    {
                        _Width = (e.X - _cursorStartPoint.X) + _currentControlStartSize.Width;
                        _Height -= (e.Y - _cursorStartPoint.Y);
                        _Top += (e.Y - _cursorStartPoint.Y);

                    }
                    else if (MouseIsInBottomEdge)
                    {
                        _Width = (e.X - _cursorStartPoint.X) + _currentControlStartSize.Width;
                        _Height = (e.Y - _cursorStartPoint.Y) + _currentControlStartSize.Height;                    
                    }
                    else
                    {
                        _Width = (e.X - _cursorStartPoint.X)+_currentControlStartSize.Width;
                    }
                }
                else if (MouseIsInTopEdge)
                {
                    _Height -=  (e.Y - _cursorStartPoint.Y);
                    _Top += (e.Y - _cursorStartPoint.Y);
                    //System.Diagnostics.Debug.WriteLine("e.Y = " + e.Y);
                }
                else if (MouseIsInBottomEdge)
                {
                    _Height = (e.Y - _cursorStartPoint.Y) + _currentControlStartSize.Height;
                }
                else
                {
                     StopDragOrResizing(control);
                }
            }
            else if (_moving)
            {
                _moveIsInterNal = !_moveIsInterNal;
                if (!_moveIsInterNal)
                {
                    _Left = (e.X - _cursorStartPoint.X) + control.Left;
                    _Top = (e.Y - _cursorStartPoint.Y) + control.Top;
                    //control.Location = new Point(x, y);
                }
            }

            if (_resizing || _moving)
            {
                bool Change = false;

                Size controlSize = new Size((int)(_Width / 20) * 20, (int)(_Height / 20) * 20);
                Point controlLocation = new Point((int)(_Left / 20) * 20, (int)(_Top / 20) * 20);
                if (controlSize.Width < 60)
                {
                    controlSize.Width = 60;
                    controlLocation.X = control.Location.X;
                }
                if (controlSize.Height < 60)
                {
                    controlSize.Height = 60;
                    controlLocation.Y = control.Location.Y;
                }
                if (controlLocation.X < 0)
                    controlLocation.X = 0;
                if (controlLocation.Y < 0)
                    controlLocation.Y = 0;
                if (controlSize.Height != control.Size.Height || controlSize.Width != control.Size.Width || controlLocation.X != control.Location.X || controlLocation.Y != control.Location.Y)
                {
                    Change = true;
                }

                if (Change)
                {
                    //((NextUI.BaseUI.BaseUI)control).AllowPaint = false;
                    if (controlSize.Height != control.Size.Height)
                    {
                        control.Height = controlSize.Height;
                    }
                    if (controlSize.Width != control.Size.Width)
                    {
                        control.Width = controlSize.Width;
                    }
                    if (controlLocation.X != control.Location.X)
                    {
                        control.Left = controlLocation.X;
                    }
                    if (controlLocation.Y != control.Location.Y)
                    {
                        control.Top = controlLocation.Y;
                    }
                    //System.Diagnostics.Debug.WriteLine("_Top = " + _Top + " Top = " + controlLocation.Y);
                    //System.Diagnostics.Debug.WriteLine("_Height = " + _Height + " Height = " + controlSize.Height);
                    //((NextUI.BaseUI.BaseUI)control).AllowPaint = true;
                }
            }

        }

        private static void StopDragOrResizing(Control control)
        {
            _resizing = false;
            _moving = false;
            control.Capture = false;
            UpdateMouseCursor(control);
            control.Invalidate();
            control.ResumeLayout(false);
            control.PerformLayout();
        }

        #region Save And Load

        private static List<Control> GetAllChildControls(Control control, List<Control> list)
        {
            List<Control> controls = control.Controls.Cast<Control>().ToList();
            list.AddRange(controls);
            return controls.SelectMany(ctrl => GetAllChildControls(ctrl, list)).ToList();
        }

        internal static string GetSizeAndPositionOfControlsToString(Control container)
        {
            List<Control> controls = new List<Control>();
            GetAllChildControls(container, controls);
            CultureInfo cultureInfo = new CultureInfo("en");
            string info = string.Empty;
            foreach (Control control in controls)
            {
                info += control.Name + ":" + control.Left.ToString(cultureInfo) + "," + control.Top.ToString(cultureInfo) + "," +
                        control.Width.ToString(cultureInfo) + "," + control.Height.ToString(cultureInfo) + "*";
            }
            return info;
        }
        internal static void SetSizeAndPositionOfControlsFromString(Control container, string controlsInfoStr)
        {
            List<Control> controls = new List<Control>();
            GetAllChildControls(container, controls);
            string[] controlsInfo = controlsInfoStr.Split(new []{"*"},StringSplitOptions.RemoveEmptyEntries );
            Dictionary<string, string> controlsInfoDictionary = new Dictionary<string, string>();
            foreach (string controlInfo in controlsInfo)
            {
                string[] info = controlInfo.Split(new [] { ":" }, StringSplitOptions.RemoveEmptyEntries);
                controlsInfoDictionary.Add(info[0], info[1]);
            }
            foreach (Control control in controls)
            {
                string propertiesStr;
                controlsInfoDictionary.TryGetValue(control.Name, out propertiesStr);
                string[] properties = propertiesStr.Split(new [] { "," }, StringSplitOptions.RemoveEmptyEntries);
                if (properties.Length == 4)
                {
                    control.Left = int.Parse(properties[0]);
                    control.Top = int.Parse(properties[1]);
                    control.Width = int.Parse(properties[2]);
                    control.Height = int.Parse(properties[3]);
                }
            }
        }

        #endregion
    }
}