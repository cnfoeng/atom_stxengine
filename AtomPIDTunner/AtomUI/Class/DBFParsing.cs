﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace AtomUI.Class
{
    public class DBFParsing
    {
        public static void AddDBFChannels(string fpath)
        {
            acsChannelInfo channelItem;
            VariableInfo varItem;
            List<string> ParsedChannels = DBFparsingNormal(fpath);

            acsModuleInfo item = new acsModuleInfo("Inca", "PUMA", 0, 0, ChannelIO.Input, ChannelType.Analog, 0, 0, ParsedChannels.Count);
            item.UseFor = ChannelUseFor.ECU;

            Ethercat.sysInfo.ECUModuleInfos.Add(item);

            int Index = 0;

            foreach (string Channel in ParsedChannels)
            {
                string[] Var = Channel.Split(',');
                channelItem = new acsChannelInfo(item, int.Parse(Var[2]), Index, Var[1], int.Parse(Var[3]));
                if (Var[5] == "U")
                    channelItem.ValueType = 0;
                Ethercat.chInfo.ECU_Channels.Add(channelItem);
                varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
                varItem.Unit = Var[11];
                varItem.NorminalDescription = Var[0];
                varItem.Factor = double.Parse(Var[10]);
                varItem.Offset = double.Parse(Var[9]);
                DaqData.sysVarInfo.ECU_Variables.Add(varItem);

                Index++;
            }
        }
        private static List<string> DBFparsingNormal(string fpath)
        {
            string mSG = "[START_SIGNALS]";
            string mSGStartM = "[START_MSG]";
            List<string> Result = new List<string>();

            //dbf는 전체 라인을 읽어와야 한다.
            string[] dbfAllValue = System.IO.File.ReadAllLines(fpath);
            int frameIndex = 0;

            for (int i = 0; i < dbfAllValue.Length; i++)
            {
                if (dbfAllValue[i].Contains(mSGStartM))
                {
                    frameIndex++;
                }
                else if (dbfAllValue[i].Contains(mSG)) //시그널 행체크후 다음행에 특수행인지 [mSGValue]를 체크해야 한다.  ## 특수문자 데이터 처리.
                {
                    string sName = frameIndex.ToString() + "frame," + dbfAllValue[i].Substring(mSG.Length + 1, dbfAllValue[i].Length - mSG.Length - 1);

                    if (sName.Length > 0)
                    {
                        Result.Add(sName);
                    }
                }
            }
            return Result;
        }

    }
}
