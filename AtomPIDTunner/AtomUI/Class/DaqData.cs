﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;

namespace AtomUI.Class
{
    public class DaqData
    {
        public static bool DataLogging = false;
        public static SysVarInfo sysVarInfo = new SysVarInfo();
        private static List<VariableInfo> DataLogging_Variables = new List<VariableInfo>();
        public static bool dataIndexParsed = false;

        public static SysStatus SystemStatus = new SysStatus();
        static int InitialTime = 0;

        public static void GetSysStatus()
        {
            if (Ethercat.g_PC_Read_Data != null)
            {
                foreach (SysStatusVariable sys in SystemStatus.Status)
                {
                    if (sys.Address < 0x60)
                        sys.Value = Ethercat.g_PC_Read_Data[sys.Address];
                    else
                        sys.Value = Ethercat.g_PC_Read_Data[sys.Address] / 10;

                }

                SystemStatus.ParseStatus();
            }
        }

        public static void ParseValue()
        {
            GetValuesDigital(sysVarInfo.FrontPanel_Variables_Input, Ethercat.g_PC_Read_Data);
            GetValuesDigital(sysVarInfo.FrontPanel_Variables_Output, Ethercat.g_PC_Read_Data);
            GetValuesDigital(sysVarInfo.HMI_IOVariables, Ethercat.g_PC_Read_Data);
            GetValuesDigital(sysVarInfo.DI_Variables, Ethercat.g_PC_Read_Data);
            GetValuesAnalog(sysVarInfo.AI_Variables, Ethercat.g_PC_Read_Data);
            GetValuesDigital(sysVarInfo.DO_Variables, Ethercat.g_PC_Read_Data);
            GetValuesDigital(sysVarInfo.AO_Variables, Ethercat.g_PC_Read_Data);
            GetValuesAnalog(sysVarInfo.Calc_Variables, Ethercat.g_PC_Read_Data);
            //try
            //{
            //    //G = UDiff / URef x Emax / Cn = 2.79995 mV / 9.9997 V x 50 kg / 2(mV / V)) = 7.00009 kg
            //    sysVarInfo.Calc_Variables[0].IOValue = (int)((sysVarInfo.AI_Variables[4].RealValue / sysVarInfo.AI_Variables[5].RealValue * 967 / 2 - 58.4) * 100);
            //}
            //catch { }

            GetSysStatus();

            if (DataLogging)
            {
                DataSave();
            }
        }

        private static string strFileName;
        private static int BufferStart = 0;
        private static int BufferStartIndex = 0x900;
        private static int BufferIncreaseIndex = 0x100;
        private static int BufferSize = 20;
        private static int LastBufferUniqueNo = 0;
        private static int LastBufferNo = 0;

        public static void DataSaveStart()
        {
            DaqData.DataLogging = true;
            DataLogging_Variables = sysVarInfo.DataLogging_Variables;
            VarAddressMappingInput(DataLogging_Variables);

            BufferStart = 0;
            InitialTime = 0;
            string Header = "Buffer,UniqueNo.,StartTime";
            foreach (VariableInfo AI in DataLogging_Variables)
                Header = Header + "," + AI.VariableName;
            strFileName = Application.StartupPath + "\\" + Properties.Settings.Default.DataLoggingDir + "\\" + 
                "DataSave" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm") + "_" + DateTime.Now.ToFileTime().ToString() + ".csv";
            System.IO.File.AppendAllText(strFileName, Header + "\r\n");
        }

        public static void DataSaveStop()
        {
            DaqData.DataLogging = false;
        }

        public static void DataSave()
        {
            int BufferIndex = LastBufferNo + 1;
            if (BufferIndex >= BufferSize)
                BufferIndex = 0;


            for (int i = 0; i < BufferSize; i++)
            {
                int Index = BufferStartIndex + BufferIndex * BufferIncreaseIndex;
                int BufferUniqueNo = (Ethercat.g_PC_Read_Data)[Index + 1];
                if (LastBufferUniqueNo == 32766)
                    LastBufferUniqueNo = -1;
                if ((BufferStart == 1 & BufferUniqueNo == LastBufferUniqueNo + 1) || (BufferStart == 0 && BufferUniqueNo > LastBufferUniqueNo))
                {
                    int[] DataBuffer = new int[0xb0];
                    int BufferNo = (Ethercat.g_PC_Read_Data)[Index];

                    BufferStart = 1;
                    LastBufferUniqueNo = BufferUniqueNo;
                    LastBufferNo = BufferNo;
                    if (InitialTime==0)
                        InitialTime = (Ethercat.g_PC_Read_Data)[Index + 5];
                    int StartTime = (Ethercat.g_PC_Read_Data)[Index + 5] - InitialTime;

                    Array.Copy(Ethercat.g_PC_Read_Data, Index+16, DataBuffer, 0, 0xb0);
                    String Value = BufferNo.ToString() + "," + BufferUniqueNo.ToString() + "," + StartTime.ToString();

                    foreach (VariableInfo AI in DataLogging_Variables)
                    {
                        //foreach (VariableInfo var in DataLogging_Variables)
                        //    var.IOIndex = var.IOIndex - 496; //512-16

                        GetValue(AI, DataBuffer, 512);

                        Value = Value + "," + AI.RealValue.ToString();

                    } //foreach (VariableInfo AI in DataLogging_Variables)
                    System.IO.File.AppendAllText(strFileName, Value + "\r\n");

                } //if (BufferUniqueNo > LastBufferUniqueNo)
                BufferIndex++;
                if (BufferIndex >= BufferSize)
                    BufferIndex = 0;
            } //for (int i = 0; i < BufferSize; i++)

        }

        public static void GetValuesAnalog(List<VariableInfo> Variables, int[] Vector)
        {
            if (Vector != null)
            {
                foreach (VariableInfo var in Variables)
                {
                    if (var.IOIndex != 0)
                    {
                        var.IOValue = ((double)Vector[var.IOIndex]) / 1000;
                    }
                }
            }
        }

        public static void GetValuesDigital(List<VariableInfo> Variables, int[] Vector)
        {
            if (Vector != null)
            {
                foreach (VariableInfo var in Variables)
                {
                    if (var.IOIndex != 0)
                    {
                        //int Mask = (int)(Math.Pow(2, var.ChannelInfo.ChannelBit));
                        var.IOValue = Vector[var.IOIndex];//(Vector[var.IOIndex] & Mask) / Mask;

                    }
                }
            }
        }

        public static void GetValue(VariableInfo var, int[] Vector)
        {
            if (var.IOIndex != 0)
            {
                if (var.ChannelInfo.ModuleInfo.ChannelType == ChannelType.Analog)
                {
                    var.IOValue = Vector[var.IOIndex];
                }
                else
                {
                    int Mask = (int)(Math.Pow(2, var.ChannelInfo.ChannelBit));
                    var.IOValue = (Vector[var.IOIndex] & Mask) / Mask;
                }
            }
        }

        public static void GetValue(VariableInfo var, int[] Vector, int Offset)
        {
            if (var.IOIndex != 0)
            {
                if (var.ChannelInfo.ModuleInfo.ChannelType == ChannelType.Analog)
                {
                    var.IOValue = Vector[var.IOIndex- Offset];
                }
                else
                {
                    int Mask = (int)(Math.Pow(2, var.ChannelInfo.ChannelBit));
                    var.IOValue = (Vector[var.IOIndex] & Mask) / Mask;
                }
            }
        }

        public static void GetAddressIndex()
        {
            sysVarInfo.FrontPanel_Variables_Output.Clear();
            sysVarInfo.FrontPanel_Variables_Input.Clear();
            sysVarInfo.FrontPanel_Variables_Output.Add(FindVariable("g_HMI_DO_DynoOn_Lamp", sysVarInfo.HMI_IOVariables));
            sysVarInfo.FrontPanel_Variables_Output.Add(FindVariable("g_HMI_DO_EngIGOn_Lamp", sysVarInfo.HMI_IOVariables));
            sysVarInfo.FrontPanel_Variables_Output.Add(FindVariable("g_HMI_DO_Manual_Lamp", sysVarInfo.HMI_IOVariables));
            sysVarInfo.FrontPanel_Variables_Output.Add(FindVariable("g_HMI_DO_Remote_Lamp", sysVarInfo.HMI_IOVariables));
            sysVarInfo.FrontPanel_Variables_Output.Add(FindVariable("g_HMI_DO_Automatic_Lamp", sysVarInfo.HMI_IOVariables));

            //VarAddressMappingInput(sysVarInfo.FrontPanel_Variables_Input);
            //VarAddressMappingOutput(sysVarInfo.FrontPanel_Variables_Output);
            //VarAddressMappingInput(sysVarInfo.HMI_IOVariables);
            //VarAddressMappingInput(sysVarInfo.DI_Variables);
            //VarAddressMappingInput(sysVarInfo.AI_Variables);
            //VarAddressMappingOutput(sysVarInfo.DO_Variables);
            //VarAddressMappingOutput(sysVarInfo.AO_Variables);

            dataIndexParsed = true;
        }

        public static VariableInfo FindVariable(string VarName, List<VariableInfo> VarList)
        {
            VariableInfo resVar = null;
            foreach (VariableInfo Var in VarList)
            {
                if (Var.NorminalName == VarName)
                {
                    resVar = Var;
                    break;
                }
            }
            return resVar;
        }

        public static void VarAddressMappingInput(List<VariableInfo> Variables)
        {
            int DataBaseAddress = 512;
            int OffsetBaseAddress = 1024;
            foreach (VariableInfo var in Variables)
            {
                if (var.ChannelInfo != null)
                {
                    for (int ind = 0; ind < 36; ind++)
                    {
                        if (var.ChannelInfo.Offset == Ethercat.g_PC_Read_Data[OffsetBaseAddress + ind])
                        {
                            var.IOIndex = DataBaseAddress + ind;
                            break;
                        }
                    }
                }
            }
        }

        public static void ClearLists()
        {
            sysVarInfo.ClearLists();
        }

        public static void Init()
        {
            Init_HMI_IOVariables();
            if (Ethercat.chInfo.HMI_Channels.Count == sysVarInfo.HMI_IOVariables.Count)
            {
                for (int i = 0; i < sysVarInfo.HMI_IOVariables.Count; i++)
                {
                    sysVarInfo.HMI_IOVariables[i].ChannelInfo = Ethercat.chInfo.HMI_Channels[i];
                }
            }
            Init_HMI_Variables();
            Init_DI_Variables();
            Init_DO_Variables();
            Init_AI_Variables();
            Init_AO_Variables();
            Init_Calc_Variables();

            SaveSysInfo("VarList.xml");
            Properties.Settings.Default.VarInfoFileName = "VarList.xml";
            Properties.Settings.Default.Save();
        }

        public static void SaveSysInfo()
        {
            try
            {
                sysVarInfo.Save(Application.StartupPath + "\\" + Properties.Settings.Default.VarInfoDir + "\\" + Properties.Settings.Default.VarInfoFileName);
            }
            catch { }
        }

        public static void LoadSysInfo()
        {
            try
            {
                sysVarInfo.Load(Application.StartupPath + "\\" + Properties.Settings.Default.VarInfoDir + "\\" + Properties.Settings.Default.VarInfoFileName);
            }
            catch { }
        }

        public static void SaveSysInfo(string FileName)
        {
            FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write);
            XmlSerializer xs = new XmlSerializer(sysVarInfo.GetType());
            xs.Serialize(fs, sysVarInfo);
            fs.Close();
        }

        public static void LoadSysInfo(string FileName)
        {
            sysVarInfo.ClearLists();
            FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
            XmlSerializer xs = new XmlSerializer(sysVarInfo.GetType());
            Object dd = xs.Deserialize(fs);
            sysVarInfo = (SysVarInfo)dd;
            fs.Close();
        }

        public static void Init_HMI_Variables()
        {
            sysVarInfo.HMI_Variables.Add(new VariableInfo("-", "-", "-", 111, sysVarInfo.HMI_Variables.Count));

        }

        public static void Init_HMI_IOVariables()
        {
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("OPEN_LOOP_SW", "g_HMI_DI_DynoService_Button", "OPEN LOOP TEST SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ENG_MON_SW", "g_HMI_DI_Monitor_Button", "ENG MONITOR SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("MISC_SW", "g_HMI_DI_Misc_Button", "MISC ON/OFF  SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("MAN_SW", "g_HMI_DI_Manual_Button", "MANUAL SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("PANEL_SW", "PANEL_SW", "PANEL ENABLE  SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("AUTO_SW", "g_HMI_DI_Automatic_Button", "AUTOMATIC SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP1_SW", "SP1_SW", "SP1_SW", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("REM_SW", "g_HMI_DI_Remote_Button", "REMOTE SWITCH for BK DAS", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("PREHEAT_SW", "g_HMI_DI_EngPreheatOn_Button", "PREHEAT SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("IG_SW", "g_HMI_DI_EngIGOn_Button", "IG ON/OFF SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ENG_STOP_SW", "g_HMI_DI_Stop_Button", "ENG STOP SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ENG_START_SW", "g_HMI_DI_EngStart_Button", "ENG START SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP3_SW", "SP3_SW", "SPARE3 SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP4_SW", "SP4_SW", "SPARE4 SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP5_SW", "SP5_SW", "SPARE5 SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP6_SW", "SP6_SW", "SPARE6 SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("BKREADY", "g_HMI_DI_BKReady_Button", "READY SWITCH for BK DAS", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP8_SW", "SP8_SW", "SPARE8 SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP9_SW", "SP9_SW", "SPARE9 SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP10_SW", "SP10_SW", "SPARE10 SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("DYNO_SW", "g_HMI_DI_DynoOn_Button", "DYNO ON/OFF SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ALM_RESET_SW", "g_HMI_DI_Reset_Button", "ALARM RESET SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("CFG_SW", "g_HMI_DI_ConfigMenu_Button", "CONFIG MENU SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ALM HIST_SW", "g_HMI_DI_AlarmHistory_Button", "ALARM HIST SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP11_SW", "SP11_SW", "SPARE11 SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP12_SW", "g_HMI_DI_HornOff_Button", "SPARE12 SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("IDLE CTR_SW", "g_HMI_DI_IdleContrl_Button", "IDLE CONTROL SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("IDLE_SW", "g_HMI_DI_Idle_Button", "IDLE SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("N/A_SW", "g_HMI_DI_DyRPM_EngTA_Button", "N/A SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("T/A_SW", "g_HMI_DI_DyTo_EngTA_Button", "T/A SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("N/T_SW", "g_HMI_DI_DyRPM_EngTo_Button", "N/T SWITCH", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("T/N_SW", "g_HMI_DI_DyTo_EngRPM_Button", "T/N SWITCH", sysVarInfo.HMI_IOVariables.Count));

            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("OPEN_LOOP_LP", "g_HMI_DO_DynoService_Lamp", "OPEN LOOP TEST LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ENG_MON_LP", "g_HMI_DO_Monitor_Lamp", "ENG MONITOR LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("MISC_LP", "g_HMI_DO_MiscOn_Lamp", "MISC ON/OFF  LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("MAN_LP", "g_HMI_DO_Manual_Lamp", "MANUAL LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("PANEL_LP", "g_HMI_DO_PanelActive_Lamp", "PANEL ENABLE  LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("AUTO_LP", "g_HMI_DO_Automatic_Lamp", "AUTO MATIC LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("REM_LP", "SP1_LP", "REMOTE LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP2_LP", "g_HMI_DO_Remote_Lamp", "SPARE2 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("PREHEAT_LP", "g_HMI_DO_EngPreheatOn_Lamp", "PREHEAT LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("IG_LP", "g_HMI_DO_EngIGOn_Lamp", "IG ON/OFF LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ENG_STOP_LP", "g_HMI_DO_Stop_Lamp", "ENG STOP LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ENG_START_LP", "g_HMI_DO_EngStarting_Lamp", "ENG START LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP3_LP", "SP3_LP", "SPARE3 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP4_LP", "SP4_LP", "SPARE4 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP5_LP", "SP5_LP", "SPARE5 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP6_LP", "SP6_LP", "SPARE6 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP7_LP", "SP7_LP", "READY", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP8_LP", "SP8_LP", "SPARE8 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP9_LP", "SP9_LP", "SPARE9 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP10_LP", "SP10_LP", "SPARE10 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("DYNO_LP", "g_HMI_DO_DynoOn_Lamp", "DYNO ON/OFF LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ALM_RESET_LP", "g_HMI_DO_AlarmReset_Lamp", "ALARM RESET LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("CFG_MENU_LP", "g_HMI_DO_ConfigMenu_Lamp", "CONFIG MENU LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("ALM HIST_LP", "g_HMI_DO_AlarmHistory_Lamp", "ALARM HIST LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP11_LP", "SP11_LP", "SPARE11 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("SP12_LP", "g_HMI_DO_HornOff_Lamp", "SPARE12 LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("IDLE_CTR_LP", "g_HMI_DO_IdleControl_Lamp", "IDLE CONTROL LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("IDLE_LP", "g_HMI_DO_Idle_Lamp", "IDLE LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("N/A_LP", "g_HMI_DO_DyRPM_EngTA_Lamp", "N/A LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("T/A_LP", "g_HMI_DO_DyTo_EngTA_Lamp", "T/A LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("N/T_LP", "g_HMI_DO_DyRPM_EngTo_Lamp", "N/T LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("T/N_LP", "g_HMI_DO_DyTo_EngRPM_Lamp", "T/N LAMP", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("KNOB_DYNO_Counter", "g_HMI_AI_Dyno_CalRefCnt", "KNOB DYNO A", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("KNOB_ENGINE_Counter", "g_HMI_AI_Eng_CalRefCnt", "KNOB ENGINE A", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("KNOB_DYNO_Period", "KNOB_DYNO_Period", "KNOB DYNO B", sysVarInfo.HMI_IOVariables.Count));
            sysVarInfo.HMI_IOVariables.Add(new VariableInfo("KNOB_ENGINE_Period", "KNOB_ENGINE_Period", "KNOB ENGINE B", sysVarInfo.HMI_IOVariables.Count));
        }

        public static void Init_DI_Variables()
        {
            sysVarInfo.DI_Variables.Add(new VariableInfo("RMT_DY_F", "g_DI_DynoTroubleSignal", "DYNO TROUBLE", sysVarInfo.DI_Variables.Count));
            sysVarInfo.DI_Variables.Add(new VariableInfo("RMT_ON", "g_DI_RemoteOnSignal", "REMOTE ON from BK DAS", sysVarInfo.DI_Variables.Count));
            sysVarInfo.DI_Variables.Add(new VariableInfo("RMT_DY_SCS", "g_DI_DynoSpeedModeSignal", "DY SPEED MODE from BK DAS", sysVarInfo.DI_Variables.Count));
            sysVarInfo.DI_Variables.Add(new VariableInfo("RMT_DY_TC", "g_DI_DynoTorqueModeSignal", "DY TORQUE MODE from BK DAS", sysVarInfo.DI_Variables.Count));
            sysVarInfo.DI_Variables.Add(new VariableInfo("RMT_ENG_AC", "g_DI_EngineAlphaModeSignal", "ENG ALPHA MODE from BK DAS", sysVarInfo.DI_Variables.Count));
            sysVarInfo.DI_Variables.Add(new VariableInfo("RMT_ENG_TC", "g_DI_EngineTorqueModeSignal", "ENG TORQUE MODE from BK DAS", sysVarInfo.DI_Variables.Count));
            sysVarInfo.DI_Variables.Add(new VariableInfo("OP_EMO", "g_DI_EMSButton", "OP PANNEL EMERGENCY STOP", sysVarInfo.DI_Variables.Count));
            sysVarInfo.DI_Variables.Add(new VariableInfo("SG_SW", "g_DI_SafetyGuardSignal", "SHAFT GUARD SWITCH", sysVarInfo.DI_Variables.Count));
            sysVarInfo.DI_Variables.Add(new VariableInfo("S_REL", "g_DI_SafetyRelaySignal", "SAFETY RELAY", sysVarInfo.DI_Variables.Count));
        }

        public static void Init_DO_Variables()
        {
            sysVarInfo.DO_Variables.Add(new VariableInfo("DO_READY", "g_DO_BKReadySignal", "READY for BK", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("DO_CTL_ON", "g_DO_DynoOnSignal", "DYNO CONTROL ON", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("DO_TS_VALVE1", "DO_TS_VALVE1", "Thermal shock Valve 1", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("DO_TS_VALVE2", "DO_TS_VALVE2", "Thermal shock Valve 2", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("DO_TS_HEATER", "DO_TS_HEATER", "Thermal shock Heater On", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("DO_RESET", "g_DO_DynoResetSignal", "DYNO RESET", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("DO_IDLE_SW", "g_DO_ThrottleIVSSignal", "ELECTRONIC THROTTLE IVS", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("DO_PREHEAT", "g_DO_PreHeatSignal", "PREHEAT", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("START", "g_DO_StartSignal", "START", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("IG", "g_DO_IgnitionSignal", "IGNITION", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("HORN", "g_DO_HornSignal", "HORN", sysVarInfo.DO_Variables.Count));
            sysVarInfo.DO_Variables.Add(new VariableInfo("ALARM_LAMP", "g_DO_Alarm_Lamp", "ALARM LAMP", sysVarInfo.DO_Variables.Count));
        }

        public static void Init_AI_Variables()
        {
            VariableInfo newVI = null;
            ValuePair vpValue = null;
            newVI = new VariableInfo("DY_SV_RMT", "g_AI_Target_Dyno_Remote", "DYNO SET VALUE_REMOTE", sysVarInfo.AI_Variables.Count);
            sysVarInfo.AI_Variables.Add(newVI);
            vpValue = new ValuePair(32767, 1);
            newVI.CalibrationTable.Add(vpValue);
            newVI.Unit = "-";

            newVI = new VariableInfo("ENG_SV_RMT", "g_AI_Target_Engine_Remote", "ENG SET VALUE REMOTE", sysVarInfo.AI_Variables.Count);
            sysVarInfo.AI_Variables.Add(newVI);
            vpValue = new ValuePair(32767, 1);
            newVI.CalibrationTable.Add(vpValue);
            newVI.Unit = "-";

            newVI = new VariableInfo("THROTTLE_FB", "g_AI_Feedback_Alpha", "THOTTLE POSITION FEEDBACK", sysVarInfo.AI_Variables.Count);
            sysVarInfo.AI_Variables.Add(newVI);
            vpValue = new ValuePair(32767, 1);
            newVI.CalibrationTable.Add(vpValue);
            newVI.Unit = "%";

            newVI = new VariableInfo("DY_SPEED_FB", "g_AI_Feedback_DynoSpeed", "DYNO SPEED ACTUAL_FEEDBACK", sysVarInfo.AI_Variables.Count);
            sysVarInfo.AI_Variables.Add(newVI);
            vpValue = new ValuePair(100, 1);
            newVI.CalibrationTable.Add(vpValue);
            newVI.Unit = "RPM";

            newVI = new VariableInfo("LC_VDIFF", "g_AI_Feedback_Vdiff", "TORQUE FEEDBACK", sysVarInfo.AI_Variables.Count);
            vpValue = new ValuePair(2147483647, 20);
            newVI.CalibrationTable.Add(vpValue);
            newVI.Unit = "mV";
            sysVarInfo.AI_Variables.Add(newVI);
            newVI = new VariableInfo("LC_VREF", "g_AI_Feedback_Vref", "TORQUE FEEDBACK", sysVarInfo.AI_Variables.Count);
            vpValue = new ValuePair(2147483647, 12);
            newVI.Unit = "V";
            newVI.CalibrationTable.Add(vpValue);
            sysVarInfo.AI_Variables.Add(newVI);
        }

        public static void Init_AO_Variables()
        {
            sysVarInfo.AO_Variables.Add(new VariableInfo("DY_SPEED_ACT", "g_AO_DynoSpeedActualSignal", "DYNO SPEED ACTUAL_OUTPUT", sysVarInfo.AO_Variables.Count));
            sysVarInfo.AO_Variables.Add(new VariableInfo("TORQUE_ACT", "g_AO_DynoTorqueActualSignal", "TORQUE ACTUAL_OUTPUT", sysVarInfo.AO_Variables.Count));
            sysVarInfo.AO_Variables.Add(new VariableInfo("DYNO_CTL", "g_AO_DynoControlSignal", "DYNO CONTROL VALE", sysVarInfo.AO_Variables.Count));
            sysVarInfo.AO_Variables.Add(new VariableInfo("CWTEMP_SV", "g_AO_CWTEMP_SV", "WTC TEMPERATURE SET VALUE", sysVarInfo.AO_Variables.Count));
            sysVarInfo.AO_Variables.Add(new VariableInfo("THROTTLE_ACT", "g_AO_Feedback_Alpha", "THROTTLE POSITION ACTUAL", sysVarInfo.AO_Variables.Count));
            sysVarInfo.AO_Variables.Add(new VariableInfo("THROTTLE_SV", "g_AO_ThrottleControlSignal", "THROTTLE CONTROL VALUE", sysVarInfo.AO_Variables.Count));
            sysVarInfo.AO_Variables.Add(new VariableInfo("THROTTLE_EL1", "g_AO_AlphaASignal", "ELECTRIC THROTTLE A", sysVarInfo.AO_Variables.Count));
            sysVarInfo.AO_Variables.Add(new VariableInfo("THROTTLE_EL2", "g_AO_AlphaBSignal", "ELECTRIC THROTTLE B", sysVarInfo.AO_Variables.Count));
        }

        public static void Init_Calc_Variables()
        {
            VariableInfo newVI = (new VariableInfo("TORQUE_FB", "g_AI_Feedback_Torque", "TORQUE FEEDBACK", sysVarInfo.Calc_Variables.Count));
            ValuePair vpValue = new ValuePair(98, 1);
            newVI.CalibrationTable.Add(vpValue);
            newVI.Unit = "Nm";
            newVI.Formula = "(g_AI_Feedback_Vdiff / g_AI_Feedback_Vref * 967 / 2 - 58.4)";
            newVI.Avaraging = 1;
            newVI.AvaragingCount = 100;
            sysVarInfo.Calc_Variables.Add(newVI);
        }
    }

}
