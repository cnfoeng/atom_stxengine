﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using System.Xml;
using OfficeOpenXml.Style;
using System.IO;

namespace AtomUI.Class
{
    class Excel
    {
        public void ExcelCalc()
        {
            FileInfo newFile = new FileInfo("Report.xlsx");

            using (ExcelPackage package = new ExcelPackage(newFile))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Report");
                //Add the headers
                //worksheet.Cells[1, 1].Value = "PEC - Raynar. Inc.,Ltd";


                //worksheet.Cells[1, 1].Value = "[General info]";
                //worksheet.Cells[2, 1].Value = "Project Name";
                //worksheet.Cells[3, 1].Value = "Examination Date";
                //worksheet.Cells[4, 1].Value = "Inspection Company";
                //worksheet.Cells[5, 1].Value = "Inspection Site";
                //worksheet.Cells[6, 1].Value = "Procedure Number";
                //worksheet.Cells[7, 1].Value = "Inspector Name/Level";
                //worksheet.Cells[8, 1].Value = "Remarks";
                //worksheet.Cells[9, 1].Value = "Point X Count";
                //worksheet.Cells[10, 1].Value = "Point Y Count";
                worksheet.Cells[4, 1].Formula = "sin(0.5+0.7)";
                package.Workbook.Calculate();
                object result = worksheet.Cells[5, 1].Value;
                package.Save();

            }
        }
    }
}
