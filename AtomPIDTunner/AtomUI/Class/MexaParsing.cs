﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtomUI.Class
{
    public class MexaParsing
    {
        public static bool AddMexaChannels(string FileName)
        {
            try
            {
                acsChannelInfo channelItem;
                VariableInfo varItem;
                acsModuleInfo item = new acsModuleInfo("Line1", "Mexa 9x00", 0, 0, ChannelIO.Input, ChannelType.Analog, 0, 0, 15);
                item.UseFor = ChannelUseFor.MEXA;
                Ethercat.sysInfo.MEXAModuleInfos.Add(item);

                string[] MexaAllValue = System.IO.File.ReadAllLines(FileName);
                foreach (string MexaChannel in MexaAllValue)
                {
                    string[] ChannelData = MexaChannel.Split(',');
                    channelItem = new acsChannelInfo(item, 0, 0, ChannelData[0], 0);
                    Ethercat.chInfo.MEXA9000_Channels.Add(channelItem);
                    varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
                    varItem.Unit = ChannelData[1];
                    DaqData.sysVarInfo.MEXA9000_Variables.Add(varItem);
                }

                return true;
            } catch(Exception ex)
            {
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n AddMexaChannels Error!!");
                return false;
            }
        }

        public static bool AddMexaChannels()
        {
            acsChannelInfo channelItem;
            VariableInfo varItem;
            acsModuleInfo item = new acsModuleInfo("Line1", "Mexa 9x00", 0,0,ChannelIO.Input, ChannelType.Analog, 0,0,15);
            item.UseFor = ChannelUseFor.MEXA;
            Ethercat.sysInfo.MEXAModuleInfos.Add(item);

            channelItem = new acsChannelInfo(item, 0, 0, "CO-L", 0);
            Ethercat.chInfo.MEXA9000_Channels.Add(channelItem);
            varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
            varItem.Unit = "ppm";
            DaqData.sysVarInfo.MEXA9000_Variables.Add(varItem);

            channelItem = new acsChannelInfo(item, 0, 1, "CO-H", 0);
            Ethercat.chInfo.MEXA9000_Channels.Add(channelItem);
            varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
            varItem.Unit = "vol%";
            DaqData.sysVarInfo.MEXA9000_Variables.Add(varItem);

            channelItem = new acsChannelInfo(item, 0, 2, "CO2", 0);
            Ethercat.chInfo.MEXA9000_Channels.Add(channelItem);
            varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
            varItem.Unit = "vol%";
            DaqData.sysVarInfo.MEXA9000_Variables.Add(varItem);

            channelItem = new acsChannelInfo(item, 0, 3, "ECO2", 0);
            Ethercat.chInfo.MEXA9000_Channels.Add(channelItem);
            varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
            varItem.Unit = "ppm";
            DaqData.sysVarInfo.MEXA9000_Variables.Add(varItem);

            channelItem = new acsChannelInfo(item, 0, 4, "THC", 0);
            Ethercat.chInfo.MEXA9000_Channels.Add(channelItem);
            varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
            varItem.Unit = "ppm";
            DaqData.sysVarInfo.MEXA9000_Variables.Add(varItem);

            channelItem = new acsChannelInfo(item, 0, 6, "O2", 0);
            Ethercat.chInfo.MEXA9000_Channels.Add(channelItem);
            varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
            varItem.Unit = "vol%";
            DaqData.sysVarInfo.MEXA9000_Variables.Add(varItem);

            channelItem = new acsChannelInfo(item, 0, 7, "NOX", 0);
            Ethercat.chInfo.MEXA9000_Channels.Add(channelItem);
            varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
            varItem.Unit = "ppm";
            DaqData.sysVarInfo.MEXA9000_Variables.Add(varItem);

            channelItem = new acsChannelInfo(item, 0, 8, "CO-L(R)", 0);
            Ethercat.chInfo.MEXA9000_Channels.Add(channelItem);
            varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
            varItem.Unit = "ppm";
            DaqData.sysVarInfo.MEXA9000_Variables.Add(varItem);

            channelItem = new acsChannelInfo(item, 0, 9, "CO-H(R)", 0);
            Ethercat.chInfo.MEXA9000_Channels.Add(channelItem);
            varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
            varItem.Unit = "ppm";
            DaqData.sysVarInfo.MEXA9000_Variables.Add(varItem);

            channelItem = new acsChannelInfo(item, 0, 10, "CO2(R)", 0);
            Ethercat.chInfo.MEXA9000_Channels.Add(channelItem);
            varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
            varItem.Unit = "vol%";
            DaqData.sysVarInfo.MEXA9000_Variables.Add(varItem);

            channelItem = new acsChannelInfo(item, 0, 11, "THC(R)", 0);
            Ethercat.chInfo.MEXA9000_Channels.Add(channelItem);
            varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
            varItem.Unit = "ppm";
            DaqData.sysVarInfo.MEXA9000_Variables.Add(varItem);

            channelItem = new acsChannelInfo(item, 0, 11, "O2(R)", 0);
            Ethercat.chInfo.MEXA9000_Channels.Add(channelItem);
            varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
            varItem.Unit = "ppm";
            DaqData.sysVarInfo.MEXA9000_Variables.Add(varItem);

            channelItem = new acsChannelInfo(item, 0, 12, "NO(R)", 0);
            Ethercat.chInfo.MEXA9000_Channels.Add(channelItem);
            varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
            varItem.Unit = "ppm";
            DaqData.sysVarInfo.MEXA9000_Variables.Add(varItem);

            channelItem = new acsChannelInfo(item, 0, 13, "A/F", 0);
            Ethercat.chInfo.MEXA9000_Channels.Add(channelItem);
            varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
            varItem.Unit = "-";
            DaqData.sysVarInfo.MEXA9000_Variables.Add(varItem);

            channelItem = new acsChannelInfo(item, 0, 14, "EGR", 0);
            Ethercat.chInfo.MEXA9000_Channels.Add(channelItem);
            varItem = new VariableInfo(channelItem, channelItem.VariableName, channelItem.VariableName);
            varItem.Unit = "%";
            DaqData.sysVarInfo.MEXA9000_Variables.Add(varItem);

            return true;
        }
    }
}
