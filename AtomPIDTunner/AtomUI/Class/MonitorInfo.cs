﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;
using System.ComponentModel;

namespace AtomUI.Class
{
    [Serializable]
    public class MonitorInfo
    {
        public static MonitorItemList MonitorList = new MonitorItemList();
        public static MonitorItemList MonitorDefine = new MonitorItemList(true);

        public static void SaveMonInfo(string FileName)
        {
            FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write);
            XmlSerializer xs = new XmlSerializer(MonitorList.GetType());
            xs.Serialize(fs, MonitorList);
            fs.Close();
        }

        public static void LoadMonInfo(string FileName)
        {
            FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
            XmlSerializer xs = new XmlSerializer(MonitorList.GetType());
            Object dd = xs.Deserialize(fs);
            MonitorList = (MonitorItemList)dd;
            fs.Close();
        }
    }


    [Serializable]
    public class MonitorItemList
    {
        public List<MonitorItem> Monitor = new List<MonitorItem>();

        public MonitorItemList()
        { }
        public MonitorItemList(bool Initialize)
        {
            if (Initialize)
            {
                Init();
            }
        }

        public void Init()
        {
            MonitorItem MI = new MonitorItem();
            MI.monitorType = NextUI.BaseUI.NextUIType.Speedo;
            Monitor.Add(MI);

            MI = new MonitorItem();
            MI.monitorType = NextUI.BaseUI.NextUIType.HalfSpeedo;
            Monitor.Add(MI);

            MI = new MonitorItem();
            MI.monitorType = NextUI.BaseUI.NextUIType.Graph;
            Monitor.Add(MI);

            MI = new MonitorItem();
            MI.monitorType = NextUI.BaseUI.NextUIType.Digital;
            Monitor.Add(MI);
        }
    }

    [Serializable]
    [DefaultPropertyAttribute("Name")]
    public class MonitorItem
    {
        private NextUI.BaseUI.NextUIType _monitorType;
        private int _Left;
        private int _Top;
        private int _Width;
        private int _Height;
        private string _Unit;
        private double _Warning_Low;
        private double _Warning_High;
        private double _Alarm_Low;
        private double _Alarm_High;
        private string _Formula;
        private string _name;

        public List<VariableInfo> VARs;

        public MonitorItem()
        { }

        [CategoryAttribute("Monitor Setup"), DescriptionAttribute("Type of the Monitor Item")]
        public NextUI.BaseUI.NextUIType monitorType
        {
            get
            {
                return _monitorType;
            }
            set
            {
                _monitorType = value;
            }
        }

        [CategoryAttribute("Behavior"), DescriptionAttribute("Units of the Monitor Variable")]
        public string Unit
        {
            get
            {
                return _Unit;
            }
            set
            {
                _Unit = value;
            }
        }

        [CategoryAttribute("Behavior"), DescriptionAttribute("Warning Limits of the Monitor Variable")]
        public double Warning_Low
        {
            get
            {
                return _Warning_Low;
            }
            set
            {
                _Warning_Low = value;
            }
        }

        [CategoryAttribute("Behavior"), DescriptionAttribute("Alarm Limits of the Monitor Variable")]
        public double Alarm_Low
        {
            get
            {
                return _Alarm_Low;
            }
            set
            {
                _Alarm_Low = value;
            }
        }

        [CategoryAttribute("Behavior"), DescriptionAttribute("Warning Limits of the Monitor Variable")]
        public double Warning_High
        {
            get
            {
                return _Warning_High;
            }
            set
            {
                _Warning_High = value;
            }
        }

        [CategoryAttribute("Behavior"), DescriptionAttribute("Alarm Limits of the Monitor Variable")]
        public double Alarm_High
        {
            get
            {
                return _Alarm_High;
            }
            set
            {
                _Alarm_High = value;
            }
        }

        [CategoryAttribute("Behavior"), DescriptionAttribute("Formula of the Monitor Variable")]
        public string Formula
        {
            get
            {
                return _Formula;
            }
            set
            {
                _Formula = value;
            }
        }

        [CategoryAttribute("Monitor Setup"), DescriptionAttribute("Name of the Monitor Item")]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
        [CategoryAttribute("Appearance"), DescriptionAttribute("Left position of Control")]
        public int Left
        {
            get
            {
                return _Left;
            }
            set
            {
                _Left = value;
            }
        }
        [CategoryAttribute("Appearance"), DescriptionAttribute("Top position of Control")]
        public int Top
        {
            get
            {
                return _Top;
            }
            set
            {
                _Top = value;
            }
        }
        [CategoryAttribute("Appearance"), DescriptionAttribute("Width of Control")]
        public int Width
        {
            get
            {
                return _Width;
            }
            set
            {
                _Width = value;
            }
        }
        [CategoryAttribute("Appearance"), DescriptionAttribute("Height of Control")]
        public int Height
        {
            get
            {
                return _Height;
            }
            set
            {
                _Height = value;
            }
        }

    }
}
