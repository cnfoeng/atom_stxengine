﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AtomUI.Class;
using System.Data;
using System.Xml.Serialization;
using System.IO;

namespace AtomUI.Class
{
    public enum DynoMode
    {
        Speed_Mode = 0,
        Torque_Mode = 1
    }
    public enum EngineMode
    {
        Alpha_Mode = 0,
        Torque_Mode = 1
    }

    [Serializable]
    class StepMode
    {
        public static StepModeDefine StepModeData = new StepModeDefine();

        public static void Save(string FileName)
        {
            FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write);
            XmlSerializer xs = new XmlSerializer(StepModeData.Data.GetType());
            xs.Serialize(fs, StepModeData.Data);
            fs.Close();
        }

        public static void Load(string FileName)
        {
            FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
            XmlSerializer xs = new XmlSerializer(StepModeData.Data.GetType());
            Object dd = xs.Deserialize(fs);
            fs.Close();
            StepModeData.Data = (DataSet)dd;
        }
    }

    [Serializable]
    public class StepModeDefine
    {
        public DataSet Data = new DataSet();

        public StepModeDefine()
        {
            MakeDataSet();
        }

        public void MakeDataSet()
        {
            DataTable dt = new DataTable();
            dt.TableName = "StepMode";

            DataColumn dc = dt.Columns.Add("STEP");
            dc.DataType = Type.GetType("System.Int32");

            dc = dt.Columns.Add("Dyno");
            dc.DataType = Type.GetType("System.Double");

            dc = dt.Columns.Add("D_Mode");
            dc.DataType = Type.GetType("System.String");

            dc = dt.Columns.Add("D_Ramp");
            dc.DataType = Type.GetType("System.Int32");

            dc = dt.Columns.Add("Engine");
            dc.DataType = Type.GetType("System.Double");

            dc = dt.Columns.Add("E_Mode");
            dc.DataType = Type.GetType("System.String");

            dc = dt.Columns.Add("E_Ramp");
            dc.DataType = Type.GetType("System.Int32");

            dc = dt.Columns.Add("Settling");
            dc.DataType = Type.GetType("System.Int32");

            dc = dt.Columns.Add("MeasuringNo");
            dc.DataType = Type.GetType("System.Int32");

            dc = dt.Columns.Add("Duration");
            dc.DataType = Type.GetType("System.Int32");

            dc = dt.Columns.Add("Period");
            dc.DataType = Type.GetType("System.Int32");

            dc = dt.Columns.Add("RestTime");
            dc.DataType = Type.GetType("System.Int32");

            dc = dt.Columns.Add("TotalTime");
            dc.DataType = Type.GetType("System.Int32");

            dc = dt.Columns.Add("ThermalShock");
            dc.DataType = Type.GetType("System.Boolean");

            dc = dt.Columns.Add("Heater");
            dc.DataType = Type.GetType("System.Boolean");

            dc = dt.Columns.Add("AlarmTable");
            dc.DataType = Type.GetType("System.String");

            dc = dt.Columns.Add("Goto");
            dc.DataType = Type.GetType("System.String");

            Data.Tables.Add(dt);
        }
    }

}
