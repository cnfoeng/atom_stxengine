﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace AtomUI.Class
{
    [Serializable]
    public class SysChannelInfo
    {
        public List<acsChannelInfo> HMI_Channels = new List<acsChannelInfo>();
        public List<acsChannelInfo> DI_Channels = new List<acsChannelInfo>();
        public List<acsChannelInfo> AI_Channels = new List<acsChannelInfo>();
        public List<acsChannelInfo> DO_Channels = new List<acsChannelInfo>();
        public List<acsChannelInfo> AO_Channels = new List<acsChannelInfo>();
        public List<acsChannelInfo> MEXA9000_Channels = new List<acsChannelInfo>();
        public List<acsChannelInfo> ECU_Channels = new List<acsChannelInfo>();
        public List<acsChannelInfo> ModBus_Channels = new List<acsChannelInfo>();

        //public static Dictionary<int, acsModuleInfo> HMIModuleInfos = new Dictionary<int, acsModuleInfo>();
        //public static Dictionary<int, acsModuleInfo> ModuleInfos = new Dictionary<int, acsModuleInfo>();
        //public static Dictionary<int, acsChannelInfo> HMI_Channels = new Dictionary<int, acsChannelInfo>();
        //public static Dictionary<int, acsChannelInfo> DI_Channels = new Dictionary<int, acsChannelInfo>();
        //public static Dictionary<int, acsChannelInfo> AI_Channels = new Dictionary<int, acsChannelInfo>();
        //public static Dictionary<int, acsChannelInfo> DO_Channels = new Dictionary<int, acsChannelInfo>();
        //public static Dictionary<int, acsChannelInfo> AO_Channels = new Dictionary<int, acsChannelInfo>();

        public SysChannelInfo()
        { }

        public void ClearLists()
        {
            HMI_Channels.Clear();
            DI_Channels.Clear();
            AI_Channels.Clear();
            DO_Channels.Clear();
            AO_Channels.Clear();
            MEXA9000_Channels.Clear();
            ECU_Channels.Clear();
            ModBus_Channels.Clear();
        }

        public void Save(string FileName)
        {
            FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write);
            XmlSerializer xs = new XmlSerializer(this.GetType());
            xs.Serialize(fs, this);
            fs.Close();
        }

        public void Load(string FileName)
        {
            this.ClearLists();
            FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
            XmlSerializer xs = new XmlSerializer(this.GetType());
            Object dd = xs.Deserialize(fs);
            fs.Close();
            SysChannelInfo chinfo = new SysChannelInfo();
            chinfo = (SysChannelInfo)dd;
            this.HMI_Channels = chinfo.HMI_Channels;
            this.DI_Channels = chinfo.DI_Channels;
            this.AI_Channels = chinfo.AI_Channels;
            this.DO_Channels = chinfo.DO_Channels;
            this.AO_Channels = chinfo.AO_Channels;
            this.MEXA9000_Channels = chinfo.MEXA9000_Channels;
            this.ECU_Channels = chinfo.ECU_Channels;
            this.ModBus_Channels = chinfo.ModBus_Channels;
            chinfo = null;
        }
    }

    [Serializable]
    public class acsChannelInfo
    {
        public acsModuleInfo ModuleInfo;
        public int ChannelBit;
        public int ChannelIndex;
        public int Offset;
        public int ValueType = 1;
        public string VariableName;

        public acsChannelInfo()
        { }

        public acsChannelInfo(acsModuleInfo ModuleInfo, int ChannelBit, int ChannelIndex, string VariableName, int Offset)
        {
            this.ModuleInfo = ModuleInfo;
            this.ChannelBit = ChannelBit;
            this.ChannelIndex = ChannelIndex;
            this.VariableName = VariableName;
            this.Offset = Offset;
        }
    }
}
