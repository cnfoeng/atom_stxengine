﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtomUI.Class
{
    public enum ControllerPowerStatus
    {
        PowerOn = 0,
        Ready = 1,
        ControlOn = 2
    }

    public enum ControllerErrorStatus
    {
        Normal = 0,
        Fault = 1
    }

    public enum ControllerBufferStatus
    {
        Init = 0,
        Idle = 1,
        Run = 2,
        Pause = 3,
        Down = 4
    }

    public enum ControlModeStatus
    {
        Monitor = 0,
        DynoService = 1,
        Manual = 2,
        Automatic = 3,
        Remote = 4
    }

    public enum PIDModeStatus
    {
        Default = 0,
        Idle = 1,
        IdleControl = 2,
        TorqueAlpha = 3,
        SpeedAlpha = 4,
        TorqueSpeed = 5,
        SpeedTorque = 6
    }

    [Serializable]
    public class SysStatus
    {
        public ControllerPowerStatus Controller_Power_Status = ControllerPowerStatus.PowerOn;

        public ControllerErrorStatus Controller_ErrorStasus = ControllerErrorStatus.Normal;
        public int AlarmExists = 0;
        public int ErrorExists = 0;
        public int ErrorID = 0;
        public int WarningExists = 0;
        public int AlarmID = 0;

        public ControllerBufferStatus Controller_Buffer_Status = ControllerBufferStatus.Init;
        public ControlModeStatus ControlMode_Status = ControlModeStatus.Monitor;
        public PIDModeStatus PIDMode_Status = PIDModeStatus.Default;

        public List<SysStatusVariable> Status = new List<SysStatusVariable>();

        public SysStatus()
        {
            Init();
        }

        public void ParseStatus()
        {
            if (GetValue("ConotrollerPowerOn") != 0)
            {
                Controller_Power_Status = ControllerPowerStatus.PowerOn;
            }
            if (GetValue("ConotrollerReady") != 0)
            {
                Controller_Power_Status = ControllerPowerStatus.Ready;
            }
            if (GetValue("ConotrolOn") != 0)
            {
                Controller_Power_Status = ControllerPowerStatus.ControlOn;
            }

            if (GetValue("ConotrollerNormal") != 0)
            {
                Controller_ErrorStasus = ControllerErrorStatus.Normal;
            }

            if (GetValue("ConotrollerFault") != 0)
            {
                Controller_ErrorStasus = ControllerErrorStatus.Fault;
            }

            AlarmExists = GetValue("AlarmExists");
            ErrorExists = GetValue("ErrorExists");
            ErrorID = GetValue("ErrorID");
            AlarmID = GetValue("AlarmID");
            WarningExists = GetValue("WaringExists");

            if (GetValue("BufferInit") != 0)
            {
                Controller_Buffer_Status = ControllerBufferStatus.Init;
            }
            if (GetValue("BufferIdle") != 0)
            {
                Controller_Buffer_Status = ControllerBufferStatus.Idle;
            }
            if (GetValue("BufferRun") != 0)
            {
                Controller_Buffer_Status = ControllerBufferStatus.Run;
            }
            if (GetValue("BufferPause") != 0)
            {
                Controller_Buffer_Status = ControllerBufferStatus.Pause;
            }
            if (GetValue("BufferDown") != 0)
            {
                Controller_Buffer_Status = ControllerBufferStatus.Down;
            }
            if (GetValue("Monitor") != 0)
            {
                ControlMode_Status = ControlModeStatus.Monitor;
            }
            if (GetValue("DynoService") != 0)
            {
                ControlMode_Status = ControlModeStatus.DynoService;
            }
            if (GetValue("Manual") != 0)
            {
                ControlMode_Status = ControlModeStatus.Manual;
            }
            if (GetValue("Automatic") != 0)
            {
                ControlMode_Status = ControlModeStatus.Automatic;
            }
            if (GetValue("Remote") != 0)
            {
                ControlMode_Status = ControlModeStatus.Remote;
            }

            PIDMode_Status = PIDModeStatus.Default;
            if (GetValue("Idle") != 0)
            {
                PIDMode_Status = PIDModeStatus.Idle;
            }
            if (GetValue("IdleControl") != 0)
            {
                PIDMode_Status = PIDModeStatus.IdleControl;
            }
            if (GetValue("TorqueAlpha") != 0)
            {
                PIDMode_Status = PIDModeStatus.TorqueAlpha;
            }
            if (GetValue("SpeedAlpha") != 0)
            {
                PIDMode_Status = PIDModeStatus.SpeedAlpha;
            }
            if (GetValue("TorqueSpeed") != 0)
            {
                PIDMode_Status = PIDModeStatus.TorqueSpeed;
            }
            if (GetValue("SpeedTorque") != 0)
            {
                PIDMode_Status = PIDModeStatus.SpeedTorque;
            }
        }

        public int GetValue(string VarName)
        {
            int i = 0;

            foreach(SysStatusVariable sys in Status)
            {
                if (sys.Name == VarName)
                    return sys.Value;
            }
            return i;
        }

        public void Init()
        {
            Status.Add(new SysStatusVariable("Power-up Elapsed Time", 0x00));
            Status.Add(new SysStatusVariable("ConotrollerPowerOn", 0x02));
            Status.Add(new SysStatusVariable("ConotrollerReady", 0x03));
            Status.Add(new SysStatusVariable("ConotrolOn", 0x04));
            Status.Add(new SysStatusVariable("ConotrollerNormal", 0x06));
            Status.Add(new SysStatusVariable("ConotrollerFault", 0x07));
            Status.Add(new SysStatusVariable("BufferInit", 0x09));
            Status.Add(new SysStatusVariable("BufferIdle", 0x0A));
            Status.Add(new SysStatusVariable("BufferRun", 0x0B));
            Status.Add(new SysStatusVariable("BufferPause", 0x0C));
            Status.Add(new SysStatusVariable("BufferDown", 0x0D));
            Status.Add(new SysStatusVariable("AlarmExists", 0x10));
            Status.Add(new SysStatusVariable("ErrorExists", 0x11));
            Status.Add(new SysStatusVariable("ErrorID", 0x12));
            Status.Add(new SysStatusVariable("WarningExists", 0x13));
            Status.Add(new SysStatusVariable("AlarmID", 0x14));
            Status.Add(new SysStatusVariable("Monitor", 0x30));
            Status.Add(new SysStatusVariable("DynoService", 0x31));
            Status.Add(new SysStatusVariable("Manual", 0x32));
            Status.Add(new SysStatusVariable("Automatic", 0x33));
            Status.Add(new SysStatusVariable("Remote", 0x34));
            Status.Add(new SysStatusVariable("Idle", 0x50));
            Status.Add(new SysStatusVariable("IdleControl", 0x51));
            Status.Add(new SysStatusVariable("TorqueAlpha", 0x52));
            Status.Add(new SysStatusVariable("SpeedAlpha", 0x53));
            Status.Add(new SysStatusVariable("TorqueSpeed", 0x54));
            Status.Add(new SysStatusVariable("SpeedTorque", 0x55));

            Status.Add(new SysStatusVariable("FeedbackSpeed", 0xD0));
            Status.Add(new SysStatusVariable("FeedbackAlpha", 0xD1));
            Status.Add(new SysStatusVariable("FeedbackTorque", 0xD2));
            Status.Add(new SysStatusVariable("FeedbackPower", 0xD3));
            Status.Add(new SysStatusVariable("FeedbackCurrent", 0xD4));
            Status.Add(new SysStatusVariable("DemandSpeed", 0xE0));
            Status.Add(new SysStatusVariable("DemandAlpha", 0xE1));
            Status.Add(new SysStatusVariable("DemandTorque", 0xE2));
            Status.Add(new SysStatusVariable("DemandCurrent", 0xE4));
            Status.Add(new SysStatusVariable("TargetDynoSpeed", 0x100));
            Status.Add(new SysStatusVariable("TargetDynoTorque", 0x101));
            Status.Add(new SysStatusVariable("TargetEngSpeed", 0x102));
            Status.Add(new SysStatusVariable("TargetEngTorque", 0x103));
            Status.Add(new SysStatusVariable("TargetEngAlpha", 0x104));
        }
    }

    [Serializable]
    public class SysStatusVariable
    {
        public string Name;
        public int Address;
        public int Value;

        public SysStatusVariable()
        { }

        public SysStatusVariable(string Name, int Address)
        {
            this.Name = Name;
            this.Address = Address;
        }
    }
}
