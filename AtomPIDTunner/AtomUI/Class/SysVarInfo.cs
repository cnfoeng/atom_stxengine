﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;

namespace AtomUI.Class
{
    [Serializable]
    public class SysVarInfo
    {
        public List<VariableInfo> FrontPanel_Variables_Output = new List<VariableInfo>();
        public List<VariableInfo> FrontPanel_Variables_Input = new List<VariableInfo>();
        public List<VariableInfo> DataLogging_Variables = new List<VariableInfo>();

        public List<VariableInfo> HMI_IOVariables = new List<VariableInfo>();
        public List<VariableInfo> HMI_Variables = new List<VariableInfo>();

        public List<VariableInfo> DI_Variables = new List<VariableInfo>();
        public List<VariableInfo> AI_Variables = new List<VariableInfo>();
        public List<VariableInfo> DO_Variables = new List<VariableInfo>();
        public List<VariableInfo> AO_Variables = new List<VariableInfo>();
        public List<VariableInfo> MEXA9000_Variables = new List<VariableInfo>();
        public List<VariableInfo> ECU_Variables = new List<VariableInfo>();
        public List<VariableInfo> ModBus_Variables = new List<VariableInfo>();

        public List<VariableInfo> Calc_Variables = new List<VariableInfo>();

        public SysVarInfo()
        { }

        public void ClearLists()
        {
            FrontPanel_Variables_Output.Clear();
            FrontPanel_Variables_Input.Clear();
            DataLogging_Variables.Clear();

            HMI_IOVariables.Clear();
            HMI_Variables.Clear();

            DI_Variables.Clear();
            AI_Variables.Clear();
            DO_Variables.Clear();
            AO_Variables.Clear();
            Calc_Variables.Clear();
        }

        public void Save(string FileName)
        {
            FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write);
            XmlSerializer xs = new XmlSerializer(this.GetType());
            xs.Serialize(fs, this);
            fs.Close();
        }

        public void Load(string FileName)
        {
            FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
            XmlSerializer xs = new XmlSerializer(this.GetType());
            Object dd = xs.Deserialize(fs);
            fs.Close();
            SysVarInfo varinfo = new SysVarInfo();
            varinfo = (SysVarInfo)dd;
            this.FrontPanel_Variables_Output = varinfo.FrontPanel_Variables_Output;
            this.FrontPanel_Variables_Input = varinfo.FrontPanel_Variables_Input;
            this.DataLogging_Variables = varinfo.DataLogging_Variables;

            this.HMI_IOVariables = varinfo.HMI_IOVariables;
            this.HMI_Variables = varinfo.HMI_Variables;

            this.DI_Variables = varinfo.DI_Variables;
            this.AI_Variables = varinfo.AI_Variables;
            this.DO_Variables = varinfo.DO_Variables;
            this.AO_Variables = varinfo.AO_Variables;

            this.MEXA9000_Variables = varinfo.MEXA9000_Variables;
            this.ECU_Variables = varinfo.ECU_Variables;
            this.ModBus_Variables = varinfo.ModBus_Variables;

            this.Calc_Variables = varinfo.Calc_Variables;

            varinfo = null;
        }

        public void SaveVariables(string FileName, List<VariableInfo> Variables)
        {
            FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write);
            XmlSerializer xs = new XmlSerializer(Variables.GetType());
            xs.Serialize(fs, Variables);
            fs.Close();
        }

        public List<VariableInfo> LoadVariables(string FileName)
        {
            List<VariableInfo> Variables = new List<VariableInfo>();
            try
            {
                FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read);
                XmlSerializer xs = new XmlSerializer(this.GetType());
                Object dd = xs.Deserialize(fs);
                fs.Close();
                Variables = (List<VariableInfo>)dd;
            }
            catch {
                MessageBox.Show("File Format Not Matched. Try other file.");
            }
            return Variables;
        }
    }

    [Serializable]
    public class ValuePair
    {
        public int IOValue = 0;
        public double RealValue = 0.0;
        public ValuePair()
        {
        }

        public ValuePair(int IOValue, int RealValue)
        {
            this.IOValue = IOValue;
            this.RealValue = RealValue;
        }
    }

    public enum AlarmReaction
    {
        Warninng = 0,
        Alarm = 1,
        Stop = 2
    }

    [Serializable]
    public class VariableInfo
    {
        public acsChannelInfo ChannelInfo;
        public string VariableName;
        public string NorminalName;
        public string NorminalDescription;
        public int Tag;
        public int Index;
        public bool Removable;
        public string Formula;
        public double IOValue;
        public int IOIndex;
        public int Avaraging;
        public int AvaragingCount;
        public List<ValuePair> CalibrationTable = new List<ValuePair>();
        public double Offset = 0;
        public double Factor = 0;
        public string Unit;
        public double Warning_Low;
        public double Warning_High;
        public double Alarm_Low;
        public double Alarm_High;
        public AlarmReaction Reaction;

        public int CalType
        {
            get
            {
                if (CalibrationTable.Count == 0)
                    return 0;
                else
                    return 1;
            }
        }

        public double RealValue
        {
            get {
                double RValue = 0;
                //if (CalibrationTable.Count == 1)
                //{
                //    RValue = (double)IOValue / (double)CalibrationTable[0].IOValue * (double)CalibrationTable[0].RealValue;
                //}
                //else
                    RValue = (double)IOValue;

                return Math.Round(RValue, 2);
            }
        }

        public VariableInfo Clone()
        {
            VariableInfo var = new VariableInfo();
            var.ChannelInfo = this.ChannelInfo;
            var.VariableName = this.VariableName;
            var.NorminalName = this.NorminalName;
            var.NorminalDescription = this.NorminalDescription;
            var.Tag = this.Tag;
            var.Index = this.Index;
            var.Removable = this.Removable;
            var.Formula = this.Formula;
            var.IOIndex = this.IOIndex;
            var.IOValue = this.IOValue;
            var.CalibrationTable = this.CalibrationTable;
            var.Unit = this.Unit;
            var.Warning_Low = this.Warning_Low;
            var.Warning_High = this.Warning_High;
            var.Alarm_Low = this.Alarm_Low;
            var.Alarm_High = this.Alarm_High;
            var.Reaction = this.Reaction;

            return var;
        }
        public VariableInfo()
        { }

        public VariableInfo(string VariableName, string NorminalName)
        {
            this.NorminalName = NorminalName;
            this.VariableName = VariableName;
        }
        public VariableInfo(string VariableName, string NorminalName, string NorminalDescription, int Tag, int Index)
        {
            this.NorminalName = NorminalName;
            this.NorminalDescription = NorminalDescription;
            this.VariableName = VariableName;
            this.Tag = Tag;
            this.Index = Index;
            this.Removable = false;
        }

        public VariableInfo(string VariableName, string NorminalName, string NorminalDescription, int Index)
        {
            this.NorminalName = NorminalName;
            this.NorminalDescription = NorminalDescription;
            this.VariableName = VariableName;
            this.Index = Index;
            this.Removable = false;
        }

        public VariableInfo(acsChannelInfo ChannelInfo, string VariableName, string NorminalName)
        {
            this.ChannelInfo = ChannelInfo;
            this.NorminalName = NorminalName;
            this.VariableName = VariableName;
            this.Removable = false;
        }

        public VariableInfo(string VariableName)
        {
            this.VariableName = VariableName;
            this.Removable = true;
        }
    }

}
