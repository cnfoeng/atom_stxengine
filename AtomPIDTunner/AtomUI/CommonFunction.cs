﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace AtomUI
{
    public enum LogType { Log, Data, Vib, Error };

    public class CommonFunction
    {
        static string strLogFileName = Application.StartupPath + "\\log\\Atom_";
        static string strStartUpPath = Application.StartupPath;
        static int nErrorIndex = 0;

        public static void LogWrite(string strMessage)
        {
            try
            {
                string strFileName = strLogFileName + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
                System.IO.File.AppendAllText(strFileName, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + strMessage + "\r\n");
            }
            catch
            {
                System.IO.Directory.CreateDirectory(strStartUpPath + "\\log");
                string strFileName = strLogFileName + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
                System.IO.File.AppendAllText(strFileName, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + strMessage + "\r\n");
            }
        }

        public static void LogWrite(LogType enLogType, string strMessage)
        {
            try
            {
                string strFileName = "";
                switch (enLogType)
                {
                    case LogType.Log:
                        strFileName = strLogFileName + "Log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
                        break;
                    case LogType.Data:
                        strFileName = strLogFileName + "Data_" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
                        break;
                    case LogType.Error:
                        strFileName = strLogFileName + "Error_" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
                        break;
                }
                System.IO.File.AppendAllText(strFileName, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + strMessage + "\r\n");
            }
            catch
            {
                nErrorIndex++;
                string strFileName = strLogFileName + "LogError" + DateTime.Now.ToString("yyyy-MM-dd") + "_" + DateTime.Now.ToFileTime().ToString() + "_" + nErrorIndex.ToString() + ".log";
                System.IO.File.AppendAllText(strFileName, strMessage);
            }
        }

        public static void LogWrite(LogType enLogType, int nUID, string strMessage)
        {
            try
            {
                string strFileName = "";
                switch (enLogType)
                {
                    case LogType.Log:
                        strFileName = strLogFileName + "Log_" + DateTime.Now.ToString("yyyy-MM-dd") + "_" + nUID.ToString() + ".log";
                        break;
                    case LogType.Data:
                        strFileName = strLogFileName + "Data_" + DateTime.Now.ToString("yyyy-MM-dd") + "_" + nUID.ToString() + ".log";
                        break;
                    case LogType.Error:
                        strFileName = strLogFileName + "Error_" + DateTime.Now.ToString("yyyy-MM-dd") + "_" + nUID.ToString() + ".log";
                        break;
                }
                System.IO.File.AppendAllText(strFileName, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + strMessage + "\r\n");
            }
            catch
            {
                nErrorIndex++;
                string strFileName = strLogFileName + "LogError" + DateTime.Now.ToString("yyyy-MM-dd") + "_" + nUID.ToString() + "_" + DateTime.Now.ToFileTime().ToString() + "_" + nErrorIndex.ToString() + ".log";
                System.IO.File.AppendAllText(strFileName, strMessage);
            }
        }

        public static void LogWrite(LogType enLogType, int nUID, string strHeader, byte[] strMessage)
        {
            try
            {
                string strFileName = "";
                switch (enLogType)
                {
                    case LogType.Log:
                        strFileName = strLogFileName + "Log_" + DateTime.Now.ToString("yyyy-MM-dd") + "_" + nUID.ToString() + ".log";
                        break;
                    case LogType.Data:
                        strFileName = strLogFileName + "Data_" + DateTime.Now.ToString("yyyy-MM-dd") + "_" + nUID.ToString() + ".log";
                        break;
                    case LogType.Error:
                        strFileName = strLogFileName + "Error_" + DateTime.Now.ToString("yyyy-MM-dd") + "_" + nUID.ToString() + ".log";
                        break;
                }
                System.IO.File.AppendAllText(strFileName, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + strHeader + "\t" + GetHexaString(strMessage) + "\r\n");
            }
            catch
            {
                nErrorIndex++;
                string strFileName = strLogFileName + "LogError" + DateTime.Now.ToString("yyyy-MM-dd") + "_" + nUID.ToString() + "_" + DateTime.Now.ToFileTime().ToString() + "_" + nErrorIndex.ToString() + ".log";
                System.IO.File.AppendAllText(strFileName, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + strHeader + "\t" + GetHexaString(strMessage) + "\r\n");
            }
        }

        public static void LogWrite(LogType enLogType, int nUID, double[] DataX, double[] DataY, double[] DataZ)
        {
            try
            {
                string strFileName = "";
                switch (enLogType)
                {
                    case LogType.Vib:
                        strFileName = strLogFileName + "Vib_" + DateTime.Now.ToString("yyyy-MM-dd") + "_" + nUID.ToString() + ".log";
                        break;
                }
                for (int i=0; i<DataX.Length; i++)
                    System.IO.File.AppendAllText(strFileName, DataX[i].ToString() + "," + DataY[i].ToString() + "," + DataZ[i].ToString() + "\r\n");
            }
            catch
            {
                nErrorIndex++;
                string strFileName = strLogFileName + "VibError" + DateTime.Now.ToString("yyyy-MM-dd") + "_" + nUID.ToString() + "_" + DateTime.Now.ToFileTime().ToString() + "_" + nErrorIndex.ToString() + ".log";
            }
        }

        public static string GetHexaString(byte[] Mesage)
        {
            return BitConverter.ToString(Mesage, 0);
        }

        /// <summary>
        /// 해당 글자를 byte로 변환한다.(글자수에 맞추어)
        /// </summary>
        /// <param name="strMessage"></param>
        /// <param name="nSize"></param>
        /// <returns></returns>
        public static byte[] GetByte(string strMessage, int nSize)
        {
            byte[] dimResult = new byte[nSize];
            byte[] dim = Encoding.UTF8.GetBytes(strMessage);
            for (int i = 0; i < dimResult.Length; i++)
            {
                if (dim.Length > i)
                {
                    dimResult[i] = dim[i];
                }
            }

            return dimResult;
        }


        /// <summary>
        /// byte 를 더한다.
        /// </summary>
        /// <param name="dim1"></param>
        /// <param name="dim2"></param>
        /// <returns></returns>
        public static byte[] BytePlus(byte[] dim1, byte[] dim2)
        {
            byte[] dimResult = new byte[dim1.Length + dim2.Length];
            int nIndex = 0;
            for (int i = 0; i < dim1.Length; i++)
            {
                dimResult[nIndex] = dim1[i];
                nIndex++;
            }
            for (int i = 0; i < dim2.Length; i++)
            {
                dimResult[nIndex] = dim2[i];
                nIndex++;
            }

            return dimResult;
        }

        public static byte[] BytePlus(byte dim1, byte dim2)
        {
            byte[] dimResult = new byte[2];

            dimResult[0] = dim1;
            dimResult[1] = dim2;

            return dimResult;
        }
        public static byte[] BytePlus(byte[] dim1, byte dim2)
        {
            byte[] dimResult = new byte[dim1.Length + 1];
            int nIndex = 0;
            for (int i = 0; i < dim1.Length; i++)
            {
                dimResult[nIndex] = dim1[i];
                nIndex++;
            }

            dimResult[nIndex] = dim2;

            return dimResult;
        }

        public static byte[] BytePlus(byte dim1, byte[] dim2)
        {
            byte[] dimResult = new byte[1 + dim2.Length];
            int nIndex = 0;

            dimResult[nIndex] = dim1;
            nIndex++;
            for (int i = 0; i < dim2.Length; i++)
            {
                dimResult[nIndex] = dim2[i];
                nIndex++;
            }

            return dimResult;
        }


        /// <summary>
        /// 체크썸
        /// </summary>
        /// <param name="dim"></param>
        /// <returns></returns>
        static public byte CheckSum(byte[] dim)
        {
            byte chk = 165;

            for (int i = 0; i < dim.Length; i++)
            {
                chk ^= (byte)dim[i];
            }

            return chk;
        }

        /// <summary>
        /// 체크썸
        /// </summary>
        /// <param name="dim"></param>
        /// <returns></returns>
        static public byte CheckSum(byte[] dim, int Start)
        {
            byte chk = 165;

            for (int i = Start; i < dim.Length; i++)
            {
                chk ^= (byte)dim[i];
            }

            return chk;
        }
        /// <summary>
        /// 체크썸
        /// </summary>
        /// <param name="dim"></param>
        /// <returns></returns>
        static public byte CheckSum(byte[] dim, int Start, int Length)
        {
            byte chk = 165;

            for (int i = Start; i < Start+ Length; i++)
            {
                chk ^= (byte)dim[i];
            }

            return chk;
        }

        static public bool FileMove(string Source, string Target)
        {
            try
            {
                System.IO.File.Move(Source, Target);
                return true;
            }
            catch (Exception ex)
            {
                LogWrite(LogType.Error, ex.Message + ex.StackTrace + "   파일이동 에러");
                return false;
            }

        }

        static public bool DirCreate(string DirName)
        {
            try
            {
                Directory.CreateDirectory(DirName);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
