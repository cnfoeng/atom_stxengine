﻿namespace AtomUI.ControlForm
{
    partial class ControlPad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlPad));
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbALpha = new System.Windows.Forms.TextBox();
            this.tbTorque = new System.Windows.Forms.TextBox();
            this.tbSpeed = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbRamp = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.lbTGEAlpha = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.lbTGDTorque = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.lbTGDSpeed = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.lbFBTorque = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.lbFBAlpha = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.lbFBSpeed = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lbEngineDuration = new System.Windows.Forms.Label();
            this.lbDynoDuration = new System.Windows.Forms.Label();
            this.tbEngineDuration = new System.Windows.Forms.TextBox();
            this.tbDynoDuration = new System.Windows.Forms.TextBox();
            this.lbEngine = new System.Windows.Forms.Label();
            this.lbDyno = new System.Windows.Forms.Label();
            this.chFeedBackEngine = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chFeedBackDyno = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnNT = new System.Windows.Forms.Button();
            this.btnNA = new System.Windows.Forms.Button();
            this.btnHornOff = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnAlarm = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.btnRemote = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.tmr_Refresh = new System.Windows.Forms.Timer(this.components);
            this.tmr_lamp = new System.Windows.Forms.Timer(this.components);
            this.btnTN = new System.Windows.Forms.Button();
            this.btnTA = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox5.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chFeedBackEngine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chFeedBackDyno)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.groupBox10);
            this.groupBox5.Controls.Add(this.groupBox9);
            this.groupBox5.Controls.Add(this.groupBox7);
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox5.Location = new System.Drawing.Point(12, 11);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Size = new System.Drawing.Size(959, 579);
            this.groupBox5.TabIndex = 75;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Step Mode Control Panel";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.btnSend);
            this.groupBox10.Controls.Add(this.label9);
            this.groupBox10.Controls.Add(this.label8);
            this.groupBox10.Controls.Add(this.label7);
            this.groupBox10.Controls.Add(this.tbALpha);
            this.groupBox10.Controls.Add(this.tbTorque);
            this.groupBox10.Controls.Add(this.tbSpeed);
            this.groupBox10.Controls.Add(this.label6);
            this.groupBox10.Controls.Add(this.label5);
            this.groupBox10.Controls.Add(this.label3);
            this.groupBox10.Controls.Add(this.tbRamp);
            this.groupBox10.Controls.Add(this.label1);
            this.groupBox10.Controls.Add(this.label16);
            this.groupBox10.Location = new System.Drawing.Point(8, 411);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox10.Size = new System.Drawing.Size(460, 159);
            this.groupBox10.TabIndex = 35;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Command";
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(323, 103);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(111, 30);
            this.btnSend.TabIndex = 56;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.label9.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(257, 103);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 30);
            this.label9.TabIndex = 55;
            this.label9.Text = "%";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.label8.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(257, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 30);
            this.label8.TabIndex = 54;
            this.label8.Text = "Nm";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.label7.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(257, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 30);
            this.label7.TabIndex = 53;
            this.label7.Text = "RPM";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbALpha
            // 
            this.tbALpha.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbALpha.Location = new System.Drawing.Point(160, 103);
            this.tbALpha.Name = "tbALpha";
            this.tbALpha.Size = new System.Drawing.Size(91, 29);
            this.tbALpha.TabIndex = 52;
            this.tbALpha.Tag = "";
            this.tbALpha.Text = "0";
            // 
            // tbTorque
            // 
            this.tbTorque.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbTorque.Location = new System.Drawing.Point(160, 65);
            this.tbTorque.Name = "tbTorque";
            this.tbTorque.Size = new System.Drawing.Size(91, 29);
            this.tbTorque.TabIndex = 51;
            this.tbTorque.Tag = "";
            this.tbTorque.Text = "0";
            // 
            // tbSpeed
            // 
            this.tbSpeed.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbSpeed.Location = new System.Drawing.Point(160, 25);
            this.tbSpeed.Name = "tbSpeed";
            this.tbSpeed.Size = new System.Drawing.Size(91, 29);
            this.tbSpeed.TabIndex = 50;
            this.tbSpeed.Tag = "";
            this.tbSpeed.Text = "0";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label6.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(7, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(147, 30);
            this.label6.TabIndex = 49;
            this.label6.Tag = "F";
            this.label6.Text = "Target ENG Alpha";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label5.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(7, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 30);
            this.label5.TabIndex = 48;
            this.label5.Tag = "F";
            this.label5.Text = "Target Dyno Torque";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.label3.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(395, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 21);
            this.label3.TabIndex = 47;
            this.label3.Text = "SEC";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbRamp
            // 
            this.tbRamp.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbRamp.Location = new System.Drawing.Point(323, 64);
            this.tbRamp.Name = "tbRamp";
            this.tbRamp.Size = new System.Drawing.Size(66, 22);
            this.tbRamp.TabIndex = 36;
            this.tbRamp.Text = "2";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label1.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(323, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 30);
            this.label1.TabIndex = 35;
            this.label1.Tag = "F";
            this.label1.Text = "RAMP";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label16.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(7, 25);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(147, 30);
            this.label16.TabIndex = 34;
            this.label16.Tag = "F";
            this.label16.Text = "Target Dyno Speed";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.lbTGEAlpha);
            this.groupBox9.Controls.Add(this.label36);
            this.groupBox9.Controls.Add(this.lbTGDTorque);
            this.groupBox9.Controls.Add(this.label38);
            this.groupBox9.Controls.Add(this.lbTGDSpeed);
            this.groupBox9.Controls.Add(this.label40);
            this.groupBox9.Controls.Add(this.lbFBTorque);
            this.groupBox9.Controls.Add(this.label30);
            this.groupBox9.Controls.Add(this.lbFBAlpha);
            this.groupBox9.Controls.Add(this.label28);
            this.groupBox9.Controls.Add(this.lbFBSpeed);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Location = new System.Drawing.Point(8, 248);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox9.Size = new System.Drawing.Size(460, 159);
            this.groupBox9.TabIndex = 34;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Control IO Status";
            // 
            // lbTGEAlpha
            // 
            this.lbTGEAlpha.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbTGEAlpha.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbTGEAlpha.ForeColor = System.Drawing.Color.White;
            this.lbTGEAlpha.Location = new System.Drawing.Point(302, 114);
            this.lbTGEAlpha.Name = "lbTGEAlpha";
            this.lbTGEAlpha.Size = new System.Drawing.Size(131, 28);
            this.lbTGEAlpha.TabIndex = 48;
            this.lbTGEAlpha.Text = "0 %";
            this.lbTGEAlpha.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label36
            // 
            this.label36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label36.ForeColor = System.Drawing.Color.White;
            this.label36.Location = new System.Drawing.Point(303, 93);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(131, 21);
            this.label36.TabIndex = 47;
            this.label36.Tag = "F";
            this.label36.Text = "Target ENG Alpha";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbTGDTorque
            // 
            this.lbTGDTorque.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbTGDTorque.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbTGDTorque.ForeColor = System.Drawing.Color.White;
            this.lbTGDTorque.Location = new System.Drawing.Point(162, 114);
            this.lbTGDTorque.Name = "lbTGDTorque";
            this.lbTGDTorque.Size = new System.Drawing.Size(131, 28);
            this.lbTGDTorque.TabIndex = 46;
            this.lbTGDTorque.Text = "0 Nm";
            this.lbTGDTorque.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label38
            // 
            this.label38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label38.ForeColor = System.Drawing.Color.White;
            this.label38.Location = new System.Drawing.Point(163, 93);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(131, 21);
            this.label38.TabIndex = 45;
            this.label38.Tag = "F";
            this.label38.Text = "Target Dyno Torque";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbTGDSpeed
            // 
            this.lbTGDSpeed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbTGDSpeed.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbTGDSpeed.ForeColor = System.Drawing.Color.White;
            this.lbTGDSpeed.Location = new System.Drawing.Point(22, 114);
            this.lbTGDSpeed.Name = "lbTGDSpeed";
            this.lbTGDSpeed.Size = new System.Drawing.Size(131, 28);
            this.lbTGDSpeed.TabIndex = 44;
            this.lbTGDSpeed.Text = "0 RPM";
            this.lbTGDSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label40
            // 
            this.label40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label40.ForeColor = System.Drawing.Color.White;
            this.label40.Location = new System.Drawing.Point(23, 93);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(131, 21);
            this.label40.TabIndex = 43;
            this.label40.Tag = "F";
            this.label40.Text = "Target Dyno Speed";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbFBTorque
            // 
            this.lbFBTorque.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbFBTorque.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbFBTorque.ForeColor = System.Drawing.Color.White;
            this.lbFBTorque.Location = new System.Drawing.Point(162, 47);
            this.lbFBTorque.Name = "lbFBTorque";
            this.lbFBTorque.Size = new System.Drawing.Size(131, 28);
            this.lbFBTorque.TabIndex = 40;
            this.lbFBTorque.Text = "0 Nm";
            this.lbFBTorque.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(163, 26);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(131, 21);
            this.label30.TabIndex = 39;
            this.label30.Tag = "F";
            this.label30.Text = "Feedback Torque";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbFBAlpha
            // 
            this.lbFBAlpha.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbFBAlpha.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbFBAlpha.ForeColor = System.Drawing.Color.White;
            this.lbFBAlpha.Location = new System.Drawing.Point(302, 47);
            this.lbFBAlpha.Name = "lbFBAlpha";
            this.lbFBAlpha.Size = new System.Drawing.Size(131, 28);
            this.lbFBAlpha.TabIndex = 38;
            this.lbFBAlpha.Text = "0 %";
            this.lbFBAlpha.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(303, 26);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(131, 21);
            this.label28.TabIndex = 37;
            this.label28.Tag = "F";
            this.label28.Text = "Feedback Alpha";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbFBSpeed
            // 
            this.lbFBSpeed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbFBSpeed.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbFBSpeed.ForeColor = System.Drawing.Color.White;
            this.lbFBSpeed.Location = new System.Drawing.Point(22, 47);
            this.lbFBSpeed.Name = "lbFBSpeed";
            this.lbFBSpeed.Size = new System.Drawing.Size(131, 28);
            this.lbFBSpeed.TabIndex = 36;
            this.lbFBSpeed.Text = "0 RPM";
            this.lbFBSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(23, 26);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(131, 21);
            this.label18.TabIndex = 17;
            this.label18.Tag = "F";
            this.label18.Text = "Feedback Speed";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.lbEngineDuration);
            this.groupBox7.Controls.Add(this.lbDynoDuration);
            this.groupBox7.Controls.Add(this.tbEngineDuration);
            this.groupBox7.Controls.Add(this.tbDynoDuration);
            this.groupBox7.Controls.Add(this.lbEngine);
            this.groupBox7.Controls.Add(this.lbDyno);
            this.groupBox7.Controls.Add(this.chFeedBackEngine);
            this.groupBox7.Controls.Add(this.chFeedBackDyno);
            this.groupBox7.Location = new System.Drawing.Point(474, 19);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Size = new System.Drawing.Size(479, 551);
            this.groupBox7.TabIndex = 33;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "FeedBack Value";
            // 
            // lbEngineDuration
            // 
            this.lbEngineDuration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.lbEngineDuration.ForeColor = System.Drawing.Color.White;
            this.lbEngineDuration.Location = new System.Drawing.Point(373, 483);
            this.lbEngineDuration.Name = "lbEngineDuration";
            this.lbEngineDuration.Size = new System.Drawing.Size(90, 26);
            this.lbEngineDuration.TabIndex = 83;
            this.lbEngineDuration.Tag = "F";
            this.lbEngineDuration.Text = "X Axis Duration";
            this.lbEngineDuration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbDynoDuration
            // 
            this.lbDynoDuration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.lbDynoDuration.ForeColor = System.Drawing.Color.White;
            this.lbDynoDuration.Location = new System.Drawing.Point(371, 218);
            this.lbDynoDuration.Name = "lbDynoDuration";
            this.lbDynoDuration.Size = new System.Drawing.Size(90, 26);
            this.lbDynoDuration.TabIndex = 82;
            this.lbDynoDuration.Tag = "F";
            this.lbDynoDuration.Text = "X Axis Duration";
            this.lbDynoDuration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbEngineDuration
            // 
            this.tbEngineDuration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.tbEngineDuration.Location = new System.Drawing.Point(373, 519);
            this.tbEngineDuration.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbEngineDuration.Name = "tbEngineDuration";
            this.tbEngineDuration.Size = new System.Drawing.Size(91, 21);
            this.tbEngineDuration.TabIndex = 81;
            this.tbEngineDuration.Text = "100";
            this.tbEngineDuration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbEngineDuration.TextChanged += new System.EventHandler(this.tbEngineDuration_TextChanged);
            // 
            // tbDynoDuration
            // 
            this.tbDynoDuration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.tbDynoDuration.Location = new System.Drawing.Point(370, 255);
            this.tbDynoDuration.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbDynoDuration.Name = "tbDynoDuration";
            this.tbDynoDuration.Size = new System.Drawing.Size(91, 21);
            this.tbDynoDuration.TabIndex = 80;
            this.tbDynoDuration.Text = "100";
            this.tbDynoDuration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbDynoDuration.TextChanged += new System.EventHandler(this.tbDynoDuration_TextChanged);
            // 
            // lbEngine
            // 
            this.lbEngine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.lbEngine.ForeColor = System.Drawing.Color.White;
            this.lbEngine.Location = new System.Drawing.Point(371, 290);
            this.lbEngine.Name = "lbEngine";
            this.lbEngine.Size = new System.Drawing.Size(90, 26);
            this.lbEngine.TabIndex = 79;
            this.lbEngine.Tag = "F";
            this.lbEngine.Text = "Engine";
            this.lbEngine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbDyno
            // 
            this.lbDyno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.lbDyno.ForeColor = System.Drawing.Color.White;
            this.lbDyno.Location = new System.Drawing.Point(371, 25);
            this.lbDyno.Name = "lbDyno";
            this.lbDyno.Size = new System.Drawing.Size(90, 26);
            this.lbDyno.TabIndex = 77;
            this.lbDyno.Tag = "F";
            this.lbDyno.Text = "Dyno";
            this.lbDyno.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chFeedBackEngine
            // 
            this.chFeedBackEngine.BorderlineColor = System.Drawing.Color.Black;
            this.chFeedBackEngine.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea1.Name = "ChartArea1";
            this.chFeedBackEngine.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chFeedBackEngine.Legends.Add(legend1);
            this.chFeedBackEngine.Location = new System.Drawing.Point(13, 290);
            this.chFeedBackEngine.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chFeedBackEngine.Name = "chFeedBackEngine";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chFeedBackEngine.Series.Add(series1);
            this.chFeedBackEngine.Size = new System.Drawing.Size(354, 251);
            this.chFeedBackEngine.TabIndex = 74;
            this.chFeedBackEngine.Text = "chart3";
            // 
            // chFeedBackDyno
            // 
            this.chFeedBackDyno.BorderlineColor = System.Drawing.Color.Black;
            this.chFeedBackDyno.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea2.Name = "ChartArea1";
            this.chFeedBackDyno.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chFeedBackDyno.Legends.Add(legend2);
            this.chFeedBackDyno.Location = new System.Drawing.Point(13, 25);
            this.chFeedBackDyno.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chFeedBackDyno.Name = "chFeedBackDyno";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chFeedBackDyno.Series.Add(series2);
            this.chFeedBackDyno.Size = new System.Drawing.Size(354, 251);
            this.chFeedBackDyno.TabIndex = 73;
            this.chFeedBackDyno.Text = "chart2";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btnTN);
            this.groupBox6.Controls.Add(this.btnTA);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.btnNT);
            this.groupBox6.Controls.Add(this.btnNA);
            this.groupBox6.Controls.Add(this.btnHornOff);
            this.groupBox6.Controls.Add(this.btnReset);
            this.groupBox6.Controls.Add(this.btnAlarm);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.btnRemote);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Location = new System.Drawing.Point(8, 19);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Size = new System.Drawing.Size(460, 225);
            this.groupBox6.TabIndex = 31;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "SYSTEM STATUS";
            // 
            // btnNT
            // 
            this.btnNT.BackColor = System.Drawing.Color.Silver;
            this.btnNT.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNT.BackgroundImage")));
            this.btnNT.Enabled = false;
            this.btnNT.FlatAppearance.BorderSize = 0;
            this.btnNT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNT.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnNT.Location = new System.Drawing.Point(261, 153);
            this.btnNT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnNT.Name = "btnNT";
            this.btnNT.Size = new System.Drawing.Size(66, 47);
            this.btnNT.TabIndex = 43;
            this.btnNT.UseVisualStyleBackColor = false;
            this.btnNT.Click += new System.EventHandler(this.btnNT_Click);
            // 
            // btnNA
            // 
            this.btnNA.BackColor = System.Drawing.Color.Silver;
            this.btnNA.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNA.BackgroundImage")));
            this.btnNA.Enabled = false;
            this.btnNA.FlatAppearance.BorderSize = 0;
            this.btnNA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNA.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnNA.Location = new System.Drawing.Point(261, 57);
            this.btnNA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnNA.Name = "btnNA";
            this.btnNA.Size = new System.Drawing.Size(66, 47);
            this.btnNA.TabIndex = 41;
            this.btnNA.UseVisualStyleBackColor = false;
            this.btnNA.Click += new System.EventHandler(this.btnNA_Click);
            // 
            // btnHornOff
            // 
            this.btnHornOff.BackColor = System.Drawing.Color.Silver;
            this.btnHornOff.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnHornOff.BackgroundImage")));
            this.btnHornOff.Enabled = false;
            this.btnHornOff.FlatAppearance.BorderSize = 0;
            this.btnHornOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHornOff.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnHornOff.Location = new System.Drawing.Point(367, 153);
            this.btnHornOff.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnHornOff.Name = "btnHornOff";
            this.btnHornOff.Size = new System.Drawing.Size(66, 47);
            this.btnHornOff.TabIndex = 39;
            this.btnHornOff.UseVisualStyleBackColor = false;
            this.btnHornOff.Click += new System.EventHandler(this.btnHornOff_Click);
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.Silver;
            this.btnReset.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnReset.BackgroundImage")));
            this.btnReset.Enabled = false;
            this.btnReset.FlatAppearance.BorderSize = 0;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnReset.Location = new System.Drawing.Point(368, 57);
            this.btnReset.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(66, 47);
            this.btnReset.TabIndex = 39;
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnAlarm
            // 
            this.btnAlarm.BackColor = System.Drawing.Color.Silver;
            this.btnAlarm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAlarm.BackgroundImage")));
            this.btnAlarm.Enabled = false;
            this.btnAlarm.FlatAppearance.BorderSize = 0;
            this.btnAlarm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlarm.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAlarm.Location = new System.Drawing.Point(34, 153);
            this.btnAlarm.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAlarm.Name = "btnAlarm";
            this.btnAlarm.Size = new System.Drawing.Size(66, 47);
            this.btnAlarm.TabIndex = 38;
            this.btnAlarm.UseVisualStyleBackColor = false;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(353, 123);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 21);
            this.label4.TabIndex = 34;
            this.label4.Text = "HORN OFF";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(247, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 21);
            this.label2.TabIndex = 37;
            this.label2.Text = "N/T";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(354, 27);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 21);
            this.label12.TabIndex = 34;
            this.label12.Text = "RESET";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(20, 123);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 21);
            this.label13.TabIndex = 33;
            this.label13.Text = "ALARM";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(247, 27);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(94, 21);
            this.label14.TabIndex = 32;
            this.label14.Tag = "";
            this.label14.Text = "N/α";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnRemote
            // 
            this.btnRemote.BackColor = System.Drawing.Color.Silver;
            this.btnRemote.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRemote.BackgroundImage")));
            this.btnRemote.Enabled = false;
            this.btnRemote.FlatAppearance.BorderSize = 0;
            this.btnRemote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemote.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnRemote.Location = new System.Drawing.Point(34, 57);
            this.btnRemote.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRemote.Name = "btnRemote";
            this.btnRemote.Size = new System.Drawing.Size(66, 47);
            this.btnRemote.TabIndex = 29;
            this.btnRemote.UseVisualStyleBackColor = false;
            this.btnRemote.Click += new System.EventHandler(this.btnRemote_Click);
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(20, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 21);
            this.label10.TabIndex = 23;
            this.label10.Text = "REMOTE";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tmr_Refresh
            // 
            this.tmr_Refresh.Enabled = true;
            this.tmr_Refresh.Tick += new System.EventHandler(this.tmr_Refresh_Tick);
            // 
            // tmr_lamp
            // 
            this.tmr_lamp.Tick += new System.EventHandler(this.tmr_lamp_Tick);
            // 
            // btnTN
            // 
            this.btnTN.BackColor = System.Drawing.Color.Silver;
            this.btnTN.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTN.BackgroundImage")));
            this.btnTN.Enabled = false;
            this.btnTN.FlatAppearance.BorderSize = 0;
            this.btnTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTN.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnTN.Location = new System.Drawing.Point(143, 153);
            this.btnTN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnTN.Name = "btnTN";
            this.btnTN.Size = new System.Drawing.Size(66, 47);
            this.btnTN.TabIndex = 47;
            this.btnTN.UseVisualStyleBackColor = false;
            this.btnTN.Click += new System.EventHandler(this.btnTN_Click);
            // 
            // btnTA
            // 
            this.btnTA.BackColor = System.Drawing.Color.Silver;
            this.btnTA.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTA.BackgroundImage")));
            this.btnTA.Enabled = false;
            this.btnTA.FlatAppearance.BorderSize = 0;
            this.btnTA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTA.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnTA.Location = new System.Drawing.Point(143, 57);
            this.btnTA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnTA.Name = "btnTA";
            this.btnTA.Size = new System.Drawing.Size(66, 47);
            this.btnTA.TabIndex = 46;
            this.btnTA.UseVisualStyleBackColor = false;
            this.btnTA.Click += new System.EventHandler(this.btnTA_Click);
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(129, 123);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 21);
            this.label11.TabIndex = 45;
            this.label11.Text = "T/N";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(129, 27);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 21);
            this.label15.TabIndex = 44;
            this.label15.Tag = "";
            this.label15.Text = "T/α";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ControlPad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(986, 600);
            this.Controls.Add(this.groupBox5);
            this.Name = "ControlPad";
            this.Text = "ControlPad";
            this.Load += new System.EventHandler(this.ControlPad_Load);
            this.groupBox5.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chFeedBackEngine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chFeedBackDyno)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbRamp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label lbTGEAlpha;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label lbTGDTorque;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label lbTGDSpeed;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label lbFBTorque;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label lbFBAlpha;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label lbFBSpeed;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label lbEngineDuration;
        private System.Windows.Forms.Label lbDynoDuration;
        private System.Windows.Forms.TextBox tbEngineDuration;
        private System.Windows.Forms.TextBox tbDynoDuration;
        private System.Windows.Forms.Label lbEngine;
        private System.Windows.Forms.Label lbDyno;
        private System.Windows.Forms.DataVisualization.Charting.Chart chFeedBackEngine;
        private System.Windows.Forms.DataVisualization.Charting.Chart chFeedBackDyno;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnNT;
        private System.Windows.Forms.Button btnNA;
        private System.Windows.Forms.Button btnHornOff;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnAlarm;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnRemote;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Timer tmr_Refresh;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbALpha;
        private System.Windows.Forms.TextBox tbTorque;
        private System.Windows.Forms.TextBox tbSpeed;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Timer tmr_lamp;
        private System.Windows.Forms.Button btnTN;
        private System.Windows.Forms.Button btnTA;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label15;
    }
}