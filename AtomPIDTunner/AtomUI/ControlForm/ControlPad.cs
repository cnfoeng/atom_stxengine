﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;
using ACS.SPiiPlusNET;
using System.Windows.Forms.DataVisualization.Charting;

namespace AtomUI.ControlForm
{
    public partial class ControlPad : Form
    {
        int DynoDuration = 100;
        int EngineDuration = 100;
        

        public ControlPad()
        {
            InitializeComponent();

            btnControl(true);

        }

        private void btnControl(bool Enable)
        {
            ButtonStatusChange(btnAlarm, false, Enable);
            ButtonStatusChange(btnReset, false, Enable);
            ButtonStatusChange(btnHornOff, false, Enable);

            ButtonStatusChange(btnRemote, false, Enable);
            ButtonStatusChange(btnNA, false, Enable);
            ButtonStatusChange(btnNT, false, Enable);
            ButtonStatusChange(btnTA, false, Enable);
            ButtonStatusChange(btnTN, false, Enable);


        }
        private void tmr_Refresh_Tick(object sender, EventArgs e)
        {
            CurrentTime = CurrentTime + tmr_Refresh.Interval/1000.0;
            COntrolHWStatusUpdate();
            SetGraph();

        }

        private void ChartInit(Chart chart1)
        {
            //X축 데이터 타입
            chart1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(223)))), ((int)(((byte)(193)))));
            chart1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(64)))), ((int)(((byte)(1)))));
            chart1.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chart1.BorderlineWidth = 2;

            chart1.ChartAreas[0].BackColor = System.Drawing.Color.OldLace;
            chart1.ChartAreas[0].BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.ChartAreas[0].BackSecondaryColor = System.Drawing.Color.White;
            chart1.ChartAreas[0].BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;

            //chart1.ChartAreas[0].CursorX.LineColor = Color.Gray;
            //chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            //chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;

            chart1.ChartAreas[0].AxisX.LabelStyle.Format = "#.0";
            chart1.ChartAreas[0].AxisX.IsLabelAutoFit = false;
            chart1.ChartAreas[0].AxisX.LabelStyle.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            chart1.ChartAreas[0].AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].AxisX.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].AxisY.IsLabelAutoFit = false;
            chart1.ChartAreas[0].AxisY.LabelStyle.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            chart1.ChartAreas[0].AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            //chart1.ChartAreas[0].AxisY.Maximum = 5000;
            //chart1.ChartAreas[0].AxisY.Minimum = 1000;

        }

        private void SeriesInit(Chart chart1)
        {
            chart1.Series.Clear();
            chart1.Series.Add("Target");
            chart1.Series.Add("Feedback");

            chart1.Series[0].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            //Y축 데이터 타입
            chart1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;  //분산형 그래프
            chart1.Series[1].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;  //분산형 그래프
            chart1.Series[0].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;

            //차트 타입
            chart1.Series[0].IsXValueIndexed = false;
            chart1.Series[1].IsXValueIndexed = false;
            chart1.Series[0].Color = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(65)))), ((int)(((byte)(140)))), ((int)(((byte)(240)))));
            chart1.Series[1].Color = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(224)))), ((int)(((byte)(64)))), ((int)(((byte)(10)))));

            //chart1.ChartAreas[0].CursorX.Position = 50;
            //chart1.ChartAreas[0].CursorX.LineColor = Color.Gray;
            //chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            //chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
        }

        private void COntrolHWStatusUpdate()
        {
            ButtonStatusChange(btnRemote, (Ethercat.g_SystemState[0] == 0 ? false : true), true);
            ButtonStatusChange(btnNA, ((Ethercat.g_SystemState[1] == 1 && Ethercat.g_SystemState[2] == 0) ? true : false), true);
            ButtonStatusChange(btnNT, ((Ethercat.g_SystemState[1] == 0 && Ethercat.g_SystemState[2] == 1) ? true : false), true);
            ButtonStatusChange(btnTA, ((Ethercat.g_SystemState[1] == 0 && Ethercat.g_SystemState[2] == 0) ? true : false), true);
            ButtonStatusChange(btnTN, ((Ethercat.g_SystemState[1] == 1 && Ethercat.g_SystemState[2] == 1) ? true : false), true);
            ButtonStatusChange(btnAlarm, (Ethercat.g_SystemState[3] == 0 ? false : true), true);
            ButtonStatusChange(btnReset, (Ethercat.g_SystemState[4] == 0 ? false : true), true);
            ButtonStatusChange(btnHornOff, (Ethercat.g_SystemState[5] == 0 ? false : true), true);

            lbFBSpeed.Text = DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).RealValue.ToString() + " " + DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).Unit;
            lbFBTorque.Text = DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).RealValue.ToString() + " " + DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).Unit;
            lbFBAlpha.Text = DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).RealValue.ToString() + " " + DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).Unit;
            lbTGDSpeed.Text = Math.Round(Ethercat.g_TargetValue[0], DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).Round).ToString() + " " + DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).Unit;
            lbTGDTorque.Text = Math.Round(Ethercat.g_TargetValue[1], DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).Round).ToString() + " " + DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).Unit;
            lbTGEAlpha.Text = Math.Round(Ethercat.g_TargetValue[2], DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).Round).ToString() + " " + DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).Unit;
            //g_FeedbackValue;
        }

        private void ButtonStatusChange(Button btn, bool status, bool enable)
        {
            if (status)
            {
                btn.BackgroundImage = global::AtomUI.Properties.Resources.on버튼;
            }
            else
            {
                if (enable)
                    btn.BackgroundImage = global::AtomUI.Properties.Resources.off버튼;
                else
                    btn.BackgroundImage = global::AtomUI.Properties.Resources.off버튼_회색;
            }

            btn.Enabled = enable;
        }
        private void btnRemote_Click(object sender, EventArgs e)
        {
            double[] SendData;
            if (Ethercat.g_SystemState[0] == 0)
                SendData = new double[1] { 1 };
            else
                SendData = new double[1] { 0 };

            Ethercat.Ch.WriteVariable(SendData, "g_SystemState_Remote", ProgramBuffer.ACSC_NONE, 0, 0, 0, 0);
            Ethercat.Ch.WriteVariable(1, "g_SystemState_Mode_Change", ProgramBuffer.ACSC_NONE, 0, 0, 0, 0);
        }

        private void btnHornOff_Click(object sender, EventArgs e)
        {
            double[] SendData;

            if (Ethercat.g_SystemState[5] == 0)
                SendData = new double[1] { 1 };
            else
                SendData = new double[1] { 0 };

            Ethercat.Ch.WriteVariable(SendData, "g_SystemState_HornOff", ProgramBuffer.ACSC_NONE, 0, 0, 0, 0);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            double[] SendData;
            SendData = new double[1] { 1 };
            Ethercat.Ch.WriteVariable(SendData, "g_SystemState_Reset", ProgramBuffer.ACSC_NONE, 0, 0, 0, 0);
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            btnSend.Enabled = false;
            duration = double.Parse(tbRamp.Text);
            double[] g_TargetValue_Temp = new double[3] { double.Parse(tbSpeed.Text), double.Parse(tbTorque.Text), double.Parse(tbALpha.Text) };
            t_TargetValue = new double[3]
                    {
                        Ethercat.GetIOValue(g_TargetValue_Temp[0], DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).CalibrationTable),
                        Ethercat.GetIOValue(g_TargetValue_Temp[1], DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).CalibrationTable),
                        Ethercat.GetIOValue(g_TargetValue_Temp[2], DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).CalibrationTable)
                    };


            l_TargetValue = new double[3]
                    {
                        Ethercat.GetIOValue(Ethercat.g_TargetValue[0], DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).CalibrationTable),
                        Ethercat.GetIOValue(Ethercat.g_TargetValue[1], DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).CalibrationTable),
                        Ethercat.GetIOValue(Ethercat.g_TargetValue[2], DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).CalibrationTable)
                    }; 
            elapsed = 0;
            tmr_lamp.Start();
        }

        private void ControlPad_Load(object sender, EventArgs e)
        {
            ChartInit(chFeedBackDyno);
            SeriesInit(chFeedBackDyno);

            ChartInit(chFeedBackEngine);
            SeriesInit(chFeedBackEngine);

            GraphResize();
        }
        double CurrentTime;

        private void SetGraph()
        {
            //CurrentTime = CurrentTime + 0.1;
            //PhaseCurrentTime = PhaseCurrentTime + 0.1;

            DataRowCollection DT = TransientMode.TransientModeData.Data.Tables["TransientMode"].Rows;

            double Target = 0.0;
            double FeedBack = 0.0;

            Target = Math.Round(Ethercat.g_TargetValue[0], DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).Round);
            FeedBack = DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).RealValue;

            //if (chFeedBackDyno.Series[0].Points.Count > DynoDuration*10 + 1)
            //{
            //    chFeedBackDyno.Series[0].Points.RemoveAt(0);
            //    chFeedBackDyno.Series[1].Points.RemoveAt(0);
            //}

            chFeedBackDyno.Series[0].Points.Add(new DataPoint(CurrentTime, Target));
            chFeedBackDyno.Series[1].Points.Add(new DataPoint(CurrentTime, FeedBack));
            if (chFeedBackDyno.Series[0].Points.Count > 0)
            {
                if (CurrentTime > DynoDuration)
                {
                    chFeedBackDyno.ChartAreas[0].AxisX.Minimum = CurrentTime - DynoDuration;
                    chFeedBackDyno.ChartAreas[0].AxisX.Maximum = CurrentTime;
                }
                else
                {
                    chFeedBackDyno.ChartAreas[0].AxisX.Minimum = 0;
                    chFeedBackDyno.ChartAreas[0].AxisX.Maximum = DynoDuration;
                }
                chFeedBackDyno.Invalidate();
            }

            if (Ethercat.g_SystemState[1] == 1)
            {
                Target = Math.Round(Ethercat.g_TargetValue[2], DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).Round);
                FeedBack = DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).RealValue;
            }
            else
            {
                Target = Math.Round(Ethercat.g_TargetValue[1], DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).Round);
                FeedBack = DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).RealValue;
            }

            chFeedBackEngine.Series[0].Points.Add(new DataPoint(CurrentTime, Target));
            chFeedBackEngine.Series[1].Points.Add(new DataPoint(CurrentTime, FeedBack));
            if (chFeedBackEngine.Series[0].Points.Count > 0)
            {
                if (CurrentTime > EngineDuration)
                {
                    chFeedBackEngine.ChartAreas[0].AxisX.Minimum = CurrentTime - EngineDuration;
                    chFeedBackEngine.ChartAreas[0].AxisX.Maximum = CurrentTime;
                }
                else
                {
                    chFeedBackEngine.ChartAreas[0].AxisX.Minimum = 0;
                    chFeedBackEngine.ChartAreas[0].AxisX.Maximum = EngineDuration;
                }
                chFeedBackEngine.Invalidate();
            }
        }
        private void GraphResize()
        {
        //    chFeedBackDyno.Height = groupBox7.Height - 52;
        //    chFeedBackEngine.Height = groupBox7.Height - 52;

        //    chFeedBackDyno.Width = (groupBox7.Width - 52) / 2;
        //    chFeedBackEngine.Width = (groupBox7.Width - 52) / 2;

        //    chFeedBackEngine.Left = (groupBox7.Width / 2) + 10;

        //    lbDyno.Left = chFeedBackDyno.Left + chFeedBackDyno.Width - 110;
        //    lbDynoDuration.Left = chFeedBackDyno.Left + chFeedBackDyno.Width - 110;
        //    tbDynoDuration.Left = chFeedBackDyno.Left + chFeedBackDyno.Width - 110;

        //    lbEngine.Left = chFeedBackEngine.Left + chFeedBackEngine.Width - 110;
        //    lbEngineDuration.Left = chFeedBackEngine.Left + chFeedBackEngine.Width - 110;
        //    tbEngineDuration.Left = chFeedBackEngine.Left + chFeedBackEngine.Width - 110;
        }

        private void tbDynoDuration_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DynoDuration = int.Parse(tbDynoDuration.Text);
            }
            catch (Exception ex)
            {
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n connect Error!!");
            }
        }

        private void tbEngineDuration_TextChanged(object sender, EventArgs e)
        {
             try
            {
           EngineDuration = int.Parse(tbEngineDuration.Text);
            }
            catch (Exception ex)
            {
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n connect Error!!");
            }
        }

        double elapsed = 0;
        double duration = 0;
        double[] l_TargetValue;
        double[] t_TargetValue;

        private void tmr_lamp_Tick(object sender, EventArgs e)
        {
            double[] SendData;
            //t_TargetValue = new double[3] { double.Parse(tbSpeed.Text), double.Parse(tbTorque.Text), double.Parse(tbALpha.Text) };
            //l_TargetValue = Ethercat.g_TargetValue;

            SendData = new double[3] { (t_TargetValue[0] - l_TargetValue[0]) / duration * elapsed + l_TargetValue[0], (t_TargetValue[1] - l_TargetValue[1]) / duration * elapsed + l_TargetValue[1], (t_TargetValue[2] - l_TargetValue[2]) / duration * elapsed + l_TargetValue[2] };

            if (duration > elapsed)
            {

                Ethercat.Ch.WriteVariable(SendData, "g_Target", ProgramBuffer.ACSC_NONE, 0, 0, 0, 2);

                elapsed = elapsed + tmr_lamp.Interval / 1000.0;
            }
            else
            {
                SendData = new double[3] { t_TargetValue[0], t_TargetValue[1], t_TargetValue[2] };
                Ethercat.Ch.WriteVariable(SendData, "g_Target", ProgramBuffer.ACSC_NONE, 0, 0, 0, 2);

                btnSend.Enabled = true;

                tmr_lamp.Stop();
            }
        }

        private void btnNA_Click(object sender, EventArgs e)
        {
            double[] SendData;
            SendData = new double[3] { 1, 0, 0 };
            Ethercat.Ch.WriteVariable(SendData, "g_SystemState_ControlModeSignal", ProgramBuffer.ACSC_NONE, 0, 0, 0, 2);
            Ethercat.Ch.WriteVariable(1, "g_SystemState_Mode_Change", ProgramBuffer.ACSC_NONE, 0, 0, 0, 0);
        }

        private void btnNT_Click(object sender, EventArgs e)
        {
            double[] SendData;
            SendData = new double[3] { 1, 0, 1 };
            Ethercat.Ch.WriteVariable(SendData, "g_SystemState_ControlModeSignal", ProgramBuffer.ACSC_NONE, 0, 0, 0, 2);
            Ethercat.Ch.WriteVariable(1, "g_SystemState_Mode_Change", ProgramBuffer.ACSC_NONE, 0, 0, 0, 0);
        }

        private void btnTA_Click(object sender, EventArgs e)
        {
            double[] SendData;
            SendData = new double[3] { 0, 1, 0 };
            Ethercat.Ch.WriteVariable(SendData, "g_SystemState_ControlModeSignal", ProgramBuffer.ACSC_NONE, 0, 0, 0, 2);
            Ethercat.Ch.WriteVariable(1, "g_SystemState_Mode_Change", ProgramBuffer.ACSC_NONE, 0, 0, 0, 0);
        }

        private void btnTN_Click(object sender, EventArgs e)
        {
            double[] SendData;
            SendData = new double[3] { 0, 1, 1 };
            Ethercat.Ch.WriteVariable(SendData, "g_SystemState_ControlModeSignal", ProgramBuffer.ACSC_NONE, 0, 0, 0, 2);
            Ethercat.Ch.WriteVariable(1, "g_SystemState_Mode_Change", ProgramBuffer.ACSC_NONE, 0, 0, 0, 0);
        }
    }
}
