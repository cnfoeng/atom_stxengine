﻿namespace AtomUI.ControlForm
{
    partial class FMControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FMControl));
            this.pnlMexaControl = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.btnBypass = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnFuel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnResetStatus = new System.Windows.Forms.Button();
            this.btnStandByStatus = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.btnMEASUREStatus = new System.Windows.Forms.Button();
            this.btnRemoteStatus = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.tbSec = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnEmpa = new System.Windows.Forms.Button();
            this.pnlMexaControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMexaControl
            // 
            this.pnlMexaControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlMexaControl.Controls.Add(this.label3);
            this.pnlMexaControl.Controls.Add(this.btnBypass);
            this.pnlMexaControl.Controls.Add(this.label2);
            this.pnlMexaControl.Controls.Add(this.btnFuel);
            this.pnlMexaControl.Controls.Add(this.label1);
            this.pnlMexaControl.Controls.Add(this.btnResetStatus);
            this.pnlMexaControl.Controls.Add(this.btnStandByStatus);
            this.pnlMexaControl.Controls.Add(this.label25);
            this.pnlMexaControl.Controls.Add(this.label26);
            this.pnlMexaControl.Controls.Add(this.label27);
            this.pnlMexaControl.Controls.Add(this.btnMEASUREStatus);
            this.pnlMexaControl.Controls.Add(this.btnRemoteStatus);
            this.pnlMexaControl.Location = new System.Drawing.Point(12, 42);
            this.pnlMexaControl.Name = "pnlMexaControl";
            this.pnlMexaControl.Size = new System.Drawing.Size(319, 172);
            this.pnlMexaControl.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(212, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 21);
            this.label3.TabIndex = 129;
            this.label3.Text = "BYPASS";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBypass
            // 
            this.btnBypass.BackColor = System.Drawing.Color.Silver;
            this.btnBypass.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBypass.BackgroundImage")));
            this.btnBypass.Enabled = false;
            this.btnBypass.FlatAppearance.BorderSize = 0;
            this.btnBypass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBypass.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnBypass.Location = new System.Drawing.Point(226, 33);
            this.btnBypass.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnBypass.Name = "btnBypass";
            this.btnBypass.Size = new System.Drawing.Size(65, 45);
            this.btnBypass.TabIndex = 128;
            this.btnBypass.UseVisualStyleBackColor = false;
            this.btnBypass.Click += new System.EventHandler(this.btnBypass_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(109, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 21);
            this.label2.TabIndex = 127;
            this.label2.Text = "FUEL FILLING";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnFuel
            // 
            this.btnFuel.BackColor = System.Drawing.Color.Silver;
            this.btnFuel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFuel.BackgroundImage")));
            this.btnFuel.Enabled = false;
            this.btnFuel.FlatAppearance.BorderSize = 0;
            this.btnFuel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFuel.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnFuel.Location = new System.Drawing.Point(123, 33);
            this.btnFuel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnFuel.Name = "btnFuel";
            this.btnFuel.Size = new System.Drawing.Size(65, 45);
            this.btnFuel.TabIndex = 126;
            this.btnFuel.UseVisualStyleBackColor = false;
            this.btnFuel.Click += new System.EventHandler(this.btnFuel_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(212, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 21);
            this.label1.TabIndex = 125;
            this.label1.Text = "RESET";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnResetStatus
            // 
            this.btnResetStatus.BackColor = System.Drawing.Color.Silver;
            this.btnResetStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnResetStatus.BackgroundImage")));
            this.btnResetStatus.Enabled = false;
            this.btnResetStatus.FlatAppearance.BorderSize = 0;
            this.btnResetStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResetStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnResetStatus.Location = new System.Drawing.Point(226, 117);
            this.btnResetStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnResetStatus.Name = "btnResetStatus";
            this.btnResetStatus.Size = new System.Drawing.Size(65, 45);
            this.btnResetStatus.TabIndex = 124;
            this.btnResetStatus.UseVisualStyleBackColor = false;
            this.btnResetStatus.Click += new System.EventHandler(this.btnResetStatus_Click);
            // 
            // btnStandByStatus
            // 
            this.btnStandByStatus.BackgroundImage = global::AtomUI.Properties.Resources.off버튼_회색;
            this.btnStandByStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnStandByStatus.FlatAppearance.BorderSize = 0;
            this.btnStandByStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStandByStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnStandByStatus.Location = new System.Drawing.Point(27, 117);
            this.btnStandByStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnStandByStatus.Name = "btnStandByStatus";
            this.btnStandByStatus.Size = new System.Drawing.Size(65, 45);
            this.btnStandByStatus.TabIndex = 119;
            this.btnStandByStatus.UseVisualStyleBackColor = false;
            this.btnStandByStatus.Click += new System.EventHandler(this.btnStandByStatus_Click);
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label25.Location = new System.Drawing.Point(11, 10);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(94, 21);
            this.label25.TabIndex = 116;
            this.label25.Text = "REMOTE";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label26.Location = new System.Drawing.Point(109, 94);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(94, 21);
            this.label26.TabIndex = 117;
            this.label26.Text = "MEASURE";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label27.Location = new System.Drawing.Point(13, 94);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(94, 21);
            this.label27.TabIndex = 118;
            this.label27.Text = "STANDBY";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMEASUREStatus
            // 
            this.btnMEASUREStatus.BackColor = System.Drawing.Color.Silver;
            this.btnMEASUREStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMEASUREStatus.BackgroundImage")));
            this.btnMEASUREStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMEASUREStatus.Enabled = false;
            this.btnMEASUREStatus.FlatAppearance.BorderSize = 0;
            this.btnMEASUREStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMEASUREStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMEASUREStatus.Location = new System.Drawing.Point(125, 117);
            this.btnMEASUREStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnMEASUREStatus.Name = "btnMEASUREStatus";
            this.btnMEASUREStatus.Size = new System.Drawing.Size(65, 45);
            this.btnMEASUREStatus.TabIndex = 120;
            this.btnMEASUREStatus.UseVisualStyleBackColor = false;
            this.btnMEASUREStatus.Click += new System.EventHandler(this.btnMEASUREStatus_Click);
            // 
            // btnRemoteStatus
            // 
            this.btnRemoteStatus.BackColor = System.Drawing.Color.Silver;
            this.btnRemoteStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRemoteStatus.BackgroundImage")));
            this.btnRemoteStatus.Enabled = false;
            this.btnRemoteStatus.FlatAppearance.BorderSize = 0;
            this.btnRemoteStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemoteStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnRemoteStatus.Location = new System.Drawing.Point(27, 33);
            this.btnRemoteStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRemoteStatus.Name = "btnRemoteStatus";
            this.btnRemoteStatus.Size = new System.Drawing.Size(65, 45);
            this.btnRemoteStatus.TabIndex = 121;
            this.btnRemoteStatus.UseVisualStyleBackColor = false;
            this.btnRemoteStatus.Click += new System.EventHandler(this.btnRemoteStatus_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "Measurement Time";
            // 
            // tbSec
            // 
            this.tbSec.Location = new System.Drawing.Point(137, 15);
            this.tbSec.Name = "tbSec";
            this.tbSec.Size = new System.Drawing.Size(65, 21);
            this.tbSec.TabIndex = 6;
            this.tbSec.Text = "5";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(208, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 12);
            this.label5.TabIndex = 7;
            this.label5.Text = "Sec";
            // 
            // btnEmpa
            // 
            this.btnEmpa.Location = new System.Drawing.Point(241, 15);
            this.btnEmpa.Name = "btnEmpa";
            this.btnEmpa.Size = new System.Drawing.Size(92, 23);
            this.btnEmpa.TabIndex = 8;
            this.btnEmpa.Text = "Set Value";
            this.btnEmpa.UseVisualStyleBackColor = true;
            this.btnEmpa.Click += new System.EventHandler(this.btnEmpa_Click);
            // 
            // FMControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(345, 226);
            this.Controls.Add(this.btnEmpa);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbSec);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pnlMexaControl);
            this.Name = "FMControl";
            this.Text = "FMControl";
            this.Load += new System.EventHandler(this.FMControl_Load);
            this.pnlMexaControl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlMexaControl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnResetStatus;
        private System.Windows.Forms.Button btnStandByStatus;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button btnMEASUREStatus;
        private System.Windows.Forms.Button btnRemoteStatus;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnBypass;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnFuel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbSec;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnEmpa;
    }
}