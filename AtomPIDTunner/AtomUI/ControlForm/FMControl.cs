﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtomUI.ControlForm
{
    public partial class FMControl : Form
    {
        public FMControl()
        {
            InitializeComponent();
        }


        private void FMControl_Load(object sender, EventArgs e)
        {
            ButtonStatusChange(btnStandByStatus, false, false);
            ButtonStatusChange(btnRemoteStatus, false, true);
            ButtonStatusChange(btnResetStatus, false, true);
            ButtonStatusChange(btnMEASUREStatus, false, false);
            ButtonStatusChange(btnBypass, false, false);
            ButtonStatusChange(btnFuel, false, false);

            tbSec.Text = AsynchronousSocketListener.fuelMeter.MeasureTime.ToString();
            timer1.Start();
        }

        private void ButtonStatusChange(Button btn, bool status, bool enable)
        {
            if (status)
            {
                btn.BackgroundImage = global::AtomUI.Properties.Resources.on버튼;
            }
            else
            {
                if (enable)
                    btn.BackgroundImage = global::AtomUI.Properties.Resources.off버튼;
                else
                    btn.BackgroundImage = global::AtomUI.Properties.Resources.off버튼_회색;
            }

            btn.Enabled = enable;
        }

        private void MEXAStatusUpdate()
        {
            if (AsynchronousSocketListener.fuelMeter != null)
            {
                if (AsynchronousSocketListener.fuelMeter.ST_Remote == DAQ.SerialComm.FuelMeter.FMStatus.SREM)
                {
                    ButtonStatusChange(btnRemoteStatus, true, true);
                }
                else
                {
                    ButtonStatusChange(btnRemoteStatus, false, true);
                }
                if (AsynchronousSocketListener.fuelMeter.ST_Function == DAQ.SerialComm.FuelMeter.FMStatus.STBY
                    || AsynchronousSocketListener.fuelMeter.ST_Function == DAQ.SerialComm.FuelMeter.FMStatus.SMES)
                {
                    ButtonStatusChange(btnStandByStatus, true, false);
                }
                else
                {
                    if (AsynchronousSocketListener.fuelMeter.ST_Remote == DAQ.SerialComm.FuelMeter.FMStatus.SREM)
                        ButtonStatusChange(btnStandByStatus, false, true);
                    else
                        ButtonStatusChange(btnStandByStatus, false, false);
                }
                if (AsynchronousSocketListener.fuelMeter.ST_Function == DAQ.SerialComm.FuelMeter.FMStatus.SRES)
                {
                    ButtonStatusChange(btnResetStatus, true, false);
                }
                else
                {
                    ButtonStatusChange(btnResetStatus, false, true);
                }
                if (AsynchronousSocketListener.fuelMeter.ST_Function == DAQ.SerialComm.FuelMeter.FMStatus.SMZV)
                {
                    ButtonStatusChange(btnMEASUREStatus, true, false);
                }
                else
                {
                    if (AsynchronousSocketListener.fuelMeter.ST_Remote == DAQ.SerialComm.FuelMeter.FMStatus.SREM
                        && (AsynchronousSocketListener.fuelMeter.ST_Function == DAQ.SerialComm.FuelMeter.FMStatus.STBY
                        || AsynchronousSocketListener.fuelMeter.ST_Function == DAQ.SerialComm.FuelMeter.FMStatus.SMES))
                        ButtonStatusChange(btnMEASUREStatus, false, true);
                    else
                        ButtonStatusChange(btnMEASUREStatus, false, false);
                }
                if (AsynchronousSocketListener.fuelMeter.ST_FUEL == DAQ.SerialComm.FuelMeter.FMStatus.SAVA)
                {
                    ButtonStatusChange(btnFuel, true, true);
                }
                else
                {
                    ButtonStatusChange(btnFuel, false, true);
                }
                if (AsynchronousSocketListener.fuelMeter.ST_BYPASS == DAQ.SerialComm.FuelMeter.FMStatus.SBVA)
                {
                    ButtonStatusChange(btnBypass, true, true);
                }
                else
                {
                    ButtonStatusChange(btnBypass, false, true);
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            MEXAStatusUpdate();
        }

        private void btnStandByStatus_Click(object sender, EventArgs e)
        {
            AsynchronousSocketListener.fuelMeter.SM_Command_List.Add("STBY");
            ButtonStatusChange(btnResetStatus, true, false);
        }

        private void btnRemoteStatus_Click(object sender, EventArgs e)
        {
            if (AsynchronousSocketListener.fuelMeter.ST_Remote == DAQ.SerialComm.FuelMeter.FMStatus.SREM)
            {
                AsynchronousSocketListener.fuelMeter.SM_Command_List.Add("SMAN");
                ButtonStatusChange(btnResetStatus, true, false);
            } else
            {
                AsynchronousSocketListener.fuelMeter.SM_Command_List.Add("SREM");
                ButtonStatusChange(btnResetStatus, true, false);

            }
        }

        private void btnMEASUREStatus_Click(object sender, EventArgs e)
        {
            AsynchronousSocketListener.fuelMeter.SM_Command_List.Add("SMZV");
            ButtonStatusChange(btnResetStatus, true, false);
        }

        private void btnResetStatus_Click(object sender, EventArgs e)
        {
            AsynchronousSocketListener.fuelMeter.SM_Command_List.Add("SRES");
            ButtonStatusChange(btnResetStatus, true, false);
        }

        private void btnFuel_Click(object sender, EventArgs e)
        {
            if (AsynchronousSocketListener.fuelMeter.ST_FUEL == DAQ.SerialComm.FuelMeter.FMStatus.SAVA)
            {
                AsynchronousSocketListener.fuelMeter.SM_Command_List.Add("SAVZ");
                ButtonStatusChange(btnResetStatus, false, false);
            } else
            {
                AsynchronousSocketListener.fuelMeter.SM_Command_List.Add("SAVA");
                ButtonStatusChange(btnResetStatus, true, false);
            }
        }

        private void btnBypass_Click(object sender, EventArgs e)
        {
            if (AsynchronousSocketListener.fuelMeter.ST_BYPASS == DAQ.SerialComm.FuelMeter.FMStatus.SBVA)
            {
                AsynchronousSocketListener.fuelMeter.SM_Command_List.Add("SBVZ");
                ButtonStatusChange(btnResetStatus, false, false);
            }
            else
            {
                AsynchronousSocketListener.fuelMeter.SM_Command_List.Add("SBVA");
                ButtonStatusChange(btnResetStatus, true, false);
            }
        }

        private void btnEmpa_Click(object sender, EventArgs e)
        {
            int result;
            if (int.TryParse(tbSec.Text, out result))
                AsynchronousSocketListener.fuelMeter.SM_Command_List.Add("EMPA " + tbSec.Text);
            else
                MessageBox.Show("올바른 숫자 형식이 아닙니다.");
        }
    }
}
