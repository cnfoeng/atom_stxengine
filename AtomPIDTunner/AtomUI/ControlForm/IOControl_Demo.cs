﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using AtomUI.Class;
using System.Windows.Forms.DataVisualization.Charting;
using ACS.SPiiPlusNET;

namespace AtomUI.ControlForm
{
    public partial class IOControl_Demo : Form
    {
        //string[] DataLines;
        List<double> ValueLines = new List<double>();

        public IOControl_Demo()
        {
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            string FileName = TargetDir + "\\" + tbFileName.Text;
            if (File.Exists(FileName))
            {
                LoadCSVFile(FileName);
                MessageBox.Show("Data File Loaded Successfully");
                btnRun.Enabled = true;
                btnRun.BackgroundImage = global::AtomUI.Properties.Resources.run버튼;
            }
        }

        public void LoadCSVFile(string FileName)
        {
            try
            {
                StreamReader sr = new StreamReader(new FileStream(FileName, FileMode.Open));
                string[] DataLines = sr.ReadToEnd().Split('\n');
                sr.Close();
                sr.Dispose();

                foreach(string Data in DataLines)
                {
                    ValueLines.Add(double.Parse(Data));
                }
            }
            catch (Exception ex)
            {
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n LoadCSVFile Error!!");
            }

        }

        private void IOControl_Demo_Load(object sender, EventArgs e)
        {
            ChartInit(chFeedBackDyno);
            SeriesInit(chFeedBackDyno);

            FileBoxRefresh();
        }

        private void ChartInit(Chart chart1)
        {
            //X축 데이터 타입
            chart1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(223)))), ((int)(((byte)(193)))));
            chart1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(64)))), ((int)(((byte)(1)))));
            chart1.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chart1.BorderlineWidth = 2;

            chart1.ChartAreas[0].BackColor = System.Drawing.Color.OldLace;
            chart1.ChartAreas[0].BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.ChartAreas[0].BackSecondaryColor = System.Drawing.Color.White;
            chart1.ChartAreas[0].BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;

            //chart1.ChartAreas[0].CursorX.LineColor = Color.Gray;
            //chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            //chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;

            chart1.ChartAreas[0].AxisX.LabelStyle.Format = "#.0";
            chart1.ChartAreas[0].AxisX.IsLabelAutoFit = false;
            chart1.ChartAreas[0].AxisX.LabelStyle.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            chart1.ChartAreas[0].AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].AxisX.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].AxisY.IsLabelAutoFit = false;
            chart1.ChartAreas[0].AxisY.LabelStyle.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            chart1.ChartAreas[0].AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            //chart1.ChartAreas[0].AxisY.Maximum = 5000;
            //chart1.ChartAreas[0].AxisY.Minimum = 1000;

        }

        private void SeriesInit(Chart chart1)
        {
            chart1.Series.Clear();
            chart1.Series.Add("Target");
            chart1.Series.Add("Feedback");

            chart1.Series[0].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            //Y축 데이터 타입
            chart1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;  //분산형 그래프
            chart1.Series[1].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;  //분산형 그래프
            chart1.Series[0].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;

            //차트 타입
            chart1.Series[0].IsXValueIndexed = false;
            chart1.Series[1].IsXValueIndexed = false;
            chart1.Series[0].Color = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(65)))), ((int)(((byte)(140)))), ((int)(((byte)(240)))));
            chart1.Series[1].Color = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(224)))), ((int)(((byte)(64)))), ((int)(((byte)(10)))));

            //chart1.ChartAreas[0].CursorX.Position = 50;
            //chart1.ChartAreas[0].CursorX.LineColor = Color.Gray;
            //chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            //chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
        }

        public string TargetDir = Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\SetupData";
        private void FileBoxRefresh()
        {
            lbFileList.Items.Clear();
            string[] Files = Directory.GetFiles(TargetDir);
            foreach (string file in Files)
            {
                string filename = file.Substring(file.LastIndexOf('\\') + 1, file.Length - file.LastIndexOf('\\') - 1);
                lbFileList.Items.Add(filename);
            }
            tbFileName.Text = (string)lbFileList.Items[0];

        }

        private void lbFileList_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbFileName.Text = lbFileList.SelectedItem.ToString();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            btnRun.Enabled = true;
            btnRun.BackgroundImage = global::AtomUI.Properties.Resources.run버튼;

            btnStop.Enabled = false;
            btnStop.BackgroundImage = global::AtomUI.Properties.Resources.stop버튼_회;

            tmrOutPut.Stop();
            tmrInput.Stop();
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            btnRun.Enabled = false;
            btnRun.BackgroundImage = global::AtomUI.Properties.Resources.run버튼_회;

            btnStop.Enabled = true;
            btnStop.BackgroundImage = global::AtomUI.Properties.Resources.stop버튼;

            tmrOutPut.Interval = int.Parse(tbOutput.Text);
            tmrInput.Interval = int.Parse(tbInput.Text);

            TotalStep = ValueLines.Count();
            CurrentStep = 0;
            CurrentTime = 0;

            lbTotalTime.Text = (TotalStep * tmrOutPut.Interval / 1000.0).ToString();

            tmrOutPut.Start();
            tmrInput.Start();
        }

        int TotalStep = 0;
        int CurrentStep = 0;
        double CurrentTime = 0;
        //int DynoDuration = 10;

        private void tmrOutPut_Tick(object sender, EventArgs e)
        {
            CurrentTime = CurrentStep * tmrOutPut.Interval / 1000.0;
            lbCurrentTime.Text = CurrentTime.ToString();

            Ethercat.Ch.WriteVariable(ValueLines[CurrentStep], "g_eCAT_AO_Data", ProgramBuffer.ACSC_NONE, 0, 0, 0, 0);
            CurrentStep++;

            if (CurrentStep >= ValueLines.Count())
                CurrentStep = 0;
        }

        private void tmrInput_Tick(object sender, EventArgs e)
        {
            //double Target = 0.0;
            //double FeedBack = 0.0;

            //Target = DaqData.sysVarInfo.AI_Variables[0].RealValue;
            //FeedBack = DaqData.sysVarInfo.ECU_Variables[0].RealValue;

            //chFeedBackDyno.Series[0].Points.Add(new DataPoint(CurrentTime, Target));
            //chFeedBackDyno.Series[1].Points.Add(new DataPoint(CurrentTime, FeedBack));

            //if (chFeedBackDyno.Series[0].Points.Count > 0)
            //{
            //    double Max = chFeedBackDyno.Series[0].Points[chFeedBackDyno.Series[0].Points.Count - 1].XValue;
            //    if (Max > DynoDuration)
            //    {
            //        chFeedBackDyno.ChartAreas[0].AxisX.Minimum = Max - DynoDuration;
            //        chFeedBackDyno.ChartAreas[0].AxisX.Maximum = Max;
            //    }
            //    else
            //    {
            //        chFeedBackDyno.ChartAreas[0].AxisX.Minimum = 0;
            //        chFeedBackDyno.ChartAreas[0].AxisX.Maximum = DynoDuration;
            //    }
            //    chFeedBackDyno.Invalidate();
            //}
        }
    }
}
