﻿namespace AtomUI.ControlForm
{
    partial class Mexa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mexa));
            this.pnlMexaControl = new System.Windows.Forms.Panel();
            this.btnMexaPURGEStatus = new System.Windows.Forms.Button();
            this.btnMexaCPUStatus = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.btnMexaMEASUREStatus = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.btnMexaRESETStatus = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pnlMexaControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMexaControl
            // 
            this.pnlMexaControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlMexaControl.Controls.Add(this.btnMexaPURGEStatus);
            this.pnlMexaControl.Controls.Add(this.btnMexaCPUStatus);
            this.pnlMexaControl.Controls.Add(this.label25);
            this.pnlMexaControl.Controls.Add(this.label26);
            this.pnlMexaControl.Controls.Add(this.label27);
            this.pnlMexaControl.Controls.Add(this.btnMexaMEASUREStatus);
            this.pnlMexaControl.Controls.Add(this.label28);
            this.pnlMexaControl.Controls.Add(this.btnMexaRESETStatus);
            this.pnlMexaControl.Location = new System.Drawing.Point(37, 44);
            this.pnlMexaControl.Name = "pnlMexaControl";
            this.pnlMexaControl.Size = new System.Drawing.Size(211, 172);
            this.pnlMexaControl.TabIndex = 2;
            // 
            // btnMexaPURGEStatus
            // 
            this.btnMexaPURGEStatus.BackColor = System.Drawing.Color.Silver;
            this.btnMexaPURGEStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMexaPURGEStatus.BackgroundImage")));
            this.btnMexaPURGEStatus.Enabled = false;
            this.btnMexaPURGEStatus.FlatAppearance.BorderSize = 0;
            this.btnMexaPURGEStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMexaPURGEStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMexaPURGEStatus.Location = new System.Drawing.Point(125, 113);
            this.btnMexaPURGEStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnMexaPURGEStatus.Name = "btnMexaPURGEStatus";
            this.btnMexaPURGEStatus.Size = new System.Drawing.Size(65, 45);
            this.btnMexaPURGEStatus.TabIndex = 123;
            this.btnMexaPURGEStatus.UseVisualStyleBackColor = false;
            this.btnMexaPURGEStatus.Click += new System.EventHandler(this.btnMexaPURGEStatus_Click);
            // 
            // btnMexaCPUStatus
            // 
            this.btnMexaCPUStatus.BackgroundImage = global::AtomUI.Properties.Resources.off버튼_회색;
            this.btnMexaCPUStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMexaCPUStatus.FlatAppearance.BorderSize = 0;
            this.btnMexaCPUStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMexaCPUStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMexaCPUStatus.Location = new System.Drawing.Point(20, 33);
            this.btnMexaCPUStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnMexaCPUStatus.Name = "btnMexaCPUStatus";
            this.btnMexaCPUStatus.Size = new System.Drawing.Size(65, 45);
            this.btnMexaCPUStatus.TabIndex = 119;
            this.btnMexaCPUStatus.UseVisualStyleBackColor = false;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label25.Location = new System.Drawing.Point(5, 10);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(94, 21);
            this.label25.TabIndex = 116;
            this.label25.Text = "CPU";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label26.Location = new System.Drawing.Point(5, 90);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(94, 21);
            this.label26.TabIndex = 117;
            this.label26.Text = "MEASURE";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label27.Location = new System.Drawing.Point(111, 10);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(94, 21);
            this.label27.TabIndex = 118;
            this.label27.Text = "RESET";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMexaMEASUREStatus
            // 
            this.btnMexaMEASUREStatus.BackColor = System.Drawing.Color.Silver;
            this.btnMexaMEASUREStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMexaMEASUREStatus.BackgroundImage")));
            this.btnMexaMEASUREStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMexaMEASUREStatus.Enabled = false;
            this.btnMexaMEASUREStatus.FlatAppearance.BorderSize = 0;
            this.btnMexaMEASUREStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMexaMEASUREStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMexaMEASUREStatus.Location = new System.Drawing.Point(19, 113);
            this.btnMexaMEASUREStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnMexaMEASUREStatus.Name = "btnMexaMEASUREStatus";
            this.btnMexaMEASUREStatus.Size = new System.Drawing.Size(65, 45);
            this.btnMexaMEASUREStatus.TabIndex = 120;
            this.btnMexaMEASUREStatus.UseVisualStyleBackColor = false;
            this.btnMexaMEASUREStatus.Click += new System.EventHandler(this.btnMexaMEASUREStatus_Click);
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label28.Location = new System.Drawing.Point(111, 90);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(94, 21);
            this.label28.TabIndex = 122;
            this.label28.Text = "PURGE";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMexaRESETStatus
            // 
            this.btnMexaRESETStatus.BackColor = System.Drawing.Color.Silver;
            this.btnMexaRESETStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMexaRESETStatus.BackgroundImage")));
            this.btnMexaRESETStatus.Enabled = false;
            this.btnMexaRESETStatus.FlatAppearance.BorderSize = 0;
            this.btnMexaRESETStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMexaRESETStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMexaRESETStatus.Location = new System.Drawing.Point(125, 33);
            this.btnMexaRESETStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnMexaRESETStatus.Name = "btnMexaRESETStatus";
            this.btnMexaRESETStatus.Size = new System.Drawing.Size(65, 45);
            this.btnMexaRESETStatus.TabIndex = 121;
            this.btnMexaRESETStatus.UseVisualStyleBackColor = false;
            this.btnMexaRESETStatus.Click += new System.EventHandler(this.btnMexaRESETStatus_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Mexa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.pnlMexaControl);
            this.Name = "Mexa";
            this.Text = "Mexa";
            this.pnlMexaControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMexaControl;
        private System.Windows.Forms.Button btnMexaPURGEStatus;
        private System.Windows.Forms.Button btnMexaCPUStatus;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button btnMexaMEASUREStatus;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button btnMexaRESETStatus;
        private System.Windows.Forms.Timer timer1;
    }
}