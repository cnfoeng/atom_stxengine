﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtomUI.ControlForm
{
    public partial class Mexa : Form
    {
        public Mexa()
        {
            InitializeComponent();

            timer1.Start();
        }

        private void MEXAStatusUpdate()
        {

            string CPUOnOffCheck = "";
            string OperationConditionLine1 = "";

            if (DAQ_GPIB.GPIBCom.IsConnected)
            {
                CPUOnOffCheck = DAQ_GPIB.GPIBCom.CPUOnOffCheck;
                OperationConditionLine1 = DAQ_GPIB.GPIBCom.OperationConditionLine1;
            }
            else if (DAQ_GPIB.VisaCom.IsConnected)
            {
                CPUOnOffCheck = DAQ_GPIB.VisaCom.CPUOnOffCheck;
                OperationConditionLine1 = DAQ_GPIB.VisaCom.OperationConditionLine1;
            }

            if (CPUOnOffCheck == "01")
            {
                ButtonStatusChange(btnMexaCPUStatus, true, true);

                if (OperationConditionLine1 == "01")
                    ButtonStatusChange(btnMexaRESETStatus, true, true);
                else
                    ButtonStatusChange(btnMexaRESETStatus, false, true);

                if (OperationConditionLine1 == "05")
                    ButtonStatusChange(btnMexaMEASUREStatus, true, true);
                else
                    ButtonStatusChange(btnMexaMEASUREStatus, false, true);

                if (OperationConditionLine1 == "06")
                    ButtonStatusChange(btnMexaPURGEStatus, true, true);
                else
                    ButtonStatusChange(btnMexaPURGEStatus, false, true);
            }
            else
            {
                ButtonStatusChange(btnMexaCPUStatus, false, true);
                ButtonStatusChange(btnMexaRESETStatus, false, true);
                ButtonStatusChange(btnMexaMEASUREStatus, false, true);
                ButtonStatusChange(btnMexaPURGEStatus, false, true);
            }
        }

        private void SysbtnControl(bool Enable)
        {
            ButtonStatusChange(btnMexaCPUStatus, false, Enable);
            ButtonStatusChange(btnMexaRESETStatus, false, Enable);
            ButtonStatusChange(btnMexaMEASUREStatus, false, Enable);
            ButtonStatusChange(btnMexaPURGEStatus, false, Enable);

            //ButtonStatusChange(btnMexaNoOP, false, Enable);
            //ButtonStatusChange(btnMexaRESET, false, Enable);
            //ButtonStatusChange(btnMexaMEASURE, false, Enable);
            //ButtonStatusChange(btnMexaPURGE, false, Enable);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            MEXAStatusUpdate();
        }

        private void ButtonStatusChange(Button btn, bool status, bool enable)
        {
            if (status)
            {
                btn.BackgroundImage = global::AtomUI.Properties.Resources.on버튼;
            }
            else
            {
                if (enable)
                    btn.BackgroundImage = global::AtomUI.Properties.Resources.off버튼;
                else
                    btn.BackgroundImage = global::AtomUI.Properties.Resources.off버튼_회색;
            }

            btn.Enabled = enable;
        }

        private void btnMexaRESETStatus_Click(object sender, EventArgs e)
        {
            if (DAQ_GPIB.GPIBCom.IsConnected)
            {
                AsynchronousSocketListener.gpibcom.WriteAOP_Reset();
            }
            else if (DAQ_GPIB.VisaCom.IsConnected)
            {
                AsynchronousSocketListener.visacom.WriteAOP_Reset();
            }
        }

        private void btnMexaMEASUREStatus_Click(object sender, EventArgs e)
        {
            if (DAQ_GPIB.GPIBCom.IsConnected)
            {
                AsynchronousSocketListener.gpibcom.WriteAOP_Measure();
            }
            else if (DAQ_GPIB.VisaCom.IsConnected)
            {
                AsynchronousSocketListener.visacom.WriteAOP_Measure();
            }
        }

        private void btnMexaPURGEStatus_Click(object sender, EventArgs e)
        {
            if (DAQ_GPIB.GPIBCom.IsConnected)
            {
                AsynchronousSocketListener.gpibcom.WriteAOP_Purge();
            }
            else if (DAQ_GPIB.VisaCom.IsConnected)
            {
                AsynchronousSocketListener.visacom.WriteAOP_Purge();
            }
        }
    }
}
