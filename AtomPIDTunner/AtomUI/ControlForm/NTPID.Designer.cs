﻿namespace AtomUI
{
    partial class NTPID
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSetRange_ = new System.Windows.Forms.Button();
            this.tbUpper_ = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbLower_ = new System.Windows.Forms.TextBox();
            this.cbData_ = new System.Windows.Forms.CheckBox();
            this.GraphDisplay_1 = new GraphLib.PlotterDisplayEx();
            this.btnSetRange = new System.Windows.Forms.Button();
            this.tbUpper = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbLower = new System.Windows.Forms.TextBox();
            this.cbData = new System.Windows.Forms.CheckBox();
            this.tbControl = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.GraphDisplay = new GraphLib.PlotterDisplayEx();
            this.button1 = new System.Windows.Forms.Button();
            this.tbDvalue = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbIvalue = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbPvalue = new System.Windows.Forms.TextBox();
            this.PortLb = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbEngineIntrval = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnSetRange2_ = new System.Windows.Forms.Button();
            this.tbUpper2_ = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbLower2_ = new System.Windows.Forms.TextBox();
            this.cbData2_ = new System.Windows.Forms.CheckBox();
            this.GraphDisplay2_1 = new GraphLib.PlotterDisplayEx();
            this.label9 = new System.Windows.Forms.Label();
            this.btnSetRange2 = new System.Windows.Forms.Button();
            this.tbUpper2 = new System.Windows.Forms.TextBox();
            this.tbLower2 = new System.Windows.Forms.TextBox();
            this.cbData2 = new System.Windows.Forms.CheckBox();
            this.tbControl2 = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.GraphDisplay2 = new GraphLib.PlotterDisplayEx();
            this.button2 = new System.Windows.Forms.Button();
            this.tbEngineDValue = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbEngineIValue = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbEnginePValue = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tbDynoIntrval = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbDynoIntrval);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.btnSetRange_);
            this.groupBox1.Controls.Add(this.tbUpper_);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tbLower_);
            this.groupBox1.Controls.Add(this.cbData_);
            this.groupBox1.Controls.Add(this.GraphDisplay_1);
            this.groupBox1.Controls.Add(this.btnSetRange);
            this.groupBox1.Controls.Add(this.tbUpper);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.tbLower);
            this.groupBox1.Controls.Add(this.cbData);
            this.groupBox1.Controls.Add(this.tbControl);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.GraphDisplay);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.tbDvalue);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tbIvalue);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbPvalue);
            this.groupBox1.Controls.Add(this.PortLb);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1554, 462);
            this.groupBox1.TabIndex = 56;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dyno";
            // 
            // btnSetRange_
            // 
            this.btnSetRange_.Location = new System.Drawing.Point(1270, 16);
            this.btnSetRange_.Name = "btnSetRange_";
            this.btnSetRange_.Size = new System.Drawing.Size(93, 23);
            this.btnSetRange_.TabIndex = 83;
            this.btnSetRange_.Text = "Y Range";
            this.btnSetRange_.UseVisualStyleBackColor = true;
            this.btnSetRange_.Click += new System.EventHandler(this.btnSetRange__Click);
            // 
            // tbUpper_
            // 
            this.tbUpper_.Location = new System.Drawing.Point(1476, 15);
            this.tbUpper_.Name = "tbUpper_";
            this.tbUpper_.Size = new System.Drawing.Size(70, 25);
            this.tbUpper_.TabIndex = 82;
            this.tbUpper_.Text = "2";
            this.tbUpper_.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(1452, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(18, 15);
            this.label10.TabIndex = 81;
            this.label10.Text = "~";
            // 
            // tbLower_
            // 
            this.tbLower_.Location = new System.Drawing.Point(1373, 15);
            this.tbLower_.Name = "tbLower_";
            this.tbLower_.Size = new System.Drawing.Size(72, 25);
            this.tbLower_.TabIndex = 80;
            this.tbLower_.Text = "-2";
            this.tbLower_.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cbData_
            // 
            this.cbData_.AutoSize = true;
            this.cbData_.Checked = true;
            this.cbData_.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbData_.Location = new System.Drawing.Point(1106, 17);
            this.cbData_.Name = "cbData_";
            this.cbData_.Size = new System.Drawing.Size(156, 19);
            this.cbData_.TabIndex = 79;
            this.cbData_.Text = "Y2 Axis Auto Scale";
            this.cbData_.UseVisualStyleBackColor = true;
            this.cbData_.CheckedChanged += new System.EventHandler(this.cbData__CheckedChanged);
            // 
            // GraphDisplay_1
            // 
            this.GraphDisplay_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GraphDisplay_1.BackColor = System.Drawing.Color.Transparent;
            this.GraphDisplay_1.BackgroundColorBot = System.Drawing.Color.White;
            this.GraphDisplay_1.BackgroundColorTop = System.Drawing.Color.White;
            this.GraphDisplay_1.DashedGridColor = System.Drawing.Color.DarkGray;
            this.GraphDisplay_1.DoubleBuffering = true;
            this.GraphDisplay_1.Location = new System.Drawing.Point(580, 286);
            this.GraphDisplay_1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.GraphDisplay_1.Name = "GraphDisplay_1";
            this.GraphDisplay_1.PlaySpeed = 0.5F;
            this.GraphDisplay_1.Size = new System.Drawing.Size(967, 170);
            this.GraphDisplay_1.SolidGridColor = System.Drawing.Color.DarkGray;
            this.GraphDisplay_1.TabIndex = 78;
            // 
            // btnSetRange
            // 
            this.btnSetRange.Location = new System.Drawing.Point(730, 16);
            this.btnSetRange.Name = "btnSetRange";
            this.btnSetRange.Size = new System.Drawing.Size(93, 23);
            this.btnSetRange.TabIndex = 77;
            this.btnSetRange.Text = "Y Range";
            this.btnSetRange.UseVisualStyleBackColor = true;
            this.btnSetRange.Click += new System.EventHandler(this.btnSetRange_Click);
            // 
            // tbUpper
            // 
            this.tbUpper.Location = new System.Drawing.Point(936, 15);
            this.tbUpper.Name = "tbUpper";
            this.tbUpper.Size = new System.Drawing.Size(70, 25);
            this.tbUpper.TabIndex = 76;
            this.tbUpper.Text = "2";
            this.tbUpper.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(912, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(18, 15);
            this.label8.TabIndex = 75;
            this.label8.Text = "~";
            // 
            // tbLower
            // 
            this.tbLower.Location = new System.Drawing.Point(833, 15);
            this.tbLower.Name = "tbLower";
            this.tbLower.Size = new System.Drawing.Size(72, 25);
            this.tbLower.TabIndex = 74;
            this.tbLower.Text = "-2";
            this.tbLower.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cbData
            // 
            this.cbData.AutoSize = true;
            this.cbData.Checked = true;
            this.cbData.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbData.Location = new System.Drawing.Point(580, 17);
            this.cbData.Name = "cbData";
            this.cbData.Size = new System.Drawing.Size(148, 19);
            this.cbData.TabIndex = 73;
            this.cbData.Text = "Y Axis Auto Scale";
            this.cbData.UseVisualStyleBackColor = true;
            this.cbData.CheckedChanged += new System.EventHandler(this.cbData_CheckedChanged);
            // 
            // tbControl
            // 
            this.tbControl.Location = new System.Drawing.Point(223, 46);
            this.tbControl.Name = "tbControl";
            this.tbControl.Size = new System.Drawing.Size(350, 410);
            this.tbControl.TabIndex = 66;
            this.tbControl.Text = "";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(232, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 26);
            this.label5.TabIndex = 65;
            this.label5.Text = "Control Value";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // GraphDisplay
            // 
            this.GraphDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GraphDisplay.BackColor = System.Drawing.Color.Transparent;
            this.GraphDisplay.BackgroundColorBot = System.Drawing.Color.White;
            this.GraphDisplay.BackgroundColorTop = System.Drawing.Color.White;
            this.GraphDisplay.DashedGridColor = System.Drawing.Color.DarkGray;
            this.GraphDisplay.DoubleBuffering = true;
            this.GraphDisplay.Location = new System.Drawing.Point(580, 46);
            this.GraphDisplay.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.GraphDisplay.Name = "GraphDisplay";
            this.GraphDisplay.PlaySpeed = 0.5F;
            this.GraphDisplay.Size = new System.Drawing.Size(967, 234);
            this.GraphDisplay.SolidGridColor = System.Drawing.Color.DarkGray;
            this.GraphDisplay.TabIndex = 64;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(36, 240);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(134, 40);
            this.button1.TabIndex = 63;
            this.button1.Text = "PID 변수 변경";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbDvalue
            // 
            this.tbDvalue.Location = new System.Drawing.Point(73, 133);
            this.tbDvalue.Name = "tbDvalue";
            this.tbDvalue.Size = new System.Drawing.Size(97, 25);
            this.tbDvalue.TabIndex = 60;
            this.tbDvalue.Text = "10.0.0.100";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(33, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 38);
            this.label3.TabIndex = 58;
            this.label3.Text = "D";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbIvalue
            // 
            this.tbIvalue.Location = new System.Drawing.Point(73, 89);
            this.tbIvalue.Name = "tbIvalue";
            this.tbIvalue.Size = new System.Drawing.Size(97, 25);
            this.tbIvalue.TabIndex = 61;
            this.tbIvalue.Text = "10.0.0.100";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(33, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 38);
            this.label2.TabIndex = 59;
            this.label2.Text = "I";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbPvalue
            // 
            this.tbPvalue.Location = new System.Drawing.Point(73, 46);
            this.tbPvalue.Name = "tbPvalue";
            this.tbPvalue.Size = new System.Drawing.Size(97, 25);
            this.tbPvalue.TabIndex = 57;
            this.tbPvalue.Text = "10.0.0.100";
            // 
            // PortLb
            // 
            this.PortLb.Location = new System.Drawing.Point(33, 37);
            this.PortLb.Name = "PortLb";
            this.PortLb.Size = new System.Drawing.Size(107, 38);
            this.PortLb.TabIndex = 56;
            this.PortLb.Text = "P";
            this.PortLb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbEngineIntrval);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.btnSetRange2_);
            this.groupBox2.Controls.Add(this.tbUpper2_);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.tbLower2_);
            this.groupBox2.Controls.Add(this.cbData2_);
            this.groupBox2.Controls.Add(this.GraphDisplay2_1);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.btnSetRange2);
            this.groupBox2.Controls.Add(this.tbUpper2);
            this.groupBox2.Controls.Add(this.tbLower2);
            this.groupBox2.Controls.Add(this.cbData2);
            this.groupBox2.Controls.Add(this.tbControl2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.GraphDisplay2);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.tbEngineDValue);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.tbEngineIValue);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.tbEnginePValue);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(12, 492);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1554, 462);
            this.groupBox2.TabIndex = 67;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Engine";
            // 
            // tbEngineIntrval
            // 
            this.tbEngineIntrval.Location = new System.Drawing.Point(95, 171);
            this.tbEngineIntrval.Name = "tbEngineIntrval";
            this.tbEngineIntrval.Size = new System.Drawing.Size(75, 25);
            this.tbEngineIntrval.TabIndex = 90;
            this.tbEngineIntrval.Text = "10.0.0.100";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(33, 162);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(107, 38);
            this.label12.TabIndex = 89;
            this.label12.Text = "Interval";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnSetRange2_
            // 
            this.btnSetRange2_.Location = new System.Drawing.Point(1270, 16);
            this.btnSetRange2_.Name = "btnSetRange2_";
            this.btnSetRange2_.Size = new System.Drawing.Size(93, 23);
            this.btnSetRange2_.TabIndex = 88;
            this.btnSetRange2_.Text = "Y Range";
            this.btnSetRange2_.UseVisualStyleBackColor = true;
            this.btnSetRange2_.Click += new System.EventHandler(this.btnSetRange2__Click);
            // 
            // tbUpper2_
            // 
            this.tbUpper2_.Location = new System.Drawing.Point(1476, 15);
            this.tbUpper2_.Name = "tbUpper2_";
            this.tbUpper2_.Size = new System.Drawing.Size(70, 25);
            this.tbUpper2_.TabIndex = 87;
            this.tbUpper2_.Text = "2";
            this.tbUpper2_.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1452, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(18, 15);
            this.label11.TabIndex = 86;
            this.label11.Text = "~";
            // 
            // tbLower2_
            // 
            this.tbLower2_.Location = new System.Drawing.Point(1373, 15);
            this.tbLower2_.Name = "tbLower2_";
            this.tbLower2_.Size = new System.Drawing.Size(72, 25);
            this.tbLower2_.TabIndex = 85;
            this.tbLower2_.Text = "-2";
            this.tbLower2_.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cbData2_
            // 
            this.cbData2_.AutoSize = true;
            this.cbData2_.Checked = true;
            this.cbData2_.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbData2_.Location = new System.Drawing.Point(1106, 17);
            this.cbData2_.Name = "cbData2_";
            this.cbData2_.Size = new System.Drawing.Size(156, 19);
            this.cbData2_.TabIndex = 84;
            this.cbData2_.Text = "Y2 Axis Auto Scale";
            this.cbData2_.UseVisualStyleBackColor = true;
            this.cbData2_.CheckedChanged += new System.EventHandler(this.cbData2__CheckedChanged);
            // 
            // GraphDisplay2_1
            // 
            this.GraphDisplay2_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GraphDisplay2_1.BackColor = System.Drawing.Color.Transparent;
            this.GraphDisplay2_1.BackgroundColorBot = System.Drawing.Color.White;
            this.GraphDisplay2_1.BackgroundColorTop = System.Drawing.Color.White;
            this.GraphDisplay2_1.DashedGridColor = System.Drawing.Color.DarkGray;
            this.GraphDisplay2_1.DoubleBuffering = true;
            this.GraphDisplay2_1.Location = new System.Drawing.Point(580, 286);
            this.GraphDisplay2_1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.GraphDisplay2_1.Name = "GraphDisplay2_1";
            this.GraphDisplay2_1.PlaySpeed = 0.5F;
            this.GraphDisplay2_1.Size = new System.Drawing.Size(967, 170);
            this.GraphDisplay2_1.SolidGridColor = System.Drawing.Color.DarkGray;
            this.GraphDisplay2_1.TabIndex = 79;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(912, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(18, 15);
            this.label9.TabIndex = 82;
            this.label9.Text = "~";
            // 
            // btnSetRange2
            // 
            this.btnSetRange2.Location = new System.Drawing.Point(730, 16);
            this.btnSetRange2.Name = "btnSetRange2";
            this.btnSetRange2.Size = new System.Drawing.Size(93, 23);
            this.btnSetRange2.TabIndex = 81;
            this.btnSetRange2.Text = "Y Range";
            this.btnSetRange2.UseVisualStyleBackColor = true;
            this.btnSetRange2.Click += new System.EventHandler(this.btnSetRange2_Click);
            // 
            // tbUpper2
            // 
            this.tbUpper2.Location = new System.Drawing.Point(936, 15);
            this.tbUpper2.Name = "tbUpper2";
            this.tbUpper2.Size = new System.Drawing.Size(70, 25);
            this.tbUpper2.TabIndex = 80;
            this.tbUpper2.Text = "2";
            this.tbUpper2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbLower2
            // 
            this.tbLower2.Location = new System.Drawing.Point(833, 15);
            this.tbLower2.Name = "tbLower2";
            this.tbLower2.Size = new System.Drawing.Size(72, 25);
            this.tbLower2.TabIndex = 79;
            this.tbLower2.Text = "-2";
            this.tbLower2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cbData2
            // 
            this.cbData2.AutoSize = true;
            this.cbData2.Checked = true;
            this.cbData2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbData2.Location = new System.Drawing.Point(580, 17);
            this.cbData2.Name = "cbData2";
            this.cbData2.Size = new System.Drawing.Size(148, 19);
            this.cbData2.TabIndex = 78;
            this.cbData2.Text = "Y Axis Auto Scale";
            this.cbData2.UseVisualStyleBackColor = true;
            this.cbData2.CheckedChanged += new System.EventHandler(this.cbData2_CheckedChanged);
            // 
            // tbControl2
            // 
            this.tbControl2.Location = new System.Drawing.Point(223, 46);
            this.tbControl2.Name = "tbControl2";
            this.tbControl2.Size = new System.Drawing.Size(350, 410);
            this.tbControl2.TabIndex = 66;
            this.tbControl2.Text = "";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(232, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 26);
            this.label1.TabIndex = 65;
            this.label1.Text = "Control Value";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // GraphDisplay2
            // 
            this.GraphDisplay2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GraphDisplay2.BackColor = System.Drawing.Color.Transparent;
            this.GraphDisplay2.BackgroundColorBot = System.Drawing.Color.White;
            this.GraphDisplay2.BackgroundColorTop = System.Drawing.Color.White;
            this.GraphDisplay2.DashedGridColor = System.Drawing.Color.DarkGray;
            this.GraphDisplay2.DoubleBuffering = true;
            this.GraphDisplay2.Location = new System.Drawing.Point(580, 46);
            this.GraphDisplay2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.GraphDisplay2.Name = "GraphDisplay2";
            this.GraphDisplay2.PlaySpeed = 0.5F;
            this.GraphDisplay2.Size = new System.Drawing.Size(967, 234);
            this.GraphDisplay2.SolidGridColor = System.Drawing.Color.DarkGray;
            this.GraphDisplay2.TabIndex = 64;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(36, 229);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(134, 40);
            this.button2.TabIndex = 63;
            this.button2.Text = "PID 변수 변경";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // tbEngineDValue
            // 
            this.tbEngineDValue.Location = new System.Drawing.Point(73, 133);
            this.tbEngineDValue.Name = "tbEngineDValue";
            this.tbEngineDValue.Size = new System.Drawing.Size(97, 25);
            this.tbEngineDValue.TabIndex = 60;
            this.tbEngineDValue.Text = "10.0.0.100";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(33, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 38);
            this.label4.TabIndex = 58;
            this.label4.Text = "D";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbEngineIValue
            // 
            this.tbEngineIValue.Location = new System.Drawing.Point(73, 89);
            this.tbEngineIValue.Name = "tbEngineIValue";
            this.tbEngineIValue.Size = new System.Drawing.Size(97, 25);
            this.tbEngineIValue.TabIndex = 61;
            this.tbEngineIValue.Text = "10.0.0.100";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(33, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 38);
            this.label6.TabIndex = 59;
            this.label6.Text = "I";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbEnginePValue
            // 
            this.tbEnginePValue.Location = new System.Drawing.Point(73, 46);
            this.tbEnginePValue.Name = "tbEnginePValue";
            this.tbEnginePValue.Size = new System.Drawing.Size(97, 25);
            this.tbEnginePValue.TabIndex = 57;
            this.tbEnginePValue.Text = "10.0.0.100";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(33, 37);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 38);
            this.label7.TabIndex = 56;
            this.label7.Text = "P";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tbDynoIntrval
            // 
            this.tbDynoIntrval.Location = new System.Drawing.Point(95, 183);
            this.tbDynoIntrval.Name = "tbDynoIntrval";
            this.tbDynoIntrval.Size = new System.Drawing.Size(75, 25);
            this.tbDynoIntrval.TabIndex = 92;
            this.tbDynoIntrval.Text = "10.0.0.100";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(33, 174);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 38);
            this.label13.TabIndex = 91;
            this.label13.Text = "Interval";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // NTPID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1578, 967);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "NTPID";
            this.Text = "NTPID";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NTPID_FormClosing);
            this.Load += new System.EventHandler(this.NTPID_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox tbControl;
        private System.Windows.Forms.Label label5;
        private GraphLib.PlotterDisplayEx GraphDisplay;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbDvalue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbIvalue;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbPvalue;
        private System.Windows.Forms.Label PortLb;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox tbControl2;
        private System.Windows.Forms.Label label1;
        private GraphLib.PlotterDisplayEx GraphDisplay2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox tbEngineDValue;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbEngineIValue;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbEnginePValue;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnSetRange;
        private System.Windows.Forms.TextBox tbUpper;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbLower;
        private System.Windows.Forms.CheckBox cbData;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnSetRange2;
        private System.Windows.Forms.TextBox tbUpper2;
        private System.Windows.Forms.TextBox tbLower2;
        private System.Windows.Forms.CheckBox cbData2;
        private GraphLib.PlotterDisplayEx GraphDisplay_1;
        private GraphLib.PlotterDisplayEx GraphDisplay2_1;
        private System.Windows.Forms.Button btnSetRange_;
        private System.Windows.Forms.TextBox tbUpper_;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbLower_;
        private System.Windows.Forms.CheckBox cbData_;
        private System.Windows.Forms.Button btnSetRange2_;
        private System.Windows.Forms.TextBox tbUpper2_;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbLower2_;
        private System.Windows.Forms.CheckBox cbData2_;
        private System.Windows.Forms.TextBox tbEngineIntrval;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbDynoIntrval;
        private System.Windows.Forms.Label label13;
    }
}