﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ACS.SPiiPlusNET;
using System.Runtime.InteropServices; //for COMException class
using GraphLib;

namespace AtomUI
{
    public partial class NTPID : Form
    {
        public Api Ch; //  For communcating between the UMD and the Host Application. All communication commands are methods within the AsyncChannel Class.
        private int NumGraphs = 3;
        private int Index = 0;
        cPoint[] data_1 = new cPoint[512];
        cPoint[] data_2 = new cPoint[512];
        cPoint[] data_3 = new cPoint[512];
        cPoint[] data_4 = new cPoint[512];
        cPoint[] data_5 = new cPoint[512];
        cPoint[] data2_1 = new cPoint[512];
        cPoint[] data2_2 = new cPoint[512];
        cPoint[] data2_3 = new cPoint[512];
        cPoint[] data2_4 = new cPoint[512];
        cPoint[] data2_5 = new cPoint[512];
        cPoint[] data2_6 = new cPoint[512];

        public NTPID()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //try
            //{
                object Result;

                String Str = "";

            //Reading whole Variable, if the Variable is two-dimensional array
            if (Ch.IsConnected)
            {
                Result = Ch.ReadVariable("g_Dyno_PID_Process_ValList", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);
                Str = "";
                Str = Str + ControlToString((double[])Result);
                Str = Str + "\n\n";
                tbControl.Text = Str;

                data_1[Index].x = Index;
                data_1[Index].y = (float)((double[])Result)[10];
                data_2[Index].x = Index;
                data_2[Index].y = (float)((double[])Result)[15];
                data_3[Index].x = Index;
                data_3[Index].y = (float)((double[])Result)[16];
                data_4[Index].x = Index;
                data_4[Index].y = (float)((double[])Result)[21];
                data_5[Index].x = Index;
                data_5[Index].y = (float)((double[])Result)[22];

                //Reading whole Variable, if the Variable is two-dimensional array
                Result = Ch.ReadVariable("g_Eng_PID_Process_ValList", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);
                Str = "";
                Str = Str + ControlToString((double[])Result);
                Object Result2 = Ch.ReadVariable("g_Eng_PidControl_StateMachine", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);
                tbControl2.Text = Str + "StateMachine =\t\t" + ((int)Result2).ToString() + "\n\n";

                data2_1[Index].x = Index;
                data2_1[Index].y = (float)((double[])Result)[10];
                data2_2[Index].x = Index;
                data2_2[Index].y = (float)((double[])Result)[15];
                data2_3[Index].x = Index;
                data2_3[Index].y = (float)((double[])Result)[16];
                data2_4[Index].x = Index;
                data2_4[Index].y = (float)((double[])Result)[21];
                data2_5[Index].x = Index;
                data2_5[Index].y = (float)((double[])Result)[22];
                data2_6[Index].x = Index;
                data2_6[Index].y = (float)((double[])Result)[23];

                Index++;
                GraphDisplay.DataSources[0].Length = Index;
                GraphDisplay.DataSources[1].Length = Index;
                GraphDisplay.DataSources[2].Length = Index;
                GraphDisplay2.DataSources[0].Length = Index;
                GraphDisplay2.DataSources[1].Length = Index;
                GraphDisplay2.DataSources[2].Length = Index;
                GraphDisplay_1.DataSources[0].Length = Index;
                GraphDisplay_1.DataSources[1].Length = Index;
                GraphDisplay2_1.DataSources[0].Length = Index;
                GraphDisplay2_1.DataSources[1].Length = Index;
                GraphDisplay2_1.DataSources[2].Length = Index;
                cPoint[] gdata_1 = GraphDisplay.DataSources[0].Samples;
                cPoint[] gdata_2 = GraphDisplay.DataSources[1].Samples;
                cPoint[] gdata_3 = GraphDisplay.DataSources[2].Samples;
                cPoint[] gdata_4 = GraphDisplay_1.DataSources[0].Samples;
                cPoint[] gdata_5 = GraphDisplay_1.DataSources[1].Samples;
                cPoint[] gdata2_1 = GraphDisplay2.DataSources[0].Samples;
                cPoint[] gdata2_2 = GraphDisplay2.DataSources[1].Samples;
                cPoint[] gdata2_3 = GraphDisplay2.DataSources[2].Samples;
                cPoint[] gdata2_4 = GraphDisplay2_1.DataSources[0].Samples;
                cPoint[] gdata2_5 = GraphDisplay2_1.DataSources[1].Samples;
                cPoint[] gdata2_6 = GraphDisplay2_1.DataSources[2].Samples;
                System.Array.Copy(data_1, gdata_1, Index);
                System.Array.Copy(data_2, gdata_2, Index);
                System.Array.Copy(data_3, gdata_3, Index);
                System.Array.Copy(data_4, gdata_4, Index);
                System.Array.Copy(data_5, gdata_5, Index);
                System.Array.Copy(data2_1, gdata2_1, Index);
                System.Array.Copy(data2_2, gdata2_2, Index);
                System.Array.Copy(data2_3, gdata2_3, Index);
                System.Array.Copy(data2_4, gdata2_4, Index);
                System.Array.Copy(data2_5, gdata2_5, Index);
                System.Array.Copy(data2_6, gdata2_6, Index);
                RefreshGraph();
                if (Index == 512)
                    Index = 0;
            }
            //}
            //catch
            //{

            //}
        }

        private String ControlToString(double[] Vector)
        {
            String Str = "";

            Str = Str + "PidList_Kp = \t\t" + (Vector[0]) + "\n";
            Str = Str + "PidList_Ki = \t\t" + (Vector[1]) + "\n";
            Str = Str + "PidList_Kd = \t\t" + (Vector[2]) + "\n";

            Str = Str + "PidList_C = \t\t" + (Vector[4]) + "\n";
            Str = Str + "PidList_RefADelta = \t" + (Vector[7]) + "\n";
            Str = Str + "PidList_Y = \t\t" + (Vector[10]) + "\n";
            Str = Str + "PidList_Ymin = \t\t" + (Vector[11]) + "\n";
            Str = Str + "PidList_Target = \t\t" + (Vector[15]) + "\n";
            Str = Str + "PidList_Ref = \t\t" + (Vector[16]) + "\n";
            Str = Str + "PidList_P_Error = \t" + (Vector[17]) + "\n";
            Str = Str + "PidList_P_PrevError = \t" + (Vector[18]) + "\n";
            Str = Str + "PidList_I_Error = \t\t" + (Vector[19]) + "\n";
            Str = Str + "PidList_D_Error = \t" + (Vector[20]) + "\n";
            Str = Str + "PidList_P_Portion = \t" + (Vector[21]) + "\n";
            Str = Str + "PidList_I_Portion = \t" + (Vector[22]) + "\n";
            Str = Str + "PidList_D_Portion = \t" + (Vector[23]) + "\n";
            Str = Str + "PidList_PidIntervals = \t" + (Vector[24]) + "\n";
            Str = Str + "PidList_C_Ref = \t\t" + (Vector[27]) + "\n";
            Str = Str + "PidList_C_PrevOutput = \t" + (Vector[28]) + "\n";

            return Str;
        }

        private void NTPID_Load(object sender, EventArgs e)
        {
            timer1.Start();

            //Reading whole Variable, if the Variable is two-dimensional array
            object Result;

            Result = Ch.ReadVariable("g_Dyno_PID_Process_ValList", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);

            tbPvalue.Text = (((double[])Result)[0]).ToString();
            tbIvalue.Text = (((double[])Result)[1]).ToString();
            tbDvalue.Text = (((double[])Result)[2]).ToString();

            Result = Ch.ReadVariable("g_Eng_PID_Process_ValList", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);

            tbEnginePValue.Text = (((double[])Result)[0]).ToString();
            tbEngineIValue.Text = (((double[])Result)[1]).ToString();
            tbEngineDValue.Text = (((double[])Result)[2]).ToString();
            
            Result = Ch.ReadVariable("Eng_PID_Excution_Interval", ProgramBuffer.ACSC_BUFFER_6, -1, -1, -1, -1);
            tbEngineIntrval.Text = (((double)Result)).ToString();
            Result = Ch.ReadVariable("Dyno_PID_Excution_Interval", ProgramBuffer.ACSC_BUFFER_5, -1, -1, -1, -1);
            tbDynoIntrval.Text = (((double)Result)).ToString();
            GraphInit();
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            double[] PIDData = new double[3] { double.Parse(tbPvalue.Text), double.Parse(tbIvalue.Text), double.Parse(tbDvalue.Text) };
            Ch.WriteVariable(PIDData, "g_Dyno_PID_Process_ValList", ProgramBuffer.ACSC_NONE, 0, 0, 0, 2);
            double[] PIDData2 = new double[1] { double.Parse(tbDynoIntrval.Text) };
            Ch.WriteVariable(PIDData2, "Dyno_PID_Excution_Interval", ProgramBuffer.ACSC_BUFFER_5, 0, 0, 0, 0);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            double[] PIDData = new double[3] { double.Parse(tbEnginePValue.Text), double.Parse(tbEngineIValue.Text), double.Parse(tbEngineDValue.Text) };
            Ch.WriteVariable(PIDData, "g_Eng_PID_Process_ValList", ProgramBuffer.ACSC_NONE, 0, 0, 0, 2);
            double[] PIDData2 = new double[1] { double.Parse(tbEngineIntrval.Text) };
            Ch.WriteVariable(PIDData2, "Eng_PID_Excution_Interval", ProgramBuffer.ACSC_BUFFER_6, 0, 0, 0, 0);
        }

        private void RefreshGraph()
        {
            this.SuspendLayout();
            GraphDisplay.Refresh();
            GraphDisplay2.Refresh();
            GraphDisplay_1.Refresh();
            GraphDisplay2_1.Refresh();
            this.ResumeLayout();
        }

        private void GraphInit()
        {
            this.SuspendLayout();
            GraphDisplay.DataSources.Clear();
            GraphDisplay.SetDisplayRangeX(-10, 530);
            GraphDisplay.PanelLayout = PlotterGraphPaneEx.LayoutMode.NORMAL;
            GraphDisplay.Smoothing = System.Drawing.Drawing2D.SmoothingMode.None;
            GraphDisplay2.DataSources.Clear();
            GraphDisplay2.SetDisplayRangeX(-10, 530);
            GraphDisplay2.PanelLayout = PlotterGraphPaneEx.LayoutMode.NORMAL;
            GraphDisplay2.Smoothing = System.Drawing.Drawing2D.SmoothingMode.None;

            GraphDisplay_1.DataSources.Clear();
            GraphDisplay_1.SetDisplayRangeX(-10, 530);
            GraphDisplay_1.PanelLayout = PlotterGraphPaneEx.LayoutMode.NORMAL;
            GraphDisplay_1.Smoothing = System.Drawing.Drawing2D.SmoothingMode.None;
            GraphDisplay2_1.DataSources.Clear();
            GraphDisplay2_1.SetDisplayRangeX(-10, 530);
            GraphDisplay2_1.PanelLayout = PlotterGraphPaneEx.LayoutMode.NORMAL;
            GraphDisplay2_1.Smoothing = System.Drawing.Drawing2D.SmoothingMode.None;

            for (int j = 0; j < NumGraphs; j++)
            {
                GraphDisplay.DataSources.Add(new DataSource());
                GraphDisplay.DataSources[j].AutoScaleY = true;
                GraphDisplay.DataSources[j].SetDisplayRangeY(-2000f, 2000f);
                //GraphDisplay.DataSources[j].OnRenderXAxisLabel += RenderXLabel;
                GraphDisplay.DataSources[j].Length = 0;
                //GraphDisplay.DataSources[j].SetGridDistanceY((float)1);
                GraphDisplay2.DataSources.Add(new DataSource());
                GraphDisplay2.DataSources[j].AutoScaleY = true;
                GraphDisplay2.DataSources[j].SetDisplayRangeY(-2000f, 2000f);
                //GraphDisplay.DataSources[j].OnRenderXAxisLabel += RenderXLabel;
                GraphDisplay2.DataSources[j].Length = 0;
                //GraphDisplay.DataSources[j].SetGridDistanceY((float)1);
            }

            for (int j = 0; j < 2; j++)
            {
                GraphDisplay_1.DataSources.Add(new DataSource());
                GraphDisplay_1.DataSources[j].AutoScaleY = true;
                GraphDisplay_1.DataSources[j].SetDisplayRangeY(-2000f, 2000f);
                //GraphDisplay_1.DataSources[j].OnRenderXAxisLabel += RenderXLabel;
                GraphDisplay_1.DataSources[j].Length = 0;
                //GraphDisplay_1.DataSources[j].SetGridDistanceY((float)1);
            }

            for (int j = 0; j < 3; j++)
            {
                GraphDisplay2_1.DataSources.Add(new DataSource());
                GraphDisplay2_1.DataSources[j].AutoScaleY = true;
                GraphDisplay2_1.DataSources[j].SetDisplayRangeY(-2000f, 2000f);
                //GraphDisplay_1.DataSources[j].OnRenderXAxisLabel += RenderXLabel;
                GraphDisplay2_1.DataSources[j].Length = 0;
                //GraphDisplay_1.DataSources[j].SetGridDistanceY((float)1);
            }
            GraphDisplay.DataSources[0].Name = "Real";
            GraphDisplay.DataSources[1].Name = "Target";
            GraphDisplay.DataSources[2].Name = "Ref";
            GraphDisplay2.DataSources[0].Name = "Real";
            GraphDisplay2.DataSources[1].Name = "Target";
            GraphDisplay2.DataSources[2].Name = "Ref";
            GraphDisplay_1.DataSources[0].Name = "Error_P";
            GraphDisplay_1.DataSources[1].Name = "Error_I";
            GraphDisplay2_1.DataSources[0].Name = "Error_P";
            GraphDisplay2_1.DataSources[1].Name = "Error_I";
            GraphDisplay2_1.DataSources[2].Name = "Error_D";

            Color[] cols = { Color.DarkRed,
                                        Color.DarkSlateGray,
                                        Color.DarkCyan,
                                        Color.DarkGreen,
                                        Color.DarkBlue ,
                                        Color.DarkMagenta,
                                        Color.DeepPink };

            for (int j = 0; j < NumGraphs; j++)
            {
                GraphDisplay.DataSources[j].GraphColor = cols[j % 7];
                GraphDisplay2.DataSources[j].GraphColor = cols[j % 7];
            }
            for (int j = 0; j < 2; j++)
            {
                GraphDisplay_1.DataSources[j].GraphColor = cols[j % 7];
            }
            for (int j = 0; j < 3; j++)
            {
                GraphDisplay2_1.DataSources[j].GraphColor = cols[j % 7];
            }

            GraphDisplay.BackgroundColorTop = Color.White;
            GraphDisplay.BackgroundColorBot = Color.LightGray;
            GraphDisplay.SolidGridColor = Color.LightGray;
            GraphDisplay.DashedGridColor = Color.LightGray;

            GraphDisplay2.BackgroundColorTop = Color.White;
            GraphDisplay2.BackgroundColorBot = Color.LightGray;
            GraphDisplay2.SolidGridColor = Color.LightGray;
            GraphDisplay2.DashedGridColor = Color.LightGray;

            GraphDisplay_1.BackgroundColorTop = Color.White;
            GraphDisplay_1.BackgroundColorBot = Color.LightGray;
            GraphDisplay_1.SolidGridColor = Color.LightGray;
            GraphDisplay_1.DashedGridColor = Color.LightGray;

            GraphDisplay2_1.BackgroundColorTop = Color.White;
            GraphDisplay2_1.BackgroundColorBot = Color.LightGray;
            GraphDisplay2_1.SolidGridColor = Color.LightGray;
            GraphDisplay2_1.DashedGridColor = Color.LightGray;

            this.ResumeLayout();
            GraphDisplay.Refresh();
            GraphDisplay2.Refresh();
            GraphDisplay_1.Refresh();
            GraphDisplay2_1.Refresh();

        }

        private void NTPID_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Stop();
        }

        private void YScale(int i)
        {
            CheckBox cbObjct = null;
            TextBox Lower = null;
            TextBox Upper = null;
            PlotterDisplayEx Display = null;
            switch (i)
            {
                case 0:
                    cbObjct = cbData;
                    Lower = tbLower;
                    Upper = tbUpper;
                    Display = GraphDisplay;
                    break;
                case 1:
                    cbObjct = cbData2;
                    Lower = tbLower2;
                    Upper = tbUpper2;
                    Display = GraphDisplay2;
                    break;
                case 2:
                    cbObjct = cbData_;
                    Lower = tbLower_;
                    Upper = tbUpper_;
                    Display = GraphDisplay_1;
                    break;
                case 3:
                    cbObjct = cbData2_;
                    Lower = tbLower2_;
                    Upper = tbUpper2_;
                    Display = GraphDisplay2_1;
                    break;
            }

            if (cbObjct != null && cbObjct.Checked == true)
            {
                for (int ind = 0; ind < Display.DataSources.Count; ind++)
                    Display.DataSources[ind].AutoScaleY = true;
            }
            else if (Lower.Text != "" && Upper.Text != "")
            {
                for (int ind = 0; ind < Display.DataSources.Count; ind++)
                {
                    Display.DataSources[ind].AutoScaleY = false;
                    Display.DataSources[ind].SetDisplayRangeY(float.Parse(Lower.Text), float.Parse(Upper.Text));
                }
            }

        }

        private void btnSetRange_Click(object sender, EventArgs e)
        {
            cbData.Checked = false;
            YScale(0);

        }

        private void btnSetRange2_Click(object sender, EventArgs e)
        {
            cbData2.Checked = false;
            YScale(1);
        }

        private void cbData_CheckedChanged(object sender, EventArgs e)
        {
            YScale(0);
        }

        private void cbData2_CheckedChanged(object sender, EventArgs e)
        {
            YScale(1);
        }

        private void cbData__CheckedChanged(object sender, EventArgs e)
        {
            YScale(2);
        }

        private void btnSetRange__Click(object sender, EventArgs e)
        {
            cbData_.Checked = false;
            YScale(2);
        }

        private void cbData2__CheckedChanged(object sender, EventArgs e)
        {
            YScale(3);
        }

        private void btnSetRange2__Click(object sender, EventArgs e)
        {
            cbData2_.Checked = false;
            YScale(3);
        }

    }
}
