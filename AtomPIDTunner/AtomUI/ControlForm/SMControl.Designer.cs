﻿namespace AtomUI.ControlForm
{
    partial class SMControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SMControl));
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            this.pnlMexaControl = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnResetStatus = new System.Windows.Forms.Button();
            this.btnPURGEStatus = new System.Windows.Forms.Button();
            this.btnReadyStatus = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.btnMEASUREStatus = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.btnRemoteStatus = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lvOutput = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label2 = new System.Windows.Forms.Label();
            this.tbVolume = new System.Windows.Forms.TextBox();
            this.tbCount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnParamSetting = new System.Windows.Forms.Button();
            this.lblError = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblAirTemp = new System.Windows.Forms.Label();
            this.pnlMexaControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMexaControl
            // 
            this.pnlMexaControl.Controls.Add(this.label1);
            this.pnlMexaControl.Controls.Add(this.btnResetStatus);
            this.pnlMexaControl.Controls.Add(this.btnPURGEStatus);
            this.pnlMexaControl.Controls.Add(this.btnReadyStatus);
            this.pnlMexaControl.Controls.Add(this.label25);
            this.pnlMexaControl.Controls.Add(this.label26);
            this.pnlMexaControl.Controls.Add(this.label27);
            this.pnlMexaControl.Controls.Add(this.btnMEASUREStatus);
            this.pnlMexaControl.Controls.Add(this.label28);
            this.pnlMexaControl.Controls.Add(this.btnRemoteStatus);
            this.pnlMexaControl.Location = new System.Drawing.Point(40, 274);
            this.pnlMexaControl.Name = "pnlMexaControl";
            this.pnlMexaControl.Size = new System.Drawing.Size(313, 172);
            this.pnlMexaControl.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(211, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 21);
            this.label1.TabIndex = 125;
            this.label1.Text = "RESET";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnResetStatus
            // 
            this.btnResetStatus.BackColor = System.Drawing.Color.Silver;
            this.btnResetStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnResetStatus.BackgroundImage")));
            this.btnResetStatus.Enabled = false;
            this.btnResetStatus.FlatAppearance.BorderSize = 0;
            this.btnResetStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResetStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnResetStatus.Location = new System.Drawing.Point(225, 33);
            this.btnResetStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnResetStatus.Name = "btnResetStatus";
            this.btnResetStatus.Size = new System.Drawing.Size(65, 45);
            this.btnResetStatus.TabIndex = 124;
            this.btnResetStatus.UseVisualStyleBackColor = false;
            this.btnResetStatus.Click += new System.EventHandler(this.btnResetStatus_Click);
            // 
            // btnPURGEStatus
            // 
            this.btnPURGEStatus.BackColor = System.Drawing.Color.Silver;
            this.btnPURGEStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPURGEStatus.BackgroundImage")));
            this.btnPURGEStatus.Enabled = false;
            this.btnPURGEStatus.FlatAppearance.BorderSize = 0;
            this.btnPURGEStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPURGEStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnPURGEStatus.Location = new System.Drawing.Point(231, 112);
            this.btnPURGEStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnPURGEStatus.Name = "btnPURGEStatus";
            this.btnPURGEStatus.Size = new System.Drawing.Size(65, 45);
            this.btnPURGEStatus.TabIndex = 123;
            this.btnPURGEStatus.UseVisualStyleBackColor = false;
            this.btnPURGEStatus.Click += new System.EventHandler(this.btnPURGEStatus_Click);
            // 
            // btnReadyStatus
            // 
            this.btnReadyStatus.BackgroundImage = global::AtomUI.Properties.Resources.off버튼_회색;
            this.btnReadyStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnReadyStatus.FlatAppearance.BorderSize = 0;
            this.btnReadyStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReadyStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnReadyStatus.Location = new System.Drawing.Point(20, 33);
            this.btnReadyStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnReadyStatus.Name = "btnReadyStatus";
            this.btnReadyStatus.Size = new System.Drawing.Size(65, 45);
            this.btnReadyStatus.TabIndex = 119;
            this.btnReadyStatus.UseVisualStyleBackColor = false;
            this.btnReadyStatus.Click += new System.EventHandler(this.btnReadyStatus_Click);
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label25.Location = new System.Drawing.Point(111, 10);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(94, 21);
            this.label25.TabIndex = 116;
            this.label25.Text = "REMOTE";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label26.Location = new System.Drawing.Point(111, 89);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(94, 21);
            this.label26.TabIndex = 117;
            this.label26.Text = "MEASURE";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label27.Location = new System.Drawing.Point(5, 10);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(94, 21);
            this.label27.TabIndex = 118;
            this.label27.Text = "READY";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMEASUREStatus
            // 
            this.btnMEASUREStatus.BackColor = System.Drawing.Color.Silver;
            this.btnMEASUREStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMEASUREStatus.BackgroundImage")));
            this.btnMEASUREStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMEASUREStatus.Enabled = false;
            this.btnMEASUREStatus.FlatAppearance.BorderSize = 0;
            this.btnMEASUREStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMEASUREStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnMEASUREStatus.Location = new System.Drawing.Point(125, 112);
            this.btnMEASUREStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnMEASUREStatus.Name = "btnMEASUREStatus";
            this.btnMEASUREStatus.Size = new System.Drawing.Size(65, 45);
            this.btnMEASUREStatus.TabIndex = 120;
            this.btnMEASUREStatus.UseVisualStyleBackColor = false;
            this.btnMEASUREStatus.Click += new System.EventHandler(this.btnMEASUREStatus_Click);
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label28.Location = new System.Drawing.Point(217, 89);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(94, 21);
            this.label28.TabIndex = 122;
            this.label28.Text = "PURGE";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnRemoteStatus
            // 
            this.btnRemoteStatus.BackColor = System.Drawing.Color.Silver;
            this.btnRemoteStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRemoteStatus.BackgroundImage")));
            this.btnRemoteStatus.Enabled = false;
            this.btnRemoteStatus.FlatAppearance.BorderSize = 0;
            this.btnRemoteStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemoteStatus.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnRemoteStatus.Location = new System.Drawing.Point(125, 33);
            this.btnRemoteStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRemoteStatus.Name = "btnRemoteStatus";
            this.btnRemoteStatus.Size = new System.Drawing.Size(65, 45);
            this.btnRemoteStatus.TabIndex = 121;
            this.btnRemoteStatus.UseVisualStyleBackColor = false;
            this.btnRemoteStatus.Click += new System.EventHandler(this.btnRemoteStatus_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lvOutput
            // 
            this.lvOutput.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lvOutput.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lvOutput.FullRowSelect = true;
            this.lvOutput.GridLines = true;
            listViewGroup1.Header = "ListViewGroup";
            listViewGroup1.Name = "Global Variables";
            listViewGroup2.Header = "ListViewGroup";
            listViewGroup2.Name = "Local Variables";
            this.lvOutput.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2});
            this.lvOutput.Location = new System.Drawing.Point(40, 25);
            this.lvOutput.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lvOutput.MultiSelect = false;
            this.lvOutput.Name = "lvOutput";
            this.lvOutput.ShowGroups = false;
            this.lvOutput.Size = new System.Drawing.Size(313, 173);
            this.lvOutput.TabIndex = 86;
            this.lvOutput.UseCompatibleStateImageBehavior = false;
            this.lvOutput.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 147;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Values";
            this.columnHeader2.Width = 135;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 218);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 87;
            this.label2.Text = "측정용량 ( ml )";
            // 
            // tbVolume
            // 
            this.tbVolume.Location = new System.Drawing.Point(153, 211);
            this.tbVolume.Name = "tbVolume";
            this.tbVolume.Size = new System.Drawing.Size(100, 21);
            this.tbVolume.TabIndex = 88;
            this.tbVolume.Text = "1000";
            // 
            // tbCount
            // 
            this.tbCount.Location = new System.Drawing.Point(153, 247);
            this.tbCount.Name = "tbCount";
            this.tbCount.Size = new System.Drawing.Size(100, 21);
            this.tbCount.TabIndex = 90;
            this.tbCount.Text = "3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 250);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 89;
            this.label3.Text = "측정횟수";
            // 
            // btnParamSetting
            // 
            this.btnParamSetting.Location = new System.Drawing.Point(265, 211);
            this.btnParamSetting.Name = "btnParamSetting";
            this.btnParamSetting.Size = new System.Drawing.Size(88, 51);
            this.btnParamSetting.TabIndex = 91;
            this.btnParamSetting.Text = "설정";
            this.btnParamSetting.UseVisualStyleBackColor = true;
            this.btnParamSetting.Click += new System.EventHandler(this.btnParamSetting_Click);
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Location = new System.Drawing.Point(129, 486);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 12);
            this.lblError.TabIndex = 95;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(46, 486);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 94;
            this.label4.Text = "에러메시지 : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(46, 462);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 12);
            this.label5.TabIndex = 96;
            this.label5.Text = "Air Temp. : ";
            // 
            // lblAirTemp
            // 
            this.lblAirTemp.AutoSize = true;
            this.lblAirTemp.Location = new System.Drawing.Point(129, 462);
            this.lblAirTemp.Name = "lblAirTemp";
            this.lblAirTemp.Size = new System.Drawing.Size(0, 12);
            this.lblAirTemp.TabIndex = 97;
            // 
            // SMControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 544);
            this.Controls.Add(this.lblAirTemp);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnParamSetting);
            this.Controls.Add(this.tbCount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbVolume);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lvOutput);
            this.Controls.Add(this.pnlMexaControl);
            this.Name = "SMControl";
            this.Text = "SMControl";
            this.Load += new System.EventHandler(this.SMControl_Load);
            this.pnlMexaControl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlMexaControl;
        private System.Windows.Forms.Button btnPURGEStatus;
        private System.Windows.Forms.Button btnReadyStatus;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button btnMEASUREStatus;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button btnRemoteStatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnResetStatus;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ListView lvOutput;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbVolume;
        private System.Windows.Forms.TextBox tbCount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnParamSetting;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblAirTemp;
    }
}