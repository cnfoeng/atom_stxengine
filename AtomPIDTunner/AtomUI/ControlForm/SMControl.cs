﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;

namespace AtomUI.ControlForm
{
    public partial class SMControl : Form
    {
        public SMControl()
        {
            InitializeComponent();
        }

        private void SMControl_Load(object sender, EventArgs e)
        {
            ButtonStatusChange(btnReadyStatus, false, false);
            ButtonStatusChange(btnRemoteStatus, false, true);
            ButtonStatusChange(btnResetStatus, false, true);
            ButtonStatusChange(btnMEASUREStatus, false, false);
            ButtonStatusChange(btnPURGEStatus, false, false);

            lvOutput.Items.Clear();
            foreach (VariableInfo itmx in DaqData.sysVarInfo.SmokeMeter_Variables)
            {
                ListViewItem itm = lvOutput.Items.Add(itmx.VariableName);
                itm.SubItems.Add("0");
            }
        }

        private void MEXAStatusUpdate()
        {
            if (AsynchronousSocketListener.smokeMeter.ST_Remote == DAQ.SerialComm.SmokeMeter.SMStatus.SREM)
            {
                ButtonStatusChange(btnRemoteStatus, true, true);
            }
            else
            {
                ButtonStatusChange(btnRemoteStatus, false, true);
            }
            if (AsynchronousSocketListener.smokeMeter.ST_Function == DAQ.SerialComm.SmokeMeter.SMStatus.SRDY)
            {
                ButtonStatusChange(btnReadyStatus, true, false);
                btnParamSetting.Enabled = true;
            }
            else
            {
                btnParamSetting.Enabled = false;
                ButtonStatusChange(btnReadyStatus, false, true);
            }
            if (AsynchronousSocketListener.smokeMeter.ST_Function == DAQ.SerialComm.SmokeMeter.SMStatus.SRES)
            {
                ButtonStatusChange(btnResetStatus, true, false);
            }
            else
            {
                ButtonStatusChange(btnResetStatus, false, true);
            }
            if (AsynchronousSocketListener.smokeMeter.ST_Function == DAQ.SerialComm.SmokeMeter.SMStatus.SPUL)
            {
                ButtonStatusChange(btnPURGEStatus, true, false);
            }
            else
            {
                if (AsynchronousSocketListener.smokeMeter.ST_Remote == DAQ.SerialComm.SmokeMeter.SMStatus.SREM
                    && AsynchronousSocketListener.smokeMeter.ST_Function == DAQ.SerialComm.SmokeMeter.SMStatus.SRDY)
                    ButtonStatusChange(btnPURGEStatus, false, true);
                else
                    ButtonStatusChange(btnPURGEStatus, false, false);
            }
            if (AsynchronousSocketListener.smokeMeter.ST_Function == DAQ.SerialComm.SmokeMeter.SMStatus.SMES)
            {
                ButtonStatusChange(btnMEASUREStatus, true, false);
            }
            else
            {
                if (AsynchronousSocketListener.smokeMeter.ST_Remote == DAQ.SerialComm.SmokeMeter.SMStatus.SREM
                    && AsynchronousSocketListener.smokeMeter.ST_Function == DAQ.SerialComm.SmokeMeter.SMStatus.SRDY)
                    ButtonStatusChange(btnMEASUREStatus, false, true);
                else
                    ButtonStatusChange(btnMEASUREStatus, false, false);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                MEXAStatusUpdate();

                lblAirTemp.Text = AsynchronousSocketListener.smokeMeter.AIR_TEMP.ToString(); ;
                if (AsynchronousSocketListener.smokeMeter.ERROR_CODE > 0)
                {
                    lblError.Text = AsynchronousSocketListener.smokeMeter.Error_Description;

                }
                else
                {
                    lblError.Text = "";
                }
                for (int ind = 0; ind < DaqData.sysVarInfo.SmokeMeter_Variables.Count; ind++)
                {
                    try
                    {
                        string Value = DaqData.sysVarInfo.SmokeMeter_Variables[ind].IOValue.ToString();
                        if (lvOutput.Items[ind].SubItems[1].Text == Value)
                            lvOutput.Items[ind].SubItems[0].ForeColor = Color.Black;
                        else
                        {
                            lvOutput.Items[ind].SubItems[1].Text = Value;
                            lvOutput.Items[ind].SubItems[0].ForeColor = Color.Red;
                        }
                    }
                    catch { }
                }

            }
            catch { }
        }

        private void ButtonStatusChange(Button btn, bool status, bool enable)
        {
            if (status)
            {
                btn.BackgroundImage = global::AtomUI.Properties.Resources.on버튼;
            }
            else
            {
                if (enable)
                    btn.BackgroundImage = global::AtomUI.Properties.Resources.off버튼;
                else
                    btn.BackgroundImage = global::AtomUI.Properties.Resources.off버튼_회색;
            }

            btn.Enabled = enable;
        }

        private void btnRemoteStatus_Click(object sender, EventArgs e)
        {
            AsynchronousSocketListener.smokeMeter.SM_Command_List.Add("SREM");
            ButtonStatusChange(btnReadyStatus, true, false);
        }

        private void btnMEASUREStatus_Click(object sender, EventArgs e)
        {
            AsynchronousSocketListener.smokeMeter.SM_Command_List.Add("SMES");
            ButtonStatusChange(btnMEASUREStatus, true, false);
        }

        private void btnPURGEStatus_Click(object sender, EventArgs e)
        {
            AsynchronousSocketListener.smokeMeter.SM_Command_List.Add("SPUL");
            ButtonStatusChange(btnPURGEStatus, true, false);
        }

        private void btnResetStatus_Click(object sender, EventArgs e)
        {
            AsynchronousSocketListener.smokeMeter.SM_Command_List.Add("SRES");
            ButtonStatusChange(btnResetStatus, true, false);
        }

        private void btnParamSetting_Click(object sender, EventArgs e)
        {
            AsynchronousSocketListener.smokeMeter.SetParameter(int.Parse(tbVolume.Text), int.Parse(tbCount.Text));
            AsynchronousSocketListener.smokeMeter.SM_Command_List.Add("EMZY");
        }

        private void btnReadyStatus_Click(object sender, EventArgs e)
        {
            AsynchronousSocketListener.smokeMeter.SM_Command_List.Add("SRDY");
        }
    }
}

