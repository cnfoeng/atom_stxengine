﻿namespace AtomUI.ControlForm
{
    partial class StepModeDefine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.dgStepMode = new System.Windows.Forms.DataGridView();
            this.STEP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dyno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D_Mode = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.D_Ramp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Engine = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.E_Mode = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.E_Ramp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Settling = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MeasuringNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Duration = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Period = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RestTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ignition = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ThermalShock = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Heater = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.AlarmTable = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Goto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Iteration = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.tbFileName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lbFileList = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lbEngine = new System.Windows.Forms.Label();
            this.lbDyno = new System.Windows.Forms.Label();
            this.lbTime = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgStepMode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgStepMode
            // 
            this.dgStepMode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgStepMode.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgStepMode.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgStepMode.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.STEP,
            this.Dyno,
            this.D_Mode,
            this.D_Ramp,
            this.Engine,
            this.E_Mode,
            this.E_Ramp,
            this.Settling,
            this.MeasuringNo,
            this.Duration,
            this.Period,
            this.RestTime,
            this.TotalTime,
            this.Ignition,
            this.ThermalShock,
            this.Heater,
            this.AlarmTable,
            this.Goto,
            this.Iteration});
            this.dgStepMode.Location = new System.Drawing.Point(10, 305);
            this.dgStepMode.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgStepMode.Name = "dgStepMode";
            this.dgStepMode.RowTemplate.Height = 27;
            this.dgStepMode.Size = new System.Drawing.Size(1416, 379);
            this.dgStepMode.TabIndex = 0;
            this.dgStepMode.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgStepMode_CellEndEdit);
            this.dgStepMode.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgStepMode_CellLeave_1);
            this.dgStepMode.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgStepMode_RowsAdded);
            // 
            // STEP
            // 
            this.STEP.HeaderText = "STEP";
            this.STEP.Name = "STEP";
            this.STEP.Width = 62;
            // 
            // Dyno
            // 
            this.Dyno.HeaderText = "Dyno SV";
            this.Dyno.Name = "Dyno";
            this.Dyno.Width = 59;
            // 
            // D_Mode
            // 
            this.D_Mode.HeaderText = "Dyno Mode";
            this.D_Mode.Name = "D_Mode";
            this.D_Mode.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.D_Mode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.D_Mode.Width = 87;
            // 
            // D_Ramp
            // 
            this.D_Ramp.HeaderText = "Dyno Ramp";
            this.D_Ramp.Name = "D_Ramp";
            this.D_Ramp.Width = 88;
            // 
            // Engine
            // 
            this.Engine.HeaderText = "Engine";
            this.Engine.Name = "Engine";
            this.Engine.Width = 69;
            // 
            // E_Mode
            // 
            this.E_Mode.HeaderText = "Engine Mode";
            this.E_Mode.Name = "E_Mode";
            this.E_Mode.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.E_Mode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.E_Mode.Width = 96;
            // 
            // E_Ramp
            // 
            this.E_Ramp.HeaderText = "Engine Ramp";
            this.E_Ramp.Name = "E_Ramp";
            this.E_Ramp.Width = 97;
            // 
            // Settling
            // 
            this.Settling.HeaderText = "Settling Time";
            this.Settling.Name = "Settling";
            this.Settling.Width = 96;
            // 
            // MeasuringNo
            // 
            this.MeasuringNo.HeaderText = "Measuring No.";
            this.MeasuringNo.Name = "MeasuringNo";
            this.MeasuringNo.Width = 105;
            // 
            // Duration
            // 
            this.Duration.HeaderText = "Duration";
            this.Duration.Name = "Duration";
            this.Duration.Width = 76;
            // 
            // Period
            // 
            this.Period.HeaderText = "Period";
            this.Period.Name = "Period";
            this.Period.Width = 66;
            // 
            // RestTime
            // 
            this.RestTime.HeaderText = "Rest Time";
            this.RestTime.Name = "RestTime";
            this.RestTime.Width = 81;
            // 
            // TotalTime
            // 
            this.TotalTime.HeaderText = "Total Time";
            this.TotalTime.Name = "TotalTime";
            this.TotalTime.Width = 84;
            // 
            // Ignition
            // 
            this.Ignition.HeaderText = "Ignition";
            this.Ignition.Name = "Ignition";
            this.Ignition.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Ignition.Visible = false;
            this.Ignition.Width = 51;
            // 
            // ThermalShock
            // 
            this.ThermalShock.HeaderText = "Thermal Shock";
            this.ThermalShock.Name = "ThermalShock";
            this.ThermalShock.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ThermalShock.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ThermalShock.Visible = false;
            this.ThermalShock.Width = 106;
            // 
            // Heater
            // 
            this.Heater.HeaderText = "Heater";
            this.Heater.Name = "Heater";
            this.Heater.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Heater.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Heater.Visible = false;
            this.Heater.Width = 66;
            // 
            // AlarmTable
            // 
            this.AlarmTable.HeaderText = "Alarm Table";
            this.AlarmTable.Name = "AlarmTable";
            this.AlarmTable.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AlarmTable.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.AlarmTable.Visible = false;
            this.AlarmTable.Width = 91;
            // 
            // Goto
            // 
            this.Goto.HeaderText = "Goto";
            this.Goto.Name = "Goto";
            this.Goto.Width = 56;
            // 
            // Iteration
            // 
            this.Iteration.HeaderText = "Iteration";
            this.Iteration.Name = "Iteration";
            this.Iteration.Width = 74;
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea2.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart1.Legends.Add(legend2);
            this.chart1.Location = new System.Drawing.Point(518, 10);
            this.chart1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chart1.Name = "chart1";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chart1.Series.Add(series2);
            this.chart1.Size = new System.Drawing.Size(908, 270);
            this.chart1.TabIndex = 1;
            this.chart1.Text = "chart1";
            this.chart1.CursorPositionChanged += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.CursorEventArgs>(this.chart1_CursorPositionChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 290);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 12);
            this.label1.TabIndex = 61;
            this.label1.Text = "STEP MODE DATA";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnLoad);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.tbFileName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.lbFileList);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(10, 10);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(494, 270);
            this.groupBox1.TabIndex = 62;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "CONTROL";
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = global::AtomUI.Properties.Resources.Close;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(365, 217);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 36);
            this.btnClose.TabIndex = 68;
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnLoad
            // 
            this.btnLoad.BackgroundImage = global::AtomUI.Properties.Resources.Load_frome_file;
            this.btnLoad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnLoad.FlatAppearance.BorderSize = 0;
            this.btnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoad.Location = new System.Drawing.Point(365, 61);
            this.btnLoad.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(120, 36);
            this.btnLoad.TabIndex = 66;
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackgroundImage = global::AtomUI.Properties.Resources.save_to_file;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(234, 61);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(120, 36);
            this.btnSave.TabIndex = 65;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbFileName
            // 
            this.tbFileName.Location = new System.Drawing.Point(234, 36);
            this.tbFileName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbFileName.Name = "tbFileName";
            this.tbFileName.Size = new System.Drawing.Size(255, 21);
            this.tbFileName.TabIndex = 64;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(231, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 12);
            this.label3.TabIndex = 63;
            this.label3.Text = "File Name";
            // 
            // lbFileList
            // 
            this.lbFileList.FormattingEnabled = true;
            this.lbFileList.ItemHeight = 12;
            this.lbFileList.Location = new System.Drawing.Point(5, 36);
            this.lbFileList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lbFileList.Name = "lbFileList";
            this.lbFileList.Size = new System.Drawing.Size(224, 220);
            this.lbFileList.TabIndex = 62;
            this.lbFileList.SelectedIndexChanged += new System.EventHandler(this.lbFileList_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 12);
            this.label2.TabIndex = 61;
            this.label2.Text = "Saved File Name";
            // 
            // lbEngine
            // 
            this.lbEngine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbEngine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbEngine.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbEngine.ForeColor = System.Drawing.Color.White;
            this.lbEngine.Location = new System.Drawing.Point(1309, 249);
            this.lbEngine.Name = "lbEngine";
            this.lbEngine.Size = new System.Drawing.Size(93, 21);
            this.lbEngine.TabIndex = 76;
            this.lbEngine.Text = "0 Sec";
            this.lbEngine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbDyno
            // 
            this.lbDyno.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbDyno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbDyno.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbDyno.ForeColor = System.Drawing.Color.White;
            this.lbDyno.Location = new System.Drawing.Point(1309, 179);
            this.lbDyno.Name = "lbDyno";
            this.lbDyno.Size = new System.Drawing.Size(93, 21);
            this.lbDyno.TabIndex = 77;
            this.lbDyno.Text = "0 Sec";
            this.lbDyno.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbTime
            // 
            this.lbTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbTime.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbTime.ForeColor = System.Drawing.Color.White;
            this.lbTime.Location = new System.Drawing.Point(1309, 106);
            this.lbTime.Name = "lbTime";
            this.lbTime.Size = new System.Drawing.Size(93, 21);
            this.lbTime.TabIndex = 78;
            this.lbTime.Text = "0 Sec";
            this.lbTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(1309, 222);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 26);
            this.label6.TabIndex = 73;
            this.label6.Tag = "F";
            this.label6.Text = "Engine";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(1309, 153);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 26);
            this.label4.TabIndex = 74;
            this.label4.Tag = "F";
            this.label4.Text = "Dyno";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(1309, 80);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(93, 26);
            this.label20.TabIndex = 75;
            this.label20.Tag = "F";
            this.label20.Text = "Time";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // StepModeDefine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1437, 694);
            this.Controls.Add(this.lbEngine);
            this.Controls.Add(this.lbDyno);
            this.Controls.Add(this.lbTime);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.dgStepMode);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "StepModeDefine";
            this.Text = "Step Mode Designer";
            this.Load += new System.EventHandler(this.StepModeDefine_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgStepMode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgStepMode;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox tbFileName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lbFileList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbEngine;
        private System.Windows.Forms.Label lbDyno;
        private System.Windows.Forms.Label lbTime;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DataGridViewTextBoxColumn STEP;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dyno;
        private System.Windows.Forms.DataGridViewComboBoxColumn D_Mode;
        private System.Windows.Forms.DataGridViewTextBoxColumn D_Ramp;
        private System.Windows.Forms.DataGridViewTextBoxColumn Engine;
        private System.Windows.Forms.DataGridViewComboBoxColumn E_Mode;
        private System.Windows.Forms.DataGridViewTextBoxColumn E_Ramp;
        private System.Windows.Forms.DataGridViewTextBoxColumn Settling;
        private System.Windows.Forms.DataGridViewTextBoxColumn MeasuringNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Duration;
        private System.Windows.Forms.DataGridViewTextBoxColumn Period;
        private System.Windows.Forms.DataGridViewTextBoxColumn RestTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalTime;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Ignition;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ThermalShock;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Heater;
        private System.Windows.Forms.DataGridViewComboBoxColumn AlarmTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn Goto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Iteration;
    }
}