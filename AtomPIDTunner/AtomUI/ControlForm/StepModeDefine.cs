﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using AtomUI.Class;
using System.Windows.Forms.DataVisualization.Charting;

namespace AtomUI.ControlForm
{
    public partial class StepModeDefine : Form
    {
        public StepModeDefine()
        {
            InitializeComponent();
        }

        private void StepModeDefine_Load(object sender, EventArgs e)
        {
            ChartInit();
            FileBoxRefresh();
            tbFileName.Text = Ethercat.sysInfo.sysSettings.StepModeFileName.Substring(0, Ethercat.sysInfo.sysSettings.StepModeFileName.LastIndexOf('.'));
            dgInit();
            DataLoad();
        }

        private void FileBoxRefresh()
        {
            lbFileList.Items.Clear();
            string[] Files = Directory.GetFiles(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.StepModeDir);
            foreach (string file in Files)
            {
                string filename = file.Substring(file.LastIndexOf('\\') + 1, file.Length - file.LastIndexOf('\\') - 1);
                string filenameonly = filename.Substring(0, filename.LastIndexOf('.'));
                lbFileList.Items.Add(filenameonly);
                if (Ethercat.sysInfo.sysSettings.StepModeFileName == file)
                {
                    lbFileList.SelectedValue = filename;
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //try
            //{
            if (DialogResult.Yes == MessageBox.Show("Save current setting to File. Continue?", "Step Mode Save", MessageBoxButtons.YesNo))//row에 있는 데이터들 dr[]배열에 넣는 과정.
            {
                StepMode.StepModeData.Data.Tables["StepMode"].Rows.Clear();
                for (int i = 0; i < dgStepMode.Rows.Count - 1; i++)
                {
                    DataRow dr = StepMode.StepModeData.Data.Tables["StepMode"].Rows.Add();
                    for (int j = 0; j < dgStepMode.Columns.Count; j++)
                    {
                        if (StepMode.StepModeData.Data.Tables["StepMode"].Columns[j].DataType == Type.GetType("System.Int32"))
                        {
                            if (dgStepMode[j, i].Value.GetType() == Type.GetType("System.Int32"))
                            {
                                dr[j] = (int)dgStepMode[j, i].Value;
                            }
                            else
                            {
                                dr[j] = int.Parse((string)dgStepMode[j, i].Value);
                            }
                        }
                        else if (StepMode.StepModeData.Data.Tables["StepMode"].Columns[j].DataType == Type.GetType("System.Boolean"))
                        {
                            if (dgStepMode[j, i].Value == null)
                            {
                                dr[j] = false;
                            }
                            else
                            {
                                dr[j] = (bool)dgStepMode[j, i].Value;
                            }
                        }
                        else  if (StepMode.StepModeData.Data.Tables["StepMode"].Columns[j].DataType == Type.GetType("System.Double"))
                        {
                            dr[j] = double.Parse((string)dgStepMode[j, i].Value);
                        }
                        else if (j == 2 || j == 5 || j == 16)//column이 3,6,17번째면
                        {
                            dr[j] = ((DataGridViewComboBoxCell)dgStepMode[j, i]).Value;
                        }
                        else
                        {
                            dr[j] = (string)dgStepMode[j, i].Value;
                        }
                    }
                }

                Ethercat.sysInfo.sysSettings.StepModeFileName = tbFileName.Text + ".xml";
                Ethercat.sysInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SysInfoDir + "\\" + Ethercat.sysInfo.sysSettings.SysInfoFileName);

                StepMode.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.StepModeDir + "\\" + Ethercat.sysInfo.sysSettings.StepModeFileName);
                //StepMode.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.StepModeDir + "\\" + Ethercat.sysInfo.sysSettings.StepModeFileName);
                FileBoxRefresh();
            }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Error occured during Save Variables", "Step Mode Save");
            //}
        }
        private void DataLoad()
        {
            if (File.Exists(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.StepModeDir + "\\" + Ethercat.sysInfo.sysSettings.StepModeFileName))
            {
                dgStepMode.Rows.Clear();

                StepMode.Load(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.StepModeDir + "\\" + Ethercat.sysInfo.sysSettings.StepModeFileName);
                for (int i = 0; i < StepMode.StepModeData.Data.Tables["StepMode"].Rows.Count; i++)
                {
                    dgStepMode.Rows.Add();
                    for (int j = 0; j < StepMode.StepModeData.Data.Tables["StepMode"].Columns.Count; j++)
                    {
                        if (j == 13 || j == 14 || j == 15)
                            dgStepMode[j, i].Value = StepMode.StepModeData.Data.Tables["StepMode"].Rows[i][j];
                        else if (j == 2 || j == 5 || j == 16)
                        {
                            ((DataGridViewComboBoxCell)dgStepMode[j, i]).Value = StepMode.StepModeData.Data.Tables["StepMode"].Rows[i][j];
                        }
                        else dgStepMode[j, i].Value = StepMode.StepModeData.Data.Tables["StepMode"].Rows[i][j].ToString();
                    }
                    //dgStepMode.Rows.Add();
                }
                //dgStepMode.Rows.Add();
            }
            Redraw();
        }
        private void btnLoad_Click(object sender, EventArgs e)
        {
            //try
            //{
            if (DialogResult.Yes == MessageBox.Show("Load setting from File. Current setting will be overwriting. Continue?", "Step Mode Save", MessageBoxButtons.YesNo))
            {
                Ethercat.sysInfo.sysSettings.StepModeFileName = tbFileName.Text + ".xml";
                Ethercat.sysInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SysInfoDir + "\\" + Ethercat.sysInfo.sysSettings.SysInfoFileName);
                DataLoad();
            }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Error occured during Load Variables", "Step Mode Load");
            //}

        }

        private void dgInit()
        {
            //dgStepMode.DataSource = StepMode.StepModeData.Data;
            //dgStepMode.DataMember = "StepMode";

            DataGridViewComboBoxColumn comboBoxColumn = (DataGridViewComboBoxColumn)dgStepMode.Columns[2];
            //comboBoxColumn.Items.AddRange("N", "T");
            comboBoxColumn.Items.AddRange("N", "T");
            comboBoxColumn = (DataGridViewComboBoxColumn)dgStepMode.Columns[5];
            comboBoxColumn.Items.AddRange("α", "T", "N");
            comboBoxColumn = (DataGridViewComboBoxColumn)dgStepMode.Columns[16];
            comboBoxColumn.Items.AddRange("REGLUAR", "RATED POWER", "IDLE");

        }

        private void dgStepMode_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
        }
        private void TimeCalc()
        {

            for (int i = 0; i < dgStepMode.Rows.Count; i++)
            {
                int Time = 0;
                try
                {
                    int time1 = (int.Parse((string)dgStepMode[3, i].Value));
                    int time2 = (int.Parse((string)dgStepMode[6, i].Value));
                    double Dyno = (double.Parse((string)dgStepMode[1, i].Value));
                    double Engine = (double.Parse((string)dgStepMode[4, i].Value));

                    if (time1 > time2)
                        Time = Time + time1;
                    else
                        Time = Time + time2;

                    int Count1 = (int.Parse((string)dgStepMode[8, i].Value));
                    int Count2 = Count1 - 1;
                    if (Count2 < 0)
                        Count2 = 0;

                    Time = Time + (int.Parse((string)dgStepMode[7, i].Value));

                    Time = Time + (int.Parse((string)dgStepMode[9, i].Value)) * Count1 + (int.Parse((string)dgStepMode[10, i].Value)) * Count2;

                    Time = Time + (int.Parse((string)dgStepMode[11, i].Value));
                    dgStepMode[12, i].Value = Time;
                }
                catch { }
            }
        }
    private void dgStepMode_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            TimeCalc();
            Redraw();
        }

        private void ChartInit()
        {
            //X축 데이터 타입
            chart1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(223)))), ((int)(((byte)(193)))));
            chart1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(64)))), ((int)(((byte)(1)))));
            this.chart1.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.chart1.BorderlineWidth = 2;

            chart1.ChartAreas[0].BackColor = System.Drawing.Color.OldLace;
            chart1.ChartAreas[0].BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.ChartAreas[0].BackSecondaryColor = System.Drawing.Color.White;
            chart1.ChartAreas[0].BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chart1.ChartAreas[0].AxisY2.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.DashDotDot;
            chart1.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].AxisY2.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));

            chart1.Series.Clear();
            chart1.Series.Add("Dyno");
            chart1.Series.Add("Engine");

            chart1.Series[0].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            //Y축 데이터 타입
            chart1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;  //분산형 그래프
            chart1.Series[1].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;  //분산형 그래프
            chart1.Series[0].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;

            //차트 타입
            chart1.Series[0].IsValueShownAsLabel = true;
            chart1.Series[1].IsValueShownAsLabel = true;
            chart1.Series[0].IsXValueIndexed = false;
            chart1.Series[1].IsXValueIndexed = false;
            chart1.Series[0].BorderWidth = 3;
            chart1.Series[1].BorderWidth = 3;
            chart1.Series[0].MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Diamond;
            chart1.Series[1].MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            chart1.Series[0].MarkerSize = 6;
            chart1.Series[0].MarkerBorderWidth = 2;
            chart1.Series[1].MarkerSize = 6;
            chart1.Series[1].MarkerBorderWidth = 2;
            chart1.Series[0].ShadowColor = System.Drawing.Color.Black;
            chart1.Series[1].ShadowColor = System.Drawing.Color.Black;
            chart1.Series[0].ShadowOffset = 2;
            chart1.Series[1].ShadowOffset = 2;
            chart1.Series[0].Color = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(65)))), ((int)(((byte)(140)))), ((int)(((byte)(240)))));
            chart1.Series[1].Color = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(224)))), ((int)(((byte)(64)))), ((int)(((byte)(10)))));

            chart1.ChartAreas[0].CursorX.LineColor = Color.Gray;
            chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;

        }

        private void Redraw()
        {
            //X축 데이터 타입
            chart1.Series[0].Points.Clear();
            chart1.Series[1].Points.Clear();

            if (dgStepMode.Rows.Count > 1)
            {
                DataTable dt = new DataTable();
                int Time = 0;
                try
                {
                    DataPoint dataPoint1 = new DataPoint(0, 0);
                    DataPoint dataPoint2 = new DataPoint(0, 0);
                    chart1.Series[0].Points.Add(dataPoint1);
                    chart1.Series[1].Points.Add(dataPoint2);

                    for (int i = 0; i < dgStepMode.Rows.Count; i++)
                    {
                        int time1 = (int.Parse((string)dgStepMode[3 , i].Value));
                        int time2 = (int.Parse((string)dgStepMode[6,i].Value));
                        int Dyno = (int.Parse((string)dgStepMode[1, i].Value));
                        int Engine = (int.Parse((string)dgStepMode[4, i].Value));

                        if (time1 > time2)
                            Time = Time + time1;
                        else
                            Time = Time + time2;

                        dataPoint1 = new DataPoint(Time, Dyno);
                        dataPoint2 = new DataPoint(Time, Engine);
                        chart1.Series[0].Points.Add(dataPoint1);
                        chart1.Series[1].Points.Add(dataPoint2);

                        int Count1 = (int.Parse((string)dgStepMode[8,i].Value));
                        int Count2 = Count1 - 1;
                        if (Count2 < 0)
                            Count2 = 0;

                        Time = Time + (int.Parse((string)dgStepMode[7,i].Value));

                        dataPoint1 = new DataPoint(Time, Dyno);
                        dataPoint2 = new DataPoint(Time, Engine);
                        chart1.Series[0].Points.Add(dataPoint1);
                        chart1.Series[1].Points.Add(dataPoint2);

                        Time = Time + (int.Parse((string)dgStepMode[9, i].Value)) * Count1 + (int.Parse((string)dgStepMode[10, i].Value)) * Count2;

                        dataPoint1 = new DataPoint(Time, Dyno);
                        dataPoint2 = new DataPoint(Time, Engine);
                        chart1.Series[0].Points.Add(dataPoint1);
                        chart1.Series[1].Points.Add(dataPoint2);

                        Time = Time + (int.Parse((string)dgStepMode[11, i].Value));

                        dataPoint1 = new DataPoint(Time, Dyno);
                        dataPoint2 = new DataPoint(Time, Engine);
                        chart1.Series[0].Points.Add(dataPoint1);
                        chart1.Series[1].Points.Add(dataPoint2);
                    }
                }
                catch (Exception ex)
                {
                    CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n connect Error!!");
                }
            }

        }

        private void lbFileList_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbFileName.Text = lbFileList.SelectedItem.ToString();
        }

        private void SetData(double Position)
        {
            int Time = (int)Position;
            int Y1Index = 0;
            int Y2Index = 0;
            lbTime.Text = "-";
            lbDyno.Text = "-";
            lbEngine.Text = "-";

            for (int i = 0; i < chart1.Series[0].Points.Count; i++)
            {
                if (Time < (int)chart1.Series[0].Points[i].XValue)
                {
                    Y1Index = i;
                    break;
                }
            }
            for (int i = 0; i < chart1.Series[0].Points.Count; i++)
            {
                if (Time <(int)chart1.Series[0].Points[i].XValue)
                {
                    Y2Index = i;
                    break;
                }
            }
            if (Y1Index > 0)
            {
                lbTime.Text = Time.ToString();

                lbDyno.Text = ((double)chart1.Series[0].Points[Y1Index - 1].YValues[0]).ToString();
                lbEngine.Text = ((double)chart1.Series[1].Points[Y2Index - 1].YValues[0]).ToString();
            }
        }

        private void chart1_CursorPositionChanged(object sender, CursorEventArgs e)
        {
            SetData(e.NewPosition);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {

        }

        private void btnChannel_Click(object sender, EventArgs e)
        {

        }

        private void dgStepMode_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            TimeCalc();
            //Redraw();
        }

        private void dgStepMode_CellLeave_1(object sender, DataGridViewCellEventArgs e)
        {
            TimeCalc();
            //Redraw();
        }
    }
}
