﻿namespace AtomUI.ControlForm
{
    partial class StepRun
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.ListViewGroup listViewGroup3 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup4 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StepRun));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.lvInput = new System.Windows.Forms.ListView();
            this.Header1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Ignition = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnLoad = new System.Windows.Forms.Button();
            this.tbFileName = new System.Windows.Forms.TextBox();
            this.lbFileList = new System.Windows.Forms.ListBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.chkLoggingAvg = new System.Windows.Forms.CheckBox();
            this.chkLoggingConstant = new System.Windows.Forms.CheckBox();
            this.tbCycle = new System.Windows.Forms.TextBox();
            this.lblCycle = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnPause = new System.Windows.Forms.Button();
            this.lbCurrentPhase = new System.Windows.Forms.Label();
            this.lbPhaseLeftTime = new System.Windows.Forms.Label();
            this.lbCurrentTime = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lbCurrentStep = new System.Windows.Forms.Label();
            this.lbPhaseTotalTime = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.lbTotalTime = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.lbTGEAlpha = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.lbTGDTorque = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.lbTGDSpeed = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.lbFBTorque = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.lbFBAlpha = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.lbFBSpeed = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lbEngineDuration = new System.Windows.Forms.Label();
            this.lbDynoDuration = new System.Windows.Forms.Label();
            this.tbEngineDuration = new System.Windows.Forms.TextBox();
            this.tbDynoDuration = new System.Windows.Forms.TextBox();
            this.lbEngine = new System.Windows.Forms.Label();
            this.lbDyno = new System.Windows.Forms.Label();
            this.chFeedBackEngine = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chFeedBackDyno = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnTN = new System.Windows.Forms.Button();
            this.btnTA = new System.Windows.Forms.Button();
            this.lblTN = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnNT = new System.Windows.Forms.Button();
            this.btnNA = new System.Windows.Forms.Button();
            this.btnHornOff = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnAlarm = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.btnRemote = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.chkLoggingCtn = new System.Windows.Forms.CheckBox();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chFeedBackEngine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chFeedBackDyno)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.chart1);
            this.groupBox2.Controls.Add(this.lvInput);
            this.groupBox2.Location = new System.Drawing.Point(232, 10);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(1257, 285);
            this.groupBox2.TabIndex = 73;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Loaded Step Definition";
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chart1.BorderlineColor = System.Drawing.Color.Black;
            this.chart1.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea4.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.chart1.Legends.Add(legend4);
            this.chart1.Location = new System.Drawing.Point(5, 136);
            this.chart1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chart1.Name = "chart1";
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.chart1.Series.Add(series4);
            this.chart1.Size = new System.Drawing.Size(1247, 142);
            this.chart1.TabIndex = 78;
            this.chart1.Text = "chart1";
            // 
            // lvInput
            // 
            this.lvInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvInput.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Header1,
            this.Header2,
            this.Header3,
            this.Header4,
            this.Header5,
            this.Header6,
            this.Header7,
            this.Header8,
            this.Header9,
            this.Header10,
            this.Header11,
            this.Header12,
            this.Header13,
            this.Ignition,
            this.Header14,
            this.Header15,
            this.Header16,
            this.Header17,
            this.columnHeader1});
            this.lvInput.FullRowSelect = true;
            this.lvInput.GridLines = true;
            listViewGroup3.Header = "ListViewGroup";
            listViewGroup3.Name = "Global Variables";
            listViewGroup4.Header = "ListViewGroup";
            listViewGroup4.Name = "Local Variables";
            this.lvInput.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup3,
            listViewGroup4});
            this.lvInput.HideSelection = false;
            this.lvInput.Location = new System.Drawing.Point(5, 19);
            this.lvInput.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lvInput.MultiSelect = false;
            this.lvInput.Name = "lvInput";
            this.lvInput.ShowGroups = false;
            this.lvInput.Size = new System.Drawing.Size(1247, 113);
            this.lvInput.TabIndex = 77;
            this.lvInput.UseCompatibleStateImageBehavior = false;
            this.lvInput.View = System.Windows.Forms.View.Details;
            // 
            // Header1
            // 
            this.Header1.Text = "Step";
            this.Header1.Width = 62;
            // 
            // Header2
            // 
            this.Header2.Text = "Dyno SV";
            this.Header2.Width = 81;
            // 
            // Header3
            // 
            this.Header3.Text = "Dyno Mode";
            this.Header3.Width = 98;
            // 
            // Header4
            // 
            this.Header4.Text = "Dyno Ramp";
            this.Header4.Width = 94;
            // 
            // Header5
            // 
            this.Header5.Text = "Engine SV";
            this.Header5.Width = 86;
            // 
            // Header6
            // 
            this.Header6.Text = "Engine Mode";
            this.Header6.Width = 102;
            // 
            // Header7
            // 
            this.Header7.Text = "Engine Ramp";
            this.Header7.Width = 103;
            // 
            // Header8
            // 
            this.Header8.Text = "Settling Time";
            this.Header8.Width = 104;
            // 
            // Header9
            // 
            this.Header9.Text = "Measuring No.";
            this.Header9.Width = 114;
            // 
            // Header10
            // 
            this.Header10.Text = "Duration";
            this.Header10.Width = 79;
            // 
            // Header11
            // 
            this.Header11.Text = "Period";
            this.Header11.Width = 62;
            // 
            // Header12
            // 
            this.Header12.Text = "Rest Time";
            this.Header12.Width = 83;
            // 
            // Header13
            // 
            this.Header13.Text = "Total Time";
            this.Header13.Width = 86;
            // 
            // Ignition
            // 
            this.Ignition.Text = "Ignition";
            this.Ignition.Width = 0;
            // 
            // Header14
            // 
            this.Header14.Text = "Thermal Shock";
            this.Header14.Width = 0;
            // 
            // Header15
            // 
            this.Header15.Text = "Heater";
            this.Header15.Width = 0;
            // 
            // Header16
            // 
            this.Header16.Text = "Alarm Table";
            this.Header16.Width = 0;
            // 
            // Header17
            // 
            this.Header17.Text = "Goto";
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Iteration";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnLoad);
            this.groupBox1.Controls.Add(this.tbFileName);
            this.groupBox1.Controls.Add(this.lbFileList);
            this.groupBox1.Location = new System.Drawing.Point(10, 10);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(200, 286);
            this.groupBox1.TabIndex = 72;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Step Definition Load";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 194);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 12);
            this.label3.TabIndex = 73;
            this.label3.Text = "File Name";
            // 
            // btnLoad
            // 
            this.btnLoad.BackgroundImage = global::AtomUI.Properties.Resources.TransientRun_loadfromfile;
            this.btnLoad.FlatAppearance.BorderSize = 0;
            this.btnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoad.Location = new System.Drawing.Point(5, 234);
            this.btnLoad.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(174, 36);
            this.btnLoad.TabIndex = 72;
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // tbFileName
            // 
            this.tbFileName.Location = new System.Drawing.Point(5, 209);
            this.tbFileName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbFileName.Name = "tbFileName";
            this.tbFileName.Size = new System.Drawing.Size(174, 21);
            this.tbFileName.TabIndex = 71;
            // 
            // lbFileList
            // 
            this.lbFileList.FormattingEnabled = true;
            this.lbFileList.ItemHeight = 12;
            this.lbFileList.Location = new System.Drawing.Point(5, 19);
            this.lbFileList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lbFileList.Name = "lbFileList";
            this.lbFileList.Size = new System.Drawing.Size(174, 160);
            this.lbFileList.TabIndex = 70;
            this.lbFileList.SelectedIndexChanged += new System.EventHandler(this.lbFileList_SelectedIndexChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.groupBox10);
            this.groupBox5.Controls.Add(this.groupBox9);
            this.groupBox5.Controls.Add(this.groupBox7);
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox5.Location = new System.Drawing.Point(10, 308);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Size = new System.Drawing.Size(1479, 417);
            this.groupBox5.TabIndex = 74;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Step Mode Control Panel";
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this.chkLoggingCtn);
            this.groupBox10.Controls.Add(this.chkLoggingAvg);
            this.groupBox10.Controls.Add(this.chkLoggingConstant);
            this.groupBox10.Controls.Add(this.tbCycle);
            this.groupBox10.Controls.Add(this.lblCycle);
            this.groupBox10.Controls.Add(this.label21);
            this.groupBox10.Controls.Add(this.label1);
            this.groupBox10.Controls.Add(this.label15);
            this.groupBox10.Controls.Add(this.btnPause);
            this.groupBox10.Controls.Add(this.lbCurrentPhase);
            this.groupBox10.Controls.Add(this.lbPhaseLeftTime);
            this.groupBox10.Controls.Add(this.lbCurrentTime);
            this.groupBox10.Controls.Add(this.label41);
            this.groupBox10.Controls.Add(this.label25);
            this.groupBox10.Controls.Add(this.label22);
            this.groupBox10.Controls.Add(this.lbCurrentStep);
            this.groupBox10.Controls.Add(this.lbPhaseTotalTime);
            this.groupBox10.Controls.Add(this.label31);
            this.groupBox10.Controls.Add(this.label23);
            this.groupBox10.Controls.Add(this.lbTotalTime);
            this.groupBox10.Controls.Add(this.label20);
            this.groupBox10.Controls.Add(this.btnStop);
            this.groupBox10.Controls.Add(this.btnRun);
            this.groupBox10.Controls.Add(this.label17);
            this.groupBox10.Controls.Add(this.label16);
            this.groupBox10.Controls.Add(this.progressBar1);
            this.groupBox10.Location = new System.Drawing.Point(474, 248);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox10.Size = new System.Drawing.Size(999, 159);
            this.groupBox10.TabIndex = 35;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Command";
            // 
            // chkLoggingAvg
            // 
            this.chkLoggingAvg.AutoSize = true;
            this.chkLoggingAvg.Checked = true;
            this.chkLoggingAvg.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLoggingAvg.Location = new System.Drawing.Point(120, 21);
            this.chkLoggingAvg.Name = "chkLoggingAvg";
            this.chkLoggingAvg.Size = new System.Drawing.Size(76, 16);
            this.chkLoggingAvg.TabIndex = 159;
            this.chkLoggingAvg.Text = "평균 저장";
            this.chkLoggingAvg.UseVisualStyleBackColor = true;
            // 
            // chkLoggingConstant
            // 
            this.chkLoggingConstant.AutoSize = true;
            this.chkLoggingConstant.Checked = true;
            this.chkLoggingConstant.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLoggingConstant.Location = new System.Drawing.Point(20, 38);
            this.chkLoggingConstant.Name = "chkLoggingConstant";
            this.chkLoggingConstant.Size = new System.Drawing.Size(76, 16);
            this.chkLoggingConstant.TabIndex = 158;
            this.chkLoggingConstant.Text = "상시 저장";
            this.chkLoggingConstant.UseVisualStyleBackColor = true;
            // 
            // tbCycle
            // 
            this.tbCycle.BackColor = System.Drawing.Color.AliceBlue;
            this.tbCycle.Location = new System.Drawing.Point(484, 91);
            this.tbCycle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbCycle.Name = "tbCycle";
            this.tbCycle.Size = new System.Drawing.Size(56, 21);
            this.tbCycle.TabIndex = 81;
            this.tbCycle.Text = "1";
            this.tbCycle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblCycle
            // 
            this.lblCycle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lblCycle.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCycle.ForeColor = System.Drawing.Color.White;
            this.lblCycle.Location = new System.Drawing.Point(410, 91);
            this.lblCycle.Name = "lblCycle";
            this.lblCycle.Size = new System.Drawing.Size(84, 21);
            this.lblCycle.TabIndex = 48;
            this.lblCycle.Text = "1   /";
            this.lblCycle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(410, 69);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(130, 22);
            this.label21.TabIndex = 47;
            this.label21.Tag = "F";
            this.label21.Text = "CYCLE";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.label1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(221, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 21);
            this.label1.TabIndex = 46;
            this.label1.Text = "IDLE";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(221, 69);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(97, 22);
            this.label15.TabIndex = 45;
            this.label15.Tag = "F";
            this.label15.Text = "Current Mode";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnPause
            // 
            this.btnPause.BackColor = System.Drawing.Color.Transparent;
            this.btnPause.BackgroundImage = global::AtomUI.Properties.Resources.pause버튼_회;
            this.btnPause.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPause.FlatAppearance.BorderSize = 0;
            this.btnPause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPause.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnPause.ForeColor = System.Drawing.Color.White;
            this.btnPause.Location = new System.Drawing.Point(368, 14);
            this.btnPause.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(120, 45);
            this.btnPause.TabIndex = 43;
            this.btnPause.UseVisualStyleBackColor = false;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // lbCurrentPhase
            // 
            this.lbCurrentPhase.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbCurrentPhase.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbCurrentPhase.ForeColor = System.Drawing.Color.White;
            this.lbCurrentPhase.Location = new System.Drawing.Point(119, 91);
            this.lbCurrentPhase.Name = "lbCurrentPhase";
            this.lbCurrentPhase.Size = new System.Drawing.Size(97, 21);
            this.lbCurrentPhase.TabIndex = 42;
            this.lbCurrentPhase.Text = "RAMP";
            this.lbCurrentPhase.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbPhaseLeftTime
            // 
            this.lbPhaseLeftTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPhaseLeftTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbPhaseLeftTime.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbPhaseLeftTime.ForeColor = System.Drawing.Color.White;
            this.lbPhaseLeftTime.Location = new System.Drawing.Point(854, 91);
            this.lbPhaseLeftTime.Name = "lbPhaseLeftTime";
            this.lbPhaseLeftTime.Size = new System.Drawing.Size(140, 21);
            this.lbPhaseLeftTime.TabIndex = 42;
            this.lbPhaseLeftTime.Text = "0 Sec";
            this.lbPhaseLeftTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbCurrentTime
            // 
            this.lbCurrentTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCurrentTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbCurrentTime.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbCurrentTime.ForeColor = System.Drawing.Color.White;
            this.lbCurrentTime.Location = new System.Drawing.Point(854, 43);
            this.lbCurrentTime.Name = "lbCurrentTime";
            this.lbCurrentTime.Size = new System.Drawing.Size(140, 21);
            this.lbCurrentTime.TabIndex = 42;
            this.lbCurrentTime.Text = "0 Sec";
            this.lbCurrentTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label41
            // 
            this.label41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label41.ForeColor = System.Drawing.Color.White;
            this.label41.Location = new System.Drawing.Point(119, 69);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(97, 22);
            this.label41.TabIndex = 41;
            this.label41.Tag = "F";
            this.label41.Text = "Current Phase";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(854, 69);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(140, 22);
            this.label25.TabIndex = 41;
            this.label25.Tag = "F";
            this.label25.Text = "Phase Left Time";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(854, 17);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(140, 26);
            this.label22.TabIndex = 41;
            this.label22.Tag = "F";
            this.label22.Text = "Current Time";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbCurrentStep
            // 
            this.lbCurrentStep.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbCurrentStep.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbCurrentStep.ForeColor = System.Drawing.Color.White;
            this.lbCurrentStep.Location = new System.Drawing.Point(17, 91);
            this.lbCurrentStep.Name = "lbCurrentStep";
            this.lbCurrentStep.Size = new System.Drawing.Size(97, 21);
            this.lbCurrentStep.TabIndex = 40;
            this.lbCurrentStep.Text = "1";
            this.lbCurrentStep.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbPhaseTotalTime
            // 
            this.lbPhaseTotalTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPhaseTotalTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbPhaseTotalTime.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbPhaseTotalTime.ForeColor = System.Drawing.Color.White;
            this.lbPhaseTotalTime.Location = new System.Drawing.Point(715, 91);
            this.lbPhaseTotalTime.Name = "lbPhaseTotalTime";
            this.lbPhaseTotalTime.Size = new System.Drawing.Size(137, 21);
            this.lbPhaseTotalTime.TabIndex = 40;
            this.lbPhaseTotalTime.Text = "1";
            this.lbPhaseTotalTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label31.ForeColor = System.Drawing.Color.White;
            this.label31.Location = new System.Drawing.Point(17, 69);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(97, 22);
            this.label31.TabIndex = 39;
            this.label31.Tag = "";
            this.label31.Text = "Current STEP";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(715, 69);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(137, 22);
            this.label23.TabIndex = 39;
            this.label23.Tag = "";
            this.label23.Text = "Phase Total Time";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbTotalTime
            // 
            this.lbTotalTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTotalTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbTotalTime.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbTotalTime.ForeColor = System.Drawing.Color.White;
            this.lbTotalTime.Location = new System.Drawing.Point(715, 43);
            this.lbTotalTime.Name = "lbTotalTime";
            this.lbTotalTime.Size = new System.Drawing.Size(137, 21);
            this.lbTotalTime.TabIndex = 40;
            this.lbTotalTime.Text = "0 Sec";
            this.lbTotalTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(715, 17);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(137, 26);
            this.label20.TabIndex = 39;
            this.label20.Tag = "F";
            this.label20.Text = "Total Time";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.Color.Transparent;
            this.btnStop.BackgroundImage = global::AtomUI.Properties.Resources.stop버튼_회;
            this.btnStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnStop.FlatAppearance.BorderSize = 0;
            this.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStop.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnStop.ForeColor = System.Drawing.Color.Linen;
            this.btnStop.Location = new System.Drawing.Point(517, 14);
            this.btnStop.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(120, 45);
            this.btnStop.TabIndex = 38;
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnRun
            // 
            this.btnRun.BackColor = System.Drawing.Color.Transparent;
            this.btnRun.BackgroundImage = global::AtomUI.Properties.Resources.run버튼_회;
            this.btnRun.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRun.FlatAppearance.BorderSize = 0;
            this.btnRun.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRun.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnRun.ForeColor = System.Drawing.Color.White;
            this.btnRun.Location = new System.Drawing.Point(220, 14);
            this.btnRun.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(120, 45);
            this.btnRun.TabIndex = 37;
            this.btnRun.UseVisualStyleBackColor = false;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.label17.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(850, 119);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(144, 30);
            this.label17.TabIndex = 35;
            this.label17.Text = "0 %";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label16.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(5, 119);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(216, 30);
            this.label16.TabIndex = 34;
            this.label16.Tag = "F";
            this.label16.Text = "Progress";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.progressBar1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(182)))), ((int)(((byte)(81)))));
            this.progressBar1.Location = new System.Drawing.Point(220, 119);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(632, 30);
            this.progressBar1.TabIndex = 33;
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox9.Controls.Add(this.lbTGEAlpha);
            this.groupBox9.Controls.Add(this.label36);
            this.groupBox9.Controls.Add(this.lbTGDTorque);
            this.groupBox9.Controls.Add(this.label38);
            this.groupBox9.Controls.Add(this.lbTGDSpeed);
            this.groupBox9.Controls.Add(this.label40);
            this.groupBox9.Controls.Add(this.lbFBTorque);
            this.groupBox9.Controls.Add(this.label30);
            this.groupBox9.Controls.Add(this.lbFBAlpha);
            this.groupBox9.Controls.Add(this.label28);
            this.groupBox9.Controls.Add(this.lbFBSpeed);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Location = new System.Drawing.Point(8, 248);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox9.Size = new System.Drawing.Size(460, 159);
            this.groupBox9.TabIndex = 34;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Control IO Status";
            // 
            // lbTGEAlpha
            // 
            this.lbTGEAlpha.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbTGEAlpha.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbTGEAlpha.ForeColor = System.Drawing.Color.White;
            this.lbTGEAlpha.Location = new System.Drawing.Point(302, 114);
            this.lbTGEAlpha.Name = "lbTGEAlpha";
            this.lbTGEAlpha.Size = new System.Drawing.Size(131, 28);
            this.lbTGEAlpha.TabIndex = 48;
            this.lbTGEAlpha.Text = "0 RPM";
            this.lbTGEAlpha.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label36
            // 
            this.label36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label36.ForeColor = System.Drawing.Color.White;
            this.label36.Location = new System.Drawing.Point(303, 93);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(131, 21);
            this.label36.TabIndex = 47;
            this.label36.Tag = "F";
            this.label36.Text = "Target ENG Alpha";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbTGDTorque
            // 
            this.lbTGDTorque.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbTGDTorque.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbTGDTorque.ForeColor = System.Drawing.Color.White;
            this.lbTGDTorque.Location = new System.Drawing.Point(162, 114);
            this.lbTGDTorque.Name = "lbTGDTorque";
            this.lbTGDTorque.Size = new System.Drawing.Size(131, 28);
            this.lbTGDTorque.TabIndex = 46;
            this.lbTGDTorque.Text = "0 RPM";
            this.lbTGDTorque.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label38
            // 
            this.label38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label38.ForeColor = System.Drawing.Color.White;
            this.label38.Location = new System.Drawing.Point(163, 93);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(131, 21);
            this.label38.TabIndex = 45;
            this.label38.Tag = "F";
            this.label38.Text = "Target Dyno Torque";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbTGDSpeed
            // 
            this.lbTGDSpeed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbTGDSpeed.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbTGDSpeed.ForeColor = System.Drawing.Color.White;
            this.lbTGDSpeed.Location = new System.Drawing.Point(22, 114);
            this.lbTGDSpeed.Name = "lbTGDSpeed";
            this.lbTGDSpeed.Size = new System.Drawing.Size(131, 28);
            this.lbTGDSpeed.TabIndex = 44;
            this.lbTGDSpeed.Text = "0 RPM";
            this.lbTGDSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label40
            // 
            this.label40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label40.ForeColor = System.Drawing.Color.White;
            this.label40.Location = new System.Drawing.Point(23, 93);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(131, 21);
            this.label40.TabIndex = 43;
            this.label40.Tag = "F";
            this.label40.Text = "Target Dyno Speed";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbFBTorque
            // 
            this.lbFBTorque.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbFBTorque.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbFBTorque.ForeColor = System.Drawing.Color.White;
            this.lbFBTorque.Location = new System.Drawing.Point(162, 47);
            this.lbFBTorque.Name = "lbFBTorque";
            this.lbFBTorque.Size = new System.Drawing.Size(131, 28);
            this.lbFBTorque.TabIndex = 40;
            this.lbFBTorque.Text = "0 RPM";
            this.lbFBTorque.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(163, 26);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(131, 21);
            this.label30.TabIndex = 39;
            this.label30.Tag = "F";
            this.label30.Text = "Feedback Torque";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbFBAlpha
            // 
            this.lbFBAlpha.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbFBAlpha.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbFBAlpha.ForeColor = System.Drawing.Color.White;
            this.lbFBAlpha.Location = new System.Drawing.Point(302, 47);
            this.lbFBAlpha.Name = "lbFBAlpha";
            this.lbFBAlpha.Size = new System.Drawing.Size(131, 28);
            this.lbFBAlpha.TabIndex = 38;
            this.lbFBAlpha.Text = "0 RPM";
            this.lbFBAlpha.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(303, 26);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(131, 21);
            this.label28.TabIndex = 37;
            this.label28.Tag = "F";
            this.label28.Text = "Feedback Alpha";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbFBSpeed
            // 
            this.lbFBSpeed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbFBSpeed.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbFBSpeed.ForeColor = System.Drawing.Color.White;
            this.lbFBSpeed.Location = new System.Drawing.Point(22, 47);
            this.lbFBSpeed.Name = "lbFBSpeed";
            this.lbFBSpeed.Size = new System.Drawing.Size(131, 28);
            this.lbFBSpeed.TabIndex = 36;
            this.lbFBSpeed.Text = "0 RPM";
            this.lbFBSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(23, 26);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(131, 21);
            this.label18.TabIndex = 17;
            this.label18.Tag = "F";
            this.label18.Text = "Feedback Speed";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.lbEngineDuration);
            this.groupBox7.Controls.Add(this.lbDynoDuration);
            this.groupBox7.Controls.Add(this.tbEngineDuration);
            this.groupBox7.Controls.Add(this.tbDynoDuration);
            this.groupBox7.Controls.Add(this.lbEngine);
            this.groupBox7.Controls.Add(this.lbDyno);
            this.groupBox7.Controls.Add(this.chFeedBackEngine);
            this.groupBox7.Controls.Add(this.chFeedBackDyno);
            this.groupBox7.Location = new System.Drawing.Point(474, 19);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Size = new System.Drawing.Size(999, 224);
            this.groupBox7.TabIndex = 33;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "FeedBack Value";
            this.groupBox7.SizeChanged += new System.EventHandler(this.groupBox7_SizeChanged);
            // 
            // lbEngineDuration
            // 
            this.lbEngineDuration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbEngineDuration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.lbEngineDuration.ForeColor = System.Drawing.Color.White;
            this.lbEngineDuration.Location = new System.Drawing.Point(879, 138);
            this.lbEngineDuration.Name = "lbEngineDuration";
            this.lbEngineDuration.Size = new System.Drawing.Size(90, 26);
            this.lbEngineDuration.TabIndex = 83;
            this.lbEngineDuration.Tag = "F";
            this.lbEngineDuration.Text = "X Axis Duration";
            this.lbEngineDuration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbDynoDuration
            // 
            this.lbDynoDuration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbDynoDuration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.lbDynoDuration.ForeColor = System.Drawing.Color.White;
            this.lbDynoDuration.Location = new System.Drawing.Point(448, 138);
            this.lbDynoDuration.Name = "lbDynoDuration";
            this.lbDynoDuration.Size = new System.Drawing.Size(90, 26);
            this.lbDynoDuration.TabIndex = 82;
            this.lbDynoDuration.Tag = "F";
            this.lbDynoDuration.Text = "X Axis Duration";
            this.lbDynoDuration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbEngineDuration
            // 
            this.tbEngineDuration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.tbEngineDuration.Location = new System.Drawing.Point(747, 164);
            this.tbEngineDuration.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbEngineDuration.Name = "tbEngineDuration";
            this.tbEngineDuration.Size = new System.Drawing.Size(91, 21);
            this.tbEngineDuration.TabIndex = 81;
            this.tbEngineDuration.Text = "10";
            this.tbEngineDuration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbEngineDuration.TextChanged += new System.EventHandler(this.tbEngineDuration_TextChanged);
            // 
            // tbDynoDuration
            // 
            this.tbDynoDuration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.tbDynoDuration.Location = new System.Drawing.Point(316, 164);
            this.tbDynoDuration.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbDynoDuration.Name = "tbDynoDuration";
            this.tbDynoDuration.Size = new System.Drawing.Size(91, 21);
            this.tbDynoDuration.TabIndex = 80;
            this.tbDynoDuration.Text = "10";
            this.tbDynoDuration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbDynoDuration.TextChanged += new System.EventHandler(this.tbDynoDuration_TextChanged);
            // 
            // lbEngine
            // 
            this.lbEngine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbEngine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.lbEngine.ForeColor = System.Drawing.Color.White;
            this.lbEngine.Location = new System.Drawing.Point(879, 94);
            this.lbEngine.Name = "lbEngine";
            this.lbEngine.Size = new System.Drawing.Size(90, 26);
            this.lbEngine.TabIndex = 79;
            this.lbEngine.Tag = "F";
            this.lbEngine.Text = "Engine";
            this.lbEngine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbDyno
            // 
            this.lbDyno.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbDyno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.lbDyno.ForeColor = System.Drawing.Color.White;
            this.lbDyno.Location = new System.Drawing.Point(448, 94);
            this.lbDyno.Name = "lbDyno";
            this.lbDyno.Size = new System.Drawing.Size(90, 26);
            this.lbDyno.TabIndex = 77;
            this.lbDyno.Tag = "F";
            this.lbDyno.Text = "Dyno";
            this.lbDyno.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chFeedBackEngine
            // 
            this.chFeedBackEngine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chFeedBackEngine.BorderlineColor = System.Drawing.Color.Black;
            this.chFeedBackEngine.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea5.Name = "ChartArea1";
            this.chFeedBackEngine.ChartAreas.Add(chartArea5);
            legend5.Name = "Legend1";
            this.chFeedBackEngine.Legends.Add(legend5);
            this.chFeedBackEngine.Location = new System.Drawing.Point(442, 25);
            this.chFeedBackEngine.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chFeedBackEngine.Name = "chFeedBackEngine";
            series5.ChartArea = "ChartArea1";
            series5.Legend = "Legend1";
            series5.Name = "Series1";
            this.chFeedBackEngine.Series.Add(series5);
            this.chFeedBackEngine.Size = new System.Drawing.Size(548, 182);
            this.chFeedBackEngine.TabIndex = 74;
            this.chFeedBackEngine.Text = "chart3";
            // 
            // chFeedBackDyno
            // 
            this.chFeedBackDyno.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chFeedBackDyno.BorderlineColor = System.Drawing.Color.Black;
            this.chFeedBackDyno.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea6.Name = "ChartArea1";
            this.chFeedBackDyno.ChartAreas.Add(chartArea6);
            legend6.Name = "Legend1";
            this.chFeedBackDyno.Legends.Add(legend6);
            this.chFeedBackDyno.Location = new System.Drawing.Point(13, 25);
            this.chFeedBackDyno.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chFeedBackDyno.Name = "chFeedBackDyno";
            series6.ChartArea = "ChartArea1";
            series6.Legend = "Legend1";
            series6.Name = "Series1";
            this.chFeedBackDyno.Series.Add(series6);
            this.chFeedBackDyno.Size = new System.Drawing.Size(542, 182);
            this.chFeedBackDyno.TabIndex = 73;
            this.chFeedBackDyno.Text = "chart2";
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox6.Controls.Add(this.btnTN);
            this.groupBox6.Controls.Add(this.btnTA);
            this.groupBox6.Controls.Add(this.lblTN);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.btnNT);
            this.groupBox6.Controls.Add(this.btnNA);
            this.groupBox6.Controls.Add(this.btnHornOff);
            this.groupBox6.Controls.Add(this.btnReset);
            this.groupBox6.Controls.Add(this.btnAlarm);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.btnRemote);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Location = new System.Drawing.Point(8, 19);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Size = new System.Drawing.Size(460, 224);
            this.groupBox6.TabIndex = 31;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "SYSTEM STATUS";
            // 
            // btnTN
            // 
            this.btnTN.BackColor = System.Drawing.Color.Silver;
            this.btnTN.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTN.BackgroundImage")));
            this.btnTN.Enabled = false;
            this.btnTN.FlatAppearance.BorderSize = 0;
            this.btnTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTN.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnTN.Location = new System.Drawing.Point(150, 160);
            this.btnTN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnTN.Name = "btnTN";
            this.btnTN.Size = new System.Drawing.Size(66, 47);
            this.btnTN.TabIndex = 47;
            this.btnTN.UseVisualStyleBackColor = false;
            // 
            // btnTA
            // 
            this.btnTA.BackColor = System.Drawing.Color.Silver;
            this.btnTA.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTA.BackgroundImage")));
            this.btnTA.Enabled = false;
            this.btnTA.FlatAppearance.BorderSize = 0;
            this.btnTA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTA.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnTA.Location = new System.Drawing.Point(150, 64);
            this.btnTA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnTA.Name = "btnTA";
            this.btnTA.Size = new System.Drawing.Size(66, 47);
            this.btnTA.TabIndex = 46;
            this.btnTA.UseVisualStyleBackColor = false;
            // 
            // lblTN
            // 
            this.lblTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.lblTN.ForeColor = System.Drawing.Color.Black;
            this.lblTN.Location = new System.Drawing.Point(136, 130);
            this.lblTN.Name = "lblTN";
            this.lblTN.Size = new System.Drawing.Size(94, 21);
            this.lblTN.TabIndex = 45;
            this.lblTN.Text = "T/N";
            this.lblTN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(136, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 21);
            this.label6.TabIndex = 44;
            this.label6.Tag = "";
            this.label6.Text = "T/α";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNT
            // 
            this.btnNT.BackColor = System.Drawing.Color.Silver;
            this.btnNT.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNT.BackgroundImage")));
            this.btnNT.Enabled = false;
            this.btnNT.FlatAppearance.BorderSize = 0;
            this.btnNT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNT.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnNT.Location = new System.Drawing.Point(259, 160);
            this.btnNT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnNT.Name = "btnNT";
            this.btnNT.Size = new System.Drawing.Size(66, 47);
            this.btnNT.TabIndex = 43;
            this.btnNT.UseVisualStyleBackColor = false;
            // 
            // btnNA
            // 
            this.btnNA.BackColor = System.Drawing.Color.Silver;
            this.btnNA.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNA.BackgroundImage")));
            this.btnNA.Enabled = false;
            this.btnNA.FlatAppearance.BorderSize = 0;
            this.btnNA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNA.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnNA.Location = new System.Drawing.Point(259, 64);
            this.btnNA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnNA.Name = "btnNA";
            this.btnNA.Size = new System.Drawing.Size(66, 47);
            this.btnNA.TabIndex = 41;
            this.btnNA.UseVisualStyleBackColor = false;
            this.btnNA.Click += new System.EventHandler(this.btnNA_Click);
            // 
            // btnHornOff
            // 
            this.btnHornOff.BackColor = System.Drawing.Color.Silver;
            this.btnHornOff.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnHornOff.BackgroundImage")));
            this.btnHornOff.Enabled = false;
            this.btnHornOff.FlatAppearance.BorderSize = 0;
            this.btnHornOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHornOff.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnHornOff.Location = new System.Drawing.Point(367, 160);
            this.btnHornOff.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnHornOff.Name = "btnHornOff";
            this.btnHornOff.Size = new System.Drawing.Size(66, 47);
            this.btnHornOff.TabIndex = 39;
            this.btnHornOff.UseVisualStyleBackColor = false;
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.Silver;
            this.btnReset.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnReset.BackgroundImage")));
            this.btnReset.Enabled = false;
            this.btnReset.FlatAppearance.BorderSize = 0;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnReset.Location = new System.Drawing.Point(367, 64);
            this.btnReset.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(66, 47);
            this.btnReset.TabIndex = 39;
            this.btnReset.UseVisualStyleBackColor = false;
            // 
            // btnAlarm
            // 
            this.btnAlarm.BackColor = System.Drawing.Color.Silver;
            this.btnAlarm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAlarm.BackgroundImage")));
            this.btnAlarm.Enabled = false;
            this.btnAlarm.FlatAppearance.BorderSize = 0;
            this.btnAlarm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlarm.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAlarm.Location = new System.Drawing.Point(39, 160);
            this.btnAlarm.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAlarm.Name = "btnAlarm";
            this.btnAlarm.Size = new System.Drawing.Size(66, 47);
            this.btnAlarm.TabIndex = 38;
            this.btnAlarm.UseVisualStyleBackColor = false;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(353, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 21);
            this.label4.TabIndex = 34;
            this.label4.Text = "HORN OFF";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(245, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 21);
            this.label2.TabIndex = 37;
            this.label2.Text = "N/T";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(353, 34);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 21);
            this.label12.TabIndex = 34;
            this.label12.Text = "RESET";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(25, 130);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 21);
            this.label13.TabIndex = 33;
            this.label13.Text = "ALARM";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(245, 34);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(94, 21);
            this.label14.TabIndex = 32;
            this.label14.Tag = "";
            this.label14.Text = "N/α";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnRemote
            // 
            this.btnRemote.BackColor = System.Drawing.Color.Silver;
            this.btnRemote.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRemote.BackgroundImage")));
            this.btnRemote.Enabled = false;
            this.btnRemote.FlatAppearance.BorderSize = 0;
            this.btnRemote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemote.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnRemote.Location = new System.Drawing.Point(39, 64);
            this.btnRemote.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRemote.Name = "btnRemote";
            this.btnRemote.Size = new System.Drawing.Size(66, 47);
            this.btnRemote.TabIndex = 29;
            this.btnRemote.UseVisualStyleBackColor = false;
            this.btnRemote.Click += new System.EventHandler(this.btnRemote_Click);
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(25, 34);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 21);
            this.label10.TabIndex = 23;
            this.label10.Text = "REMOTE";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // chkLoggingCtn
            // 
            this.chkLoggingCtn.AutoSize = true;
            this.chkLoggingCtn.Checked = true;
            this.chkLoggingCtn.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLoggingCtn.Location = new System.Drawing.Point(120, 39);
            this.chkLoggingCtn.Name = "chkLoggingCtn";
            this.chkLoggingCtn.Size = new System.Drawing.Size(76, 16);
            this.chkLoggingCtn.TabIndex = 160;
            this.chkLoggingCtn.Text = "연속 저장";
            this.chkLoggingCtn.UseVisualStyleBackColor = true;
            // 
            // StepRun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.ClientSize = new System.Drawing.Size(1500, 727);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "StepRun";
            this.Text = "StepRun";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.StepRun_FormClosed);
            this.Load += new System.EventHandler(this.StepRun_Load);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chFeedBackEngine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chFeedBackDyno)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView lvInput;
        private System.Windows.Forms.ColumnHeader Header1;
        private System.Windows.Forms.ColumnHeader Header2;
        private System.Windows.Forms.ColumnHeader Header3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.TextBox tbFileName;
        private System.Windows.Forms.ListBox lbFileList;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Label lbCurrentPhase;
        private System.Windows.Forms.Label lbPhaseLeftTime;
        private System.Windows.Forms.Label lbCurrentTime;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lbCurrentStep;
        private System.Windows.Forms.Label lbPhaseTotalTime;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lbTotalTime;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label lbTGEAlpha;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label lbTGDTorque;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label lbTGDSpeed;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label lbFBTorque;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label lbFBAlpha;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label lbFBSpeed;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.DataVisualization.Charting.Chart chFeedBackEngine;
        private System.Windows.Forms.DataVisualization.Charting.Chart chFeedBackDyno;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnNT;
        private System.Windows.Forms.Button btnNA;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnAlarm;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnRemote;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ColumnHeader Header4;
        private System.Windows.Forms.ColumnHeader Header5;
        private System.Windows.Forms.ColumnHeader Header6;
        private System.Windows.Forms.ColumnHeader Header7;
        private System.Windows.Forms.ColumnHeader Header8;
        private System.Windows.Forms.ColumnHeader Header9;
        private System.Windows.Forms.ColumnHeader Header10;
        private System.Windows.Forms.ColumnHeader Header11;
        private System.Windows.Forms.ColumnHeader Header12;
        private System.Windows.Forms.ColumnHeader Header13;
        private System.Windows.Forms.ColumnHeader Header14;
        private System.Windows.Forms.ColumnHeader Header15;
        private System.Windows.Forms.ColumnHeader Header16;
        private System.Windows.Forms.ColumnHeader Header17;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lbDyno;
        private System.Windows.Forms.Label lbEngine;
        private System.Windows.Forms.TextBox tbEngineDuration;
        private System.Windows.Forms.TextBox tbDynoDuration;
        private System.Windows.Forms.Label lbEngineDuration;
        private System.Windows.Forms.Label lbDynoDuration;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ColumnHeader Ignition;
        private System.Windows.Forms.TextBox tbCycle;
        private System.Windows.Forms.Label lblCycle;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Button btnHornOff;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnTN;
        private System.Windows.Forms.Button btnTA;
        private System.Windows.Forms.Label lblTN;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkLoggingConstant;
        private System.Windows.Forms.CheckBox chkLoggingAvg;
        private System.Windows.Forms.CheckBox chkLoggingCtn;
    }
}