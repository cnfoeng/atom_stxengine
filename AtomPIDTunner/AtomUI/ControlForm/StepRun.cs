﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using AtomUI.Class;
using System.Windows.Forms.DataVisualization.Charting;
using ACS.SPiiPlusNET;
using System.Diagnostics;
using System.Threading;


namespace AtomUI.ControlForm
{
    public partial class StepRun : Form
    {
        Stopwatch _stopwatch = new Stopwatch();
        bool bLoggingF = false;
        PerforTimer timerDataLogging = null;
        Thread thLog;
        AtomUI pMain = null;


        public StepRun(AtomUI _pMain)
        {
            InitializeComponent();
            pMain = _pMain;

            timerDataLogging = new PerforTimer();
            timerDataLogging.MTimeoutInit(timer1.Interval, true);  // 처음 바로 시작

            thLog = new Thread(new ThreadStart(RunningStep));
            thLog.Priority = ThreadPriority.Highest;
            thLog.Start();

            lvInput.GetType().GetProperty("DoubleBuffered",
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic).SetValue(lvInput, true, null);

        }

        private void StepRun_Load(object sender, EventArgs e)
        {
            ChartInit();
            ChartInit(chFeedBackDyno);
            SeriesInit(chFeedBackDyno);

            ChartInit(chFeedBackEngine);
            SeriesInit();
            SeriesInit(chFeedBackEngine);

            GraphResize();

            FileBoxRefresh();

            tbFileName.Text = Ethercat.sysInfo.sysSettings.StepModeFileName.Substring(0, Ethercat.sysInfo.sysSettings.StepModeFileName.LastIndexOf('.'));
            DataLoad();
            Redraw();
        }

        private void DataLoad()
        {
            if (File.Exists(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.StepModeDir + "\\" + Ethercat.sysInfo.sysSettings.StepModeFileName))
            {
                lvInput.Items.Clear();

                StepMode.Load(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.StepModeDir + "\\" + Ethercat.sysInfo.sysSettings.StepModeFileName);
                DataRowCollection DT = StepMode.StepModeData.Data.Tables["StepMode"].Rows;

                for (int i = 0; i < DT.Count; i++)
                {
                    int Step = (int)DT[i][0];
                    ListViewItem itm = lvInput.Items.Add(Step.ToString());

                    for (int j = 1; j < StepMode.StepModeData.Data.Tables["StepMode"].Columns.Count; j++)
                    {
                        itm.SubItems.Add(DT[i][j].ToString());
                    }
                }

                // PCREAD +0x33 Automatic ON -> then enable button
                TotalTime = 0;
                for (int i = 0; i < StepMode.StepModeData.Data.Tables["StepMode"].Rows.Count; i++)
                {
                    TotalTime = TotalTime + (int)StepMode.StepModeData.Data.Tables["StepMode"].Rows[i][12];
                }

            }
        }

        private void Redraw()
        {
            //X축 데이터 타입
            chart1.Series[0].Points.Clear();
            chart1.Series[1].Points.Clear();

            DataRowCollection DT = StepMode.StepModeData.Data.Tables["StepMode"].Rows;

            if (DT.Count > 0)
            {
                int Time = 0;
                try
                {
                    DataPoint dataPoint1 = new DataPoint(0, 0);
                    DataPoint dataPoint2 = new DataPoint(0, 0);
                    chart1.Series[0].Points.Add(dataPoint1);
                    chart1.Series[1].Points.Add(dataPoint2);

                    for (int i = 0; i < DT.Count; i++)
                    {
                        int time1 = (int)DT[i][3];
                        int time2 = (int)DT[i][6];
                        double Dyno = (double)DT[i][1];
                        double Engine = (double)DT[i][4];

                        if (time1 > time2)
                            Time = Time + time1;
                        else
                            Time = Time + time2;

                        dataPoint1 = new DataPoint(Time, Dyno);
                        dataPoint2 = new DataPoint(Time, Engine);
                        chart1.Series[0].Points.Add(dataPoint1);
                        chart1.Series[1].Points.Add(dataPoint2);

                        int Count1 = (int)DT[i][8];
                        int Count2 = Count1 - 1;
                        if (Count2 < 0)
                            Count2 = 0;

                        Time = Time + (int)DT[i][7];

                        dataPoint1 = new DataPoint(Time, Dyno);
                        dataPoint2 = new DataPoint(Time, Engine);
                        chart1.Series[0].Points.Add(dataPoint1);
                        chart1.Series[1].Points.Add(dataPoint2);

                        Time = Time + (int)DT[i][9] * Count1 + (int)DT[i][10] * Count2;

                        dataPoint1 = new DataPoint(Time, Dyno);
                        dataPoint2 = new DataPoint(Time, Engine);
                        chart1.Series[0].Points.Add(dataPoint1);
                        chart1.Series[1].Points.Add(dataPoint2);

                        Time = Time + (int)DT[i][11];

                        dataPoint1 = new DataPoint(Time, Dyno);
                        dataPoint2 = new DataPoint(Time, Engine);
                        chart1.Series[0].Points.Add(dataPoint1);
                        chart1.Series[1].Points.Add(dataPoint2);
                    }
                }
                catch (Exception ex)
                {
                    CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n connect Error!!");
                }
            }

        }

        private void FileBoxRefresh()
        {
            lbFileList.Items.Clear();
            string[] Files = Directory.GetFiles(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.StepModeDir);
            foreach (string file in Files)
            {
                string filename = file.Substring(file.LastIndexOf('\\') + 1, file.Length - file.LastIndexOf('\\') - 1);
                string filenameonly = filename.Substring(0, filename.LastIndexOf('.'));
                lbFileList.Items.Add(filenameonly);
                if (Ethercat.sysInfo.sysSettings.StepModeFileName == file)
                {
                    lbFileList.SelectedValue = filename;
                }
            }
        }

        private void ChartInit()
        {
            //X축 데이터 타입
            chart1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(223)))), ((int)(((byte)(193)))));
            chart1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(64)))), ((int)(((byte)(1)))));
            this.chart1.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.chart1.BorderlineWidth = 2;

            chart1.ChartAreas[0].BackColor = System.Drawing.Color.OldLace;
            chart1.ChartAreas[0].BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.ChartAreas[0].BackSecondaryColor = System.Drawing.Color.White;
            chart1.ChartAreas[0].BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chart1.ChartAreas[0].AxisY2.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.DashDotDot;
            chart1.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].AxisY2.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));

            chart1.ChartAreas[0].CursorX.LineColor = Color.Gray;
            chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
        }

        private void ChartInit(Chart chart1)
        {
            //X축 데이터 타입
            chart1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(223)))), ((int)(((byte)(193)))));
            chart1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(64)))), ((int)(((byte)(1)))));
            this.chart1.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.chart1.BorderlineWidth = 2;

            chart1.ChartAreas[0].BackColor = System.Drawing.Color.OldLace;
            chart1.ChartAreas[0].BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.ChartAreas[0].BackSecondaryColor = System.Drawing.Color.White;
            chart1.ChartAreas[0].BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;

            //chart1.ChartAreas[0].CursorX.LineColor = Color.Gray;
            //chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            //chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;

            chart1.ChartAreas[0].AxisX.LabelStyle.Format = "#.0";
            chart1.ChartAreas[0].AxisX.IsLabelAutoFit = false;
            chart1.ChartAreas[0].AxisX.LabelStyle.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            chart1.ChartAreas[0].AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].AxisX.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].AxisY.IsLabelAutoFit = false;
            chart1.ChartAreas[0].AxisY.LabelStyle.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            chart1.ChartAreas[0].AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            //chart1.ChartAreas[0].AxisY.Maximum = 5000;
            //chart1.ChartAreas[0].AxisY.Minimum = 1000;

        }

        private void SeriesInit()
        {
            chart1.Series.Clear();
            chart1.Series.Add("Dyno");
            chart1.Series.Add("Engine");

            chart1.Series[0].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            //Y축 데이터 타입
            chart1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;  //분산형 그래프
            chart1.Series[1].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;  //분산형 그래프
            chart1.Series[0].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;

            //차트 타입
            chart1.Series[0].IsValueShownAsLabel = true;
            chart1.Series[1].IsValueShownAsLabel = true;
            chart1.Series[0].IsXValueIndexed = false;
            chart1.Series[1].IsXValueIndexed = false;
            chart1.Series[0].BorderWidth = 3;
            chart1.Series[1].BorderWidth = 3;
            chart1.Series[0].MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Diamond;
            chart1.Series[1].MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            chart1.Series[0].MarkerSize = 6;
            chart1.Series[0].MarkerBorderWidth = 2;
            chart1.Series[1].MarkerSize = 6;
            chart1.Series[1].MarkerBorderWidth = 2;
            chart1.Series[0].ShadowColor = System.Drawing.Color.Black;
            chart1.Series[1].ShadowColor = System.Drawing.Color.Black;
            chart1.Series[0].ShadowOffset = 2;
            chart1.Series[1].ShadowOffset = 2;
            chart1.Series[0].Color = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(65)))), ((int)(((byte)(140)))), ((int)(((byte)(240)))));
            chart1.Series[1].Color = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(224)))), ((int)(((byte)(64)))), ((int)(((byte)(10)))));

            //chart1.ChartAreas[0].CursorX.Position = 50;
            //chart1.ChartAreas[0].CursorX.LineColor = Color.Gray;
            //chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            //chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
        }

        private void SeriesInit(Chart chart1)
        {
            chart1.Series.Clear();
            chart1.Series.Add("Target");
            chart1.Series.Add("Feedback");

            chart1.Series[0].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            //Y축 데이터 타입
            chart1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;  //분산형 그래프
            chart1.Series[1].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;  //분산형 그래프
            chart1.Series[0].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;

            //차트 타입
            chart1.Series[0].IsXValueIndexed = false;
            chart1.Series[1].IsXValueIndexed = false;
            chart1.Series[0].Color = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(65)))), ((int)(((byte)(140)))), ((int)(((byte)(240)))));
            chart1.Series[1].Color = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(224)))), ((int)(((byte)(64)))), ((int)(((byte)(10)))));

            //chart1.ChartAreas[0].CursorX.Position = 50;
            //chart1.ChartAreas[0].CursorX.LineColor = Color.Gray;
            //chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            //chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
        }

        private void groupBox7_SizeChanged(object sender, EventArgs e)
        {
            GraphResize();
        }

        private void GraphResize()
        {
            chFeedBackDyno.Height = groupBox7.Height - 52;
            chFeedBackEngine.Height = groupBox7.Height - 52;

            chFeedBackDyno.Width = (groupBox7.Width - 52) / 2;
            chFeedBackEngine.Width = (groupBox7.Width - 52) / 2;

            chFeedBackEngine.Left = (groupBox7.Width / 2) + 10;

            lbDyno.Left = chFeedBackDyno.Left + chFeedBackDyno.Width - 110;
            lbDynoDuration.Left = chFeedBackDyno.Left + chFeedBackDyno.Width - 110;
            tbDynoDuration.Left = chFeedBackDyno.Left + chFeedBackDyno.Width - 110;

            lbEngine.Left = chFeedBackEngine.Left + chFeedBackEngine.Width - 110;
            lbEngineDuration.Left = chFeedBackEngine.Left + chFeedBackEngine.Width - 110;
            tbEngineDuration.Left = chFeedBackEngine.Left + chFeedBackEngine.Width - 110;
        }

        private bool run = false;
        //bool ready = false;
        bool pause = true;

        double TotalTime = 0;
        double dbCurrentTime;

        int CurrentStep = 0;
        int CurrentPhase = 0;
        //int CurrentMeasuring = 0;

        enum enumPhase
        {
            Start = 1,
            Ramp ,
            Settling ,
            Measuring ,
            Period ,
            Rest 
        }

        int DynoDuration = 10;
        int EngineDuration = 10;
        int CurrentCycle = 0;
        int TotalCycle = 0;


        private void RunningStep()
        {
            while (true)
            {
                timerDataLogging.MTimeoutStart();

                Dele_StatusUpdate();

                if (run & !pause)
                {
                    Running();
                    Dele_SetGraph(nCurrentIndex, dbCurrentTime);
                    Dele_SetTime(dbCurrentTime, PhaseCurrentTime);
                    dbCurrentTime = ((double)nCurrentIndex + 1) / (1000 / timer1.Interval);
                    PhaseCurrentTime += (double)timer1.Interval / 1000;

                    if (nCurrentIndex + 1 >= SetValueList.Count) // 종료
                    {
                        CurrentCycle++;
                        if (CurrentCycle > TotalCycle)
                        {
                            run = false;
                            _stopwatch.Stop();
                            MessageBox.Show("Step mode completed.\n\r" + _stopwatch.ElapsedMilliseconds.ToString() + "mSec Elapsed.", "Information");
                            _stopwatch.Reset();

                        }
                        else
                        {
                            Dele_InitCycle();

                            if (DaqData.DataLoggingConstant || DaqData.DataLoggingAvg || DaqData.DataLoggingCtn)
                                Thread.Sleep(1000);
                            if (DaqData.DataLoggingConstant || DaqData.DataLoggingAvg || DaqData.DataLoggingCtn)
                                Thread.Sleep(1000);
                            if (DaqData.DataLoggingConstant || DaqData.DataLoggingAvg || DaqData.DataLoggingCtn)
                                Thread.Sleep(1000);
                            if (DaqData.DataLoggingConstant || DaqData.DataLoggingAvg || DaqData.DataLoggingCtn)
                                Thread.Sleep(1000);
                            if (DaqData.DataLoggingConstant || DaqData.DataLoggingAvg || DaqData.DataLoggingCtn)
                                Thread.Sleep(1000);
                            if (DaqData.DataLoggingConstant || DaqData.DataLoggingAvg || DaqData.DataLoggingCtn)
                            {
                                LoggingStop();
                                Thread.Sleep(1000);
                            }
                            dbCurrentTime = 0;
                            nCurrentIndex = 0;
                            CurrentStep = 0;
                            PhaseCurrentTime = 0;
                        }
                        timerDataLogging.MTimeoutInit(timer1.Interval, true);
                        nCurrentIndex = 0;
                    }
                    else
                        nCurrentIndex++;
                }
            }
        }

       


        private void timer1_Tick(object sender, EventArgs e)
        {
            
        }

        double PrevDyno = 0;
        double PrevEngine = 0;
        string PrevDynoMode = "";
        string PrevEngineMode = "";
        int DAddress = 0;
        int EAddress = 0;

        int EngineRampTime = 0;
        int DynoRampTime = 0;
        //lbFBSpeed.Text = DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).RealValue.ToString() + " " + DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).Unit;
        //    lbFBTorque.Text = DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).RealValue.ToString() + " " + DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).Unit;
        //    lbFBAlpha.Text = DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).RealValue.ToString() + " " + DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).Unit;
        //    lbTGDSpeed.Text = Ethercat.g_TargetValue[0].ToString() + " " + DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).Unit;
        //    lbTGDTorque.Text = Ethercat.g_TargetValue[1].ToString() + " " + DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).Unit;
        //    lbTGEAlpha.Text = Ethercat.g_TargetValue[2].ToString() + " " + DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).Unit;

        double[] t_TargetValue = null;
        private void Running()
        {
            if (CurrentStep != SetValueList[nCurrentIndex].Step)
            {
                CurrentStep = SetValueList[nCurrentIndex].Step;
                Dele_SetlvInput();
            }

            if (CurrentPhase != (int)SetValueList[nCurrentIndex].Phase)
            {
                CurrentPhase = (int)SetValueList[nCurrentIndex].Phase;
                PhaseTotalTime = (int)SetValueList[nCurrentIndex].PhaseTime;
                PhaseCurrentTime = 0;

                EngineRampTime = (int)SetValueList[nCurrentIndex].RampTime_Engine;
                DynoRampTime = (int)SetValueList[nCurrentIndex].RampTime_Dyno;

                if (CurrentPhase == (int)enumPhase.Ramp)
                {
                    if (chkLoggingConstant.Checked && !DaqData.DataLoggingConstant)
                    {
                        pMain.DataLoggingConstantStartStop(true, true, TotalTime);
                    }

                    if (PrevDynoMode != SetValueList[nCurrentIndex].dynoMode || nCurrentIndex == 0)
                    {
                        if (SetValueList[nCurrentIndex].dynoMode == "N")
                            PrevDyno = (double)(DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).RealValue);
                        else
                            PrevDyno = (double)(DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).RealValue);
                    }
                    else
                    {
                        PrevDyno = SetValueList[nCurrentIndex - 1].SetValueDyno;
                    }

                    if (PrevEngineMode != SetValueList[nCurrentIndex].engineMode || nCurrentIndex == 0)
                    {
                        if (SetValueList[nCurrentIndex].engineMode == "N")
                            PrevEngine = (double)(DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).RealValue);
                        else if (SetValueList[nCurrentIndex].engineMode == "T")
                            PrevEngine = (double)(DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).RealValue);
                        else
                            PrevEngine = (double)(DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).RealValue);
                    }
                    else
                    {
                        PrevEngine = SetValueList[nCurrentIndex - 1].SetValueEngine;
                    }
                    bLoggingF = false;
                }
                else if (CurrentPhase == (int)enumPhase.Measuring)
                {
                    if (!bLoggingF && SetValueList[nCurrentIndex].PhaseTime > 0)
                    {
                        bLoggingF = true;
                        if (chkLoggingAvg.Checked && !DaqData.DataLoggingAvg)
                        {
                            pMain.DataLoggingAvgStartStop(true, SetValueList[nCurrentIndex].PhaseTime);
                        }
                        if (chkLoggingCtn.Checked && !DaqData.DataLoggingCtn)
                        {
                            pMain.DataLoggingCtnStartStop(true, SetValueList[nCurrentIndex].PhaseTime);
                        }
                    }
                }
                else
                {
                    bLoggingF = false;
                }
            }

            CurrentMode = getMode(SetValueList[nCurrentIndex].dynoMode, SetValueList[nCurrentIndex].engineMode);
            PrevDynoMode = SetValueList[nCurrentIndex].dynoMode;
            PrevEngineMode = SetValueList[nCurrentIndex].engineMode;

            Dele_SetChart(dbCurrentTime);

            double CurrentDyno = 0;
            double CurrentEngine = 0;

            if (CurrentPhase == 1 && DynoRampTime > PhaseCurrentTime)
            {
                CurrentDyno = (SetValueList[nCurrentIndex].SetValueDyno - PrevDyno) * PhaseCurrentTime / PhaseTotalTime + PrevDyno;
            } else
            {
                CurrentDyno = SetValueList[nCurrentIndex].SetValueDyno;
            }

            if (CurrentPhase == 1 && EngineRampTime > PhaseCurrentTime)
            {
                CurrentEngine = (SetValueList[nCurrentIndex].SetValueEngine - PrevEngine) * PhaseCurrentTime / PhaseTotalTime + PrevEngine;
            }
            else
            {
                CurrentEngine = SetValueList[nCurrentIndex].SetValueEngine;
            }

            double[] SendData = null;

            if (CurrentMode == enumMode.ModeNA)
            {
                SendData = new double[3] { 1, 0, 0 };
                t_TargetValue = new double[3] { CurrentDyno, Ethercat.g_TargetValue[1], CurrentEngine };
            }
            else if (CurrentMode == enumMode.ModeNT)
            {
                SendData = new double[3] { 1, 0, 1 };
                t_TargetValue = new double[3] { CurrentDyno, CurrentEngine, Ethercat.g_TargetValue[2] };
            }
            else if (CurrentMode == enumMode.ModeTA)
            {
                SendData = new double[3] { 0, 1, 0 };
                t_TargetValue = new double[3] { Ethercat.g_TargetValue[0], CurrentDyno, CurrentEngine };
            }
            else if (CurrentMode == enumMode.ModeTN)
            {
                SendData = new double[3] { 0, 1, 1 };
                t_TargetValue = new double[3] { CurrentEngine, CurrentDyno, Ethercat.g_TargetValue[2] };
            }

            Ethercat.Ch.WriteVariable(SendData, "g_SystemState_ControlModeSignal", ProgramBuffer.ACSC_NONE, 0, 0, 0, 2);


            SendData = new double[3]
            {
                        Ethercat.GetIOValue(t_TargetValue[0], DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).CalibrationTable),
                        Ethercat.GetIOValue(t_TargetValue[1], DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).CalibrationTable),
                        Ethercat.GetIOValue(t_TargetValue[2], DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).CalibrationTable)
            };

            Ethercat.Ch.WriteVariable(SendData, "g_Target", ProgramBuffer.ACSC_NONE, 0, 0, 0, 2);

            //PC_Write[DAddress] = SetValueList[CurrentIndex].SetValueDyno;
            //PC_Write[EAddress] = SetValueList[CurrentIndex].SetValueEngine;

            //Ethercat.Ch.WriteVariable(PC_Write, "g_PC_Write_Data", ProgramBuffer.ACSC_NONE, 0, PC_Write.Length - 1, -1, -1);
        }



        public void Dele_InitCycle()
        {
            if (this.InvokeRequired)
            {
                object[] p = { };
                this.BeginInvoke(new dlInitCycle(InitCycle), p);
            }
            else
            {
                InitCycle();
            }
        }
        private delegate void dlInitCycle();
        private void InitCycle()
        {
            chFeedBackDyno.Series[0].Points.Clear();
            chFeedBackDyno.Series[1].Points.Clear();
            chFeedBackEngine.Series[0].Points.Clear();
            chFeedBackEngine.Series[1].Points.Clear();
            lblCycle.Text = CurrentCycle.ToString() + "  /";
        }


        public void Dele_SetlvInput()
        {
            if (this.InvokeRequired)
            {
                object[] p = { };
                this.BeginInvoke(new dlSetlvInput(SetlvInput), p);
            }
            else
            {
                SetlvInput();
            }
        }
        private delegate void dlSetlvInput();
        private void SetlvInput()
        {
            lvInput.Items[CurrentStep].Selected = true;
            lvInput.EnsureVisible(CurrentStep);
        }


        public void Dele_SetChart(double dbCurrT)
        {
            if (this.InvokeRequired)
            {
                object[] p = { dbCurrT };
                this.BeginInvoke(new dlSetChart(SetChart), p);
            }
            else
            {
                SetChart(dbCurrT);
            }
        }
        private delegate void dlSetChart(double dbCurrT);
        private void SetChart(double dbCurrT)
        {
            chart1.ChartAreas[0].CursorX.SetCursorPosition(dbCurrT);
        }












        public void Dele_StatusUpdate()
        {
            if (this.InvokeRequired)
            {
                object[] p = { };
                this.BeginInvoke(new dlStatusUpdate(StatusUpdate), p);
            }
            else
            {
                StatusUpdate();
            }
        }
        private delegate void dlStatusUpdate();
        private void StatusUpdate()
        {
            COntrolHWStatusUpdate();
            ButtonControl();
        }

        private void COntrolHWStatusUpdate()
        {
            ButtonStatusChange(btnRemote, (Ethercat.g_SystemState[0] == 0 ? false : true), true);
            ButtonStatusChange(btnNA, ((Ethercat.g_SystemState[1] == 1 && Ethercat.g_SystemState[2] == 0) ? true : false), true);
            ButtonStatusChange(btnNT, ((Ethercat.g_SystemState[1] == 0 && Ethercat.g_SystemState[2] == 1) ? true : false), true);
            ButtonStatusChange(btnTA, ((Ethercat.g_SystemState[1] == 0 && Ethercat.g_SystemState[2] == 0) ? true : false), true);
            ButtonStatusChange(btnTN, ((Ethercat.g_SystemState[1] == 1 && Ethercat.g_SystemState[2] == 1) ? true : false), true);
            ButtonStatusChange(btnAlarm, (Ethercat.g_SystemState[3] == 0 ? false : true), true);
            ButtonStatusChange(btnReset, (Ethercat.g_SystemState[4] == 0 ? false : true), true);
            ButtonStatusChange(btnHornOff, (Ethercat.g_SystemState[5] == 0 ? false : true), true);

            lbFBSpeed.Text = DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).RealValue.ToString() + " " + DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).Unit;
            lbFBTorque.Text = DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).RealValue.ToString() + " " + DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).Unit;
            lbFBAlpha.Text = DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).RealValue.ToString() + " " + DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).Unit;
            lbTGDSpeed.Text = Math.Round(Ethercat.g_TargetValue[0], DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).Round).ToString() + " " + DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).Unit;
            lbTGDTorque.Text = Math.Round(Ethercat.g_TargetValue[1], DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).Round).ToString() + " " + DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).Unit;
            lbTGEAlpha.Text = Math.Round(Ethercat.g_TargetValue[2], DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).Round).ToString() + " " + DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).Unit;
            //g_FeedbackValue;
        }

        double PhaseTotalTime = 0;
        double PhaseCurrentTime = 0;
        public void Dele_SetTime(double dbCurrT, double dbPhaseT)
        {
            if (this.InvokeRequired)
            {
                object[] p = { dbCurrT, dbPhaseT };
                this.BeginInvoke(new dlSetTime(SetTime), p);
            }
            else
            {
                SetTime(dbCurrT, dbPhaseT);
            }
        }
        private delegate void dlSetTime(double dbCurrT, double dbPhaseT);
        private void SetTime(double dbCurrT, double dbPhaseT)
        {
            lbTotalTime.Text = TotalTime.ToString("#0.0") + " Sec";
            lbCurrentTime.Text = dbCurrT.ToString("#0.0") + " Sec";
            lbCurrentStep.Text = "STEP " + (CurrentStep+1).ToString();

            switch (CurrentPhase)
            {
                case ((int)enumPhase.Ramp):
                    lbCurrentPhase.Text = "Ramp";
                    break;
                case ((int)enumPhase.Settling):
                    lbCurrentPhase.Text = "Settling";
                    break;
                case ((int)enumPhase.Measuring):
                    lbCurrentPhase.Text = "Measuring";
                    break;
                case ((int)enumPhase.Period):
                    lbCurrentPhase.Text = "Period";
                    break;
                case ((int)enumPhase.Rest):
                    lbCurrentPhase.Text = "Rest";
                    break;
            }
            switch ((int)CurrentMode)
            {
                case ((int)enumMode.ModeIdle):
                    label1.Text = "Idle";
                    break;
                case ((int)enumMode.ModeIdleControl):
                    label1.Text = "IdelControl";
                    break;
                case ((int)enumMode.ModeNA):
                    label1.Text = "NA Mode";
                    break;
                case ((int)enumMode.ModeNT):
                    label1.Text = "NT Mode";
                    break;
                case ((int)enumMode.ModeTA):
                    label1.Text = "TA Mode";
                    break;
                case ((int)enumMode.ModeTN):
                    label1.Text = "TN Mode";
                    break;
            }

            lbPhaseTotalTime.Text = PhaseTotalTime.ToString("#0.0") + " Sec";
            lbPhaseLeftTime.Text = (PhaseTotalTime - dbPhaseT).ToString("#0.0") + " Sec";

            progressBar1.Maximum = (int)TotalTime;
            progressBar1.Value = ((int)dbCurrT < progressBar1.Maximum)? (int)dbCurrT : progressBar1.Maximum;
            label17.Text = ((dbCurrT / TotalTime * 100)).ToString("#0.0") + " %";
        }
        private void ButtonControl()
        {
            int AutoModeOnFromHMI = Ethercat.g_SystemState[0];
            if ((run == false & AutoModeOnFromHMI == 1) || CommonFunction.bUseDebug)
            {
                btnRun.Enabled = true;
                btnRun.BackgroundImage = global::AtomUI.Properties.Resources.run버튼;
            }
            else
            {
                btnRun.Enabled = false;
                btnRun.BackgroundImage = global::AtomUI.Properties.Resources.run버튼_회;
            }
            if (run)
            {
                btnPause.Enabled = true;
                btnStop.Enabled = true;
                btnPause.BackgroundImage = global::AtomUI.Properties.Resources.pause버튼;
                btnStop.BackgroundImage = global::AtomUI.Properties.Resources.stop버튼;
            }
            else
            {
                btnPause.Enabled = false;
                btnStop.Enabled = false;
                btnPause.BackgroundImage = global::AtomUI.Properties.Resources.pause버튼_회;
                btnStop.BackgroundImage = global::AtomUI.Properties.Resources.stop버튼_회;
            }
        }
        public void Dele_SetGraph(int nCurrentIndex, double dbCurrT)
        {
            if (this.InvokeRequired)
            {
                object[] p = { nCurrentIndex , dbCurrT };
                this.BeginInvoke(new dlSetGraph(SetGraph), p);
            }
            else
            {
                SetGraph(nCurrentIndex, dbCurrT);
            }
        }
        private delegate void dlSetGraph(int nCurrentIndex, double dbCurrT);
        private void SetGraph(int nCurrentIndex, double dbCurrT)
        {
            RemoveChart(chFeedBackDyno);
            RemoveChart(chFeedBackEngine);

            //CurrentTime = CurrentTime + 0.1;
            //PhaseCurrentTime = PhaseCurrentTime + 0.1;

            DataRowCollection DT = TransientMode.TransientModeData.Data.Tables["TransientMode"].Rows;

            double Target = 0.0;
            double FeedBack = 0.0;
            if (SetValueList[nCurrentIndex].dynoMode == "N")
            {
                Target = t_TargetValue[0];
                FeedBack = DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).RealValue;
            }
            else
            {
                Target = t_TargetValue[1];
                FeedBack = DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).RealValue;
            }
            //if (chFeedBackDyno.Series[0].Points.Count > DynoDuration*10 + 1)
            //{
            //    chFeedBackDyno.Series[0].Points.RemoveAt(0);
            //    chFeedBackDyno.Series[1].Points.RemoveAt(0);
            //}

            chFeedBackDyno.Series[0].Points.Add(new DataPoint(dbCurrT, Target));
            chFeedBackDyno.Series[1].Points.Add(new DataPoint(dbCurrT, FeedBack));
            if (chFeedBackDyno.Series[0].Points.Count > 0)
            {
                double Max = chFeedBackDyno.Series[0].Points[chFeedBackDyno.Series[0].Points.Count - 1].XValue;
                if (Max > DynoDuration)
                {
                    chFeedBackDyno.ChartAreas[0].AxisX.Minimum = Max - DynoDuration;
                    chFeedBackDyno.ChartAreas[0].AxisX.Maximum = Max;
                }
                else
                {
                    chFeedBackDyno.ChartAreas[0].AxisX.Minimum = 0;
                    chFeedBackDyno.ChartAreas[0].AxisX.Maximum = DynoDuration;
                }
                chFeedBackDyno.Invalidate();
            }

            if (SetValueList[nCurrentIndex].engineMode == "T")
            {
                Target = t_TargetValue[1];
                FeedBack = DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).RealValue;
            }
            else
            {
                Target = t_TargetValue[2];
                FeedBack = DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).RealValue;
            }

            //if (chFeedBackEngine.Series[0].Points.Count > EngineDuration * 10 + 1)
            //{
            //    chFeedBackEngine.Series[0].Points.RemoveAt(0);
            //    chFeedBackEngine.Series[1].Points.RemoveAt(0);
            //}
            chFeedBackEngine.Series[0].Points.Add(new DataPoint(dbCurrT, Target));
            chFeedBackEngine.Series[1].Points.Add(new DataPoint(dbCurrT, FeedBack));
            if (chFeedBackEngine.Series[0].Points.Count > 0)
            {
                double Max = chFeedBackEngine.Series[0].Points[chFeedBackEngine.Series[0].Points.Count - 1].XValue;
                if (Max > EngineDuration)
                {
                    chFeedBackEngine.ChartAreas[0].AxisX.Minimum = Max - EngineDuration;
                    chFeedBackEngine.ChartAreas[0].AxisX.Maximum = Max;
                } else
                {
                    chFeedBackEngine.ChartAreas[0].AxisX.Minimum = 0;
                    chFeedBackEngine.ChartAreas[0].AxisX.Maximum = EngineDuration;
                }
                chFeedBackEngine.Invalidate();
            }
        }
        private void RemoveChart(Chart chart)
        {
            if (chart.Series[0].Points.Count > 10000) // 메모리풀, 화면갱신 안정때문에 
            {
                int nCnt = 0;
                while (nCnt++ < 2)
                {
                    chart.Series[0].Points.RemoveAt(0);
                    chart.Series[1].Points.RemoveAt(0);
                }
            }
        }


        private void SetValues()
        {
            lbFBSpeed.Text = (DaqData.SystemStatus.GetValue("FeedbackSpeed")).ToString() + " RPM";
            lbFBTorque.Text = (DaqData.SystemStatus.GetValue("FeedbackTorque")).ToString() + " Nm";
            lbFBAlpha.Text = (DaqData.SystemStatus.GetValue("FeedbackAlpha")).ToString() + " %";

            lbTGDSpeed.Text = (DaqData.SystemStatus.GetValue("TargetDynoSpeed")).ToString() + " RPM";
            lbTGDTorque.Text = (DaqData.SystemStatus.GetValue("TargetDynoTorque")).ToString() + " Nm";
            lbTGEAlpha.Text = (DaqData.SystemStatus.GetValue("TargetEngAlpha")).ToString() + " %";
            //lbTGETorque.Text = (DaqData.SystemStatus.GetValue("TargetEngTorque")).ToString() + " Nm";
        }

        private void ButtonStatusChange(Button btn, bool status, bool enable)
        {
            if (status)
            {
                btn.BackgroundImage = global::AtomUI.Properties.Resources.on버튼;
            }
            else
            {
                if (enable)
                    btn.BackgroundImage = global::AtomUI.Properties.Resources.off버튼;
                else
                    btn.BackgroundImage = global::AtomUI.Properties.Resources.off버튼_회색;
            }

            btn.Enabled = enable;
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Load setting from File. Current setting will be overwriting. Continue?", "Step Mode Save", MessageBoxButtons.YesNo))
            {
                Ethercat.sysInfo.sysSettings.StepModeFileName = tbFileName.Text + ".xml";
                Ethercat.sysInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SysInfoDir + "\\" + Ethercat.sysInfo.sysSettings.SysInfoFileName);
                DataLoad();
                Redraw();
            }
        }

        private void tbDynoDuration_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DynoDuration = int.Parse(tbDynoDuration.Text);
            } catch
            {

            }
        }

        private void tbEngineDuration_TextChanged(object sender, EventArgs e)
        {
            try
            {
                EngineDuration = int.Parse(tbEngineDuration.Text);
            }
            catch
            {

            }
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            // PCREAD +0x33 Automatic ON -> then enable button
            DaqData.InitDaqDataLogging();

            ProfileMake();
            nCurrentIndex = 0;
            CurrentMode = getMode(SetValueList[nCurrentIndex].dynoMode, SetValueList[nCurrentIndex].engineMode);
            //ready = true;
            dbCurrentTime = 0;
            CurrentStep = 0;
            CurrentPhase = 0;
            PhaseTotalTime = 0;
            PhaseCurrentTime = 0;
            CurrentMode = 0;

            TotalTime = 0;
            CurrentCycle = 1;
            TotalCycle = int.Parse(tbCycle.Text);
            lblCycle.Text = CurrentCycle.ToString() + "  /";


            for (int i=0;i< StepMode.StepModeData.Data.Tables["StepMode"].Rows.Count;i++)
            {
                TotalTime = TotalTime + (int)StepMode.StepModeData.Data.Tables["StepMode"].Rows[i][12];
            }
            dbCurrentTime = 0;
            nCurrentIndex = 0;
            CurrentStep = 0;
            chFeedBackDyno.Series[0].Points.Clear();
            chFeedBackDyno.Series[1].Points.Clear();
            chFeedBackEngine.Series[0].Points.Clear();
            chFeedBackEngine.Series[1].Points.Clear();
            // Step Mode Setting
            //Ethercat.Ch.WriteVariable(0, "AutoMd_TestCycle", ProgramBuffer.ACSC_BUFFER_4, -1, -1, -1, -1);
            // PCWrite : AUTONormal, AUTOIdle, AUTOSWON, ModeChange, ProfileComplete
            CurrentMode = getMode(SetValueList[nCurrentIndex].dynoMode, SetValueList[nCurrentIndex].engineMode);
            CurrentPhase = 0;

            //PCWriteMake((int)enumAutoError.AUTONormal, (int)enumAutoMode.AUTOIdle, (int)enumAutoSW.AUTOSWON, (int)enumControl.ControlRun, CurrentMode, (int)enumProfileStatus.ProfileComplete);
            //PC_Write[0x120] = SetValueList[CurrentIndex].Ignition;
            //Ethercat.Ch.WriteVariable(PC_Write, "g_PC_Write_Data", ProgramBuffer.ACSC_NONE, 0, PC_Write.Length - 1, -1, -1);

            run = true;
            pause = false;
            //ready = false;
            tbDynoDuration.Text = TotalTime.ToString();
            tbEngineDuration.Text = TotalTime.ToString();
            _stopwatch.Start();

        }
        int nCurrentIndex = 0;
        private void btnPause_Click(object sender, EventArgs e)
        {
            if (pause)
                pause = false;
            else
                pause = true;
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            LoggingStop();
            // PCWrite : AUTONormal, AUTOIdle, AUTOSWON, ModeChange, ProfileComplete
            //CurrentMode = (int)getMode(SetValueList[CurrentIndex].dynoMode, SetValueList[CurrentIndex].engineMode);
            //PCWriteMake((int)enumAutoError.AUTONormal, (int)enumAutoMode.AUTOIdle, (int)enumAutoSW.AUTOSWON, (int)enumControl.ControlStop, CurrentMode, (int)enumProfileStatus.ProfileComplete);
            //PC_Write[0x120] = SetValueList[CurrentIndex].Ignition;
            //Ethercat.Ch.WriteVariable(PC_Write, "g_PC_Write_Data", ProgramBuffer.ACSC_NONE, 0, PC_Write.Length - 1, -1, -1);
            run = false;
        }

        private void btnReady_Click(object sender, EventArgs e)
        {
            //PCWriteMake(int AutoError, int AutoMode, int AutoSW, int Control, int Mode, int ProfileStatus)
            // PCWrite : AUTONormal, AUTOIdle, AUTOSWON, ModeIdle, ProfileWriting
            //try
            //{
                ProfileMake();
                nCurrentIndex = 0;
                CurrentMode = getMode(SetValueList[nCurrentIndex].dynoMode, SetValueList[nCurrentIndex].engineMode);
                //PCWriteMake((int)enumAutoError.AUTONormal, (int)enumAutoMode.AUTOIdle, (int)enumAutoSW.AUTOSWON, (int)enumControl.ControlStop, CurrentMode, (int)enumProfileStatus.ProfileNone);
                //PC_Write[0x120] = SetValueList[CurrentIndex].Ignition;

                //Ethercat.Ch.WriteVariable(PC_Write, "g_PC_Write_Data", ProgramBuffer.ACSC_NONE, 0, PC_Write.Length - 1, -1, -1);
                //ready = true;
                dbCurrentTime = 0;
                CurrentStep = 0;
                CurrentPhase = 0;
                PhaseTotalTime = 0;
                PhaseCurrentTime = 0;
                CurrentMode = 0;

                //MessageBox.Show("Profile Writing Success. \r\nPress AUTOMATIC button on Controlbox and Press run button on this windows.","Information");
            //} catch
            //{
            //    MessageBox.Show("Profile writing failed. Check ACS Controller or Connection!", "Error!");
            //}
        }


        enumMode CurrentMode;
        int AutoError;
        int AutoMode;
        int AutoSW;
        int Control;
        int Mode;
        int ProfileStatus;

        private void PCWriteMake(int AutoError, int AutoMode, int AutoSW, int Control, int Mode, int ProfileStatus)
        {
            this.AutoError = AutoError;
            this.AutoMode = AutoMode;
            this.AutoSW = AutoSW;
            this.Control = Control;
            this.Mode = Mode;
            this.ProfileStatus = ProfileStatus;

            PC_Write = new int[0x2FF];

            PC_Write[AutoError] = 1;
            PC_Write[AutoMode] = 1;
            PC_Write[AutoSW] = 1;
            PC_Write[Control] = 1;
            PC_Write[Mode] = 1;

            switch (Mode)
            {
                case (int)enumMode.ModeIdle:
                    break;
                case (int)enumMode.ModeIdleControl:
                    break;
                case (int)enumMode.ModeTA:
                    switch (ProfileStatus)
                    {
                        case (int)enumProfileStatus.ProfileWriting:
                            PC_Write[(int)PCWriteIndex.TAwriting] = 1;
                            break;
                        case (int)enumProfileStatus.ProfileComplete:
                            PC_Write[(int)PCWriteIndex.TAcomplete] = 1;
                            break;
                        case (int)enumProfileStatus.ProfileUsing:
                            PC_Write[(int)PCWriteIndex.TAcomplete] = 1;
                            PC_Write[(int)PCWriteIndex.TAusing] = 1;
                            break;
                    }
                    break;
                case (int)enumMode.ModeNA:
                    switch (ProfileStatus)
                    {
                        case (int)enumProfileStatus.ProfileWriting:
                            PC_Write[(int)PCWriteIndex.NAwriting] = 1;
                            break;
                        case (int)enumProfileStatus.ProfileComplete:
                            PC_Write[(int)PCWriteIndex.NAcomplete] = 1;
                            break;
                        case (int)enumProfileStatus.ProfileUsing:
                            PC_Write[(int)PCWriteIndex.NAcomplete] = 1;
                            PC_Write[(int)PCWriteIndex.NAusing] = 1;
                            break;
                    }
                    break;
                case (int)enumMode.ModeTN:
                    switch (ProfileStatus)
                    {
                        case (int)enumProfileStatus.ProfileWriting:
                            PC_Write[(int)PCWriteIndex.TNwriting] = 1;
                            break;
                        case (int)enumProfileStatus.ProfileComplete:
                            PC_Write[(int)PCWriteIndex.TNcomplete] = 1;
                            PC_Write[(int)PCWriteIndex.TNusing] = 1;
                            break;
                        case (int)enumProfileStatus.ProfileUsing:
                            PC_Write[(int)PCWriteIndex.TNcomplete] = 1;
                            PC_Write[(int)PCWriteIndex.TNusing] = 1;
                            break;
                    }
                    break;
                case (int)enumMode.ModeNT:
                    switch (ProfileStatus)
                    {
                        case (int)enumProfileStatus.ProfileWriting:
                            PC_Write[(int)PCWriteIndex.NTwriting] = 1;
                            break;
                        case (int)enumProfileStatus.ProfileComplete:
                            PC_Write[(int)PCWriteIndex.NTcomplete] = 1;
                            PC_Write[(int)PCWriteIndex.NTusing] = 1;
                            break;
                        case (int)enumProfileStatus.ProfileUsing:
                            PC_Write[(int)PCWriteIndex.NTcomplete] = 1;
                            PC_Write[(int)PCWriteIndex.NTusing] = 1;
                            break;
                    }
                    break;
            }

            PC_Write[0x100] = (DaqData.SystemStatus.GetValue("FeedbackTorque"));
            PC_Write[0x101] = (DaqData.SystemStatus.GetValue("FeedbackSpeed"));
            PC_Write[0x110] = (DaqData.SystemStatus.GetValue("FeedbackTorque"));
            PC_Write[0x111] = (DaqData.SystemStatus.GetValue("FeedbackSpeed"));
            PC_Write[0x112] = (DaqData.SystemStatus.GetValue("FeedbackAlpha"));

        }

        enum enumProfileStatus
        {
            ProfileNone = 0,
            ProfileWriting = 1,
            ProfileComplete = 2,
            ProfileUsing = 3
        }

        enum enumAutoError
        {
            AUTONormal = 0x06,
            AUTOFault = 0x07
        }
        enum enumAutoMode
        {
            AUTOInit = 0x09,
            AUTOIdle = 0x0A,
            AUTORun = 0x0B,
            AUTOPause = 0x0C,
            AUTODown = 0x0D
        }
        enum enumAutoSW
        {
            AUTOSWON = 0x20,
            AUTOSWOFF = 0x21
        }
        enum enumControl
        {
            ControlIdle = 0x31,
            ControlRun = 0x32,
            ControlStop = 0x33
        }
        enum enumMode
        {
            ModeIdle = 0x60,
            ModeIdleControl = 0x61,
            ModeTA = 0x62,
            ModeNA = 0x63,
            ModeTN = 0x64,
            ModeNT = 0x65
        }

        enum PCWriteIndex
        {
            PcStatus = 0,

            TAwriting = 0x90,
            TAcomplete = 0x91,
            TAusing = 0x92,

            NAwriting = 0x94,
            NAcomplete = 0x95,
            NAusing = 0x96,

            TNwriting = 0x98,
            TNcomplete = 0x99,
            TNusing = 0x9A,

            NTwriting = 0x9C,
            NTcomplete = 0x9D,
            NTusing = 0x9E,

            ControlCMD = 0x30,
            RunDriveMode = 0x60,
            //ProfileWrite = 0x90,
            eCatOutputData = 0x200
        }

        enum ProfileWriteIndex
        {
            Start = 0,
            Dyno = 100,
            Engine = 2600,
            DynoEngDevide = 2499
        }

        Dictionary<string, string> VarNameDic = new Dictionary<string, string>();

        private void DicInit()
        {
            VarNameDic.Clear();

            VarNameDic.Add("PC_Write", "g_PC_Write_Data");
            VarNameDic.Add("AutoMd", "AutoMd_TestCycle");
            VarNameDic.Add("TAMode", "g_PC_Write_Data_TqAlphaProfile");
            VarNameDic.Add("NAMode", "g_PC_Write_Data_SpAlphaProfile");
            VarNameDic.Add("TNMode", "g_PC_Write_Data_TqSpProfile");
            VarNameDic.Add("NTMode", "g_PC_Write_Data_SpTqProfile");
        }

        int[] PC_Write = new int[0x2FF];
        double[] Data_Profile = new double[5100];
        //string Data_Profile_Name = "";

        private void VarInit()
        {
            PC_Write = new int[0x2FF];
            Data_Profile = new double[5100];
        }

        List<SetValue> SetValueList = new List<SetValue>();

        class SetValue
        {
            public string dynoMode;
            public string engineMode;
            public enumPhase Phase;
            public int Step;
            public double SetValueEngine;
            public double SetValueDyno;
            public int PhaseTime;
            public int RampTime_Dyno;
            public int RampTime_Engine;
            public int Ignition;
            public int Thermal;
            public int Heater;

            public SetValue(string dynoMode, string engineMode, int Step, enumPhase Phase, int PhaseTime, int RampTime_Dyno, int RampTime_Engine, double SetValueDyno, double SetValueEngine, int Ignition, int Thermal, int Heater)
            {
                this.dynoMode = dynoMode;
                this.engineMode = engineMode;
                this.Step = Step;
                this.Phase = Phase;
                this.SetValueDyno = SetValueDyno;
                this.SetValueEngine = SetValueEngine;
                this.PhaseTime = PhaseTime;
                this.RampTime_Engine = RampTime_Engine;
                this.PhaseTime = PhaseTime;
                this.Ignition = Ignition;
                this.Thermal = Thermal;
                this.Heater = Heater;
            }
        }

        private void ProfileMake()
        {
            SetValueList.Clear();

            DataRowCollection DT = StepMode.StepModeData.Data.Tables["StepMode"].Rows;
            double PrevDyno = 0;
            double PrevEngine = 0;
            double CurrentDyno = 0;
            double CurrentEngine = 0;

            int Ignition = 0;
            int Thermal = 0;
            int Heater = 0;
            int CountPerSec = 0;
            int time1 = 0;
            int time2 = 0;

            enumPhase Phase;

            if (DT.Count > 0)
            {
                int Time = 0;
                string dynoMode = (string)DT[0][2];
                string engineMode = (string)DT[0][5];

                CountPerSec = 1000 / timer1.Interval;
                
                if (dynoMode == "N")
                    PrevDyno = DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).RealValue;
                else
                    PrevDyno = DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).RealValue;
                if (engineMode == "N")
                    PrevEngine = DaqData.FindVariable("AI_SPEED_ACT", DaqData.sysVarInfo.AI_Variables).RealValue;
                else if(engineMode == "T")
                    PrevEngine = DaqData.FindVariable("AI_TORQUE_ACT", DaqData.sysVarInfo.AI_Variables).RealValue;
                else
                    PrevEngine = DaqData.FindVariable("AI_THROTTLE_SET", DaqData.sysVarInfo.AI_Variables).RealValue;

                //PrevDyno = (double)DT[0][1];
                //PrevEngine = (double)DT[0][4];

                Ignition = ((bool)DT[0][13] ? 1 : 0);
                Thermal = ((bool)DT[0][14] ? 1 : 0);
                Heater = ((bool)DT[0][15] ? 1 : 0);

                Phase = enumPhase.Start;
                CurrentDyno = (double)DT[0][1];
                CurrentEngine = (double)DT[0][4];

                SetValueList.Add(new SetValue(dynoMode, engineMode, 0, Phase, Time, time1, time2, CurrentDyno, CurrentEngine, Ignition, Thermal, Heater));

                for (int i = 0; i < DT.Count; i++)
                {
                    dynoMode = (string)DT[i][2];
                    engineMode = (string)DT[i][5];
                    double Dyno = (double)DT[i][1];
                    double Engine = (double)DT[i][4];
                    Ignition = ((bool)DT[i][13] ? 1 : 0);
                    Thermal = ((bool)DT[i][14] ? 1 : 0);
                    Heater = ((bool)DT[i][15] ? 1 : 0);

                    Phase = enumPhase.Ramp;

                    time1 = (int)DT[i][3];
                    time2 = (int)DT[i][6]; 
                    if (time1 > time2)
                        Time = time1;
                    else
                        Time = time2;

                    if (time1 > time2)
                        Time = time1;
                    else
                        Time = time2;

                    for (int ind = 0; ind < Time * CountPerSec; ind++)
                    {
                        CurrentDyno = (double)((Dyno - PrevDyno) * ind / (Time * CountPerSec) + PrevDyno);
                        CurrentEngine = (double)((Engine - PrevEngine) * ind / (Time * CountPerSec) + PrevEngine);
                        //CurrentDyno = Dyno;
                        //CurrentEngine = Engine;
                        SetValueList.Add(new SetValue(dynoMode, engineMode, i, Phase, Time, 0,0,  CurrentDyno, CurrentEngine, Ignition, Thermal, Heater));
                    }

                    Time = (int)DT[i][7];
                    Phase = enumPhase.Settling;
                    for (int ind = 0; ind < Time * CountPerSec; ind++)
                    {
                        CurrentDyno = Dyno;
                        CurrentEngine = Engine;
                        SetValueList.Add(new SetValue(dynoMode, engineMode, i, Phase, Time, 0, 0, CurrentDyno, CurrentEngine, Ignition, Thermal, Heater));
                    }

                    int Count1 = (int)DT[i][8];
                    int Count2 = Count1 - 1;
                    if (Count2 < 0)
                        Count2 = 0;

                    for (int MeasureNo = 0; MeasureNo < Count1; MeasureNo++)
                    {
                        Phase = enumPhase.Measuring;
                        Time = (int)DT[i][9];
                        for (int Ave = 0; Ave < (int)DT[i][9] * CountPerSec; Ave++)
                        {
                            SetValueList.Add(new SetValue(dynoMode, engineMode, i, Phase, Time, 0, 0, CurrentDyno, CurrentEngine, Ignition, Thermal, Heater));
                        }

                        if (MeasureNo == Count2)
                            break;
                        Phase = enumPhase.Period;
                        Time = (int)DT[i][10];
                        for (int Ave = 0; Ave < (int)DT[i][10] * CountPerSec; Ave++)
                        {
                            SetValueList.Add(new SetValue(dynoMode, engineMode, i, Phase, Time, 0, 0, CurrentDyno, CurrentEngine, Ignition, Thermal, Heater));
                        }
                    }
                    Time = (int)DT[i][11];
                    Phase = enumPhase.Rest;
                    for (int ind = 0; ind < Time * CountPerSec; ind++)
                    {
                        CurrentDyno = Dyno;
                        CurrentEngine = Engine;
                        SetValueList.Add(new SetValue(dynoMode, engineMode, i, Phase, Time, 0, 0, CurrentDyno, CurrentEngine, Ignition, Thermal, Heater));
                    }

                    PrevDyno = CurrentDyno;
                    PrevEngine = CurrentEngine;
                }

            }

        }

        private enumMode getMode(string Dyno, string Engine)
        {
            enumMode CurrentMode;
            if (Dyno == "N")
            {
                if (Engine == "α")
                {
                    CurrentMode = enumMode.ModeNA;
                }
                else
                {
                    CurrentMode = enumMode.ModeNT;
                }
            }
            else
            {
                if (Engine == "α")
                {
                    CurrentMode = enumMode.ModeTA;
                }
                else
                {
                    CurrentMode = enumMode.ModeTN;
                }
            }

            return CurrentMode;
        }

        private void lbFileList_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbFileName.Text = lbFileList.SelectedItem.ToString();
        }

        private void btnRemote_Click(object sender, EventArgs e)
        {
            double[] SendData;
            if (Ethercat.g_SystemState[0] == 0)
                SendData = new double[1] { 1 };
            else
                SendData = new double[1] { 0 };

            Ethercat.Ch.WriteVariable(SendData, "g_SystemState_Remote", ProgramBuffer.ACSC_NONE, 0, 0, 0, 0);
            Ethercat.Ch.WriteVariable(1, "g_SystemState_Mode_Change", ProgramBuffer.ACSC_NONE, 0, 0, 0, 0);
        }

        private void btnNA_Click(object sender, EventArgs e)
        {

        }

        private void StepRun_FormClosed(object sender, FormClosedEventArgs e)
        {
            CloseStepRun();
        }

        public void CloseStepRun()
        {
            LoggingStop();
            if (thLog != null)
            {
                thLog.Abort();
                thLog.Join();
            }
        }
        private void LoggingStop()
        {
            if (DaqData.DataLoggingAvg)
            {
                pMain.DataLoggingAvgStartStop(false);
            }
            if (DaqData.DataLoggingCtn)
            {
                pMain.DataLoggingCtnStartStop(false);
            }
            if (DaqData.DataLoggingConstant)
            {
                pMain.DataLoggingConstantStartStop(false, true);
            }
        }



    }
}
