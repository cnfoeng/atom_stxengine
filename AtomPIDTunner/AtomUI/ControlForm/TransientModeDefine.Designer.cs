﻿namespace AtomUI.ControlForm
{
    partial class TransientModeDefine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tbFileName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbFileList = new System.Windows.Forms.ListBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgStepMode = new System.Windows.Forms.DataGridView();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rdDynoTQ = new System.Windows.Forms.RadioButton();
            this.rdDynoSP = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rdEngineTQ = new System.Windows.Forms.RadioButton();
            this.rdEngineALP = new System.Windows.Forms.RadioButton();
            this.lbTime = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbDyno = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbEngine = new System.Windows.Forms.Label();
            this.btnChannel = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgStepMode)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbFileName
            // 
            this.tbFileName.Location = new System.Drawing.Point(234, 36);
            this.tbFileName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbFileName.Name = "tbFileName";
            this.tbFileName.Size = new System.Drawing.Size(255, 21);
            this.tbFileName.TabIndex = 64;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(231, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 12);
            this.label3.TabIndex = 63;
            this.label3.Text = "File Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 12);
            this.label2.TabIndex = 61;
            this.label2.Text = "Saved File Name";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnChannel);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnLoad);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.tbFileName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.lbFileList);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(10, 10);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(494, 205);
            this.groupBox1.TabIndex = 66;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "CONTROL";
            // 
            // lbFileList
            // 
            this.lbFileList.FormattingEnabled = true;
            this.lbFileList.ItemHeight = 12;
            this.lbFileList.Location = new System.Drawing.Point(5, 36);
            this.lbFileList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lbFileList.Name = "lbFileList";
            this.lbFileList.Size = new System.Drawing.Size(224, 160);
            this.lbFileList.TabIndex = 62;
            this.lbFileList.SelectedIndexChanged += new System.EventHandler(this.lbFileList_SelectedIndexChanged);
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chart1.BorderlineColor = System.Drawing.Color.Black;
            this.chart1.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(522, 10);
            this.chart1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(433, 624);
            this.chart1.TabIndex = 67;
            this.chart1.Text = "chart1";
            this.chart1.CursorPositionChanged += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.CursorEventArgs>(this.chart1_CursorPositionChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.dgStepMode);
            this.groupBox2.Location = new System.Drawing.Point(10, 284);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(494, 350);
            this.groupBox2.TabIndex = 68;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "TRANSIENT MODE DATA";
            // 
            // dgStepMode
            // 
            this.dgStepMode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgStepMode.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgStepMode.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgStepMode.Location = new System.Drawing.Point(18, 25);
            this.dgStepMode.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgStepMode.Name = "dgStepMode";
            this.dgStepMode.RowTemplate.Height = 27;
            this.dgStepMode.Size = new System.Drawing.Size(456, 312);
            this.dgStepMode.TabIndex = 64;
            this.dgStepMode.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgStepMode_CellLeave);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rdDynoTQ);
            this.groupBox3.Controls.Add(this.rdDynoSP);
            this.groupBox3.Location = new System.Drawing.Point(10, 220);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(228, 59);
            this.groupBox3.TabIndex = 69;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dyno Mode";
            // 
            // rdDynoTQ
            // 
            this.rdDynoTQ.AutoSize = true;
            this.rdDynoTQ.Location = new System.Drawing.Point(126, 27);
            this.rdDynoTQ.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rdDynoTQ.Name = "rdDynoTQ";
            this.rdDynoTQ.Size = new System.Drawing.Size(73, 16);
            this.rdDynoTQ.TabIndex = 1;
            this.rdDynoTQ.Text = "TORQUE";
            this.rdDynoTQ.UseVisualStyleBackColor = true;
            // 
            // rdDynoSP
            // 
            this.rdDynoSP.AutoSize = true;
            this.rdDynoSP.Checked = true;
            this.rdDynoSP.Location = new System.Drawing.Point(28, 27);
            this.rdDynoSP.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rdDynoSP.Name = "rdDynoSP";
            this.rdDynoSP.Size = new System.Drawing.Size(63, 16);
            this.rdDynoSP.TabIndex = 0;
            this.rdDynoSP.TabStop = true;
            this.rdDynoSP.Text = "SPEED";
            this.rdDynoSP.UseVisualStyleBackColor = true;
            this.rdDynoSP.CheckedChanged += new System.EventHandler(this.rdDynoSP_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rdEngineTQ);
            this.groupBox4.Controls.Add(this.rdEngineALP);
            this.groupBox4.Location = new System.Drawing.Point(276, 220);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(228, 59);
            this.groupBox4.TabIndex = 70;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Engine Mode";
            // 
            // rdEngineTQ
            // 
            this.rdEngineTQ.AutoSize = true;
            this.rdEngineTQ.Location = new System.Drawing.Point(123, 27);
            this.rdEngineTQ.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rdEngineTQ.Name = "rdEngineTQ";
            this.rdEngineTQ.Size = new System.Drawing.Size(73, 16);
            this.rdEngineTQ.TabIndex = 3;
            this.rdEngineTQ.Text = "TORQUE";
            this.rdEngineTQ.UseVisualStyleBackColor = true;
            // 
            // rdEngineALP
            // 
            this.rdEngineALP.AutoSize = true;
            this.rdEngineALP.Checked = true;
            this.rdEngineALP.Location = new System.Drawing.Point(25, 27);
            this.rdEngineALP.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rdEngineALP.Name = "rdEngineALP";
            this.rdEngineALP.Size = new System.Drawing.Size(62, 16);
            this.rdEngineALP.TabIndex = 2;
            this.rdEngineALP.TabStop = true;
            this.rdEngineALP.Text = "ALPHA";
            this.rdEngineALP.UseVisualStyleBackColor = true;
            this.rdEngineALP.CheckedChanged += new System.EventHandler(this.rdEngineALP_CheckedChanged);
            // 
            // lbTime
            // 
            this.lbTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbTime.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbTime.ForeColor = System.Drawing.Color.White;
            this.lbTime.Location = new System.Drawing.Point(854, 120);
            this.lbTime.Name = "lbTime";
            this.lbTime.Size = new System.Drawing.Size(93, 21);
            this.lbTime.TabIndex = 72;
            this.lbTime.Text = "0 Sec";
            this.lbTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(854, 94);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(93, 26);
            this.label20.TabIndex = 71;
            this.label20.Tag = "F";
            this.label20.Text = "Time";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(854, 166);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 26);
            this.label4.TabIndex = 71;
            this.label4.Tag = "F";
            this.label4.Text = "Dyno";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbDyno
            // 
            this.lbDyno.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbDyno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbDyno.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbDyno.ForeColor = System.Drawing.Color.White;
            this.lbDyno.Location = new System.Drawing.Point(854, 193);
            this.lbDyno.Name = "lbDyno";
            this.lbDyno.Size = new System.Drawing.Size(93, 21);
            this.lbDyno.TabIndex = 72;
            this.lbDyno.Text = "0 Sec";
            this.lbDyno.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(854, 236);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 26);
            this.label6.TabIndex = 71;
            this.label6.Tag = "F";
            this.label6.Text = "Engine";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbEngine
            // 
            this.lbEngine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbEngine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbEngine.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbEngine.ForeColor = System.Drawing.Color.White;
            this.lbEngine.Location = new System.Drawing.Point(854, 262);
            this.lbEngine.Name = "lbEngine";
            this.lbEngine.Size = new System.Drawing.Size(93, 21);
            this.lbEngine.TabIndex = 72;
            this.lbEngine.Text = "0 Sec";
            this.lbEngine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnChannel
            // 
            this.btnChannel.BackColor = System.Drawing.Color.Transparent;
            this.btnChannel.BackgroundImage = global::AtomUI.Properties.Resources.csv_file_load;
            this.btnChannel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnChannel.FlatAppearance.BorderSize = 0;
            this.btnChannel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChannel.Location = new System.Drawing.Point(234, 114);
            this.btnChannel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnChannel.Name = "btnChannel";
            this.btnChannel.Size = new System.Drawing.Size(120, 36);
            this.btnChannel.TabIndex = 69;
            this.btnChannel.UseVisualStyleBackColor = false;
            this.btnChannel.Click += new System.EventHandler(this.btnChannel_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.BackgroundImage = global::AtomUI.Properties.Resources.Close;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(365, 157);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 36);
            this.btnClose.TabIndex = 68;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.BackColor = System.Drawing.Color.Transparent;
            this.btnLoad.BackgroundImage = global::AtomUI.Properties.Resources.Load_frome_file;
            this.btnLoad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnLoad.FlatAppearance.BorderSize = 0;
            this.btnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoad.Location = new System.Drawing.Point(365, 61);
            this.btnLoad.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(120, 36);
            this.btnLoad.TabIndex = 66;
            this.btnLoad.UseVisualStyleBackColor = false;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Transparent;
            this.btnSave.BackgroundImage = global::AtomUI.Properties.Resources.save_to_file;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(234, 61);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(120, 36);
            this.btnSave.TabIndex = 65;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // TransientModeDefine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(970, 643);
            this.Controls.Add(this.lbEngine);
            this.Controls.Add(this.lbDyno);
            this.Controls.Add(this.lbTime);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "TransientModeDefine";
            this.Text = "TransientModeDefine";
            this.Load += new System.EventHandler(this.TransientModeDefine_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgStepMode)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnChannel;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox tbFileName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox lbFileList;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgStepMode;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rdDynoSP;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rdDynoTQ;
        private System.Windows.Forms.RadioButton rdEngineTQ;
        private System.Windows.Forms.RadioButton rdEngineALP;
        private System.Windows.Forms.Label lbTime;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbDyno;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbEngine;
    }
}