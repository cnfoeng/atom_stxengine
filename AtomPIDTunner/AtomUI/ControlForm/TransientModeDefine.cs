﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using AtomUI.Class;
using System.Windows.Forms.DataVisualization.Charting;

namespace AtomUI.ControlForm
{
    public partial class TransientModeDefine : Form
    {
        public TransientModeDefine()
        {
            InitializeComponent();
        }

        private void TransientModeDefine_Load(object sender, EventArgs e)
        {
            ChartInit();
            FileBoxRefresh();
            tbFileName.Text = Ethercat.sysInfo.sysSettings.TranModeFileName.Substring(0, Ethercat.sysInfo.sysSettings.TranModeFileName.LastIndexOf('.'));
            DataLoad();

            //DataTable DT = TransientMode.TransientModeData.Data.Tables["TransientMode"];
            //dgStepMode.DataSource = TransientMode.TransientModeData.Data;
            //dgStepMode.DataMember = "TransientMode";

            Redraw();
        }

        private void FileBoxRefresh()
        {
            lbFileList.Items.Clear();
            string[] Files = Directory.GetFiles(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.TranModeDir);
            foreach (string file in Files)
            {
                string filename = file.Substring(file.LastIndexOf('\\') + 1, file.Length - file.LastIndexOf('\\') - 1);
                string filenameonly = filename.Substring(0, filename.LastIndexOf('.'));
                lbFileList.Items.Add(filenameonly);
                if (Ethercat.sysInfo.sysSettings.TranModeFileName == file)
                {
                    lbFileList.SelectedValue = filename;
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Save current setting to File. Continue?", "Transient Mode Save", MessageBoxButtons.YesNo))
            {
                Ethercat.sysInfo.sysSettings.TranModeFileName = tbFileName.Text + ".xml";
                Ethercat.sysInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SysInfoDir + "\\" + Ethercat.sysInfo.sysSettings.SysInfoFileName);

                TransientMode.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.TranModeDir + "\\" + Ethercat.sysInfo.sysSettings.TranModeFileName);
                //StepMode.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.StepModeDir + "\\" + Ethercat.sysInfo.sysSettings.StepModeFileName);
                FileBoxRefresh();
            }
        }

        private void DataLoad()
        {
            if (File.Exists(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.TranModeDir + "\\" + Ethercat.sysInfo.sysSettings.TranModeFileName))
            {
                //dgStepMode.Rows.Clear();
                dgStepMode.DataSource = null;
                //dgStepMode.DataMember = "TransientMode";

                TransientMode.Load(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.TranModeDir + "\\" + Ethercat.sysInfo.sysSettings.TranModeFileName);
                dgStepMode.DataSource = TransientMode.TransientModeData.Data;
                dgStepMode.DataMember = "TransientMode";

                if (TransientMode.TransientModeData.DM == DynoMode.Speed_Mode)
                {
                    rdDynoSP.Checked = true;
                }
                else
                {
                    rdDynoTQ.Checked = true;
                }
                if (TransientMode.TransientModeData.EM == EngineMode.Alpha_Mode)
                {
                    rdEngineALP.Checked = true;
                }
                else
                {
                    rdEngineTQ.Checked = true;
                }
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Load setting from File. Current setting will be overwriting. Continue?", "Step Mode Save", MessageBoxButtons.YesNo))
            {
                Ethercat.sysInfo.sysSettings.TranModeFileName = tbFileName.Text + ".xml";
                Ethercat.sysInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SysInfoDir + "\\" + Ethercat.sysInfo.sysSettings.SysInfoFileName);
                DataLoad();
                Redraw();
            }
        }

        private void ChartInit()
        {
            //X축 데이터 타입
            chart1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(223)))), ((int)(((byte)(193)))));
            chart1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(64)))), ((int)(((byte)(1)))));
            this.chart1.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.chart1.BorderlineWidth = 2;

            chart1.ChartAreas[0].BackColor = System.Drawing.Color.OldLace;
            chart1.ChartAreas[0].BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.ChartAreas[0].BackSecondaryColor = System.Drawing.Color.White;
            chart1.ChartAreas[0].BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chart1.ChartAreas[0].AxisY2.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.DashDotDot;
            chart1.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].AxisY2.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));

            chart1.Series.Clear();
            chart1.Series.Add("Dyno");
            chart1.Series.Add("Engine");

            chart1.Series[0].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            //Y축 데이터 타입
            chart1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;  //분산형 그래프
            chart1.Series[1].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;  //분산형 그래프
            chart1.Series[0].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;

            //차트 타입
            chart1.Series[0].IsXValueIndexed = false;
            chart1.Series[1].IsXValueIndexed = false;
            chart1.Series[0].Color = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(65)))), ((int)(((byte)(140)))), ((int)(((byte)(240)))));
            chart1.Series[1].Color = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(224)))), ((int)(((byte)(64)))), ((int)(((byte)(10)))));

            chart1.ChartAreas[0].CursorX.LineColor = Color.Gray;
            chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
        }

        private void Redraw()
        {
            //X축 데이터 타입
            chart1.Series[0].Points.Clear();
            chart1.Series[1].Points.Clear();

            if (dgStepMode.Rows.Count > 1)
            {
                try
                {

                    for (int i = 0; i < dgStepMode.Rows.Count; i++)
                    {
                        int Time = (int)dgStepMode[0, i].Value;
                        double Dyno = (double)dgStepMode[1, i].Value;
                        double Engine = (double)dgStepMode[2, i].Value;

                        DataPoint dataPoint1 = new DataPoint(Time, Dyno);
                        DataPoint dataPoint2 = new DataPoint(Time, Engine);
                        chart1.Series[0].Points.Add(dataPoint1);
                        chart1.Series[1].Points.Add(dataPoint2);
                    }
                }
                catch (Exception ex)
                {
                    CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n connect Error!!");
                }
            }

        }

        private void dgStepMode_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            Redraw();
        }

        private void btnChannel_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "CSV File Format|*.csv";
            openFileDialog1.Title = "Load an Transient Mode CSV File";
            openFileDialog1.FileName = "";
            if (DialogResult.OK == openFileDialog1.ShowDialog() && openFileDialog1.FileName != "")
            {
                CSVDataLoad(openFileDialog1.FileName);
            }
        }
        private void CSVDataLoad(string FileName)
        {
            if (File.Exists(FileName))
            {
                dgStepMode.DataSource = null;
                //dgStepMode.DataMember = "TransientMode";
                TransientMode.TransientModeData.LoadCSVFile(FileName);
                dgStepMode.DataSource = TransientMode.TransientModeData.Data;
                dgStepMode.DataMember = "TransientMode";

                if (TransientMode.TransientModeData.DM == DynoMode.Speed_Mode)
                {
                    rdDynoSP.Checked = true;
                }
                else
                {
                    rdDynoTQ.Checked = true;
                }
                if (TransientMode.TransientModeData.EM == EngineMode.Alpha_Mode)
                {
                    rdEngineALP.Checked = true;
                }
                else
                {
                    rdEngineTQ.Checked = true;
                }

                //TransientMode.TransientModeData.LoadCSVFile(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Properties.Settings.Default.TranModeDir + "\\" + Properties.Settings.Default.TranModeFileName);
                //dgStepMode.SuspendLayout();

                //for (int i = 0; i < DT.Rows.Count; i++)
                //{
                //    dgStepMode.Rows.Add();
                //    for (int j = 0; j < DT.Columns.Count; j++)
                //    {
                //        dgStepMode[j, i].Value = DT.Rows[i][j].ToString();
                //    }
                //}
                //dgStepMode.ResumeLayout();
            }
        }

        private void rdDynoSP_CheckedChanged(object sender, EventArgs e)
        {
            if (rdDynoSP.Checked == true)
            {
                TransientMode.TransientModeData.DM = DynoMode.Speed_Mode;
            } else
            {
                TransientMode.TransientModeData.DM = DynoMode.Torque_Mode;

            }
            chart1.ChartAreas[0].CursorX.Position = 0;
            chart1.ChartAreas[0].AxisX.ScaleView.ZoomReset();
            SetData(0);
        }

        private void rdEngineALP_CheckedChanged(object sender, EventArgs e)
        {
            if (rdEngineALP.Checked == true)
            {
                TransientMode.TransientModeData.EM = EngineMode.Alpha_Mode;
            }
            else
            {
                TransientMode.TransientModeData.EM = EngineMode.Torque_Mode;

            }
            chart1.ChartAreas[0].CursorX.Position = 0;
            chart1.ChartAreas[0].AxisX.ScaleView.ZoomReset();
            SetData(0);
        }

        private void lbFileList_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbFileName.Text = lbFileList.SelectedItem.ToString();
        }

        private void chart1_CursorPositionChanged(object sender, CursorEventArgs e)
        {
            SetData(e.NewPosition);
        }

        private void SetData(double Position)
        {
            int Time = (int)Position;

            for (int i = 0; i < dgStepMode.Rows.Count-1; i++)
            {
                if (Time == (int)dgStepMode[0, i].Value)
                {
                    lbTime.Text = Time.ToString();
                    lbDyno.Text = ((double)dgStepMode[1, i].Value).ToString();
                    lbEngine.Text = ((double)dgStepMode[2, i].Value).ToString();
                    return;
                }
            }
            lbTime.Text = "-";
            lbDyno.Text = "-";
            lbEngine.Text = "-";
        }

        private void btnStart_Click(object sender, EventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {

        }
    }
}
