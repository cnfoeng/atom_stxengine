﻿namespace AtomUI.ControlForm
{
    partial class TransientRun
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TransientRun));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnLoad = new System.Windows.Forms.Button();
            this.tbFileName = new System.Windows.Forms.TextBox();
            this.lbFileList = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rdEngineTQ = new System.Windows.Forms.RadioButton();
            this.rdEngineALP = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rdDynoTQ = new System.Windows.Forms.RadioButton();
            this.rdDynoSP = new System.Windows.Forms.RadioButton();
            this.lvInput = new System.Windows.Forms.ListView();
            this.Header1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.lbCurrentTime = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lbTotalTime = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.btnProfileWrite = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.lbTGETorque = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.lbTGEAlpha = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.lbTGDTorque = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.lbTGDSpeed = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.lbFBTorque = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.lbFBAlpha = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.lbFBSpeed = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lbEngineDuration = new System.Windows.Forms.Label();
            this.lbDynoDuration = new System.Windows.Forms.Label();
            this.tbEngineDuration = new System.Windows.Forms.TextBox();
            this.tbDynoDuration = new System.Windows.Forms.TextBox();
            this.lbTitleEngine = new System.Windows.Forms.Label();
            this.lbTitleDyno = new System.Windows.Forms.Label();
            this.chFeedBackEngine = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chFeedBackDyno = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnNT = new System.Windows.Forms.Button();
            this.btnTN = new System.Windows.Forms.Button();
            this.btnNA = new System.Windows.Forms.Button();
            this.btnTA = new System.Windows.Forms.Button();
            this.btnIdleControl = new System.Windows.Forms.Button();
            this.btnIDLE = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.btnData = new System.Windows.Forms.Button();
            this.btnAuto = new System.Windows.Forms.Button();
            this.btnRemote = new System.Windows.Forms.Button();
            this.btnManual = new System.Windows.Forms.Button();
            this.btnEngine = new System.Windows.Forms.Button();
            this.btnDyno = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lbEngine = new System.Windows.Forms.Label();
            this.lbDyno = new System.Windows.Forms.Label();
            this.lbTime = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chFeedBackEngine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chFeedBackDyno)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnLoad);
            this.groupBox1.Controls.Add(this.tbFileName);
            this.groupBox1.Controls.Add(this.lbFileList);
            this.groupBox1.Location = new System.Drawing.Point(10, 10);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(200, 286);
            this.groupBox1.TabIndex = 70;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Transient Profile Load";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 194);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 12);
            this.label3.TabIndex = 73;
            this.label3.Text = "File Name";
            // 
            // btnLoad
            // 
            this.btnLoad.BackgroundImage = global::AtomUI.Properties.Resources.TransientRun_loadfromfile;
            this.btnLoad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnLoad.FlatAppearance.BorderSize = 0;
            this.btnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoad.Location = new System.Drawing.Point(5, 234);
            this.btnLoad.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(174, 36);
            this.btnLoad.TabIndex = 72;
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // tbFileName
            // 
            this.tbFileName.Location = new System.Drawing.Point(5, 209);
            this.tbFileName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbFileName.Name = "tbFileName";
            this.tbFileName.Size = new System.Drawing.Size(174, 21);
            this.tbFileName.TabIndex = 71;
            // 
            // lbFileList
            // 
            this.lbFileList.FormattingEnabled = true;
            this.lbFileList.ItemHeight = 12;
            this.lbFileList.Location = new System.Drawing.Point(5, 19);
            this.lbFileList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lbFileList.Name = "lbFileList";
            this.lbFileList.Size = new System.Drawing.Size(174, 160);
            this.lbFileList.TabIndex = 70;
            this.lbFileList.SelectedIndexChanged += new System.EventHandler(this.lbFileList_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.lvInput);
            this.groupBox2.Location = new System.Drawing.Point(232, 10);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(380, 285);
            this.groupBox2.TabIndex = 71;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Loaded Transient Profile";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rdEngineTQ);
            this.groupBox4.Controls.Add(this.rdEngineALP);
            this.groupBox4.Location = new System.Drawing.Point(186, 24);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(188, 59);
            this.groupBox4.TabIndex = 79;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Engine Mode";
            // 
            // rdEngineTQ
            // 
            this.rdEngineTQ.AutoSize = true;
            this.rdEngineTQ.Location = new System.Drawing.Point(91, 27);
            this.rdEngineTQ.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rdEngineTQ.Name = "rdEngineTQ";
            this.rdEngineTQ.Size = new System.Drawing.Size(73, 16);
            this.rdEngineTQ.TabIndex = 3;
            this.rdEngineTQ.Text = "TORQUE";
            this.rdEngineTQ.UseVisualStyleBackColor = true;
            this.rdEngineTQ.CheckedChanged += new System.EventHandler(this.rdEngineTQ_CheckedChanged);
            // 
            // rdEngineALP
            // 
            this.rdEngineALP.AutoSize = true;
            this.rdEngineALP.Checked = true;
            this.rdEngineALP.Location = new System.Drawing.Point(21, 27);
            this.rdEngineALP.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rdEngineALP.Name = "rdEngineALP";
            this.rdEngineALP.Size = new System.Drawing.Size(62, 16);
            this.rdEngineALP.TabIndex = 2;
            this.rdEngineALP.TabStop = true;
            this.rdEngineALP.Text = "ALPHA";
            this.rdEngineALP.UseVisualStyleBackColor = true;
            this.rdEngineALP.CheckedChanged += new System.EventHandler(this.rdEngineALP_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rdDynoTQ);
            this.groupBox3.Controls.Add(this.rdDynoSP);
            this.groupBox3.Location = new System.Drawing.Point(5, 24);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(176, 59);
            this.groupBox3.TabIndex = 78;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dyno Mode";
            // 
            // rdDynoTQ
            // 
            this.rdDynoTQ.AutoSize = true;
            this.rdDynoTQ.Location = new System.Drawing.Point(84, 27);
            this.rdDynoTQ.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rdDynoTQ.Name = "rdDynoTQ";
            this.rdDynoTQ.Size = new System.Drawing.Size(73, 16);
            this.rdDynoTQ.TabIndex = 1;
            this.rdDynoTQ.Text = "TORQUE";
            this.rdDynoTQ.UseVisualStyleBackColor = true;
            this.rdDynoTQ.CheckedChanged += new System.EventHandler(this.rdDynoTQ_CheckedChanged);
            // 
            // rdDynoSP
            // 
            this.rdDynoSP.AutoSize = true;
            this.rdDynoSP.Checked = true;
            this.rdDynoSP.Location = new System.Drawing.Point(12, 27);
            this.rdDynoSP.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rdDynoSP.Name = "rdDynoSP";
            this.rdDynoSP.Size = new System.Drawing.Size(63, 16);
            this.rdDynoSP.TabIndex = 0;
            this.rdDynoSP.TabStop = true;
            this.rdDynoSP.Text = "SPEED";
            this.rdDynoSP.UseVisualStyleBackColor = true;
            this.rdDynoSP.CheckedChanged += new System.EventHandler(this.rdDynoSP_CheckedChanged);
            // 
            // lvInput
            // 
            this.lvInput.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Header1,
            this.Header2,
            this.Header3});
            this.lvInput.FullRowSelect = true;
            this.lvInput.GridLines = true;
            listViewGroup1.Header = "ListViewGroup";
            listViewGroup1.Name = "Global Variables";
            listViewGroup2.Header = "ListViewGroup";
            listViewGroup2.Name = "Local Variables";
            this.lvInput.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2});
            this.lvInput.HideSelection = false;
            this.lvInput.Location = new System.Drawing.Point(5, 88);
            this.lvInput.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lvInput.MultiSelect = false;
            this.lvInput.Name = "lvInput";
            this.lvInput.ShowGroups = false;
            this.lvInput.Size = new System.Drawing.Size(370, 185);
            this.lvInput.TabIndex = 77;
            this.lvInput.UseCompatibleStateImageBehavior = false;
            this.lvInput.View = System.Windows.Forms.View.Details;
            // 
            // Header1
            // 
            this.Header1.Text = "Time";
            this.Header1.Width = 106;
            // 
            // Header2
            // 
            this.Header2.Text = "Dyno";
            this.Header2.Width = 107;
            // 
            // Header3
            // 
            this.Header3.Text = "Engine";
            this.Header3.Width = 117;
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chart1.BorderlineColor = System.Drawing.Color.Black;
            this.chart1.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(617, 10);
            this.chart1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(872, 286);
            this.chart1.TabIndex = 72;
            this.chart1.Text = "chart1";
            this.chart1.CursorPositionChanged += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.CursorEventArgs>(this.chart1_CursorPositionChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.groupBox10);
            this.groupBox5.Controls.Add(this.groupBox9);
            this.groupBox5.Controls.Add(this.groupBox7);
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox5.Location = new System.Drawing.Point(10, 301);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Size = new System.Drawing.Size(1479, 417);
            this.groupBox5.TabIndex = 73;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Transient Mode Control Panel";
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this.lbCurrentTime);
            this.groupBox10.Controls.Add(this.label22);
            this.groupBox10.Controls.Add(this.lbTotalTime);
            this.groupBox10.Controls.Add(this.label20);
            this.groupBox10.Controls.Add(this.btnStop);
            this.groupBox10.Controls.Add(this.btnRun);
            this.groupBox10.Controls.Add(this.btnProfileWrite);
            this.groupBox10.Controls.Add(this.label17);
            this.groupBox10.Controls.Add(this.label16);
            this.groupBox10.Controls.Add(this.progressBar1);
            this.groupBox10.Location = new System.Drawing.Point(606, 270);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox10.Size = new System.Drawing.Size(867, 137);
            this.groupBox10.TabIndex = 35;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Command";
            // 
            // lbCurrentTime
            // 
            this.lbCurrentTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCurrentTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbCurrentTime.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbCurrentTime.ForeColor = System.Drawing.Color.White;
            this.lbCurrentTime.Location = new System.Drawing.Point(765, 42);
            this.lbCurrentTime.Name = "lbCurrentTime";
            this.lbCurrentTime.Size = new System.Drawing.Size(97, 21);
            this.lbCurrentTime.TabIndex = 42;
            this.lbCurrentTime.Text = "0 Sec";
            this.lbCurrentTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(765, 15);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(97, 26);
            this.label22.TabIndex = 41;
            this.label22.Tag = "F";
            this.label22.Text = "Current Time";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbTotalTime
            // 
            this.lbTotalTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTotalTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbTotalTime.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbTotalTime.ForeColor = System.Drawing.Color.White;
            this.lbTotalTime.Location = new System.Drawing.Point(662, 42);
            this.lbTotalTime.Name = "lbTotalTime";
            this.lbTotalTime.Size = new System.Drawing.Size(97, 21);
            this.lbTotalTime.TabIndex = 40;
            this.lbTotalTime.Text = "0 Sec";
            this.lbTotalTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(662, 15);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(97, 26);
            this.label20.TabIndex = 39;
            this.label20.Tag = "F";
            this.label20.Text = "Total Time";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.Color.Transparent;
            this.btnStop.BackgroundImage = global::AtomUI.Properties.Resources.stop버튼_회;
            this.btnStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnStop.FlatAppearance.BorderSize = 0;
            this.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStop.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnStop.ForeColor = System.Drawing.Color.Linen;
            this.btnStop.Location = new System.Drawing.Point(273, 15);
            this.btnStop.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(132, 47);
            this.btnStop.TabIndex = 38;
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnRun
            // 
            this.btnRun.BackColor = System.Drawing.Color.Transparent;
            this.btnRun.BackgroundImage = global::AtomUI.Properties.Resources.run버튼_회;
            this.btnRun.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRun.FlatAppearance.BorderSize = 0;
            this.btnRun.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRun.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnRun.ForeColor = System.Drawing.Color.White;
            this.btnRun.Location = new System.Drawing.Point(145, 15);
            this.btnRun.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(122, 47);
            this.btnRun.TabIndex = 37;
            this.btnRun.UseVisualStyleBackColor = false;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // btnProfileWrite
            // 
            this.btnProfileWrite.BackColor = System.Drawing.Color.Transparent;
            this.btnProfileWrite.BackgroundImage = global::AtomUI.Properties.Resources.profile_write버튼;
            this.btnProfileWrite.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnProfileWrite.FlatAppearance.BorderSize = 0;
            this.btnProfileWrite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProfileWrite.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnProfileWrite.ForeColor = System.Drawing.Color.White;
            this.btnProfileWrite.Location = new System.Drawing.Point(6, 15);
            this.btnProfileWrite.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnProfileWrite.Name = "btnProfileWrite";
            this.btnProfileWrite.Size = new System.Drawing.Size(134, 47);
            this.btnProfileWrite.TabIndex = 36;
            this.btnProfileWrite.UseVisualStyleBackColor = false;
            this.btnProfileWrite.Click += new System.EventHandler(this.btnProfileWrite_Click);
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.label17.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(718, 97);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(144, 30);
            this.label17.TabIndex = 35;
            this.label17.Text = "0 %";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label16.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(5, 97);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(216, 30);
            this.label16.TabIndex = 34;
            this.label16.Tag = "F";
            this.label16.Text = "Progress";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.progressBar1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(182)))), ((int)(((byte)(81)))));
            this.progressBar1.Location = new System.Drawing.Point(220, 97);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(500, 30);
            this.progressBar1.TabIndex = 33;
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox9.Controls.Add(this.lbTGETorque);
            this.groupBox9.Controls.Add(this.label34);
            this.groupBox9.Controls.Add(this.lbTGEAlpha);
            this.groupBox9.Controls.Add(this.label36);
            this.groupBox9.Controls.Add(this.lbTGDTorque);
            this.groupBox9.Controls.Add(this.label38);
            this.groupBox9.Controls.Add(this.lbTGDSpeed);
            this.groupBox9.Controls.Add(this.label40);
            this.groupBox9.Controls.Add(this.lbFBTorque);
            this.groupBox9.Controls.Add(this.label30);
            this.groupBox9.Controls.Add(this.lbFBAlpha);
            this.groupBox9.Controls.Add(this.label28);
            this.groupBox9.Controls.Add(this.lbFBSpeed);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Location = new System.Drawing.Point(8, 270);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox9.Size = new System.Drawing.Size(593, 137);
            this.groupBox9.TabIndex = 34;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Control IO Status";
            // 
            // lbTGETorque
            // 
            this.lbTGETorque.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTGETorque.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbTGETorque.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbTGETorque.ForeColor = System.Drawing.Color.White;
            this.lbTGETorque.Location = new System.Drawing.Point(443, 106);
            this.lbTGETorque.Name = "lbTGETorque";
            this.lbTGETorque.Size = new System.Drawing.Size(131, 21);
            this.lbTGETorque.TabIndex = 50;
            this.lbTGETorque.Text = "0 RPM";
            this.lbTGETorque.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label34.ForeColor = System.Drawing.Color.White;
            this.label34.Location = new System.Drawing.Point(443, 86);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(131, 21);
            this.label34.TabIndex = 49;
            this.label34.Tag = "F";
            this.label34.Text = "Target ENG Torque";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbTGEAlpha
            // 
            this.lbTGEAlpha.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTGEAlpha.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbTGEAlpha.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbTGEAlpha.ForeColor = System.Drawing.Color.White;
            this.lbTGEAlpha.Location = new System.Drawing.Point(303, 106);
            this.lbTGEAlpha.Name = "lbTGEAlpha";
            this.lbTGEAlpha.Size = new System.Drawing.Size(131, 21);
            this.lbTGEAlpha.TabIndex = 48;
            this.lbTGEAlpha.Text = "0 RPM";
            this.lbTGEAlpha.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label36
            // 
            this.label36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label36.ForeColor = System.Drawing.Color.White;
            this.label36.Location = new System.Drawing.Point(303, 86);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(131, 21);
            this.label36.TabIndex = 47;
            this.label36.Tag = "F";
            this.label36.Text = "Target ENG Alpha";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbTGDTorque
            // 
            this.lbTGDTorque.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTGDTorque.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbTGDTorque.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbTGDTorque.ForeColor = System.Drawing.Color.White;
            this.lbTGDTorque.Location = new System.Drawing.Point(163, 106);
            this.lbTGDTorque.Name = "lbTGDTorque";
            this.lbTGDTorque.Size = new System.Drawing.Size(131, 21);
            this.lbTGDTorque.TabIndex = 46;
            this.lbTGDTorque.Text = "0 RPM";
            this.lbTGDTorque.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label38
            // 
            this.label38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label38.ForeColor = System.Drawing.Color.White;
            this.label38.Location = new System.Drawing.Point(163, 86);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(131, 21);
            this.label38.TabIndex = 45;
            this.label38.Tag = "F";
            this.label38.Text = "Target Dyno Torque";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbTGDSpeed
            // 
            this.lbTGDSpeed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTGDSpeed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbTGDSpeed.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbTGDSpeed.ForeColor = System.Drawing.Color.White;
            this.lbTGDSpeed.Location = new System.Drawing.Point(23, 106);
            this.lbTGDSpeed.Name = "lbTGDSpeed";
            this.lbTGDSpeed.Size = new System.Drawing.Size(131, 21);
            this.lbTGDSpeed.TabIndex = 44;
            this.lbTGDSpeed.Text = "0 RPM";
            this.lbTGDSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label40
            // 
            this.label40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label40.ForeColor = System.Drawing.Color.White;
            this.label40.Location = new System.Drawing.Point(23, 86);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(131, 21);
            this.label40.TabIndex = 43;
            this.label40.Tag = "F";
            this.label40.Text = "Target Dyno Speed";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbFBTorque
            // 
            this.lbFBTorque.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbFBTorque.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbFBTorque.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbFBTorque.ForeColor = System.Drawing.Color.White;
            this.lbFBTorque.Location = new System.Drawing.Point(163, 47);
            this.lbFBTorque.Name = "lbFBTorque";
            this.lbFBTorque.Size = new System.Drawing.Size(131, 21);
            this.lbFBTorque.TabIndex = 40;
            this.lbFBTorque.Text = "0 RPM";
            this.lbFBTorque.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(163, 26);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(131, 21);
            this.label30.TabIndex = 39;
            this.label30.Tag = "F";
            this.label30.Text = "Feedback Torque";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbFBAlpha
            // 
            this.lbFBAlpha.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbFBAlpha.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbFBAlpha.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbFBAlpha.ForeColor = System.Drawing.Color.White;
            this.lbFBAlpha.Location = new System.Drawing.Point(303, 47);
            this.lbFBAlpha.Name = "lbFBAlpha";
            this.lbFBAlpha.Size = new System.Drawing.Size(131, 21);
            this.lbFBAlpha.TabIndex = 38;
            this.lbFBAlpha.Text = "0 RPM";
            this.lbFBAlpha.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(303, 26);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(131, 21);
            this.label28.TabIndex = 37;
            this.label28.Tag = "F";
            this.label28.Text = "Feedback Alpha";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbFBSpeed
            // 
            this.lbFBSpeed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbFBSpeed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbFBSpeed.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbFBSpeed.ForeColor = System.Drawing.Color.White;
            this.lbFBSpeed.Location = new System.Drawing.Point(23, 47);
            this.lbFBSpeed.Name = "lbFBSpeed";
            this.lbFBSpeed.Size = new System.Drawing.Size(131, 21);
            this.lbFBSpeed.TabIndex = 36;
            this.lbFBSpeed.Text = "0 RPM";
            this.lbFBSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(23, 26);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(131, 21);
            this.label18.TabIndex = 17;
            this.label18.Tag = "F";
            this.label18.Text = "Feedback Speed";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.lbEngineDuration);
            this.groupBox7.Controls.Add(this.lbDynoDuration);
            this.groupBox7.Controls.Add(this.tbEngineDuration);
            this.groupBox7.Controls.Add(this.tbDynoDuration);
            this.groupBox7.Controls.Add(this.lbTitleEngine);
            this.groupBox7.Controls.Add(this.lbTitleDyno);
            this.groupBox7.Controls.Add(this.chFeedBackEngine);
            this.groupBox7.Controls.Add(this.chFeedBackDyno);
            this.groupBox7.Location = new System.Drawing.Point(606, 34);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Size = new System.Drawing.Size(866, 224);
            this.groupBox7.TabIndex = 33;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "FeedBack Value";
            this.groupBox7.SizeChanged += new System.EventHandler(this.groupBox7_SizeChanged);
            // 
            // lbEngineDuration
            // 
            this.lbEngineDuration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbEngineDuration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.lbEngineDuration.ForeColor = System.Drawing.Color.White;
            this.lbEngineDuration.Location = new System.Drawing.Point(754, 148);
            this.lbEngineDuration.Name = "lbEngineDuration";
            this.lbEngineDuration.Size = new System.Drawing.Size(90, 26);
            this.lbEngineDuration.TabIndex = 87;
            this.lbEngineDuration.Tag = "F";
            this.lbEngineDuration.Text = "X Axis Duration";
            this.lbEngineDuration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbDynoDuration
            // 
            this.lbDynoDuration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbDynoDuration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.lbDynoDuration.ForeColor = System.Drawing.Color.White;
            this.lbDynoDuration.Location = new System.Drawing.Point(323, 148);
            this.lbDynoDuration.Name = "lbDynoDuration";
            this.lbDynoDuration.Size = new System.Drawing.Size(90, 26);
            this.lbDynoDuration.TabIndex = 86;
            this.lbDynoDuration.Tag = "F";
            this.lbDynoDuration.Text = "X Axis Duration";
            this.lbDynoDuration.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbEngineDuration
            // 
            this.tbEngineDuration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.tbEngineDuration.Location = new System.Drawing.Point(754, 174);
            this.tbEngineDuration.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbEngineDuration.Name = "tbEngineDuration";
            this.tbEngineDuration.Size = new System.Drawing.Size(91, 21);
            this.tbEngineDuration.TabIndex = 85;
            this.tbEngineDuration.Text = "10";
            this.tbEngineDuration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbEngineDuration.TextChanged += new System.EventHandler(this.tbEngineDuration_TextChanged);
            // 
            // tbDynoDuration
            // 
            this.tbDynoDuration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.tbDynoDuration.Location = new System.Drawing.Point(323, 174);
            this.tbDynoDuration.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbDynoDuration.Name = "tbDynoDuration";
            this.tbDynoDuration.Size = new System.Drawing.Size(91, 21);
            this.tbDynoDuration.TabIndex = 84;
            this.tbDynoDuration.Text = "10";
            this.tbDynoDuration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbDynoDuration.TextChanged += new System.EventHandler(this.tbDynoDuration_TextChanged);
            // 
            // lbTitleEngine
            // 
            this.lbTitleEngine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTitleEngine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.lbTitleEngine.ForeColor = System.Drawing.Color.White;
            this.lbTitleEngine.Location = new System.Drawing.Point(756, 102);
            this.lbTitleEngine.Name = "lbTitleEngine";
            this.lbTitleEngine.Size = new System.Drawing.Size(88, 26);
            this.lbTitleEngine.TabIndex = 78;
            this.lbTitleEngine.Tag = "F";
            this.lbTitleEngine.Text = "Engine";
            this.lbTitleEngine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbTitleDyno
            // 
            this.lbTitleDyno.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTitleDyno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.lbTitleDyno.ForeColor = System.Drawing.Color.White;
            this.lbTitleDyno.Location = new System.Drawing.Point(323, 102);
            this.lbTitleDyno.Name = "lbTitleDyno";
            this.lbTitleDyno.Size = new System.Drawing.Size(90, 26);
            this.lbTitleDyno.TabIndex = 77;
            this.lbTitleDyno.Tag = "F";
            this.lbTitleDyno.Text = "Dyno";
            this.lbTitleDyno.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chFeedBackEngine
            // 
            this.chFeedBackEngine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chFeedBackEngine.BorderlineColor = System.Drawing.Color.Black;
            this.chFeedBackEngine.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea2.Name = "ChartArea1";
            this.chFeedBackEngine.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chFeedBackEngine.Legends.Add(legend2);
            this.chFeedBackEngine.Location = new System.Drawing.Point(442, 25);
            this.chFeedBackEngine.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chFeedBackEngine.Name = "chFeedBackEngine";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chFeedBackEngine.Series.Add(series2);
            this.chFeedBackEngine.Size = new System.Drawing.Size(410, 182);
            this.chFeedBackEngine.TabIndex = 74;
            this.chFeedBackEngine.Text = "chart3";
            // 
            // chFeedBackDyno
            // 
            this.chFeedBackDyno.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chFeedBackDyno.BorderlineColor = System.Drawing.Color.Black;
            this.chFeedBackDyno.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea3.Name = "ChartArea1";
            this.chFeedBackDyno.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chFeedBackDyno.Legends.Add(legend3);
            this.chFeedBackDyno.Location = new System.Drawing.Point(13, 25);
            this.chFeedBackDyno.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chFeedBackDyno.Name = "chFeedBackDyno";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.chFeedBackDyno.Series.Add(series3);
            this.chFeedBackDyno.Size = new System.Drawing.Size(410, 182);
            this.chFeedBackDyno.TabIndex = 73;
            this.chFeedBackDyno.Text = "chart2";
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox6.Controls.Add(this.btnNT);
            this.groupBox6.Controls.Add(this.btnTN);
            this.groupBox6.Controls.Add(this.btnNA);
            this.groupBox6.Controls.Add(this.btnTA);
            this.groupBox6.Controls.Add(this.btnIdleControl);
            this.groupBox6.Controls.Add(this.btnIDLE);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.btnData);
            this.groupBox6.Controls.Add(this.btnAuto);
            this.groupBox6.Controls.Add(this.btnRemote);
            this.groupBox6.Controls.Add(this.btnManual);
            this.groupBox6.Controls.Add(this.btnEngine);
            this.groupBox6.Controls.Add(this.btnDyno);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Location = new System.Drawing.Point(8, 34);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Size = new System.Drawing.Size(593, 224);
            this.groupBox6.TabIndex = 31;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "SYSTEM STATUS";
            // 
            // btnNT
            // 
            this.btnNT.BackColor = System.Drawing.Color.Transparent;
            this.btnNT.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNT.BackgroundImage")));
            this.btnNT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnNT.Enabled = false;
            this.btnNT.FlatAppearance.BorderSize = 0;
            this.btnNT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNT.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnNT.Location = new System.Drawing.Point(496, 148);
            this.btnNT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnNT.Name = "btnNT";
            this.btnNT.Size = new System.Drawing.Size(66, 47);
            this.btnNT.TabIndex = 43;
            this.btnNT.UseVisualStyleBackColor = false;
            // 
            // btnTN
            // 
            this.btnTN.BackColor = System.Drawing.Color.Transparent;
            this.btnTN.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTN.BackgroundImage")));
            this.btnTN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTN.Enabled = false;
            this.btnTN.FlatAppearance.BorderSize = 0;
            this.btnTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTN.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnTN.Location = new System.Drawing.Point(402, 148);
            this.btnTN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnTN.Name = "btnTN";
            this.btnTN.Size = new System.Drawing.Size(66, 47);
            this.btnTN.TabIndex = 42;
            this.btnTN.UseVisualStyleBackColor = false;
            // 
            // btnNA
            // 
            this.btnNA.BackColor = System.Drawing.Color.Transparent;
            this.btnNA.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNA.BackgroundImage")));
            this.btnNA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnNA.Enabled = false;
            this.btnNA.FlatAppearance.BorderSize = 0;
            this.btnNA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNA.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnNA.Location = new System.Drawing.Point(307, 148);
            this.btnNA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnNA.Name = "btnNA";
            this.btnNA.Size = new System.Drawing.Size(66, 47);
            this.btnNA.TabIndex = 41;
            this.btnNA.UseVisualStyleBackColor = false;
            // 
            // btnTA
            // 
            this.btnTA.BackColor = System.Drawing.Color.Transparent;
            this.btnTA.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTA.BackgroundImage")));
            this.btnTA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTA.Enabled = false;
            this.btnTA.FlatAppearance.BorderSize = 0;
            this.btnTA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTA.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnTA.Location = new System.Drawing.Point(213, 148);
            this.btnTA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnTA.Name = "btnTA";
            this.btnTA.Size = new System.Drawing.Size(66, 47);
            this.btnTA.TabIndex = 40;
            this.btnTA.UseVisualStyleBackColor = false;
            // 
            // btnIdleControl
            // 
            this.btnIdleControl.BackColor = System.Drawing.Color.Transparent;
            this.btnIdleControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnIdleControl.BackgroundImage")));
            this.btnIdleControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnIdleControl.Enabled = false;
            this.btnIdleControl.FlatAppearance.BorderSize = 0;
            this.btnIdleControl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIdleControl.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIdleControl.Location = new System.Drawing.Point(118, 148);
            this.btnIdleControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnIdleControl.Name = "btnIdleControl";
            this.btnIdleControl.Size = new System.Drawing.Size(66, 47);
            this.btnIdleControl.TabIndex = 39;
            this.btnIdleControl.UseVisualStyleBackColor = false;
            // 
            // btnIDLE
            // 
            this.btnIDLE.BackColor = System.Drawing.Color.Transparent;
            this.btnIDLE.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnIDLE.BackgroundImage")));
            this.btnIDLE.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnIDLE.Enabled = false;
            this.btnIDLE.FlatAppearance.BorderSize = 0;
            this.btnIDLE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIDLE.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnIDLE.Location = new System.Drawing.Point(24, 148);
            this.btnIDLE.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnIDLE.Name = "btnIDLE";
            this.btnIDLE.Size = new System.Drawing.Size(66, 47);
            this.btnIDLE.TabIndex = 38;
            this.btnIDLE.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(482, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 21);
            this.label2.TabIndex = 37;
            this.label2.Text = "N/T";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(199, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 21);
            this.label4.TabIndex = 36;
            this.label4.Text = "T/α";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(388, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 21);
            this.label5.TabIndex = 35;
            this.label5.Text = "T/N";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(104, 118);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 21);
            this.label12.TabIndex = 34;
            this.label12.Text = "IDLE Control";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(10, 118);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 21);
            this.label13.TabIndex = 33;
            this.label13.Text = "IDLE";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(293, 118);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(94, 21);
            this.label14.TabIndex = 32;
            this.label14.Tag = "";
            this.label14.Text = "N/α";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnData
            // 
            this.btnData.BackColor = System.Drawing.Color.Transparent;
            this.btnData.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnData.BackgroundImage")));
            this.btnData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnData.Enabled = false;
            this.btnData.FlatAppearance.BorderSize = 0;
            this.btnData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnData.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnData.Location = new System.Drawing.Point(496, 55);
            this.btnData.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnData.Name = "btnData";
            this.btnData.Size = new System.Drawing.Size(66, 47);
            this.btnData.TabIndex = 31;
            this.btnData.UseVisualStyleBackColor = false;
            // 
            // btnAuto
            // 
            this.btnAuto.BackColor = System.Drawing.Color.Transparent;
            this.btnAuto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAuto.BackgroundImage")));
            this.btnAuto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnAuto.Enabled = false;
            this.btnAuto.FlatAppearance.BorderSize = 0;
            this.btnAuto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAuto.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAuto.Location = new System.Drawing.Point(402, 55);
            this.btnAuto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAuto.Name = "btnAuto";
            this.btnAuto.Size = new System.Drawing.Size(66, 47);
            this.btnAuto.TabIndex = 30;
            this.btnAuto.UseVisualStyleBackColor = false;
            // 
            // btnRemote
            // 
            this.btnRemote.BackColor = System.Drawing.Color.Transparent;
            this.btnRemote.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRemote.BackgroundImage")));
            this.btnRemote.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRemote.Enabled = false;
            this.btnRemote.FlatAppearance.BorderSize = 0;
            this.btnRemote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemote.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnRemote.Location = new System.Drawing.Point(307, 55);
            this.btnRemote.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRemote.Name = "btnRemote";
            this.btnRemote.Size = new System.Drawing.Size(66, 47);
            this.btnRemote.TabIndex = 29;
            this.btnRemote.UseVisualStyleBackColor = false;
            // 
            // btnManual
            // 
            this.btnManual.BackColor = System.Drawing.Color.Transparent;
            this.btnManual.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnManual.BackgroundImage")));
            this.btnManual.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnManual.Enabled = false;
            this.btnManual.FlatAppearance.BorderSize = 0;
            this.btnManual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManual.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnManual.Location = new System.Drawing.Point(213, 55);
            this.btnManual.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnManual.Name = "btnManual";
            this.btnManual.Size = new System.Drawing.Size(66, 47);
            this.btnManual.TabIndex = 28;
            this.btnManual.UseVisualStyleBackColor = false;
            // 
            // btnEngine
            // 
            this.btnEngine.BackColor = System.Drawing.Color.Transparent;
            this.btnEngine.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEngine.BackgroundImage")));
            this.btnEngine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnEngine.Enabled = false;
            this.btnEngine.FlatAppearance.BorderSize = 0;
            this.btnEngine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEngine.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnEngine.Location = new System.Drawing.Point(118, 55);
            this.btnEngine.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnEngine.Name = "btnEngine";
            this.btnEngine.Size = new System.Drawing.Size(66, 47);
            this.btnEngine.TabIndex = 27;
            this.btnEngine.UseVisualStyleBackColor = false;
            // 
            // btnDyno
            // 
            this.btnDyno.BackColor = System.Drawing.Color.Transparent;
            this.btnDyno.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDyno.BackgroundImage")));
            this.btnDyno.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnDyno.Enabled = false;
            this.btnDyno.FlatAppearance.BorderSize = 0;
            this.btnDyno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDyno.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnDyno.Location = new System.Drawing.Point(24, 55);
            this.btnDyno.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnDyno.Name = "btnDyno";
            this.btnDyno.Size = new System.Drawing.Size(66, 47);
            this.btnDyno.TabIndex = 26;
            this.btnDyno.UseVisualStyleBackColor = false;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(482, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 21);
            this.label11.TabIndex = 24;
            this.label11.Text = "DATA SAVE";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(293, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 21);
            this.label10.TabIndex = 23;
            this.label10.Text = "REMOTE";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(388, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 21);
            this.label9.TabIndex = 22;
            this.label9.Text = "AUTO";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(199, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 21);
            this.label8.TabIndex = 20;
            this.label8.Text = "MANUAL";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(104, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 21);
            this.label7.TabIndex = 17;
            this.label7.Text = "ENGINE ON";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(10, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 21);
            this.label6.TabIndex = 15;
            this.label6.Tag = "F";
            this.label6.Text = "DYNO ON";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lbEngine
            // 
            this.lbEngine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbEngine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbEngine.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbEngine.ForeColor = System.Drawing.Color.White;
            this.lbEngine.Location = new System.Drawing.Point(1367, 235);
            this.lbEngine.Name = "lbEngine";
            this.lbEngine.Size = new System.Drawing.Size(93, 21);
            this.lbEngine.TabIndex = 77;
            this.lbEngine.Text = "0 Sec";
            this.lbEngine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbDyno
            // 
            this.lbDyno.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbDyno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbDyno.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbDyno.ForeColor = System.Drawing.Color.White;
            this.lbDyno.Location = new System.Drawing.Point(1367, 166);
            this.lbDyno.Name = "lbDyno";
            this.lbDyno.Size = new System.Drawing.Size(93, 21);
            this.lbDyno.TabIndex = 78;
            this.lbDyno.Text = "0 Sec";
            this.lbDyno.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbTime
            // 
            this.lbTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(190)))), ((int)(((byte)(214)))));
            this.lbTime.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbTime.ForeColor = System.Drawing.Color.White;
            this.lbTime.Location = new System.Drawing.Point(1367, 93);
            this.lbTime.Name = "lbTime";
            this.lbTime.Size = new System.Drawing.Size(93, 21);
            this.lbTime.TabIndex = 79;
            this.lbTime.Text = "0 Sec";
            this.lbTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(1367, 209);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(93, 26);
            this.label15.TabIndex = 74;
            this.label15.Tag = "F";
            this.label15.Text = "Engine";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(1367, 139);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(93, 26);
            this.label19.TabIndex = 75;
            this.label19.Tag = "F";
            this.label19.Text = "Dyno";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(89)))), ((int)(((byte)(152)))));
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(1367, 66);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(93, 26);
            this.label23.TabIndex = 76;
            this.label23.Tag = "F";
            this.label23.Text = "Time";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TransientRun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(246)))), ((int)(((byte)(249)))));
            this.ClientSize = new System.Drawing.Size(1500, 727);
            this.Controls.Add(this.lbEngine);
            this.Controls.Add(this.lbDyno);
            this.Controls.Add(this.lbTime);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "TransientRun";
            this.Text = "TransientRun";
            this.Load += new System.EventHandler(this.TransientRun_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chFeedBackEngine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chFeedBackDyno)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.TextBox tbFileName;
        private System.Windows.Forms.ListBox lbFileList;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.ListView lvInput;
        private System.Windows.Forms.ColumnHeader Header1;
        private System.Windows.Forms.ColumnHeader Header2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rdEngineTQ;
        private System.Windows.Forms.RadioButton rdEngineALP;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rdDynoTQ;
        private System.Windows.Forms.RadioButton rdDynoSP;
        private System.Windows.Forms.ColumnHeader Header3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnData;
        private System.Windows.Forms.Button btnAuto;
        private System.Windows.Forms.Button btnRemote;
        private System.Windows.Forms.Button btnManual;
        private System.Windows.Forms.Button btnEngine;
        private System.Windows.Forms.Button btnDyno;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button btnNT;
        private System.Windows.Forms.Button btnTN;
        private System.Windows.Forms.Button btnNA;
        private System.Windows.Forms.Button btnTA;
        private System.Windows.Forms.Button btnIdleControl;
        private System.Windows.Forms.Button btnIDLE;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label lbCurrentTime;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lbTotalTime;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Button btnProfileWrite;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lbFBSpeed;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DataVisualization.Charting.Chart chFeedBackDyno;
        private System.Windows.Forms.Label lbTGETorque;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label lbTGEAlpha;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label lbTGDTorque;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label lbTGDSpeed;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label lbFBTorque;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label lbFBAlpha;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.DataVisualization.Charting.Chart chFeedBackEngine;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lbEngine;
        private System.Windows.Forms.Label lbDyno;
        private System.Windows.Forms.Label lbTime;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lbTitleEngine;
        private System.Windows.Forms.Label lbTitleDyno;
        private System.Windows.Forms.Label lbEngineDuration;
        private System.Windows.Forms.Label lbDynoDuration;
        private System.Windows.Forms.TextBox tbEngineDuration;
        private System.Windows.Forms.TextBox tbDynoDuration;
    }
}