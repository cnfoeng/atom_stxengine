﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using AtomUI.Class;
using System.Windows.Forms.DataVisualization.Charting;
using ACS.SPiiPlusNET;

namespace AtomUI.ControlForm
{
    public partial class TransientRun : Form
    {
        public TransientRun()
        {
            InitializeComponent();
        }

        private void TransientRun_Load(object sender, EventArgs e)
        {
            DicInit();

            ChartInit();
            ChartInit(chFeedBackDyno);
            ChartInit(chFeedBackEngine);
            SeriesInit();
            SeriesInit(chFeedBackDyno);
            SeriesInit(chFeedBackEngine);

            GraphResize();

            FileBoxRefresh();
            tbFileName.Text = Ethercat.sysInfo.sysSettings.TranModeFileName.Substring(0, Ethercat.sysInfo.sysSettings.TranModeFileName.LastIndexOf('.'));
            DataLoad();

            Redraw();
        }

        private void ChartInit()
        {
            //X축 데이터 타입
            chart1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(223)))), ((int)(((byte)(193)))));
            chart1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(64)))), ((int)(((byte)(1)))));
            this.chart1.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.chart1.BorderlineWidth = 2;

            chart1.ChartAreas[0].BackColor = System.Drawing.Color.OldLace;
            chart1.ChartAreas[0].BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.ChartAreas[0].BackSecondaryColor = System.Drawing.Color.White;
            chart1.ChartAreas[0].BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chart1.ChartAreas[0].AxisY2.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.DashDotDot;
            chart1.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].AxisY2.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));

            chart1.ChartAreas[0].CursorX.LineColor = Color.Gray;
            chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
        }

        private void ChartInit(Chart chart1)
        {
            //X축 데이터 타입
            chart1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(223)))), ((int)(((byte)(193)))));
            chart1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(64)))), ((int)(((byte)(1)))));
            this.chart1.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.chart1.BorderlineWidth = 2;

            chart1.ChartAreas[0].BackColor = System.Drawing.Color.OldLace;
            chart1.ChartAreas[0].BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.ChartAreas[0].BackSecondaryColor = System.Drawing.Color.White;
            chart1.ChartAreas[0].BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;

            //chart1.ChartAreas[0].CursorX.LineColor = Color.Gray;
            //chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            //chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;

            chart1.ChartAreas[0].AxisX.LabelStyle.Format = "#0.0";
            chart1.ChartAreas[0].AxisX.IsLabelAutoFit = false;
            chart1.ChartAreas[0].AxisX.LabelStyle.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            chart1.ChartAreas[0].AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].AxisX.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].AxisY.IsLabelAutoFit = false;
            chart1.ChartAreas[0].AxisY.LabelStyle.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Bold);
            chart1.ChartAreas[0].AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            //chart1.ChartAreas[0].AxisY.Maximum = 5000;
            //chart1.ChartAreas[0].AxisY.Minimum = 1000;

        }

        private void SeriesInit()
        {
            chart1.Series.Clear();
            chart1.Series.Add("Dyno");
            chart1.Series.Add("Engine");

            chart1.Series[0].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            //Y축 데이터 타입
            chart1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;  //분산형 그래프
            chart1.Series[1].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;  //분산형 그래프
            chart1.Series[0].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;

            //차트 타입
            chart1.Series[0].IsXValueIndexed = false;
            chart1.Series[1].IsXValueIndexed = false;
            chart1.Series[0].Color = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(65)))), ((int)(((byte)(140)))), ((int)(((byte)(240)))));
            chart1.Series[1].Color = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(224)))), ((int)(((byte)(64)))), ((int)(((byte)(10)))));

            //chart1.ChartAreas[0].CursorX.Position = 50;
            //chart1.ChartAreas[0].CursorX.LineColor = Color.Gray;
            //chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            //chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
        }

        private void SeriesInit(Chart chart1)
        {
            chart1.Series.Clear();
            chart1.Series.Add("Target");
            chart1.Series.Add("Feedback");

            chart1.Series[0].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            //Y축 데이터 타입
            chart1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;  //분산형 그래프
            chart1.Series[1].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;  //분산형 그래프
            chart1.Series[0].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            chart1.Series[1].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;

            //차트 타입
            chart1.Series[0].IsXValueIndexed = false;
            chart1.Series[1].IsXValueIndexed = false;
            chart1.Series[0].Color = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(65)))), ((int)(((byte)(140)))), ((int)(((byte)(240)))));
            chart1.Series[1].Color = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(224)))), ((int)(((byte)(64)))), ((int)(((byte)(10)))));

            //chart1.ChartAreas[0].CursorX.Position = 50;
            //chart1.ChartAreas[0].CursorX.LineColor = Color.Gray;
            //chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            //chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
        }

        private void FileBoxRefresh()
        {
            lbFileList.Items.Clear();
            string[] Files = Directory.GetFiles(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.TranModeDir);
            foreach (string file in Files)
            {
                string filename = file.Substring(file.LastIndexOf('\\') + 1, file.Length - file.LastIndexOf('\\') - 1);
                string filenameonly = filename.Substring(0, filename.LastIndexOf('.'));
                lbFileList.Items.Add(filenameonly);
                if (Ethercat.sysInfo.sysSettings.TranModeFileName == file)
                {
                    lbFileList.SelectedValue = filename;
                }
            }
        }

        private void DataLoad()
        {
            if (File.Exists(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.TranModeDir + "\\" + Ethercat.sysInfo.sysSettings.TranModeFileName))
            {
                lvInput.Items.Clear();
                TransientMode.Load(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.TranModeDir + "\\" + Ethercat.sysInfo.sysSettings.TranModeFileName);
                DataRowCollection DT = TransientMode.TransientModeData.Data.Tables["TransientMode"].Rows;

                if (DT.Count > 1)
                {
                    try
                    {
                        for (int i = 0; i < DT.Count; i++)
                        {
                            int Time = (int)DT[i][0];
                            double Dyno = (double)DT[i][1];
                            double Engine = (double)DT[i][2];

                            ListViewItem itm = lvInput.Items.Add(Time.ToString());
                            itm.SubItems.Add(Dyno.ToString());
                            itm.SubItems.Add(Engine.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n connect Error!!");
                    }
                }

                if (TransientMode.TransientModeData.DM == DynoMode.Speed_Mode)
                {
                    rdDynoSP.Checked = true;
                }
                else
                {
                    rdDynoTQ.Checked = true;
                }
                if (TransientMode.TransientModeData.EM == EngineMode.Alpha_Mode)
                {
                    rdEngineALP.Checked = true;
                }
                else
                {
                    rdEngineTQ.Checked = true;
                }
            }
        }

        private void Redraw()
        {
            //X축 데이터 타입
            chart1.Series[0].Points.Clear();
            chart1.Series[1].Points.Clear();

            DataRowCollection DT = TransientMode.TransientModeData.Data.Tables["TransientMode"].Rows;

            if (DT.Count > 1)
            {
                try
                {
                    DataPoint dataPoint1 = new DataPoint(0, 0);
                    DataPoint dataPoint2 = new DataPoint(0, 0);
                    chart1.Series[0].Points.Add(dataPoint1);
                    chart1.Series[1].Points.Add(dataPoint2);

                    for (int i = 0; i < DT.Count; i++)
                    {
                        int Time = (int)DT[i][0];
                        double Dyno = (double)DT[i][1];
                        double Engine = (double)DT[i][2];

                        dataPoint1 = new DataPoint(Time, Dyno);
                        dataPoint2 = new DataPoint(Time, Engine);
                        chart1.Series[0].Points.Add(dataPoint1);
                        chart1.Series[1].Points.Add(dataPoint2);
                    }
                }
                catch (Exception ex)
                {
                    CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n connect Error!!");
                }
            }

        }

        private bool run = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            StatusUpdate();

            if (run)
            {
                SetGraph();
            }
        }

        private void StatusUpdate()
        {
            SetFrontPanel();
            SetPIDModePanel();
            SetValues();
            ButtonControl();
            SetTime();
        }
        private void SetTime()
        {
            TotalTime = TransientMode.TransientModeData.Data.Tables["TransientMode"].Rows.Count - 1;
            lbTotalTime.Text = TotalTime.ToString("#0.0") + " Sec";
            lbCurrentTime.Text = CurrentTime.ToString("#0.0") + " Sec";
            progressBar1.Maximum = (int)TotalTime;
            progressBar1.Value = (int)CurrentTime;
            label17.Text = ((CurrentTime/TotalTime * 100)).ToString("#0.0") + " %";
            chart1.ChartAreas[0].CursorX.SetCursorPosition(CurrentTime);
            lvInput.Items[(int)CurrentTime].Selected = true;
            lvInput.EnsureVisible((int)CurrentTime);
        }
        private void ButtonControl()
        {
            int AutoModeOnFromHMI = Ethercat.g_PC_Read_Data[0x33];
            if (Data_Profile_Writing & Data_Profile_Name.Length > 0 & run == false & AutoModeOnFromHMI==1)
            {
                btnRun.Enabled = true;
                btnRun.BackgroundImage = global::AtomUI.Properties.Resources.run버튼;
            }
            else
            {
                btnRun.Enabled = false;
                btnRun.BackgroundImage = global::AtomUI.Properties.Resources.run버튼_회;
            }
            if (run)
            {
                btnStop.Enabled = true;
                btnStop.BackgroundImage = global::AtomUI.Properties.Resources.stop버튼;
            }
            else
            {
                btnStop.Enabled = false;
                btnStop.BackgroundImage = global::AtomUI.Properties.Resources.stop버튼_회;
            }
        }

        double TotalTime;
        double CurrentTime;
        private void SetGraph()
        {
            CurrentTime = CurrentTime + 0.1;
            DataRowCollection DT = TransientMode.TransientModeData.Data.Tables["TransientMode"].Rows;

            double Target = 0.0;
            double FeedBack = 0.0;

            if (TransientMode.TransientModeData.DM == DynoMode.Speed_Mode)
            {
                Target = DaqData.SystemStatus.GetValue("TargetDynoSpeed");
                FeedBack = DaqData.SystemStatus.GetValue("FeedbackSpeed");
            }
            else
            {
                Target = DaqData.SystemStatus.GetValue("TargetDynoTorque");
                FeedBack = DaqData.SystemStatus.GetValue("FeedbackTorque");
            }
            if (chFeedBackDyno.Series[0].Points.Count > DynoDuration * 10 + 1)
            {
                chFeedBackDyno.Series[0].Points.RemoveAt(0);
                chFeedBackDyno.Series[1].Points.RemoveAt(0);
            }

            chFeedBackDyno.Series[0].Points.Add(new DataPoint(CurrentTime, Target));
            chFeedBackDyno.Series[1].Points.Add(new DataPoint(CurrentTime, FeedBack));
            if (chFeedBackDyno.Series[0].Points.Count > 0)
            {
                double Max = chFeedBackDyno.Series[0].Points[chFeedBackDyno.Series[0].Points.Count - 1].XValue;
                if (Max > DynoDuration)
                {
                    chFeedBackDyno.ChartAreas[0].AxisX.Minimum = Max - DynoDuration;
                    chFeedBackDyno.ChartAreas[0].AxisX.Maximum = Max;
                }
                else
                {
                    chFeedBackDyno.ChartAreas[0].AxisX.Minimum = 0;
                    chFeedBackDyno.ChartAreas[0].AxisX.Maximum = DynoDuration;
                }
                chFeedBackDyno.Invalidate();
            }


            if (TransientMode.TransientModeData.EM == EngineMode.Alpha_Mode)
            {
                Target = DaqData.SystemStatus.GetValue("TargetEngAlpha");
                FeedBack = DaqData.SystemStatus.GetValue("FeedbackAlpha");
            }
            else
            {
                Target = DaqData.SystemStatus.GetValue("TargetEngTorque");
                FeedBack = DaqData.SystemStatus.GetValue("FeedbackTorque");
            }

            if (chFeedBackEngine.Series[0].Points.Count > EngineDuration * 10 + 1)
            {
                chFeedBackEngine.Series[0].Points.RemoveAt(0);
                chFeedBackEngine.Series[1].Points.RemoveAt(0);
            }
            chFeedBackEngine.Series[0].Points.Add(new DataPoint(CurrentTime, Target));
            chFeedBackEngine.Series[1].Points.Add(new DataPoint(CurrentTime, FeedBack));
            if (chFeedBackEngine.Series[0].Points.Count > 0)
            {
                double Max = chFeedBackEngine.Series[0].Points[chFeedBackEngine.Series[0].Points.Count - 1].XValue;
                if (Max > EngineDuration)
                {
                    chFeedBackEngine.ChartAreas[0].AxisX.Minimum = Max - EngineDuration;
                    chFeedBackEngine.ChartAreas[0].AxisX.Maximum = Max;
                }
                else
                {
                    chFeedBackEngine.ChartAreas[0].AxisX.Minimum = 0;
                    chFeedBackEngine.ChartAreas[0].AxisX.Maximum = EngineDuration;
                }
                chFeedBackEngine.Invalidate();
            }
        }

        private void SetValues()
        {
            lbFBSpeed.Text = (DaqData.SystemStatus.GetValue("FeedbackSpeed")).ToString() + " RPM";
            lbFBTorque.Text = (DaqData.SystemStatus.GetValue("FeedbackTorque")).ToString() + " Nm";
            lbFBAlpha.Text = (DaqData.SystemStatus.GetValue("FeedbackAlpha")).ToString() + " %";

            lbTGDSpeed.Text = (DaqData.SystemStatus.GetValue("TargetDynoSpeed")).ToString() + " RPM";
            lbTGDTorque.Text = (DaqData.SystemStatus.GetValue("TargetDynoTorque")).ToString() + " Nm";
            lbTGEAlpha.Text = (DaqData.SystemStatus.GetValue("TargetEngAlpha")).ToString() + " %";
            lbTGETorque.Text = (DaqData.SystemStatus.GetValue("TargetEngTorque")).ToString() + " Nm";
        }

        private void SetPIDModePanel()
        {
            if (DaqData.SystemStatus.PIDMode_Status == PIDModeStatus.Idle)
                ButtonStatusChange(btnIDLE, true, true);
            else
                ButtonStatusChange(btnIDLE, false, true);

            if (DaqData.SystemStatus.PIDMode_Status == PIDModeStatus.IdleControl)
                ButtonStatusChange(btnIdleControl, true, true);
            else
                ButtonStatusChange(btnIdleControl, false, true);

            if (DaqData.SystemStatus.PIDMode_Status == PIDModeStatus.SpeedAlpha)
                ButtonStatusChange(btnNA, true, true);
            else
                ButtonStatusChange(btnNA, false, true);

            if (DaqData.SystemStatus.PIDMode_Status == PIDModeStatus.SpeedTorque)
                ButtonStatusChange(btnNT, true, true);
            else
                ButtonStatusChange(btnNT, false, true);

            if (DaqData.SystemStatus.PIDMode_Status == PIDModeStatus.TorqueAlpha)
                ButtonStatusChange(btnTA, true, true);
            else
                ButtonStatusChange(btnTA, false, true);

            if (DaqData.SystemStatus.PIDMode_Status == PIDModeStatus.TorqueSpeed)
                ButtonStatusChange(btnTN, true, true);
            else
                ButtonStatusChange(btnTN, false, true);
        }

        private void SetFrontPanel()
        {
            if (DaqData.sysVarInfo.FrontPanel_Variables_Output[0].IOValue == 1)
                ButtonStatusChange(btnDyno, true, true);
            else
                ButtonStatusChange(btnDyno, false, true);

            if (DaqData.sysVarInfo.FrontPanel_Variables_Output[1].IOValue == 1)
                ButtonStatusChange(btnEngine, true, true);
            else
                ButtonStatusChange(btnEngine, false, true);

            if (DaqData.sysVarInfo.FrontPanel_Variables_Output[2].IOValue == 1)
                ButtonStatusChange(btnManual, true, true);
            else
                ButtonStatusChange(btnManual, false, true);

            if (DaqData.sysVarInfo.FrontPanel_Variables_Output[4].IOValue == 1)
                ButtonStatusChange(btnAuto, true, true);
            else
                ButtonStatusChange(btnAuto, false, true);


            if (DaqData.sysVarInfo.FrontPanel_Variables_Output[3].IOValue == 1)
                ButtonStatusChange(btnRemote, true, true);
            else
                ButtonStatusChange(btnRemote, false, true);
        }

        private void ButtonStatusChange(Button btn, bool status, bool enable)
        {
            if (status)
            {
                btn.BackgroundImage = global::AtomUI.Properties.Resources.on버튼;
            }
            else
            {
                if (enable)
                    btn.BackgroundImage = global::AtomUI.Properties.Resources.off버튼;
                else
                    btn.BackgroundImage = global::AtomUI.Properties.Resources.off버튼_회색;
            }

            btn.Enabled = enable;
        }

        private void groupBox7_SizeChanged(object sender, EventArgs e)
        {
            GraphResize();
        }

        private void GraphResize()
        {
            chFeedBackDyno.Height = groupBox7.Height - 52;
            chFeedBackEngine.Height = groupBox7.Height - 52;

            chFeedBackDyno.Width = (groupBox7.Width - 52) / 2;
            chFeedBackEngine.Width = (groupBox7.Width - 52) / 2;

            chFeedBackEngine.Left = (groupBox7.Width / 2) + 10;

            lbTitleDyno.Left = chFeedBackDyno.Left + chFeedBackDyno.Width - 110;
            lbDynoDuration.Left = chFeedBackDyno.Left + chFeedBackDyno.Width - 110;
            tbDynoDuration.Left = chFeedBackDyno.Left + chFeedBackDyno.Width - 110;

            lbTitleEngine.Left = chFeedBackEngine.Left + chFeedBackEngine.Width - 110;
            lbEngineDuration.Left = chFeedBackEngine.Left + chFeedBackEngine.Width - 110;
            tbEngineDuration.Left = chFeedBackEngine.Left + chFeedBackEngine.Width - 110;

        }

        private void chart1_CursorPositionChanged(object sender, CursorEventArgs e)
        {
            SetData(e.NewPosition);
        }

        private void SetData(double Position)
        {
            int Time = (int)Position;
            lbTime.Text = "-";
            lbDyno.Text = "-";
            lbEngine.Text = "-";

            DataRowCollection DT = TransientMode.TransientModeData.Data.Tables["TransientMode"].Rows;
            for (int i = 0; i < DT.Count; i++)
            {
                if (Time == (int)DT[i][0])
                {
                    lbTime.Text = ((int)DT[i][0]).ToString();
                    lbDyno.Text = ((double)DT[i][1]).ToString();
                    lbEngine.Text = ((double)DT[i][2]).ToString();
                }
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Load setting from File. Current setting will be overwriting. Continue?", "Step Mode Save", MessageBoxButtons.YesNo))
            {
                Ethercat.sysInfo.sysSettings.TranModeFileName = tbFileName.Text + ".xml";
                Ethercat.sysInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SysInfoDir + "\\" + Ethercat.sysInfo.sysSettings.SysInfoFileName);
                DataLoad();
                Redraw();
            }
        }

        int DynoDuration = 10;
        int EngineDuration = 10;

        private void tbDynoDuration_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DynoDuration = int.Parse(tbDynoDuration.Text);
            }
            catch
            {

            }
        }

        private void tbEngineDuration_TextChanged(object sender, EventArgs e)
        {
            try
            {
                EngineDuration = int.Parse(tbEngineDuration.Text);
            }
            catch
            {

            }
        }

        int CurrentMode;
        int AutoError;
        int AutoMode;
        int AutoSW;
        int Control;
        int Mode;
        int ProfileStatus;

        private void PCWriteMake(int AutoError, int AutoMode, int AutoSW, int Control, int Mode, int ProfileStatus)
        {
            this.AutoError = AutoError;
            this.AutoMode = AutoMode;
            this.AutoSW = AutoSW;
            this.Control = Control;
            this.Mode = Mode;
            this.ProfileStatus = ProfileStatus;

            PC_Write = new int[0x2FF];

            PC_Write[AutoError] = 1;
            PC_Write[AutoMode] = 1;
            PC_Write[AutoSW] = 1;
            PC_Write[Control] = 1;
            PC_Write[Mode] = 1;
            PC_Write[0x120] = 1;

            switch (this.CurrentMode)
            {
                case (int)enumMode.ModeIdle:
                    break;
                case (int)enumMode.ModeIdleControl:
                    break;
                case (int)enumMode.ModeTA:
                    switch (ProfileStatus)
                    {
                        case (int)enumProfileStatus.ProfileWriting:
                            PC_Write[(int)PCWriteIndex.TAwriting] = 1;
                            break;
                        case (int)enumProfileStatus.ProfileComplete:
                            PC_Write[(int)PCWriteIndex.TAcomplete] = 1;
                            break;
                        case (int)enumProfileStatus.ProfileUsing:
                            PC_Write[(int)PCWriteIndex.TAcomplete] = 1;
                            PC_Write[(int)PCWriteIndex.TAusing] = 1;
                            break;
                    }
                    break;
                case (int)enumMode.ModeNA:
                    switch (ProfileStatus)
                    {
                        case (int)enumProfileStatus.ProfileWriting:
                            PC_Write[(int)PCWriteIndex.NAwriting] = 1;
                            break;
                        case (int)enumProfileStatus.ProfileComplete:
                            PC_Write[(int)PCWriteIndex.NAcomplete] = 1;
                            break;
                        case (int)enumProfileStatus.ProfileUsing:
                            PC_Write[(int)PCWriteIndex.NAcomplete] = 1;
                            PC_Write[(int)PCWriteIndex.NAusing] = 1;
                            break;
                    }
                    break;
                case (int)enumMode.ModeTN:
                    switch (ProfileStatus)
                    {
                        case (int)enumProfileStatus.ProfileWriting:
                            PC_Write[(int)PCWriteIndex.TNwriting] = 1;
                            break;
                        case (int)enumProfileStatus.ProfileComplete:
                            PC_Write[(int)PCWriteIndex.TNcomplete] = 1;
                            PC_Write[(int)PCWriteIndex.TNusing] = 1;
                            break;
                        case (int)enumProfileStatus.ProfileUsing:
                            PC_Write[(int)PCWriteIndex.TNcomplete] = 1;
                            PC_Write[(int)PCWriteIndex.TNusing] = 1;
                            break;
                    }
                   break;
                case (int)enumMode.ModeNT:
                    switch (ProfileStatus)
                    {
                        case (int)enumProfileStatus.ProfileWriting:
                            PC_Write[(int)PCWriteIndex.NTwriting] = 1;
                            break;
                        case (int)enumProfileStatus.ProfileComplete:
                            PC_Write[(int)PCWriteIndex.NTcomplete] = 1;
                            PC_Write[(int)PCWriteIndex.NTusing] = 1;
                            break;
                        case (int)enumProfileStatus.ProfileUsing:
                            PC_Write[(int)PCWriteIndex.NTcomplete] = 1;
                            PC_Write[(int)PCWriteIndex.NTusing] = 1;
                            break;
                    }
                    break;
            }
        }

        enum enumProfileStatus
        {
            ProfileNone = 0,
            ProfileWriting = 1,
            ProfileComplete = 2,
            ProfileUsing = 3
        }

        enum enumAutoError
        {
            AUTONormal = 0x06,
            AUTOFault = 0x07
        }
        enum enumAutoMode
        {
            AUTOInit = 0x09,
            AUTOIdle = 0x0A,
            AUTORun = 0x0B,
            AUTOPause = 0x0C,
            AUTODown = 0x0D
        }
        enum enumAutoSW
        {
            AUTOSWON = 0x20,
            AUTOSWOFF = 0x21
        }
        enum enumControl
        {
            ControlIdle = 0x31,
            ControlRun = 0x32,
            ControlStop = 0x33
        }
        enum enumMode
        {
            ModeIdle = 0x60,
            ModeIdleControl = 0x61,
            ModeTA = 0x62,
            ModeNA = 0x63,
            ModeTN = 0x64,
            ModeNT = 0x65
        }

        enum PCWriteIndex
        {
            PcStatus = 0,

            TAwriting = 0x90,
            TAcomplete = 0x91,
            TAusing = 0x92,

            NAwriting = 0x94,
            NAcomplete = 0x95,
            NAusing = 0x96,

            TNwriting = 0x98,
            TNcomplete = 0x99,
            TNusing = 0x9A,

            NTwriting = 0x9C,
            NTcomplete = 0x9D,
            NTusing = 0x9E,

            ControlCMD = 0x30,
            RunDriveMode = 0x60,
            //ProfileWrite = 0x90,
            eCatOutputData = 0x200
        }

        enum ProfileWriteIndex
        {
            Start = 0,
            Dyno = 100,
            Engine = 2600,
            DynoEngDevide = 2499
        }

        Dictionary<string, string> VarNameDic = new Dictionary<string, string>();

        private void DicInit()
        {
            VarNameDic.Clear();

            VarNameDic.Add("PC_Write", "g_PC_Write_Data");
            VarNameDic.Add("AutoMd", "AutoMd_TestCycle");
            VarNameDic.Add("TAMode", "g_PC_Write_Data_TqAlphaProfile");
            VarNameDic.Add("NAMode", "g_PC_Write_Data_SpAlphaProfile");
            VarNameDic.Add("TNMode", "g_PC_Write_Data_TqSpProfile");
            VarNameDic.Add("NTMode", "g_PC_Write_Data_SpTqProfile");
        }

        int[] PC_Write = new int[0x2FF];
        double[] Data_Profile = new double[5100];
        string Data_Profile_Name = "";
        bool Data_Profile_Writing = false;

        private void VarInit()
        {
            PC_Write = new int[0x2FF];
            Data_Profile = new double[5100];
        }

        private void ProfileMake()
        {
            Data_Profile = new double[5100];

            // Profile Writing
            // Index 0 : Total Time
            // Index 100~ : DYno Profile max 2300개
            // Index 2600~ : Engine Profile max 2300개
            DataRowCollection DT = TransientMode.TransientModeData.Data.Tables["TransientMode"].Rows;

            if (TransientMode.TransientModeData.DM == DynoMode.Speed_Mode)
            {
                if (TransientMode.TransientModeData.EM == EngineMode.Alpha_Mode)
                {
                    Data_Profile_Name = VarNameDic["NAMode"];
                    CurrentMode = (int)enumMode.ModeNA;
                }
                else
                {
                    Data_Profile_Name = VarNameDic["NTMode"];
                    CurrentMode = (int)enumMode.ModeNT;
                }
            }
            else
            {
                if (TransientMode.TransientModeData.EM == EngineMode.Alpha_Mode)
                {
                    Data_Profile_Name = VarNameDic["TAMode"];
                    CurrentMode = (int)enumMode.ModeTA;
                }
                else
                {
                    MessageBox.Show("Invalid Control Mode");
                }
            }

            if (Data_Profile_Name.Length >0 & DT.Count > 1 & DT.Count < 2300)
            {
                Data_Profile[(int)ProfileWriteIndex.Start] = DT.Count;
                for (int i = 0; i < DT.Count; i++)
                {
                    Data_Profile[(int)ProfileWriteIndex.Dyno + i] = ((double)DT[i][1]);
                    Data_Profile[(int)ProfileWriteIndex.Engine + i] = ((double)DT[i][2]);
                }
            }
            else
            {
                MessageBox.Show("Cycle Count must be smaller than 2300 and greater than 0.");
            }
        }

        private void btnProfileWrite_Click(object sender, EventArgs e)
        {
            //PCWriteMake(int AutoError, int AutoMode, int AutoSW, int Control, int Mode, int ProfileStatus)
            // PCWrite : AUTONormal, AUTOIdle, AUTOSWON, ModeIdle, ProfileWriting
            ProfileMake();
            PCWriteMake((int)enumAutoError.AUTONormal, (int)enumAutoMode.AUTOIdle, (int)enumAutoSW.AUTOSWON, (int)enumControl.ControlIdle, CurrentMode, (int)enumProfileStatus.ProfileWriting);
            Ethercat.Ch.WriteVariable(PC_Write, "g_PC_Write_Data", ProgramBuffer.ACSC_NONE, 0, PC_Write.Length-1, -1, -1);
            // Profile Writing
            Ethercat.Ch.WriteVariable(Data_Profile, Data_Profile_Name, ProgramBuffer.ACSC_NONE, 0, Data_Profile.Length-1, -1, -1);

            // PCWrite : AUTONormal, AUTOIdle, AUTOSWON, ModeIdle, ProfileComplete
            PCWriteMake((int)enumAutoError.AUTONormal, (int)enumAutoMode.AUTOIdle, (int)enumAutoSW.AUTOSWON, (int)enumControl.ControlIdle, CurrentMode, (int)enumProfileStatus.ProfileComplete);
            Ethercat.Ch.WriteVariable(PC_Write, "g_PC_Write_Data", ProgramBuffer.ACSC_NONE, 0, PC_Write.Length - 1, -1, -1);

            PCWriteMake((int)enumAutoError.AUTONormal, (int)enumAutoMode.AUTOIdle, (int)enumAutoSW.AUTOSWON, (int)enumControl.ControlIdle, CurrentMode, (int)enumProfileStatus.ProfileUsing);
            Ethercat.Ch.WriteVariable(PC_Write, "g_PC_Write_Data", ProgramBuffer.ACSC_NONE, 0, PC_Write.Length - 1, -1, -1);

            Data_Profile_Writing = true;

            MessageBox.Show("Profile Writing Success. \r\nPress AUTOMATIC button on Controlbox and Press run button on this windows.", "Information");

        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            // PCREAD +0x33 Automatic ON -> then enable button

            // Transient Mode Setting
            Ethercat.Ch.WriteVariable(1, "AutoMd_TestCycle", ProgramBuffer.ACSC_BUFFER_4, -1, -1, -1, -1);
            // PCWrite : AUTONormal, AUTOIdle, AUTOSWON, ModeChange, ProfileComplete
            PCWriteMake((int)enumAutoError.AUTONormal, (int)enumAutoMode.AUTOIdle, (int)enumAutoSW.AUTOSWON, (int)enumControl.ControlRun, CurrentMode, (int)enumProfileStatus.ProfileComplete);
            Ethercat.Ch.WriteVariable(PC_Write, "g_PC_Write_Data", ProgramBuffer.ACSC_NONE, 0, PC_Write.Length - 1, -1, -1);

            CurrentTime = 0;
            chFeedBackDyno.Series[0].Points.Clear();
            chFeedBackDyno.Series[1].Points.Clear();
            chFeedBackEngine.Series[0].Points.Clear();
            chFeedBackEngine.Series[1].Points.Clear();
            tbDynoDuration.Text = TotalTime.ToString();
            tbEngineDuration.Text = TotalTime.ToString();
            run = true;
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            // PCWrite : AUTONormal, AUTOIdle, AUTOSWON, ModeChange, ProfileComplete
            PCWriteMake((int)enumAutoError.AUTONormal, (int)enumAutoMode.AUTOIdle, (int)enumAutoSW.AUTOSWON, (int)enumControl.ControlStop, CurrentMode, (int)enumProfileStatus.ProfileComplete);
            Ethercat.Ch.WriteVariable(PC_Write, "g_PC_Write_Data", ProgramBuffer.ACSC_NONE, 0, PC_Write.Length - 1, -1, -1);
            run = false;

        }

        private void rdDynoSP_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void rdDynoTQ_CheckedChanged(object sender, EventArgs e)
        {
            if (rdEngineTQ.Checked)
            {
                MessageBox.Show("Engine is Already in Torque mode");
                rdDynoSP.Checked = true;
            }
        }

        private void rdEngineALP_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void rdEngineTQ_CheckedChanged(object sender, EventArgs e)
        {
            if (rdDynoTQ.Checked)
            {
                MessageBox.Show("Dyno is Already in Torque mode");
                rdEngineALP.Checked = true;
            }
        }

        private void lbFileList_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbFileName.Text = lbFileList.SelectedItem.ToString();
        }
    }
}
