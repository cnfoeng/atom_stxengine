﻿namespace AtomUI.ControllerForm
{
    partial class ACS_Add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CancelBtn = new System.Windows.Forms.Button();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.ConnTypeCmB = new System.Windows.Forms.ComboBox();
            this.BaudRateCmB = new System.Windows.Forms.ComboBox();
            this.RemoteAddressTB = new System.Windows.Forms.TextBox();
            this.SlotNumberTB = new System.Windows.Forms.TextBox();
            this.CommPortCmB = new System.Windows.Forms.ComboBox();
            this.PortLb = new System.Windows.Forms.Label();
            this.RateLb = new System.Windows.Forms.Label();
            this.ChannelLb = new System.Windows.Forms.Label();
            this.ComTypeCmB = new System.Windows.Forms.ComboBox();
            this.chkHMI = new System.Windows.Forms.CheckBox();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnFileOpen = new System.Windows.Forms.Button();
            this.tbFilename = new System.Windows.Forms.TextBox();
            this.lblFile = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CancelBtn
            // 
            this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CancelBtn.Location = new System.Drawing.Point(196, 311);
            this.CancelBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(91, 23);
            this.CancelBtn.TabIndex = 59;
            this.CancelBtn.Text = "Cancel";
            // 
            // SaveBtn
            // 
            this.SaveBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.SaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveBtn.Location = new System.Drawing.Point(65, 311);
            this.SaveBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(91, 23);
            this.SaveBtn.TabIndex = 58;
            this.SaveBtn.Text = "Save";
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // ConnTypeCmB
            // 
            this.ConnTypeCmB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ConnTypeCmB.Items.AddRange(new object[] {
            "Point to Point",
            "Network"});
            this.ConnTypeCmB.Location = new System.Drawing.Point(118, 161);
            this.ConnTypeCmB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ConnTypeCmB.Name = "ConnTypeCmB";
            this.ConnTypeCmB.Size = new System.Drawing.Size(218, 20);
            this.ConnTypeCmB.TabIndex = 55;
            // 
            // BaudRateCmB
            // 
            this.BaudRateCmB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BaudRateCmB.Items.AddRange(new object[] {
            "Auto",
            "300",
            "1200",
            "4800",
            "9600",
            "19200",
            "57600",
            "115200"});
            this.BaudRateCmB.Location = new System.Drawing.Point(118, 161);
            this.BaudRateCmB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BaudRateCmB.Name = "BaudRateCmB";
            this.BaudRateCmB.Size = new System.Drawing.Size(150, 20);
            this.BaudRateCmB.TabIndex = 54;
            // 
            // RemoteAddressTB
            // 
            this.RemoteAddressTB.Location = new System.Drawing.Point(118, 118);
            this.RemoteAddressTB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.RemoteAddressTB.Name = "RemoteAddressTB";
            this.RemoteAddressTB.Size = new System.Drawing.Size(218, 21);
            this.RemoteAddressTB.TabIndex = 53;
            this.RemoteAddressTB.Text = "10.0.0.100";
            // 
            // SlotNumberTB
            // 
            this.SlotNumberTB.Location = new System.Drawing.Point(118, 118);
            this.SlotNumberTB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SlotNumberTB.Name = "SlotNumberTB";
            this.SlotNumberTB.Size = new System.Drawing.Size(104, 21);
            this.SlotNumberTB.TabIndex = 52;
            this.SlotNumberTB.Text = "-1";
            // 
            // CommPortCmB
            // 
            this.CommPortCmB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CommPortCmB.Items.AddRange(new object[] {
            "Com1",
            "Com2",
            "Com3",
            "Com4",
            "Com5",
            "Com6",
            "Com7",
            "Com8",
            "Com9",
            "Com10",
            "Com11",
            "Com12",
            "Com13",
            "Com14",
            "Com15",
            "Com16"});
            this.CommPortCmB.Location = new System.Drawing.Point(118, 118);
            this.CommPortCmB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CommPortCmB.Name = "CommPortCmB";
            this.CommPortCmB.Size = new System.Drawing.Size(150, 20);
            this.CommPortCmB.TabIndex = 51;
            // 
            // PortLb
            // 
            this.PortLb.Location = new System.Drawing.Point(27, 111);
            this.PortLb.Name = "PortLb";
            this.PortLb.Size = new System.Drawing.Size(94, 30);
            this.PortLb.TabIndex = 50;
            this.PortLb.Text = "Port";
            this.PortLb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // RateLb
            // 
            this.RateLb.Location = new System.Drawing.Point(27, 158);
            this.RateLb.Name = "RateLb";
            this.RateLb.Size = new System.Drawing.Size(94, 23);
            this.RateLb.TabIndex = 49;
            this.RateLb.Text = "Rate (bps)";
            this.RateLb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ChannelLb
            // 
            this.ChannelLb.Location = new System.Drawing.Point(12, 73);
            this.ChannelLb.Name = "ChannelLb";
            this.ChannelLb.Size = new System.Drawing.Size(66, 22);
            this.ChannelLb.TabIndex = 48;
            this.ChannelLb.Text = "Channel";
            this.ChannelLb.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ComTypeCmB
            // 
            this.ComTypeCmB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComTypeCmB.Items.AddRange(new object[] {
            "Serial",
            "Network",
            "PCI Bus",
            "Simulator"});
            this.ComTypeCmB.Location = new System.Drawing.Point(118, 76);
            this.ComTypeCmB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ComTypeCmB.Name = "ComTypeCmB";
            this.ComTypeCmB.Size = new System.Drawing.Size(218, 20);
            this.ComTypeCmB.TabIndex = 47;
            this.ComTypeCmB.SelectedIndexChanged += new System.EventHandler(this.ComTypeCmB_SelectedIndexChanged);
            // 
            // chkHMI
            // 
            this.chkHMI.AutoSize = true;
            this.chkHMI.Location = new System.Drawing.Point(119, 274);
            this.chkHMI.Name = "chkHMI";
            this.chkHMI.Size = new System.Drawing.Size(116, 16);
            this.chkHMI.TabIndex = 60;
            this.chkHMI.Text = "Use HMI Control";
            this.chkHMI.UseVisualStyleBackColor = true;
            // 
            // tbDescription
            // 
            this.tbDescription.Location = new System.Drawing.Point(119, 33);
            this.tbDescription.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(218, 21);
            this.tbDescription.TabIndex = 62;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(26, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 30);
            this.label1.TabIndex = 61;
            this.label1.Text = "Description";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(119, 33);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(150, 21);
            this.textBox1.TabIndex = 62;
            // 
            // btnFileOpen
            // 
            this.btnFileOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFileOpen.Location = new System.Drawing.Point(295, 206);
            this.btnFileOpen.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnFileOpen.Name = "btnFileOpen";
            this.btnFileOpen.Size = new System.Drawing.Size(43, 23);
            this.btnFileOpen.TabIndex = 76;
            this.btnFileOpen.Text = "...";
            this.btnFileOpen.Click += new System.EventHandler(this.btnFileOpen_Click);
            // 
            // tbFilename
            // 
            this.tbFilename.Location = new System.Drawing.Point(139, 208);
            this.tbFilename.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbFilename.Name = "tbFilename";
            this.tbFilename.Size = new System.Drawing.Size(150, 21);
            this.tbFilename.TabIndex = 75;
            // 
            // lblFile
            // 
            this.lblFile.Location = new System.Drawing.Point(27, 203);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(94, 30);
            this.lblFile.TabIndex = 74;
            this.lblFile.Text = "Channel Definition File";
            this.lblFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ACS_Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(367, 357);
            this.Controls.Add(this.btnFileOpen);
            this.Controls.Add(this.tbFilename);
            this.Controls.Add(this.lblFile);
            this.Controls.Add(this.tbDescription);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkHMI);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.ConnTypeCmB);
            this.Controls.Add(this.BaudRateCmB);
            this.Controls.Add(this.RemoteAddressTB);
            this.Controls.Add(this.SlotNumberTB);
            this.Controls.Add(this.CommPortCmB);
            this.Controls.Add(this.PortLb);
            this.Controls.Add(this.RateLb);
            this.Controls.Add(this.ChannelLb);
            this.Controls.Add(this.ComTypeCmB);
            this.Name = "ACS_Add";
            this.Text = "ACS 추가";
            this.Load += new System.EventHandler(this.ACS_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.ComboBox ConnTypeCmB;
        private System.Windows.Forms.ComboBox BaudRateCmB;
        private System.Windows.Forms.TextBox RemoteAddressTB;
        private System.Windows.Forms.TextBox SlotNumberTB;
        private System.Windows.Forms.ComboBox CommPortCmB;
        private System.Windows.Forms.Label PortLb;
        private System.Windows.Forms.Label RateLb;
        private System.Windows.Forms.Label ChannelLb;
        private System.Windows.Forms.ComboBox ComTypeCmB;
        private System.Windows.Forms.CheckBox chkHMI;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbDescription;
        private System.Windows.Forms.Button btnFileOpen;
        private System.Windows.Forms.TextBox tbFilename;
        private System.Windows.Forms.Label lblFile;
    }
}