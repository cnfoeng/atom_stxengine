﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;
using ACS.SPiiPlusNET;

namespace AtomUI.ControllerForm
{
    public partial class ACS_Add : Form
    {
        public acsConnectionInfo ConnectionInfo = null;
        public ACS_Add()
        {
            InitializeComponent();
        }
        public ACS_Add(acsConnectionInfo ConnectionInfo)
        {
            InitializeComponent();
            this.ConnectionInfo = ConnectionInfo;
        }

        private int ComTypeOld;
        //System.Drawing.Image iGrey;
        //System.Drawing.Image iGreen;

        private void ACS_Load(object sender, EventArgs e)
        {
            if (ConnectionInfo == null)
            {
                tbDescription.Text = "ACS Controller";
                BaudRateCmB.SelectedIndex = 0; // Ethercat.sysInfo.ConnectionInfo.Baud_Rate;   //  Baud rate
                ConnTypeCmB.SelectedIndex = 1; // Ethercat.sysInfo.ConnectionInfo.ConnType;   //  Ethernet connection type
                ComTypeOld = 1; // Ethercat.sysInfo.ConnectionInfo.ComType;
                ComTypeCmB.SelectedIndex = 1; // Ethercat.sysInfo.ConnectionInfo.ComType;    //  Communication medium (e.g. Serial, Ethernet)
                CommPortCmB.SelectedIndex = 0; // Ethercat.sysInfo.ConnectionInfo.CommPort;   //  COM channel
                RemoteAddressTB.Text = "10.0.0.100";  //Ethercat.sysInfo.ConnectionInfo.RemoteAddress;
                SlotNumberTB.Text = "-1";  //Ethercat.sysInfo.ConnectionInfo.SlotNumber;
                                           //ConnectionInfo.Baud_RateValue = "AUTO";
            }
            else
            {
                tbDescription.Text = ConnectionInfo.Description;
                BaudRateCmB.SelectedIndex = ConnectionInfo.Baud_Rate; // Ethercat.sysInfo.ConnectionInfo.Baud_Rate;   //  Baud rate
                ConnTypeCmB.SelectedIndex = ConnectionInfo.ConnType; // Ethercat.sysInfo.ConnectionInfo.ConnType;   //  Ethernet connection type
                ComTypeOld = ConnectionInfo.ComType; // Ethercat.sysInfo.ConnectionInfo.ComType;
                ComTypeCmB.SelectedIndex = ConnectionInfo.ComType; // Ethercat.sysInfo.ConnectionInfo.ComType;    //  Communication medium (e.g. Serial, Ethernet)
                CommPortCmB.SelectedIndex = ConnectionInfo.CommPort; // Ethercat.sysInfo.ConnectionInfo.CommPort;   //  COM channel
                RemoteAddressTB.Text = ConnectionInfo.RemoteAddress;  //Ethercat.sysInfo.ConnectionInfo.RemoteAddress;
                SlotNumberTB.Text = ConnectionInfo.SlotNumber;  //Ethercat.sysInfo.ConnectionInfo.SlotNumber;
                                                                //ConnectionInfo.Baud_RateValue = "AUTO";
                lblFile.Visible = false;
                tbFilename.Visible = false;
                btnFileOpen.Visible = false;
            }
        }

        private void ComTypeCmB_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Show/hide the form's controls relevant for the selected communication media
            // Set all visibility state to false
            CommPortCmB.Visible = false;
            BaudRateCmB.Visible = false;
            ConnTypeCmB.Visible = false;
            RemoteAddressTB.Visible = false;
            SlotNumberTB.Visible = false;
            PortLb.Visible = false;
            RateLb.Visible = false;

            //  Turn on visibility based on user selection
            if (ComTypeCmB.SelectedIndex == 0)       // serial
            {
                CommPortCmB.Visible = true;
                BaudRateCmB.Visible = true;
                PortLb.Visible = true;
                RateLb.Visible = true;
                PortLb.Text = "Port";
                RateLb.Text = "Rate (bps)";
            }
            else if (ComTypeCmB.SelectedIndex == 1)         // Ethernet
            {
                ConnTypeCmB.Visible = true;
                RemoteAddressTB.Visible = true;
                PortLb.Visible = true;
                RateLb.Visible = true;
                PortLb.Text = "Remote Address";
                RateLb.Text = "Connection";
            }
            else if (ComTypeCmB.SelectedIndex == 2)     // PCI
            {
                SlotNumberTB.Visible = true;
                PortLb.Visible = true;
                PortLb.Text = "Slot Number";
            }
        }

        private void ACSConnect_FormClosing(object sender, FormClosingEventArgs e)
        {
            //e.Cancel = true;
            //this.Hide();
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            bool Error = false;
            if (ConnectionInfo == null)
            {
                foreach (acsConnectionInfo info in Ethercat.sysInfo.HardWareInfo)
                {
                    if ((info.HWType == HardWareType.ACS) && (info.RemoteAddress == RemoteAddressTB.Text))
                    {
                        Error = true;
                        break;
                    }
                }

                if (Error)
                {
                    MessageBox.Show("동일한 하드웨어가 이미 추가되어 있습니다.");
                }
                else

                {
                    acsConnectionInfo ConnectionInfo3 = new acsConnectionInfo();
                    ConnectionInfo3.HWType = HardWareType.ACS;
                    ConnectionInfo3.Baud_Rate = BaudRateCmB.SelectedIndex;   //  Baud rate
                    ConnectionInfo3.ConnType = ConnTypeCmB.SelectedIndex;   //  Ethernet connection type
                    ConnectionInfo3.ComType = ComTypeCmB.SelectedIndex;    //  Communication medium (e.g. Serial, Ethernet)
                    ConnectionInfo3.CommPort = CommPortCmB.SelectedIndex;   //  COM channel
                    ConnectionInfo3.RemoteAddress = RemoteAddressTB.Text;
                    ConnectionInfo3.SlotNumber = SlotNumberTB.Text;
                    ConnectionInfo3.Baud_RateValue = BaudRateCmB.SelectedText;   //  Baud rate
                    ConnectionInfo3.HMIExist = chkHMI.Checked;
                    ConnectionInfo3.Description = tbDescription.Text;
                    Ethercat.sysInfo.HardWareInfo.Add(ConnectionInfo3);

                    if (System.IO.File.Exists(tbFilename.Text))
                        AddChannels(tbFilename.Text);
                    this.Close();
                }
            } else
            {
                ConnectionInfo.HWType = HardWareType.ACS;
                ConnectionInfo.Baud_Rate = BaudRateCmB.SelectedIndex;   //  Baud rate
                ConnectionInfo.ConnType = ConnTypeCmB.SelectedIndex;   //  Ethernet connection type
                ConnectionInfo.ComType = ComTypeCmB.SelectedIndex;    //  Communication medium (e.g. Serial, Ethernet)
                ConnectionInfo.CommPort = CommPortCmB.SelectedIndex;   //  COM channel
                ConnectionInfo.RemoteAddress = RemoteAddressTB.Text;
                ConnectionInfo.SlotNumber = SlotNumberTB.Text;
                ConnectionInfo.Baud_RateValue = BaudRateCmB.SelectedText;   //  Baud rate
                ConnectionInfo.HMIExist = chkHMI.Checked;
                ConnectionInfo.Description = tbDescription.Text;
                this.Close();
            }
        }

        public bool AddChannels(string FileName)
        {
            try
            {
                acsChannelInfo channelItem;
                VariableInfo varItem;

                int Index = 0;

                string[] acsAllValue = System.IO.File.ReadAllLines(FileName);
                List<string> varlist = acsAllValue.ToList<string>();
                varlist.RemoveAt(0);
                // 일단, acs 하드웨어 정보는 자동으로 먼저 읽는 것으로 한다.
                // 변수 정의는 하드웨어 채널 정의 순서에 맞춰져야 한다.
                //acsModuleInfo item = new acsModuleInfo("ACS DAQ", "ACS DAQ", 0, 0, ChannelIO.Input, ChannelType.Analog, 0, 0, acsAllValue.Length);
                //item.UseFor = ChannelUseFor.DAQ;
                //Ethercat.sysInfo.MEXAModuleInfos.Add(item);
                //Ethercat.chInfo.AI_Channels.Clear();
                //Ethercat.chInfo.DI_Channels.Clear();

                List<acsChannelInfo> CHList = null;
                List<VariableInfo> VarList = null;
                DaqData.sysVarInfo.AI_Variables.Clear();
                DaqData.sysVarInfo.DI_Variables.Clear();

                DaqData.sysVarInfo.AO_Variables.Clear();
                DaqData.sysVarInfo.DO_Variables.Clear();

                foreach (string acsVariable in varlist)
                {
                    string[] ChannelData = acsVariable.Split(',');

                    if (ChannelData[2] == "AI")
                    {
                        CHList = Ethercat.chInfo.AI_Channels;
                        VarList = DaqData.sysVarInfo.AI_Variables;
                    }
                    else if (ChannelData[2] == "DI")
                    {
                        CHList = Ethercat.chInfo.DI_Channels;
                        VarList = DaqData.sysVarInfo.DI_Variables;
                    }
                    else if (ChannelData[2] == "DO")
                    {
                        CHList = Ethercat.chInfo.DO_Channels;
                        VarList = DaqData.sysVarInfo.DO_Variables;
                    }
                    else if (ChannelData[2] == "AO")
                    {
                        CHList = Ethercat.chInfo.AO_Channels;
                        VarList = DaqData.sysVarInfo.AO_Variables;
                    }

                    if (CHList != null && VarList != null && CHList.Count > VarList.Count)
                    {
                        channelItem = CHList[VarList.Count];
                        varItem = new VariableInfo(channelItem, ChannelData[1], ChannelData[1]);
                        varItem.Unit = ChannelData[3];
                        varItem.CalibrationTable.Add(new ValuePair(double.Parse(ChannelData[6]), double.Parse(ChannelData[4])));
                        varItem.CalibrationTable.Add(new ValuePair(double.Parse(ChannelData[7]), double.Parse(ChannelData[5])));
                        varItem.CalType = CalibrationType.CalByTable;
                        varItem.Alarm_High = (double.Parse(ChannelData[10]));
                        varItem.Alarm_Low = (double.Parse(ChannelData[9]));
                        varItem.AlarmUse = (bool.Parse(ChannelData[8]));
                        varItem.Warning_High = (double.Parse(ChannelData[13]));
                        varItem.Warning_Low = (double.Parse(ChannelData[12]));
                        varItem.AlarmUse = (bool.Parse(ChannelData[11]));
                        varItem.Index = Index++;
                        VarList.Add(varItem);
                    } else
                    {
                        MessageBox.Show("Error. Variable Definition is not Correct!");
                        return false;
                    }
                    CHList = null;
                    VarList = null;

                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error. Variable Definition is not Correct!");
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n AddMexaChannels Error!!");
                return false;
            }
        }

        private void btnFileOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "csv File Format|*.csv";
            open.Title = "Load an ACO Channel Define File";

            open.InitialDirectory = Ethercat.sysInfo.sysSettings.DefaultDirectory;
            open.FileName = "";
            if (DialogResult.OK == open.ShowDialog() && open.FileName != "")
            {
                tbFilename.Text = open.FileName.ToString();
            }
        }
    }
}
