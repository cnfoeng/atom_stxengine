﻿namespace AtomUI.ControllerForm
{
    partial class ChannelInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            this.lvList = new System.Windows.Forms.ListView();
            this.Header6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnSave = new System.Windows.Forms.Button();
            this.cmbVariableDefine = new System.Windows.Forms.ComboBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // lvList
            // 
            this.lvList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lvList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Header6,
            this.Header8,
            this.Header1,
            this.Header2,
            this.Header3,
            this.Header4,
            this.Header5,
            this.Header10,
            this.Header7,
            this.Header11,
            this.Header12});
            this.lvList.FullRowSelect = true;
            this.lvList.GridLines = true;
            listViewGroup1.Header = "ListViewGroup";
            listViewGroup1.Name = "Global Variables";
            listViewGroup2.Header = "ListViewGroup";
            listViewGroup2.Name = "Local Variables";
            this.lvList.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2});
            this.lvList.Location = new System.Drawing.Point(12, 33);
            this.lvList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lvList.MultiSelect = false;
            this.lvList.Name = "lvList";
            this.lvList.ShowGroups = false;
            this.lvList.Size = new System.Drawing.Size(1170, 394);
            this.lvList.TabIndex = 79;
            this.lvList.UseCompatibleStateImageBehavior = false;
            this.lvList.View = System.Windows.Forms.View.Details;
            // 
            // Header6
            // 
            this.Header6.DisplayIndex = 1;
            this.Header6.Text = "USE";
            this.Header6.Width = 61;
            // 
            // Header8
            // 
            this.Header8.DisplayIndex = 2;
            this.Header8.Text = "Index";
            this.Header8.Width = 54;
            // 
            // Header1
            // 
            this.Header1.DisplayIndex = 3;
            this.Header1.Text = "Variable Name";
            this.Header1.Width = 136;
            // 
            // Header2
            // 
            this.Header2.DisplayIndex = 4;
            this.Header2.Text = "Product Name";
            this.Header2.Width = 172;
            // 
            // Header3
            // 
            this.Header3.DisplayIndex = 0;
            this.Header3.Text = "Slave No";
            this.Header3.Width = 75;
            // 
            // Header4
            // 
            this.Header4.Text = "IO";
            this.Header4.Width = 73;
            // 
            // Header5
            // 
            this.Header5.Text = "TYPE";
            this.Header5.Width = 73;
            // 
            // Header10
            // 
            this.Header10.Text = "OFFSET";
            this.Header10.Width = 100;
            // 
            // Header7
            // 
            this.Header7.Text = "CHANNEL BIT";
            this.Header7.Width = 144;
            // 
            // Header11
            // 
            this.Header11.Text = "Variable";
            this.Header11.Width = 140;
            // 
            // Header12
            // 
            this.Header12.Text = "Value";
            this.Header12.Width = 92;
            // 
            // btnSave
            // 
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(535, 445);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(123, 40);
            this.btnSave.TabIndex = 78;
            this.btnSave.Text = "OK";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cmbVariableDefine
            // 
            this.cmbVariableDefine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVariableDefine.FormattingEnabled = true;
            this.cmbVariableDefine.Items.AddRange(new object[] {
            "HMI IO",
            "DI",
            "AI",
            "DO",
            "AO",
            "MEXA",
            "ECU",
            "MODBUS",
            "SMOKE METER"});
            this.cmbVariableDefine.Location = new System.Drawing.Point(12, 10);
            this.cmbVariableDefine.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbVariableDefine.Name = "cmbVariableDefine";
            this.cmbVariableDefine.Size = new System.Drawing.Size(281, 20);
            this.cmbVariableDefine.TabIndex = 80;
            this.cmbVariableDefine.SelectedIndexChanged += new System.EventHandler(this.cmbVariableDefine_SelectedIndexChanged);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // ChannelInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1193, 494);
            this.Controls.Add(this.cmbVariableDefine);
            this.Controls.Add(this.lvList);
            this.Controls.Add(this.btnSave);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ChannelInfo";
            this.Text = "ChannelInfo";
            this.Load += new System.EventHandler(this.ChannelInfo_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvList;
        private System.Windows.Forms.ColumnHeader Header8;
        private System.Windows.Forms.ColumnHeader Header1;
        private System.Windows.Forms.ColumnHeader Header2;
        private System.Windows.Forms.ColumnHeader Header3;
        private System.Windows.Forms.ColumnHeader Header4;
        private System.Windows.Forms.ColumnHeader Header5;
        private System.Windows.Forms.ColumnHeader Header6;
        private System.Windows.Forms.ColumnHeader Header10;
        private System.Windows.Forms.ColumnHeader Header7;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ColumnHeader Header11;
        private System.Windows.Forms.ComboBox cmbVariableDefine;
        private System.Windows.Forms.ColumnHeader Header12;
        private System.Windows.Forms.Timer timer1;
    }
}