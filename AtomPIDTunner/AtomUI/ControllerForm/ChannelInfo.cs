﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;

namespace AtomUI.ControllerForm
{
    public partial class ChannelInfo : Form
    {
        public ChannelInfo()
        {
            InitializeComponent();
        }

        private void ChannelInfo_Load(object sender, EventArgs e)
        {
            cmbVariableDefine.Items.Clear();
            foreach (string varType in DaqData.getVarTypeList())
            {
                cmbVariableDefine.Items.Add(varType);
            }
            cmbVariableDefine.SelectedIndex = 3;
        }

        private void cmbVariableDefine_SelectedIndexChanged(object sender, EventArgs e)
        {
            timer1.Stop();
            MakeListView();
            timer1.Start();
        }

        private void MakeListView()
        {
            lvList.Items.Clear();

            string USE = ((VariableType)cmbVariableDefine.SelectedIndex).ToString();
            List<acsChannelInfo> Channels = Ethercat.chInfo.getChannels(cmbVariableDefine.SelectedIndex);
            List<VariableInfo> Variables = DaqData.sysVarInfo.getVariables(cmbVariableDefine.SelectedIndex);
            if (Channels == null)
                return;

            foreach (acsChannelInfo Channel in Channels)
            {
                ListViewItem itm = lvList.Items.Add(USE);
                itm.SubItems.Add(Channel.ChannelIndex.ToString());
                itm.SubItems.Add(Channel.VariableName);
                itm.SubItems.Add(Channel.ModuleInfo.ProductName);
                itm.SubItems.Add(Channel.ModuleInfo.SlaveNo.ToString());

                itm.SubItems.Add(Ethercat.GetIOString(Channel.ModuleInfo.ChannelIO));
                itm.SubItems.Add(Ethercat.GetTypeString(Channel.ModuleInfo.ChannelType));
                itm.SubItems.Add(Channel.Offset.ToString());
                itm.SubItems.Add(Channel.ChannelBit.ToString());
                foreach (VariableInfo var in Variables)
                {
                    if (var.ChannelInfo != null && var.ChannelInfo.ChannelIndex == Channel.ChannelIndex)
                    {
                        itm.SubItems.Add(var.VariableName);
                        itm.SubItems.Add(var.IOValue.ToString());
                    }
                }
            }
        }

        private void RefreshListView()
        {
            try
            {
                List<acsChannelInfo> Channels = Ethercat.chInfo.getChannels(cmbVariableDefine.SelectedIndex);
                List<VariableInfo> Variables = DaqData.sysVarInfo.getVariables(cmbVariableDefine.SelectedIndex);
                if (lvList.Items.Count > 0)
                {
                    if (Channels != null)
                    {
                        for (int i = 0; i < Channels.Count; i++)
                        {
                            acsChannelInfo Channel = Channels[i];
                            if (Variables != null)
                            {
                                foreach (VariableInfo var in Variables)
                                {
                                    if (var.ChannelInfo != null && var.ChannelInfo.ChannelIndex == Channel.ChannelIndex)
                                    {
                                        if (lvList.Items[i].SubItems.Count > 10)
                                        {
                                            if (lvList.Items[i].SubItems[10].Text == var.IOValue.ToString())
                                                lvList.Items[i].SubItems[10].ForeColor = Color.Black;
                                            else
                                            {
                                                lvList.Items[i].SubItems[10].Text = var.IOValue.ToString();
                                                lvList.Items[i].SubItems[10].ForeColor = Color.Red;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception ex)
            {
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "\n connect Error!!");
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            RefreshListView();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
