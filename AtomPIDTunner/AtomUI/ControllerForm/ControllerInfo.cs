﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;

namespace AtomUI.ControllerForm
{
    public partial class ControllerInfo : Form
    {
        public ControllerInfo()
        {
            InitializeComponent();
        }

        private void ControllerInfo_Load(object sender, EventArgs e)
        {
            ListViewItem itm = lvList.Items.Add("Serial Number");
            itm.SubItems.Add(Ethercat.SerialNumber);

        }
    }
}
