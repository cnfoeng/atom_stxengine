﻿namespace AtomUI.ControllerForm
{
    partial class HWConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewGroup listViewGroup3 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup4 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnADD = new System.Windows.Forms.Button();
            this.cmbHWAdd = new System.Windows.Forms.ComboBox();
            this.btnEdit = new System.Windows.Forms.Button();
            this.lvList = new System.Windows.Forms.ListView();
            this.Header8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // btnRemove
            // 
            this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRemove.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnRemove.Location = new System.Drawing.Point(920, 448);
            this.btnRemove.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(123, 40);
            this.btnRemove.TabIndex = 87;
            this.btnRemove.Text = "REMOVE MODULE";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(1058, 448);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(123, 40);
            this.btnCancel.TabIndex = 86;
            this.btnCancel.Text = "DONE";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnADD
            // 
            this.btnADD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnADD.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnADD.Location = new System.Drawing.Point(315, 448);
            this.btnADD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnADD.Name = "btnADD";
            this.btnADD.Size = new System.Drawing.Size(123, 40);
            this.btnADD.TabIndex = 85;
            this.btnADD.Text = "ADD MODULE";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnADD_Click);
            // 
            // cmbHWAdd
            // 
            this.cmbHWAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbHWAdd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHWAdd.FormattingEnabled = true;
            this.cmbHWAdd.ItemHeight = 12;
            this.cmbHWAdd.Items.AddRange(new object[] {
            "ACS",
            "MEXA-9X00",
            "ECU",
            "MODBUS DAQ",
            "SMOKE METER",
            "FUEL METER",
            "BLOWBY METER"});
            this.cmbHWAdd.Location = new System.Drawing.Point(12, 459);
            this.cmbHWAdd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbHWAdd.Name = "cmbHWAdd";
            this.cmbHWAdd.Size = new System.Drawing.Size(281, 20);
            this.cmbHWAdd.TabIndex = 88;
            // 
            // btnEdit
            // 
            this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEdit.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnEdit.Location = new System.Drawing.Point(782, 448);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(123, 40);
            this.btnEdit.TabIndex = 89;
            this.btnEdit.Text = "SAVE MODULE";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // lvList
            // 
            this.lvList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lvList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Header8,
            this.Header1,
            this.Header2,
            this.Header3,
            this.Header4,
            this.Header5,
            this.Header6,
            this.Header10,
            this.Header7,
            this.Header9,
            this.Header11});
            this.lvList.FullRowSelect = true;
            this.lvList.GridLines = true;
            listViewGroup3.Header = "ListViewGroup";
            listViewGroup3.Name = "Global Variables";
            listViewGroup4.Header = "ListViewGroup";
            listViewGroup4.Name = "Local Variables";
            this.lvList.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup3,
            listViewGroup4});
            this.lvList.HideSelection = false;
            this.lvList.Location = new System.Drawing.Point(12, 11);
            this.lvList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lvList.MultiSelect = false;
            this.lvList.Name = "lvList";
            this.lvList.ShowGroups = false;
            this.lvList.Size = new System.Drawing.Size(1169, 420);
            this.lvList.TabIndex = 78;
            this.lvList.UseCompatibleStateImageBehavior = false;
            this.lvList.View = System.Windows.Forms.View.Details;
            this.lvList.DoubleClick += new System.EventHandler(this.lvList_DoubleClick);
            // 
            // Header8
            // 
            this.Header8.Text = "HWType";
            this.Header8.Width = 69;
            // 
            // Header1
            // 
            this.Header1.Text = "Remote Address";
            this.Header1.Width = 120;
            // 
            // Header2
            // 
            this.Header2.Text = "Desc";
            this.Header2.Width = 252;
            // 
            // Header3
            // 
            this.Header3.Text = "ConnType";
            this.Header3.Width = 75;
            // 
            // Header4
            // 
            this.Header4.Text = "ComType";
            this.Header4.Width = 99;
            // 
            // Header5
            // 
            this.Header5.Text = "SlotNumber";
            this.Header5.Width = 80;
            // 
            // Header6
            // 
            this.Header6.Text = "Comm Port";
            this.Header6.Width = 100;
            // 
            // Header10
            // 
            this.Header10.Text = "Slave Unit ID";
            this.Header10.Width = 100;
            // 
            // Header7
            // 
            this.Header7.Text = "Baud Rate";
            this.Header7.Width = 88;
            // 
            // Header9
            // 
            this.Header9.Text = "Baud Rate Value";
            this.Header9.Width = 110;
            // 
            // Header11
            // 
            this.Header11.Text = "HMI Exist";
            this.Header11.Width = 120;
            // 
            // HWConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1196, 509);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.cmbHWAdd);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnADD);
            this.Controls.Add(this.lvList);
            this.Name = "HWConfig";
            this.Text = "HW Module Config";
            this.Load += new System.EventHandler(this.HWConfig_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.HWConfig_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvList;
        private System.Windows.Forms.ColumnHeader Header8;
        private System.Windows.Forms.ColumnHeader Header1;
        private System.Windows.Forms.ColumnHeader Header2;
        private System.Windows.Forms.ColumnHeader Header3;
        private System.Windows.Forms.ColumnHeader Header4;
        private System.Windows.Forms.ColumnHeader Header5;
        private System.Windows.Forms.ColumnHeader Header6;
        private System.Windows.Forms.ColumnHeader Header10;
        private System.Windows.Forms.ColumnHeader Header7;
        private System.Windows.Forms.ColumnHeader Header9;
        private System.Windows.Forms.ColumnHeader Header11;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnADD;
        private System.Windows.Forms.ComboBox cmbHWAdd;
        private System.Windows.Forms.Button btnEdit;
    }
}