﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;

namespace AtomUI.ControllerForm
{
    public partial class HWConfig : Form
    {
        public HWConfig()
        {
            InitializeComponent();
        }

        private void HWConfig_Load(object sender, EventArgs e)
        {
            cmbHWAdd.SelectedIndex = 0;
            cmb_Fill();
            lvList_Refresh();
        }

        private void cmb_Fill()
        {
            cmbHWAdd.Items.Clear();
            foreach(string hwType in Ethercat.getHWList())
            {
                cmbHWAdd.Items.Add(hwType);
            }
        }

        private void lvList_Refresh()
        {
            lvList.Items.Clear();

            foreach (acsConnectionInfo conInfo in Ethercat.sysInfo.HardWareInfo)
            {
                ListViewItem itm = lvList.Items.Add(conInfo.HWType.ToString());
                itm.SubItems.Add(conInfo.RemoteAddress);
                itm.SubItems.Add(conInfo.Description);
                itm.SubItems.Add(conInfo.ConnType.ToString());
                itm.SubItems.Add(conInfo.ComType.ToString());
                itm.SubItems.Add(conInfo.SlotNumber);
                itm.SubItems.Add(conInfo.CommPort.ToString());
                itm.SubItems.Add(conInfo.SlaveUnitIdentifier);
                itm.SubItems.Add(conInfo.Baud_Rate.ToString());
                itm.SubItems.Add(conInfo.Baud_RateValue);
                itm.SubItems.Add(conInfo.HMIExist.ToString());

           }
        }


        private void btnADD_Click(object sender, EventArgs e)
        {
            HardWareType hwType = (HardWareType)(cmbHWAdd.SelectedIndex + 1);
            if (hwType == HardWareType.SERVER)
            { }
            else if (hwType == HardWareType.ACS)
            {
                ControllerForm.ACS_Add acsForm = new ControllerForm.ACS_Add();
                acsForm.ShowDialog();
            }
            else
            {
                ControllerForm.HW_Add hw_add = new ControllerForm.HW_Add(hwType);
                hw_add.ShowDialog();
            }
            lvList_Refresh();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (lvList.SelectedItems.Count > 0)
            {
                if (lvList.SelectedItems[0].Text == HardWareType.MEXA.ToString())
                {
                    Ethercat.sysInfo.MEXAModuleInfos.Clear();
                    Ethercat.chInfo.MEXA9000_Channels.Clear();
                    DaqData.sysVarInfo.MEXA9000_Variables.Clear();

                }
                else if (lvList.SelectedItems[0].Text == HardWareType.ECU.ToString())
                {
                    Ethercat.sysInfo.ECUModuleInfos.Clear();
                    Ethercat.chInfo.ECU_Channels.Clear();
                    DaqData.sysVarInfo.ECU_Variables.Clear();
                }
                else if (lvList.SelectedItems[0].Text == HardWareType.MODBUS.ToString())
                {
                    Ethercat.sysInfo.ModBusModuleInfos.Clear();
                    Ethercat.chInfo.ModBus_Channels.Clear();
                    DaqData.sysVarInfo.ModBus_Variables.Clear();
                }
                else if (lvList.SelectedItems[0].Text == HardWareType.BlowByMeter.ToString())
                {
                    Ethercat.sysInfo.BBModuleInfos.Clear();
                    Ethercat.chInfo.BlowByMeter_Channels.Clear();
                    DaqData.sysVarInfo.ModBus_Variables.Clear();
                }
                else if (lvList.SelectedItems[0].Text == HardWareType.FuelMeter.ToString())
                {
                    Ethercat.sysInfo.FMModuleInfos.Clear();
                    Ethercat.chInfo.FuelMeter_Channels.Clear();
                    DaqData.sysVarInfo.ModBus_Variables.Clear();
                }
                else if (lvList.SelectedItems[0].Text == HardWareType.SmokeMeter.ToString())
                {
                    Ethercat.sysInfo.SMModuleInfos.Clear();
                    Ethercat.chInfo.SmokeMeter_Channels.Clear();
                    DaqData.sysVarInfo.ModBus_Variables.Clear();
                }
                else if (lvList.SelectedItems[0].Text == HardWareType.OSIRIS.ToString())
                {
                    Ethercat.sysInfo.OsirisModuleInfos.Clear();
                    Ethercat.chInfo.Osiris_Channels.Clear();
                    DaqData.sysVarInfo.Osiris_Variables.Clear();
                }

                foreach (acsConnectionInfo ConnectionInfo in Ethercat.sysInfo.HardWareInfo)
                {
                    if (ConnectionInfo.HWType.ToString() == lvList.SelectedItems[0].Text)
                    {
                        Ethercat.sysInfo.HardWareInfo.Remove(ConnectionInfo);
                        break;
                    }
                }

                lvList.SelectedItems[0].Remove();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Ethercat.SaveSysInfo();
            DaqData.SaveSysInfo();

            MessageBox.Show("저장이 완료되었습니다.");
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void HWConfig_Paint(object sender, PaintEventArgs e)
        {
            //mainPanel을 그리기 표면으로 사용하기 위해 변수지정
            Graphics g = e.Graphics;
            DrawShadow(g, lvList);
        }

        public static void DrawShadow(Graphics g, Control pnlControl)
        {
            //우측상단
            g.DrawImage(Properties.Resources.topRight_Shadow, pnlControl.Right, pnlControl.Top, 5, 5);

            //우측, TextureBrush 사용
            TextureBrush tb = new TextureBrush(Properties.Resources.right_Shadow);
            tb.TranslateTransform(pnlControl.Right, 0); //이미지 반복 시작 위치를 설정합니다. (기본값은 (0,0) 입니다.)
            g.FillRectangle(tb, pnlControl.Right, pnlControl.Top + 5, 5, pnlControl.Height - 5);

            //우측하단
            g.DrawImage(Properties.Resources.bottomRight_Shadow, pnlControl.Right, pnlControl.Bottom, 5, 5);

            //하단
            tb = new TextureBrush(Properties.Resources.bottom_Shadow);
            tb.TranslateTransform(0, pnlControl.Bottom); //이미지 반복 시작 위치를 설정합니다. (기본값은 (0,0) 입니다.)
            g.FillRectangle(tb, pnlControl.Left + 5, pnlControl.Bottom, pnlControl.Width - 5, 5);

            //좌측 하단
            g.DrawImage(Properties.Resources.bottomLeft_Shadow, pnlControl.Left, pnlControl.Bottom, 5, 5);
        }

        private void lvList_DoubleClick(object sender, EventArgs e)
        {
            switch (lvList.SelectedIndices[0])
            {
                case 0:
                    ControllerForm.ACS_Add acsForm = new ControllerForm.ACS_Add(Ethercat.sysInfo.HardWareInfo[lvList.SelectedIndices[0]]);
                    acsForm.ShowDialog();
                    break;
                default:
                    ControllerForm.HW_Add hwForm = new ControllerForm.HW_Add(Ethercat.sysInfo.HardWareInfo[lvList.SelectedIndices[0]]);
                    hwForm.ShowDialog();
                    break;
            }
            lvList_Refresh();
        }
    }
}
