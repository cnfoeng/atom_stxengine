﻿namespace AtomUI.ControllerForm
{
    partial class HW_Add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.RemoteAddressTB = new System.Windows.Forms.TextBox();
            this.PortLb = new System.Windows.Forms.Label();
            this.tbSlave = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.lblFile = new System.Windows.Forms.Label();
            this.tbFilename = new System.Windows.Forms.TextBox();
            this.btnFileOpen = new System.Windows.Forms.Button();
            this.RemoteAddressCMB = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // tbDescription
            // 
            this.tbDescription.Location = new System.Drawing.Point(134, 32);
            this.tbDescription.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(150, 21);
            this.tbDescription.TabIndex = 66;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(22, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 30);
            this.label1.TabIndex = 65;
            this.label1.Text = "Description";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // RemoteAddressTB
            // 
            this.RemoteAddressTB.Location = new System.Drawing.Point(134, 77);
            this.RemoteAddressTB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.RemoteAddressTB.Name = "RemoteAddressTB";
            this.RemoteAddressTB.Size = new System.Drawing.Size(150, 21);
            this.RemoteAddressTB.TabIndex = 64;
            this.RemoteAddressTB.Visible = false;
            // 
            // PortLb
            // 
            this.PortLb.Location = new System.Drawing.Point(22, 71);
            this.PortLb.Name = "PortLb";
            this.PortLb.Size = new System.Drawing.Size(106, 30);
            this.PortLb.TabIndex = 63;
            this.PortLb.Text = "Remote Address";
            this.PortLb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbSlave
            // 
            this.tbSlave.Location = new System.Drawing.Point(134, 121);
            this.tbSlave.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbSlave.Name = "tbSlave";
            this.tbSlave.Size = new System.Drawing.Size(150, 21);
            this.tbSlave.TabIndex = 68;
            this.tbSlave.Visible = false;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(22, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 30);
            this.label2.TabIndex = 67;
            this.label2.Text = "Slave Identifier";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Visible = false;
            // 
            // CancelBtn
            // 
            this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CancelBtn.Location = new System.Drawing.Point(193, 218);
            this.CancelBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(91, 23);
            this.CancelBtn.TabIndex = 70;
            this.CancelBtn.Text = "Cancel";
            // 
            // SaveBtn
            // 
            this.SaveBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.SaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveBtn.Location = new System.Drawing.Point(24, 218);
            this.SaveBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(91, 23);
            this.SaveBtn.TabIndex = 69;
            this.SaveBtn.Text = "Save";
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // lblFile
            // 
            this.lblFile.Location = new System.Drawing.Point(22, 161);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(94, 30);
            this.lblFile.TabIndex = 71;
            this.lblFile.Text = "Channel Definition File";
            this.lblFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFile.Visible = false;
            this.lblFile.Click += new System.EventHandler(this.lblFile_Click);
            // 
            // tbFilename
            // 
            this.tbFilename.Location = new System.Drawing.Point(134, 170);
            this.tbFilename.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbFilename.Name = "tbFilename";
            this.tbFilename.Size = new System.Drawing.Size(150, 21);
            this.tbFilename.TabIndex = 72;
            this.tbFilename.Visible = false;
            this.tbFilename.TextChanged += new System.EventHandler(this.tbFilename_TextChanged);
            // 
            // btnFileOpen
            // 
            this.btnFileOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFileOpen.Location = new System.Drawing.Point(290, 168);
            this.btnFileOpen.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnFileOpen.Name = "btnFileOpen";
            this.btnFileOpen.Size = new System.Drawing.Size(43, 23);
            this.btnFileOpen.TabIndex = 73;
            this.btnFileOpen.Text = "...";
            this.btnFileOpen.Visible = false;
            this.btnFileOpen.Click += new System.EventHandler(this.btnFileOpen_Click);
            // 
            // RemoteAddressCMB
            // 
            this.RemoteAddressCMB.FormattingEnabled = true;
            this.RemoteAddressCMB.Items.AddRange(new object[] {
            "PCAN_USB:FD 1 (51h)",
            "PCAN_PCI 1 (41h)"});
            this.RemoteAddressCMB.Location = new System.Drawing.Point(134, 77);
            this.RemoteAddressCMB.Name = "RemoteAddressCMB";
            this.RemoteAddressCMB.Size = new System.Drawing.Size(150, 20);
            this.RemoteAddressCMB.TabIndex = 74;
            this.RemoteAddressCMB.Visible = false;
            // 
            // HW_Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(345, 261);
            this.Controls.Add(this.RemoteAddressCMB);
            this.Controls.Add(this.btnFileOpen);
            this.Controls.Add(this.tbFilename);
            this.Controls.Add(this.lblFile);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.tbSlave);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbDescription);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RemoteAddressTB);
            this.Controls.Add(this.PortLb);
            this.Name = "HW_Add";
            this.Text = "HW_Add";
            this.Load += new System.EventHandler(this.HW_Add_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbDescription;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox RemoteAddressTB;
        private System.Windows.Forms.Label PortLb;
        private System.Windows.Forms.TextBox tbSlave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Label lblFile;
        private System.Windows.Forms.TextBox tbFilename;
        private System.Windows.Forms.Button btnFileOpen;
        private System.Windows.Forms.ComboBox RemoteAddressCMB;
    }
}