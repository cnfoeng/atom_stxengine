﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;
using System.IO.Ports;

namespace AtomUI.ControllerForm
{
    public partial class HW_Add : Form
    {
        public acsConnectionInfo ConnectionInfo = null;
        public HardWareType HWType = HardWareType.ECU;
        public HW_Add(HardWareType HWType)
        {
            InitializeComponent();
            this.HWType = HWType;
        }
        public HW_Add(acsConnectionInfo ConnectionInfo)
        {
            InitializeComponent();
            this.ConnectionInfo = ConnectionInfo;
            this.HWType = ConnectionInfo.HWType;
        }

        private void HW_Add_Load(object sender, EventArgs e)
        {
            string[] PortNames = SerialPort.GetPortNames();
            if (ConnectionInfo == null)
            {

                switch (HWType)
                {
                    case HardWareType.ECU:
                        this.Text = "ECU 추가";
                        RemoteAddressCMB.SelectedIndex = 0;
                        RemoteAddressCMB.Visible = true;
                        //RemoteAddressTB.Text = "PCAN_USB:FD 1(51h)";
                        tbDescription.Text = "ECU Data Module";
                        lblFile.Visible = true;
                        tbFilename.Visible = true;
                        btnFileOpen.Visible = true;
                        break;
                    case HardWareType.MEXA:
                        RemoteAddressTB.Text = "GPIB3::3::INSTR";
                        RemoteAddressTB.Visible = true;
                        this.Text = "MEXA 9X00 추가";
                        tbDescription.Text = "MEXA 9X00";
                        lblFile.Visible = true;
                        tbFilename.Visible = true;
                        btnFileOpen.Visible = true;
                        break;
                    case HardWareType.MEXA_SERIAL:
                        PortLb.Text = "Serial Port";
                        RemoteAddressCMB.BeginUpdate();
                        RemoteAddressCMB.Items.Clear();
                        foreach (string comport in SerialPort.GetPortNames())
                        {
                            RemoteAddressCMB.Items.Add(comport);
                        }
                        RemoteAddressCMB.EndUpdate();
                        if (SerialPort.GetPortNames().Length != 0)
                        {
                            RemoteAddressCMB.SelectedIndex = 0;
                        }
                        RemoteAddressCMB.Visible = true;
                        label2.Text = "Controller Address";
                        label2.Visible = true;
                        tbSlave.Text = "13";
                        tbSlave.Visible = true;
                        this.Text = "MEXA 9X00 추가";
                        tbDescription.Text = "MEXA 9X00";
                        lblFile.Visible = true;
                        tbFilename.Visible = true;
                        btnFileOpen.Visible = true;
                        break;
                    case HardWareType.MODBUS:
                        RemoteAddressTB.Text = "10.10.0.1";
                        this.Text = "MODBUS DAQ 추가";
                        tbSlave.Visible = true;
                        label2.Visible = true;
                        tbDescription.Text = "MODBUS DAQ";
                        lblFile.Visible = true;
                        tbFilename.Visible = true;
                        btnFileOpen.Visible = true;
                        break;
                    case HardWareType.SmokeMeter:
                        this.Text = "SMOKE METER 추가";
                        RemoteAddressCMB.Items.Clear();
                        for (int i = 0; i < PortNames.Length; i++)
                            RemoteAddressCMB.Items.Add(PortNames[i]);

                        if (RemoteAddressCMB.Items.Count > 0)
                            RemoteAddressCMB.SelectedIndex = 0;
                        else
                            SaveBtn.Enabled = false;
                        RemoteAddressCMB.Visible = true;
                        tbDescription.Text = "SMOKE METER AVL 415S";
                        break;
                    case HardWareType.BlowByMeter:
                        this.Text = "BLOWBY METER 추가";
                        RemoteAddressCMB.Items.Clear();
                        for (int i = 0; i < PortNames.Length; i++)
                            RemoteAddressCMB.Items.Add(PortNames[i]);

                        if (RemoteAddressCMB.Items.Count > 0)
                            RemoteAddressCMB.SelectedIndex = 0;
                        else
                            SaveBtn.Enabled = false;
                        RemoteAddressCMB.Visible = true;
                        tbDescription.Text = "BLOWBY METER";
                        break;
                    case HardWareType.FuelMeter:
                        this.Text = "FUEL METER 추가";
                        RemoteAddressCMB.Items.Clear();
                        for (int i = 0; i < PortNames.Length; i++)
                            RemoteAddressCMB.Items.Add(PortNames[i]);
                        RemoteAddressCMB.Items.Add("COM7");
                        if (RemoteAddressCMB.Items.Count > 0)
                            RemoteAddressCMB.SelectedIndex = 0;
                        else
                            SaveBtn.Enabled = false;
                        RemoteAddressCMB.Visible = true;
                        tbDescription.Text = "FUEL METER AVL 733S";
                        break;
                    case HardWareType.OSIRIS:
                        this.Text = "OSIRIS 추가";
                        RemoteAddressTB.Text = "192.168.2.20";
                        RemoteAddressTB.Visible = true;
                        label2.Text = "Port";
                        tbSlave.Text = "22222";
                        tbDescription.Text = "OSIRIS AK Protocol with TCP";
                        label2.Visible = true;
                        tbSlave.Visible = true;
                        lblFile.Visible = true;
                        tbFilename.Visible = true;
                        btnFileOpen.Visible = true;
                        break;

                }
            }
            else
            {
                switch (HWType)
                {
                    case HardWareType.ECU:
                        this.Text = "ECU 수정";
                        if (ConnectionInfo.RemoteAddress == RemoteAddressCMB.Items[0].ToString())
                            RemoteAddressCMB.SelectedIndex = 0;
                        else
                            RemoteAddressCMB.SelectedIndex = 1;

                        RemoteAddressCMB.Visible = true;
                        //RemoteAddressTB.Text = "PCAN_USB:FD 1(51h)";
                        tbDescription.Text = ConnectionInfo.Description;
                        break;
                    case HardWareType.MEXA:
                        RemoteAddressTB.Text = ConnectionInfo.RemoteAddress;
                        RemoteAddressTB.Visible = true;
                        this.Text = "MEXA 9X00 수정";
                        tbDescription.Text = ConnectionInfo.Description;
                        break;
                    case HardWareType.MEXA_SERIAL:
                        PortLb.Text = "Serial Port";
                        RemoteAddressCMB.BeginUpdate();
                        RemoteAddressCMB.Items.Clear();
                        for (int i = 0; i < PortNames.Length; i++)
                        {
                            RemoteAddressCMB.Items.Add(PortNames[i]);
                            if (ConnectionInfo.RemoteAddress == RemoteAddressCMB.Items[i].ToString())
                            {
                                RemoteAddressCMB.SelectedIndex = i;
                            }
                        }
                        if (RemoteAddressCMB.Items.Count > 0)
                        {
                            if (RemoteAddressCMB.SelectedItem == null)
                                RemoteAddressCMB.SelectedIndex = 0;
                        }
                        else
                        {
                            SaveBtn.Enabled = false;
                        }
                        RemoteAddressCMB.EndUpdate();
                        RemoteAddressCMB.Visible = true;
                        label2.Text = "Controller Address";
                        label2.Visible = true;
                        tbSlave.Text = ConnectionInfo.SlaveUnitIdentifier;
                        tbSlave.Visible = true;
                        this.Text = "MEXA 9X00 추가";
                        tbDescription.Text = "MEXA 9X00";
                        lblFile.Visible = true;
                        tbFilename.Visible = true;
                        btnFileOpen.Visible = true;
                        break;
                    case HardWareType.MODBUS:
                        RemoteAddressTB.Text = ConnectionInfo.RemoteAddress;
                        this.Text = "MODBUS DAQ 수정";
                        tbSlave.Visible = true;
                        label2.Visible = true;
                        tbDescription.Text = ConnectionInfo.Description;
                        break;
                    case HardWareType.SmokeMeter:
                        this.Text = "SMOKE METER 수정";
                        RemoteAddressCMB.Items.Clear();
                        for (int i = 0; i < PortNames.Length; i++)
                        {
                            RemoteAddressCMB.Items.Add(PortNames[i]);
                            if (ConnectionInfo.RemoteAddress == RemoteAddressCMB.Items[i].ToString())
                            {
                                RemoteAddressCMB.SelectedIndex = i;
                            }
                        }
                        if (RemoteAddressCMB.Items.Count > 0)
                        {
                            if (RemoteAddressCMB.SelectedItem == null)
                                RemoteAddressCMB.SelectedIndex = 0;
                        }
                        else
                        {
                            SaveBtn.Enabled = false;
                        }
                        RemoteAddressCMB.Visible = true;
                        tbDescription.Text = ConnectionInfo.Description;
                        break;
                    case HardWareType.BlowByMeter:
                        this.Text = "BLOWBY METER 수정";
                        RemoteAddressCMB.Items.Clear();
                        for (int i = 0; i < PortNames.Length; i++)
                        {
                            RemoteAddressCMB.Items.Add(PortNames[i]);
                            if (ConnectionInfo.RemoteAddress == RemoteAddressCMB.Items[i].ToString())
                            {
                                RemoteAddressCMB.SelectedIndex = i;
                            }
                        }
                        if (RemoteAddressCMB.Items.Count > 0)
                        {
                            if (RemoteAddressCMB.SelectedItem == null)
                                RemoteAddressCMB.SelectedIndex = 0;
                        }
                        else
                        {
                            SaveBtn.Enabled = false;
                        }
                        RemoteAddressCMB.Visible = true;
                        tbDescription.Text = ConnectionInfo.Description;
                        break;
                    case HardWareType.FuelMeter:
                        this.Text = "FUEL METER 수정";
                        RemoteAddressCMB.Items.Clear();
                        for (int i = 0; i < PortNames.Length; i++)
                        {
                            RemoteAddressCMB.Items.Add(PortNames[i]);
                            if (ConnectionInfo.RemoteAddress == RemoteAddressCMB.Items[i].ToString())
                            {
                                RemoteAddressCMB.SelectedIndex = i;
                            }
                        }
                        if (RemoteAddressCMB.Items.Count > 0)
                        {
                            if (RemoteAddressCMB.SelectedItem == null)
                                RemoteAddressCMB.SelectedIndex = 0;
                        }
                        else
                        {
                            SaveBtn.Enabled = false;
                        }
                        RemoteAddressCMB.Visible = true;
                        tbDescription.Text = ConnectionInfo.Description;
                        break;
                    case HardWareType.OSIRIS:
                        this.Text = "OSIRIS 수정";
                        RemoteAddressTB.Text = ConnectionInfo.RemoteAddress;
                        tbDescription.Text = ConnectionInfo.Description;

                        this.Text = "OSIRIS 수정";
                        RemoteAddressTB.Text = ConnectionInfo.RemoteAddress;
                        tbDescription.Text = ConnectionInfo.Description;
                        RemoteAddressTB.Visible = true;
                        label2.Text = "Port";
                        tbSlave.Text = ConnectionInfo.CommPort.ToString();
                        tbDescription.Text = "OSIRIS AK Protocol with TCP";
                        label2.Visible = true;
                        tbSlave.Visible = true;
                        lblFile.Visible = true;
                        tbFilename.Visible = true;
                        btnFileOpen.Visible = true;

                        break;
                }
            }
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            bool Error = false;

            if (ConnectionInfo == null)
            {
                foreach (acsConnectionInfo info in Ethercat.sysInfo.HardWareInfo)
                {
                    if ((info.HWType == HWType) && (((info.RemoteAddress == RemoteAddressTB.Text) && (info.SlaveUnitIdentifier == tbSlave.Text))))
                    {
                        Error = true;
                        break;
                    }
                }

                if (Error)
                {
                    MessageBox.Show("동일한 하드웨어가 이미 추가되어 있습니다.");
                }
                else

                {
                    acsConnectionInfo ConnectionInfo2 = new acsConnectionInfo();
                    ConnectionInfo2.HWType = HWType;
                    ConnectionInfo2.Description = tbDescription.Text;

                    switch (HWType)
                    {
                        case HardWareType.ECU:
                            if (tbFilename.Text == "")
                            {
                                MessageBox.Show("dbf 파일을 지정하셔야 합니다.");
                                Error = true;
                            }
                            else
                            {
                                ConnectionInfo2.RemoteAddress = (string)RemoteAddressCMB.SelectedItem;
                                DBFParsing.AddDBFChannels(tbFilename.Text);
                            }
                            break;
                        case HardWareType.MEXA:
                            if (tbFilename.Text == "")
                            {
                                if (DialogResult.Yes == MessageBox.Show("기본 정의를 사용하여 채널을 생성하시겠습니까?", "정의파일 미선택", MessageBoxButtons.YesNo))
                                {
                                    ConnectionInfo2.RemoteAddress = RemoteAddressTB.Text;
                                    MexaParsing.AddMexaChannels();
                                }
                                else
                                {
                                    Error = true;
                                }
                            }
                            else
                            {
                                ConnectionInfo2.RemoteAddress = RemoteAddressTB.Text;
                                MexaParsing.AddMexaChannels(tbFilename.Text);
                            }
                            break;
                        case HardWareType.MEXA_SERIAL:
                            if (tbFilename.Text == "")
                            {
                                if (DialogResult.Yes == MessageBox.Show("기본 정의를 사용하여 채널을 생성하시겠습니까?", "정의파일 미선택", MessageBoxButtons.YesNo))
                                {
                                    ConnectionInfo2.SlaveUnitIdentifier = tbSlave.Text;
                                    ConnectionInfo2.RemoteAddress = (string)RemoteAddressCMB.SelectedItem;
                                    MexaParsing.AddMexaChannels();
                                }
                                else
                                {
                                    Error = true;
                                }
                            }
                            else
                            {
                                ConnectionInfo2.RemoteAddress = RemoteAddressTB.Text;
                                ConnectionInfo2.SlaveUnitIdentifier = tbSlave.Text;
                                MexaParsing.AddMexaChannels(tbFilename.Text);
                            }
                            break;
                        case HardWareType.MODBUS:
                            if (tbFilename.Text == "")
                            {
                                MessageBox.Show("채널정의 파일을 지정하셔야 합니다.");
                                Error = true;
                            }
                            else
                            {
                                ConnectionInfo2.RemoteAddress = RemoteAddressTB.Text;
                                ConnectionInfo2.SlaveUnitIdentifier = tbSlave.Text;
                                ModbusParsing.AddModbusChannels(tbFilename.Text);
                            }
                            ConnectionInfo2.SlaveUnitIdentifier = tbSlave.Text;
                            break;
                        case HardWareType.SmokeMeter:
                        case HardWareType.BlowByMeter:
                        case HardWareType.FuelMeter:
                            if (RemoteAddressCMB.Items.Count == 0 || RemoteAddressCMB.SelectedItem == null)
                            {
                                MessageBox.Show("시리얼포트가 없거나 지정하지 않았습니다.");
                                Error = true;
                            }
                            else
                            {
                                ConnectionInfo2.RemoteAddress = (string)RemoteAddressCMB.SelectedItem;
                                SerialParsing.AddChannels(HWType);
                            }
                            break;
                        case HardWareType.OSIRIS :
                            if (tbFilename.Text == "")
                            {
                                MessageBox.Show("채널정의 파일을 지정하셔야 합니다.");
                                Error = true;
                            }
                            else
                            {
                                ConnectionInfo2.RemoteAddress = RemoteAddressTB.Text;
                                ConnectionInfo2.CommPort = int.Parse(tbSlave.Text);
                                OsirisParsing.AddOsirisChannels(tbFilename.Text);
                            }
                            break;
                    }

                    Ethercat.sysInfo.HardWareInfo.Add(ConnectionInfo2);
                }
            }
            else
            {
                ConnectionInfo.Description = tbDescription.Text;

                switch (HWType)
                {
                    case HardWareType.ECU:
                        ConnectionInfo.RemoteAddress = (string)RemoteAddressCMB.SelectedItem;
                        break;
                    case HardWareType.MEXA:
                        ConnectionInfo.RemoteAddress = RemoteAddressTB.Text;
                        break;
                    case HardWareType.MEXA_SERIAL:
                        ConnectionInfo.RemoteAddress = (string)RemoteAddressCMB.SelectedItem;
                        ConnectionInfo.SlaveUnitIdentifier = tbSlave.Text;
                        break;
                    case HardWareType.MODBUS:
                        ConnectionInfo.RemoteAddress = RemoteAddressTB.Text;
                        ConnectionInfo.SlaveUnitIdentifier = tbSlave.Text;
                        break;
                    case HardWareType.SmokeMeter:
                    case HardWareType.BlowByMeter:
                    case HardWareType.FuelMeter:
                        if (RemoteAddressCMB.Items.Count == 0 || RemoteAddressCMB.SelectedItem == null)
                        {
                            MessageBox.Show("시리얼포트가 없거나 지정하지 않았습니다.");
                            Error = true;
                        }
                        else
                        {
                            ConnectionInfo.RemoteAddress = (string)RemoteAddressCMB.SelectedItem;
                        }
                        break;
                    case HardWareType.OSIRIS :
                        ConnectionInfo.RemoteAddress = RemoteAddressTB.Text;
                        ConnectionInfo.CommPort = int.Parse(tbSlave.Text);
                        break;
                }

                this.Close();

            }
        }
    

        private void btnFileOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            switch (HWType)
            {
                case HardWareType.ECU:
                    open.Filter = "dbf File Format|*.dbf";
                    open.Title = "Load an INCA dbf File";
                    break;
                case HardWareType.MEXA:
                    open.Filter = "CSV File Format|*.csv";
                    open.Title = "Load an Horiba Channel Define File";
                    break;
                case HardWareType.MODBUS:
                    open.Filter = "CSV File Format|*.csv";
                    open.Title = "Load an MODBUS Channel Define File";
                    break;
                case HardWareType.OSIRIS:
                    open.Filter = "CSV File Format|*.csv";
                    open.Title = "Load an OSIRIS Channel Define File";
                    break;
            }
            open.InitialDirectory = Ethercat.sysInfo.sysSettings.DefaultDirectory;
            open.FileName = "";
            if (DialogResult.OK == open.ShowDialog() && open.FileName != "")
            {
                tbFilename.Text = open.FileName.ToString();
            }
        }

        private void tbFilename_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblFile_Click(object sender, EventArgs e)
        {

        }
    }
}
