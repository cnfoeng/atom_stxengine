﻿namespace AtomUI.ControllerForm
{
    partial class ModuleInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            this.lvList = new System.Windows.Forms.ListView();
            this.Header1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnSave = new System.Windows.Forms.Button();
            this.Header3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lvList
            // 
            this.lvList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lvList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Header8,
            this.Header1,
            this.Header2,
            this.Header3,
            this.Header4,
            this.Header5,
            this.Header6,
            this.Header10,
            this.Header7,
            this.Header9,
            this.Header11});
            this.lvList.FullRowSelect = true;
            this.lvList.GridLines = true;
            listViewGroup1.Header = "ListViewGroup";
            listViewGroup1.Name = "Global Variables";
            listViewGroup2.Header = "ListViewGroup";
            listViewGroup2.Name = "Local Variables";
            this.lvList.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2});
            this.lvList.Location = new System.Drawing.Point(12, 12);
            this.lvList.MultiSelect = false;
            this.lvList.Name = "lvList";
            this.lvList.ShowGroups = false;
            this.lvList.Size = new System.Drawing.Size(1337, 524);
            this.lvList.TabIndex = 77;
            this.lvList.UseCompatibleStateImageBehavior = false;
            this.lvList.View = System.Windows.Forms.View.Details;
            // 
            // Header1
            // 
            this.Header1.Text = "Name";
            this.Header1.Width = 73;
            // 
            // Header2
            // 
            this.Header2.Text = "Desc";
            this.Header2.Width = 252;
            // 
            // btnSave
            // 
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(542, 558);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(141, 50);
            this.btnSave.TabIndex = 76;
            this.btnSave.Text = "OK";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // Header3
            // 
            this.Header3.Text = "VenderID";
            this.Header3.Width = 75;
            // 
            // Header4
            // 
            this.Header4.Text = "ProductID";
            this.Header4.Width = 99;
            // 
            // Header5
            // 
            this.Header5.Text = "IO";
            this.Header5.Width = 63;
            // 
            // Header6
            // 
            this.Header6.Text = "Type";
            this.Header6.Width = 70;
            // 
            // Header7
            // 
            this.Header7.Text = "OffsetBase";
            this.Header7.Width = 88;
            // 
            // Header8
            // 
            this.Header8.Text = "SlaveNo";
            this.Header8.Width = 69;
            // 
            // Header9
            // 
            this.Header9.Text = "ChannelEA";
            this.Header9.Width = 95;
            // 
            // Header10
            // 
            this.Header10.Text = "USE";
            this.Header10.Width = 64;
            // 
            // Header11
            // 
            this.Header11.Text = "Variables";
            this.Header11.Width = 229;
            // 
            // ModuleInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1361, 620);
            this.Controls.Add(this.lvList);
            this.Controls.Add(this.btnSave);
            this.Name = "ModuleInfo";
            this.Text = "ModuleInfo";
            this.Load += new System.EventHandler(this.ModuleInfo_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvList;
        private System.Windows.Forms.ColumnHeader Header1;
        private System.Windows.Forms.ColumnHeader Header2;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ColumnHeader Header3;
        private System.Windows.Forms.ColumnHeader Header4;
        private System.Windows.Forms.ColumnHeader Header5;
        private System.Windows.Forms.ColumnHeader Header6;
        private System.Windows.Forms.ColumnHeader Header7;
        private System.Windows.Forms.ColumnHeader Header8;
        private System.Windows.Forms.ColumnHeader Header9;
        private System.Windows.Forms.ColumnHeader Header10;
        private System.Windows.Forms.ColumnHeader Header11;
    }
}