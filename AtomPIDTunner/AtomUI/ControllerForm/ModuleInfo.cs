﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;

namespace AtomUI.ControllerForm
{
    public partial class ModuleInfo : Form
    {
        public ModuleInfo()
        {
            InitializeComponent();
        }

        private void ModuleInfo_Load(object sender, EventArgs e)
        {
            foreach (acsModuleInfo Module in Ethercat.sysInfo.HMIModuleInfos)
            {
                lvItemADD(lvList, Module);
            }
            foreach (acsModuleInfo Module in Ethercat.sysInfo.ModuleInfos)
            {
                lvItemADD(lvList, Module);
            }
            foreach (acsModuleInfo Module in Ethercat.sysInfo.MEXAModuleInfos)
            {
                lvItemADD(lvList, Module);
            }
            foreach (acsModuleInfo Module in Ethercat.sysInfo.ECUModuleInfos)
            {
                lvItemADD(lvList, Module);
            }
            foreach (acsModuleInfo Module in Ethercat.sysInfo.ModBusModuleInfos)
            {
                lvItemADD(lvList, Module);
            }
        }

        private void lvItemADD(ListView lvList, acsModuleInfo Module)
        {
            ListViewItem itm = lvList.Items.Add(Module.SlaveNo.ToString());
            itm.SubItems.Add(Module.ProductName);
            itm.SubItems.Add(Module.ProductDesc);
            itm.SubItems.Add(String.Format("0x{0:X}", Module.VendorID));
            itm.SubItems.Add(String.Format("0x{0:X}", Module.ProductID));

            itm.SubItems.Add(Module.ChannelIO.ToString());
            itm.SubItems.Add(Module.ChannelType.ToString());
            itm.SubItems.Add(Module.UseFor.ToString());

            //itm.SubItems.Add(Ethercat.GetIOString(Module.ChannelIO));
            //itm.SubItems.Add(Ethercat.GetTypeString(Module.ChannelType));
            //itm.SubItems.Add(Ethercat.GetUseForString(Module.UseFor));
            itm.SubItems.Add(Module.OffsetBase.ToString());
            itm.SubItems.Add(Module.ChannelEa.ToString());
            itm.SubItems.Add(Ethercat.GetVarString(Module.Variables));
        }
    }
}
