﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;
using System.IO;

namespace AtomUI
{
    public partial class SystemSetup : Form
    {
        public SysInfo sysInfo = new SysInfo();
        public SysChannelInfo chInfo = new SysChannelInfo();

        public SystemSetup()
        {
            InitializeComponent();
        }

        private void SystemSetup_Load(object sender, EventArgs e)
        {
            sysInfo.ConnectionInfo = Ethercat.sysInfo.ConnectionInfo;
            Ethercat.makeSystemInfo(sysInfo, chInfo);

            tbController.Text = Ethercat.SerialNumber;
            FillTreeView();

            string[] Files = Directory.GetFiles(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SysInfoDir);
            foreach (string file in Files)
            {
                string filename = file.Substring(file.LastIndexOf('\\')+1, file.Length- file.LastIndexOf('\\')-1);
                string filenameonly = filename.Substring(0, filename.LastIndexOf('.'));
                lbFileList.Items.Add(filenameonly);
                if (Ethercat.sysInfo.sysSettings.SysInfoFileName == file)
                {
                    lbFileList.SelectedValue = filename;
                }
            }
            tbFileName.Text = Ethercat.sysInfo.sysSettings.SysInfoFileName.Substring(0, Ethercat.sysInfo.sysSettings.SysInfoFileName.LastIndexOf('.'));
        }

        private void FillTreeView()
        {
            TreeNode tn = tvSystemInfo.Nodes.Add("Controller : " + Ethercat.SerialNumber);

            TreeNode tn_hmi_module = tn.Nodes.Add("Ethercat HMI Slaves");
            Ethercat.MakeModuleNode(tn_hmi_module, sysInfo.HMIModuleInfos);

            TreeNode tn_daq_module = tn.Nodes.Add("Ethercat DAQ Slaves");
            Ethercat.MakeModuleNode(tn_daq_module, sysInfo.ModuleInfos);

            TreeNode tn_hmi = tn.Nodes.Add("HMI Channels");
            Ethercat.MakeChannelNode(tn_hmi, chInfo.HMI_Channels);

            TreeNode tn_DI = tn.Nodes.Add("Digital Input Channels");
            Ethercat.MakeChannelNode(tn_DI, chInfo.DI_Channels);

            TreeNode tn_AI = tn.Nodes.Add("Alanog Input Channels");
            Ethercat.MakeChannelNode(tn_AI, chInfo.AI_Channels);

            TreeNode tn_DO = tn.Nodes.Add("Digital Output Channels");
            Ethercat.MakeChannelNode(tn_DO, chInfo.DO_Channels);

            TreeNode tn_AO = tn.Nodes.Add("Alanog Output Channels");
            Ethercat.MakeChannelNode(tn_AO, chInfo.AO_Channels);

            tn_AO.EnsureVisible();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Ethercat.sysInfo.sysSettings.SysInfoFileName = tbFileName.Text + ".xml";
            Ethercat.sysInfo.sysSettings.ChannelInfoFileName = tbFileName.Text + ".xml";

            sysInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SysInfoDir + "\\" + Ethercat.sysInfo.sysSettings.SysInfoFileName);
            chInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.ChannelInfoDir + "\\" + Ethercat.sysInfo.sysSettings.ChannelInfoFileName);
        }

        private void lbFileList_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbFileName.Text = lbFileList.SelectedItem.ToString();
        }
    }
}
