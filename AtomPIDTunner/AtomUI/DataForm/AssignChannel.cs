﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;

namespace AtomUI.DataForm
{
    public partial class AssignChannel : Form
    {
        public SysVarInfo sysVarInfo = null;
        private SysChannelInfo sysChannelInfo = Ethercat.chInfo;

        public int VariablesIndex;
        public int VarIndex;
        List<acsChannelInfo> Channels = null;
        List<VariableInfo> Variables = null;
        VariableInfo Var = null;

        public AssignChannel()
        {
            InitializeComponent();
        }

        private void btnChannel_Click(object sender, EventArgs e)
        {
            int Ch_Index = lvChannel.SelectedIndices[0];
            foreach (VariableInfo var in Variables)
            {
                if (var.ChannelInfo != null && var.ChannelInfo.ChannelIndex == Ch_Index)
                    var.ChannelInfo = null;
            }
            Var.ChannelInfo = Channels[Ch_Index];
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            Var.ChannelInfo = null;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

        }

        private void AssignChannel_Load(object sender, EventArgs e)
        {
            MakeVarListView();
            MakeChannelListView();
        }

        private void MakeVarListView()
        {
            string pre = getPredefineLabel(VariablesIndex);
            Variables = getVariables(VariablesIndex);
            Var = Variables[VarIndex];

            ListViewItem itm = lvList.Items.Add(Var.VariableName);
            itm.SubItems.Add(Var.NorminalName);
            itm.SubItems.Add(Var.NorminalDescription);
            itm.SubItems.Add(Var.Tag.ToString());
            itm.SubItems.Add(Var.Index.ToString());
            itm.SubItems.Add(Var.Removable.ToString());
            if (pre != "" && Var.ChannelInfo != null)
                itm.SubItems.Add(pre + " CH" + Var.ChannelInfo.ChannelIndex.ToString());
            else if (Var.Formula != "")
                itm.SubItems.Add(Var.Formula);
        }

        private void MakeChannelListView()
        {
            string USE = "";

            switch (VariablesIndex)
            {
                case 0:
                    Channels = Ethercat.chInfo.HMI_Channels;
                    USE = "HMI";
                    break;
                case 2:
                    Channels = Ethercat.chInfo.DI_Channels;
                    USE = "DI";
                    break;
                case 3:
                    Channels = Ethercat.chInfo.AI_Channels;
                    USE = "AI";
                    break;
                case 4:
                    Channels = Ethercat.chInfo.DO_Channels;
                    USE = "DO";
                    break;
                case 5:
                    Channels = Ethercat.chInfo.AO_Channels;
                    USE = "AO";
                    break;
                case 6:
                    Channels = Ethercat.chInfo.MEXA9000_Channels;
                    USE = "MEXA9000";
                    break;
                case 7:
                    Channels = Ethercat.chInfo.ECU_Channels;
                    USE = "ECU";
                    break;
                case 8:
                    Channels = Ethercat.chInfo.ModBus_Channels;
                    USE = "ModBus";
                    break;
            }

            foreach (acsChannelInfo Channel in Channels)
            {
                ListViewItem itm = lvChannel.Items.Add(USE);
                itm.SubItems.Add(Channel.ChannelIndex.ToString());
                itm.SubItems.Add(Channel.VariableName);
                itm.SubItems.Add(Channel.ModuleInfo.ProductName);
                itm.SubItems.Add(Channel.ModuleInfo.SlaveNo.ToString());

                itm.SubItems.Add(Ethercat.GetIOString(Channel.ModuleInfo.ChannelIO));
                itm.SubItems.Add(Ethercat.GetTypeString(Channel.ModuleInfo.ChannelType));
                itm.SubItems.Add(Channel.Offset.ToString());
                itm.SubItems.Add(Channel.ChannelBit.ToString());
                foreach(VariableInfo var in Variables)
                {
                    if (var.ChannelInfo!=null && var.ChannelInfo.ChannelIndex == Channel.ChannelIndex)
                        itm.SubItems.Add(var.VariableName);
                }
                if (Var!=null && Var.ChannelInfo!=null)
                {
                    if (Var.ChannelInfo.ChannelIndex == Channel.ChannelIndex)
                    {
                        lvChannel.SelectedIndices.Add(lvChannel.Items.Count - 1);
                    }
                }
            }
        }

        private List<VariableInfo> getVariables(int Index)
        {
            List<VariableInfo> Variables = null;

            switch (Index)
            {
                case 0:
                    Variables = sysVarInfo.HMI_IOVariables;
                    break;
                case 1:
                    Variables = sysVarInfo.HMI_Variables;
                    break;
                case 2:
                    Variables = sysVarInfo.DI_Variables;
                    break;
                case 3:
                    Variables = sysVarInfo.AI_Variables;
                    break;
                case 4:
                    Variables = sysVarInfo.DO_Variables;
                    break;
                case 5:
                    Variables = sysVarInfo.AO_Variables;
                    break;
                case 6:
                    Variables = sysVarInfo.MEXA9000_Variables;
                    break;
                case 7:
                    Variables = sysVarInfo.ECU_Variables;
                    break;
                case 8:
                    Variables = sysVarInfo.MEXA9000_Variables;
                    break;
                case 9:
                    Variables = sysVarInfo.Calc_Variables;
                    break;
            }

            return Variables;

        }

        private string getPredefineLabel(int Index)
        {
            string pre = "";

            switch (Index)
            {
                case 0:
                    pre = "HMI";
                    break;
                case 1:
                    break;
                case 2:
                    pre = "DI";
                    break;
                case 3:
                    pre = "AI";
                    break;
                case 4:
                    pre = "DO";
                    break;
                case 5:
                    pre = "AO";
                    break;
                case 6:
                    pre = "MEXA9000";
                    break;
                case 7:
                    pre = "ECU";
                    break;
                case 8:
                    pre = "ModBus";
                    break;
                case 9:
                    pre = "Calc";
                    break;
            }

            return pre;

        }
    }
}
