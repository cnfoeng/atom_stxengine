﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;

namespace AtomUI.DataForm
{
    public partial class CalcRefVariableDefine : Form
    {
        public CalcRefVariableDefine()
        {
            InitializeComponent();
        }

        private void MakeListView()
        {
            string pre = ((VariableType)cmbVariableDefine.SelectedIndex).ToString();
            List<VariableInfo> Variables = DaqData.sysVarInfo.getVariables(cmbVariableDefine.SelectedIndex);
            Reindex(Variables);

            lvList.Items.Clear();

            foreach (VariableInfo Var in Variables)
            {
                ListViewItem itm = lvList.Items.Add(Var.VariableName);
                if (pre != "" && Var.ChannelInfo != null)
                    itm.SubItems.Add(pre + " CH" + Var.ChannelInfo.ChannelIndex.ToString());
                else if (Var.Formula != "")
                    itm.SubItems.Add(Var.Formula);
                itm.SubItems.Add(Var.NorminalName);
                itm.SubItems.Add(Var.NorminalDescription);
            }
        }
        private void MakeRefListView()
        {
            List<VariableInfo> Variables = DaqData.sysVarInfo.CalcRef_Variables;

            lvAssigned.Items.Clear();

            foreach (VariableInfo Var in Variables)
            {
                string pre = DaqData.sysVarInfo.getPredefineLabel(Var);

                ListViewItem itm = lvAssigned.Items.Add(Var.abbreviation);
                itm.SubItems.Add(Var.VariableName);
                if (pre != "" && Var.ChannelInfo != null)
                    itm.SubItems.Add(pre + " CH" + Var.ChannelInfo.ChannelIndex.ToString());
                else if (Var.Formula != "")
                    itm.SubItems.Add(Var.Formula);
                itm.SubItems.Add(Var.NorminalName);
                itm.SubItems.Add(Var.NorminalDescription);
            }
        }

        private void cmbVariableDefine_SelectedIndexChanged(object sender, EventArgs e)
        {
            MakeListView();
            MakeRefListView();
        }

        private void CalcRefVariableDefine_Load(object sender, EventArgs e)
        {
            cmbVariableDefine.Items.Clear();
            foreach (string varType in DaqData.getVarTypeList())
            {
                if (varType != VariableType.Calculation.ToString())
                    cmbVariableDefine.Items.Add(varType);
            }

            cmbVariableDefine.SelectedIndex = 3;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (lvList.SelectedItems.Count == 0)
            {
                return;
            }

            if (tbAlias.Text == "")
            {
                MessageBox.Show("변수에 지정할 약어를 입력하여 주십시오.");
                return;
            }
            VariableInfo RefVar = null;
            List<VariableInfo> Variables = DaqData.sysVarInfo.getVariables(cmbVariableDefine.SelectedIndex);

            foreach (VariableInfo var in DaqData.sysVarInfo.CalcRef_Variables)
            {
                if (var.abbreviation == tbAlias.Text)
                {
                    MessageBox.Show("동일한 약어가 이미 존재합니다.");
                    return;
                }
                if (var == Variables[lvList.SelectedIndices[0]])
                {
                    MessageBox.Show("동일한 변수가 이미 존재합니다.");
                    return;
                }
            }

            RefVar = Variables[lvList.SelectedIndices[0]];
            RefVar.abbreviation = tbAlias.Text;
            DaqData.sysVarInfo.CalcRef_Variables.Add(RefVar);

            MakeRefListView();
            DaqData.sysVarInfo.RenewDataTable();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (lvAssigned.SelectedItems.Count > 0)
            {
                List<VariableInfo> Variables = DaqData.sysVarInfo.CalcRef_Variables;
                Variables.RemoveAt(lvAssigned.SelectedIndices[0]);
                Reindex(Variables);
                MakeRefListView();
            }
            else
            {
                MessageBox.Show("Select Variable, First!", "Variable Define");
            }
            DaqData.sysVarInfo.RenewDataTable();

        }

        private void Reindex(List<VariableInfo> Variables)
        {
            for (int i = 0; i < Variables.Count; i++)
                Variables[i].Index = i;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DaqData.SaveSysInfo();
            this.Close();
        }
    }
}
