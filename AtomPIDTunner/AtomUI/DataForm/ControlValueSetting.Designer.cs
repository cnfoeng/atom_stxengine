﻿namespace AtomUI.DataForm
{
    partial class ControlValueSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbDynoTorque = new System.Windows.Forms.TextBox();
            this.tbDynoSpeed = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbEngineAlpha = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbEngineSpeed = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbEngineTorque = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Dyno Torque Range";
            // 
            // tbDynoTorque
            // 
            this.tbDynoTorque.Location = new System.Drawing.Point(207, 27);
            this.tbDynoTorque.Name = "tbDynoTorque";
            this.tbDynoTorque.Size = new System.Drawing.Size(100, 25);
            this.tbDynoTorque.TabIndex = 1;
            this.tbDynoTorque.Text = "4000";
            this.tbDynoTorque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbDynoSpeed
            // 
            this.tbDynoSpeed.Location = new System.Drawing.Point(207, 72);
            this.tbDynoSpeed.Name = "tbDynoSpeed";
            this.tbDynoSpeed.Size = new System.Drawing.Size(100, 25);
            this.tbDynoSpeed.TabIndex = 3;
            this.tbDynoSpeed.Text = "8000";
            this.tbDynoSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Dyno Speed Range";
            // 
            // tbEngineAlpha
            // 
            this.tbEngineAlpha.Location = new System.Drawing.Point(207, 135);
            this.tbEngineAlpha.Name = "tbEngineAlpha";
            this.tbEngineAlpha.Size = new System.Drawing.Size(100, 25);
            this.tbEngineAlpha.TabIndex = 5;
            this.tbEngineAlpha.Text = "100";
            this.tbEngineAlpha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Engine Alpha Range";
            // 
            // tbEngineSpeed
            // 
            this.tbEngineSpeed.Location = new System.Drawing.Point(207, 178);
            this.tbEngineSpeed.Name = "tbEngineSpeed";
            this.tbEngineSpeed.Size = new System.Drawing.Size(100, 25);
            this.tbEngineSpeed.TabIndex = 7;
            this.tbEngineSpeed.Text = "8000";
            this.tbEngineSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(41, 181);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(145, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "Engine Speed Range";
            // 
            // tbEngineTorque
            // 
            this.tbEngineTorque.Location = new System.Drawing.Point(207, 223);
            this.tbEngineTorque.Name = "tbEngineTorque";
            this.tbEngineTorque.Size = new System.Drawing.Size(100, 25);
            this.tbEngineTorque.TabIndex = 9;
            this.tbEngineTorque.Text = "4000";
            this.tbEngineTorque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(41, 226);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(148, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Engine Torque Range";
            // 
            // btnSave
            // 
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(57, 315);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(105, 23);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "SAVE";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(192, 315);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(105, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "CANCEL";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // ControlValueSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(360, 386);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tbEngineTorque);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbEngineSpeed);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbEngineAlpha);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbDynoSpeed);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbDynoTorque);
            this.Controls.Add(this.label1);
            this.Name = "ControlValueSetting";
            this.Text = "ACS Controller Setting";
            this.Load += new System.EventHandler(this.ControlValueSetting_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbDynoTorque;
        private System.Windows.Forms.TextBox tbDynoSpeed;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbEngineAlpha;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbEngineSpeed;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbEngineTorque;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
    }
}