﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using AtomUI.Class;
using System.Windows.Forms.DataVisualization.Charting;
using ACS.SPiiPlusNET;

namespace AtomUI.DataForm
{
    public partial class ControlValueSetting : Form
    {
        public ControlValueSetting()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            HMIVariableSetting();
            PCWriteSetting();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

        }

        private void ControlValueSetting_Load(object sender, EventArgs e)
        {
            tbDynoTorque.Text = Ethercat.inVariables.Dyno_Torque_Range.ToString();
            tbDynoSpeed.Text = Ethercat.inVariables.Dyno_Speed_Range.ToString();
            tbEngineAlpha.Text = Ethercat.inVariables.Engine_AlphaPosi_Range.ToString();
            tbEngineSpeed.Text = Ethercat.inVariables.Engine_Speed_Range.ToString();
            tbEngineTorque.Text = Ethercat.inVariables.Engine_Torque_Range.ToString();
        }

        private void PCWriteSetting()
        {
            int TotalCount = DaqData.sysVarInfo.DI_Variables.Count + DaqData.sysVarInfo.DO_Variables.Count
                                + DaqData.sysVarInfo.AI_Variables.Count + DaqData.sysVarInfo.AO_Variables.Count;
            string Template = File.ReadAllText(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.TemplateDir + "\\PCWrite.txt");
            string CMD = "";
            string DECLARE = "";
            int Count = 0;
            int OffsetBaseAddress = 512;

            foreach (VariableInfo var in DaqData.sysVarInfo.HMI_IOVariables)
            {
                if (var.Removable)
                {
                    CMD = string.Format("{0}\n\teCAT_InputData_SortingList(0)({1})=g_eCAT_HMI_DI_Data(0)({2}).({3});"
                        , CMD, Count, var.ChannelInfo.ChannelIndex, var.ChannelInfo.ChannelBit);
                }
                else
                {
                    CMD = string.Format("{0}\n\teCAT_InputData_SortingList(0)({1})={2};", CMD, Count, var.NorminalName);
                }
                var.IOIndex = OffsetBaseAddress + Count;
                Count++;
            }
            foreach (VariableInfo var in DaqData.sysVarInfo.DI_Variables)
            {
                if (var.Removable)
                {
                    CMD = string.Format("{0}\n\teCAT_InputData_SortingList(0)({1})=g_eCAT_HMI_DI_Data(0)({2}).({3});"
                        , CMD, Count, var.ChannelInfo.ChannelIndex, var.ChannelInfo.ChannelBit);
                }
                else
                {
                    CMD = string.Format("{0}\n\teCAT_InputData_SortingList(0)({1})={2};", CMD, Count, var.NorminalName);
                }
                var.IOIndex = OffsetBaseAddress + Count;
                Count++;
            }
            foreach (VariableInfo var in DaqData.sysVarInfo.AI_Variables)
            {
                if (var.Removable)
                {
                    CMD = string.Format("{0}\n\teCAT_InputData_SortingList(0)({1})=g_eCAT_HMI_AI_Data(0)({2});"
                        , CMD, Count, var.ChannelInfo.ChannelIndex);
                }
                else
                {
                    CMD = string.Format("{0}\n\teCAT_InputData_SortingList(0)({1})={2}*1000;", CMD, Count, var.NorminalName);
                }
                var.IOIndex = OffsetBaseAddress + Count;
                Count++;
            }
            foreach (VariableInfo var in DaqData.sysVarInfo.DO_Variables)
            {
                if (var.Removable)
                {
                    CMD = string.Format("{0}\n\teCAT_InputData_SortingList(0)({1})=g_eCAT_HMI_AI_Data(0)({2});"
                        , CMD, Count, var.ChannelInfo.ChannelIndex, var.ChannelInfo.ChannelBit);
                }
                else
                {
                    CMD = string.Format("{0}\n\teCAT_InputData_SortingList(0)({1})={2};", CMD, Count, var.NorminalName);
                }
                var.IOIndex = OffsetBaseAddress + Count;
                Count++;
            }
            foreach (VariableInfo var in DaqData.sysVarInfo.AO_Variables)
            {
                if (var.Removable)
                {
                    CMD = string.Format("{0}\n\teCAT_InputData_SortingList(0)({1})=g_eCAT_HMI_AI_Data(0)({2});"
                        , CMD, Count, var.ChannelInfo.ChannelIndex);
                }
                else
                {
                    CMD = string.Format("{0}\n\teCAT_InputData_SortingList(0)({1})={2};", CMD, Count, var.NorminalName);
                }
                var.IOIndex = OffsetBaseAddress + Count;
                Count++;
            }
            foreach (VariableInfo var in DaqData.sysVarInfo.Calc_Variables)
            {
                if (var.Removable)
                {
                    CMD = string.Format("{0}\n\teCAT_InputData_SortingList(0)({1})={2};", CMD, Count, var.Formula);
                }
                else
                {
                    CMD = string.Format("{0}\n\teCAT_InputData_SortingList(0)({1})={2}*1000;", CMD, Count, var.NorminalName);
                }
                var.IOIndex = OffsetBaseAddress + Count;
                Count++;
            }

            Template = Template.Replace("#0#", Count.ToString()).Replace("#1#", DECLARE).Replace("#2#", CMD);

            Ethercat.ExecuteCommand(ProgramBuffer.ACSC_BUFFER_7, 500, Template);

        }

        private void HMIVariableSetting()
        {
            bool Err = false;
            Ethercat.inVariables.Dyno_Torque_Range = int.Parse(tbDynoTorque.Text);
            Ethercat.inVariables.Dyno_Speed_Range = int.Parse(tbDynoSpeed.Text);
            Ethercat.inVariables.Engine_AlphaPosi_Range = int.Parse(tbEngineAlpha.Text);
            Ethercat.inVariables.Engine_Speed_Range = int.Parse(tbEngineSpeed.Text);
            Ethercat.inVariables.Engine_Torque_Range = int.Parse(tbEngineTorque.Text);
            string CMD = File.ReadAllText(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.TemplateDir + "\\RemoteControlValueSettingCommand.txt");

            CMD = CMD.Replace("#1#", tbDynoTorque.Text).Replace("#2#", tbDynoSpeed.Text).Replace("#3#", tbEngineAlpha.Text).Replace("#4#", tbEngineSpeed.Text).Replace("#5#", tbEngineTorque.Text);

            try
            {
                CMD = CMD.Replace("#6#", DaqData.FindVariable("g_AI_Feedback_DynoSpeed", DaqData.sysVarInfo.AI_Variables).ChannelInfo.ChannelIndex.ToString());
                CMD = CMD.Replace("#9#", DaqData.FindVariable("g_AI_Feedback_Alpha", DaqData.sysVarInfo.AI_Variables).ChannelInfo.ChannelIndex.ToString());
                CMD = CMD.Replace("#10#", DaqData.FindVariable("g_AO_DynoControlSignal", DaqData.sysVarInfo.AO_Variables).ChannelInfo.ChannelIndex.ToString());
                CMD = CMD.Replace("#11#", DaqData.FindVariable("g_AO_AlphaASignal", DaqData.sysVarInfo.AO_Variables).ChannelInfo.ChannelIndex.ToString());
                CMD = CMD.Replace("#12#", DaqData.FindVariable("g_AO_AlphaBSignal", DaqData.sysVarInfo.AO_Variables).ChannelInfo.ChannelIndex.ToString());
                CMD = CMD.Replace("#13#", ((int)(DaqData.FindVariable("g_DO_ThrottleIVSSignal", DaqData.sysVarInfo.DO_Variables).ChannelInfo.ChannelIndex / 8)).ToString());
                CMD = CMD.Replace("#14#", DaqData.FindVariable("g_DO_ThrottleIVSSignal", DaqData.sysVarInfo.DO_Variables).ChannelInfo.ChannelBit.ToString());
            }
            catch
            {
                Err = true;
            }
            if (Err)
            {
                MessageBox.Show("Variable Mapping is incorrect. Try after Variable Mapping Correctly.");
            }
            else
            {
                Ethercat.ExecuteCommand(ProgramBuffer.ACSC_BUFFER_9, 1, CMD);
                Ethercat.SaveToFlash();
            }
        }
    }
}
