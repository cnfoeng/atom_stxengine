﻿namespace AtomUI.DataForm
{
    partial class DataLogSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DataLogSetup));
            this.btnDN = new System.Windows.Forms.Button();
            this.btnUP = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.Header7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvList = new System.Windows.Forms.ListView();
            this.Header8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label2 = new System.Windows.Forms.Label();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tbMaxRowsPerFile = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbConstantInterval = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbAvgInterval = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbCtnInterval = new System.Windows.Forms.TextBox();
            this.btnFolder = new System.Windows.Forms.Button();
            this.lblDirectory = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbPrefix = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.cbDataLogType = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDN
            // 
            this.btnDN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDN.Location = new System.Drawing.Point(1218, 6);
            this.btnDN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnDN.Name = "btnDN";
            this.btnDN.Size = new System.Drawing.Size(45, 22);
            this.btnDN.TabIndex = 93;
            this.btnDN.Text = "DN";
            this.btnDN.UseVisualStyleBackColor = true;
            this.btnDN.Click += new System.EventHandler(this.btnDN_Click);
            // 
            // btnUP
            // 
            this.btnUP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUP.Location = new System.Drawing.Point(1159, 5);
            this.btnUP.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUP.Name = "btnUP";
            this.btnUP.Size = new System.Drawing.Size(45, 22);
            this.btnUP.TabIndex = 92;
            this.btnUP.Text = "UP";
            this.btnUP.UseVisualStyleBackColor = true;
            this.btnUP.Click += new System.EventHandler(this.btnUP_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRemove.Location = new System.Drawing.Point(155, 482);
            this.btnRemove.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(123, 40);
            this.btnRemove.TabIndex = 89;
            this.btnRemove.Text = "제거";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(1141, 685);
            this.btnOK.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(123, 40);
            this.btnOK.TabIndex = 86;
            this.btnOK.Text = "확인";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAdd.Location = new System.Drawing.Point(12, 482);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(123, 40);
            this.btnAdd.TabIndex = 87;
            this.btnAdd.Text = "추가";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // Header7
            // 
            this.Header7.Text = "Index";
            this.Header7.Width = 98;
            // 
            // Header3
            // 
            this.Header3.Text = "Description";
            this.Header3.Width = 200;
            // 
            // Header1
            // 
            this.Header1.Text = "Name";
            this.Header1.Width = 300;
            // 
            // lvList
            // 
            this.lvList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvList.BackColor = System.Drawing.Color.GhostWhite;
            this.lvList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Header1,
            this.Header3,
            this.Header7,
            this.Header8,
            this.Header9});
            this.lvList.FullRowSelect = true;
            this.lvList.GridLines = true;
            listViewGroup1.Header = "ListViewGroup";
            listViewGroup1.Name = "Global Variables";
            listViewGroup2.Header = "ListViewGroup";
            listViewGroup2.Name = "Local Variables";
            this.lvList.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2});
            this.lvList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvList.HideSelection = false;
            this.lvList.Location = new System.Drawing.Point(11, 34);
            this.lvList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lvList.Name = "lvList";
            this.lvList.ShowGroups = false;
            this.lvList.Size = new System.Drawing.Size(1251, 444);
            this.lvList.TabIndex = 85;
            this.lvList.UseCompatibleStateImageBehavior = false;
            this.lvList.View = System.Windows.Forms.View.Details;
            // 
            // Header8
            // 
            this.Header8.Text = "IO Value";
            this.Header8.Width = 77;
            // 
            // Header9
            // 
            this.Header9.Text = "Value";
            this.Header9.Width = 81;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 12);
            this.label2.TabIndex = 94;
            this.label2.Text = "할당된 변수";
            // 
            // btnLoad
            // 
            this.btnLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoad.Location = new System.Drawing.Point(1118, 482);
            this.btnLoad.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(144, 40);
            this.btnLoad.TabIndex = 96;
            this.btnLoad.Text = "파일로부터 불러오기";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(976, 482);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(123, 40);
            this.btnSave.TabIndex = 95;
            this.btnSave.Text = "파일로 저장";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.tbMaxRowsPerFile);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.tbConstantInterval);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.tbAvgInterval);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.tbCtnInterval);
            this.panel1.Controls.Add(this.btnFolder);
            this.panel1.Controls.Add(this.lblDirectory);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.tbPrefix);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(12, 528);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1252, 150);
            this.panel1.TabIndex = 100;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(996, 112);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 12);
            this.label11.TabIndex = 124;
            this.label11.Text = "Rows / 1 File";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(690, 115);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(198, 12);
            this.label10.TabIndex = 122;
            this.label10.Text = "상시 데이터 저장 파일 최대 Row 수";
            // 
            // tbMaxRowsPerFile
            // 
            this.tbMaxRowsPerFile.Location = new System.Drawing.Point(890, 111);
            this.tbMaxRowsPerFile.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbMaxRowsPerFile.Name = "tbMaxRowsPerFile";
            this.tbMaxRowsPerFile.Size = new System.Drawing.Size(100, 21);
            this.tbMaxRowsPerFile.TabIndex = 123;
            this.tbMaxRowsPerFile.Text = "1000";
            this.tbMaxRowsPerFile.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(744, 96);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(141, 12);
            this.label8.TabIndex = 119;
            this.label8.Text = "상시 데이터 저장 Interval";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(996, 94);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 12);
            this.label9.TabIndex = 121;
            this.label9.Text = "mSec";
            // 
            // tbConstantInterval
            // 
            this.tbConstantInterval.Location = new System.Drawing.Point(890, 92);
            this.tbConstantInterval.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbConstantInterval.Name = "tbConstantInterval";
            this.tbConstantInterval.Size = new System.Drawing.Size(100, 21);
            this.tbConstantInterval.TabIndex = 120;
            this.tbConstantInterval.Text = "1000";
            this.tbConstantInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(744, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(141, 12);
            this.label6.TabIndex = 116;
            this.label6.Text = "평균 데이터 저장 Interval";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(996, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 12);
            this.label7.TabIndex = 118;
            this.label7.Text = "mSec";
            // 
            // tbAvgInterval
            // 
            this.tbAvgInterval.Location = new System.Drawing.Point(890, 72);
            this.tbAvgInterval.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbAvgInterval.Name = "tbAvgInterval";
            this.tbAvgInterval.Size = new System.Drawing.Size(100, 21);
            this.tbAvgInterval.TabIndex = 117;
            this.tbAvgInterval.Text = "100";
            this.tbAvgInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(744, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 12);
            this.label1.TabIndex = 113;
            this.label1.Text = "연속 데이터 저장 Interval";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(996, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 12);
            this.label3.TabIndex = 115;
            this.label3.Text = "mSec";
            // 
            // tbCtnInterval
            // 
            this.tbCtnInterval.Location = new System.Drawing.Point(890, 53);
            this.tbCtnInterval.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbCtnInterval.Name = "tbCtnInterval";
            this.tbCtnInterval.Size = new System.Drawing.Size(100, 21);
            this.tbCtnInterval.TabIndex = 114;
            this.tbCtnInterval.Text = "100";
            this.tbCtnInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnFolder
            // 
            this.btnFolder.Location = new System.Drawing.Point(1120, 16);
            this.btnFolder.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnFolder.Name = "btnFolder";
            this.btnFolder.Size = new System.Drawing.Size(123, 28);
            this.btnFolder.TabIndex = 101;
            this.btnFolder.Text = "경로 설정";
            this.btnFolder.UseVisualStyleBackColor = true;
            this.btnFolder.Click += new System.EventHandler(this.btnFolder_Click);
            // 
            // lblDirectory
            // 
            this.lblDirectory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDirectory.Location = new System.Drawing.Point(161, 16);
            this.lblDirectory.Name = "lblDirectory";
            this.lblDirectory.Size = new System.Drawing.Size(915, 28);
            this.lblDirectory.TabIndex = 103;
            this.lblDirectory.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 12);
            this.label5.TabIndex = 102;
            this.label5.Text = "데이터 파일 폴더";
            // 
            // tbPrefix
            // 
            this.tbPrefix.Location = new System.Drawing.Point(161, 58);
            this.tbPrefix.Name = "tbPrefix";
            this.tbPrefix.Size = new System.Drawing.Size(100, 21);
            this.tbPrefix.TabIndex = 101;
            this.tbPrefix.Text = "Data";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 12);
            this.label4.TabIndex = 100;
            this.label4.Text = "데이터 파일 Prefix";
            // 
            // cbDataLogType
            // 
            this.cbDataLogType.FormattingEnabled = true;
            this.cbDataLogType.Items.AddRange(new object[] {
            "주기 수집",
            "상시 저장"});
            this.cbDataLogType.Location = new System.Drawing.Point(85, 8);
            this.cbDataLogType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbDataLogType.Name = "cbDataLogType";
            this.cbDataLogType.Size = new System.Drawing.Size(121, 20);
            this.cbDataLogType.TabIndex = 101;
            this.cbDataLogType.SelectedIndexChanged += new System.EventHandler(this.cbDataLogType_SelectedIndexChanged);
            // 
            // DataLogSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1273, 729);
            this.Controls.Add(this.cbDataLogType);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnDN);
            this.Controls.Add(this.btnUP);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lvList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "DataLogSetup";
            this.Text = "데이터 저장 설정";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DataLogSetup_FormClosed);
            this.Load += new System.EventHandler(this.DataLogSetup_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDN;
        private System.Windows.Forms.Button btnUP;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ColumnHeader Header7;
        private System.Windows.Forms.ColumnHeader Header3;
        private System.Windows.Forms.ColumnHeader Header1;
        private System.Windows.Forms.ListView lvList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ColumnHeader Header8;
        private System.Windows.Forms.ColumnHeader Header9;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblDirectory;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbPrefix;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnFolder;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ComboBox cbDataLogType;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbMaxRowsPerFile;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbConstantInterval;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbAvgInterval;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbCtnInterval;
    }
}