﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using AtomUI.Class;

namespace AtomUI.DataForm
{
    public partial class DataLogSetup : Form
    {
        public List<VariableInfo> InstantDataLogging_Variables = DaqData.sysVarInfo.InstantDataLogging_Variables;
        public List<VariableInfo> ConstantDataLogging_Variables = DaqData.sysVarInfo.ConstantDataLogging_Variables;

        public DataLogSetup()
        {
            InitializeComponent();
            cbDataLogType.SelectedIndex = 0;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            string str = "";

            if (cbDataLogType.SelectedIndex == 0)
            {
                str = "InstantDataLog";
            }
            else if (cbDataLogType.SelectedIndex == 1)
            {
                str = "ConstantDataLog";
            }

            DataForm.DataSelect ACSDataLog = new DataForm.DataSelect(str);
            DialogResult res = ACSDataLog.ShowDialog();

            MakeListView();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            string VariableName;
            int index = 0;
            if (cbDataLogType.SelectedIndex == 0)
            {
                foreach (ListViewItem itm in lvList.SelectedItems)
                {
                    VariableName = itm.SubItems[0].Text;
                    index = 0;
                    foreach (VariableInfo variable in InstantDataLogging_Variables)
                    {
                        if (variable.VariableName == VariableName)
                        {
                            InstantDataLogging_Variables.RemoveAt(index);
                            break;
                        }
                        index++;
                    }

                }

            }
            else if (cbDataLogType.SelectedIndex == 1)
            {
                foreach (ListViewItem itm in lvList.SelectedItems)
                {
                    VariableName = itm.SubItems[0].Text;
                    index = 0;
                    foreach (VariableInfo variable in ConstantDataLogging_Variables)
                    {
                        if (variable.VariableName == VariableName)
                        {
                            ConstantDataLogging_Variables.RemoveAt(index);
                            break;
                        }
                        index++;
                    }
                }
            }

            MakeListView();
            if (lvList.Items.Count > 0)
            {
                if (lvList.Items.Count > index)
                    lvList.Items[index].Selected = true;
                else
                    lvList.Items[index - 1].Selected = true;
            }
        }

        private void btnUP_Click(object sender, EventArgs e)
        {
            VariableInfo Var = null;
            if (lvList.SelectedItems.Count > 0)
            {
                int index = lvList.SelectedIndices[0];
                if (index > 0)
                {
                    if (cbDataLogType.SelectedIndex == 0)
                    {
                        Var = InstantDataLogging_Variables[index];
                        InstantDataLogging_Variables.RemoveAt(index);
                        InstantDataLogging_Variables.Insert(index - 1, Var);
                    }
                    else if (cbDataLogType.SelectedIndex == 1)
                    {
                        Var = ConstantDataLogging_Variables[index];
                        ConstantDataLogging_Variables.RemoveAt(index);
                        ConstantDataLogging_Variables.Insert(index - 1, Var);
                    }
                    MakeListView();
                    lvList.SelectedIndices.Clear();
                    lvList.SelectedIndices.Add(index - 1);
                }
            }
        }

        private void btnDN_Click(object sender, EventArgs e)
        {
            VariableInfo Var = null;
            if (lvList.SelectedItems.Count > 0)
            {
                int index = lvList.SelectedIndices[0];
                if (index < lvList.Items.Count - 1)
                {
                    if (cbDataLogType.SelectedIndex == 0)
                    {
                        Var = InstantDataLogging_Variables[index];
                        InstantDataLogging_Variables.RemoveAt(index);
                        InstantDataLogging_Variables.Insert(index + 1, Var);
                    }
                    else if (cbDataLogType.SelectedIndex == 1)
                    {
                        Var = ConstantDataLogging_Variables[index];
                        ConstantDataLogging_Variables.RemoveAt(index);
                        ConstantDataLogging_Variables.Insert(index + 1, Var);
                    }
                    MakeListView();
                    lvList.SelectedIndices.Add(index + 1);
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            SaveDataLog();
        }
        private void DataLogSetup_FormClosed(object sender, FormClosedEventArgs e)
        {
            SaveDataLog();
        }
        private void SaveDataLog()
        {
            try
            {
                SaveData.saveData.DataCtnLoggingInterval = int.Parse(tbCtnInterval.Text);
                SaveData.saveData.DataAvgLoggingInterval = int.Parse(tbAvgInterval.Text);
                SaveData.saveData.DataConstantLoggingInterval = int.Parse(tbConstantInterval.Text);
                SaveData.saveData.DataMaxRowsPerFile = int.Parse(tbMaxRowsPerFile.Text);
                SaveData.saveData.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SaveDataDir + "\\" + Ethercat.sysInfo.sysSettings.SaveDataFileName);
            }
            catch
            {
                System.IO.Directory.CreateDirectory(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SaveDataDir);
                SaveData.saveData.DataCtnLoggingInterval = int.Parse(tbCtnInterval.Text);
                SaveData.saveData.DataAvgLoggingInterval = int.Parse(tbAvgInterval.Text);
                SaveData.saveData.DataConstantLoggingInterval = int.Parse(tbConstantInterval.Text);
                SaveData.saveData.DataMaxRowsPerFile = int.Parse(tbMaxRowsPerFile.Text);
                SaveData.saveData.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SaveDataDir + "\\" + Ethercat.sysInfo.sysSettings.SaveDataFileName);
                //return;
            }

            if (Directory.Exists(lblDirectory.Text))
                Ethercat.sysInfo.sysSettings.DataLoggingDir = lblDirectory.Text;
            else
            {
                System.IO.Directory.CreateDirectory(Ethercat.sysInfo.sysSettings.DataLoggingDir + "\\Instant");
                System.IO.Directory.CreateDirectory(Ethercat.sysInfo.sysSettings.DataLoggingDir + "\\Constant");
                //return;
            }

            if (tbPrefix.Text.Length > 0)
                Ethercat.sysInfo.sysSettings.DataLoggingPrefix = tbPrefix.Text;
            else
            {
                MessageBox.Show(new Form { TopMost = true }, "Prefix Must Exist!");
                return;
            }

            Ethercat.sysInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SysInfoDir + "\\" + Ethercat.sysInfo.sysSettings.SysInfoFileName);

            DaqData.SaveSysInfo();
            this.Close();
        }

        private void DataLogSetup_Load(object sender, EventArgs e)
        {
            try
            {
                SaveData.saveData.Load(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SaveDataDir + "\\" + Ethercat.sysInfo.sysSettings.SaveDataFileName);

                tbCtnInterval.Text = SaveData.saveData.DataCtnLoggingInterval.ToString();
                tbAvgInterval.Text = SaveData.saveData.DataAvgLoggingInterval.ToString();
                tbConstantInterval.Text = SaveData.saveData.DataConstantLoggingInterval.ToString();
                tbMaxRowsPerFile.Text = SaveData.saveData.DataMaxRowsPerFile.ToString();
                lblDirectory.Text = Ethercat.sysInfo.sysSettings.DataLoggingDir;
                tbPrefix.Text = Ethercat.sysInfo.sysSettings.DataLoggingPrefix;
                MakeListView();
            }
            catch
            {

            }
        }

        private void MakeListView()
        {
            lvList.Items.Clear();

            if (cbDataLogType.SelectedIndex == 0)
            {
                for (int Index = 0; Index < DaqData.sysVarInfo.InstantDataLogging_Variables.Count; Index++)
                {
                    VariableInfo Var = DaqData.sysVarInfo.InstantDataLogging_Variables[Index];
                    ListViewItem itm = lvList.Items.Add(Var.VariableName);
                    //itm.SubItems.Add(Var.NorminalName);
                    itm.SubItems.Add(Var.NorminalDescription);
                    //itm.SubItems.Add(Var.Tag.ToString());
                    itm.SubItems.Add(Index.ToString());
                    //itm.SubItems.Add(Var.Removable.ToString());
                    if (Var.ChannelInfo != null)
                    {
                        //itm.SubItems.Add(" CH" + Var.ChannelInfo.ChannelIndex.ToString());
                        itm.SubItems.Add(Var.IOValue.ToString());
                        itm.SubItems.Add(Var.RealValue.ToString());
                        //itm.SubItems.Add(Var.ChannelInfo.ModuleInfo.SlaveNo.ToString());
                    }
                    else if (Var.Formula != "")
                    {
                        //itm.SubItems.Add(Var.Formula);
                        //itm.SubItems.Add("");
                        //itm.SubItems.Add(Var.RealValue.ToString());
                        //itm.SubItems.Add("");
                    }
                }
            }
            else if (cbDataLogType.SelectedIndex == 1)
            {
                for (int Index = 0; Index < DaqData.sysVarInfo.ConstantDataLogging_Variables.Count; Index++)
                {
                    VariableInfo Var = DaqData.sysVarInfo.ConstantDataLogging_Variables[Index];
                    ListViewItem itm = lvList.Items.Add(Var.VariableName);
                    //itm.SubItems.Add(Var.NorminalName);
                    itm.SubItems.Add(Var.NorminalDescription);
                    //itm.SubItems.Add(Var.Tag.ToString());
                    itm.SubItems.Add(Index.ToString());
                    //itm.SubItems.Add(Var.Removable.ToString());
                    if (Var.ChannelInfo != null)
                    {
                        ///itm.SubItems.Add(" CH" + Var.ChannelInfo.ChannelIndex.ToString());
                        itm.SubItems.Add(Var.IOValue.ToString());
                        itm.SubItems.Add(Var.RealValue.ToString());
                        //itm.SubItems.Add(Var.ChannelInfo.ModuleInfo.SlaveNo.ToString());
                    }
                    else if (Var.Formula != "")
                    {
                        //itm.SubItems.Add(Var.Formula);
                        //itm.SubItems.Add("");
                        //itm.SubItems.Add(Var.RealValue.ToString());
                        //itm.SubItems.Add("");
                    }
                }
            }
        }

        private void Reindex(List<VariableInfo> Variables)
        {
            for (int i = 0; i < Variables.Count; i++)
                Variables[i].Index = i;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "XML File Format|*.xml";
            saveFileDialog1.Title = "Save an Data Logging Config.";
            saveFileDialog1.FileName = "DataLogging.XML";

            if (DialogResult.OK == saveFileDialog1.ShowDialog() && saveFileDialog1.FileName != "")
            {
                if (cbDataLogType.SelectedIndex == 0)
                {
                    DaqData.sysVarInfo.SaveVariables(saveFileDialog1.FileName, InstantDataLogging_Variables);
                }
                else
                {
                    DaqData.sysVarInfo.SaveVariables(saveFileDialog1.FileName, ConstantDataLogging_Variables);
                }
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "XML File Format|*.xml";
            openFileDialog1.Title = "Load an Data Logging Config.";
            openFileDialog1.FileName = "DataLogging.XML";
            if (DialogResult.OK == openFileDialog1.ShowDialog() && openFileDialog1.FileName != "")
            {
                if (cbDataLogType.SelectedIndex == 0)
                {
                    DaqData.sysVarInfo.InstantDataLogging_Variables = DaqData.sysVarInfo.LoadVariables(openFileDialog1.FileName);
                    DaqData.sysVarInfo.LoadVariables(openFileDialog1.FileName);
                    InstantDataLogging_Variables = DaqData.sysVarInfo.InstantDataLogging_Variables;
                }
                else
                {
                    DaqData.sysVarInfo.ConstantDataLogging_Variables = DaqData.sysVarInfo.LoadVariables(openFileDialog1.FileName);
                    DaqData.sysVarInfo.LoadVariables(openFileDialog1.FileName);
                    ConstantDataLogging_Variables = DaqData.sysVarInfo.ConstantDataLogging_Variables;
                }
            }
            MakeListView();
        }

        private void btnFolder_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.Description = "데이터를 저장할 폴더를 선택해 주십시오.";
            folderBrowserDialog1.SelectedPath = lblDirectory.Text;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                if (Directory.Exists(folderBrowserDialog1.SelectedPath))
                    lblDirectory.Text = folderBrowserDialog1.SelectedPath;
                else
                {
                    MessageBox.Show(new Form { TopMost = true }, "해당 Directory가 존재하지 않습니다.");
                }
            }
        }

        private void cbDataLogType_SelectedIndexChanged(object sender, EventArgs e)
        {
            MakeListView();
        }
    }
}
