﻿namespace AtomUI.DataForm
{
    partial class DataSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.cmbVariableDefine = new System.Windows.Forms.ComboBox();
            this.lvList = new System.Windows.Forms.ListView();
            this.Header1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Header9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(1033, 522);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(123, 40);
            this.btnCancel.TabIndex = 88;
            this.btnCancel.Text = "CANCEL";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(895, 522);
            this.btnOK.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(123, 40);
            this.btnOK.TabIndex = 86;
            this.btnOK.Text = "ADD";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // cmbVariableDefine
            // 
            this.cmbVariableDefine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVariableDefine.FormattingEnabled = true;
            this.cmbVariableDefine.Items.AddRange(new object[] {
            "HMI IO",
            "HMI Variables",
            "DI Variables",
            "AI Variables",
            "DO Variables",
            "AO Variables",
            "Mexa9000 Variables",
            "ECU Variables",
            "Modbus Variables",
            "SmokeMeter Variables",
            "Calculation Variables"});
            this.cmbVariableDefine.Location = new System.Drawing.Point(11, 12);
            this.cmbVariableDefine.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbVariableDefine.Name = "cmbVariableDefine";
            this.cmbVariableDefine.Size = new System.Drawing.Size(281, 20);
            this.cmbVariableDefine.TabIndex = 84;
            this.cmbVariableDefine.SelectedIndexChanged += new System.EventHandler(this.cmbVariableDefine_SelectedIndexChanged);
            // 
            // lvList
            // 
            this.lvList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvList.BackColor = System.Drawing.Color.GhostWhite;
            this.lvList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Header1,
            this.Header2,
            this.Header3,
            this.Header4,
            this.Header5,
            this.Header6,
            this.Header7,
            this.Header8,
            this.Header9});
            this.lvList.FullRowSelect = true;
            this.lvList.GridLines = true;
            listViewGroup1.Header = "ListViewGroup";
            listViewGroup1.Name = "Global Variables";
            listViewGroup2.Header = "ListViewGroup";
            listViewGroup2.Name = "Local Variables";
            this.lvList.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2});
            this.lvList.HideSelection = false;
            this.lvList.Location = new System.Drawing.Point(11, 45);
            this.lvList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lvList.Name = "lvList";
            this.lvList.ShowGroups = false;
            this.lvList.Size = new System.Drawing.Size(1146, 462);
            this.lvList.TabIndex = 89;
            this.lvList.UseCompatibleStateImageBehavior = false;
            this.lvList.View = System.Windows.Forms.View.Details;
            // 
            // Header1
            // 
            this.Header1.DisplayIndex = 1;
            this.Header1.Text = "Name";
            this.Header1.Width = 158;
            // 
            // Header2
            // 
            this.Header2.DisplayIndex = 2;
            this.Header2.Text = "Norminal Name";
            this.Header2.Width = 179;
            // 
            // Header3
            // 
            this.Header3.DisplayIndex = 3;
            this.Header3.Text = "Description";
            this.Header3.Width = 334;
            // 
            // Header4
            // 
            this.Header4.DisplayIndex = 4;
            this.Header4.Text = "Tag";
            this.Header4.Width = 80;
            // 
            // Header5
            // 
            this.Header5.DisplayIndex = 0;
            this.Header5.Text = "Index";
            this.Header5.Width = 65;
            // 
            // Header6
            // 
            this.Header6.Text = "Removable";
            this.Header6.Width = 111;
            // 
            // Header7
            // 
            this.Header7.Text = "Channel";
            this.Header7.Width = 161;
            // 
            // Header8
            // 
            this.Header8.Text = "IO Value";
            this.Header8.Width = 77;
            // 
            // Header9
            // 
            this.Header9.Text = "Value";
            this.Header9.Width = 81;
            // 
            // DataSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1167, 570);
            this.Controls.Add(this.lvList);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.cmbVariableDefine);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "DataSelect";
            this.Text = "DataSelect";
            this.Load += new System.EventHandler(this.DataSelect_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.ComboBox cmbVariableDefine;
        private System.Windows.Forms.ListView lvList;
        private System.Windows.Forms.ColumnHeader Header1;
        private System.Windows.Forms.ColumnHeader Header2;
        private System.Windows.Forms.ColumnHeader Header3;
        private System.Windows.Forms.ColumnHeader Header4;
        private System.Windows.Forms.ColumnHeader Header5;
        private System.Windows.Forms.ColumnHeader Header6;
        private System.Windows.Forms.ColumnHeader Header7;
        private System.Windows.Forms.ColumnHeader Header8;
        private System.Windows.Forms.ColumnHeader Header9;
    }
}