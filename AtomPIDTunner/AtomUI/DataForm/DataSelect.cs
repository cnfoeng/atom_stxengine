﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;

namespace AtomUI.DataForm
{
    public partial class DataSelect : Form
    {
        public SysVarInfo sysVarInfo = DaqData.sysVarInfo;
        public string parent = "";

        public DataSelect(string str)
        {
            InitializeComponent();
            parent = str;
        }

        private void DataSelect_Load(object sender, EventArgs e)
        {
            cmbVariableDefine.Items.Clear();
            foreach (string varType in DaqData.getVarTypeList())
            {
                    cmbVariableDefine.Items.Add(varType);
            }
            cmbVariableDefine.SelectedIndex = 3;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            VariableInfo Var = null;
            List<VariableInfo> Variables = DaqData.sysVarInfo.getVariables(cmbVariableDefine.SelectedIndex);

            foreach (ListViewItem itm in lvList.SelectedItems)
            {
                int index = int.Parse(itm.SubItems[4].Text);
                Var = Variables[index];
                VarADD(Var);
            }
        }

        private void VarADD(VariableInfo Var)
        {
            bool check = true;
            if (parent == "Math")
            {

            }           
            else if (parent == "InstantDataLog")
            {
                sysVarInfo.InstantDataLogging_Variables.Add(Var);
            }
            else if (parent == "ConstantDataLog")
            {
                sysVarInfo.ConstantDataLogging_Variables.Add(Var);
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

        }

        private void cmbVariableDefine_SelectedIndexChanged(object sender, EventArgs e)
        {
            MakeListView();
        }

        //private List<VariableInfo> getVariables(int Index)
        //{
        //    List<VariableInfo> Variables = null;

        //    switch (Index)
        //    {
        //        case 0:
        //            Variables = sysVarInfo.HMI_IOVariables;
        //            break;
        //        case 1:
        //            Variables = sysVarInfo.HMI_Variables;
        //            break;
        //        case 2:
        //            Variables = sysVarInfo.DI_Variables;
        //            break;
        //        case 3:
        //            Variables = sysVarInfo.AI_Variables;
        //            break;
        //        case 4:
        //            Variables = sysVarInfo.DO_Variables;
        //            break;
        //        case 5:
        //            Variables = sysVarInfo.AO_Variables;
        //            break;
        //        case 6:
        //            Variables = sysVarInfo.Calc_Variables;
        //            break;
        //    }

        //    return Variables;

        //}

        //private string getPredefineLabel(int Index)
        //{
        //    string pre = "";

        //    switch (Index)
        //    {
        //        case 0:
        //            pre = "HMI";
        //            break;
        //        case 1:
        //            break;
        //        case 2:
        //            pre = "DI";
        //            break;
        //        case 3:
        //            pre = "AI";
        //            break;
        //        case 4:
        //            pre = "DO";
        //            break;
        //        case 5:
        //            pre = "AO";
        //            break;
        //        case 6:
        //            break;
        //    }

        //    return pre;

        //}

        private void MakeListView()
        {
            string pre = ((VariableType)cmbVariableDefine.SelectedIndex).ToString();
            List<VariableInfo> Variables = DaqData.sysVarInfo.getVariables(cmbVariableDefine.SelectedIndex);

            lvList.Items.Clear();
            int Index = 0;
            foreach (VariableInfo Var in Variables)
            {
                ListViewItem itm = lvList.Items.Add(Var.VariableName);
                itm.SubItems.Add(Var.NorminalName);
                itm.SubItems.Add(Var.NorminalDescription);
                itm.SubItems.Add(Var.Tag.ToString());
                itm.SubItems.Add(Index.ToString());
                itm.SubItems.Add(Var.Removable.ToString());
                if (pre != "" && Var.ChannelInfo != null)
                    itm.SubItems.Add(pre + " CH" + Var.ChannelInfo.ChannelIndex.ToString());
                else if (Var.Formula != "")
                    itm.SubItems.Add(Var.Formula);
                Index++;
            }
        }

    }
}
