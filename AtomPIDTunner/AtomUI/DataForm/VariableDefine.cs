﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;

namespace AtomUI.DataForm
{
    public partial class VariableDefine : Form
    {
        public SysVarInfo sysVarInfo = DaqData.sysVarInfo;

        public VariableDefine()
        {
            InitializeComponent();
        }

        private void VariableDefine_Load(object sender, EventArgs e)
        {
            cmbVariableDefine.Items.Clear();
            foreach (string varType in DaqData.getVarTypeList())
            {
                cmbVariableDefine.Items.Add(varType);
            }
            cmbVariableDefine.SelectedIndex = 3;
        }

        private void cmbVariableDefine_SelectedIndexChanged(object sender, EventArgs e)
        {
            MakeListView();
        }


        private void MakeListView()
        {
            string pre = ((VariableType)cmbVariableDefine.SelectedIndex).ToString();
            List<VariableInfo> Variables = DaqData.sysVarInfo.getVariables(cmbVariableDefine.SelectedIndex);

            lvList.Items.Clear();

            foreach (VariableInfo Var in Variables)
            {
                ListViewItem itm = lvList.Items.Add(Var.VariableName);
                itm.SubItems.Add(Var.NorminalName);
                itm.SubItems.Add(Var.NorminalDescription);
                itm.SubItems.Add(Var.Tag.ToString());
                itm.SubItems.Add(Var.Index.ToString());
                itm.SubItems.Add(Var.Removable.ToString());
                if (pre!="" && Var.ChannelInfo != null)
                    itm.SubItems.Add(pre + " CH" + Var.ChannelInfo.ChannelIndex.ToString());
                else if (Var.Formula != "")
                    itm.SubItems.Add(Var.Formula);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            List<VariableInfo> Variables = DaqData.sysVarInfo.getVariables(cmbVariableDefine.SelectedIndex);
            VariableInfo Var = new VariableInfo("New Variables");

            bool Calc = false;
            if (((string)cmbVariableDefine.Items[cmbVariableDefine.SelectedIndex]).Contains("Calc"))
                Calc = true;
            DataForm.VariableEdit varForm = new DataForm.VariableEdit(Variables, Var, false, Calc);
            varForm.Text = (string)cmbVariableDefine.Items[cmbVariableDefine.SelectedIndex] + " : " + Var.VariableName + " Add";
            varForm.Var = Var;

            DialogResult res = varForm.ShowDialog();
            Reindex(Variables);
            MakeListView();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            VariableInfo Var = null;
            if (lvList.SelectedItems.Count > 0)
            {
                List<VariableInfo> Variables = DaqData.sysVarInfo.getVariables(cmbVariableDefine.SelectedIndex);
                int index = lvList.SelectedIndices[0];
                Var = Variables[index];
                if (Var.Removable == true)
                {
                    Variables.RemoveAt(index);
                    Reindex(Variables);
                } else
                {
                    MessageBox.Show("Selected Variable is not Editable!", "Variable Define");
                }
            }
            else
            {
                MessageBox.Show("Select Variable, First!", "Variable Define");
            }
            MakeListView();
        }

        private void Reindex(List<VariableInfo> Variables)
        {
            for (int i = 0; i < Variables.Count; i++)
                Variables[i].Index = i;
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            Modify();
        }

        private void btnChannel_Click(object sender, EventArgs e)
        {
            if (cmbVariableDefine.SelectedIndex != 1 && cmbVariableDefine.SelectedIndex != 9)
            {
                if (lvList.SelectedItems.Count > 0 )
                {
                    List<VariableInfo> Variables = DaqData.sysVarInfo.getVariables(cmbVariableDefine.SelectedIndex);
                    int index = lvList.SelectedIndices[0];

                    DataForm.AssignChannel ACSVarDefine = new DataForm.AssignChannel();
                    ACSVarDefine.sysVarInfo = sysVarInfo;
                    ACSVarDefine.VariablesIndex = cmbVariableDefine.SelectedIndex;
                    ACSVarDefine.VarIndex = index;
                    DialogResult res = ACSVarDefine.ShowDialog();

                    MakeListView();
                    lvList.SelectedIndices.Add(index);
                }
                else
                {
                    MessageBox.Show("Select Variable, First!", "Variable Define");
                }
            }
            else if (cmbVariableDefine.SelectedIndex == 9)
            {
                // 계산식 지정 폼을 여기에서 띄운다.
            }
            else
            {
                MessageBox.Show("Assign not Allowed beside to IO Variable", "Variable Define");
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DaqData.SaveSysInfo();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

        }

        private void btnUP_Click(object sender, EventArgs e)
        {
            VariableInfo Var = null;
            if (lvList.SelectedItems.Count > 0)
            {
                List<VariableInfo> Variables = DaqData.sysVarInfo.getVariables(cmbVariableDefine.SelectedIndex);
                int index = int.Parse(lvList.SelectedItems[0].SubItems[4].Text);
                if (index > 0)
                {
                    Var = Variables[index];
                    Variables.RemoveAt(index);
                    Variables.Insert(index - 1, Var);
                    Reindex(Variables);
                    MakeListView();
                    lvList.SelectedIndices.Add(index - 1);
                }
            }
        }

        private void btnDN_Click(object sender, EventArgs e)
        {
            VariableInfo Var = null;
            if (lvList.SelectedItems.Count > 0)
            {
                List<VariableInfo> Variables = DaqData.sysVarInfo.getVariables(cmbVariableDefine.SelectedIndex);
                int index = int.Parse(lvList.SelectedItems[0].SubItems[4].Text);
                if (index < lvList.Items.Count-1)
                {
                    Var = Variables[index];
                    Variables.RemoveAt(index);
                    Variables.Insert(index + 1, Var);
                    Reindex(Variables);
                    MakeListView();
                    lvList.SelectedIndices.Add(index + 1);
                }
            }

        }

        private void lvList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lvList_DoubleClick(object sender, EventArgs e)
        {
            Modify();
        }

        private void Modify()
        {
            VariableInfo Var = null;
            if (lvList.SelectedItems.Count > 0)
            {
                List<VariableInfo> Variables = DaqData.sysVarInfo.getVariables(cmbVariableDefine.SelectedIndex);
                int index = lvList.SelectedIndices[0];
                Var = Variables[index];

                bool Calc = false;
                if (((string)cmbVariableDefine.Items[cmbVariableDefine.SelectedIndex]).Contains("Calc"))
                    Calc = true;
                DataForm.VariableEdit varForm = new DataForm.VariableEdit(Variables, Var, true, Calc);
                varForm.Text = (string)cmbVariableDefine.Items[cmbVariableDefine.SelectedIndex] + " : " + Var.VariableName + " Edit";
                varForm.Var = Var;

                DialogResult res = varForm.ShowDialog();
            }
            else
            {
                MessageBox.Show("Select Variable, First!", "Variable Define");
            }
            MakeListView();
        }
    }
}
