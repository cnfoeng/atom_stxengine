﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;
using System.Diagnostics;

namespace AtomUI.DataForm
{
    public partial class VariableEdit : Form
    {
        public VariableInfo Var = null;
        public List<VariableInfo> Variables = null;
        private bool Edit = false;
        private bool Calc = false;

        public VariableEdit(List<VariableInfo> Variables, VariableInfo Var, bool Edit, bool Calc)
        {
            InitializeComponent();

            this.Var = Var;
            this.Variables = Variables;
            this.Edit = Edit;
            this.Calc = Calc;

            pgVar.SelectedObject = Var;

            if (Edit)
                btnAssign.Text = "Close";
        }

        private void VariableEdit_Load(object sender, EventArgs e)
        {
            MakeRefListView();
            MakeCalTable();
        }

        private void MakeRefListView()
        {
            List<VariableInfo> Variables = DaqData.sysVarInfo.CalcRef_Variables;

            lvAssigned.Items.Clear();

            foreach (VariableInfo Var in Variables)
            {
                string pre = DaqData.sysVarInfo.getPredefineLabel(Var);

                ListViewItem itm = lvAssigned.Items.Add(Var.abbreviation);
                itm.SubItems.Add(Var.VariableName);
                itm.SubItems.Add(Var.NorminalName);
                itm.SubItems.Add(Var.NorminalDescription);
                itm.SubItems.Add(Var.Tag.ToString());
                itm.SubItems.Add(Var.Index.ToString());
                if (pre != "" && Var.ChannelInfo != null)
                    itm.SubItems.Add(pre + " CH" + Var.ChannelInfo.ChannelIndex.ToString());
                else if (Var.Formula != "")
                    itm.SubItems.Add(Var.Formula);
            }
        }

        private void btnAssign_Click(object sender, EventArgs e)
        {
            if (!Calc && (this.Var.Formula == null || this.Var.Formula.Length > 0))
            {
                MessageBox.Show("계산 변수가 아닌경우 수식입력이 허용되지 않습니다. 수식을 제거합니다.");
                this.Var.Formula = "";
            }

            if (!Edit)
                Variables.Add(this.Var);

            this.Close();

            //Debug.WriteLine(this.Var.Formula);

            //DaqData.sysVarInfo.CalcRef_Variables[0].IOValue = 3;

            //Debug.WriteLine(this.Var.RealValue);
            //DaqData.sysVarInfo.RenewDataTable();

            //DaqData.sysVarInfo.CalcRef_Variables[0].IOValue = 5;
            //Debug.WriteLine(this.Var.RealValue);
        }

        private void MakeCalTable()
        {
            //        public List<ValuePair> CalibrationTable = new List<ValuePair>();

            lvCalibration.Items.Clear();

            foreach (ValuePair vp in Var.CalibrationTable)
            {
                ListViewItem lvItem = new ListViewItem(vp.IOValue.ToString());
                lvItem.SubItems.Add(vp.RealValue.ToString());

                lvCalibration.Items.Add(lvItem);
            }
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            //lvCalibration.Items lvCalibration.SelectedIndices[0];

            if (lvCalibration.SelectedIndices[0] > 0)
            {
                ValuePair vp = Var.CalibrationTable[lvCalibration.SelectedIndices[0]];
                Var.CalibrationTable.RemoveAt(lvCalibration.SelectedIndices[0]);
                Var.CalibrationTable.Insert(lvCalibration.SelectedIndices[0] - 1, vp);

                MakeCalTable();
            }
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            if (lvCalibration.SelectedIndices[0] < lvCalibration.Items.Count-1)
            {
                ValuePair vp = Var.CalibrationTable[lvCalibration.SelectedIndices[0]];
                Var.CalibrationTable.RemoveAt(lvCalibration.SelectedIndices[0]);
                Var.CalibrationTable.Insert(lvCalibration.SelectedIndices[0] + 1, vp);
                MakeCalTable();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lvCalibration.Items.Count >0 && lvCalibration.SelectedItems.Count > 0)
            {
                Var.CalibrationTable.RemoveAt(lvCalibration.SelectedIndices[0]);
                MakeCalTable();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (tbIO.Text.Length > 0 && tbReal.Text.Length > 0)
            {
                ValuePair vp = new ValuePair(double.Parse(tbIO.Text), double.Parse(tbReal.Text));
                Var.CalibrationTable.Add(vp);
                Var.CalibrationTable.Sort(delegate (ValuePair p1, ValuePair p2) { return p1.IOValue.CompareTo(p2.IOValue); });
                MakeCalTable();
            }
        }

        private void VariableEdit_FormClosed(object sender, FormClosedEventArgs e)
        {
            DaqData.SaveSysInfo();
            Ethercat.SaveSysInfo();
        }
    }
}
