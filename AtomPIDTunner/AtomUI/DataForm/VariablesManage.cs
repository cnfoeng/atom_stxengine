﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;
using System.IO;

namespace AtomUI.DataForm
{
    public partial class VariablesManage : Form
    {
        public SysVarInfo varInfo = new SysVarInfo();

        public VariablesManage()
        {
            InitializeComponent();
        }

        private void VariablesManage_Load(object sender, EventArgs e)
        {
            FillTreeView(tvSysVariables, ref DaqData.sysVarInfo, "Current Variables");
            //FillTreeView(tvFileVariables, varInfo);

            string[] Files = Directory.GetFiles(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.VarInfoDir);
            foreach (string file in Files)
            {
                string filename = file.Substring(file.LastIndexOf('\\') + 1, file.Length - file.LastIndexOf('\\') - 1);
                string filenameonly = filename.Substring(0, filename.LastIndexOf('.'));
                lbFileList.Items.Add(filenameonly);
                if (Ethercat.sysInfo.sysSettings.VarInfoFileName == file)
                {
                    lbFileList.SelectedValue = filename;
                }
            }
            tbFileName.Text = Ethercat.sysInfo.sysSettings.VarInfoFileName.Substring(0, Ethercat.sysInfo.sysSettings.VarInfoFileName.LastIndexOf('.'));
        }

        private void FillTreeView(TreeView tvSysVariables, ref SysVarInfo VarInfo, string RootNodeName)
        {
            // Variables Node Start
            tvSysVariables.Nodes.Clear();
            TreeNode tv = tvSysVariables.Nodes.Add(RootNodeName);

            TreeNode tv_HMIIOV = tv.Nodes.Add("HMI IO Variables");
            Ethercat.MakeVarNode(tv_HMIIOV, VarInfo.HMI_IOVariables, "HIO");

            TreeNode tv_HMIV = tv.Nodes.Add("HMI Variables");
            Ethercat.MakeVarNode(tv_HMIV, VarInfo.HMI_Variables, "HMI");

            TreeNode tv_DIV = tv.Nodes.Add("Digital Input Variables");
            Ethercat.MakeVarNode(tv_DIV, VarInfo.DI_Variables, "DI");

            TreeNode tv_AIV = tv.Nodes.Add("Analog Input Variables");
            Ethercat.MakeVarNode(tv_AIV, VarInfo.AI_Variables, "AI");

            TreeNode tv_DOV = tv.Nodes.Add("Digital Output Variables");
            Ethercat.MakeVarNode(tv_DOV, VarInfo.DO_Variables, "DO");

            TreeNode tv_AOV = tv.Nodes.Add("Analog Output Variables");
            Ethercat.MakeVarNode(tv_AOV, VarInfo.AO_Variables, "AO");

            TreeNode tv_CV = tv.Nodes.Add("Calculating Variables");
            Ethercat.MakeVarNode(tv_CV, VarInfo.Calc_Variables, "CV");

            tv_CV.EnsureVisible();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (DialogResult.Yes == MessageBox.Show("Save current variables setting to File. Continue?", "Variable Save", MessageBoxButtons.YesNo))
                {
                    Ethercat.sysInfo.sysSettings.VarInfoFileName = tbFileName.Text + ".xml";
                    Ethercat.sysInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SysInfoDir + "\\" + Ethercat.sysInfo.sysSettings.SysInfoFileName);

                    DaqData.sysVarInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.VarInfoDir + "\\" + Ethercat.sysInfo.sysSettings.VarInfoFileName);
                }
            }
            catch
            {
                MessageBox.Show("Error occured during Save Variables", "Variable Save");
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            try
            {
                if (DialogResult.Yes == MessageBox.Show("Load variables setting from File. Current variables will be overwriting. Continue?", "Variable Save", MessageBoxButtons.YesNo))
                {
                    Ethercat.sysInfo.sysSettings.VarInfoFileName = tbFileName.Text + ".xml";
                    Ethercat.sysInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SysInfoDir + "\\" + Ethercat.sysInfo.sysSettings.SysInfoFileName);

                    DaqData.sysVarInfo.Load(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.VarInfoDir + "\\" + Ethercat.sysInfo.sysSettings.VarInfoFileName);
                    FillTreeView(tvSysVariables, ref DaqData.sysVarInfo, "Current Variables");
                }
            }
            catch
            {
                MessageBox.Show("Error occured during Load Variables", "Variable Load");
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {

        }

        private void lbFileList_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbFileName.Text = lbFileList.SelectedItem.ToString();
        }

        private void tbFileName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                varInfo.Load(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.VarInfoDir + "\\" + tbFileName.Text + ".xml");
                FillTreeView(tvFileVariables, ref varInfo, tbFileName.Text + " Variables");
                btnLoad.Enabled = true;
            }
            catch {
                tvFileVariables.Nodes.Clear();
                btnLoad.Enabled = false;
            }
        }
    }
}
