﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtomUI
{
    public partial class InputForm : Form
    {
        public string varname;
        public string varvalue;

        public InputForm()
        {
            InitializeComponent();
        }

        public string strReturn    //속성 선언
        {
            get
            {
                return tbValue.Text;
            }
        }

        private void InputForm_Load(object sender, EventArgs e)
        {
            label2.Text = varname;
            label4.Text = varvalue;
            tbValue.Text = varvalue;
        }
    }
}
