﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtomUI
{
    public partial class Mapping : Form
    {
        public List<AIFunction> lstAIDef = new List<AIFunction>();
        public List<AIFunction> lstSaveDef = new List<AIFunction>();

        public Mapping()
        {
            InitializeComponent();
        }

        private void Mapping_Load(object sender, EventArgs e)
        {
            lbRefresh();
        }

        private void lbRefresh()
        {
            lbSaveData.Items.Clear();
            foreach (AIFunction AIFSave in lstSaveDef)
            {
                lbSaveData.Items.Add(AIFSave.Name);

            }
            lbAllData.Items.Clear();
            foreach (AIFunction AIF in lstAIDef)
            {
                bool Exist = false;
                foreach (AIFunction AIFSave in lstSaveDef)
                {
                    if (AIF.Name == AIFSave.Name)
                    {
                        Exist = true;
                        break;
                    }
                }
                if (!Exist)
                    lbAllData.Items.Add(AIF.Name);
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            foreach (string str in lbAllData.SelectedItems)
            {
                bool Exist = false;
                foreach (AIFunction AIFSave in lstSaveDef)
                {
                    if (str == AIFSave.Name)
                    {
                        Exist = true;
                        break;
                    }
                }
                if (!Exist)
                {
                    foreach (AIFunction AIF in lstAIDef)
                    {
                        if (AIF.Name == str)
                        {
                            lstSaveDef.Add(AIF);
                        }
                    }
                }
            }
            lbRefresh();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            foreach (string str in lbSaveData.SelectedItems)
            {
                AIFunction SelectedAIF = null;
                foreach (AIFunction AIF in lstSaveDef)
                {
                    if (AIF.Name == str)
                    {
                        SelectedAIF = AIF;
                        break;
                    }
                }
                lstSaveDef.Remove(SelectedAIF);
            }
            lbRefresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ((BufferDataSave)this.Owner).lstSaveDef = lstSaveDef;
            ((BufferDataSave)this.Owner).lstAIDef = lstAIDef;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lstSaveDef.Clear();

            foreach (AIFunction AIF in lstAIDef)
            {
                lstSaveDef.Add(AIF);
            }
            lbRefresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            lstSaveDef.Clear();
            lbRefresh();
        }
    }
}
