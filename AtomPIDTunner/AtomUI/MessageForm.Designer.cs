﻿namespace AtomUI
{
    partial class MessageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.chkBuffer0 = new System.Windows.Forms.CheckBox();
            this.chkBuffer8 = new System.Windows.Forms.CheckBox();
            this.chkBuffer7 = new System.Windows.Forms.CheckBox();
            this.chkBuffer6 = new System.Windows.Forms.CheckBox();
            this.chkBuffer5 = new System.Windows.Forms.CheckBox();
            this.chkBuffer4 = new System.Windows.Forms.CheckBox();
            this.chkBuffer3 = new System.Windows.Forms.CheckBox();
            this.chkBuffer2 = new System.Windows.Forms.CheckBox();
            this.chkBuffer1 = new System.Windows.Forms.CheckBox();
            this.chkBuffer9 = new System.Windows.Forms.CheckBox();
            this.tbMessage = new System.Windows.Forms.RichTextBox();
            this.tbCMD = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // chkBuffer0
            // 
            this.chkBuffer0.AutoSize = true;
            this.chkBuffer0.Location = new System.Drawing.Point(13, 51);
            this.chkBuffer0.Name = "chkBuffer0";
            this.chkBuffer0.Size = new System.Drawing.Size(80, 19);
            this.chkBuffer0.TabIndex = 0;
            this.chkBuffer0.Text = "Buffer 0";
            this.chkBuffer0.UseVisualStyleBackColor = true;
            this.chkBuffer0.CheckedChanged += new System.EventHandler(this.chkBuffer0_CheckedChanged);
            // 
            // chkBuffer8
            // 
            this.chkBuffer8.AutoSize = true;
            this.chkBuffer8.Location = new System.Drawing.Point(701, 51);
            this.chkBuffer8.Name = "chkBuffer8";
            this.chkBuffer8.Size = new System.Drawing.Size(80, 19);
            this.chkBuffer8.TabIndex = 1;
            this.chkBuffer8.Text = "Buffer 8";
            this.chkBuffer8.UseVisualStyleBackColor = true;
            this.chkBuffer8.CheckedChanged += new System.EventHandler(this.chkBuffer0_CheckedChanged);
            // 
            // chkBuffer7
            // 
            this.chkBuffer7.AutoSize = true;
            this.chkBuffer7.Location = new System.Drawing.Point(615, 51);
            this.chkBuffer7.Name = "chkBuffer7";
            this.chkBuffer7.Size = new System.Drawing.Size(80, 19);
            this.chkBuffer7.TabIndex = 2;
            this.chkBuffer7.Text = "Buffer 7";
            this.chkBuffer7.UseVisualStyleBackColor = true;
            this.chkBuffer7.CheckedChanged += new System.EventHandler(this.chkBuffer0_CheckedChanged);
            // 
            // chkBuffer6
            // 
            this.chkBuffer6.AutoSize = true;
            this.chkBuffer6.Location = new System.Drawing.Point(529, 51);
            this.chkBuffer6.Name = "chkBuffer6";
            this.chkBuffer6.Size = new System.Drawing.Size(80, 19);
            this.chkBuffer6.TabIndex = 3;
            this.chkBuffer6.Text = "Buffer 6";
            this.chkBuffer6.UseVisualStyleBackColor = true;
            this.chkBuffer6.CheckedChanged += new System.EventHandler(this.chkBuffer0_CheckedChanged);
            // 
            // chkBuffer5
            // 
            this.chkBuffer5.AutoSize = true;
            this.chkBuffer5.Location = new System.Drawing.Point(443, 51);
            this.chkBuffer5.Name = "chkBuffer5";
            this.chkBuffer5.Size = new System.Drawing.Size(80, 19);
            this.chkBuffer5.TabIndex = 4;
            this.chkBuffer5.Text = "Buffer 5";
            this.chkBuffer5.UseVisualStyleBackColor = true;
            this.chkBuffer5.CheckedChanged += new System.EventHandler(this.chkBuffer0_CheckedChanged);
            // 
            // chkBuffer4
            // 
            this.chkBuffer4.AutoSize = true;
            this.chkBuffer4.Location = new System.Drawing.Point(357, 51);
            this.chkBuffer4.Name = "chkBuffer4";
            this.chkBuffer4.Size = new System.Drawing.Size(80, 19);
            this.chkBuffer4.TabIndex = 5;
            this.chkBuffer4.Text = "Buffer 4";
            this.chkBuffer4.UseVisualStyleBackColor = true;
            this.chkBuffer4.CheckedChanged += new System.EventHandler(this.chkBuffer0_CheckedChanged);
            // 
            // chkBuffer3
            // 
            this.chkBuffer3.AutoSize = true;
            this.chkBuffer3.Location = new System.Drawing.Point(271, 51);
            this.chkBuffer3.Name = "chkBuffer3";
            this.chkBuffer3.Size = new System.Drawing.Size(80, 19);
            this.chkBuffer3.TabIndex = 6;
            this.chkBuffer3.Text = "Buffer 3";
            this.chkBuffer3.UseVisualStyleBackColor = true;
            this.chkBuffer3.CheckedChanged += new System.EventHandler(this.chkBuffer0_CheckedChanged);
            // 
            // chkBuffer2
            // 
            this.chkBuffer2.AutoSize = true;
            this.chkBuffer2.Location = new System.Drawing.Point(185, 51);
            this.chkBuffer2.Name = "chkBuffer2";
            this.chkBuffer2.Size = new System.Drawing.Size(80, 19);
            this.chkBuffer2.TabIndex = 7;
            this.chkBuffer2.Text = "Buffer 2";
            this.chkBuffer2.UseVisualStyleBackColor = true;
            this.chkBuffer2.CheckedChanged += new System.EventHandler(this.chkBuffer0_CheckedChanged);
            // 
            // chkBuffer1
            // 
            this.chkBuffer1.AutoSize = true;
            this.chkBuffer1.Location = new System.Drawing.Point(99, 51);
            this.chkBuffer1.Name = "chkBuffer1";
            this.chkBuffer1.Size = new System.Drawing.Size(80, 19);
            this.chkBuffer1.TabIndex = 8;
            this.chkBuffer1.Text = "Buffer 1";
            this.chkBuffer1.UseVisualStyleBackColor = true;
            this.chkBuffer1.CheckedChanged += new System.EventHandler(this.chkBuffer0_CheckedChanged);
            // 
            // chkBuffer9
            // 
            this.chkBuffer9.AutoSize = true;
            this.chkBuffer9.Location = new System.Drawing.Point(787, 51);
            this.chkBuffer9.Name = "chkBuffer9";
            this.chkBuffer9.Size = new System.Drawing.Size(80, 19);
            this.chkBuffer9.TabIndex = 9;
            this.chkBuffer9.Text = "Buffer 9";
            this.chkBuffer9.UseVisualStyleBackColor = true;
            this.chkBuffer9.CheckedChanged += new System.EventHandler(this.chkBuffer0_CheckedChanged);
            // 
            // tbMessage
            // 
            this.tbMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMessage.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbMessage.Location = new System.Drawing.Point(12, 76);
            this.tbMessage.Name = "tbMessage";
            this.tbMessage.Size = new System.Drawing.Size(851, 356);
            this.tbMessage.TabIndex = 72;
            this.tbMessage.Text = "";
            // 
            // tbCMD
            // 
            this.tbCMD.Location = new System.Drawing.Point(12, 11);
            this.tbCMD.Name = "tbCMD";
            this.tbCMD.Size = new System.Drawing.Size(511, 25);
            this.tbCMD.TabIndex = 73;
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(534, 13);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(161, 23);
            this.btnSend.TabIndex = 74;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(706, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(161, 23);
            this.button1.TabIndex = 75;
            this.button1.Text = "Clear";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MessageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(875, 444);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.tbCMD);
            this.Controls.Add(this.tbMessage);
            this.Controls.Add(this.chkBuffer9);
            this.Controls.Add(this.chkBuffer1);
            this.Controls.Add(this.chkBuffer2);
            this.Controls.Add(this.chkBuffer3);
            this.Controls.Add(this.chkBuffer4);
            this.Controls.Add(this.chkBuffer5);
            this.Controls.Add(this.chkBuffer6);
            this.Controls.Add(this.chkBuffer7);
            this.Controls.Add(this.chkBuffer8);
            this.Controls.Add(this.chkBuffer0);
            this.Name = "MessageForm";
            this.Text = "MessageForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MessageForm_FormClosed);
            this.Load += new System.EventHandler(this.MessageForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkBuffer0;
        private System.Windows.Forms.CheckBox chkBuffer8;
        private System.Windows.Forms.CheckBox chkBuffer7;
        private System.Windows.Forms.CheckBox chkBuffer6;
        private System.Windows.Forms.CheckBox chkBuffer5;
        private System.Windows.Forms.CheckBox chkBuffer4;
        private System.Windows.Forms.CheckBox chkBuffer3;
        private System.Windows.Forms.CheckBox chkBuffer2;
        private System.Windows.Forms.CheckBox chkBuffer1;
        private System.Windows.Forms.CheckBox chkBuffer9;
        private System.Windows.Forms.RichTextBox tbMessage;
        private System.Windows.Forms.TextBox tbCMD;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer timer1;
    }
}