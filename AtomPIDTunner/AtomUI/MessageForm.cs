﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ACS.SPiiPlusNET;

namespace AtomUI
{
    public partial class MessageForm : Form
    {
        public Api Ch; //  For communcating between the UMD and the Host Application. All communication commands are methods within the AsyncChannel Class.
        public List<displayCmdVal> lstCMD = new List<displayCmdVal>();
        public MessageForm()
        {
            InitializeComponent();
            InitializeList();
        }

        public void InitializeList()
        {
            lstCMD.Add(new displayCmdVal("g_Buffer0_displayCmdLevel", ProgramBuffer.ACSC_NONE, chkBuffer0));
            lstCMD.Add(new displayCmdVal("g_Buffer1_displayCmdLevel", ProgramBuffer.ACSC_NONE, chkBuffer1));
            lstCMD.Add(new displayCmdVal("g_Buffer2_displayCmdLevel", ProgramBuffer.ACSC_NONE, chkBuffer2));
            lstCMD.Add(new displayCmdVal("g_Buffer3_displayCmdLevel", ProgramBuffer.ACSC_NONE, chkBuffer3));
            lstCMD.Add(new displayCmdVal("g_Buffer4_displayCmdLevel", ProgramBuffer.ACSC_NONE, chkBuffer4));
            lstCMD.Add(new displayCmdVal("g_Buffer5_displayCmdLevel", ProgramBuffer.ACSC_NONE, chkBuffer5));
            lstCMD.Add(new displayCmdVal("g_Buffer6_displayCmdLevel", ProgramBuffer.ACSC_NONE, chkBuffer6));
            lstCMD.Add(new displayCmdVal("g_Buffer7_displayCmdLevel", ProgramBuffer.ACSC_NONE, chkBuffer7));
            lstCMD.Add(new displayCmdVal("g_Buffer8_displayCmdLevel", ProgramBuffer.ACSC_NONE, chkBuffer8));
            lstCMD.Add(new displayCmdVal("g_Buffer9_displayCmdLevel", ProgramBuffer.ACSC_NONE, chkBuffer9));
        }

        private void MessageForm_Load(object sender, EventArgs e)
        {
            object Result = null;
            if (Ch.IsConnected)
                foreach (displayCmdVal val in lstCMD)
                {
                    try
                    {
                        Result = Ch.ReadVariable(val.varName, val.buffer0, -1, -1, -1, -1);
                        if ((int)Result > 0)
                        {
                            val.chkBuffer.Checked = true;
                        }
                        else
                        {
                            val.chkBuffer.Checked = false;
                        }

                    }
                    catch { }
                }

            // Open message buffer
            int bufferSize = 5000;
            //The method opens an unsolicited messages
            //buffer size of the opened buffer is 5000 
            Ch.OpenMessageBuffer(bufferSize);
            timer1.Start();
        }

        private void chkBuffer0_CheckedChanged(object sender, EventArgs e)
        {
            int Value = 0;
            foreach (displayCmdVal val in lstCMD)
            {
                try
                {
                    if (((CheckBox)sender).Equals(val.chkBuffer))
                    {
                        if (val.chkBuffer.Checked)
                            Value = 15;
                        Ch.WriteVariable(Value, val.varName, val.buffer0, -1, -1, -1, -1);
                    }
                }
                catch { }
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                string MSG = Ch.Transaction(tbCMD.Text);
                tbMessage.Text = tbCMD.Text + "\r" + MSG + tbMessage.Text;
            } 
            catch
            {
                MessageBox.Show("Invalid Command");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tbMessage.Text = "";
        }

        private void MessageForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Close message buffer
            //The method closes the messages buffer
            //and discards all stored unsolicited messages
            Ch.CloseMessageBuffer();
            timer1.Stop();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int timeout = 100;
            timer1.Stop();
            try
            {
                for (int i = 0; i < 100; i++)
                {
                    string message = Ch.GetSingleMessage(timeout);
                    tbMessage.Text = message + tbMessage.Text;
                }
            }
            catch { }
            timer1.Start();
        }
    }

    public class displayCmdVal
    {
        public string varName;
        public ProgramBuffer buffer0;
        public CheckBox chkBuffer;
        public displayCmdVal(string varName, ProgramBuffer buffer0, CheckBox chkBuffer)
        {
            this.varName = varName;
            this.buffer0 = buffer0;
            this.chkBuffer = chkBuffer;
        }
    }
}
