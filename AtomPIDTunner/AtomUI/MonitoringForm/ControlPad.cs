﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;

namespace AtomUI.MonitoringForm
{
    public partial class ControlPad : Form
    {
        public bool bClose = false;
        public int SelectedItem = 0;
        public List<NextUI.BaseUI.BaseUI> PNList = new List<NextUI.BaseUI.BaseUI>();
        public List<MonitorItem> MIList = new List<MonitorItem>();

        public ControlPad()
        {
            InitializeComponent();
            this.Top = 0;
            this.Height = 1024;
        }

        private void ControlPad_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!bClose)
                e.Cancel = true;
        }

        public NextUI.BaseUI.NextUIType SelectedUI;

        private void ControlPad_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < 9; i++)
            {
                NextUI.BaseUI.BaseUI pn = CreatePanel((NextUI.BaseUI.NextUIType)i, new Point(4, (Height / 9 - 4) * i), new Size(Width-20, Height / 9 - 4), "", i);
                PNList.Add(pn);
            }
        }

        private NextUI.BaseUI.BaseUI CreatePanel(NextUI.BaseUI.NextUIType UIType, Point Location, Size size, string Title, int id)
        {
            NextUI.BaseUI.BaseUI pn = new NextUI.BaseUI.BaseUI();
            pn.Location = Location;
            pn.BackColor = Color.PaleGoldenrod;
            pn.Size = size;
            pn.UIType = UIType;
            pn.ID = id;
            //if (UIType != NextUI.BaseUI.NextUIType.KnobDigital)
            //ControlMoverOrResizer.Init(pn);
            pn.Value = 1.1f;
            pn.Click += Pn_Click;
            this.Controls.Add(pn);

            return pn;
        }

        private void Pn_Click(object sender, EventArgs e)
        {
            SelectedItem = 0;
            for (int i = 0; i < PNList.Count; i++)
            {
                if ((NextUI.BaseUI.BaseUI)sender == PNList[i])
                {
                    PNList[i].BorderStyle = BorderStyle.Fixed3D;
                    SelectedUI = PNList[i].UIType;
                    SelectedItem = i;
                }
                else
                {
                    PNList[i].BorderStyle = BorderStyle.FixedSingle;
                }
            }
        }
    }
}
