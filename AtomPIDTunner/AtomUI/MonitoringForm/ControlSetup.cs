﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;

namespace AtomUI.MonitoringForm
{
    public partial class ControlSetup : Form
    {
        public MonitorItem ControlItem;
        public SysVarInfo sysVarInfo = DaqData.sysVarInfo;

        public ControlSetup()
        {
            InitializeComponent();
        }

        private void ControlSetup_SizeChanged(object sender, EventArgs e)
        {
            tbTitle.Left = this.Width / 4;
            tbTitle.Width = this.Width / 2;
        }

        private void ControlSetup_Load(object sender, EventArgs e)
        {
            pgMonitor.SelectedObject = ControlItem;
            cmbVariableDefine.Items.Clear();
            foreach (string varType in DaqData.getVarTypeList())
            {
                cmbVariableDefine.Items.Add(varType);
            }
            cmbVariableDefine.SelectedIndex = 3;
            MakeListViewAssigned();
            SetTitle();
        }

        private void cmbVariableDefine_SelectedIndexChanged(object sender, EventArgs e)
        {
            MakeListView();
        }

        //private List<VariableInfo> getVariables(int Index)
        //{
        //    List<VariableInfo> Variables = null;

        //    switch (Index)
        //    {
        //        case 0:
        //            Variables = sysVarInfo.HMI_IOVariables;
        //            break;
        //        case 1:
        //            Variables = sysVarInfo.HMI_Variables;
        //            break;
        //        case 2:
        //            Variables = sysVarInfo.DI_Variables;
        //            break;
        //        case 3:
        //            Variables = sysVarInfo.AI_Variables;
        //            break;
        //        case 4:
        //            Variables = sysVarInfo.DO_Variables;
        //            break;
        //        case 5:
        //            Variables = sysVarInfo.AO_Variables;
        //            break;
        //        case 6:
        //            Variables = sysVarInfo.Calc_Variables;
        //            break;
        //    }

        //    return Variables;

        //}

        //private string getPredefineLabel(int Index)
        //{
        //    string pre = "";

        //    switch (Index)
        //    {
        //        case 0:
        //            pre = "HMI";
        //            break;
        //        case 1:
        //            break;
        //        case 2:
        //            pre = "DI";
        //            break;
        //        case 3:
        //            pre = "AI";
        //            break;
        //        case 4:
        //            pre = "DO";
        //            break;
        //        case 5:
        //            pre = "AO";
        //            break;
        //        case 6:
        //            break;
        //    }

        //    return pre;

        //}


        private void MakeListView()
        {
            string pre = ((VariableType)cmbVariableDefine.SelectedIndex).ToString();
            List<VariableInfo> Variables = DaqData.sysVarInfo.getVariables(cmbVariableDefine.SelectedIndex);

            lvList.Items.Clear();

            foreach (VariableInfo Var in Variables)
            {
                ListViewItem itm = lvList.Items.Add(Var.VariableName);
                itm.SubItems.Add(Var.NorminalName);
                itm.SubItems.Add(Var.NorminalDescription);
                itm.SubItems.Add(Var.Tag.ToString());
                itm.SubItems.Add(Var.Index.ToString());
                itm.SubItems.Add(Var.Removable.ToString());
                if (pre != "" && Var.ChannelInfo != null)
                    itm.SubItems.Add(pre + " CH" + Var.ChannelInfo.ChannelIndex.ToString());
                else if (Var.Formula != "")
                    itm.SubItems.Add(Var.Formula);
            }
        }

        private void MakeListViewAssigned()
        {
            if (ControlItem.VARs == null)
            {
                ControlItem.VARs = new List<VariableInfo>();
            }
            List<VariableInfo> Variables = ControlItem.VARs;

            lvAssigned.Items.Clear();

            foreach (VariableInfo Var in Variables)
            {
                ListViewItem itm = lvAssigned.Items.Add(Var.VariableName);
                itm.SubItems.Add(Var.NorminalName);
                itm.SubItems.Add(Var.NorminalDescription);
                itm.SubItems.Add(Var.Tag.ToString());
                itm.SubItems.Add(Var.Index.ToString());
                itm.SubItems.Add(Var.Removable.ToString());
                if (Var.Formula != "")
                    itm.SubItems.Add(Var.Formula);
                else
                    itm.SubItems.Add(" CH" + Var.ChannelInfo.ChannelIndex.ToString());

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            VariableInfo Var = null;
            if (lvList.SelectedItems.Count > 0)
            {
                List<VariableInfo> Variables = DaqData.sysVarInfo.getVariables(cmbVariableDefine.SelectedIndex);
                int index = int.Parse(lvList.SelectedItems[0].SubItems[4].Text);
                foreach (VariableInfo vinfo in Variables)
                {
                    if (vinfo.Index == index)
                    {
                        Var = vinfo;
                        break;
                    }
                }
                ControlItem.VARs.Add(Var);
                MakeListViewAssigned();

                ControlItem.Name = Var.VariableName;

                ControlItem.Range_High = Var.Range_High;
                ControlItem.Range_Low = Var.Range_Low;
                ControlItem.Alarm_Low = Var.Alarm_Low;
                ControlItem.Alarm_High = Var.Alarm_High;
                ControlItem.Warning_High = Var.Warning_High;
                ControlItem.Warning_Low = Var.Warning_Low;
                ControlItem.Unit = Var.Unit;
                ControlItem.Formula = Var.Formula;
                pgMonitor.Refresh();
                SetTitle();
            }
            else
            {
                MessageBox.Show("Select Variable, First!", "Variable Define");
            }
        }

        private void btnVariables_Click(object sender, EventArgs e)
        {
            VariableInfo Var = null;
            if (lvAssigned.SelectedItems.Count > 0)
            {
                List<VariableInfo> Variables = ControlItem.VARs;
                int index = lvAssigned.SelectedIndices[0];
                Var = Variables[index];
                 ControlItem.VARs.RemoveAt(index);
                MakeListViewAssigned();

            }
            else
            {
                MessageBox.Show("Select Variable, First!", "Variable Define");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pgMonitor_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            SetTitle();
        }

        private void SetTitle()
        {
            tbTitle.Text = ControlItem.monitorType.ToString() + " : " + ControlItem.Name;
        }
    }
}
