﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;
using System.Windows.Forms.DataVisualization.Charting;

namespace AtomUI.MonitoringForm
{
    public partial class CustomMonitor : Form
    {
        List<NextUIControl> UIControls = new List<NextUIControl>();
        string _MonitorName;
        public MonitorInfo monitor = new MonitorInfo();

        public CustomMonitor(string MonitorName)
        {
            InitializeComponent();

            _MonitorName = MonitorName;
            this.Text = _MonitorName;
            monitor.LoadMonInfo(_MonitorName);
        }

        private void CustomMonitor_Load(object sender, EventArgs e)
        {
            int id = 0;
            foreach (MonitorItem mon in monitor.MonitorList.Monitor)
            {
                Size size = new Size(mon.Width, mon.Height);
                Point pt = new Point(mon.Left, mon.Top);
                NextUI.BaseUI.BaseUI pn = CreatePanel(mon.monitorType, pt, size, "", id);
                UIPropertySet(mon, pn);
                id++;
            }

            timer1.Enabled = true;
        }

        private void UIPropertyGet(MonitorItem mon, NextUI.BaseUI.BaseUI pn)
        {
            mon.BackGroundColor = pn.BackGroundColor;
            mon.TextBackGroundColorWarning = pn.TextBackGroundColorWarning;
            mon.TextBackGroundColorError = pn.TextBackGroundColorError;
            mon.TextBackGroundColor = pn.TextBackGroundColor;
            mon.UnitTextColor = pn.UnitTextColor;
            mon.TitleTextColor = pn.TitleTextColor;
            mon.ValueTextColor = pn.ValueTextColor;
            mon.ValueScaleColor = pn.ValueScaleColor;

            mon.Range_Low = pn.StartValue;
            mon.Range_High = pn.EndValue;

            mon.Warning_High = pn.WarningValueHigh;
            mon.Warning_Low = pn.WarningValueLow;
            mon.Alarm_High = pn.ErrorValueHigh;
            mon.Alarm_Low = pn.ErrorValueLow;

            mon.Left = pn.Left;
            mon.Top = pn.Top;
            mon.Width = pn.Width;
            mon.Height = pn.Height;

            mon.Name = pn.Title;
            mon.Unit = pn.Unit;

        }

        private void UIPropertySet(MonitorItem mon, NextUI.BaseUI.BaseUI pn)
        {
            pn.BackGroundColor = mon.BackGroundColor;
            pn.TextBackGroundColorWarning = mon.TextBackGroundColorWarning;
            pn.TextBackGroundColorError = mon.TextBackGroundColorError;
            pn.TextBackGroundColor = mon.TextBackGroundColor;
            pn.UnitTextColor = mon.UnitTextColor;
            pn.TitleTextColor = mon.TitleTextColor;
            pn.ValueTextColor = mon.ValueTextColor;
            pn.ValueScaleColor = mon.ValueScaleColor;
            try
            {
                pn.BelowDecimalNo = mon.VARs[0].Round;
            }catch(ArgumentOutOfRangeException e){
                MessageBox.Show("변수 목록에 존재하지 않는 모니터가 존재합니다.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            pn.WarningValueHigh = (int)mon.Warning_High;
            pn.WarningValueLow = (int)mon.Warning_Low;
            pn.ErrorValueHigh = (int)mon.Alarm_High;
            pn.ErrorValueLow = (int)mon.Alarm_Low;

            pn.StartValue = (int)mon.Range_Low;
            pn.EndValue = (int)mon.Range_High;

            pn.Left = mon.Left;
            pn.Top = mon.Top;
            pn.Width = mon.Width;
            pn.Height = mon.Height;

            pn.Title = mon.Name;
            pn.Unit = mon.Unit;
            pn.RePaint = true;
            pn.Invalidate();

        }

        private NextUI.BaseUI.BaseUI CreatePanel(NextUI.BaseUI.NextUIType UIType, Point Location, Size size, string Title, int id)
        {
            NextUI.BaseUI.BaseUI pn = new NextUI.BaseUI.BaseUI();
            pn.Location = Location;
            pn.BackColor = Color.PaleGoldenrod;
            pn.Size = size;
            pn.UIType = UIType;
            pn.ID = id;
            //if (UIType != NextUI.BaseUI.NextUIType.KnobDigital)
            ControlMoverOrResizer.Init(pn);
            pn.Value = 1.1f;
            this.Controls.Add(pn);

            return pn;
        }

        //double time = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (monitor.MonitorList.Monitor.Count == this.Controls.Count)
            {
                for (int ind = 0; ind < this.Controls.Count; ind++)
                {
                    NextUI.BaseUI.BaseUI pn = (NextUI.BaseUI.BaseUI)(this.Controls[ind]);
                    MonitorItem mon = monitor.MonitorList.Monitor[pn.ID];

                    if (mon.VARs.Count > 0)
                    {
                        //System.Diagnostics.Debug.WriteLine(monitor.MonitorList.Monitor[pn.ID].VARs[0].VariableName);
                        pn.Value = (float)monitor.MonitorList.Monitor[pn.ID].VARs[0].RealValue;
                    }
                }
            }
            //if (monitor.MonitorList.Monitor.Count == this.Controls.Count)
            //{
            //    time += timer1.Interval / 1000;

            //    //for (int i = 0; i < MonitorInfo.MonitorList.Monitor.Count; i++)
            //    //{
            //    //    MonitorInfo.MonitorList.Monitor[i].Left = Controls[i].Left;
            //    //    MonitorInfo.MonitorList.Monitor[i].Top = Controls[i].Top;
            //    //    MonitorInfo.MonitorList.Monitor[i].Width = Controls[i].Width;
            //    //    MonitorInfo.MonitorList.Monitor[i].Height = Controls[i].Height;
            //    //}
            //    //MonitorInfo.SaveMonInfo(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.MonInfoDir + "\\" + Ethercat.sysInfo.sysSettings.MonInfoFileName);

            //    for (int ind = 0; ind < monitor.MonitorList.Monitor.Count; ind++)
            //    {
            //        NextUI.BaseUI.BaseUI pn = (NextUI.BaseUI.BaseUI)(this.Controls[ind]);
            //        MonitorItem mon = monitor.MonitorList.Monitor[pn.ID];
            //        if (mon.monitorType == NextUI.BaseUI.NextUIType.Graph)
            //        {
            //            if (mon.VARs.Count == 0)
            //            {
            //                DataPoint dataP = new DataPoint(time, (double)(new Random((int)DateTime.Now.Ticks + ind).NextDouble()));
            //                pn.PointValue = dataP;
            //            }
            //        }
            //        else if (mon.monitorType == NextUI.BaseUI.NextUIType.Lamp || mon.monitorType == NextUI.BaseUI.NextUIType.Button)
            //        {
            //            if (mon.VARs.Count == 0)
            //            {
            //                float addvalue = ind;
            //                if (pn.Value > 160)
            //                    addvalue = ind * (-1);
            //                pn.Value = (float)(int)(new Random((int)DateTime.Now.Ticks + ind * 10).NextDouble() * 3);
            //            }
            //        }
            //        else if (mon.monitorType != NextUI.BaseUI.NextUIType.KnobDigital)
            //        {
            //            if (mon.VARs.Count == 0)
            //            {
            //                float addvalue = (float)(int)(new Random((int)DateTime.Now.Ticks + ind * 10).NextDouble() * 5);
            //                pn.Value = pn.Value + addvalue;
            //                if (pn.Value > 200)
            //                    pn.Value = 0; ;
            //            }
            //        }
            //    }
            //}
        }

        private void CustomMonitor_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void CustomMonitor_FormClosing(object sender, FormClosingEventArgs e)
        {
            Save();
        }

        private void CustomMonitor_VisibleChanged(object sender, EventArgs e)
        {
            //Save();
        }

        private void Save()
        {
            for (int i = 0; i < monitor.MonitorList.Monitor.Count; i++)
            {
                NextUI.BaseUI.BaseUI pn = (NextUI.BaseUI.BaseUI)(this.Controls[i]);
                MonitorItem mon = monitor.MonitorList.Monitor[pn.ID];

                mon.Left = pn.Left;
                mon.Top = pn.Top;
                mon.Width = pn.Width;
                mon.Height = pn.Height;
            }
            monitor.SaveMonInfo(_MonitorName); 
        }

        private void CustomMonitor_FormClosed_1(object sender, FormClosedEventArgs e)
        {
        }
    }
}
