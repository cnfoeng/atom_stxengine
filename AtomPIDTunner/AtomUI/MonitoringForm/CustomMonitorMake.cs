﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;
using System.Windows.Forms.DataVisualization.Charting;

namespace AtomUI.MonitoringForm
{
    public partial class CustomMonitorMake : Form
    {
        string _MonitorName;
        MonitorInfo monitor = new MonitorInfo();

        public CustomMonitorMake(string MonitorName)
        {
            InitializeComponent();
            _MonitorName = MonitorName;
            this.Text = "Edit: " + _MonitorName;
            monitor.LoadMonInfo(_MonitorName);
        }

        private void AnalogInputMonitor_Load(object sender, EventArgs e)
        {
            CPForm = new ControlPad();
            CPForm.Show();
            ControlMoverOrResizer.WorkType = ControlMoverOrResizer.MoveOrResize.MoveAndResize;

            int id = 0;
            foreach (MonitorItem mon in monitor.MonitorList.Monitor)
            {
                Size size = new Size(mon.Width, mon.Height);
                Point pt = new Point(mon.Left, mon.Top);
                NextUI.BaseUI.BaseUI pn = CreatePanel(mon.monitorType, pt, size, "", id);

                UIPropertySet(mon, pn);
                id++;
            }
        }
        private void UIPropertyGet(MonitorItem mon, NextUI.BaseUI.BaseUI pn)
        {
            mon.BackGroundColor = pn.BackGroundColor;
            mon.TextBackGroundColorWarning = pn.TextBackGroundColorWarning;
            mon.TextBackGroundColorError = pn.TextBackGroundColorError;
            mon.TextBackGroundColor = pn.TextBackGroundColor;
            mon.UnitTextColor = pn.UnitTextColor;
            mon.TitleTextColor = pn.TitleTextColor;
            mon.ValueTextColor = pn.ValueTextColor;
            mon.ValueScaleColor = pn.ValueScaleColor;

            mon.Range_Low = pn.StartValue;
            mon.Range_High = pn.EndValue;

            mon.Warning_High = pn.WarningValueHigh;
            mon.Warning_Low = pn.WarningValueLow;
            mon.Alarm_High = pn.ErrorValueHigh;
            mon.Alarm_Low = pn.ErrorValueLow;

            mon.Left = pn.Left;
            mon.Top = pn.Top;
            mon.Width = pn.Width;
            mon.Height = pn.Height;

            mon.Name = pn.Title;
            mon.Unit = pn.Unit;

        }

        private void UIPropertySet(MonitorItem mon, NextUI.BaseUI.BaseUI pn)
        {
            pn.BackGroundColor = mon.BackGroundColor;
            pn.TextBackGroundColorWarning = mon.TextBackGroundColorWarning;
            pn.TextBackGroundColorError = mon.TextBackGroundColorError;
            pn.TextBackGroundColor = mon.TextBackGroundColor;
            pn.UnitTextColor = mon.UnitTextColor;
            pn.TitleTextColor = mon.TitleTextColor;
            pn.ValueTextColor = mon.ValueTextColor;
            pn.ValueScaleColor = mon.ValueScaleColor;

            pn.WarningValueHigh = (int)mon.Warning_High;
            pn.WarningValueLow = (int)mon.Warning_Low;
            pn.ErrorValueHigh = (int)mon.Alarm_High;
            pn.ErrorValueLow = (int)mon.Alarm_Low;

            pn.StartValue = (int)mon.Range_Low;
            pn.EndValue = (int)mon.Range_High;

            pn.Left = mon.Left;
            pn.Top = mon.Top;
            pn.Width = mon.Width;
            pn.Height = mon.Height;

            pn.Title = mon.Name;
            pn.Unit = mon.Unit;
            pn.RePaint = true;
            pn.Invalidate();

        }

        ControlPad CPForm;
        private void AnalogInputMonitor_FormClosed(object sender, FormClosedEventArgs e)
        {
            CPForm.bClose = true;
            CPForm.Close();
            CPForm.Dispose();
        }
        private void pnBase_MouseDown(object sender, MouseEventArgs e)
        {
            //MonitorInfo.SaveMonInfo(pnBase, Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Properties.Settings.Default.MonInfoDir + "\\" + Properties.Settings.Default.MonInfoFileName);
        }

        private void pnBase_Click(object sender, EventArgs e)
        {
        }

        List<NextUI.BaseUI.BaseUI> UIControls = new List<NextUI.BaseUI.BaseUI>();

        private void pnBase_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                MonitorItemCreate((NextUI.BaseUI.NextUIType)CPForm.SelectedItem, e.Location, new Size(200, 200));
            }
        }

        private void MonitorItemCreate(NextUI.BaseUI.NextUIType UIType, Point Location, Size size)
        {
            MonitorItem item = new MonitorItem();
            item.monitorType = UIType;
            monitor.MonitorList.Monitor.Add(item);
            NextUI.BaseUI.BaseUI pn = CreatePanel(UIType, Location, size, "Test", monitor.MonitorList.Monitor.Count-1);

            UIPropertyGet(item, pn);

            pn.RePaint = true;
            pn.Invalidate();
        }

        private NextUI.BaseUI.BaseUI CreatePanel(NextUI.BaseUI.NextUIType UIType, Point Location, Size size, string Title, int id)
        {
            NextUI.BaseUI.BaseUI pn = new NextUI.BaseUI.BaseUI();
            pn.Location = Location;
            pn.BackColor = Color.PaleGoldenrod;
            pn.Size = size;
            pn.UIType = UIType;
            pn.ID = id;
            //if (UIType != NextUI.BaseUI.NextUIType.KnobDigital)
            ControlMoverOrResizer.Init(pn);
            pn.Value = 1.1f;
            pnBase.Controls.Add(pn);
            pn.ContextMenuStrip = contextMenuStrip1;
            UIControls.Add(pn);

            pn.RePaint = true;
            pn.Invalidate();

            return pn;
        }

        private void Save()
        {
            for (int i=0; i< monitor.MonitorList.Monitor.Count; i++)
            {
                UIPropertyGet(monitor.MonitorList.Monitor[i], (NextUI.BaseUI.BaseUI)UIControls[i]);
            }
            monitor.SaveMonInfo(_MonitorName);
        }

        private void saveToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Save();
            this.Close();
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void assignVariableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int ControlIndex = findControl();

            UIPropertyGet(monitor.MonitorList.Monitor[ControlIndex], (NextUI.BaseUI.BaseUI)UIControls[ControlIndex]);

            ControlSetup CSForm = new ControlSetup();
            CSForm.ControlItem = monitor.MonitorList.Monitor[ControlIndex];
            CSForm.ShowDialog();

            UIPropertySet(monitor.MonitorList.Monitor[ControlIndex], (NextUI.BaseUI.BaseUI)UIControls[ControlIndex]);
        }

        private void removeControlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int ControlIndex = findControl();

            pnBase.Controls.Remove(UIControls[ControlIndex]);
            UIControls.RemoveAt(ControlIndex);
            monitor.MonitorList.Monitor.RemoveAt(ControlIndex);
        }

        private int findControl()
        {
            int res = 0;
            for (int i = 0; i < UIControls.Count; i++)
            {
                if ((NextUI.BaseUI.BaseUI)ControlMoverOrResizer.selectedObject == UIControls[i])
                {
                    res = i;
                    break;
                }
            }
            return res;
        }
    }
}
