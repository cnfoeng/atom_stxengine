﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;

namespace AtomUI.MonitoringForm
{
    public partial class TextDialog : Form
    {

        public string name = "";
        public TextDialog()
        {
            InitializeComponent();
        }

        private void tbName_Leave(object sender, EventArgs e)
        {
            name = tbName.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (name.Length == 0)
            {
                MessageBox.Show("모니터의 이름을 입력해 주십시오.");
                return;
            }

            if (Ethercat.sysInfo.sysSettings.MonitorList.Contains(name))
            {
                MessageBox.Show("중복된 이름이 존재합니다.");
                return;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
