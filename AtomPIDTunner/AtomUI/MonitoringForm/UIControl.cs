﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtomUI.MonitoringForm
{
    public partial class UIControl : UserControl
    {

        public UIControl()
        {
            InitializeComponent();
        }

        private void UIControl_Paint(object sender, PaintEventArgs e)
        {
            using (var pen = new Pen(Color.FromArgb(50, 50, 50)))
            {
                e.Graphics.DrawLine(pen, new Point(0, 50), new Point(60, 50));
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }
    }
}
