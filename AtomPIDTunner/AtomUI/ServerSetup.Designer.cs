﻿namespace AtomUI
{
    partial class ServerSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CancelBtn = new System.Windows.Forms.Button();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.cmbConnType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlSub = new System.Windows.Forms.Panel();
            this.tbIPAddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbPort = new System.Windows.Forms.TextBox();
            this.PortLb = new System.Windows.Forms.Label();
            this.pnlSub.SuspendLayout();
            this.SuspendLayout();
            // 
            // CancelBtn
            // 
            this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CancelBtn.Location = new System.Drawing.Point(173, 190);
            this.CancelBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(91, 23);
            this.CancelBtn.TabIndex = 45;
            this.CancelBtn.Text = "Cancel";
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // SaveBtn
            // 
            this.SaveBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.SaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveBtn.Location = new System.Drawing.Point(26, 190);
            this.SaveBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(91, 23);
            this.SaveBtn.TabIndex = 44;
            this.SaveBtn.Text = "Save";
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // cmbConnType
            // 
            this.cmbConnType.FormattingEnabled = true;
            this.cmbConnType.Items.AddRange(new object[] {
            "내부 DAQ 서버 사용",
            "원격 DAQ 서버 사용"});
            this.cmbConnType.Location = new System.Drawing.Point(125, 49);
            this.cmbConnType.Name = "cmbConnType";
            this.cmbConnType.Size = new System.Drawing.Size(121, 20);
            this.cmbConnType.TabIndex = 46;
            this.cmbConnType.SelectedIndexChanged += new System.EventHandler(this.cmbConnType_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(23, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 30);
            this.label2.TabIndex = 47;
            this.label2.Text = "DAQ 유형 선택";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlSub
            // 
            this.pnlSub.Controls.Add(this.tbIPAddress);
            this.pnlSub.Controls.Add(this.label1);
            this.pnlSub.Controls.Add(this.tbPort);
            this.pnlSub.Controls.Add(this.PortLb);
            this.pnlSub.Location = new System.Drawing.Point(6, 76);
            this.pnlSub.Name = "pnlSub";
            this.pnlSub.Size = new System.Drawing.Size(283, 100);
            this.pnlSub.TabIndex = 48;
            // 
            // tbIPAddress
            // 
            this.tbIPAddress.Location = new System.Drawing.Point(118, 22);
            this.tbIPAddress.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbIPAddress.Name = "tbIPAddress";
            this.tbIPAddress.Size = new System.Drawing.Size(141, 21);
            this.tbIPAddress.TabIndex = 44;
            this.tbIPAddress.Text = "127.0.0.1";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(18, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 30);
            this.label1.TabIndex = 43;
            this.label1.Text = "IP Adress";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbPort
            // 
            this.tbPort.Location = new System.Drawing.Point(118, 61);
            this.tbPort.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(141, 21);
            this.tbPort.TabIndex = 42;
            this.tbPort.Text = "1555";
            // 
            // PortLb
            // 
            this.PortLb.Location = new System.Drawing.Point(18, 55);
            this.PortLb.Name = "PortLb";
            this.PortLb.Size = new System.Drawing.Size(94, 30);
            this.PortLb.TabIndex = 41;
            this.PortLb.Text = "Port";
            this.PortLb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ServerSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(290, 233);
            this.Controls.Add(this.pnlSub);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbConnType);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.SaveBtn);
            this.Name = "ServerSetup";
            this.Text = "DAQ Setup";
            this.Load += new System.EventHandler(this.ServerSetup_Load);
            this.pnlSub.ResumeLayout(false);
            this.pnlSub.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.ComboBox cmbConnType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlSub;
        private System.Windows.Forms.TextBox tbIPAddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbPort;
        private System.Windows.Forms.Label PortLb;
    }
}