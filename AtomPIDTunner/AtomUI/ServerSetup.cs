﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;

namespace AtomUI
{
    public partial class ServerSetup : Form
    {
        public ServerSetup()
        {
            InitializeComponent();
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            Ethercat.sysInfo.ConnectionInfo.RemoteAddress = tbIPAddress.Text;   //  COM channel
            Ethercat.sysInfo.ConnectionInfo.ConnType = cmbConnType.SelectedIndex;   //  COM channel
            Ethercat.sysInfo.ConnectionInfo.CommPort = int.Parse(tbPort.Text);
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {

        }

        private void ServerSetup_Load(object sender, EventArgs e)
        {
            tbIPAddress.Text = Ethercat.sysInfo.ConnectionInfo.RemoteAddress;
            tbPort.Text = Ethercat.sysInfo.ConnectionInfo.CommPort.ToString();
            cmbConnType.SelectedIndex = Ethercat.sysInfo.ConnectionInfo.ConnType;
        }

        private void cmbConnType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbConnType.SelectedIndex == 0)
            {
                pnlSub.Visible = false;
            }
            else
            {
                pnlSub.Visible = true;
            }
        }
    }
}
