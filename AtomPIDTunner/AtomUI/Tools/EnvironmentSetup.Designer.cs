﻿namespace AtomUI.Tools
{
    partial class EnvironmentSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.tbDefaultDirectory = new System.Windows.Forms.TextBox();
            this.btnDefaultDirectory = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.chkUseDebug = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "설정파일 기본 디렉토리";
            // 
            // tbDefaultDirectory
            // 
            this.tbDefaultDirectory.Location = new System.Drawing.Point(162, 20);
            this.tbDefaultDirectory.Name = "tbDefaultDirectory";
            this.tbDefaultDirectory.Size = new System.Drawing.Size(402, 21);
            this.tbDefaultDirectory.TabIndex = 1;
            // 
            // btnDefaultDirectory
            // 
            this.btnDefaultDirectory.Location = new System.Drawing.Point(589, 18);
            this.btnDefaultDirectory.Name = "btnDefaultDirectory";
            this.btnDefaultDirectory.Size = new System.Drawing.Size(75, 23);
            this.btnDefaultDirectory.TabIndex = 2;
            this.btnDefaultDirectory.Text = "변경...";
            this.btnDefaultDirectory.UseVisualStyleBackColor = true;
            this.btnDefaultDirectory.Click += new System.EventHandler(this.btnDefaultDirectory_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(489, 226);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "저장";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(589, 226);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "취소";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // chkUseDebug
            // 
            this.chkUseDebug.AutoSize = true;
            this.chkUseDebug.Location = new System.Drawing.Point(14, 75);
            this.chkUseDebug.Name = "chkUseDebug";
            this.chkUseDebug.Size = new System.Drawing.Size(86, 16);
            this.chkUseDebug.TabIndex = 235;
            this.chkUseDebug.Text = "Use Debug";
            this.chkUseDebug.UseVisualStyleBackColor = true;
            this.chkUseDebug.CheckedChanged += new System.EventHandler(this.chkUseDebug_CheckedChanged);
            // 
            // EnvironmentSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(687, 261);
            this.Controls.Add(this.chkUseDebug);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnDefaultDirectory);
            this.Controls.Add(this.tbDefaultDirectory);
            this.Controls.Add(this.label1);
            this.Name = "EnvironmentSetup";
            this.Text = "환경설정";
            this.Load += new System.EventHandler(this.EnvironmentSetup_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbDefaultDirectory;
        private System.Windows.Forms.Button btnDefaultDirectory;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.CheckBox chkUseDebug;
    }
}