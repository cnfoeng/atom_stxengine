﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AtomUI.Class;
using System.IO;

namespace AtomUI.Tools
{
    public partial class EnvironmentSetup : Form
    {
        public EnvironmentSetup()
        {
            InitializeComponent();
            chkUseDebug.Checked = CommonFunction.bUseDebug;
        }

        private void EnvironmentSetup_Load(object sender, EventArgs e)
        {
            tbDefaultDirectory.Text = Ethercat.sysInfo.sysSettings.DefaultDirectory;
        }

        private void btnDefaultDirectory_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.Description = "Config 디렉토리가 존재하는 폴더를 선택해 주십시오.";
            folderBrowserDialog1.SelectedPath = tbDefaultDirectory.Text;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                bool founded = false;
                string[] Dir_List = Directory.GetDirectories(folderBrowserDialog1.SelectedPath);
                foreach (string dir in Dir_List)
                {
                    if (dir == "Config")
                    {
                        founded = true;
                        break;
                    }
                }
                if (founded)
                    tbDefaultDirectory.Text = folderBrowserDialog1.SelectedPath;
                else
                {
                    MessageBox.Show("Config Directory가 존재하지 않습니다.");
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Ethercat.sysInfo.sysSettings.DefaultDirectory = tbDefaultDirectory.Text;
            Ethercat.sysInfo.Save(Ethercat.sysInfo.sysSettings.DefaultDirectory + "\\" + Ethercat.sysInfo.sysSettings.SysInfoDir + "\\" + Ethercat.sysInfo.sysSettings.SysInfoFileName);
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chkUseDebug_CheckedChanged(object sender, EventArgs e)
        {
            CommonFunction.bUseDebug = chkUseDebug.Checked;
        }
    }
}
