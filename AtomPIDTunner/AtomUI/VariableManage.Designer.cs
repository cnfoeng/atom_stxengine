﻿namespace AtomUI
{
    partial class VariableManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PortLb = new System.Windows.Forms.Label();
            this.btnQuery = new System.Windows.Forms.Button();
            this.cbBuffer = new System.Windows.Forms.ComboBox();
            this.tbControl = new System.Windows.Forms.RichTextBox();
            this.btnAutoQuery = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.cbVariable = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // PortLb
            // 
            this.PortLb.Location = new System.Drawing.Point(26, 21);
            this.PortLb.Name = "PortLb";
            this.PortLb.Size = new System.Drawing.Size(107, 38);
            this.PortLb.TabIndex = 58;
            this.PortLb.Text = "Variable";
            this.PortLb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(712, 30);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(134, 25);
            this.btnQuery.TabIndex = 64;
            this.btnQuery.Text = "조회";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // cbBuffer
            // 
            this.cbBuffer.FormattingEnabled = true;
            this.cbBuffer.Items.AddRange(new object[] {
            "Global",
            "Buffer 0",
            "Buffer 1",
            "Buffer 2",
            "Buffer 3",
            "Buffer 4",
            "Buffer 5",
            "Buffer 6",
            "Buffer 7",
            "Buffer 8",
            "Buffer 9"});
            this.cbBuffer.Location = new System.Drawing.Point(92, 30);
            this.cbBuffer.Name = "cbBuffer";
            this.cbBuffer.Size = new System.Drawing.Size(167, 23);
            this.cbBuffer.TabIndex = 65;
            // 
            // tbControl
            // 
            this.tbControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbControl.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbControl.Location = new System.Drawing.Point(12, 75);
            this.tbControl.Name = "tbControl";
            this.tbControl.Size = new System.Drawing.Size(995, 267);
            this.tbControl.TabIndex = 67;
            this.tbControl.Text = "";
            this.tbControl.TextChanged += new System.EventHandler(this.tbControl_TextChanged);
            // 
            // btnAutoQuery
            // 
            this.btnAutoQuery.Location = new System.Drawing.Point(864, 30);
            this.btnAutoQuery.Name = "btnAutoQuery";
            this.btnAutoQuery.Size = new System.Drawing.Size(134, 25);
            this.btnAutoQuery.TabIndex = 68;
            this.btnAutoQuery.Text = "자동조회";
            this.btnAutoQuery.UseVisualStyleBackColor = true;
            this.btnAutoQuery.Click += new System.EventHandler(this.btnAutoQuery_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // cbVariable
            // 
            this.cbVariable.FormattingEnabled = true;
            this.cbVariable.Items.AddRange(new object[] {
            "g_ControlMode",
            "g_DriveManual_Mode",
            "g_PC_Read_Data",
            "g_PC_Write_Data",
            "g_rPC_Write_Data",
            "g_PC_Process_Data_TqAlphaProfile",
            "g_PC_Process_Data_SpAlphaProfile",
            "g_PC_Process_Data_TqSpProfile",
            "g_PC_Process_Data_SpTqProfile",
            "g_Dyno_PidControl_StateMachine",
            "g_Dyno_PID_Process_ValList",
            "g_Eng_PidControl_StateMachine",
            "g_Eng_PID_Process_ValList"});
            this.cbVariable.Location = new System.Drawing.Point(265, 30);
            this.cbVariable.Name = "cbVariable";
            this.cbVariable.Size = new System.Drawing.Size(427, 23);
            this.cbVariable.TabIndex = 65;
            // 
            // VariableManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 351);
            this.Controls.Add(this.btnAutoQuery);
            this.Controls.Add(this.tbControl);
            this.Controls.Add(this.cbVariable);
            this.Controls.Add(this.cbBuffer);
            this.Controls.Add(this.btnQuery);
            this.Controls.Add(this.PortLb);
            this.Name = "VariableManage";
            this.Text = "VariableManage";
            this.Load += new System.EventHandler(this.VariableManage_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label PortLb;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.ComboBox cbBuffer;
        private System.Windows.Forms.RichTextBox tbControl;
        private System.Windows.Forms.Button btnAutoQuery;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ComboBox cbVariable;
    }
}