﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ACS.SPiiPlusNET;

namespace AtomUI
{
    public partial class VariableManage : Form
    {
        public Api Ch; //  For communcating between the UMD and the Host Application. All communication commands are methods within the AsyncChannel Class.

        public VariableManage()
        {
            InitializeComponent();
        }

        private void btnAutoQuery_Click(object sender, EventArgs e)
        {
            if (btnAutoQuery.Text == "자동조회")
            {
                cbBuffer.Enabled = false;
                cbVariable.Enabled = false;
                btnAutoQuery.Text = "자동조회 멈춤";
                timer1.Start();
            }
            else
            {
                timer1.Stop();
                cbBuffer.Enabled = true;
                cbVariable.Enabled = true;
                btnAutoQuery.Text = "자동조회";
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            ReadVariable();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            ReadVariable();
        }

        private void ReadVariable()
        {
            string Str;
            ProgramBuffer buffer1 = ProgramBuffer.ACSC_NONE;

            if (cbVariable.Text.Length == 0)
                return;

            switch (cbBuffer.SelectedIndex)
            {
                case 0:
                    buffer1 = ProgramBuffer.ACSC_NONE;
                    break;
                case 1:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_0;
                    break;
                case 2:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_1;
                    break;
                case 3:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_2;
                    break;
                case 4:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_3;
                    break;
                case 5:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_4;
                    break;
                case 6:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_5;
                    break;
                case 7:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_6;
                    break;
                case 8:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_7;
                    break;
                case 9:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_8;
                    break;
                case 10:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_9;
                    break;
            }
            Str = "";
            Object Result;
            try
            {
                Result = Ch.ReadVariable(cbVariable.Text, buffer1, -1, -1, -1, -1);
            }
            catch
            {
                MessageBox.Show("정의되지 않은 변수입니다.");
                timer1.Stop();
                return;
            }

            Type ResultType = Result.GetType();
            if (ResultType == typeof(int))
            {
                Str = Str + ((int)Result).ToString() + "\n";
            }
            else if (ResultType == typeof(double))
            {
                Str = Str + ((double)Result).ToString() + "\n";
            }
            else if (ResultType == typeof(Double[]))
            {
                Str = Str + VectorToString(Result, true);
            }
            else if (ResultType == typeof(int[]))
            {
                Str = Str + VectorToString(Result, false);
            }
            else if (ResultType == typeof(Double[,]))
            {
                Str = Str + MatrixToString(Result, true);
            }
            else //(ResultType == typeof(int[][]))
            {
                Str = Str + MatrixToString(Result, false);
            }

            tbControl.Text = Str;
        }

        private String MatrixToString(object Matrix, bool doubleType)
        {
            String Str = "";
            int To1, To2;

            To1 = doubleType ? ((double[,])Matrix).GetUpperBound(0) : ((int[,])Matrix).GetUpperBound(0);
            To2 = doubleType ? ((double[,])Matrix).GetUpperBound(1) : ((int[,])Matrix).GetUpperBound(1);
            for (int i = 0; i <= To1; i++)
            {
                for (int j = 0; j <= To2; j++)
                {
                    Str = Str + (doubleType ? ((double[,])Matrix)[i, j] : ((int[,])Matrix)[i, j]) + " ";
                }
                Str = Str + "\n";
            }
            return Str;
        }


        private String VectorToString(object Vector, bool doubleType)
        {
            String Str = "";
            int To;
            To = doubleType ? ((double[])Vector).GetUpperBound(0) : ((int[])Vector).GetUpperBound(0);
            for (int i = 0; i <= To; i++)
            {
                if (i - i / 16 * 16 == 0)
                    Str = Str + string.Format("{0:X4}", i) + " : " + string.Format("{0,9:###,##0}", (doubleType ? ((double[])Vector)[i] : ((int[])Vector)[i])) + " ";
                else if (i - i / 16 * 16 == 15)
                    Str = Str + string.Format("{0,9:###,##0}", (doubleType ? ((double[])Vector)[i] : ((int[])Vector)[i])) + "\n";
                else
                    Str = Str + string.Format("{0,9:###,##0}", (doubleType ? ((double[])Vector)[i] : ((int[])Vector)[i])) + " ";
            }
            return Str;
        }

        private void VariableManage_Load(object sender, EventArgs e)
        {
            cbBuffer.SelectedIndex = 0;
            cbVariable.SelectedIndex = 0;
        }

        private void tbControl_TextChanged(object sender, EventArgs e)
        {

        }


    }
}
