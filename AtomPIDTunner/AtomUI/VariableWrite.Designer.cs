﻿namespace AtomUI
{
    partial class VariableWrite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbIndex4 = new System.Windows.Forms.TextBox();
            this.tbIndex3 = new System.Windows.Forms.TextBox();
            this.tbIndex2 = new System.Windows.Forms.TextBox();
            this.tbIndex1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbValue2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbBuffer2 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tbVariable2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbIndex4
            // 
            this.tbIndex4.Location = new System.Drawing.Point(635, 70);
            this.tbIndex4.Name = "tbIndex4";
            this.tbIndex4.Size = new System.Drawing.Size(75, 25);
            this.tbIndex4.TabIndex = 90;
            this.tbIndex4.Text = "-1";
            // 
            // tbIndex3
            // 
            this.tbIndex3.Location = new System.Drawing.Point(536, 70);
            this.tbIndex3.Name = "tbIndex3";
            this.tbIndex3.Size = new System.Drawing.Size(75, 25);
            this.tbIndex3.TabIndex = 89;
            this.tbIndex3.Text = "-1";
            // 
            // tbIndex2
            // 
            this.tbIndex2.Location = new System.Drawing.Point(443, 70);
            this.tbIndex2.Name = "tbIndex2";
            this.tbIndex2.Size = new System.Drawing.Size(75, 25);
            this.tbIndex2.TabIndex = 88;
            this.tbIndex2.Text = "-1";
            // 
            // tbIndex1
            // 
            this.tbIndex1.Location = new System.Drawing.Point(351, 70);
            this.tbIndex1.Name = "tbIndex1";
            this.tbIndex1.Size = new System.Drawing.Size(75, 25);
            this.tbIndex1.TabIndex = 86;
            this.tbIndex1.Text = "-1";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(285, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 38);
            this.label3.TabIndex = 87;
            this.label3.Tag = "Index";
            this.label3.Text = "Index";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbValue2
            // 
            this.tbValue2.Location = new System.Drawing.Point(78, 70);
            this.tbValue2.Name = "tbValue2";
            this.tbValue2.Size = new System.Drawing.Size(186, 25);
            this.tbValue2.TabIndex = 85;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 38);
            this.label2.TabIndex = 84;
            this.label2.Text = "Value";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbBuffer2
            // 
            this.cbBuffer2.FormattingEnabled = true;
            this.cbBuffer2.Items.AddRange(new object[] {
            "Global",
            "Buffer 0",
            "Buffer 1",
            "Buffer 2",
            "Buffer 3",
            "Buffer 4",
            "Buffer 5",
            "Buffer 6",
            "Buffer 7",
            "Buffer 8",
            "Buffer 9"});
            this.cbBuffer2.Location = new System.Drawing.Point(78, 18);
            this.cbBuffer2.Name = "cbBuffer2";
            this.cbBuffer2.Size = new System.Drawing.Size(167, 23);
            this.cbBuffer2.TabIndex = 83;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(722, 18);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(134, 77);
            this.button1.TabIndex = 82;
            this.button1.Text = "실행";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbVariable2
            // 
            this.tbVariable2.Location = new System.Drawing.Point(251, 18);
            this.tbVariable2.Name = "tbVariable2";
            this.tbVariable2.Size = new System.Drawing.Size(459, 25);
            this.tbVariable2.TabIndex = 81;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 38);
            this.label1.TabIndex = 80;
            this.label1.Text = "Variable";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // VariableWrite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 114);
            this.Controls.Add(this.tbIndex4);
            this.Controls.Add(this.tbIndex3);
            this.Controls.Add(this.tbIndex2);
            this.Controls.Add(this.tbIndex1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbValue2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbBuffer2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tbVariable2);
            this.Controls.Add(this.label1);
            this.Name = "VariableWrite";
            this.Text = "Variable Write";
            this.Load += new System.EventHandler(this.VariableWrite_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbIndex4;
        private System.Windows.Forms.TextBox tbIndex3;
        private System.Windows.Forms.TextBox tbIndex2;
        private System.Windows.Forms.TextBox tbIndex1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbValue2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbBuffer2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbVariable2;
        private System.Windows.Forms.Label label1;
    }
}