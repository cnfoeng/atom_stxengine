﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ACS.SPiiPlusNET;

namespace AtomUI
{
    public partial class VariableWrite : Form
    {
        public Api Ch; //  For communcating between the UMD and the Host Application. All communication commands are methods within the AsyncChannel Class.
        
        public VariableWrite()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProgramBuffer buffer1 = ProgramBuffer.ACSC_NONE;

            if (tbVariable2.Text.Length == 0)
                return;

            switch (cbBuffer2.SelectedIndex)
            {
                case 0:
                    buffer1 = ProgramBuffer.ACSC_NONE;
                    break;
                case 1:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_0;
                    break;
                case 2:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_1;
                    break;
                case 3:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_2;
                    break;
                case 4:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_3;
                    break;
                case 5:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_4;
                    break;
                case 6:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_5;
                    break;
                case 7:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_6;
                    break;
                case 8:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_7;
                    break;
                case 9:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_8;
                    break;
                case 10:
                    buffer1 = ProgramBuffer.ACSC_BUFFER_9;
                    break;
            }

            double Value = double.Parse(tbValue2.Text);

            try
            {
                Ch.WriteVariable(Value, tbVariable2.Text, buffer1, int.Parse(tbIndex1.Text), int.Parse(tbIndex2.Text), int.Parse(tbIndex3.Text), int.Parse(tbIndex4.Text));
            }
            catch
            {
                MessageBox.Show("Undefined Variable");
            }
        }

        private void VariableWrite_Load(object sender, EventArgs e)
        {
            cbBuffer2.SelectedIndex = 0;
        }
    }
}
