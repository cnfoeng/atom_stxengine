﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Collections;
using System.Runtime.InteropServices;
using System.Timers;
using System.IO;
using ACS.SPiiPlusNET;
using AtomUI.Class;

namespace DAQ_ACS
{
    public class ACSCom
    {
        public double testvalue;

        public static Api Ch;
        private static System.Timers.Timer aTimer1;
        private static System.Timers.Timer aTimer2;

        public static string sFTime;
        public static bool IsConnected = false;


        private int mATTimer = 50;


        public ACSCom()
        {

        }

        private void ErorMsg(COMException Ex)
        {
            string Str = "Error from " + Ex.Source + "\n\r";
            Str = Str + Ex.Message + "\n\r";
            Str = Str + "HRESULT:" + String.Format("0x{0:X}", (Ex.ErrorCode));
            Console.Write(Str);
        }
        public void Stop_ACSCom()
        {
            if (aTimer1 != null)
            {
                aTimer1.Enabled = false;
                aTimer1.Stop();
                aTimer1.Close();
            }

            if (aTimer2 != null)
            {
                aTimer2.Enabled = false;
                aTimer2.Stop();
                aTimer2.Close();
            }

            try
            {
                if (Ch != null)
                {
                    Ch.CloseComm();
                    IsConnected = false;
                }
            }
            catch (System.NullReferenceException)
            {
                Console.Write("비정상 종료가 발생하였습니다.");
            }
        }

        public void Start_ACSCom()
        {
            try
            {
                // Create new object of class Channel
                // Type Channel is defined in SPiiPlusCOM650 Type Library
                Ch = new Api();
                object pWait = 0;
                string cIP = "10.0.0.100";
                int ComType = 0;

                int Protocol = (int)EthernetCommOption.ACSC_SOCKET_STREAM_PORT;
                // Open ethernet communuication.
                // RemoteAddress.Text defines the controller's TCP/IP address.
                // Protocol is TCP/IP in case of network connection, and UDP in case of point-to-point connection.
                // string a = Ethercat.sysInfo.HardWareInfo[0].
                foreach (acsConnectionInfo conInfo in Ethercat.sysInfo.HardWareInfo)
                {
                    if (conInfo.HWType == HardWareType.ACS)
                    {
                        cIP = conInfo.RemoteAddress;
                        ComType = conInfo.ComType;
                    }
                }

                if (ComType == 3 || CommonFunction.bUseDebug)
                {
                    Ch.OpenCommSimulator();
                } else {
                    Ch.OpenCommEthernet(cIP, Protocol);

                }



                if (Ch.IsConnected)
                {
                    Console.Write("ACS 연결성공\n");
                    IsConnected = Ch.IsConnected;

                    aTimer1 = new System.Timers.Timer(mATTimer);

                    aTimer1.Elapsed += OnRun_AI;
                    aTimer1.Enabled = true;

                    //aTimer2 = new System.Timers.Timer(mATTimer);

                    //aTimer2.Elapsed += OnRun_PC;
                    //aTimer2.Enabled = true;

                }
                else
                {
                    Console.Write("연결실패입니다.");
                }

            }
            catch (COMException Ex)
            {
                ErorMsg(Ex);

            }
            catch (ACS.SPiiPlusNET.ACSException)
            {
                Console.Write("ATOM 초기화가 실패하였습니다. 장비연결을 다시 확인해 주세요");
            }

        }

        private void OnRun_PC(Object source, ElapsedEventArgs e)
        {
            Ethercat.GetACS_PCData();
        }


        private void OnRun_AI(Object source, ElapsedEventArgs e)
        {
            try
            {
                object Result;
                Result = Ch.ReadVariable("g_eCAT_AI_Data", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);

                var array = (Result as IEnumerable).Cast<object>().Select(x => x.ToString()).ToArray();
                DoRead_AI(array);
            }
            catch
            {

            }
        }
        private void OnRun_DI(Object source, ElapsedEventArgs e)
        {
            try
            {
                object Result;
                Result = Ch.ReadVariable("g_eCAT_DI_Data", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);

                var array = (Result as IEnumerable).Cast<object>().Select(x => x.ToString()).ToArray();
                DoRead_DI(array);
            }
            catch
            {

            }
        }
        private void DoRead_AI(String[] obj)
        {
            try
            {
                for (int i = 0; i < DaqData.sysVarInfo.AI_Variables.Count; i++)
                {

                    DaqData.sysVarInfo.AI_Variables[i].IOValue = int.Parse(obj[i]);
                    sFTime = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");

                }
            }
            catch
            {

            }
        }

        private void DoRead_DI(String[] obj)
        {
            try
            {
                for (int i = 0; i < DaqData.sysVarInfo.DI_Variables.Count; i++)
                {
                    DaqData.sysVarInfo.DI_Variables[i].IOValue = int.Parse(obj[i]);
                    sFTime = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");


                }
            }
            catch
            {

            }
        }
    }



}
