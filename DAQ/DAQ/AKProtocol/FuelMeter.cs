﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AtomUI.Class;
using System.IO.Ports;
using System.Windows.Forms;
using System.Threading;

namespace DAQ.SerialComm
{
    public class FuelMeter
    {
        public enum FMStatus
        {
            SREM = 1,
            SMAN = 2,
            SRES = 3,
            STBY = 4,
            SMES = 5,
            SINT = 6,
            SMZV = 7,
            SMGV = 8,
            SATK = 9,
            SKCH = 10,
            SAVA = 11,
            SAVZ = 12,
            SBVZ = 13,
            SBVA = 14
        }

        public FMStatus ST_Remote = FMStatus.SMAN;
        public FMStatus ST_Function = FMStatus.STBY;
        public FMStatus ST_BYPASS = FMStatus.SAVA;
        public FMStatus ST_FUEL = FMStatus.SBVA;

        public bool EMPASetting = false;
        public bool MeasureStart = false;
        public int MeasureTime = 30;

        private void StatusChange(string StatusStr)
        {
            for (int ind = 1; ind < 15; ind++)
            {
                if (((FMStatus)ind).ToString() == StatusStr)
                {
                    if ((FMStatus)ind == FMStatus.SREM || (FMStatus)ind == FMStatus.SMAN)
                    {
                        ST_Remote = (FMStatus)ind;
                    }
                    else if ((FMStatus)ind == FMStatus.SAVA || (FMStatus)ind == FMStatus.SAVZ)
                    {
                        ST_FUEL = (FMStatus)ind;
                    }
                    else if ((FMStatus)ind == FMStatus.SBVA || (FMStatus)ind == FMStatus.SBVZ)
                    {
                        ST_BYPASS = (FMStatus)ind;
                    }
                    else
                    {
                        ST_Function = (FMStatus)ind;
                        if (ST_Function == FMStatus.SMZV)
                                MeasureStart = true;
                    }
                }
            }
        }

        public static bool IsConnected = false;

        public List<string> SM_Command_List = new List<string>();

        string ERROR_COMMAND = "";
        string SEND_COMMAND = "";
        int ERROR_CODE = 0;

        byte[] CMD_SMAN = new byte[] { 0x02, 0x20, 0x53, 0x4D, 0x41, 0x4E, 0x03 };
        byte[] CMD_SRES = new byte[] { 0x02, 0x20, 0x53, 0x52, 0x45, 0x53, 0x03 };
        byte[] CMD_SREM = new byte[] { 0x02, 0x20, 0x53, 0x52, 0x45, 0x4D, 0x03 };
        byte[] CMD_STBY = new byte[] { 0x02, 0x20, 0x53, 0x54, 0x42, 0x59, 0x03 };
        byte[] CMD_SAVA = new byte[] { 0x02, 0x20, 0x53, 0x41, 0x56, 0x41, 0x03 };
        byte[] CMD_SAVZ = new byte[] { 0x02, 0x20, 0x53, 0x41, 0x56, 0x5A, 0x03 };
        byte[] CMD_SMZV = new byte[] { 0x02, 0x20, 0x53, 0x4D, 0x5A, 0x56, 0x03 };
        byte[] CMD_SBVA = new byte[] { 0x02, 0x20, 0x53, 0x42, 0x56, 0x41, 0x03 };
        byte[] CMD_SBVZ = new byte[] { 0x02, 0x20, 0x53, 0x42, 0x56, 0x5A, 0x03 };
        byte[] CMD_SMGV = new byte[] { 0x02, 0x20, 0x53, 0x4D, 0x47, 0x56, 0x03 };

        byte[] CMD_EMPA = new byte[] { 0x02, 0x20, 0x45, 0x4D, 0x50, 0x41, 0x20, 0x31, 0x20, 0x33, 0x30, 0x20, 0x35, 0x20, 0x03 };

        byte[] CMD_ASTZ = new byte[] { 0x02, 0x20, 0x41, 0x53, 0x54, 0x5A, 0x03 };
        byte[] CMD_ASTF = new byte[] { 0x02, 0x20, 0x41, 0x53, 0x54, 0x46, 0x03 };
        byte[] CMD_AWRT = new byte[] { 0x02, 0x20, 0x41, 0x57, 0x52, 0x54, 0x03 };
        byte[] CMD_AERG = new byte[] { 0x02, 0x20, 0x41, 0x45, 0x52, 0x47, 0x03 };

        byte[] FullBuffer = new byte[50000];
        int ReadByte = 0;

        private SerialPort port;
        String PortName;

        public FuelMeter(String PortName)
        {
            this.PortName = PortName;
            port = new SerialPort(PortName, 9600, Parity.None, 8, StopBits.One);
            if (!port.IsOpen)
            {
                port.Open();
            }
            port.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);

            IsConnected = true;
        }

        ~FuelMeter()
        {
        }

        public void Stop()
        {
            IsConnected = false;
            SM_Command_List.Clear();
            port.Close();
            port.DataReceived -= new SerialDataReceivedEventHandler(port_DataReceived);
        }

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                if (port.IsOpen)
                {
                    byte[] ReceivedPacket = null;
                    int count = port.BytesToRead;
                    byte[] NewBuffer = new byte[count];
                    if (count > 0)
                    {
                        port.Read(NewBuffer, 0, count);

                        Buffer.BlockCopy(NewBuffer, 0, FullBuffer, ReadByte, count);
                        ReadByte += count;
                        String strData = String.Format("Receive : {0} , {1} : {2}", count, ReadByte, BitConverter.ToString(NewBuffer, 0));
                        CommonFunction.LogWrite(LogType.Info, DateTime.Now, strData);

                        int end = -1;
                        int bytelen = 0;

                        for (int ind = 0; ind < ReadByte; ind++)
                        {
                            if ((FullBuffer.Length >= ind + 7) && (FullBuffer[ind] == 0x02)) // <STX>
                            {
                                for (int lastind = ind + 1; lastind < ReadByte; lastind++)
                                {
                                    if (FullBuffer[lastind] == 0x03)
                                    {
                                        bytelen = lastind - ind - 1;
                                        ReceivedPacket = new byte[bytelen];
                                        Buffer.BlockCopy(FullBuffer, ind + 1, ReceivedPacket, 0, bytelen);
                                        ind = lastind + 1;
                                        end = ind;
                                        break;
                                    }
                                }
                            }
                        }
                        if (end < 0)
                        { //ReadByte = 0; 
                        }
                        else
                        {
                            if (end > 0)
                            {
                                if (ReceivedPacket.Length > 0)
                                {
                                    string[] ReceivedCommand = Encoding.Default.GetString(ReceivedPacket).Split(' ');
                                    System.Diagnostics.Debug.Print(ReceivedCommand[1]);
                                    ERROR_CODE = int.Parse(ReceivedCommand[2]);
                                    if (ERROR_CODE > 0)
                                        ERROR_COMMAND = ReceivedCommand[1];
                                    else
                                    {
                                        if (SEND_COMMAND == ReceivedCommand[1])
                                            SEND_COMMAND = "";
                                    }
                                    switch (ReceivedCommand[1])
                                    {
                                        // DaqData.sysVarInfo.MEXA9000_Variables[i].IOValue = double.Parse(msg[i]);
                                        case "EMPA":
                                            if (ERROR_CODE == 0)
                                            {
                                                EMPASetting = true;
                                            }
                                            break;
                                        case "SREM":
                                            break;
                                        case "AERG":
                                            int MeasureCount = int.Parse(ReceivedCommand[3]);
                                            DaqData.sysVarInfo.FuelMeter_Variables[0].IOValue = double.Parse(ReceivedCommand[4]);
                                            DaqData.sysVarInfo.FuelMeter_Variables[1].IOValue = double.Parse(ReceivedCommand[5]);
                                            DaqData.sysVarInfo.FuelMeter_Variables[2].IOValue = double.Parse(ReceivedCommand[6]);
                                            MeasureStart = false;

                                            break;
                                        case "ASTZ":
                                            for (int indx = 3; indx < ReceivedCommand.Length; indx++)
                                            {
                                                if (ReceivedCommand[indx] != "")
                                                {
                                                    StatusChange(ReceivedCommand[indx]);
                                                }
                                            }
                                             break;
                                        default:
                                            break;
                                    }
                                }
                                ReadByte = ReadByte - end;
                                if (ReadByte > 0)
                                {
                                    Buffer.BlockCopy(FullBuffer, end, FullBuffer, 0, ReadByte);
                                }
                            }
                        }
                    }
                }
                else
                {
                    port.DataReceived -= new SerialDataReceivedEventHandler(port_DataReceived);
                }
            }
            catch (Exception ex)
            {
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "   sp_DataReceived Error");
            }
        }

        public void Start_CommandSend()
        {
            while (IsConnected)
            {
                if (SM_Command_List.Count > 0)
                {
                    string Command = "";

                    lock (SM_Command_List)
                    {
                        Command = SM_Command_List[0];
                        SM_Command_List.RemoveAt(0);

                        if (Command.Contains(" "))
                        {
                            string[] Temp = Command.Split(' ');
                            byte[] CMD_EMPA1 = new byte[] { 0x02, 0x20, 0x45, 0x4D, 0x50, 0x41, 0x20, 0x31, 0x20 };
                            byte[] CMD_EMPA2 = new byte[] { 0x20, 0x35, 0x20, 0x03 };
                            Command = Temp[0];
                            MeasureTime = int.Parse(Temp[1]);
                            byte[] Value = Encoding.Default.GetBytes(int.Parse(Temp[1]).ToString());
                            byte[] CMD_EMPA_TEMP = new Byte[13+Value.Length];
                            for (int i=0;i< CMD_EMPA1.Length; i++)
                            {
                                CMD_EMPA_TEMP[i] = CMD_EMPA1[i];
                            }
                            for (int i = 0; i < Value.Length; i++)
                            {
                                CMD_EMPA_TEMP[i + 9] = Value[i];
                            }
                            for (int i = 0; i < CMD_EMPA2.Length; i++)
                            {
                                CMD_EMPA_TEMP[i + CMD_EMPA1.Length + Value.Length] = CMD_EMPA2[i];
                            }
                            CMD_EMPA = CMD_EMPA_TEMP;
                        }
                    }

                    switch (Command)
                    {
                        case "SMAN":
                        case "SRES":
                        case "SREM":
                        case "SAVA":
                        case "SAVZ":
                        case "SBVA":
                        case "SBVZ":
                        case "STBY":
                        case "EMPA":
                        case "ASTZ":
                        case "ASTF":
                        case "AERG":
                            SendComm(Command);
                            break;
                        case "SMZV":
                            SendComm(Command);
                            MeasureStart = true;
                            break;
                    }
                }
                else
                {
                    if (SEND_COMMAND == "")
                    {
                        if (!EMPASetting)
                        {
                            SendComm("EMPA");
                        } else
                        {
                            if (MeasureStart)
                            {
                                if (ST_Function == FMStatus.STBY || ST_Function == FMStatus.SMES)
                                {
                                    SendComm("AERG");
                                }
                                else
                                {
                                    SendComm("ASTZ");
                                }
                            } else
                            {
                                SendComm("ASTZ");
                            }
                        }
                    }
                }
                Thread.Sleep(1000);
            }
        }

        public bool WaitStatus(string Status, int TimeOut)
        {
            int Time = TimeOut * 10;
            do
            {
                Thread.Sleep(100);
                Time--;
                Application.DoEvents();
            } while (ST_Remote.ToString() == Status || ST_Function.ToString() == Status || Time <= 0);

            if (Time <= 0)
                return false;
            else
                return true;
        }

        public void SendComm(string cmd)
        {
            try
            {
                byte[] data = CMD_ASTZ;

                switch (cmd)
                {
                    case "SMAN":
                        data = CMD_SMAN;
                        break;
                    case "SRES":
                        data = CMD_SRES;
                        break;
                    case "SREM":
                        data = CMD_SREM;
                        break;
                    case "SAVA":
                        data = CMD_SAVA;
                        break;
                    case "SAVZ":
                        data = CMD_SAVZ;
                        break;
                    case "SBVA":
                        data = CMD_SBVA;
                        break;
                    case "SBVZ":
                        data = CMD_SBVZ;
                        break; 
                    case "SMZV":
                        data = CMD_SMZV;
                        break; 
                    case "STBY":
                        data = CMD_STBY;
                        break;
                    case "EMPA":
                        data = CMD_EMPA;
                        break;
                    case "ASTZ":
                        data = CMD_ASTZ;
                        break;
                    case "ASTF":
                        data = CMD_ASTF;
                        break;
                    case "AERG":
                        data = CMD_AERG;
                        break;
                }
                port.Write(data, 0, data.Length);
                //SEND_COMMAND = cmd;
                CommonFunction.LogWrite(LogType.Info, DateTime.Now, "Send    : " + BitConverter.ToString(data, 0));
            }
            catch (Exception ex)
            {
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "   SendComm Error");
            }
        }
    }
}
