﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using AtomUI.Class;

namespace DAQ.AKProtocol
{
    public class Osiris
    {

        public static bool IsConnected = false;
        public static bool Connecting = false;
        public string hostName;
        public int portNum;
        TcpClient client;

        public Osiris()
        {
            foreach (acsConnectionInfo conInfo in Ethercat.sysInfo.HardWareInfo)
            {
                if (conInfo.HWType == HardWareType.OSIRIS)
                {
                    hostName = conInfo.RemoteAddress;
                    portNum = conInfo.CommPort;
                }
            }

        }

        public void Start_OsirisCom()
        {

            try
            {
                client = new TcpClient();
                client.ReceiveTimeout = 100;
                Connecting = true;
                client.Connect(hostName, portNum);
                Connecting = false;

                IsConnected = true;
            }
            catch (SocketException e)
            {
                return;
            }
            
            try
            {
                if (IsConnected)
                {
                    Console.Write("Connect ok");
                }
                NetworkStream ns = client.GetStream();
                StreamReader readerStream = new StreamReader(ns);
                while (IsConnected)
                {
                    if (ns.CanWrite)
                    {
                        foreach (VariableInfo var in DaqData.sysVarInfo.Osiris_Variables)
                        {
                            byte[] byteTime = ASCIIEncoding.ASCII.GetBytes((char)2 + " AGET " + var._NorminalName + (char)3);
                            ns.Write(byteTime, 0, byteTime.Length);
                        }
                        System.Threading.Thread.Sleep(800);
                    }
                    if (ns.CanRead)
                    {
                        byte[] bytes = new byte[2048];
                        int bytesRead = ns.Read(bytes, 0, bytes.Length);
                        int count = 0;

                        string response = Encoding.ASCII.GetString(bytes, 0, bytesRead);

                        string[] arrResponse = response.Split(null);
                        if (arrResponse.Length == 397)
                        {
                            for (int i = 1; i < arrResponse.Length - 2; i += 3)
                            {
                                if (arrResponse[i] == "AGET")
                                {
                                    if (arrResponse[i + 1] == "0")
                                    {
                                        if (arrResponse[i + 2].Contains("."))
                                        {
                                            DaqData.sysVarInfo.Osiris_Variables[count++].IOValue = double.Parse(arrResponse[i + 2].Trim((char)2, (char)3));
                                        }
                                        else
                                        {
                                            //error 또는 해당 이름에 해당하는 값이 OSIRIS로부터 리턴되지 않는경우(#NA)
                                            DaqData.sysVarInfo.Osiris_Variables[count++].IOValue = 0;
                                        }
                                    }
                                    else
                                    {
                                        //error
                                        DaqData.sysVarInfo.Osiris_Variables[count++].IOValue = 0;
                                    }
                                }
                            }

                            System.Threading.Thread.Sleep(100);
                        }

                        else
                        {
                            System.Threading.Thread.Sleep(100);
                            continue;
                        }
                    }

                }

                client.Close();

            }
            catch (Exception e)
            {
                IsConnected = false;
                if (client != null)
                {
                    client.Close();
                }
            }
        }

        public void Stop_OsirisCom()
        {
            IsConnected = false;
            if (client != null)
            {
                client.Close();
            }
        }
    }
}
