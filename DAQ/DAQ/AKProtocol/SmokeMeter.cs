﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using AtomUI.Class;
using System.IO.Ports;
using System.Windows.Forms;
using System.Threading;

namespace DAQ.SerialComm
{
    public class SmokeMeter
    {
        public enum SMStatus
        {
            SREM = 1,
            SMAN = 2,
            SRES = 3,
            SRDY = 4,
            SASB = 5,
            SMES = 6,
            SVOP = 7,
            SFPF = 8,
            SPUL = 9
        }

        public SMStatus ST_Remote = SMStatus.SMAN;
        public SMStatus ST_Function = SMStatus.SRES;

        public bool EMZYSetting = false;
        public bool MeasureStart = false;

        private void StatusChange(string StatusStr)
        {
            for (int ind=1; ind<10; ind++)
            {
                if (((SMStatus)ind).ToString() == StatusStr)
                {
                    if ((SMStatus)ind == SMStatus.SREM || (SMStatus)ind == SMStatus.SMAN)
                    {
                        ST_Remote = (SMStatus)ind;
                    } else
                    {
                        ST_Function = (SMStatus)ind;
                    }
                }
            }
        }

        public static bool IsConnected = false;

        public List<string> SM_Command_List = new List<string>();

        string ERROR_COMMAND = "";
        string SEND_COMMAND = "";
        public int ERROR_CODE = 0;
        int ERROR_STATUS = 0;
        public string Error_Description = "";
        public double AIR_TEMP;
        byte[] CMD_SRES = new byte[] { 0x02, 0x20, 0x53, 0x52, 0x45, 0x53, 0x03 };
        byte[] CMD_SREM = new byte[] { 0x02, 0x20, 0x53, 0x52, 0x45, 0x4D, 0x03 };
        byte[] CMD_SRDY = new byte[] { 0x02, 0x20, 0x53, 0x52, 0x44, 0x59, 0x03 };
        byte[] CMD_SASB = new byte[] { 0x02, 0x20, 0x53, 0x41, 0x53, 0x42, 0x03 };
        byte[] CMD_SMES = new byte[] { 0x02, 0x20, 0x53, 0x4D, 0x45, 0x53, 0x03 };
        byte[] CMD_SPUL = new byte[] { 0x02, 0x20, 0x53, 0x50, 0x55, 0x4C, 0x03 };

        byte[] CMD_EMZY = new byte[] { 0x02, 0x20, 0x45, 0x4D, 0x5A, 0x59, 0x20, 0x4B, 0x30, 0x20, 0x56, 0x20, 0x31, 0x30, 0x30, 0x30, 0x20, 0x33, 0x20, 0x03 };

        byte[] CMD_ASTZ = new byte[] { 0x02, 0x20, 0x41, 0x53, 0x54, 0x5A, 0x03 };
        byte[] CMD_ASTF = new byte[] { 0x02, 0x20, 0x41, 0x53, 0x54, 0x46, 0x03 };
        byte[] CMD_AFSN = new byte[] { 0x02, 0x20, 0x41, 0x46, 0x53, 0x4E, 0x03 };
        byte[] CMD_ASER = new byte[] { 0x02, 0x20, 0x41, 0x53, 0x45, 0x52, 0x03 };
        byte[] CMD_TEST = new byte[] { 0x20, 0x45, 0x4D, 0x5A, 0x59, 0x20, 0x5A, 0x20, 0x36, 0x30, 0x20, 0x32, 0x20 };

        byte[] FullBuffer = new byte[50000];
        int ReadByte = 0;

        private SerialPort port;
        String PortName;

        public void SetParameter(int Volume, int Count)
        {
            byte[] VolumeByte = Encoding.Default.GetBytes(Volume.ToString());
            byte[] CountByte = Encoding.Default.GetBytes(Count.ToString());

            byte[] TotalByte = new byte[14 + VolumeByte.Length + CountByte.Length];

            for (int Index = 0; Index < 12; Index++)
                TotalByte[Index] = CMD_EMZY[Index];
            for (int Index = 0; Index < VolumeByte.Length; Index++)
                TotalByte[12 + Index] = VolumeByte[Index];
            TotalByte[12 + VolumeByte.Length] = 0x20;
            for (int Index = 0; Index < CountByte.Length; Index++)
                TotalByte[12 + VolumeByte.Length + 1 + Index] = CountByte[Index];
            //TotalByte[9 + VolumeByte.Length + 1 + CountByte.Length] = 0x20;
            TotalByte[12 + VolumeByte.Length + 1 + CountByte.Length] = 0x03;

            CMD_EMZY = TotalByte;
        }

        public SmokeMeter(String PortName)
        {
            try
            {
                this.PortName = PortName;
                port = new SerialPort(PortName, 9600, Parity.None, 8, StopBits.One);
                if (!port.IsOpen)
                {
                    port.Open();
                }
                port.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);

                IsConnected = true;
            }
            catch { }
        }

        ~SmokeMeter()
        {
        }
        public void Stop()
        {
            IsConnected = false;
            SM_Command_List.Clear();
            port.Close();
            port.DataReceived -= new SerialDataReceivedEventHandler(port_DataReceived);
        }

        //private void test()
        //{
        //    try
        //    {
        //        if (true)
        //        {
        //            // 임시 테스트 로직
        //            int count = CMD_EMZY.Length;
        //            byte[] ReceivedPacket = null;
        //            byte[] NewBuffer = new byte[count];

        //            if (count > 0)
        //            {
        //                // 임시 테스트 로직
        //                NewBuffer = CMD_EMZY;

        //                Buffer.BlockCopy(NewBuffer, 0, FullBuffer, ReadByte, count);
        //                ReadByte += count;
        //                String strData = String.Format("Receive : {0} , {1} : {2}", count, ReadByte, BitConverter.ToString(NewBuffer, 0));
        //                CommonFunction.LogWrite(LogType.Info, DateTime.Now, strData);

        //                int start = -1;
        //                int end = -1;
        //                int bytelen = 0;

        //                for (int ind = 0; ind < ReadByte; ind++)
        //                {
        //                    if ((FullBuffer.Length >= ind + 7) && (FullBuffer[ind] == 0x02)) // <STX>
        //                    {
        //                        for (int lastind = ind + 1; lastind < ReadByte; lastind++)
        //                        {
        //                            if (FullBuffer[lastind] == 0x03)
        //                            {
        //                                bytelen = lastind - ind - 1;
        //                                ReceivedPacket = new byte[bytelen];
        //                                Buffer.BlockCopy(FullBuffer, ind + 1, ReceivedPacket, 0, bytelen);
        //                                ind = lastind + 1;
        //                                end = ind;
        //                                break;
        //                            }
        //                        }
        //                    }
        //                }
        //                if (end < 0)
        //                { ReadByte = 0; }
        //                else
        //                {
        //                    if (end > 0)
        //                    {
        //                        if (ReceivedPacket.Length > 0)
        //                        {
        //                            string[] ReceivedCommand = Encoding.Default.GetString(ReceivedPacket).Split(' ');
        //                            switch (ReceivedCommand[1])
        //                            {

        //                                case "EMZY":
        //                                    ERROR_CODE = int.Parse(ReceivedCommand[3]);
        //                                    if (ERROR_CODE > 0)
        //                                        ERROR_COMMAND = ReceivedCommand[1];
        //                                    else
        //                                    {
        //                                        if (SEND_COMMAND == ReceivedCommand[1])
        //                                            SEND_COMMAND = "";
        //                                    }
        //                                    break;
        //                                default :
        //                                    ERROR_CODE = int.Parse(ReceivedCommand[3]);
        //                                    if (ERROR_CODE > 0)
        //                                        ERROR_COMMAND = ReceivedCommand[1];
        //                                    else
        //                                    {
        //                                        if (SEND_COMMAND == ReceivedCommand[1])
        //                                            SEND_COMMAND = "";
        //                                    }
        //                                    break;
        //                            }
        //                        }
        //                        ReadByte = ReadByte - end;
        //                        if (ReadByte > 0)
        //                        {
        //                            Buffer.BlockCopy(FullBuffer, end, FullBuffer, 0, ReadByte);
        //                        }
        //                    }

        //                }


        //            }

        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "   sp_DataReceived Error");
        //    }
        //}

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                if (port.IsOpen)
                {
                    byte[] ReceivedPacket = null;
                    int count = port.BytesToRead;
                    byte[] NewBuffer = new byte[count];
                    if (count > 0)
                    {
                        port.Read(NewBuffer, 0, count);

                        Buffer.BlockCopy(NewBuffer, 0, FullBuffer, ReadByte, count);
                        ReadByte += count;
                        String strData = String.Format("Receive : {0} , {1} : {2} : {3}", count, ReadByte, BitConverter.ToString(NewBuffer, 0), Encoding.Default.GetString(NewBuffer));
                        CommonFunction.LogWrite(LogType.Info, DateTime.Now, strData);

                        int end = -1;
                        int bytelen = 0;

                        for (int ind = 0; ind < ReadByte; ind++)
                        {
                            if ((FullBuffer.Length >= ind + 7) && (FullBuffer[ind] == 0x02)) // <STX>
                            {
                                for (int lastind = ind + 1; lastind < ReadByte; lastind++)
                                {
                                    if (FullBuffer[lastind] == 0x03)
                                    {
                                        bytelen = lastind - ind - 1;
                                        ReceivedPacket = new byte[bytelen];
                                        Buffer.BlockCopy(FullBuffer, ind + 1, ReceivedPacket, 0, bytelen);
                                        ind = lastind + 1;
                                        end = ind;
                                        break;
                                    }
                                }
                                if (end > 0)
                                {
                                    break;
                                }
                            }
                        }
                        if (end < 0)
                        { //ReadByte = 0; 
                        }
                        else
                        {
                            if (end > 0)
                            {
                                if (ReceivedPacket.Length > 0)
                                {
                                    string[] ReceivedCommand = Encoding.Default.GetString(ReceivedPacket).Split(' ');
                                    System.Diagnostics.Debug.Print(Encoding.Default.GetString(ReceivedPacket));
                                    System.Diagnostics.Debug.Print("Measure Start = " + MeasureStart);
                                    ERROR_STATUS = int.Parse(ReceivedCommand[2]);
                                    if (ERROR_STATUS > 0)
                                    {
                                        ERROR_COMMAND = ReceivedCommand[1];
                                        ST_Function = SMStatus.SRES;
                                        if (ReceivedCommand[1] == "ASTF")
                                        {
                                            if (ERROR_STATUS > 0)
                                            {
                                                Error_Description = "";
                                                ERROR_CODE = int.Parse(ReceivedCommand[2]);
                                                for (int ind = 2; ind < ReceivedCommand.Length; ind++)
                                                    switch (int.Parse(ReceivedCommand[ind]))
                                                    {
                                                        case 0:
                                                            Error_Description += "";
                                                            break;
                                                        case 11:
                                                            Error_Description += "White Value out of range!\n\r";
                                                            break;
                                                        case 12:
                                                            Error_Description += "Grey Value out of range!\n\r";
                                                            break;
                                                        case 13:
                                                            Error_Description += "Black Value out of range!\n\r";
                                                            break;
                                                        case 15:
                                                            Error_Description += "Door is open!\n\r";
                                                            break;
                                                        case 17:
                                                            Error_Description += "Relative Pressure Sensor offset error!\n\r";
                                                            break;
                                                        case 16:
                                                            Error_Description += "Defferential Pressure Sensor offset error!\n\r";
                                                            break;
                                                        case 32:
                                                            Error_Description += "Working Temp. not reached yet!\n\r";
                                                            break;
                                                        case 24:
                                                            Error_Description += "Measured Temp. > 65degreeC!\n\r";
                                                            break;
                                                        case 50:
                                                            Error_Description += "Measured Temp. < 5degreeC!\n\r";
                                                            break;
                                                        default:
                                                            Error_Description += "";
                                                            break;
                                                    }
                                            }
                                            //SendComm("SRDY");
                                            //Thread.Sleep(500);
                                        }
                                        else
                                            SendComm("ASTF");
                                    }
                                    else
                                    {
                                        ERROR_CODE = 0;
                                        if (SEND_COMMAND == ReceivedCommand[1])
                                            SEND_COMMAND = "";
                                    }
                                    switch (ReceivedCommand[1])
                                    {
                                        // DaqData.sysVarInfo.MEXA9000_Variables[i].IOValue = double.Parse(msg[i]);
                                        case "SREM":
                                            if (!EMZYSetting)
                                            {
                                                SendComm("EMZY");
                                            }
                                            break;
                                        case "EMZY":
                                            if (!EMZYSetting)
                                            {
                                                EMZYSetting = true;
                                            }
                                            break;
                                        case "AFSN":
                                            int MeasureCount = int.Parse(ReceivedCommand[3]);
                                            if (MeasureCount == 1)
                                            {
                                                DaqData.sysVarInfo.SmokeMeter_Variables[0].IOValue = double.Parse(ReceivedCommand[4]);
                                                DaqData.sysVarInfo.SmokeMeter_Variables[1].IOValue = double.Parse(ReceivedCommand[4]);

                                                for (int ind = MeasureCount; ind < 5; ind++)
                                                {
                                                    if (ReceivedCommand[ind] != "")
                                                    {
                                                        DaqData.sysVarInfo.SmokeMeter_Variables[ind + 1].IOValue = 0;
                                                    }
                                                }
                                            }
                                            else if (MeasureCount > 1)
                                            {
                                                DaqData.sysVarInfo.SmokeMeter_Variables[0].IOValue = double.Parse(ReceivedCommand[4]);
                                                for (int ind = 5; ind < MeasureCount + 5; ind++)
                                                {
                                                    if (ReceivedCommand[ind] != "")
                                                    {
                                                        DaqData.sysVarInfo.SmokeMeter_Variables[ind - 4].IOValue = double.Parse(ReceivedCommand[ind]);
                                                    }
                                                }
                                                for (int ind = MeasureCount; ind < 5; ind++)
                                                {
                                                    if (ReceivedCommand[ind] != "")
                                                    {
                                                        DaqData.sysVarInfo.SmokeMeter_Variables[ind + 1].IOValue = 0;
                                                    }
                                                }
                                            }
                                            break;
                                        case "ASTF":
                                            break;
                                        case "ASER":
                                            AIR_TEMP = double.Parse(ReceivedCommand[10]);
                                            break;
                                        case "ASTZ":

                                            if (ReceivedCommand[2] == "0")
                                            {
                                                for (int indx = 3; indx < ReceivedCommand.Length; indx++)
                                                {
                                                    if (ReceivedCommand[indx] != "")
                                                    {
                                                        StatusChange(ReceivedCommand[indx]);
                                                    }
                                                }
                                                if (!MeasureStart)
                                                {
                                                    if (ST_Function == SMStatus.SMES)
                                                    {
                                                        MeasureStart = true;
                                                    }
                                                }

                                                if (MeasureStart)
                                                {
                                                    if (ST_Function == SMStatus.SRDY)
                                                    {
                                                        SendComm("AFSN");
                                                        MeasureStart = false;
                                                    }
                                                }
                                            }
                                            SM_Command_List.Add("ASER");
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                            ReadByte = ReadByte - end;
                            if (ReadByte > 0)
                            {
                                Buffer.BlockCopy(FullBuffer, end, FullBuffer, 0, ReadByte);
                            }
                        }
                    }
                }
                else
                {
                    port.DataReceived -= new SerialDataReceivedEventHandler(port_DataReceived);
                }
            }
            catch (Exception ex)
            {
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "   sp_DataReceived Error");
            }
        }

        public void Start_CommandSend()
        {
            while (IsConnected)
            {
                if (SM_Command_List.Count>0)
                {
                    string Command = "";

                    lock (SM_Command_List)
                    {
                        Command = SM_Command_List[0];
                        SM_Command_List.RemoveAt(0);
                    }

                    switch(Command)
                    {
                        case "ASER":
                        case "SRES":
                        case "SRDY":
                        case "SREM":
                        case "SASB":
                        case "EMZY":
                        case "ASTZ":
                        case "ASTF":
                        case "AFSN":
                        case "SPUL":
                            SendComm(Command);
                            break;
                        case "SMES":
                            SendComm(Command);
                            break;
                    }
                } else
                {
                    //if (SEND_COMMAND == "")
                        SendComm("ASTZ");
                }
                Thread.Sleep(1000);
            }
        }

        public bool WaitStatus(string Status, int TimeOut)
        {
            int Time = TimeOut * 10;
            do
            {
                Thread.Sleep(100);
                Time--;
                Application.DoEvents();
            } while (ST_Remote.ToString() == Status || ST_Function.ToString() == Status || Time<=0);

            if (Time <= 0)
                return false;
            else
                return true;
        }

        public void SendComm(string cmd)
        {
            try
            {
                byte[] data = CMD_ASTZ;

                switch (cmd)
                {
                    case "SRES":
                        data = CMD_SRES;
                        break;
                    case "SREM":
                        data = CMD_SREM;
                        break;
                    case "SRDY":
                        data = CMD_SRDY;
                        break;
                    case "SASB":
                        data = CMD_SASB;
                        break;
                    case "SMES":
                        data = CMD_SMES;
                        break;
                    case "ASER":
                        data = CMD_ASER;
                        break;
                    case "EMZY":
                        data = CMD_EMZY;
                        break;
                    case "ASTZ":
                        data = CMD_ASTZ;
                        break;
                    case "ASTF":
                        data = CMD_ASTF;
                        break;
                    case "AFSN":
                        data = CMD_AFSN;
                        break;
                    case "SPUL":
                        data = CMD_SPUL;
                        break;
                }
                port.Write(data, 0, data.Length);
                System.Diagnostics.Debug.Print(Encoding.Default.GetString(data));
                SEND_COMMAND = cmd;
                CommonFunction.LogWrite(LogType.Info, DateTime.Now, "Send    : " + BitConverter.ToString(data, 0));
            } catch (Exception ex) {
                CommonFunction.LogWrite(LogType.Error, DateTime.Now, ex.Message + ex.StackTrace + "   SendComm Error");
            }
        }
    }
}

