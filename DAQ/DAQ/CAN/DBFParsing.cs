﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace N_DBFParsing
{
    public partial class DBFParsing
    {
        private ArrayList dbfValueNamelist = new ArrayList();
        private ArrayList dbfNormalNamelist = new ArrayList();
        //private ArrayList dbcUnitlist = new ArrayList();


        private ArrayList dbcFramelist = new ArrayList();
        private ArrayList dbcNamelist = new ArrayList();
        private ArrayList dbcFomulalist = new ArrayList();
        private ArrayList dbcUnitlist = new ArrayList();
        private ArrayList dbcLengthlist = new ArrayList();

        private int DataChecksum = 0;

        private int frame1 = 0;
        private int frame2 = 0;
        private int frame3 = 0;
        private int frame4 = 0;
        private int frame5 = 0;
        private int frame6 = 0;
        private int frame7 = 0;
        private int frame8 = 0;
        private int frame9 = 0;
        private int frame10 = 0;
        private int frame11 = 0;
        private int frame12 = 0;
        private int frame13 = 0;
        private int frame14 = 0;
        private int frame15 = 0;
        private int frame16 = 0;
        private int frame17 = 0;
        private int frame18 = 0;
        private int frame19 = 0;
        private int frame20 = 0;

        public delegate void FMDataHandler(Object obj);
        public event FMDataHandler FMDataSendEvent;

        public DBFParsing()
        {
            
        }




        /*
        private void ScaleTxtBoxFont1()
        {
            // Only bother if there's text.
            string txt = this.textBox1.Text;
            if (txt.Length > 0)
            {
                int best_size = 100;

                // See how much room we have, allowing a bit
                // for the Label's internal margin.
                int wid = this.textBox1.DisplayRectangle.Width - 3;
                int hgt = this.textBox1.DisplayRectangle.Height - 3;

                // Make a Graphics object to measure the text.
                using (Graphics gr = this.textBox1.CreateGraphics())
                {
                    for (int i = 1; i <= 100; i++)
                    {
                        using (Font test_font =
                            new Font(this.textBox1.Font.FontFamily, i))
                        {
                            // See how much space the text would
                            // need, specifying a maximum width.
                            SizeF text_size =
                                gr.MeasureString(txt, test_font);
                            if ((text_size.Width > wid) ||
                                (text_size.Height > hgt))
                            {
                                best_size = i - 1;
                                break;
                            }
                        }
                    }
                }

                // Use that font size.
                this.textBox1.Font = new Font(this.textBox1.Font.FontFamily, best_size);
            }
        }
        */




        public string[] DBFparsing(string fpath)
        {
            string mSG       = "[START_SIGNALS]";
            string mSGValue  = "[VALUE_DESCRIPTION]";
            string mSGNumber = "[NUMBER_OF_MESSAGES]";
            string mSGStartM = "[START_MSG]";
            string mSGNEndM  = "[END_MSG]";

            dbfValueNamelist.Clear();
            dbfNormalNamelist.Clear();

            //string[] sFormula = new string[3];

            ArrayList mSGValueDesc = new ArrayList();
            //dbf는 전체 라인을 읽어와야 한다.
            string[] dbfAllValue = System.IO.File.ReadAllLines(fpath);
            string[] sItemList = null;
            int sNumberID = 0;
            int frameIndex = 0;
            Boolean startFrame = false;
            string[] ssr = null; //frame 별 아이템 네임이 들어 있음.

            if (dbfAllValue.Length > 0) //내용이 있으면.. 파싱 시작.
            {
                int itemCount = 0;

                for (int i = 0; i < dbfAllValue.Length; i++)
                {
                    if (dbfAllValue[i].Contains(mSGNumber))
                    {
                        sNumberID = Convert.ToInt32(dbfAllValue[i].Substring(mSGNumber.Length+1, dbfAllValue[i].Length - mSGNumber.Length -1));
                        ssr = new string[sNumberID];
                    }       

                    //frame count                    
                    if (dbfAllValue[i].Contains(mSGStartM))
                    {
                        startFrame = true;
                        frameIndex++;
                    }

                    if (dbfAllValue[i].Contains(mSGNEndM))
                    {
                        startFrame = false;
                    }

                    if (dbfAllValue[i].Contains(mSG))
                    {
                        if (startFrame)
                        {
                            string ss = dbfAllValue[i].Substring(mSG.Length+1, dbfAllValue[i].IndexOf(",") - mSG.Length -1);
                            ssr[frameIndex - 1] += ss + "\t"; //네임만 넣자.
                        }

                        itemCount++;
                    }
                }

                sItemList = new string[itemCount];
                int nItemIndex = 0;
                for (int i = 0; i < dbfAllValue.Length; i++)
                {   
                    if (dbfAllValue[i].Contains(mSG)) //시그널 행체크후 다음행에 특수행인지 [mSGValue]를 체크해야 한다.  ## 특수문자 데이터 처리.
                    {
                        mSGValueDesc.Clear();
                        for (int j = i + 1; j < dbfAllValue.Length - i; j++)
                        {
                            if (dbfAllValue[j].Contains(mSGValue)) //특수문자 데이터에 value description이 있는지 확인.
                            {
                                mSGValueDesc.Add(dbfAllValue[j]);
                            }
                            else //이 value description은 연속 되는 문자 열이다
                            {
                                break;
                            }
                        }

                        if (mSGValueDesc.Count > 0)//특수행 처리.
                        {
                            string sresult = "";
                            string tmep = dbfAllValue[i].ToString();

                            string ss = tmep.Substring(mSG.Length+1, tmep.IndexOf(",") - mSG.Length -1);
                            sresult += ss + "\t";
                            int itel = 0;
                            foreach(string item in mSGValueDesc)
                            {
                                string temp = item.Substring(mSGValue.Length, item.Length - mSGValue.Length);

                                if(itel == 0)
                                {
                                    sresult += temp;
                                }
                                else
                                {
                                    sresult += ":" + temp;
                                }                                
                                itel++;
                            }
                            string[] sNorArray = tmep.Split(',');
                            string sUnit = sNorArray[10].ToString();
                            sresult += "\t" + sUnit;

                            dbfValueNamelist.Add(sresult);

                            for (int k = 0; k < ssr.Length; k++)
                            {
                                string[] mt = ssr[k].Split('\t');

                                string st = "";

                                for (int h = 0; h < mt.Length -1; h++)
                                {
                                    if (sresult.Contains(mt[h]))
                                    {
                                        string temp = sresult;  //i + "frame" + "\t";
                                        st = (k + 1) + "frame" + "\t";
                                        st += temp;
                                        break;
                                    }
                                }                
                                if(st.Length > 0)
                                {
                                    sItemList[nItemIndex] = st;
                                }
                                
                            }

                                //sItemList[nItemIndex] = sresult;
                        }
                        else //일반행 처리.
                        {
                            string tmep = dbfAllValue[i].ToString();
                            dbfNormalNamelist.Add(tmep);

                            string[] sNorArray = tmep.Split(','); 
                            string sName = sNorArray[0].Substring(mSG.Length +1, sNorArray[0].Length - mSG.Length -1);
                            string sLength = sNorArray[1].ToString();
                            string sType = sNorArray[4].ToString();
                            string sMaxValue = sNorArray[5].ToString();
                            string sMinValue = sNorArray[6].ToString();
                            string sDataformat = sNorArray[7].ToString();
                            string sOffeset = sNorArray[8].ToString();
                            string sFactor = sNorArray[9].ToString();
                            string sUnit = sNorArray[10].ToString();

                            //cal formula  x1 * hex / x2 이다..
                            string X1 = "";
                            
                            double mFactor = Convert.ToDouble(sOffeset) / Convert.ToDouble(sFactor);
                            if (mFactor != 0)
                            {
                                X1 = mFactor.ToString() + "+" + Convert.ToDouble(sDataformat).ToString();

                            }
                            else
                            {
                                X1 = Convert.ToDouble(sDataformat).ToString();
                            }

                            double X_2 = (abs(Convert.ToDouble(sDataformat)) / abs(Convert.ToDouble(sFactor)));


                            if (Convert.ToDouble(sOffeset) > 0)
                            {
                                X_2 = revabs(X_2);
                            }

                            string X2 = X_2.ToString();

                            string sComp = sName + "\t" + X1 + ":" + X2 + "\t" + sUnit;
                            //arrange
                            for (int k = 0; k < ssr.Length; k++)
                            {
                                string[] mt = ssr[k].Split('\t');

                                string st = "";

                                for (int h = 0; h < mt.Length - 1; h++)
                                {
                                    if (sComp.Contains(mt[h]))
                                    {
                                        string temp = sComp;  //i + "frame" + "\t";
                                        st = (k + 1) + "frame" + "\t";
                                        st += temp;
                                        st += "\t" + sLength;

                                        break;
                                    }
                                }
                                if (st.Length > 0)
                                {
                                    sItemList[nItemIndex] = st;
                                }

                            }

                            //sItemList[nItemIndex] += sName + "\t" + X1 + ":" + X2 + "\t" + sUnit;     
                        }
                        nItemIndex++;
                    }
                }     
            }

            return sItemList;
        }

      




        double abs(double num) //절대값 구하기.
        {
            if (num < 0)
                num = -num;
            return num;
        }

        double minabs(double num) //- 구하기.
        {
            if (num > 0)
                num = -num;
            return num;
        }

        double revabs(double num) //- 구하기.
        {
            if (num > 0)
                num = -num;
            else
                num = -num;
            return num;
        }


    }
}
