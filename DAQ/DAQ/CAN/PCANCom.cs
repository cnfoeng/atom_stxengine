﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Timers;
using System.Globalization;
using AtomUI.Class;
using DAQ_CANBasic;

/// <summary>
/// Inclusion of PEAK PCAN-Basic namespace
/// </summary>

using TPCANHandle = System.UInt16;
using TPCANBitrateFD = System.String;
using TPCANTimestampFD = System.UInt64;

namespace DAQ_CAN
{
    public class CANCom
    {

        public static string sFTime;
        public static bool IsConnected = false;
        //public static int arrCount = -1;//노이즈 제거위한 평균값 계산용
        //public static double[] Sum = new double[100];//노이즈 제거위한 평균값 계산용. 일단 채널 최대 100개로
        //public static double[,] arrIOValue = new double[100, 5];//노이즈 제거위한 평균값 계산용. 일단 채널 최대 100개로
        public static bool FlagCANThread = false;//thread작동함수 while루프용

        public bool CANConnect = false;

        private class MessageStatus
        {
            private TPCANMsgFD m_Msg;
            private TPCANTimestampFD m_TimeStamp;
            private TPCANTimestampFD m_oldTimeStamp;
            private int m_iIndex = 0;
            private int m_Count;
            private bool m_bShowPeriod;
            private bool m_bWasChanged;

            public MessageStatus(TPCANMsgFD canMsg, TPCANTimestampFD canTimestamp)
            {
                m_Msg = canMsg;
                m_TimeStamp = canTimestamp;
                m_oldTimeStamp = canTimestamp;
                m_Count = 1;
                m_bShowPeriod = true;
                m_bWasChanged = false;
            }

            public void Update(TPCANMsgFD canMsg, TPCANTimestampFD canTimestamp)
            {
                m_Msg = canMsg;
                m_oldTimeStamp = m_TimeStamp;
                m_TimeStamp = canTimestamp;
                m_bWasChanged = true;
                m_Count += 1;
            }

            public TPCANMsgFD CANMsg
            {
                get { return m_Msg; }
            }

            public TPCANTimestampFD Timestamp
            {
                get { return m_TimeStamp; }
            }

            public int Position
            {
                get { return m_iIndex; }
            }

            public string TypeString
            {
                get { return GetMsgTypeString(); }
            }

            public string IdString
            {
                get { return GetIdString(); }
            }

            public string DataString
            {
                get { return GetDataString(); }
            }

            public int Count
            {
                get { return m_Count; }
            }

            public bool ShowingPeriod
            {
                get { return m_bShowPeriod; }
                set
                {
                    if (m_bShowPeriod ^ value)
                    {
                        m_bShowPeriod = value;
                        m_bWasChanged = true;
                    }
                }
            }

            public bool MarkedAsUpdated
            {
                get { return m_bWasChanged; }
                set { m_bWasChanged = value; }
            }

            public string TimeString
            {
                get { return GetTimeString(); }
            }

            private string GetTimeString()
            {
                double fTime;

                fTime = (m_TimeStamp / 1000.0);
                if (m_bShowPeriod)
                    fTime -= (m_oldTimeStamp / 1000.0);
                return fTime.ToString("F1");
            }

            private string GetDataString()
            {
                string strTemp;

                strTemp = "";

                if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_RTR) == TPCANMessageType.PCAN_MESSAGE_RTR)
                    return "Remote Request";
                else
                    for (int i = 0; i < CANCom.GetLengthFromDLC(m_Msg.DLC, (m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_FD) == 0); i++)
                        strTemp += string.Format("{0:X2} ", m_Msg.DATA[i]);

                return strTemp;
            }

            private string GetIdString()
            {
                // We format the ID of the message and show it
                //
                if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_EXTENDED) == TPCANMessageType.PCAN_MESSAGE_EXTENDED)
                    return string.Format("{0}", m_Msg.ID);
                else
                    return string.Format("{0}", m_Msg.ID);
            }

            private string GetMsgTypeString()
            {
                string strTemp;

                if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_STATUS) == TPCANMessageType.PCAN_MESSAGE_STATUS)
                    return "STATUS";

                if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_EXTENDED) == TPCANMessageType.PCAN_MESSAGE_EXTENDED)
                    strTemp = "EXT";
                else
                    strTemp = "STD";

                if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_RTR) == TPCANMessageType.PCAN_MESSAGE_RTR)
                    strTemp += "/RTR";
                else
                    if ((int)m_Msg.MSGTYPE > (int)TPCANMessageType.PCAN_MESSAGE_EXTENDED)
                    {
                        strTemp += " [ ";
                        if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_FD) == TPCANMessageType.PCAN_MESSAGE_FD)
                            strTemp += " FD";
                        if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_BRS) == TPCANMessageType.PCAN_MESSAGE_BRS)
                            strTemp += " BRS";
                        if ((m_Msg.MSGTYPE & TPCANMessageType.PCAN_MESSAGE_ESI) == TPCANMessageType.PCAN_MESSAGE_ESI)
                            strTemp += " ESI";
                        strTemp += " ]";
                    }

                return strTemp;
            }

        }
        /// <summary>
        /// Read-Delegate Handler
        /// </summary>
        private delegate void ReadDelegateHandler();


        /// <summary>
        /// Saves the desired connection mode
        /// </summary>
        //private bool m_IsFD;
        /// <summary>
        /// Saves the handle of a PCAN hardware
        /// </summary>
        private TPCANHandle m_PcanHandle;
        /// <summary>
        /// Saves the baudrate register for a conenction
        /// </summary>
        private TPCANBaudrate m_Baudrate;
        /// <summary>
        /// Saves the type of a non-plug-and-play hardware
        /// </summary>
        private TPCANType m_HwType;
        /// <summary>
        /// Stores the status of received messages for its display
        /// </summary>
        private System.Collections.ArrayList m_LastMsgsList;
        /// <summary>
        /// Read Delegate for calling the function "ReadMessages"
        /// </summary>
        private ReadDelegateHandler m_ReadDelegate;
        /// <summary>
        /// Receive-Event
        /// </summary>
        private System.Threading.AutoResetEvent m_ReceiveEvent;
        /// <summary>
        /// Thread for message reading (using events)
        /// </summary>
        private System.Threading.Thread m_ReadThread;//쓰레드를 통해서 계속 신호를 받는듯.
        /// <summary>
        /// Handles of the current available PCAN-Hardware
        /// </summary>
        private TPCANHandle[] m_HandlesArray;


        /// <summary>
        /// Convert a CAN DLC value into the actual data length of the CAN/CAN-FD frame.
        /// </summary>
        /// <param name="dlc">A value between 0 and 15 (CAN and FD DLC range)</param>
        /// <param name="isSTD">A value indicating if the msg is a standard CAN (FD Flag not checked)</param>
        /// <returns>The length represented by the DLC</returns>
        public static int GetLengthFromDLC(int dlc, bool isSTD)
        {
            if (dlc <= 8)
                return dlc;

            if (isSTD)
                return 8;

            switch (dlc)
            {
                case 9: return 12;
                case 10: return 16;
                case 11: return 20;
                case 12: return 24;
                case 13: return 32;
                case 14: return 48;
                case 15: return 64;
                default: return dlc;
            }
        }

        /// <summary>
        /// Initialization of PCAN-Basic components
        /// </summary>
        private void InitializeBasicComponents()
        {
            // Creates the list for received messages
            //
            m_LastMsgsList = new System.Collections.ArrayList();
            // Creates the delegate used for message reading
            //
            m_ReadDelegate = new ReadDelegateHandler(ReadMessages);
            // Creates the event used for signalize incomming messages 
            //
            m_ReceiveEvent = new System.Threading.AutoResetEvent(false);
            // Creates an array with all possible PCAN-Channels
            //
            m_HandlesArray = new TPCANHandle[] 
            { 
                PCANBasic.PCAN_ISABUS1,
                PCANBasic.PCAN_ISABUS2,
                PCANBasic.PCAN_ISABUS3,
                PCANBasic.PCAN_ISABUS4,
                PCANBasic.PCAN_ISABUS5,
                PCANBasic.PCAN_ISABUS6,
                PCANBasic.PCAN_ISABUS7,
                PCANBasic.PCAN_ISABUS8,
                PCANBasic.PCAN_DNGBUS1,
                PCANBasic.PCAN_PCIBUS1,
                PCANBasic.PCAN_PCIBUS2,
                PCANBasic.PCAN_PCIBUS3,
                PCANBasic.PCAN_PCIBUS4,
                PCANBasic.PCAN_PCIBUS5,
                PCANBasic.PCAN_PCIBUS6,
                PCANBasic.PCAN_PCIBUS7,
                PCANBasic.PCAN_PCIBUS8,
                PCANBasic.PCAN_PCIBUS9,
                PCANBasic.PCAN_PCIBUS10,
                PCANBasic.PCAN_PCIBUS11,
                PCANBasic.PCAN_PCIBUS12,
                PCANBasic.PCAN_PCIBUS13,
                PCANBasic.PCAN_PCIBUS14,
                PCANBasic.PCAN_PCIBUS15,
                PCANBasic.PCAN_PCIBUS16,
                PCANBasic.PCAN_USBBUS1,
                PCANBasic.PCAN_USBBUS2,
                PCANBasic.PCAN_USBBUS3,
                PCANBasic.PCAN_USBBUS4,
                PCANBasic.PCAN_USBBUS5,
                PCANBasic.PCAN_USBBUS6,
                PCANBasic.PCAN_USBBUS7,
                PCANBasic.PCAN_USBBUS8,
                PCANBasic.PCAN_USBBUS9,
                PCANBasic.PCAN_USBBUS10,
                PCANBasic.PCAN_USBBUS11,
                PCANBasic.PCAN_USBBUS12,
                PCANBasic.PCAN_USBBUS13,
                PCANBasic.PCAN_USBBUS14,
                PCANBasic.PCAN_USBBUS15,
                PCANBasic.PCAN_USBBUS16,
                PCANBasic.PCAN_PCCBUS1,
                PCANBasic.PCAN_PCCBUS2,
                PCANBasic.PCAN_LANBUS1,
                PCANBasic.PCAN_LANBUS2,
                PCANBasic.PCAN_LANBUS3,
                PCANBasic.PCAN_LANBUS4,
                PCANBasic.PCAN_LANBUS5,
                PCANBasic.PCAN_LANBUS6,
                PCANBasic.PCAN_LANBUS7,
                PCANBasic.PCAN_LANBUS8,
                PCANBasic.PCAN_LANBUS9,
                PCANBasic.PCAN_LANBUS10,
                PCANBasic.PCAN_LANBUS11,
                PCANBasic.PCAN_LANBUS12,
                PCANBasic.PCAN_LANBUS13,
                PCANBasic.PCAN_LANBUS14,
                PCANBasic.PCAN_LANBUS15,
                PCANBasic.PCAN_LANBUS16,
            };

            // Fills and configures the Data of several comboBox components
            //


            // Prepares the PCAN-Basic's debug-Log file
            //
            ConfigureLogFile();
        }

        /// <summary>
        /// Configures the Debug-Log file of PCAN-Basic
        /// </summary>
        private void ConfigureLogFile()
        {
            UInt32 iBuffer;

            // Sets the mask to catch all events
            //
            iBuffer = PCANBasic.LOG_FUNCTION_ALL;

            // Configures the log file. 
            // NOTE: The Log capability is to be used with the NONEBUS Handle. Other handle than this will 
            // cause the function fail.
            //
            PCANBasic.SetValue(PCANBasic.PCAN_NONEBUS, TPCANParameter.PCAN_LOG_CONFIGURE, ref iBuffer, sizeof(UInt32));
        }

        /// <summary>
        /// Configures the PCAN-Trace file for a PCAN-Basic Channel
        /// </summary>
        private void ConfigureTraceFile()
        {
            UInt32 iBuffer;
            TPCANStatus stsResult;

            // Configure the maximum size of a trace file to 5 megabytes
            //
            iBuffer = 5;
            stsResult = PCANBasic.SetValue(m_PcanHandle, TPCANParameter.PCAN_TRACE_SIZE, ref iBuffer, sizeof(UInt32));
            if (stsResult != TPCANStatus.PCAN_ERROR_OK)
                IncludeTextMessage(GetFormatedError(stsResult));

            // Configure the way how trace files are created: 
            // * Standard name is used
            // * Existing file is ovewritten, 
            // * Only one file is created.
            // * Recording stopts when the file size reaches 5 megabytes.
            //
            iBuffer = PCANBasic.TRACE_FILE_SINGLE | PCANBasic.TRACE_FILE_OVERWRITE;
            stsResult = PCANBasic.SetValue(m_PcanHandle, TPCANParameter.PCAN_TRACE_CONFIGURE, ref iBuffer, sizeof(UInt32));
            if (stsResult != TPCANStatus.PCAN_ERROR_OK)
                IncludeTextMessage(GetFormatedError(stsResult));
        }

        /// <summary>
        /// Help Function used to get an error as text
        /// </summary>
        /// <param name="error">Error code to be translated</param>
        /// <returns>A text with the translated error</returns>
        private string GetFormatedError(TPCANStatus error)
        {
            StringBuilder strTemp;

            // Creates a buffer big enough for a error-text
            //
            strTemp = new StringBuilder(256);
            // Gets the text using the GetErrorText API function
            // If the function success, the translated error is returned. If it fails,
            // a text describing the current error is returned.
            //
            if (PCANBasic.GetErrorText(error, 0, strTemp) != TPCANStatus.PCAN_ERROR_OK)
                return string.Format("An error occurred. Error-code's text ({0:X}) couldn't be retrieved", error);
            else
                return strTemp.ToString();
        }

        // <summary>
        // Includes a new line of text into the information Listview
        // </summary>
        // <param name="strMsg">Text to be included</param>
        private void IncludeTextMessage(string strMsg)
        {
            //lbxInfo.Items.Add(strMsg);
            //lbxInfo.SelectedIndex = lbxInfo.Items.Count - 1;
        }


        /// <summary>
        /// Activates/deaactivates the different controls of the main-form according
        /// with the current connection status
        /// </summary>
        /// <param name="bConnected">Current status. True if connected, false otherwise</param>


        /// <summary>
        /// Gets the formated text for a PCAN-Basic channel handle
        /// </summary>
        /// <param name="handle">PCAN-Basic Handle to format</param>
        /// <param name="isFD">If the channel is FD capable</param>
        /// <returns>The formatted text for a channel</returns>
        private string FormatChannelName(TPCANHandle handle, bool isFD)
        {
            TPCANDevice devDevice;
            byte byChannel;

            // Gets the owner device and channel for a 
            // PCAN-Basic handle
            //
            if (handle < 0x100)
            {
                devDevice = (TPCANDevice)(handle >> 4);
                byChannel = (byte)(handle & 0xF);
            }
            else
            {
                devDevice = (TPCANDevice)(handle >> 8);
                byChannel = (byte)(handle & 0xFF);
            }

            // Constructs the PCAN-Basic Channel name and return it
            //
            if (isFD)
                return string.Format("{0}:FD {1} ({2:X2}h)", devDevice, byChannel, handle);
            else
                return string.Format("{0} {1} ({2:X2}h)", devDevice, byChannel, handle);
        }

        /// <summary>
        /// Gets the formated text for a PCAN-Basic channel handle
        /// </summary>
        /// <param name="handle">PCAN-Basic Handle to format</param>
        /// <returns>The formatted text for a channel</returns>
        private string FormatChannelName(TPCANHandle handle)
        {
            return FormatChannelName(handle, false);
        }

        /// <summary>
        /// Display CAN messages
        /// </summary>


        private void WriteMsg(string id, string data)
        {
            string trimedID = int.Parse(id).ToString();
            int sindex = 0;
            //int r = 0;
            //arrCount++;

            for (int i = 0; i < DaqData.sysVarInfo.ECU_Variables.Count; i++)
            {
                if (DaqData.sysVarInfo.ECU_Variables[i].NorminalDescription == null)//프레임정보없는것들 처리.
                {
                    continue;
                }
                //System.Diagnostics.Debug.Print(trimedID);

                if (trimedID == DaqData.sysVarInfo.ECU_Variables[i].NorminalDescription.Replace("frame", "")) //ECU_Variables[i].NorminalDescription =  프레임넘버
                {
                    string[] arrData = data.Split(' ');
                    if (arrData.Length > 7)
                    {
                        int mindex = DaqData.sysVarInfo.ECU_Variables[i].ChannelInfo.ChannelBit / 8;



                        //if (mindex == 1)
                        //{
                        //    sindex = Program.sysvarinfo.ECU_Variables[i].ChannelInfo.Offset - 1;
                        //}
                        //else
                        //{
                        //    sindex = Program.sysvarinfo.ECU_Variables[i].ChannelInfo.Offset + mindex - 2;
                        //}
                        //}
                        //}
                        sindex = DaqData.sysVarInfo.ECU_Variables[i].ChannelInfo.Offset;


                        string stemp = "";

                        {
                            for (int j = mindex; j > 0; j--)
                            {
                                stemp += arrData[sindex + j - 2];
                            }
                        }
                        if (DaqData.sysVarInfo.ECU_Variables[i].ChannelInfo.ValueType == 1)
                        {
                            DaqData.sysVarInfo.ECU_Variables[i].IOValue = Int16.Parse(stemp, NumberStyles.HexNumber);
                        }
                        else
                        {
                            DaqData.sysVarInfo.ECU_Variables[i].IOValue = int.Parse(stemp, NumberStyles.HexNumber);
                        }
                        sFTime = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
                    }
                }
            }


            /////////////////////////////////////////////////////////////////////////////////기존거
            //if (trimedID == DaqData.sysVarInfo.ECU_Variables[i].NorminalDescription.Replace("frame", "")) //ECU_Variables[i].NorminalDescription =  프레임넘버
            //{
            //    string[] arrData = data.Split(' ');
            //    // 채널비트 = 렝스(바이트단위), 오프셋=dbf로부터읽은 바이트자리. 배열은 0번부터시작 8번이 끝.(dbc파일보면 유추가능)

            //    int mindex = DaqData.sysVarInfo.ECU_Variables[i].ChannelInfo.ChannelBit / 8;

            //    if (mindex == 1)
            //    {
            //        sindex = DaqData.sysVarInfo.ECU_Variables[i].ChannelInfo.Offset - 1;
            //    }
            //    else
            //    {
            //        sindex = DaqData.sysVarInfo.ECU_Variables[i].ChannelInfo.Offset + mindex - 2;
            //    }

            //    string stemp = "";

            //    {
            //        for (int j = 0; j < mindex; j++)
            //        {
            //            stemp += arrData[sindex - j];
            //        }
            //    }



            //    //r = arrCount % 5;
            //    //arrIOValue[i, r] = int.Parse(stemp, NumberStyles.HexNumber);
            //    DaqData.sysVarInfo.ECU_Variables[i].IOValue = int.Parse(stemp, NumberStyles.HexNumber);
            //    sFTime = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");


            //    //값 넣을때 바로 넣는게 아니라 이전값 최대 5개까지 값들의 평균값으로 넣어주어야 한다.(노이즈때문)
            //    //if (i < 5)
            //    //{
            //    //    Sum[i] += int.Parse(stemp, NumberStyles.HexNumber);
            //    //    DaqData.sysVarInfo.ECU_Variables[i].IOValue = Sum[i] / (i + 1);
            //    //    Console.Write(DaqData.sysVarInfo.ECU_Variables[i].NorminalName + " : " + DaqData.sysVarInfo.ECU_Variables[i].IOValue + "  " + DaqData.sysVarInfo.ECU_Variables[i].RealValue + "\n");

            //    //    sFTime = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
            //    //}
            //    //else
            //    //{
            //    //    Sum[i] = 0;
            //    //    for (int k = 0; k < 5; k++)
            //    //    {
            //    //        Sum[i] += arrIOValue[i, k];
            //    //    }

            //    //    DaqData.sysVarInfo.ECU_Variables[i].IOValue = Sum[i] / 5;
            //    //    Console.Write(DaqData.sysVarInfo.ECU_Variables[i].NorminalName + " : " + DaqData.sysVarInfo.ECU_Variables[i].IOValue + "  " + DaqData.sysVarInfo.ECU_Variables[i].RealValue + "\n");

            //    //    sFTime = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
            //    //}


            //}
        }

        /// <summary>
        /// Inserts a new entry for a new message in the Message-ListView
        /// </summary>
        /// <param name="newMsg">The messasge to be inserted</param>
        /// <param name="timeStamp">The Timesamp of the new message</param>
        private void InsertMsgEntry(TPCANMsgFD newMsg, TPCANTimestampFD timeStamp)
        {
            MessageStatus msgStsCurrentMsg;

            lock (m_LastMsgsList.SyncRoot)
            {

                msgStsCurrentMsg = new MessageStatus(newMsg, timeStamp);
                msgStsCurrentMsg.ShowingPeriod = true;
                WriteMsg(msgStsCurrentMsg.IdString, msgStsCurrentMsg.DataString);

            }
        }

        /// <summary>
        /// Processes a received message, in order to show it in the Message-ListView
        /// </summary>
        /// <param name="theMsg">The received PCAN-Basic message</param>
        /// <returns>True if the message must be created, false if it must be modified</returns>
        private void ProcessMessage(TPCANMsgFD theMsg, TPCANTimestampFD itsTimeStamp)
        {
            // We search if a message (Same ID and Type) is 
            // already received or if this is a new message
            //
            lock (m_LastMsgsList.SyncRoot)
            {
                foreach (MessageStatus msg in m_LastMsgsList)
                {
                    if ((msg.CANMsg.ID == theMsg.ID) && (msg.CANMsg.MSGTYPE == theMsg.MSGTYPE))
                    {
                        // Modify the message and exit
                        //
                        msg.Update(theMsg, itsTimeStamp);
                        return;
                    }
                }
                // Message not found. It will created
                //
                InsertMsgEntry(theMsg, itsTimeStamp);
            }
        }

        /// <summary>
        /// Processes a received message, in order to show it in the Message-ListView
        /// </summary>
        /// <param name="theMsg">The received PCAN-Basic message</param>
        /// <returns>True if the message must be created, false if it must be modified</returns>
        private void ProcessMessage(TPCANMsg theMsg, TPCANTimestamp itsTimeStamp)
        {
            TPCANMsgFD newMsg;
            TPCANTimestampFD newTimestamp;

            newMsg = new TPCANMsgFD();
            newMsg.DATA = new byte[64];
            newMsg.ID = theMsg.ID;
            newMsg.DLC = theMsg.LEN;
            for (int i = 0; i < ((theMsg.LEN > 8) ? 8 : theMsg.LEN); i++)
                newMsg.DATA[i] = theMsg.DATA[i];
            newMsg.MSGTYPE = theMsg.MSGTYPE;

            newTimestamp = Convert.ToUInt64(itsTimeStamp.micros + 1000 * itsTimeStamp.millis + 0x100000000 * 1000 * itsTimeStamp.millis_overflow);
            ProcessMessage(newMsg, newTimestamp);
        }


        /// <summary>
        /// Function for reading CAN messages on normal CAN devices
        /// </summary>
        /// <returns>A TPCANStatus error code</returns>
        private TPCANStatus ReadMessage()
        {
            TPCANMsg CANMsg;
            TPCANTimestamp CANTimeStamp;
            TPCANStatus stsResult;

            // We execute the "Read" function of the PCANBasic                
            //
            stsResult = PCANBasic.Read(m_PcanHandle, out CANMsg, out CANTimeStamp);
            if (stsResult != TPCANStatus.PCAN_ERROR_QRCVEMPTY)
                // We process the received message
                //
                ProcessMessage(CANMsg, CANTimeStamp);

            return stsResult;
        }

        /// <summary>
        /// Function for reading PCAN-Basic messages
        /// </summary>
        private void ReadMessages()
        {
            TPCANStatus stsResult;

            // We read at least one time the queue looking for messages.
            // If a message is found, we look again trying to find more.
            // If the queue is empty or an error occurr, we get out from
            // the dowhile statement.
            //			
            do
            {
                stsResult = ReadMessage();
                if (stsResult == TPCANStatus.PCAN_ERROR_ILLOPERATION)
                    break;
            } while (CANConnect && (!Convert.ToBoolean(stsResult & TPCANStatus.PCAN_ERROR_QRCVEMPTY)));
        }


        public CANCom()
        {
            // Initializes specific components
            //
            InitializeBasicComponents();
        }

        public void Start_CanCom()//기존 PCAN예제는 값들을 하나하나 세팅해줘야 했는데(체크박스 누르거나) 이 프로그램은 카텍장비에 맞춘거라 그냥 값 다 세팅해주는거. 그 기능을 하는 함수.
        {
            TPCANStatus stsResult;

            bool bNonPnP;
            string strTemp = "PCAN_USB:FD 1 (51h)"; // USB인 경우 PCAN_USB:FD 1 (51h) , PCI인 경우 PCAN_PCI 1 (41h). 드라이버는 동일.

            foreach (acsConnectionInfo conInfo in Ethercat.sysInfo.HardWareInfo)
            {
                if (conInfo.HWType == HardWareType.ECU)
                {
                    strTemp = conInfo.RemoteAddress;
                }
            }
            
            strTemp = strTemp.Substring(strTemp.IndexOf('(') + 1, 3);
            strTemp = strTemp.Replace('h', ' ').Trim(' ');

            // Determines if the handle belong to a No Plug&Play hardware 
            //
            m_PcanHandle = Convert.ToUInt16(strTemp, 16);
            bNonPnP = m_PcanHandle <= PCANBasic.PCAN_DNGBUS1;
            // Activates/deactivates configuration controls according with the 
            // kind of hardware
            //            


            // Connects a selected PCAN-Basic channel
            m_Baudrate = TPCANBaudrate.PCAN_BAUD_500K;
            m_HwType = TPCANType.PCAN_TYPE_ISA;

            stsResult = PCANBasic.Initialize(
                m_PcanHandle,
                m_Baudrate,
                m_HwType,
                Convert.ToUInt32("0100", 16),
                Convert.ToUInt16("3"));

            if (stsResult != TPCANStatus.PCAN_ERROR_OK)
            {
                if (stsResult != TPCANStatus.PCAN_ERROR_CAUTION)
                {
                    Console.Write(GetFormatedError(stsResult));
                    //return;
                }
                else
                {
                    IncludeTextMessage("******************************************************");
                    IncludeTextMessage("The bitrate being used is different than the given one");
                    IncludeTextMessage("******************************************************");
                    stsResult = TPCANStatus.PCAN_ERROR_OK;
                }
            }
            else
            {
                // Prepares the PCAN-Basic's PCAN-Trace file
                //
                ConfigureTraceFile();

                FlagCANThread = true;
                CANConnect = true;
                IsConnected = CANConnect;

                while (FlagCANThread)
                {
                    ReadMessage();//쓰레드
                }
            }
        }
        /// <summary>
        /// Activates/deaactivates the different controls of the main-form according
        /// with the current connection status
        /// </summary>
        /// <param name="bConnected">Current status. True if connected, false otherwise</param>


        public void Stop_CanCom()
        {
            PCANBasic.Uninitialize(m_PcanHandle);

            IsConnected = false;
            FlagCANThread = false;

            if (m_ReadThread != null)
            {
                m_ReadThread.Abort();//스레드 종료. 강제종료다. 어디서끝날지 아무도 모름. 예측불가. 중간에 종료하는건 이거쓰지않는다.
                m_ReadThread.Join();//위와 마찬가지로 종료. 그러나 쓰레드가 다 실행될때 까지 기다렸다가 종료시킨다.
                m_ReadThread = null;
            }


        }





    }
}
