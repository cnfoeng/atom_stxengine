﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Timers;
using AtomUI.Class;
using System.Diagnostics;

namespace DAQ_GPIB
{
    public class GPIBCom
    {

        public string strReadRMD = null;
        public string strReadRFS = null;
        public string[] ParcedstrReadRMD = null;
        public string Lastmsg;
        public string CurrentState;

        public static string CPUOnOffCheck = "";
        public static string OperationConditionLine1 = "";
        public static string sFTime;
        public static bool IsConnected = false;
        public static int arrCount = -1;//노이즈 제거위한 평균값 계산용
        public static double[] Sum = new double[100];//노이즈 제거위한 평균값 계산용. 일단 채널 최대 100개로
        public static double[,] arrIOValue = new double[100, 5];//노이즈 제거위한 평균값 계산용. 일단 채널 최대 100개로


        private System.Timers.Timer aTimer2;
        private System.Timers.Timer aTimer3;
        private acsConnectionInfo MexaConnectionInfo;
        private int mATTimer = 500;
        private string GPIBControllerAddress;
        SerialPort sp1 = new SerialPort();

        public void Start_GPIBCom()
        {
            // COM port parameters

            foreach (acsConnectionInfo ConnectionInfo in Ethercat.sysInfo.HardWareInfo)
            {
                if (ConnectionInfo.HWType == HardWareType.MEXA_SERIAL)
                {
                    MexaConnectionInfo = ConnectionInfo;
                }
            }

            sp1.PortName = MexaConnectionInfo.RemoteAddress;
            GPIBControllerAddress = MexaConnectionInfo.SlaveUnitIdentifier;
            sp1.BaudRate = 115200;
            sp1.DataBits = 8;
            sp1.Parity = Parity.None;
            sp1.StopBits = StopBits.One;

            // RTS/CTS handshaking
            sp1.Handshake = Handshake.RequestToSend;
            sp1.DtrEnable = true;

            // Error handling
            sp1.DiscardNull = false;
            sp1.ParityReplace = 0;



            //string strPrologixRsrc = "GPIB3::3::INSTR"; // cboVISARsrc.Text; //Get VISA resource ID from combobox 앞의 3은 board넘버. 뒤의 3은 primary adress. secondary adress가 존재하는 경우 GPIB3::3::1::INSTR 이런식.
            
            try
            {
                sp1.Open();

                sp1.Write("++mode 1\r\n");
                System.Threading.Thread.Sleep(50);
                sp1.Write("++addr " + GPIBControllerAddress + "\r\n");
                System.Threading.Thread.Sleep(50);
                

                IsConnected = true;
                
                aTimer2 = new System.Timers.Timer(mATTimer);
                aTimer2.Elapsed += OnRun1;
                aTimer2.Enabled = true;

                aTimer3 = new System.Timers.Timer(mATTimer);
                aTimer3.Elapsed += OnRun2;
                aTimer3.Enabled = true;

            }
            catch (InvalidCastException)
            {
                Console.Write("Resource selected must be a message-based session");
            }
            catch (Exception exp)
            {
                Debug.Write(exp.Message);
            }
                
            
        }

        public void Stop_GPIBCom()
        {
            try
            {

                if (aTimer2 != null)
                {
                    aTimer2.Enabled = false;
                    aTimer2.Dispose();
                }

                if (aTimer3 != null)
                {
                    aTimer3.Enabled = false;
                    aTimer3.Dispose();
                }

                if (sp1.IsOpen)
                    sp1.Close();

                IsConnected = false;
            }
            catch (Exception exp)
            {
                Console.Write(exp.Message);
            }
        }

        private void OnRun1(Object source, ElapsedEventArgs e)
        {
            string[] msg = null;

            //int j;
            //arrCount++;

            ReadRMD();



                    //값 넣을때 바로 넣는게 아니라 이전값 최대 5개까지 값들의 평균값으로 넣어주어야 한다.(노이즈때문)
                    //if (arrCount < 5)
                    //{
                    //    Sum[i] += double.Parse(msg[i]);
                    //    DaqData.sysVarInfo.MEXA9000_Variables[i].IOValue = Sum[i] / (i + 1);
                    //   // Console.Write(AsynchronousSocketListener.sysvarinfo.MEXA9000_Variables[i].NorminalName + " : " + AsynchronousSocketListener.sysvarinfo.MEXA9000_Variables[i].IOValue + "  " + AsynchronousSocketListener.sysvarinfo.MEXA9000_Variables[i].RealValue + "\n");

                    //    sFTime = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
                    //}
                    //else
                    //{
                    //    Sum[i] = 0;
                    //    for (int k = 0; k < 5; k++)
                    //    {
                    //        Sum[i] += arrIOValue[i, k];
                    //    }

                    //    DaqData.sysVarInfo.MEXA9000_Variables[i].IOValue = Sum[i] / 5;
                    //   // Console.Write(AsynchronousSocketListener.sysvarinfo.MEXA9000_Variables[i].NorminalName + " : " + AsynchronousSocketListener.sysvarinfo.MEXA9000_Variables[i].IOValue + "  " + AsynchronousSocketListener.sysvarinfo.MEXA9000_Variables[i].RealValue + "\n");

                    //}

        }
        private void OnRun2(Object source, ElapsedEventArgs e)
        {

            ParsingCMD();

        }

        private void ReadRMD()
        {

            try
            {
                sp1.Write("RMD\r\n");
                System.Threading.Thread.Sleep(100);
                //tmpReadRMD = sp1.ReadExisting();

                sp1.Write("RFS\r\n");

                //tmpRead = sp1.ReadExisting().Replace("\r", "").Split('\n');

                //for (int i = 0; i < tmpRead.Length; i++)
                //{
                //    if (tmpRead[i].Contains("RMD"))
                //    {
                //        strReadRMD = tmpRead[i];
                //    }
                //    if (tmpRead[i].Contains("RFS"))
                //    {
                //        strReadRFS = tmpRead[i];
                //    }
                //}



            }
            catch (Exception exp)
            {
                Console.Write(exp.Message);
            }


        }

        private void ParsingCMD()
        {
            string[] tmpRead = null;

            tmpRead = sp1.ReadExisting().Replace("\r", "").Split('\n');

            for (int i = 0; i < tmpRead.Length; i++)
            {
                if (tmpRead[i].Contains("RMD"))
                {
                    strReadRMD = tmpRead[i];
                }
                if (tmpRead[i].Contains("RFS"))
                {
                    strReadRFS = tmpRead[i];
                }
            }

            try
            {

                if (strReadRMD != null)
                {
                    string[] msg = strReadRMD.Split(',');
                    if (msg != null)
                    {
                        for (int i = 0; i < DaqData.sysVarInfo.MEXA9000_Variables.Count; i++)
                        {
                            //j = arrCount % 5;
                            //arrIOValue[i, j] = double.Parse(msg[i]);
                            if (i <= msg.Length)
                            {
                                DaqData.sysVarInfo.MEXA9000_Variables[i].IOValue = double.Parse(msg[i+2]);
                                sFTime = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
                            }
                        }
                    }
                     strReadRMD = null;
                }

                if (strReadRFS != null)
                {
                    string[] ResultStringRFS = strReadRFS.Split(',');

                    CPUOnOffCheck = ResultStringRFS[1];//00 = CPU Off, 01 = CPU On
                    OperationConditionLine1 = ResultStringRFS[2]; //01=Reset, 05=Measure, 06=Purge
                    strReadRFS = null;
                }

            }
            catch (Exception exp)
            {
                Console.Write(exp.Message);
            }
        }


        private void WriteAOP(string cmd)
        {
            try
            {
                sp1.Write(cmd);// 00=No Operation, 01=Reset, 05=Measure, 06=Purge
                System.Threading.Thread.Sleep(50);
            }
            catch (Exception exp)
            {
                Console.Write(exp.Message);
            }
        }


        public void WriteAOP_Purge()
        {
            string cmd;
            cmd = "AOP,06000000\r\n";
            WriteAOP(cmd);
        }

        public void WriteAOP_Reset()
        {
            string cmd;
            cmd = "AOP,01000000\r\n";
            WriteAOP(cmd);
        }

        public void WriteAOP_Measure()
        {
            string cmd;
            cmd = "AOP,05000000\r\n";
            WriteAOP(cmd);
        }


        private string ReplaceCommonEscapeSequences(string s)
        {
            return (s != null) ? s.Replace("\\n", "\n").Replace("\\r", "\r") : s;
        }

        private string InsertCommonEscapeSequences(string s)
        {
            return (s != null) ? s.Replace("\n", "\\n").Replace("\r", "\\r") : s;
        }

        private void SetupWaitingControlState(bool operationIsInProgress)
        {

        }


    }
}

