﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using NationalInstruments.VisaNS;
using System.Runtime.InteropServices;
using System.Timers;
using AtomUI.Class;

namespace DAQ_GPIB
{
    public class VisaCom
    {

        public string strReadRMD = null;
        public string strReadRFS = null;
        public string[] ParcedstrReadRMD = null;
        public static MessageBasedSession mbSession; //Create Message based session
        public static MessageBasedSession mbSession2;
        public string Lastmsg;
        public string CurrentState;

        public static string CPUOnOffCheck = "";
        public static string OperationConditionLine1 = "";
        public static string sFTime;
        public static bool IsConnected = false;
        public static int arrCount = -1;//노이즈 제거위한 평균값 계산용
        public static double[] Sum = new double[100];//노이즈 제거위한 평균값 계산용. 일단 채널 최대 100개로
        public static double[,] arrIOValue = new double[100, 5];//노이즈 제거위한 평균값 계산용. 일단 채널 최대 100개로


        private static System.Timers.Timer aTimer2;
        private static System.Timers.Timer aTimer3;

        private int mATTimer = 500;


        public VisaCom()
        {

        }
        public void Start_VisaCom()
        {
            string strVISARsrc = "GPIB3::3::INSTR"; // cboVISARsrc.Text; //Get VISA resource ID from combobox 앞의 3은 board넘버. 뒤의 3은 primary adress. secondary adress가 존재하는 경우 GPIB3::3::1::INSTR 이런식.
            try
            {
                mbSession = (MessageBasedSession)ResourceManager.GetLocalManager().Open(strVISARsrc); //[NationalInstruments.VisaNS.GpibSession] = {ResourceName="GPIB3::3::INSTR"}
                mbSession2 = (MessageBasedSession)ResourceManager.GetLocalManager().Open(strVISARsrc);
                //Instantiate and open a Message Based VISA Session
            }
            catch (InvalidCastException)
            {
                Console.Write("Resource selected must be a message-based session");
            }
            catch (Exception exp)
            {

                Console.Write(exp.Message);
            }


            if (mbSession == null)
            {
                Console.Write("GPIB 연결 실패");
            }
            else
            {

                IsConnected = true;

                aTimer2 = new System.Timers.Timer(mATTimer);
                aTimer2.Elapsed += OnRun1;
                aTimer2.Enabled = true;

                aTimer3 = new System.Timers.Timer(mATTimer);
                aTimer3.Elapsed += OnRun2;
                aTimer3.Enabled = true;

            }
        }
        ~VisaCom()
        {
            try
            {
                mbSession.Clear();
                mbSession.Dispose();
                mbSession = null;
                mbSession2.Clear();
                mbSession2.Dispose();
                mbSession2 = null;
                aTimer2.Enabled = false;
                aTimer2.Dispose();

                IsConnected = false;
            }
            catch (Exception exp)
            {
                Console.Write(exp.Message);
            }
        }
        public void Stop_VisaCom()
        {
            try
            {
                mbSession.Clear();
                mbSession.Dispose();
                mbSession = null;
                mbSession2.Clear();
                mbSession2.Dispose();
                mbSession2 = null;
                aTimer2.Enabled = false;
                aTimer2.Dispose();

                IsConnected = false;
            }
            catch (Exception exp)
            {
                Console.Write(exp.Message);
            }
        }


        private void OnRun1(Object source, ElapsedEventArgs e)
        {
            string[] msg = null;

            //int j;
            //arrCount++;

            ReadRMD();

            msg = ParcedstrReadRMD;
            if (msg != null)
            {
                for (int i = 0; i < DaqData.sysVarInfo.MEXA9000_Variables.Count; i++)
                {
                    //j = arrCount % 5;
                    //arrIOValue[i, j] = double.Parse(msg[i]);
                    DaqData.sysVarInfo.MEXA9000_Variables[i].IOValue = double.Parse(msg[i]);
                    sFTime = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");




                    //값 넣을때 바로 넣는게 아니라 이전값 최대 5개까지 값들의 평균값으로 넣어주어야 한다.(노이즈때문)
                    //if (arrCount < 5)
                    //{
                    //    Sum[i] += double.Parse(msg[i]);
                    //    DaqData.sysVarInfo.MEXA9000_Variables[i].IOValue = Sum[i] / (i + 1);
                    //   // Console.Write(AsynchronousSocketListener.sysvarinfo.MEXA9000_Variables[i].NorminalName + " : " + AsynchronousSocketListener.sysvarinfo.MEXA9000_Variables[i].IOValue + "  " + AsynchronousSocketListener.sysvarinfo.MEXA9000_Variables[i].RealValue + "\n");

                    //    sFTime = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
                    //}
                    //else
                    //{
                    //    Sum[i] = 0;
                    //    for (int k = 0; k < 5; k++)
                    //    {
                    //        Sum[i] += arrIOValue[i, k];
                    //    }

                    //    DaqData.sysVarInfo.MEXA9000_Variables[i].IOValue = Sum[i] / 5;
                    //   // Console.Write(AsynchronousSocketListener.sysvarinfo.MEXA9000_Variables[i].NorminalName + " : " + AsynchronousSocketListener.sysvarinfo.MEXA9000_Variables[i].IOValue + "  " + AsynchronousSocketListener.sysvarinfo.MEXA9000_Variables[i].RealValue + "\n");

                    //}

                }
            }



        }
        private void OnRun2(Object source, ElapsedEventArgs e)
        {

            ParsingCMD();

        }

        private void ReadRMD()
        {
            string tmpReadRMD = null;
            string tmpReadRFS = null;



            try
            {
                mbSession.Write("RMD\r\n"); //Send Write command to VISA resource //rmd라는 스트링(명령어)를 보내고 거기에 gpib가 반응하는거,
                mbSession2.Write("RFS\r\n");

                tmpReadRMD = mbSession.ReadString(); //Read string from VISA resource. 여기까지 RMD붙어있는 상태. 데이터A, 나머지것들, 끝에 /r/n도 있다.

                tmpReadRFS = mbSession2.ReadString();

                if (tmpReadRMD.Contains("RMD"))
                {
                    strReadRMD = tmpReadRMD;
                }
                else if (tmpReadRFS.Contains("RFS"))
                {
                    strReadRFS = tmpReadRFS;
                }



            }
            catch (Exception exp)
            {
                Console.Write(exp.Message);
            }


        }

        private void ParsingCMD()
        {
            string sRMDCMD = strReadRMD;
            string sRFSCMD = strReadRFS;
            try
            {

                if (sRMDCMD != null)
                {
                    strReadRMD = null;
                    string[] TempStringRMD = sRMDCMD.Split(',');
                    string[] ResultStringRMD = new string[15];
                    Array.Copy(TempStringRMD, 2, ResultStringRMD, 0, 15);

                    ParcedstrReadRMD = ResultStringRMD;

                }

                if (sRFSCMD != null)
                {
                    strReadRFS = null;
                    string[] ResultStringRFS = sRFSCMD.Split(',');

                    CPUOnOffCheck = ResultStringRFS[1];//00 = CPU Off, 01 = CPU On
                    OperationConditionLine1 = ResultStringRFS[2]; //01=Reset, 05=Measure, 06=Purge

                }

            }
            catch (Exception exp)
            {
                Console.Write(exp.Message);
            }
        }


        private void WriteAOP(string cmd)
        {
            try
            {
                mbSession.Write(cmd);// 00=No Operation, 01=Reset, 05=Measure, 06=Purge
            }
            catch (Exception exp)
            {
                Console.Write(exp.Message);
            }
        }


        public void WriteAOP_Purge()
        {
            string cmd;
            cmd = "AOP,06000000\r\n";
            WriteAOP(cmd);
        }

        public void WriteAOP_Reset()
        {
            string cmd;
            cmd = "AOP,01000000\r\n";
            WriteAOP(cmd);
        }

        public void WriteAOP_Measure()
        {
            string cmd;
            cmd = "AOP,05000000\r\n";
            WriteAOP(cmd);
        }


        private string ReplaceCommonEscapeSequences(string s)
        {
            return (s != null) ? s.Replace("\\n", "\n").Replace("\\r", "\r") : s;
        }

        private string InsertCommonEscapeSequences(string s)
        {
            return (s != null) ? s.Replace("\n", "\\n").Replace("\r", "\\r") : s;
        }

        private void SetupWaitingControlState(bool operationIsInProgress)
        {

        }


    }
}

