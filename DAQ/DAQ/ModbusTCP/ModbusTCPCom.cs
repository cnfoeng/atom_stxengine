﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAQ_ModbusTCPBasic;
using AtomUI.Class;

namespace DAQ_ModbusTCP
{
    public class ModbusTCPCom
    {
        public string sFTime;
        public static bool IsConnected = false;
        public bool FlagModbusTCPThread = false;
        public static bool Connecting = false;
        Master master;

        public void Stop_ModbusTCPCom(){
            FlagModbusTCPThread = false;
            IsConnected = false;

            if (master.connected)
            {
                master.disconnect();
                master.Dispose();
            }
        }

        public void Start_ModbusTCPCom()
        {
            string IP="10.10.0.1";

            foreach (acsConnectionInfo conInfo in Ethercat.sysInfo.HardWareInfo)
            {
                if (conInfo.HWType == HardWareType.MODBUS)
                {
                    IP = conInfo.RemoteAddress;
                }
            }

            master = new Master();
            Connecting = true;
            master.connect(IP, 502);
            Connecting = false;
            master.timeout = 30000;

            if (master.connected)
            {
                IsConnected = true;
                FlagModbusTCPThread = true;
            } 
            else
            {
                IsConnected = false;
                FlagModbusTCPThread = false;
            }

            while (FlagModbusTCPThread && IsConnected)
            {
                OnRun();

                System.Threading.Thread.Sleep(100);
            }
        }

        void OnRun()
        {
            byte[] temp = new byte[244];
            ushort modbusid = 4;
            byte modbusunit = 01;
            ushort modbusstartAddress = 0;
            ushort numinputs = 120;//Length. 읽어들이는 수 지정.
            master.ReadInputRegister(modbusid, modbusunit, modbusstartAddress, numinputs);
            if (master.tcpAsyClBuffer != null)
            {
                Array.Copy(master.tcpAsyClBuffer, 9, temp, 0, 244);//앞 9자리는 헤더. 여기서 잘라냄.
                //ModbusTCP.Master.tcpAsyClBuffer = null;

                for (int i = 0; i < DaqData.sysVarInfo.ModBus_Variables.Count; i++)
                {
                    if (DaqData.sysVarInfo.ModBus_Variables[i].ChannelInfo.ChannelBit / 8 == 2)
                    {
                        if (i > 0 && i < 6)
                        {
                            byte[] tempbyte = new byte[2];
                            Array.Copy(temp, DaqData.sysVarInfo.ModBus_Variables[i].ChannelInfo.Offset * 2, tempbyte, 0, DaqData.sysVarInfo.ModBus_Variables[i].ChannelInfo.ChannelBit / 8);
                            Array.Reverse(tempbyte);
                            DaqData.sysVarInfo.ModBus_Variables[i].IOValue = BitConverter.ToInt16(tempbyte, 0);
                        }
                        else
                        {
                            byte[] tempbyte = new byte[2];
                            Array.Copy(temp, DaqData.sysVarInfo.ModBus_Variables[i].ChannelInfo.Offset * 2, tempbyte, 0, DaqData.sysVarInfo.ModBus_Variables[i].ChannelInfo.ChannelBit / 8);
                            Array.Reverse(tempbyte);
                            DaqData.sysVarInfo.ModBus_Variables[i].IOValue = BitConverter.ToUInt16(tempbyte, 0);
                        }
                    }
                    else if (DaqData.sysVarInfo.ModBus_Variables[i].ChannelInfo.ChannelBit / 8 == 4)
                    {
                        byte[] tempbyte = new byte[4];
                        Array.Copy(temp, DaqData.sysVarInfo.ModBus_Variables[i].ChannelInfo.Offset * 2, tempbyte, 0, DaqData.sysVarInfo.ModBus_Variables[i].ChannelInfo.ChannelBit / 8);
                        Array.Reverse(tempbyte);
                        DaqData.sysVarInfo.ModBus_Variables[i].IOValue = BitConverter.ToUInt32(tempbyte, 0);
                    }
                }

            }

        }
    }
}
