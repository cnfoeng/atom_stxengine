﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using AtomUI.Class;
using DAQ_ACS;
using DAQ_GPIB;
using DAQ_CAN;
using DAQ_ModbusTCP;
using DAQ.SerialComm;
using DAQ.AKProtocol;
// State object for reading client data asynchronously
public class StateObject
{
    // Client  socket.
    public Socket workSocket = null;
    // Size of receive buffer.
    public const int BufferSize = 1024;
    // Receive buffer.
    public byte[] buffer = new byte[BufferSize];
    // Received data string.
    public StringBuilder sb = new StringBuilder();
}

public class AsynchronousSocketListener
{
    public static SysVarInfo sysvarinfo = new SysVarInfo();
    public static ACSCom acscom;
    public static VisaCom visacom;
    public static CANCom cancom;
    public static ModbusTCPCom modbustcpcom;
    public static SmokeMeter smokeMeter;
    public static FuelMeter fuelMeter;
    public static GPIBCom gpibcom;
    public static Osiris osiriscom;

    public static Thread th2;
    public static Thread th3;
    public static Thread th4;
    public static Thread th5;
    public static Thread th6;
    public static Thread th7;
    public static Thread th8;

    // Thread signal.
    public static ManualResetEvent allDone = new ManualResetEvent(false);

    public AsynchronousSocketListener()
    {
    }

    public static void StartListening()
    {

        // Data buffer for incoming data.
        byte[] bytes = new Byte[1024];

        // Establish the local endpoint for the socket.
        // The DNS name of the computer
        // running the listener is "host.contoso.com". 
        IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
        IPAddress ipAddress = ipHostInfo.AddressList[0];
        IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 11000);

        // Create a TCP/IP socket.
        Socket listener = new Socket(AddressFamily.InterNetwork,
            SocketType.Stream, ProtocolType.Tcp);

        // Bind the socket to the local endpoint and listen for incoming connections.
        try
        {
            listener.Bind(localEndPoint);
            listener.Listen(100);

            while (true)
            {
                // Set the event to nonsignaled state.
                allDone.Reset();

                // Start an asynchronous socket to listen for connections.
                Console.WriteLine("Waiting for a connection...");
                listener.BeginAccept(
                    new AsyncCallback(AcceptCallback),
                    listener);

                // Wait until a connection is made before continuing.
                allDone.WaitOne();
            }

        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

        Console.WriteLine("\nPress ENTER to continue...");
        Console.Read();

    }

    public static void AcceptCallback(IAsyncResult ar)
    {
        // Signal the main thread to continue.
        allDone.Set();

        // Get the socket that handles the client request.
        Socket listener = (Socket)ar.AsyncState;
        Socket handler = listener.EndAccept(ar);

        // Create the state object.
        StateObject state = new StateObject();
        state.workSocket = handler;
        handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
            new AsyncCallback(ReadCallback), state);
    }

    public static void ReadCallback(IAsyncResult ar)
    {
        String content = String.Empty;

        // Retrieve the state object and the handler socket
        // from the asynchronous state object.
        StateObject state = (StateObject)ar.AsyncState;
        Socket handler = state.workSocket;

        // Read data from the client socket. 
        int bytesRead = handler.EndReceive(ar);

        if (bytesRead > 0)
        {
            // There  might be more data, so store the data received so far.
            state.sb.Append(Encoding.ASCII.GetString(
                state.buffer, 0, bytesRead));

            // Check for end-of-file tag. If it is not there, read 
            // more data.
            content = state.sb.ToString();
            if (content.IndexOf("<EOF>") > -1)
            {
                // All the data has been read from the 
                // client. Display it on the console.
                Console.WriteLine("Read {0} bytes from socket. \n Data : {1}",
                    content.Length, content);
                // Echo the data back to the client.
                Send(handler, content);
            }
            else
            {
                // Not all data received. Get more.
                handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(ReadCallback), state);
            }
        }
    }

    private static void Send(Socket handler, String data)
    {
        // Convert the string data to byte data using ASCII encoding.
        byte[] byteData = Encoding.ASCII.GetBytes(data);

        // Begin sending the data to the remote device.
        handler.BeginSend(byteData, 0, byteData.Length, 0,
            new AsyncCallback(SendCallback), handler);
    }

    private static void SendCallback(IAsyncResult ar)
    {
        try
        {
            // Retrieve the socket from the state object.
            Socket handler = (Socket)ar.AsyncState;

            // Complete sending the data to the remote device.
            int bytesSent = handler.EndSend(ar);
            Console.WriteLine("Sent {0} bytes to client.", bytesSent);

            handler.Shutdown(SocketShutdown.Both);
            handler.Close();

        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }
    public static void MakeFIle() {

    }

    public static void Run()
    {
        //Thread th1 = new Thread(new ThreadStart(StartListening));//UI로부터 명령 대기하고 있는 스레드.
        //th1.Start();

        sysvarinfo = DaqData.sysVarInfo;

        foreach (acsConnectionInfo info in Ethercat.sysInfo.HardWareInfo)
        {
            if (info.HWType == HardWareType.ACS)
            {
                acscom = new ACSCom();
                th2 = new Thread(new ThreadStart(acscom.Start_ACSCom));//ACS통신 담당 스레드.
                th2.Start();
            }
            else if (info.HWType == HardWareType.MEXA)
            {
                visacom = new VisaCom();//GPIB 담당 스레드.
                th3 = new Thread(new ThreadStart(visacom.Start_VisaCom));
                th3.Start();
            }
            else if (info.HWType == HardWareType.MEXA_SERIAL)
            {
                gpibcom = new GPIBCom();//GPIB 담당 스레드.
                gpibcom.Start_GPIBCom();
                //th3 = new Thread(new ThreadStart(gpibcom.Start_GPIBCom));
                //th3.Start();
            }
            else if (info.HWType == HardWareType.ECU)
            {
                cancom = new CANCom();
                th4 = new Thread(new ThreadStart(cancom.Start_CanCom));
                th4.Start();
            }
            else if (info.HWType == HardWareType.MODBUS)
            {
                modbustcpcom = new ModbusTCPCom();
                th5 = new Thread(new ThreadStart(modbustcpcom.Start_ModbusTCPCom));
                th5.Start();
            }
            else if (info.HWType == HardWareType.SmokeMeter)
            {
                smokeMeter = new SmokeMeter(info.RemoteAddress);
                th6 = new Thread(new ThreadStart(smokeMeter.Start_CommandSend));
                th6.Start();
            }

            else if (info.HWType == HardWareType.FuelMeter)
            {
                fuelMeter = new FuelMeter(info.RemoteAddress);
                th7 = new Thread(new ThreadStart(fuelMeter.Start_CommandSend));
                th7.Start();
            }
            else if (info.HWType == HardWareType.OSIRIS)
            {
                osiriscom = new Osiris();
                th7 = new Thread(new ThreadStart(osiriscom.Start_OsirisCom));
                th7.Start();
            }

        }

    }

    public static void Stop()
    {
        //Thread th1 = new Thread(new ThreadStart(StartListening));//UI로부터 명령 대기하고 있는 스레드.
        //th1.Start();


        sysvarinfo = DaqData.sysVarInfo;

        foreach (acsConnectionInfo info in Ethercat.sysInfo.HardWareInfo)
        {
            if (info.HWType.ToString() == "ACS")
            {
                if (acscom != null)
                    acscom.Stop_ACSCom();
                if (th2 != null)
                {
                    th2.Abort();
                    th2.Join();
                }
            }
            else if (info.HWType.ToString() == "MEXA" || info.HWType.ToString() == "MEXA_SERIAL")
            {
                if (visacom != null)
                    visacom.Stop_VisaCom();
                if (gpibcom != null)
                    gpibcom.Stop_GPIBCom();
                if (th3 != null)
                {
                    th3.Abort();
                    th3.Join();
                }
            }
            else if (info.HWType.ToString() == "ECU")
            {
                if (cancom != null && cancom.CANConnect)
                    cancom.Stop_CanCom();
                if (th4 != null)
                {
                    th4.Abort();
                    th4.Join();
                }
            }
            else if (info.HWType.ToString() == "MODBUS" )
            {
                if (modbustcpcom != null && DAQ_ModbusTCP.ModbusTCPCom.IsConnected || ModbusTCPCom.Connecting)
                    modbustcpcom.Stop_ModbusTCPCom();
                if (th5 != null)
                {
                    th5.Abort();
                    th5.Join(100);
                }
            }
            else if (info.HWType.ToString() == "SmokeMeter")
            {
                if (smokeMeter != null && SmokeMeter.IsConnected )
                {
                    smokeMeter.Stop();
                    smokeMeter = null;
                }
                if (th6 != null)
                {
                    th6.Abort();
                    th6.Join();
                }
            }
            else if (info.HWType.ToString() == "FuelMeter")
            {
                if (fuelMeter != null && FuelMeter.IsConnected)
                {
                    fuelMeter.Stop();
                    fuelMeter = null;
                }
                if (th6 != null)
                {
                    th6.Abort();
                    th6.Join();
                }
            }
            else if (info.HWType.ToString() == "OSIRIS")
            {
                if (osiriscom != null && Osiris.IsConnected || Osiris.Connecting)
                {
                    osiriscom.Stop_OsirisCom();
                    osiriscom = null;
                }
                if (th7 != null)
                {
                    th7.Abort();
                    th7.Join();
                }
            }
        }
    }





     public static int Main(String[] args)
    {
        DaqData.LoadSysInfo();
        Ethercat.LoadSysInfo();
        Run();
        return 0;
        
    }
}