﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.InteropServices;
using System.Timers;
using MonitorIndicator;
using System.IO;
using ACS.SPiiPlusNET;


namespace SPIMode
{
    public partial class SPIMode
    {
        public double testvalue;

        private Api Ch;        
        private static System.Timers.Timer aTimer;

        public delegate void FormSendDataHandler(Object obj, string where);
        public event FormSendDataHandler FormSendEvent;

        public delegate void SendDataHandler(ListView.ListViewItemCollection obj, String name);
        public event SendDataHandler DataSendEvent;

        public delegate void GraphDataHandler(Object obj);
        
        public event GraphDataHandler GraphSendEvent;

        public delegate void SPIFormCloseHandler();
        public event SPIFormCloseHandler SPIFormCloseEvent;

        public delegate void SPIFormBaseUIHandler(ArrayList rpm, ArrayList torque, ArrayList throtle);
        public event SPIFormBaseUIHandler SPIFormBaseUIEvent;

        delegate void SetTextCallback(String[] obj);

        private AnalogChDesc AICHDescription = new AnalogChDesc();        

        private String mStime = "";
        private Object mDataO = null;

        private ArrayList arRpm = new ArrayList();
        private ArrayList arTorque = new ArrayList();
        private ArrayList arThrotle = new ArrayList();

        //Measuring
        private Boolean bMeasureActive = false;
        private int iMeasureTerm = 0;        
        private String sFileSaveName = null;
        private DateTime _edt = DateTime.Now;
        private String sFilePath = @"d:\cnfoeng\MeasuringData\SPI\";

        private int mATTimer = 100;

        private readonly TextBox txt = new TextBox { BorderStyle = BorderStyle.FixedSingle, Visible = false };
        private int listRowIndex = 0;
        private int listColIndex = 0;

        public SPIMode()
        {
            InitializeComponent();
            //this.WindowState = FormWindowState.Maximized;
            this.datalistView.Controls.Add(txt);
            this.datalistView.FullRowSelect = true;
            txt.Leave += (o, e) => txt.Visible = false;            
            txt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lvRecommend_KeyDown);
        }

        private void button1_Click(object sender, EventArgs e)
        {            
            try
            {                
                Ch = new Api();
                object pWait = 0;                

                int Protocol = (int)EthernetCommOption.ACSC_SOCKET_STREAM_PORT;                
                String cIP = "10.0.0.100";
                Ch.OpenCommEthernet(cIP, Protocol);

                this.btnTCPConnect.Enabled = false;
                this.btnTCPDisConnect.Enabled = true;
                this.btnDataRead.Enabled = true;
                this.btnDataReadStop.Enabled = false;
            }
            catch(COMException Ex){
                ErorMsg(Ex); 
            }
       }

        private void ErorMsg(COMException Ex)
        {
            string Str = "Error from " + Ex.Source + "\n\r";
            Str = Str + Ex.Message + "\n\r";
            Str = Str + "HRESULT:" + String.Format("0x{0:X}", (Ex.ErrorCode));
            MessageBox.Show(Str, "EnableEvent");
        }
        public void SPI_Stop()
        {
            if (aTimer != null)
            {
                aTimer.Enabled = false;
                aTimer.Stop();
                aTimer.Close();
            }
            
            try
            {
                if(Ch != null)
                    Ch.CloseComm();    
            }
            catch(System.NullReferenceException)
            {
                MessageBox.Show("비정상 종료가 발생하였습니다.");
            } 
        }

        public void SPI_Run()
        {
            try
            {
                // Create new object of class Channel
                // Type Channel is defined in SPiiPlusCOM650 Type Library
                Ch = new Api();
                object pWait = 0;
                //Establish communication with Simulator
                //Ch.OpenCommSimulator();

                int Protocol = (int)EthernetCommOption.ACSC_SOCKET_STREAM_PORT;
                // Open ethernet communuication.
                // RemoteAddress.Text defines the controller's TCP/IP address.
                // Protocol is TCP/IP in case of network connection, and UDP in case of point-to-point connection.
                String cIP = "10.0.0.100";
                Ch.OpenCommEthernet(cIP, Protocol);

                //Declar global variable   MyMatrix(3)(4) in controller
                //Ch.DeclareVariable(AcsplVariableType.ACSC_INT_TYPE, "MyMatrix(3)(4)");

                this.btnTCPConnect.Enabled = false;
                this.btnTCPDisConnect.Enabled = true;
                this.btnDataRead.Enabled = true;
                this.btnDataReadStop.Enabled = false;

                if(Ch.IsConnected)
                {
                    /*
                    if (aTimer != null)  //data read 버튼. Timer가 정지상태면, start만.
                    {
                        if (!aTimer.Enabled)
                            aTimer.Start();
                    }
                    else
                    */
                    {
                        // 0.5초의 interval을 둔 timer 만들기
                        aTimer = new System.Timers.Timer(mATTimer);

                        // Hook up the Elapsed event for the timer.
                        aTimer.Elapsed += OnRun;
                        aTimer.Enabled = true;
                    }

                    //btn 활성화
                    this.btnTCPConnect.Enabled = false;
                    this.btnTCPDisConnect.Enabled = false;
                    this.btnDataRead.Enabled = false;
                    this.btnDataReadStop.Enabled = true;         
                }
                else
                {
                    MessageBox.Show("연결실패입니다.");
                }
                
            }
            catch (COMException Ex)
            {
                ErorMsg(Ex);

                //btn false
                this.btnTCPConnect.Enabled = true;
                this.btnTCPDisConnect.Enabled = true;
                this.btnDataRead.Enabled = true;
                this.btnDataReadStop.Enabled = false;      
            }
            catch (ACS.SPiiPlusNET.ACSException)
            {
                MessageBox.Show("ATOM 초기화가 실패하였습니다. 장비연결을 다시 확인해 주세요");

                //btn false
                this.btnTCPConnect.Enabled = true;
                this.btnTCPDisConnect.Enabled = true;
                this.btnDataRead.Enabled = true;
                this.btnDataReadStop.Enabled = false;    
            }
               
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (aTimer != null)  //data read 버튼. Timer가 정지상태면, start만.
            {
                if(!aTimer.Enabled)
                    aTimer.Start();                
            }
            else
            {
                // 0.5초의 interval을 둔 timer 만들기
                aTimer = new System.Timers.Timer(200);

                // Hook up the Elapsed event for the timer.
                aTimer.Elapsed += OnRun;
                aTimer.Enabled = true;                
            }        
   
            //btn 활성화
            this.btnTCPConnect.Enabled = false;
            this.btnTCPDisConnect.Enabled = false;
            this.btnDataRead.Enabled = false;
            this.btnDataReadStop.Enabled = true;

            this.Visible = false;
        }

        private void OnRun(Object source, ElapsedEventArgs e)
        {
            object Result;
            Result = Ch.ReadVariable("g_eCAT_AI_Data", ProgramBuffer.ACSC_NONE, -1, -1, -1, -1);                           

            var array = (Result as IEnumerable).Cast<object>().Select(x => x.ToString()).ToArray();              
            doread(array);
        }
        
        private void doread(String[] obj){

            if (this.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(doread);
                this.Invoke(d, new object[] { obj });
            }
            else
            {                    
                String sTime = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                String sFTime = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");                
                //각각 옵션에서 on/off 지정을 해서 사용할수 있게 해야한다. 일단 메인으로 보내고 메인에서 체크해서 뿌릴지 말지 결정해라...                                
                //this.GraphSendEvent(obj); //graph    

                //measuring 
                if (bMeasureActive)
                {
                    if (GetNowTime())
                    {
                        sFileSaveName = sFTime;                        
                        ToSysText(obj, sFileSaveName, "new");
                    }
                    else
                    {
                        if (sFileSaveName != null)
                        {
                            ToSysText(obj, sFileSaveName, "append");
                        }                        
                    }
                }
                
                //for (int i = 0; i < obj.Length; i++) //SPI Viewer update
                for (int i = 0; i < datalistView.Items.Count; i++) 
                {
                    //기존 데이터 수정                    
                    datalistView.Items[i].SubItems[3].Text = sTime;
                    
                    //formula e= g/(32767)*10,0  (V, 소수점 3자리 표시) & f=(d-b)/(c-a)*(e-a)+b
                    
                    /*
                     * 채널 29부터 44까지 1/10으로 계산                     
                     */

                    //voltage min / max ui입력 추가시 로직 변경
                    //dRawData로  double f = (d - b) / (c - a) * (dRawData - a) + b; 바로 입력.
                    //채널별로 분기 불필요.
                    if( i < 28)
                    {                        
                        double dRawData = Convert.ToDouble(obj[i].ToString());

                        //double dEData = (dRawData / 32767) * 10; //voltage 값 추가시 삭제.                        
                        //double result = Math.Round(dEData, 3, MidpointRounding.AwayFromZero);

                        //physical
                        double d = Convert.ToDouble(datalistView.Items[i].SubItems[6].Text); //max-physical
                        double b = Convert.ToDouble(datalistView.Items[i].SubItems[5].Text); //min-physical

                        //voltage
                        double c = Convert.ToDouble(datalistView.Items[i].SubItems[8].Text);//max-voltage
                        double a = Convert.ToDouble(datalistView.Items[i].SubItems[7].Text);//min-voltage

                        double f = (d - b) / (c - a) * (dRawData - a) + b;
                        double fresult = Math.Round(f, 3, MidpointRounding.AwayFromZero);
                        testvalue = fresult;//박해준추가. 테스트용. 삭제필요. 이거 외에 버튼삭제, 버튼 이벤트 삭제 및 최상단 testvalue 선언부분 삭제 필요.

                        datalistView.Items[i].SubItems[4].Text = (fresult.ToString());                   
                    }
                    else
                    {      // 1 ~ 27                  
                        double dRawData = Convert.ToDouble(obj[i].ToString());
                        double dEdata = (dRawData / 10);
                        double fresult = Math.Round(dEdata, 3, MidpointRounding.AwayFromZero);
                        datalistView.Items[i].SubItems[4].Text = (fresult.ToString());                                                      
                    }                               
                }                
                this.DataSendEvent(this.datalistView.Items, "SPI");  //monitoring    
            }            
        }

        private Boolean GetNowTime()
        {
            Boolean bResult = false;            
            DateTime t1 = DateTime.Now; //현재시간.            
            
            TimeSpan gap = (t1 -  _edt);
            int iterm = (int)gap.TotalMinutes;
            //int iterm = (int)gap.TotalSeconds; //test

            if (iterm >= iMeasureTerm)
            //if (iterm > 10) //test
            {
                _edt = t1;
                bResult = true;
            }
            return bResult;
        }

        private String MatrixToString(object Matrix, bool doubleType)
        {
            String Str = "";
            int To1, To2;

            To1 = doubleType ? ((double[,])Matrix).GetUpperBound(0) : ((int[,])Matrix).GetUpperBound(0);
            To2 = doubleType ? ((double[,])Matrix).GetUpperBound(1) : ((int[,])Matrix).GetUpperBound(1);
            for (int i = 0; i <= To1; i++)
            {
                for (int j = 0; j <= To2; j++)
                {
                    Str = Str + (doubleType ? ((double[,])Matrix)[i, j] : ((int[,])Matrix)[i, j]) + " ";
                }
                Str = Str + "\n";
            }
            return Str;
        }

        private String VectorToString(object Vector, bool doubleType)
        {
            String Str = "";
            int To;
            To = doubleType ? ((double[])Vector).GetUpperBound(0) : ((int[])Vector).GetUpperBound(0);
            for (int i = 0; i <= To; i++)
                Str = Str + (doubleType ? ((double[])Vector)[i] : ((int[])Vector)[i]) + " ";
            return Str;
        }


        private void button3_Click(object sender, EventArgs e)
        {
            Ch.CloseComm();            

            //btn
            this.btnTCPConnect.Enabled = true;
            this.btnTCPDisConnect.Enabled = false;
            this.btnDataRead.Enabled = false;
            this.btnDataReadStop.Enabled = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            aTimer.Enabled = false;
            aTimer.Stop();

            //btn
            this.btnTCPConnect.Enabled = false;
            this.btnTCPDisConnect.Enabled = true;
            this.btnDataRead.Enabled = true;
            this.btnDataReadStop.Enabled = false;
        }

        private void CNFSPIModeF_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle;

            //btn
            this.btnTCPConnect.Enabled = true;
            this.btnTCPDisConnect.Enabled = false;
            this.btnDataRead.Enabled = false;
            this.btnDataReadStop.Enabled = false;

            //set listview
            var myListHeader = new List<string>() { "Channel", "Data Label", "Unit",  "Date", "Physical value", "Physical Min", "Physical Max","Voltage Min","Voltage Max" };
            foreach (var vheader in myListHeader)
            {
                datalistView.Columns.Add(vheader);
            }            
            datalistView.HideSelection = false;
            datalistView.Columns[0].Width = 100;
            datalistView.Columns[1].Width = 100;
            datalistView.Columns[2].Width = 100;
            datalistView.Columns[3].Width = 150;
            datalistView.Columns[4].Width = 150;
            datalistView.Columns[5].Width = 100;
            datalistView.Columns[6].Width = 100;
            datalistView.Columns[7].Width = 100;
            datalistView.Columns[8].Width = 100;

            this.datalistView.Items.Clear();
            string spath = @"D:\cnfoeng\setting\info.txt";
            LoadData(spath);

            /*
            for (int i = 0; i < 16; i++)
            {
                ListViewItem lvItem = new ListViewItem();
                lvItem.Text = ("CH " + (i+1)).ToString();
                lvItem.SubItems.Add(AICHDescription.DAC_ChR[0, i, 2].ToString());
                lvItem.SubItems.Add(AICHDescription.DAC_ChR[0, i, 5].ToString());
                lvItem.SubItems.Add("");                
                lvItem.SubItems.Add("");
                lvItem.SubItems.Add(AICHDescription.DAC_ChR[0, i, 3].ToString());
                lvItem.SubItems.Add(AICHDescription.DAC_ChR[0, i, 4].ToString());
                datalistView.Items.Add(lvItem);
            }
            */
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (datalistView.SelectedItems.Count > 0)
            {
                //int mSelRow = datalistView.SelectedItems.Count;
                this.FormSendEvent( this.datalistView.SelectedItems, "SPI");             
            }
            else
            {
                MessageBox.Show("모니터링할 항목을 선택해주세요.!");
            }
        }
        
        //measuring api
        private void ToSysText(String[] splitter, String sTime, String cmd)
        {
            //measuring file name = 측정클래스 네임 + date.txt
            //String sTime = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
            String sFileName = "SPI_" + sTime;            
            String sFileFullName = sFilePath + sFileName + ".txt";            

            if(cmd.Equals("append")){
                string result = "\r\n" + string.Join(".", splitter);
                System.IO.File.AppendAllText(sFileFullName, result, Encoding.Default);
            }
            else //new
            {
                string result = string.Join(".", splitter);
                System.IO.File.WriteAllText(sFileFullName, result, Encoding.Default);
            }
        }

        /*  평균치 연산
         *  지정된 term으로 저장된 파일에 data를 읽어 average값으로 변환하여, data parsing.                  
         */
        private void btnMeasureAverage_Click(object sender, EventArgs e) 
        {
         //   CNFMeasureViewF CNFmvF = new CNFMeasureViewF();
         //   CNFmvF.TopLevel = true;
         //   CNFmvF.Show();

            /*
            //디렉토리 안에 파일쳌,
            if(System.IO.Directory.Exists(sFilePath)){
                System.IO.DirectoryInfo di = new DirectoryInfo(sFilePath);                                

                foreach(var item in di.GetFiles()){
                    //to do
                //    item.CreationTime
                }
            } 
            */
        }


        private void btnMeasureStop_Click(object sender, EventArgs e)
        {
            bMeasureActive = false;
        }
        
        private void lvRecommend_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.datalistView.LabelEdit = true;            
            ListViewHitTestInfo hit = datalistView.HitTest(e.Location);            

            Rectangle rowBounds = hit.SubItem.Bounds;
            Rectangle labelBounds = hit.Item.GetBounds(ItemBoundsPortion.Label);
            int leftMargin = labelBounds.Left - 1;
            txt.Bounds = new Rectangle(rowBounds.Left + leftMargin, rowBounds.Top, rowBounds.Width - leftMargin - 1, rowBounds.Height);
            txt.Text = hit.SubItem.Text;
            txt.SelectAll();
            txt.Visible = true;
            txt.Focus();
            
            Point ptInListView = PointToClient(MousePosition);

            if (hit != null)
            {
                this.datalistView.LabelEdit = true;
                listRowIndex = this.datalistView.FocusedItem.Index;
                listColIndex = hit.Item.SubItems.IndexOf(hit.SubItem);
                //this.datalistView.Items[ListIndex].BeginEdit();
            }               
        }

        private void lvRecommend_MouseClick(object sender, MouseEventArgs e)
        {
              if(e.Button.Equals(MouseButtons.Right))
             {                 
                 ContextMenu m = new ContextMenu();                 
                 MenuItem m1 = new MenuItem();
                 MenuItem m2 = new MenuItem();
                 MenuItem m3 = new MenuItem();
                 MenuItem m4 = new MenuItem();
                 MenuItem m5 = new MenuItem();

                 m1.Text = "선택 아이템 모니터링 하기";
                 m2.Text = "리스트 정보 저장하기";
                 m3.Text = "리스트 정보 불러오기";
                 m4.Text = "행 추가";
                 m5.Text = "행 삭제";
                 
                 m1.Click += (senders, es) =>
                 {
                     if (this.datalistView.SelectedItems.Count > 0)
                     {
                         this.FormSendEvent( this.datalistView.SelectedItems, "SPI");
                         this.Visible = false;
                         for(int i=0; i <this.datalistView.Items.Count; i++)
                         {
                             if (this.datalistView.Items[i].SubItems[1].Text.Equals("Speed"))
                             {
                                 arRpm.Clear();
                                 arRpm.Add(this.datalistView.Items[i].SubItems[5].Text.ToString());
                                 arRpm.Add(this.datalistView.Items[i].SubItems[6].Text.ToString());
                             }

                             if (this.datalistView.Items[i].SubItems[1].Text.Equals("Torque"))
                             {
                                 arTorque.Clear();
                                 arTorque.Add(this.datalistView.Items[i].SubItems[5].Text.ToString());
                                 arTorque.Add(this.datalistView.Items[i].SubItems[6].Text.ToString());
                             }

                             if (this.datalistView.Items[i].SubItems[1].Text.Equals("Throtle"))
                             {
                                 arThrotle.Clear();
                                 arThrotle.Add(this.datalistView.Items[i].SubItems[5].Text.ToString());
                                 arThrotle.Add(this.datalistView.Items[i].SubItems[6].Text.ToString());
                             }                          
                         }

                         this.SPIFormBaseUIEvent(arRpm, arTorque, arThrotle);
                     }
                     else
                     {
                         MessageBox.Show("모니터링할 항목을 선택해주세요.!");
                     }
                 };
                 
                  
                 m2.Click += (senders, es) =>
                 {
                     string spath = @"D:\cnfoeng\setting\info.txt";
                     SaveData(spath);                                        
                 };

                 m3.Click += (senders, es) =>
                 {
                     this.datalistView.Items.Clear();
                     string spath = @"D:\cnfoeng\setting\info.txt";
                     LoadData(spath);
                 };

                 m4.Click += (senders, es) =>
                 {
                     int ito = this.datalistView.Items.Count;
                     //this.datalistView.Items.Add()
                     ListViewItem lvItem = new ListViewItem();
                     lvItem.Text = ("CH " + (ito + 1)).ToString();
                     lvItem.SubItems.Add("");
                     lvItem.SubItems.Add("");
                     lvItem.SubItems.Add("");                
                     lvItem.SubItems.Add("");
                     lvItem.SubItems.Add("");
                     lvItem.SubItems.Add("");
                     lvItem.SubItems.Add("0");
                     lvItem.SubItems.Add("0");
                     datalistView.Items.Add(lvItem);
                 };

                 m5.Click += (senders, es) =>
                 {
                     int ito = this.datalistView.Items.Count;
                     this.datalistView.Items.RemoveAt(ito -1);
                 };

                 m.MenuItems.Add(m1);
                 m.MenuItems.Add(m2);
                 m.MenuItems.Add(m3);
                 m.MenuItems.Add(m4);
                 m.MenuItems.Add(m5);    
                 m.Show(datalistView, new Point(e.X, e.Y));
             }        
        }

        /// <summary>
        /// text 파일을 읽어옵니다.
        /// </summary>
        /// <param name="fileName">읽어올 파일명</param>
        private void LoadData(string fileName)
        {
            // StreamReader를 이용하여 문자판독기를 생성합니다.
            using (TextReader tReader = new StreamReader(fileName))
            {
                // 파일의 내용을 모두 읽어와 줄바꿈을 기준으로 배열형태로 쪼갭니다.
                string[] stringLines
                    = tReader.ReadToEnd().Replace("\n", "").Split((char)Keys.Enter);

                // 한줄씩 가져와서..
                foreach (string stringLine in stringLines)
                {
                    // 빈 문자열이 아니면..
                    if (stringLine != string.Empty)
                    {
                        // 구분자를 이용해서 배열형태로 쪼갭니다.
                        string[] stringArray = stringLine.Split(';','\t');

                        // 아이템을 구성합니다.
                        ListViewItem item = new ListViewItem(stringArray[0]);
                        item.SubItems.Add(stringArray[1]);
                        item.SubItems.Add(stringArray[2]);                        
                        item.SubItems.Add("");
                        item.SubItems.Add("");
                        item.SubItems.Add(stringArray[5]);
                        item.SubItems.Add(stringArray[6]);
                        item.SubItems.Add(stringArray[7]);
                        item.SubItems.Add(stringArray[8]);
                        // ListView에 아이템을 추가합니다.
                        datalistView.Items.Add(item);
                    }
                }
            }
        }

        /// <summary>
        /// text 파일로 저장합니다.
        /// </summary>
        /// <param name="fileName">저장할 파일명</param>
        private void SaveData(string fileName)
        {
            // StreamWriter를 이용하여 문자작성기를 생성합니다.
            using (TextWriter tWriter = new StreamWriter(fileName))
            {
                // ListView의 Item을 하나씩 가져와서..
                foreach (ListViewItem item in datalistView.Items)
                {
                    // 원하는 형태의 문자열로 한줄씩 기록합니다.
                    tWriter.WriteLine(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}"
                        , item.Text, item.SubItems[1].Text, item.SubItems[2].Text, item.SubItems[3].Text, item.SubItems[4].Text, item.SubItems[5].Text, item.SubItems[6].Text, item.SubItems[7].Text, item.SubItems[8].Text));
                }
            }
        }

        private void lvRecommend_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                txt.Visible = false;
                this.datalistView.Items[listRowIndex].SubItems[listColIndex].Text = txt.Text.ToString();
            }
        }        

        private void lvwSrc_ItemDrag(object sender, ItemDragEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            {
                ListViewItem lvItem = new ListViewItem();
                lvItem = (ListViewItem)e.Item;

                String sTemp = "";
                sTemp += lvItem.SubItems[0].Text.ToString();
                sTemp += " : ";
                sTemp += lvItem.SubItems[1].Text.ToString();
                DoDragDrop(sTemp, DragDropEffects.Copy);
            }            
        }
   
        private void SPI_Closed(object sender, FormClosingEventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("동력계 측정을 종료하시겠습니까?", "동력계 측정 종료", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                //do something
                SPI_Stop();
                this.SPIFormCloseEvent();
            }
            else if (dialogResult == DialogResult.No)
            {
                //do something else       
                e.Cancel = true;
            }    

        }


    }
}
