﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using NationalInstruments.VisaNS;
// VISAExample is a small application created to give
// users an example of VISA based instrument communications.
//
// This app utilizes .NET Framework 4.0 and the associated National Instruments VISA
//Common and VISANS
// references.
//
// 1) Enter VISA resource ID
// 2) Press Open button to created new message based VISA session
// 3) Enter instrument specific command in Command Window
// 4) Press Send Button
// 5) If the command is a query (terminating in a '?'), press Read to return
// instrument response
//
// This application is provided as-is. No support or warranty is expressed or implied.
//
// Author: JC
// Company: Rigol Technologies, North America
// Date: 09.09.2011
namespace VisaCom
{
    public partial class VisaCom
    {
        private MessageBasedSession mbSession; //Create Message based session
        public VisaCom()
        {
            InitializeComponent();
        }

        private void btnOpen_Click(object sender, EventArgs e) //Open a VISA Session
        {
            string strVISARsrc = cboVISARsrc.Text; //Get VISA resource ID from combobox
            try
            {
                mbSession = (MessageBasedSession)ResourceManager.GetLocalManager().Open(strVISARsrc);
                //Instantiate and open a Message Based VISA Session
            }
            catch (InvalidCastException)
            {
                MessageBox.Show("Resource selected must be a message-based session");
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }
        private void btnClose_Click(object sender, EventArgs e) //Close VISA session
        {
            try
            {
                mbSession.Dispose(); //
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }
        private void btnWrite_Click(object sender, EventArgs e)
        //Write command to VISA resource
        {
            string strWrite = txtWrite.Text; //Get command from text box
            try
            {
                mbSession.Write(strWrite); //Send Write command to VISA resource
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }



        private void btnRead_Click(object sender, EventArgs e)
        //Read command from VISA resource
        {
            string strRead = null;
            try
            {
                strRead = mbSession.ReadString(); //Read string from VISA resource
                txtRead.Text += strRead;
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}