﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NextUI.Collection;
using NextUI.Component;
using NextUI.Frame;
using System.Windows.Forms.DataVisualization.Charting;

namespace NextUI.BaseUI
{
    /// <summary>
    /// 
    /// </summary>
    public enum NextUIType
    {
        /// <summary>
        /// 
        /// </summary>
        Speedo = 0, 
        /// <summary>
        /// 
        /// </summary>
        HalfSpeedo = 1,
        /// <summary>
        /// 
        /// </summary>
        Graph = 2,
        /// <summary>
        /// 
        /// </summary>
        Digital = 3,
        /// <summary>
        /// 
        /// </summary>
        KnobDigital = 4,
        /// <summary>
        /// 
        /// </summary>
        HorizontalLinear = 5,
        /// <summary>
        /// 
        /// </summary>
        VerticalLinear = 6,
        /// <summary>
        /// 
        /// </summary>
        Lamp = 7,
        /// <summary>
        /// 
        /// </summary>
        Button = 8
    }

    public partial class BaseUI : UserControl
    {
        /// <summary>
        /// 
        /// </summary>
        public bool RePaint = false;
        private Bitmap _map = null;
        private FrameCollection _collection;
        private bool _interactivity = false;
        private Frame.Frame _hitFrame = null;

        private Button btnSetValue = null;
        private TextBox tbSetValue = null;

        private float _Value = 0;
        private DataPoint _PointValue = new DataPoint(0,0);

        private int _BelowDecimalNo = 1;

        private string  ValueFormatString = "#0.0";

        /// <summary>
        /// 숫자판 배경 칼라 - Normal
        /// </summary>
        public Color TextBackGroundColor = Color.FromArgb(0x5a, 0x84, 0xdd);
        /// <summary>
        /// 숫자판 배경 칼라 - Warning
        /// </summary>
        public Color TextBackGroundColorWarning = Color.FromArgb(0xfe, 0xc3, 0x1a);
        /// <summary>
        /// 숫자판 배경 칼라 - Error
        /// </summary>
        public Color TextBackGroundColorError = Color.FromArgb(0xff, 0x00, 0x00);
        /// <summary>
        /// 단위 텍스트 색상
        /// </summary>
        public Color UnitTextColor = Color.FromArgb(0x3b, 0x59, 0x98);
        /// <summary>
        /// 변수 제목 표시 텍스트 색상
        /// </summary>
        public Color TitleTextColor = Color.FromArgb(0x0d, 0x25, 0x56);
        /// <summary>
        /// 숫자 표시 색상
        /// </summary>
        public Color ValueTextColor = Color.FromArgb(0x00, 0x00, 0x00);
        /// <summary>
        /// 전체 배경 색상
        /// </summary>
        public Color BackGroundColor = Color.FromArgb(0xf3, 0xf5, 0xfd);
        /// <summary>
        /// 계기판 색상
        /// </summary>
        public Color ValueScaleColor = Color.FromArgb(0x3b, 0x59, 0x98);

        /// <summary>
        /// 
        /// </summary>
        public int BelowDecimalNo
        {
            get
            {
                return _BelowDecimalNo;
            }
            set
            {
                if (value > 7) value = 7;
                if (value <0) value = 0;

                ValueFormatString = "#0.0000000";
                ValueFormatString = ValueFormatString.Substring(0, ValueFormatString.Length + value - 7);
                _BelowDecimalNo = BelowDecimalNo;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public NextUIType UIType;
        /// <summary>
        /// 
        /// </summary>
        public bool AllowPaint = true;
        /// <summary>
        /// 
        /// </summary>
        public int ID = 0;
        Chart chart1;
        CircularFrame frame;
        //NumericalFrame nframe;
        HorizontalFrame hframe;
        VerticalFrame vframe;

        FrameLabel lblValue;
        FrameLabel lblUnit;

        /// <summary>
        /// 
        /// </summary>
        public string Title = "Dynamo";

        /// <summary>
        /// 
        /// </summary>
        public string Unit = "RPM";

        /// <summary>
        /// 
        /// </summary>
        public int StartValue = 0;

        /// <summary>
        /// 
        /// </summary>
        public int EndValue = 200;

        /// <summary>
        /// 
        /// </summary>
        public int WarningValueLow = 30;

        /// <summary>
        /// 
        /// </summary>
        public int WarningValueHigh = 170;

        /// <summary>
        /// 
        /// </summary>
        public int ErrorValueLow = 20;

        /// <summary>
        /// 
        /// </summary>
        public int ErrorValueHigh = 180;

        /// <summary>
        /// Get or set to enable interactivity for all frame within this baseUI.
        /// When intaractivity is on , all pointer will be able to accept mouse event
        /// </summary>
        public bool Interact
        {
            get { return _interactivity; }
            set
            {
                if (_interactivity != value)
                {
                    _interactivity = value;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (_interactivity)
            {
                if (_hitFrame != null)
                {
                    _hitFrame.OnMouseUp(e);
                    _hitFrame = null;
                }
            }
            base.OnMouseUp(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (_interactivity)
            {
                if (_hitFrame != null)
                {
                    _hitFrame.OnMouseMove(e);
                    this.Invalidate();
                }
            }
            base.OnMouseMove(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (_interactivity)
            {
                for (int i = _collection.Count - 1; i >= 0; i--)
                {
                    if (_collection[i].OnMouseDown(e) == true)
                    {
                        if (_collection[i].GetType() == typeof(CircularFrame))
                            _hitFrame = _collection[i];
                        return;
                    }
                }
            }
            base.OnMouseDown(e);
        }

        /// <summary>
        /// Get a frame collection to add a frame to be rendered
        /// </summary>
        public FrameCollection Frame
        {
            get { return _collection; }
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        public BaseUI()
        {
            InitializeComponent();
            _collection = new FrameCollection();
            _collection.Insert += new OnInsert(_collection_Insert);
        }

        void _collection_Insert(object sender, int index)
        {
            NextUI.Frame.Frame fr = (NextUI.Frame.Frame)sender;
            fr.Parent = this;
        }

        private void BaseUI1_Paint(object sender, PaintEventArgs e)
        {
            if (AllowPaint)
            {
                this.SuspendLayout();

                if (_map == null || _map.Width != this.Width || _map.Height != this.Height || RePaint)
                {

                    switch (this.UIType)
                    {
                        case NextUIType.HorizontalLinear:
                            BaseFrame2();
                            HorizontalLinear();
                            break;
                        case NextUIType.VerticalLinear:
                            BaseFrame2();
                            VerticalLinear();
                            break;
                        case NextUIType.Speedo:
                            BaseFrame();
                            Speedo();
                            break;
                        case NextUIType.HalfSpeedo:
                            BaseFrame();
                            HalfSpeedo();
                            break;
                        case NextUIType.KnobDigital:
                            BaseFrame();
                            KnobDigital();
                            break;
                        case NextUIType.Lamp:
                            BaseFrame();
                            Lamp();
                            break;
                        case NextUIType.Graph:
                            if (chart1 == null)
                            {
                                chart1 = new Chart();
                                this.Controls.Add(chart1);
                                ChartSetup();
                            }
                            else
                            {
                            }
                            ChartFrame();
                            chart1.Location = new Point(6, 26);
                            chart1.Size = new Size(Width - 16, Height - 36);
                            //if (chart1.Series.Count > 0)
                            //    chart1.Series.Clear();
                            break;
                        case NextUIType.Digital:
                            Frame.Clear();
                            VerticalDigital();
                            break;
                    }

                    _map = new Bitmap(this.Width, this.Height);
                    RePaint = false;
                }

                if (true) //(this.UIType != NextUIType.Graph)
                {
                    Graphics g = Graphics.FromImage(_map);
                    g.FillRectangle(new SolidBrush(this.BackColor), new Rectangle(new Point(0, 0), _map.Size));
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

                    g.FillRectangle(new SolidBrush(this.BackColor), new Rectangle(0, 0, this.Width, this.Height));
                    if (this.BackgroundImage != null)
                    {
                        g.DrawImage(this.BackgroundImage, new Rectangle(0, 0, this.Width, this.Height), new Rectangle(0, 0, this.BackgroundImage.Width, this.BackgroundImage.Height), GraphicsUnit.Pixel);
                    }
                    foreach (NextUI.Frame.Frame frame in _collection)
                    {
                        frame.Render(g);
                    }

                    e.Graphics.DrawImage(_map, new Point(0, 0));
                    //base.OnPaint(e);
                    g.Dispose();
                }
                else
                {
                }
                this.ResumeLayout(true);
            }
        }

        private void BaseUI1_SizeChanged(object sender, EventArgs e)
        {
            this.Refresh();
        }

        private void BaseUI1_StyleChanged(object sender, EventArgs e)
        {
            this.Refresh();
        }


        private void VerticalLinear()
        {
            vframe = new VerticalFrame(new Rectangle(0, Height / 10, Width, Height - Height / 10 * 2));

            Frame.Add(vframe);
            Interact = true;
            vframe.FrameRenderer.Outline = NextUI.Renderer.FrameRender.FrameOutline.None;
            vframe.BackRenderer.FillGradientType = NextUI.Renderer.RendererGradient.GradientType.Solid;
            vframe.BackRenderer.CenterColor = BackGroundColor;
            vframe.BackRenderer.EndColor = BackGroundColor;
            vframe.FrameRenderer.MainColor = ValueScaleColor;

            VerticalScaleBar bar = new VerticalScaleBar(vframe);
            vframe.ScaleCollection.Add(bar);
            bar.FillGradientType = NextUI.Renderer.RendererGradient.GradientType.Solid;
            bar.OffsetFromFrame = Width / 2;
            bar.ScaleBarSize = (Height / 70) > 0 ? (Height / 70) : 1;
            bar.FillColor = ValueScaleColor;
            bar.StartValue = StartValue;
            bar.EndValue = EndValue;
            bar.BorderColor = Color.Transparent;
            bar.MinorTicknumber = 4;
            bar.MajorTickNumber = 6;
            bar.TickMinor.EnableGradient = false;
            bar.TickMinor.EnableBorder = false;
            bar.TickMinor.FillColor = ValueScaleColor;
            bar.TickMinor.TickPosition = TickBase.Position.Outer;
            bar.TickMajor.Width = (Height / 100) > 0 ? (Height / 100) : 1;
            bar.TickMajor.Height = (Height / 25) > 0 ? (Height / 25) : 1;
            bar.TickMinor.Width = (Height / 120) > 0 ? (Height / 120) : 1;
            bar.TickMinor.Height = (Height / 40) > 0 ? (Height / 40) : 1;
            bar.TickMajor.TickPosition = TickBase.Position.Outer;
            bar.TickMajor.EnableGradient = false;
            bar.TickMajor.EnableBorder = false;
            bar.TickMajor.FillColor = ValueScaleColor;
            bar.TickLabel.OffsetFromScale = (Height / 40) > 0 ? (Height / 40) : 1;
            bar.TickLabel.LabelPostion = ScaleLabel.Position.Outer;
            bar.TickLabel.LabelFont = new Font(FontFamily.GenericMonospace, (int)(Height / 25), FontStyle.Bold);
            bar.TickLabel.FontColor = ValueScaleColor;

            if (WarningValueHigh < EndValue)
            {
                VerticalRange range = new VerticalRange(vframe);
                range.EnableGradient = false;
                range.StartValue = WarningValueHigh;
                range.EndValue = EndValue;
                range.StartWidth = bar.TickMinor.Height;
                range.EndWidth = bar.TickMinor.Height;
                range.FillColor = TextBackGroundColorWarning;
                range.RangePosition = RangeBase.Position.Outer;
                range.BorderWidth = 0;
                range.BorderColor = Color.Transparent;
                bar.Range.Add(range);
            }
            if (ErrorValueHigh < EndValue)
            {
                VerticalRange range = new VerticalRange(vframe);
                range.EnableGradient = false;
                range.StartValue = ErrorValueHigh;
                range.EndValue = EndValue;
                range.StartWidth = bar.TickMinor.Height;
                range.EndWidth = bar.TickMinor.Height;
                range.FillColor = TextBackGroundColorError;
                range.RangePosition = RangeBase.Position.Outer;
                range.BorderWidth = 0;
                range.BorderColor = Color.Transparent;
                bar.Range.Add(range);
            }

            if (WarningValueLow > StartValue)
            {
                VerticalRange range = new VerticalRange(vframe);
                range.EnableGradient = false;
                range.StartValue = StartValue;
                range.EndValue = WarningValueLow;
                range.StartWidth = bar.TickMinor.Height;
                range.EndWidth = bar.TickMinor.Height;
                range.FillColor = TextBackGroundColorWarning;
                range.RangePosition = RangeBase.Position.Outer;
                range.BorderWidth = 0;
                range.BorderColor = Color.Transparent;
                bar.Range.Add(range);
            }
            if (ErrorValueLow > StartValue)
            {
                VerticalRange range = new VerticalRange(vframe);
                range.EnableGradient = false;
                range.StartValue = StartValue;
                range.EndValue = ErrorValueLow;
                range.StartWidth = bar.TickMinor.Height;
                range.EndWidth = bar.TickMinor.Height;
                range.FillColor = TextBackGroundColorError;
                range.RangePosition = RangeBase.Position.Outer;
                range.BorderWidth = 0;
                range.BorderColor = Color.Transparent;
                bar.Range.Add(range);
            }

            {
                VerticalRange range = new VerticalRange(vframe);
                range.StartWidth = (Height / 15) > 0 ? (Height / 15) : 1;
                range.EndWidth = (Height / 15) > 0 ? (Height / 15) : 1;
                range.StartValue = StartValue;
                range.EndValue = EndValue;
                range.RangePosition = RangeBase.Position.Inner;
                range.BorderWidth = 2;
                range.EnableGradient = false;
                range.BorderColor = ValueScaleColor;
                range.FillColor = TextBackGroundColor;
                bar.Range.Add(range);
            }
            {
                VerticalRange range = new VerticalRange(vframe);
                range.StartWidth = (Height / 15) > 0 ? (Height / 15) : 1;
                range.EndWidth = (Height / 15) > 0 ? (Height / 15) : 1;
                range.StartValue = StartValue;
                range.EndValue = StartValue;
                range.RangePosition = RangeBase.Position.Inner;
                range.BorderWidth = 2;
                range.BorderColor = ValueScaleColor;
                range.FillColor = ValueScaleColor;
                range.EndColor = Color.White;
                range.EnableGradient = true;
                range.Gradient.FillGradientType = Renderer.RendererGradient.GradientType.Center;
                bar.Range.Add(range);
            }

            VerticalPointer pointer = new VerticalPointer(vframe);
            pointer.PointerPosition = VerticalPointer.Position.Inner;
            pointer.BasePointer.FillColor = Color.Red;
            pointer.BasePointer.Length = (Height / 15) > 0 ? (Height / 15) : 1;

            bar.Pointer.Add(pointer);

            FrameLabel lbl = new FrameLabel(new Point((int)(Width / 2) > 0 ? (int)(Width / 2) : 1, ((int)(Height / 20) > 0 ? (int)(Height / 20) : 1)), vframe);
            lbl.LabelFont = new Font(FontFamily.GenericMonospace, (int)(Height / 30), FontStyle.Bold);
            lbl.LabelText = Title;
            lbl.LabelAlignment = HorizontalAlignment.Center;
            lbl.FontColor = TitleTextColor;
            lbl.BackColor = Color.Transparent;

            vframe.FrameLabelCollection.Add(lbl);

            lblUnit = new FrameLabel(new Point((int)(Width - Height / 40), Height - Height / 20 * 2 + 2), vframe);
            lblUnit.LabelFont = new Font(FontFamily.GenericMonospace, (int)(Height / 30), FontStyle.Bold);
            lblUnit.LabelText = Unit;
            lblUnit.LabelAlignment = HorizontalAlignment.Right;
            lblUnit.FontColor = UnitTextColor;
            lblUnit.BackColor = Color.Transparent;

            vframe.FrameLabelCollection.Add(lblUnit);

            lblValue = new FrameLabel(new Point((int)(Height / 40), Height - Height / 20 * 2 + 2), vframe);
            lblValue.LabelFont = new Font(FontFamily.GenericMonospace, (int)(Height / 24), FontStyle.Bold);
            lblValue.LabelText = Value.ToString();
            lblValue.LabelAlignment = HorizontalAlignment.Left;
            lblValue.FontColor = ValueTextColor;
            if (Value < ErrorValueLow || Value > ErrorValueHigh)
                lblValue.BackColor = TextBackGroundColorError;
            else if (Value < WarningValueLow || Value > WarningValueHigh)
                lblValue.BackColor = TextBackGroundColorWarning;
            else
                lblValue.BackColor = TextBackGroundColor;

            vframe.FrameLabelCollection.Add(lblValue);

        }

        private void HorizontalLinear()
        {
            hframe = new HorizontalFrame(new Rectangle(0, 0, Width, Height));

            Frame.Add(hframe);
            Interact = true;
            hframe.FrameRenderer.Outline = NextUI.Renderer.FrameRender.FrameOutline.None;
            hframe.BackRenderer.FillGradientType = NextUI.Renderer.RendererGradient.GradientType.Solid;
            hframe.BackRenderer.CenterColor = BackGroundColor;
            hframe.BackRenderer.EndColor = BackGroundColor;
            hframe.FrameRenderer.MainColor = ValueScaleColor;

            HorizontalScaleBar bar = new HorizontalScaleBar(hframe);
            hframe.ScaleCollection.Add(bar);
            bar.FillGradientType = NextUI.Renderer.RendererGradient.GradientType.Solid;
            bar.ScaleBarSize = (Width / 70) > 0 ? (Width / 70) : 1;
            bar.OffsetFromFrame = Width / 10;
            bar.FillColor = ValueScaleColor;
            bar.StartValue = StartValue;
            bar.EndValue = EndValue;
            bar.BorderColor = Color.Transparent;
            bar.MinorTicknumber = 4;
            bar.MajorTickNumber = 6;
            bar.TickMinor.Width = (Width / 200) > 0 ? (Width / 200) : 1;
            bar.TickMinor.EnableGradient = false;
            bar.TickMinor.EnableBorder = false;
            bar.TickMinor.FillColor = ValueScaleColor;
            bar.TickMinor.TickPosition = TickBase.Position.Outer;
            bar.TickMajor.Height = (Width / 25) > 0 ? (Width / 25) : 1;
            bar.TickMajor.Width = (Width / 100) > 0 ? (Width / 100) : 1;
            bar.TickMinor.Height = (Width / 40) > 0 ? (Width / 40) : 1;
            bar.TickMinor.Width = (Width / 120) > 0 ? (Width / 120) : 1;
            bar.TickMajor.TickPosition = TickBase.Position.Outer;
            bar.TickMajor.EnableGradient = false;
            bar.TickMajor.EnableBorder = false;
            bar.TickMajor.FillColor = ValueScaleColor;
            bar.TickLabel.OffsetFromScale = (Width / 40) > 0 ? (Width / 40) : 1;
            bar.TickLabel.LabelPostion = ScaleLabel.Position.Outer;
            bar.TickLabel.LabelFont = new Font(FontFamily.GenericMonospace, (int)(Width / 20), FontStyle.Bold);
            bar.TickLabel.FontColor = ValueScaleColor;

            if (WarningValueHigh < EndValue)
            {
                LinearRange range = new LinearRange(hframe);
                range.EnableGradient = false;
                range.StartValue = WarningValueHigh;
                range.EndValue = EndValue;
                range.StartWidth = (Width / 25) > 0 ? (Width / 25) : 1;
                range.EndWidth = (Width / 25) > 0 ? (Width / 25) : 1;
                range.FillColor = TextBackGroundColorWarning;
                range.RangePosition = RangeBase.Position.Outer;
                range.BorderWidth = 0;
                range.BorderColor = Color.Transparent;
                bar.Range.Add(range);
            }
            if (ErrorValueHigh < EndValue)
            {
                LinearRange range = new LinearRange(hframe);
                range.EnableGradient = false;
                range.StartValue = ErrorValueHigh;
                range.EndValue = EndValue;
                range.StartWidth = (Width / 25) > 0 ? (Width / 25) : 1;
                range.EndWidth = (Width / 25) > 0 ? (Width / 25) : 1;
                range.RangePosition = RangeBase.Position.Outer;
                range.BorderWidth = 0;
                range.BorderColor = Color.Transparent;
                bar.Range.Add(range);
            }

            if (WarningValueLow > StartValue)
            {
                LinearRange range = new LinearRange(hframe);
                range.EnableGradient = false;
                range.StartValue = StartValue;
                range.EndValue = WarningValueLow;
                range.StartWidth = (Width / 25) > 0 ? (Width / 25) : 1;
                range.EndWidth = (Width / 25) > 0 ? (Width / 25) : 1;
                range.FillColor = TextBackGroundColorWarning;
                range.RangePosition = RangeBase.Position.Outer;
                range.BorderWidth = 0;
                range.BorderColor = Color.Transparent;
                bar.Range.Add(range);
            }
            if (ErrorValueLow > StartValue)
            {
                LinearRange range = new LinearRange(hframe);
                range.EnableGradient = false;
                range.StartValue = StartValue;
                range.EndValue = ErrorValueLow;
                range.StartWidth = (Width / 25) > 0 ? (Width / 25) : 1;
                range.EndWidth = (Width / 25) > 0 ? (Width / 25) : 1;
                range.RangePosition = RangeBase.Position.Outer;
                range.BorderWidth = 0;
                range.BorderColor = Color.Transparent;
                bar.Range.Add(range);
            }

            {
                LinearRange range = new LinearRange(hframe);
                range.StartWidth = (Width / 15) > 0 ? (Width / 15) : 1;
                range.EndWidth = (Width / 15) > 0 ? (Width / 15) : 1;
                range.StartValue = StartValue;
                range.EndValue = EndValue;
                range.RangePosition = RangeBase.Position.Inner;
                range.EnableGradient = false;
                range.BorderWidth = 2;
                range.BorderColor = ValueScaleColor;
                range.FillColor = TextBackGroundColor;
                bar.Range.Add(range);
            }
            {
                LinearRange range = new LinearRange(hframe);
                range.StartWidth = (Width / 15) > 0 ? (Width / 15) : 1;
                range.EndWidth = (Width / 15) > 0 ? (Width / 15) : 1;
                range.StartValue = StartValue;
                range.EndValue = StartValue;
                range.RangePosition = RangeBase.Position.Inner;
                range.BorderWidth = 2;
                range.EnableGradient = true;
                range.BorderColor = ValueScaleColor;
                range.FillColor = ValueScaleColor;
                range.EndColor = Color.White;
                range.Gradient.FillGradientType = Renderer.RendererGradient.GradientType.VerticalCenter;
                bar.Range.Add(range);
            }

            HorizontalPointer pointer = new HorizontalPointer(hframe);
            pointer.PointerPosition = HorizontalPointer.Position.Inner;
            pointer.BasePointer.FillColor = Color.Red;
            pointer.BasePointer.Length = (Width / 15) > 0 ? (Width / 15) : 1;

            bar.Pointer.Add(pointer);

            FrameLabel lbl = new FrameLabel(new Point((int)(Width / 20) > 0 ? (int)(Width / 20) : 1, ((int)(Width / 4.2) > 0 ? (int)(Width / 4.2) : 1) + 2), hframe);
            lbl.LabelFont = new Font(FontFamily.GenericMonospace, (int)(Width / 20), FontStyle.Bold);
            lbl.LabelText = Title;
            lbl.LabelAlignment = HorizontalAlignment.Left;
            lbl.FontColor = TitleTextColor;
            lbl.BackColor = Color.Transparent;

            hframe.FrameLabelCollection.Add(lbl);

            lblUnit = new FrameLabel(new Point((int)(Width * 5 / 7), ((int)(Width / 4.2) > 0 ? (int)(Width / 4.2) : 1) + 2), hframe);
            lblUnit.LabelFont = new Font(FontFamily.GenericMonospace, (int)(Width / 20), FontStyle.Bold);
            lblUnit.LabelText = Unit;
            lblUnit.LabelAlignment = HorizontalAlignment.Left;
            lblUnit.FontColor = UnitTextColor;
            lblUnit.BackColor = Color.Transparent;

            hframe.FrameLabelCollection.Add(lblUnit);

            lblValue = new FrameLabel(new Point((int)(Width * 5 / 7), ((int)(Width / 4.2) > 0 ? (int)(Width / 4.2) : 1) + 2), hframe);
            lblValue.LabelFont = new Font(FontFamily.GenericMonospace, (int)(Width / 16), FontStyle.Bold);
            lblValue.LabelText = Value.ToString();
            lblValue.LabelAlignment = HorizontalAlignment.Right;
            lblValue.FontColor = ValueTextColor;
            if (Value < ErrorValueLow || Value > ErrorValueHigh)
                lblValue.BackColor = TextBackGroundColorError;
            else if (Value < WarningValueLow || Value > WarningValueHigh)
                lblValue.BackColor = TextBackGroundColorWarning;
            else
                lblValue.BackColor = TextBackGroundColor;

            hframe.FrameLabelCollection.Add(lblValue);

        }

        private void KnobDigital()
        {
            Interact = true;
            int width;

            if (Width / Height > 0.6)
                width = (int)(Height * 10.3 / 16);
            else
                width = (int)(Width * 11 / 16);

            frame = new CircularFrame(new Point((int)((Width-width)/2), Height/2 - width /2), width);
            frame.FrameRenderer.Outline = NextUI.Renderer.FrameRender.FrameOutline.None;
            frame.BackRenderer.EndColor = Color.PaleGoldenrod;
            Frame.Add(frame);

            CircularScaleBar bar = new CircularScaleBar(frame);
            bar.FillGradientType = NextUI.Renderer.RendererGradient.GradientType.Solid;
            bar.FillColor = Color.DarkSlateBlue;
            bar.BorderColor = Color.Black;
            bar.StartValue = StartValue;
            bar.EndValue = EndValue;
            bar.OffsetFromFrame = -5;
            bar.StartAngle = 50;
            bar.SweepAngle = 80;
            bar.ScaleBarSize = (int)(width / 30);
            bar.TickMajor.FillColor = Color.Black;
            bar.TickMajor.TickPosition = TickBase.Position.Outer;
            bar.TickMajor.Height = (int)((width / 15) > 1 ? (width / 15) : 1);
            bar.TickMajor.Width = (int)(width / 50);
            bar.TickMinor.Height = (int)(width / 25);
            bar.TickMinor.Width = (int)(width / 60);
            bar.TickMinor.FillColor = Color.Black;
            bar.TickMinor.TickPosition = TickBase.Position.Outer;
            bar.MinorTicknumber = 4;
            bar.MajorTickNumber = 6;
            bar.TickLabel.LabelPostion = ScaleLabel.Position.Outer;
            bar.TickLabel.OffsetFromScale = (int)(width / 8);
            bar.TickLabel.LabelFont = new Font(FontFamily.GenericSansSerif, bar.TickMajor.Height, FontStyle.Bold);

            frame.ScaleCollection.Add(bar);
            bar.Rotate += new CircularScaleBar.OnRotate(bar_Rotate);

            CircularPointer pointer = new CircularPointer(frame);
            pointer.CapPointer.CapType = PointerCapBase.PointerCapType.Type3;
            pointer.CapPointer.FillColor = Color.DarkBlue;
            pointer.CapPointer.Diameter = (int)(width *4.4 /5);
            pointer.CapOnTop = false;
            pointer.BasePointer.PointerShapeType = Pointerbase.PointerType.Type4;
            pointer.BasePointer.Length = pointer.CapPointer.Diameter / 10;
            pointer.BasePointer.OffsetFromCenter = pointer.CapPointer.Diameter / 2 - pointer.CapPointer.Diameter / 10;
            pointer.BasePointer.FillColor = Color.DeepPink;
            pointer.BasePointer.Shadow.Offset = 3;
            pointer.CapPointer.Shadow.Offset = 3;

            bar.Pointer.Add(pointer);


            if (WarningValueHigh < EndValue)
            {
                CircularRange range = new CircularRange(frame);
                range.EnableGradient = false;
                range.StartValue = WarningValueHigh;
                range.EndValue = EndValue;
                range.StartWidth = bar.TickMinor.Height;
                range.EndWidth = bar.TickMinor.Height;
                range.FillColor = Color.PaleGoldenrod;
                range.RangePosition = RangeBase.Position.Outer;

                bar.Range.Add(range);
            }
            if (ErrorValueHigh < EndValue)
            {
                CircularRange range = new CircularRange(frame);
                range.EnableGradient = false;
                range.StartValue = ErrorValueHigh;
                range.EndValue = EndValue;
                range.StartWidth = bar.TickMinor.Height;
                range.EndWidth = bar.TickMinor.Height;
                range.RangePosition = RangeBase.Position.Outer;

                bar.Range.Add(range);
            }

            if (WarningValueLow > StartValue)
            {
                CircularRange range = new CircularRange(frame);
                range.EnableGradient = false;
                range.StartValue = StartValue;
                range.EndValue = WarningValueLow;
                range.StartWidth = bar.TickMinor.Height;
                range.EndWidth = bar.TickMinor.Height;
                range.FillColor = Color.PaleGoldenrod;
                range.RangePosition = RangeBase.Position.Outer;

                bar.Range.Add(range);
            }
            if (ErrorValueLow > StartValue)
            {
                CircularRange range = new CircularRange(frame);
                range.EnableGradient = false;
                range.StartValue = StartValue;
                range.EndValue = ErrorValueLow;
                range.StartWidth = bar.TickMinor.Height;
                range.EndWidth = bar.TickMinor.Height;
                range.RangePosition = RangeBase.Position.Outer;

                bar.Range.Add(range);
            }
            FrameLabel lbl = new FrameLabel(new Point((int)(Width / 2), (int)(width / 16)), hframe);
            lbl.LabelFont = new Font(FontFamily.GenericMonospace, (int)((width / 12) >1? (width / 15) : 1), FontStyle.Bold);
            lbl.LabelText = Title;
            lbl.LabelAlignment = HorizontalAlignment.Center;
            lbl.FontColor = Color.Black;
            lbl.BackColor = Color.Transparent;

            hframe.FrameLabelCollection.Add(lbl);

            lblUnit = new FrameLabel(new Point((int)(Width  -10), (int)(width / 16 * 2)), hframe);
            lblUnit.LabelFont = new Font(FontFamily.GenericMonospace, (int)((width / 12) > 1 ? (width / 15) : 1), FontStyle.Bold);
            lblUnit.LabelText = Unit;
            lblUnit.LabelAlignment = HorizontalAlignment.Right;
            lblUnit.FontColor = Color.Black;
            lblUnit.BackColor = Color.Transparent;

            hframe.FrameLabelCollection.Add(lblUnit);

            lblValue = new FrameLabel(new Point((int)(10), (int)(width / 16 * 2.0)), hframe);
            lblValue.LabelFont = new Font(FontFamily.GenericMonospace, (int)(width / 12), FontStyle.Bold);
            lblValue.LabelText = Value.ToString();
            lblValue.LabelAlignment = HorizontalAlignment.Left;
            lblValue.FontColor = ValueTextColor;
            if (Value < ErrorValueLow || Value > ErrorValueHigh)
                lblValue.BackColor = TextBackGroundColorError;
            else if (Value < WarningValueLow || Value > WarningValueHigh)
                lblValue.BackColor = TextBackGroundColorWarning;
            else
                lblValue.BackColor = TextBackGroundColor;

            hframe.FrameLabelCollection.Add(lblValue);

            if (lblValue != null)
            {
                lblValue.LabelText = Convert.ToString(Value);
                if (Value < ErrorValueLow || Value > ErrorValueHigh)
                    lblValue.BackColor = Color.Red;
                else if (Value < WarningValueLow || Value > WarningValueHigh)
                    lblValue.BackColor = Color.PaleGoldenrod;
                else
                    lblValue.BackColor = Color.White;
            }

            if (tbSetValue == null)
            {
                btnSetValue = new Button();
                tbSetValue = new TextBox();
                btnSetValue.Click += btnSetValue_Click;

                Controls.Add(btnSetValue);
                Controls.Add(tbSetValue);
            }

            tbSetValue.Location = new Point(10, Height - (int)(width * 4 / 16));
            tbSetValue.Size = new Size((int)(width * 8 / 16), (int)(width * 3 / 16));
            tbSetValue.Font = new Font(FontFamily.GenericMonospace, (int)(width / 12), FontStyle.Bold);
            btnSetValue.Location = new Point(Width - (int)(width * 8 / 16) - 10, Height - (int)(width * 4 / 16));
            btnSetValue.Size = new Size(tbSetValue.Width, tbSetValue.Height);
            btnSetValue.Font = new Font(FontFamily.GenericMonospace, (int)(width / 12), FontStyle.Bold);
            btnSetValue.Text = "Set Value";

            Value = 50;

        }

        private void btnSetValue_Click(object sender, EventArgs e)
        {
            float Result = 0;
            if (float.TryParse(tbSetValue.Text, out Result))
            {
                this.Value = Result;
            }
            
        }

        void bar_Rotate(object sender, float value)
        {
            Value = value;
            //if (lblValue != null)
            //{
            //    lblValue.LabelText = Convert.ToString(value);
            //    if (value < ErrorValueLow || value > ErrorValueHigh)
            //        lblValue.BackColor = Color.Red;
            //    else if (value < WarningValueLow || value > WarningValueHigh)
            //        lblValue.BackColor = Color.PaleGoldenrod;
            //    else
            //        lblValue.BackColor = Color.White;
            //}
        }

        private void BaseFrame()
        {
            Frame.Clear();

            hframe = new HorizontalFrame(new Rectangle(0, 0, Width, Height));

            Frame.Add(hframe);
            //Interact = true;
            hframe.FrameRenderer.Outline = NextUI.Renderer.FrameRender.FrameOutline.None;
            hframe.BackRenderer.FillGradientType = NextUI.Renderer.RendererGradient.GradientType.DiagonalLeft;
            hframe.BackRenderer.EndColor = BackGroundColor;
            hframe.BackRenderer.CenterColor = BackGroundColor;

        }

        private void BaseFrame2()
        {
            Frame.Clear();

            hframe = new HorizontalFrame(new Rectangle(0, 0, Width, Height));

            Frame.Add(hframe);
            //Interact = true;
            hframe.FrameRenderer.Outline = NextUI.Renderer.FrameRender.FrameOutline.None;
            hframe.BackRenderer.FillGradientType = NextUI.Renderer.RendererGradient.GradientType.DiagonalLeft;
            hframe.BackRenderer.EndColor = ValueScaleColor;
            hframe.BackRenderer.CenterColor = ValueScaleColor;

        }

        private void ChartFrame()
        {
            Frame.Clear();

            HorizontalFrame frame = new HorizontalFrame(new Rectangle(0, 0, Width, Height));

            Frame.Add(frame);
            //Interact = true;
            frame.FrameRenderer.Outline = NextUI.Renderer.FrameRender.FrameOutline.None;
            frame.BackRenderer.FillGradientType = NextUI.Renderer.RendererGradient.GradientType.DiagonalLeft;
            frame.BackRenderer.CenterColor = Color.White;
            frame.BackRenderer.EndColor = Color.PaleGoldenrod;

            FrameLabel lbl = new FrameLabel(new Point(6, 0), frame);
            lbl.LabelFont = new Font(FontFamily.GenericMonospace, 12, FontStyle.Bold);
            lbl.LabelText = "Time Data Plot";//Title;
            lbl.LabelAlignment = HorizontalAlignment.Left;
            lbl.FontColor = Color.Black;
            lbl.BackColor = Color.Transparent;

            frame.FrameLabelCollection.Add(lbl);
        }

        private void ChartSetup()
        {
            //X축 데이터 타입
            chart1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(223)))), ((int)(((byte)(193)))));
            chart1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(64)))), ((int)(((byte)(1)))));
            this.chart1.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.chart1.BorderlineWidth = 2;

            chart1.ChartAreas.Add(new ChartArea());
            chart1.ChartAreas[0].BackColor = System.Drawing.Color.OldLace;
            chart1.ChartAreas[0].BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.TopBottom;
            chart1.ChartAreas[0].BackSecondaryColor = System.Drawing.Color.White;
            chart1.ChartAreas[0].BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chart1.ChartAreas[0].AxisY2.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.DashDotDot;
            chart1.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chart1.ChartAreas[0].AxisY2.MajorGrid.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));

            chart1.ChartAreas[0].CursorX.LineColor = Color.Gray;
            chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;

            chart1.Series.Clear();
            chart1.Series.Add(this.Title);

            chart1.Series[0].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            //Y축 데이터 타입
            chart1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;  //분산형 그래프
            chart1.Series[0].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            //chart1.Series[1].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;

            //차트 타입
            chart1.Series[0].IsXValueIndexed = false;
            chart1.Series[0].Color = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(65)))), ((int)(((byte)(140)))), ((int)(((byte)(240)))));
        }

        private void HalfSpeedo()
        {
            int width;

            if (Width - 10 > Height * 1.4)
                width = (int)(Height * 1.4);
            else
                width = Width -10;
            CircularFrame righframe = new CircularFrame(new Point((Width - width) / 2, 0), width);
            this.Frame.Add(righframe);
            this.frame = righframe;
            frame.BackRenderer.CenterColor = BackGroundColor;
            frame.BackRenderer.EndColor = BackGroundColor;
            frame.FrameRenderer.MainColor = ValueScaleColor;
            righframe.FrameRenderer.Outline = NextUI.Renderer.FrameRender.FrameOutline.Type3;
            righframe.Type = CircularFrame.FrameType.HalfCircle1;

            CircularScaleBar rightbar = new CircularScaleBar(righframe);
            rightbar.FillGradientType = NextUI.Renderer.RendererGradient.GradientType.Solid;
            rightbar.FillColor = ValueScaleColor;
            rightbar.ScaleBarSize = (int)(width / 30); 
            rightbar.TickMajor.FillColor = ValueScaleColor;
            rightbar.TickMajor.TickPosition = TickBase.Position.Inner;
            rightbar.TickMajor.Height = (int)(width / 20);
            rightbar.TickMajor.Width = (int)(width / 50);
            rightbar.TickMajor.EnableGradient = false;
            rightbar.TickMajor.EnableBorder = false;
            rightbar.TickMajor.Type = TickBase.TickType.RoundedRect;
            rightbar.TickMinor.Height = (int)(width / 40);
            rightbar.TickMinor.Width = (int)(width / 60);
            rightbar.TickMinor.FillColor = ValueScaleColor;
            rightbar.TickMinor.TickPosition = TickBase.Position.Inner;
            rightbar.TickMinor.EnableGradient = false;
            rightbar.TickMinor.EnableBorder = false;
            rightbar.MinorTicknumber = 4;
            rightbar.MajorTickNumber = 6;
            rightbar.StartValue = StartValue;
            rightbar.EndValue = EndValue;
            //rightbar.CustomLabel = new string[] { "1", "2", "3", "4", "5", "6", "7", "8" };
            rightbar.SweepAngle = 180;
            rightbar.StartAngle = 0;
            rightbar.TickLabel.TextDirection = CircularLabel.Direction.Horizontal;
            rightbar.TickLabel.OffsetFromScale = (int)(width / 8);
            rightbar.TickLabel.LabelFont = new Font(FontFamily.GenericMonospace, (int)(width / 20), FontStyle.Bold);
            rightbar.TickLabel.FontColor = ValueScaleColor;
            righframe.ScaleCollection.Add(rightbar);

            if (WarningValueHigh < EndValue)
            {
                CircularRange range = new CircularRange(frame);
                range.EnableGradient = false;
                range.StartValue = WarningValueHigh;
                range.EndValue = EndValue;
                range.StartWidth = ((int)(width / 20) == 0 ? 1 : (int)(width / 20));
                range.EndWidth = ((int)(width / 20) == 0 ? 1 : (int)(width / 20));
                range.FillColor = TextBackGroundColorWarning;
                range.RangePosition = RangeBase.Position.Inner;

                rightbar.Range.Add(range);
            }
            if (ErrorValueHigh < EndValue)
            {
                CircularRange range = new CircularRange(frame);
                range.EnableGradient = false;
                range.StartValue = ErrorValueHigh;
                range.EndValue = EndValue;
                range.StartWidth = ((int)(width / 20) == 0 ? 1 : (int)(width / 20));
                range.EndWidth = ((int)(width / 20) == 0 ? 1 : (int)(width / 20));
                range.RangePosition = RangeBase.Position.Inner;
                range.FillColor = Color.Red;

                rightbar.Range.Add(range);
            }

            if (WarningValueLow > StartValue)
            {
                CircularRange range = new CircularRange(frame);
                range.EnableGradient = false;
                range.StartValue = StartValue;
                range.EndValue = WarningValueLow;
                range.StartWidth = ((int)(width / 20) == 0 ? 1 : (int)(width / 20));
                range.EndWidth = ((int)(width / 20) == 0 ? 1 : (int)(width / 20));
                range.FillColor = TextBackGroundColorWarning;
                range.RangePosition = RangeBase.Position.Inner;

                rightbar.Range.Add(range);
            }
            if (ErrorValueLow > StartValue)
            {
                CircularRange range = new CircularRange(frame);
                range.EnableGradient = false;
                range.StartValue = StartValue;
                range.EndValue = ErrorValueLow;
                range.StartWidth = ((int)(width / 20) == 0 ? 1 : (int)(width / 20));
                range.EndWidth = ((int)(width / 20) == 0 ? 1 : (int)(width / 20));
                range.FillColor = Color.Red;
                range.RangePosition = RangeBase.Position.Inner;

                rightbar.Range.Add(range);
            }

            CircularPointer rightpointer = new CircularPointer(righframe);
            rightpointer.BasePointer.Length = ((int)(width / 2.2) == 0 ? 1 : (int)(width / 2.2));
            rightpointer.BasePointer.Width = ((int)(width / 20) == 0 ? 1 : (int)(width / 20));
            rightpointer.BasePointer.FillColor = Color.Red;
            rightpointer.BasePointer.PointerShapeType = Pointerbase.PointerType.Type1;
            rightpointer.BasePointer.OffsetFromCenter = -((int)(width / 15) == 0 ? 1 : (int)(width / 15));
            rightpointer.CapOnTop = false;
            rightpointer.CapPointer.FillColor = ValueScaleColor;
            rightpointer.CapPointer.BorderColor = ValueScaleColor;
            rightbar.Pointer.Add(rightpointer);

            FrameLabel lbl = new FrameLabel(new Point((int)(Width / 2), (int)(width / 8 * 3.3)), frame);
            lbl.LabelFont = new Font(FontFamily.GenericMonospace, (int)(width / 20), FontStyle.Bold);
            lbl.LabelText = Title;
            lbl.LabelAlignment = HorizontalAlignment.Center;
            lbl.FontColor = TitleTextColor;
            lbl.BackColor = Color.Transparent;

            frame.FrameLabelCollection.Add(lbl);

            lblUnit = new FrameLabel(new Point((int)(Width / 2), (int)(width / 8 * 2.7)), frame);
            lblUnit.LabelFont = new Font(FontFamily.GenericMonospace, (int)(width / 20), FontStyle.Bold);
            lblUnit.LabelText = Unit;
            lblUnit.LabelAlignment = HorizontalAlignment.Center;
            lblUnit.FontColor = UnitTextColor;
            lblUnit.BackColor = Color.Transparent;

            frame.FrameLabelCollection.Add(lblUnit);

            lblValue = new FrameLabel(new Point((int)(Width / 2), (int)(width / 8 * 5.0)), frame);
            lblValue.LabelFont = new Font(FontFamily.GenericMonospace, (int)(width / 15), FontStyle.Bold);
            lblValue.LabelText = Value.ToString();
            lblValue.LabelAlignment = HorizontalAlignment.Center;
            lblValue.FontColor = ValueTextColor;
            if (Value < ErrorValueLow || Value > ErrorValueHigh)
                lblValue.BackColor = TextBackGroundColorError;
            else if (Value < WarningValueLow || Value > WarningValueHigh)
                lblValue.BackColor = TextBackGroundColorWarning;
            else
                lblValue.BackColor = TextBackGroundColor;

            righframe.FrameLabelCollection.Add(lblValue);
        }

        private void VerticalDigital()
        {
            int _Left = 4; int _Top = 0; int _Width = 0; int _Height = 0;
            int _FrameWidth = 5; int _FontThickness = 5;
            _Top = Height * 2 / 5;
            _Width = Width - 12;
            _Height = Height * 3 / 5 - 8;
            _FrameWidth = (int)(Height / 40);
            _FontThickness = (int)((Height + Width) / 60) > 0 ? (int)((Height + Width) / 90) : 1;

            Digital(_Left, _Top, _Width, _Height, _FrameWidth, _FontThickness);
        }

        private void HorizontalDigital()
        {
            int _Left = 0; int _Top = 0; int _Width = 0; int _Height = 0;
            int _FrameWidth = 5; int _FontThickness = 5;
            _Left = (int)(Width / 4);
            _Top = 4;
            _Width = Width/2;
            _Height = Height-4;

            _FrameWidth = (int)(Height / 20);
            _FontThickness = (int)((Height + Width) / 60) > 0 ? (int)((Height + Width) / 60) : 1;


            Digital(_Left, _Top, _Width, _Height, _FrameWidth, _FontThickness);
        }

        private void Digital(int _Left, int _Top, int _Width, int _Height, int _FrameWidth, int _FontThickness)
        {
            Frame.Clear();

            HorizontalFrame frame = new HorizontalFrame(new Rectangle(0, 0, Width, Height));

            Frame.Add(frame);
            //Interact = true;
            frame.FrameRenderer.Outline = NextUI.Renderer.FrameRender.FrameOutline.None;
            frame.BackRenderer.FillGradientType = NextUI.Renderer.RendererGradient.GradientType.Solid;
            frame.BackRenderer.CenterColor = BackGroundColor;
            frame.BackRenderer.EndColor = BackGroundColor;

            //HorizontalFrame frame2 = new HorizontalFrame(new Rectangle(3, Height * 2 / 5 + 3, Width - 14, Height * 3 / 5 - 14));

            //Frame.Add(frame2);
            ////Interact = true;
            //frame.FrameRenderer.Outline = NextUI.Renderer.FrameRender.FrameOutline.None;
            //frame.BackRenderer.FillGradientType = NextUI.Renderer.RendererGradient.GradientType.Solid;
            //frame.BackRenderer.CenterColor = Color.White;
            //frame.BackRenderer.EndColor = Color.White;

            hframe = new HorizontalFrame(new Rectangle(4, Height * 2 / 5 + 4, Width - 12, Height * 3 / 5 - 12));

            Frame.Add(hframe);
            //Interact = true;
            hframe.FrameRenderer.Outline = NextUI.Renderer.FrameRender.FrameOutline.Type1;
            hframe.FrameRenderer.MainColor = ValueScaleColor;
            hframe.FrameRenderer.FrameWidth = 5;

            hframe.BackRenderer.FillGradientType = NextUI.Renderer.RendererGradient.GradientType.Solid;
            if (Value < ErrorValueLow || Value > ErrorValueHigh)
                hframe.BackRenderer.CenterColor = TextBackGroundColorError;
            else if (Value < WarningValueLow || Value > WarningValueHigh)
                hframe.BackRenderer.CenterColor = TextBackGroundColorWarning;
            else
                hframe.BackRenderer.CenterColor = TextBackGroundColor;


            lblValue = new FrameLabel(new Point(_Left + _Width, _Top + _Height/2 - (int)(Height / 10)), hframe);
            lblValue.LabelFont = new Font(FontFamily.GenericSansSerif, (int)(Height / 6), FontStyle.Bold);
            lblValue.LabelText = Value.ToString();
            lblValue.LabelAlignment = HorizontalAlignment.Right;
            lblValue.FontColor = ValueTextColor;

            lblValue.BackColor = Color.Transparent;
            hframe.FrameLabelCollection.Add(lblValue);

            //nframe = new NumericalFrame(new Rectangle(_Left, _Top, _Width, _Height));
            //Frame.Add(nframe);
            //nframe.FrameRenderer.Outline = NextUI.Renderer.FrameRender.FrameOutline.Type1;
            //nframe.FrameRenderer.MainColor = ValueScaleColor;
            //nframe.FrameRenderer.FrameWidth = _FrameWidth;

            //for (int i = 0; i < 6; i++)
            //{
            //    NextUI.Component.
            //    DigitalPanel7Segment seg = new DigitalPanel7Segment(nframe);
            //    nframe.Indicator.Panels.Add(seg);
            //    //seg.EnableGradient = false;
            //    //seg.GradientType = NextUI.Renderer.RendererGradient.GradientType.VerticalCenter;
            //    //seg.EndColor = Color.Black;
            //    seg.BackColor = Color.FromArgb(0x5a, 0x84, 0xdd);
            //    seg.MainColor = Color.Black;
            //    seg.BorderWidth = 1;
            //    seg.BorderColor = Color.FromArgb(0x5a, 0x84, 0xdd);
            //    seg.Position = DigitalPanel7Segment.Direction.Normal;
            //    seg.FontThickness = _FontThickness;
            //    seg.EnableBorder = true;
            //    seg.BackOpacity = 0;
            //}

            FrameLabel lbl = new FrameLabel(new Point(4, 4), frame);
            lbl.LabelFont = new Font(FontFamily.GenericSansSerif, (int)(Height / 7), FontStyle.Bold);
            lbl.LabelText = Title;
            lbl.LabelAlignment = HorizontalAlignment.Left;
            lbl.FontColor = TitleTextColor;
            lbl.BackColor = Color.Transparent;

            frame.FrameLabelCollection.Add(lbl);

            lblUnit = new FrameLabel(new Point(Width - 4, 4), frame);
            lblUnit.LabelFont = new Font(FontFamily.GenericSansSerif, (int)(Height /7), FontStyle.Bold);
            lblUnit.LabelText = Unit;
            lblUnit.LabelAlignment = HorizontalAlignment.Right;
            lblUnit.FontColor = UnitTextColor;
            lblUnit.BackColor = Color.Transparent;

            frame.FrameLabelCollection.Add(lblUnit);

        }

        private void Lamp()
        {

            Frame.Clear();

            HorizontalFrame frame = new HorizontalFrame(new Rectangle(0, 0, Width, Height));

            Frame.Add(frame);
            //Interact = true;
            frame.FrameRenderer.Outline = NextUI.Renderer.FrameRender.FrameOutline.None;
            frame.BackRenderer.FillGradientType = NextUI.Renderer.RendererGradient.GradientType.Solid;
            frame.BackRenderer.CenterColor = BackGroundColor;
            frame.BackRenderer.EndColor = BackGroundColor;


            hframe = new HorizontalFrame(new Rectangle(4, Height * 2 / 5 + 4, Width - 12, Height * 3 /5-12));

            Frame.Add(hframe);
            //Interact = true;
            hframe.FrameRenderer.Outline = NextUI.Renderer.FrameRender.FrameOutline.None;
            hframe.BackRenderer.FillGradientType = NextUI.Renderer.RendererGradient.GradientType.None;

            FrameLabel lbl = new FrameLabel(new Point(Width/2, Height / 5), frame);
            lbl.LabelFont = new Font(FontFamily.GenericMonospace, (int)((Width + Height) / 15), FontStyle.Bold);
            lbl.LabelText = Title;
            lbl.LabelAlignment = HorizontalAlignment.Center;
            lbl.FontColor = TitleTextColor;
            lbl.BackColor = Color.Transparent;

            frame.FrameLabelCollection.Add(lbl);

        }

        private void Speedo()
        {
            int width;

            if (Width > Height)
                width = Height;
            else
                width = Width;

            frame = new CircularFrame(new Point(2, 2), width-8);
            Frame.Add(frame);
            frame.BackRenderer.CenterColor = BackGroundColor;
            frame.BackRenderer.EndColor = BackGroundColor;
            frame.FrameRenderer.MainColor = ValueScaleColor;
            frame.FrameRenderer.Outline = NextUI.Renderer.FrameRender.FrameOutline.Type3;

            CircularScaleBar bar = new CircularScaleBar(frame);
            bar.FillGradientType = NextUI.Renderer.RendererGradient.GradientType.Solid;
            bar.ScaleBarSize = (int)(width/30);
            bar.FillColor = ValueScaleColor;
            bar.StartValue = StartValue;
            bar.EndValue = EndValue;
            bar.StartAngle = 50;
            bar.SweepAngle = 80;
            bar.MajorTickNumber = 6;
            bar.MinorTicknumber = 4;
            bar.TickMajor.EnableGradient = false;
            bar.TickMajor.EnableBorder = false;
            bar.TickMajor.Type = TickBase.TickType.RoundedRect;
            bar.TickMajor.FillColor = ValueScaleColor;
            bar.TickMajor.Height = (int)(width / 20);
            bar.TickMajor.Width = (int)(width / 50);
            bar.TickMinor.Height = (int)(width / 40);
            bar.TickMinor.Width = (int)(width / 60);
            bar.TickMinor.EnableGradient = false;
            bar.TickMinor.EnableBorder = false;
            bar.TickMinor.FillColor = ValueScaleColor;
            bar.TickMajor.TickPosition = TickBase.Position.Inner;
            bar.TickMinor.TickPosition = TickBase.Position.Inner;
            bar.TickLabel.TextDirection = CircularLabel.Direction.Horizontal;
            bar.TickLabel.OffsetFromScale = (int)(width / 8);
            bar.TickLabel.LabelFont = new Font(FontFamily.GenericMonospace, (int)(width / 20), FontStyle.Bold);
            bar.TickLabel.FontColor = ValueScaleColor;

            frame.ScaleCollection.Add(bar);
            //frame.ScaleCollection.Add(bar2);


            if (WarningValueHigh < EndValue)
            {
                CircularRange range = new CircularRange(frame);
                range.EnableGradient = false;
                range.StartValue = WarningValueHigh;
                range.EndValue = EndValue;
                range.StartWidth = ((int)(width / 20) == 0 ? 1 : (int)(width / 20));
                range.EndWidth = ((int)(width / 20) == 0 ? 1 : (int)(width / 20));
                range.FillColor = TextBackGroundColorWarning;
                range.RangePosition = RangeBase.Position.Inner;

                bar.Range.Add(range);
            }
            if (ErrorValueHigh < EndValue)
            {
                CircularRange range = new CircularRange(frame);
                range.EnableGradient = false;
                range.StartValue = ErrorValueHigh;
                range.EndValue = EndValue;
                range.StartWidth = ((int)(width / 20) == 0 ? 1 : (int)(width / 20));
                range.EndWidth = ((int)(width / 20) == 0 ? 1 : (int)(width / 20));
                range.FillColor = TextBackGroundColorError;
                range.RangePosition = RangeBase.Position.Inner;

                bar.Range.Add(range);
            }

            if (WarningValueLow > StartValue)
            {
                CircularRange range = new CircularRange(frame);
                range.EnableGradient = false;
                range.StartValue = StartValue;
                range.EndValue = WarningValueLow;
                range.StartWidth = ((int)(width / 20) == 0 ? 1 : (int)(width / 20));
                range.EndWidth = ((int)(width / 20) == 0 ? 1 : (int)(width / 20));
                range.FillColor = TextBackGroundColorWarning;
                range.RangePosition = RangeBase.Position.Inner;

                bar.Range.Add(range);
            }
            if (ErrorValueLow > StartValue)
            {
                CircularRange range = new CircularRange(frame);
                range.EnableGradient = false;
                range.StartValue = StartValue;
                range.EndValue = ErrorValueLow;
                range.StartWidth = ((int)(width / 20) == 0 ? 1 : (int)(width / 20));
                range.EndWidth = ((int)(width / 20) == 0 ? 1 : (int)(width / 20));
                range.FillColor = TextBackGroundColorError;
                range.RangePosition = RangeBase.Position.Inner;

                bar.Range.Add(range);
            }

            CircularPointer pointer = new CircularPointer(frame);
            pointer.CapPointer.Visible = true;
            pointer.CapOnTop = false;
            pointer.BasePointer.Length = ((int)(width / 2.2) == 0 ? 1 : (int)(width / 2.2));
            pointer.BasePointer.Width = ((int)(width / 20) == 0 ? 1 : (int)(width / 20));
            pointer.BasePointer.FillColor = Color.Red;
            pointer.BasePointer.PointerShapeType = Pointerbase.PointerType.Type1;
            pointer.BasePointer.OffsetFromCenter = -((int)(width / 15) == 0 ? 1 : (int)(width / 15));

            bar.Pointer.Add(pointer);

            FrameLabel lbl = new FrameLabel(new Point((int)(width / 2), (int)(width / 8 * 3.3)), frame);
            lbl.LabelFont = new Font(FontFamily.GenericMonospace, (int)(width / 20), FontStyle.Bold);
            lbl.LabelText = Title;
            lbl.LabelAlignment = HorizontalAlignment.Center;
            lbl.FontColor = TitleTextColor;
            lbl.BackColor = Color.Transparent;
            frame.FrameLabelCollection.Add(lbl);
            lblValue = new FrameLabel(new Point((int)(width / 2), (int)(width / 8 * 6.5)), frame);
            lblValue.LabelFont = new Font(FontFamily.GenericMonospace, (int)(width / 15), FontStyle.Bold);
            lblValue.LabelText = Value.ToString();
            lblValue.LabelAlignment = HorizontalAlignment.Center;
            lblValue.FontColor = ValueTextColor;
            if (Value < ErrorValueLow || Value > ErrorValueHigh)
                lblValue.BackColor = TextBackGroundColorError;
            else if (Value < WarningValueLow || Value > WarningValueHigh)
                lblValue.BackColor = TextBackGroundColorWarning;
            else
                lblValue.BackColor = TextBackGroundColor;
            frame.FrameLabelCollection.Add(lblValue);

            lblUnit = new FrameLabel(new Point((int)(width / 2), (int)(width / 8 * 2.7)), frame);
            lblUnit.LabelFont = new Font(FontFamily.GenericMonospace, (int)(width / 20), FontStyle.Bold);
            lblUnit.LabelText = Unit;
            lblUnit.LabelAlignment = HorizontalAlignment.Center;
            lblUnit.FontColor = UnitTextColor;
            lblUnit.BackColor = Color.Transparent;

            frame.FrameLabelCollection.Add(lblUnit);
        }

        /// <summary>
        ///  Real Value
        /// </summary>
        public float Value
        {
            get
            {
                return _Value;
            }
            set
            {
                _Value = value;
                switch (this.UIType)
                {
                    case NextUIType.KnobDigital:
                        _Value = (float)Math.Round((float)_Value, 1);
                        if (frame != null)
                            frame.ScaleCollection[0].Pointer[0].Value = _Value;
                        if (lblValue != null)
                        {
                            lblValue.LabelText = value.ToString(ValueFormatString);
                            if (value < ErrorValueLow || value > ErrorValueHigh)
                                lblValue.BackColor = Color.Red;
                            else if (value < WarningValueLow || value > WarningValueHigh)
                                lblValue.BackColor = Color.PaleGoldenrod;
                            else
                                lblValue.BackColor = Color.White;
                        }
                        break;
                    case NextUIType.Speedo:
                        if (frame != null)
                            frame.ScaleCollection[0].Pointer[0].Value = value;
                        if (lblValue != null)
                        {
                            lblValue.LabelText = value.ToString(ValueFormatString);
                            if (value < ErrorValueLow || value > ErrorValueHigh)
                                lblValue.BackColor = TextBackGroundColorError;
                            else if (value < WarningValueLow || value > WarningValueHigh)
                                lblValue.BackColor = TextBackGroundColorWarning;
                            else
                                lblValue.BackColor = TextBackGroundColor;
                        }
                        break;
                    case NextUIType.HalfSpeedo:
                        if (frame != null)
                            frame.ScaleCollection[0].Pointer[0].Value = value;
                        if (lblValue != null)
                        {
                            lblValue.LabelText = value.ToString(ValueFormatString);
                            if (value < ErrorValueLow || value > ErrorValueHigh)
                                lblValue.BackColor = TextBackGroundColorError;
                            else if (value < WarningValueLow || value > WarningValueHigh)
                                lblValue.BackColor = TextBackGroundColorWarning;
                            else
                                lblValue.BackColor = TextBackGroundColor;
                        }
                        break;
                    case NextUIType.HorizontalLinear:
                        if (hframe != null)
                        {
                            hframe.ScaleCollection[0].Pointer[0].Value = value;
                            hframe.ScaleCollection[0].Range[hframe.ScaleCollection[0].Range.Count-1].EndValue = value;
                        }
                        if (lblValue != null)
                        {
                            lblValue.LabelText = value.ToString(ValueFormatString);
                            if (value < ErrorValueLow || value > ErrorValueHigh)
                                lblValue.BackColor = TextBackGroundColorError;
                            else if (value < WarningValueLow || value > WarningValueHigh)
                                lblValue.BackColor = TextBackGroundColorWarning;
                            else
                                lblValue.BackColor = TextBackGroundColor;
                        }
                        break;
                    case NextUIType.VerticalLinear:
                        if (vframe != null)
                        {
                            vframe.ScaleCollection[0].Range[vframe.ScaleCollection[0].Range.Count - 1].EndValue = value;
                            vframe.ScaleCollection[0].Pointer[0].Value = value;
                        }
                        if (lblValue != null)
                        {
                            lblValue.LabelText = value.ToString(ValueFormatString);
                            if (value < ErrorValueLow || value > ErrorValueHigh)
                                lblValue.BackColor = TextBackGroundColorError;
                            else if (value < WarningValueLow || value > WarningValueHigh)
                                lblValue.BackColor = TextBackGroundColorWarning;
                            else
                                lblValue.BackColor = TextBackGroundColor;
                        }
                        break;
                    case NextUIType.Graph:
                        break;
                    case NextUIType.Digital:
                        if (hframe != null)
                        {

                            if (value < ErrorValueLow || value > ErrorValueHigh)
                                hframe.BackRenderer.CenterColor = TextBackGroundColorError;
                            else if (value < WarningValueLow || value > WarningValueHigh)
                                hframe.BackRenderer.CenterColor = TextBackGroundColorWarning;
                            else
                                hframe.BackRenderer.CenterColor = TextBackGroundColor;
                            hframe.FrameLabelCollection[0].LabelText = value.ToString(ValueFormatString);
                        }
                        //if (nframe != null)
                        //{
                        //    nframe.Indicator.DisplayValue = Convert.ToString(value);
                        //    for (int i = 0; i < 6; i++)
                        //    {
                        //        //if (Value < ErrorValueLow || Value > ErrorValueHigh)
                        //        //    nframe.Indicator.Panels[i].BackColor = Color.Red;
                        //        //else if (Value < WarningValueLow || Value > WarningValueHigh)
                        //        //    nframe.Indicator.Panels[i].BackColor = Color.FromArgb(0xfe, 0xc3, 0x1a);
                        //        //else
                        //        //    nframe.Indicator.Panels[i].BackColor = Color.FromArgb(0x5a, 0x84, 0xdd);


                        //        //if (Value < ErrorValueLow || Value > ErrorValueHigh)
                        //        //{
                        //        //    nframe.Indicator.Panels[i].BackColor = Color.Red;
                        //        //    nframe.Indicator.Panels[i].MainColor = Color.Black;
                        //        //}
                        //        //else if (Value < WarningValueLow || Value > WarningValueHigh)
                        //        //{
                        //        //    nframe.Indicator.Panels[i].BackColor = Color.PaleGoldenrod;
                        //        //    nframe.Indicator.Panels[i].MainColor = Color.Black;
                        //        //}
                        //        //else
                        //        //{
                        //        //    nframe.Indicator.Panels[i].BackColor = Color.Black;
                        //        //    nframe.Indicator.Panels[i].MainColor = Color.Yellow;
                        //        //}
                        //    }
                        //}
                        break;
                    case NextUIType.Lamp:
                        if (hframe != null)
                        {
                            if (value == 0)
                            {
                                hframe.BackImage = global::NextUI.Properties.Resource1.레드램프;
                                //hframe.BackRenderer.CenterColor = Color.Goldenrod;
                                //hframe.BackRenderer.EndColor = Color.DarkSlateBlue;
                            }
                            else
                            {
                                hframe.BackImage = global::NextUI.Properties.Resource1.그린램프;
                                //hframe.BackRenderer.CenterColor = Color.LawnGreen;
                                //hframe.BackRenderer.EndColor = Color.Beige;
                            }
                        }
                        break;
                }
            }
        }

        /// <summary>
        ///  Grapg Value
        /// </summary>
        public DataPoint PointValue
        {
            get
            {
                return _PointValue;
            }
            set
            {
                switch (this.UIType)
                {
                    case NextUIType.Speedo:
                        break;
                    case NextUIType.HalfSpeedo:
                        break;
                    case NextUIType.Graph:
                        _PointValue = value;
                        if (chart1 != null)
                        {
                            chart1.Series[0].Points.Add(value);
                        }
                        break;
                    case NextUIType.Digital:
                        break;
                }
            }
        }
    }
}
