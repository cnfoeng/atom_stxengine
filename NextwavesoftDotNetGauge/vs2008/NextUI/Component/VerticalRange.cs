﻿// *****************************************************************************
// 
//  (c) NextWave Software  2007
//  All rights reserved. The software and associated documentation 
//  supplied hereunder are the proprietary information of NextWave Software 
//	Limited, Kuala Lumpur , Malaysia and are supplied subject to 
//	licence terms.
// 
//  Version 1.0 	www.nextwavesoft.com
// *****************************************************************************
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using NextUI.Common;

namespace NextUI.Component
{
    /// <summary>
    /// a range that is used by horizontal frame
    /// </summary>
    public class VerticalRange : RangeBase
    {
        /// <summary>
        /// basic constructor
        /// </summary>
        /// <param name="toplevel">the frame that is the parent of the scale that owns the range</param>
        public VerticalRange(Frame.Frame toplevel)
            : base(toplevel)
        {

        }
        internal override void Render(RectangleWrap frame, Graphics e)
        {
            if (this.Visible)
            {
                int widthScale = this.StartWidth;
                if (this.StartWidth < this.EndWidth)
                    widthScale = this.EndWidth;
                RectangleWrap bound = null;
                Shape.CustomTriangle2 shape = null;
                int startIndex = (int)this.Parent.CalculatePosition(this.StartValue);
                int endIndex = (int)this.Parent.CalculatePosition(this.EndValue);
                int indexwidth = endIndex - startIndex;
                int top = startIndex;
                if (startIndex == endIndex)
                    return;
                if (startIndex > endIndex)
                {
                    indexwidth = startIndex - endIndex;
                    top = endIndex;
                }
                switch (this.RangePosition)
                {
                    case Position.Cross:
                        bound = new RectangleWrap(new Rectangle(
                         frame.Rect.Left + this.Parent.ScaleBarSize / 2 - widthScale / 2,
                         top, widthScale, indexwidth));
                        shape = new Shape.CustomTriangle2(bound);
                        shape.EndWidth = this.EndWidth;
                        shape.StartWidth = this.StartWidth;
                        shape.FacingDirection = NextUI.Shape.CustomTriangle2.Direction.Both;
                        break;

                    case Position.Inner:
                        bound = new RectangleWrap(new Rectangle(
                            frame.Rect.Left + this.Parent.ScaleBarSize + this.OffsetFromScale,
                         top, widthScale, indexwidth));
                        shape = new Shape.CustomTriangle2(bound);
                        shape.EndWidth = this.EndWidth;
                        shape.StartWidth = this.StartWidth;
                        shape.FacingDirection = NextUI.Shape.CustomTriangle2.Direction.Left;
                        break;
                    case Position.Outer:
                        bound = new RectangleWrap(new Rectangle(
                        frame.Rect.Left - widthScale - this.OffsetFromScale,
                         top, widthScale, indexwidth));
                        shape = new Shape.CustomTriangle2(bound);
                        shape.EndWidth = this.EndWidth;
                        shape.StartWidth = this.StartWidth;
                        shape.FacingDirection = NextUI.Shape.CustomTriangle2.Direction.Right;
                        break;
                }

                this.Shadow.Render(shape, e);
                if (this.EnableGradient)
                {
                    this.Gradient.FillGradientType = NextUI.Renderer.RendererGradient.GradientType.DiagonalLeft;
                    this.Gradient.CenterColor = Color.FromArgb(this.Opaque, this.FillColor);
                    this.Gradient.EndColor = Color.FromArgb(this.Opaque, this.EndColor);
                    this.Gradient.RenderFill(shape, e);
                }
                else
                {
                    e.FillPath(new SolidBrush(Color.FromArgb(this.Opaque, this.FillColor)), shape.RenderPath());
                }
                if (this.EnableBorder)
                {
                    e.DrawPath(new Pen(this.BorderColor, this.BorderWidth), shape.RenderPath());
                }
            }

        }
    }
}
